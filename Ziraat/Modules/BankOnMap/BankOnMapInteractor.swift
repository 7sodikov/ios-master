//
//  BankOnMapInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BankOnMapInteractorProtocol: class {
    func getMapList()
}

class BankOnMapInteractor: BankOnMapInteractorProtocol {
    weak var presenter: BankOnMapInteractorToPresenterProtocol!
    
    func getMapList() {
        NetworkService.Map.map { [weak self] (result) in
            self?.presenter.didGetMapList(with: result)
        }
    }
}
