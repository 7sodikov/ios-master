//
//  MapPinInfoInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 6/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MapPinInfoInteractorProtocol: class {
    
}

class MapPinInfoInteractor: MapPinInfoInteractorProtocol {
    weak var presenter: MapPinInfoInteractorToPresenterProtocol!
    
}
