//
//  MapPinInfoPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 6/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MapPinInfoPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: MapPinInfoVM { get }
}

protocol MapPinInfoInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class MapPinInfoPresenter: MapPinInfoPresenterProtocol {
    weak var view: MapPinInfoVCProtocol!
    var interactor: MapPinInfoInteractorProtocol!
    var router: MapPinInfoRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = MapPinInfoVM()
    
    // Private property and methods
    
}

extension MapPinInfoPresenter: MapPinInfoInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
