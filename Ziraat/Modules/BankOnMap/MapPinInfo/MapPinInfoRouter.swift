//
//  MapPinInfoRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 6/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MapPinInfoRouterProtocol: class {
    static func createModule(title: [MapPoint]) -> UIViewController
}

class MapPinInfoRouter: MapPinInfoRouterProtocol {
    static func createModule(title: [MapPoint]) -> UIViewController {
        let vc = MapPinInfoVC()
        let presenter = MapPinInfoPresenter()
        let interactor = MapPinInfoInteractor()
        let router = MapPinInfoRouter()
        
        vc.presenter = presenter
        vc.titleView = title[0]
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
