//
//  MapPinInfoVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 6/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MapPinInfoVCProtocol: class {
    
}

class MapPinInfoVC: UIViewController, MapPinInfoViewInstaller {
    var separatorLine: UIView!
    var address: UILabel!
    var phoneNumberLabel: UILabel!
    var dayOffsLabel: UILabel!
    var confirmButton: NextButton!
    var mainView: UIView { view }
    var titleLabel: UILabel!
    var presenter: MapPinInfoPresenterProtocol!
    
    var titleView: MapPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupData()
        
        confirmButton.addTarget(self, action: #selector(confirmButtonClicked), for: .touchUpInside)
        
    }
    
    @objc func confirmButtonClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupData() {
        titleLabel.text = titleView?.name
        address.text = titleView?.address
        phoneNumberLabel.text = titleView?.phone
        dayOffsLabel.text = titleView?.weekend
        
    }
    
}

extension MapPinInfoVC: MapPinInfoVCProtocol {
    
}
