//
//  MapPinInfoViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 6/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MapPinInfoViewInstaller: ViewInstaller {
    // declare your UI elements here
    var titleLabel: UILabel! { get set }
    var separatorLine: UIView! { get set }
    var address: UILabel! { get set }
    var phoneNumberLabel: UILabel! { get set }
    var dayOffsLabel: UILabel! { get set }
    var confirmButton: NextButton! { get set }
}

extension MapPinInfoViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        mainView.backgroundColor = .white
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 27, .medium)
        titleLabel.textColor = .dashDarkBlue
        titleLabel.numberOfLines = 0
        
        separatorLine = UIView()
        separatorLine.backgroundColor = .dashDarkBlue
        
        address = UILabel()
        address.font = .gothamNarrow(size: 15, .book)
        address.textColor = .dashDarkBlue
        address.numberOfLines = 0
        
        phoneNumberLabel = UILabel()
        phoneNumberLabel.font = .gothamNarrow(size: 15, .book)
        phoneNumberLabel.textColor = .dashDarkBlue
        phoneNumberLabel.textAlignment = .left
        
        dayOffsLabel = UILabel()
        dayOffsLabel.font = .gothamNarrow(size: 15, .book)
        dayOffsLabel.textColor = .dashDarkBlue
        dayOffsLabel.textAlignment = .right
        dayOffsLabel.numberOfLines = 0
        
        confirmButton = NextButton.systemButton(with: RS.btn_confirm.localized(), cornerRadius: Adaptive.val(25))
        confirmButton.setBackgroundColor(.dashRed, for: .normal)
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
        mainView.addSubview(titleLabel)
        mainView.addSubview(separatorLine)
        mainView.addSubview(address)
        mainView.addSubview(phoneNumberLabel)
        mainView.addSubview(dayOffsLabel)
        mainView.addSubview(confirmButton)

    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        titleLabel.snp.remakeConstraints { (maker) in
            maker.leading.top.equalToSuperview().offset(Adaptive.val(30))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(30))
        }
        
        address.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(30))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(30))
            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(50))
        }
        
        separatorLine.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(15))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            maker.height.equalTo(Adaptive.val(1))
            maker.top.equalTo(address.snp.bottom).offset(Adaptive.val(12))
        }
        
        phoneNumberLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(30))
            maker.top.equalTo(separatorLine.snp.bottom).offset(Adaptive.val(17))
        }
        
        dayOffsLabel.snp.remakeConstraints { (maker) in
            maker.trailing.equalToSuperview().offset(-Adaptive.val(30))
            maker.leading.equalTo(phoneNumberLabel.snp.trailing)
            maker.top.equalTo(separatorLine.snp.bottom).offset(Adaptive.val(17))
        }
        
        confirmButton.snp.remakeConstraints { (maker) in
            maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            maker.leading.equalToSuperview().offset(Adaptive.val(15))
            maker.top.equalTo(phoneNumberLabel.snp.bottom).offset(Adaptive.val(15))
            maker.height.equalTo(Adaptive.val(50))
        }
    }
}

fileprivate struct Size {
    
}
