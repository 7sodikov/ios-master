//
//  BankOnMapRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BankOnMapRouterProtocol: class {
    static func createModule() -> UIViewController
}

class BankOnMapRouter: BankOnMapRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = BankOnMapVC()
        let presenter = BankOnMapPresenter()
        let interactor = BankOnMapInteractor()
        let router = BankOnMapRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
