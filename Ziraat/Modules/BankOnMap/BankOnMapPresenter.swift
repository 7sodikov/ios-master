//
//  BankOnMapPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BankOnMapPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: BankOnMapVM { get }
    func viewDidLoad()
}

protocol BankOnMapInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetMapList(with response: ResponseResult<[MapPoint], AppError>)
}

class BankOnMapPresenter: BankOnMapPresenterProtocol {
    weak var view: BankOnMapVCProtocol!
    var interactor: BankOnMapInteractorProtocol!
    var router: BankOnMapRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = BankOnMapVM()
    
    func viewDidLoad() {
        interactor.getMapList()
    }
    
    // Private property and methods
    
}

extension BankOnMapPresenter: BankOnMapInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetMapList(with response: ResponseResult<[MapPoint], AppError>) {
        switch response {
        case let .success(result):
            viewModel.mapResponse = result.data
            view.setMap(locations: viewModel.mapResponse)
        case let .failure(_):
            break
        }
    }
}
