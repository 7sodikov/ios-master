//
//  BankOnMapVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol BankOnMapVCProtocol: class {
    func setMap(locations: [MapPoint])
    func setMapFromTableView(locations: MapPoint)
}

class BankOnMapVC: BaseViewController, BankOnMapViewInstaller {
    var mainView: UIView { view }
    var notificationsButton: UIButton!
    var searchButton: UIButton!
    var backView: UIImageView!
    var filterView: EZMapFilterControl!
    var mainMapView: MKMapView!
    var zoomInButton: UIButton!
    var zoomOutButton: UIButton!
    var userLocationButton: UIButton!
    var infoButton: UIButton!
    
    var presenter: BankOnMapPresenterProtocol!
    
    weak var delegate: MKMapViewDelegate?
    
    let manager = CLLocationManager()
//    var annotations = [MKAnnotation]()
    let regionInMeters: Double = 10000

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()

        setupSubviews()
        setNavigation()
        checkLocationServices()

        mainMapView.delegate = self
                
        userLocationButton.addTarget(self, action: #selector(userCurrentLocationClicked), for: .touchUpInside)
        zoomInButton.addTarget(self, action: #selector(zoomInClicked), for: .touchUpInside)
        zoomOutButton.addTarget(self, action: #selector(zoomOutClicked), for: .touchUpInside)
        infoButton.addTarget(self, action: #selector(infoButtonClicked), for: .touchUpInside)

        filterView.addTarget(self, action: #selector(segmentedControlChanged(_:)), for: .valueChanged)
    }
    
    @objc func infoButtonClicked() {
        var points: [MapPoint] = []
        if filterView.selectedIndex == 0 {
            points = presenter.viewModel.mapResponse
        } else if filterView.selectedIndex == 1 {
            points = presenter.viewModel.atmItem
        } else {
            points = presenter.viewModel.bankItem
        }
        let slideVC = BootomSheetViewControllerRouter.createModule(points: points, delegate: self)
        slideVC.modalPresentationStyle = .custom
        slideVC.transitioningDelegate = self
        self.present(slideVC, animated: true, completion: nil)
    }
    
    @objc func zoomInClicked() {
        let region = MKCoordinateRegion(center: self.mainMapView.region.center, span: MKCoordinateSpan(latitudeDelta: mainMapView.region.span.latitudeDelta*0.7, longitudeDelta: mainMapView.region.span.longitudeDelta*0.7))
        mainMapView.setRegion(region, animated: true)
    }
    
    @objc func zoomOutClicked() {
        let zoom = getZoom() // to get the value of zoom of your map.
           if zoom > 3.5{ // **here i have used the condition that avoid the mapview to zoom less then 3.5 to avoid crash.**

            let region = MKCoordinateRegion(center: self.mainMapView.region.center, span: MKCoordinateSpan(latitudeDelta: mainMapView.region.span.latitudeDelta/0.7, longitudeDelta: mainMapView.region.span.longitudeDelta/0.7))
            mainMapView.setRegion(region, animated: true)
        }
    }
    
    func getZoom() -> Double {
        var angleCamera = self.mainMapView.camera.heading
        if angleCamera > 270 {
            angleCamera = 360 - angleCamera
        } else if angleCamera > 90 {
            angleCamera = fabs(angleCamera - 180)
        }
        let angleRad = Double.pi * angleCamera / 180
        let width = Double(self.view.frame.size.width)
        let height = Double(self.view.frame.size.height)
        let heightOffset : Double = 20
        let spanStraight = width * self.mainMapView.region.span.longitudeDelta / (width * cos(angleRad) + (height - heightOffset) * sin(angleRad))
        return log2(360 * ((width / 256) / spanStraight)) + 1;
    }
    
    @objc private func segmentedControlChanged(_ sender: EZMapFilterControl) {
        if sender.selectedIndex == 0 {
            removeAnnotations()
            setMap(locations: presenter.viewModel.mapItem)
        } else if sender.selectedIndex == 1 {
            removeAnnotations()
            setMap(locations: presenter.viewModel.atmItem)
        } else {
            removeAnnotations()
            setMap(locations: presenter.viewModel.bankItem)
        }
    }
    
    func removeAnnotations() {
        let annotations = mainMapView.annotations.filter {
                $0 !== self.mainMapView.userLocation
            }
        mainMapView.removeAnnotations(annotations)
    }
    
    @objc func userCurrentLocationClicked() {
        checkLocationServices()
    }
    
    func setupLocationManager() {
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
    }
        
        
    func centerViewOnUserLocation() {
        if let location = manager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mainMapView.setRegion(region, animated: true)
        }
    }
        
        
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        (self.navigationController as? NavigationController)?.background(isWhite: false)
    }
        
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mainMapView.showsUserLocation = true
            centerViewOnUserLocation()
            manager.startUpdatingLocation()
            break
        case .denied:
            // Show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .restricted:
            // Show an alert letting them know what's up
            break
        case .authorizedAlways:
            break
        @unknown default:
            break
        }
    }
    
    private func setNavigation() {
        self.title = RS.lbl_nearest_branch.localized()
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: searchButton)]
    }
}

extension BankOnMapVC: BankOnMapVCProtocol, MKMapViewDelegate, CLLocationManagerDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
                
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
            annotationView?.canShowCallout = true
        }
        
        annotationView?.annotation = annotation

        if let title = annotation.title, title?.contains("ATM") == true || title?.contains("OPERATIONAL DEPARTMENT") == true {
            annotationView?.image = UIImage(named: "img_pin_atm")
        } else if annotation as! NSObject == mapView.userLocation {
            annotationView?.image = UIImage(named: "img_user_location")
        } else {
            annotationView?.image = UIImage(named: "img_pin_branch")
        }
//        annotationView?.image = UIImage(named: "img_pin_atm")
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {

        // center the mapView on the selected pin
        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: mapView.region.span)
        mapView.setRegion(region, animated: true)
        
        var mapItem: [MapPoint] {
            return presenter.viewModel.mapResponse.filter { $0.name == view.annotation?.title }
        }
        
        let slideVC = MapPinInfoRouter.createModule(title: mapItem)
        slideVC.modalPresentationStyle = .custom
        slideVC.transitioningDelegate = self
        self.present(slideVC, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            guard let location = locations.last else { return }
            let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mainMapView.setRegion(region, animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
    func setMap(locations: [MapPoint]) {
        
        for location in locations {

            let annotations = MKPointAnnotation()

            annotations.title = location.name
            annotations.coordinate = CLLocationCoordinate2D(latitude: Double(location.latitude)!, longitude: Double(location.longitude)!)

            mainMapView.addAnnotation(annotations)

            let locationCoordinate2d = annotations.coordinate
            let span = MKCoordinateSpan(latitudeDelta: 6, longitudeDelta: 6)
            let region = MKCoordinateRegion(center: locationCoordinate2d, span: span)

            mainMapView.setRegion(region, animated: false)
        }
    }
    
    func setMapFromTableView(locations: MapPoint) {
        let location = CLLocationCoordinate2DMake(CLLocationDegrees(locations.latitude)!, CLLocationDegrees(locations.longitude)!)
        //CLLocationCoordinate2D(latitude: CLLocationDegrees(locations?.latitude), longitude: CLLocationDegrees(locations?.longitude))
        let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mainMapView.setRegion(region, animated: true)
        
    }
}

extension BankOnMapVC: UIViewControllerTransitioningDelegate, BootomSheetViewControllerVCDelegate {
    func navigateToSpecificPin(pin: MapPoint) {
        setMapFromTableView(locations: pin)
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationTestController(presentedViewController: presented, presenting: presenting)
    }
}
