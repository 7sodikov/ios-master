//
//  BankOnMapViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol BankOnMapViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var notificationsButton: UIButton! { get set }
    var searchButton: UIButton! { get set }
    var filterView: EZMapFilterControl! { get set }
    var mainMapView: MKMapView! { get set }
    var zoomInButton: UIButton! { get set }
    var zoomOutButton: UIButton! { get set }
    var userLocationButton: UIButton! { get set }
    var infoButton: UIButton! { get set }
}

extension BankOnMapViewInstaller {
    func initSubviews() {        
        backView = UIImageView()
        backView.backgroundColor = .white
        
        notificationsButton = UIButton()
        notificationsButton.setImage(UIImage(named: "btn_bell"), for: .normal)
        
        searchButton = UIButton()
        searchButton.setImage(UIImage(named: "btn_search"), for: .normal)
        
        filterView = EZMapFilterControl()
        filterView.layer.borderWidth = Adaptive.val(2)
        filterView.layer.borderColor = ColorConstants.hintGray.cgColor
        filterView.layer.cornerRadius = Adaptive.val(10)
        filterView.addSegment(with: RS.lbl_all.localized())
        filterView.addSegment(with: RS.btn_atm.localized())
        filterView.addSegment(with: RS.lbl_branch_name.localized())
        filterView.selectedIndex = 0
        
        mainMapView = MKMapView()
        
        zoomInButton = UIButton()
        zoomInButton.setBackgroundColor(.white, for: .normal)
        zoomInButton.setBackgroundColor(.lightGray, for: .selected)
        zoomInButton.clipsToBounds = true
        zoomInButton.layer.cornerRadius = Adaptive.val(10)
        zoomInButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        zoomInButton.setTitle("+", for: .normal)
        zoomInButton.titleLabel?.font = .gothamNarrow(size: 30, .medium)
        zoomInButton.setTitleColor(.dashDarkBlue, for: .normal)
        zoomInButton.setTitleColor(.white, for: .highlighted)
        
        zoomOutButton = UIButton()
        zoomOutButton.setBackgroundColor(.white, for: .normal)
        zoomOutButton.setBackgroundColor(.lightGray, for: .selected)
        zoomOutButton.clipsToBounds = true
        zoomOutButton.layer.cornerRadius = Adaptive.val(10)
        zoomOutButton.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        zoomOutButton.setTitle("-", for: .normal)
        zoomOutButton.titleLabel?.font = .gothamNarrow(size: 30, .medium)
        zoomOutButton.setTitleColor(.dashDarkBlue, for: .normal)
        zoomOutButton.setTitleColor(.white, for: .highlighted)
        
        userLocationButton = UIButton()
        userLocationButton.layer.cornerRadius = Adaptive.val(8)
        userLocationButton.setBackgroundImage(UIImage(named: "btn_user_location"), for: .normal)
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "btn_info"), for: .normal)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(filterView)
        mainView.addSubview(mainMapView)
        mainView.addSubview(zoomInButton)
        mainView.addSubview(zoomOutButton)
        mainView.addSubview(userLocationButton)
        mainView.addSubview(infoButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        filterView.constraint { (maker) in
            maker.horizontal(Adaptive.val(16)).equalToSuperView()
            maker.top.equalToSuperView().offset(Adaptive.val(15))
        }
        
        mainMapView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(filterView.snp.bottom)
            maker.leading.trailing.equalToSuperview()
            maker.bottom.equalToSuperview().offset(Adaptive.val(100))
        }
        
        zoomInButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(44))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.bottom.equalTo(zoomOutButton.snp.top)
        }
        
        zoomOutButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(44))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.bottom.equalTo(userLocationButton.snp.top).offset(-Adaptive.val(9))
        }
        
        userLocationButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(44))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(49))
        }
        
        infoButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(44))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(49))
        }
    }
}

fileprivate struct Size {
    
}
