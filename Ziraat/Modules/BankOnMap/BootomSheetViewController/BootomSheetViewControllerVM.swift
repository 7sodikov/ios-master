//
//  BootomSheetViewControllerVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class BootomSheetViewControllerVM {
    //represent model from json object
    var mapPoints: [MapPoint]?
    var mapItems: [BootomSheetItemVM] = []
    
    func add() {
        for point in mapPoints ?? [] {
            mapItems.append(BootomSheetItemVM(point: point))
        }
    }
}

class BootomSheetItemVM {
    //represent model from json object
    var mapPoints: MapPoint
    
    init(point: MapPoint) {
        mapPoints = point
    }
}
