//
//  BootomSheetViewControllerInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BootomSheetViewControllerInteractorProtocol: class {
    
}

class BootomSheetViewControllerInteractor: BootomSheetViewControllerInteractorProtocol {
    weak var presenter: BootomSheetViewControllerInteractorToPresenterProtocol!
    
}
