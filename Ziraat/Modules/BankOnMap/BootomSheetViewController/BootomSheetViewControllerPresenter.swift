//
//  BootomSheetViewControllerPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BootomSheetViewControllerPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: BootomSheetViewControllerVM { get }
    func tableViewCellSelected(index: Int)
}

protocol BootomSheetViewControllerInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class BootomSheetViewControllerPresenter: BootomSheetViewControllerPresenterProtocol {
    weak var view: BootomSheetViewControllerVCProtocol!
    var interactor: BootomSheetViewControllerInteractorProtocol!
    var router: BootomSheetViewControllerRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = BootomSheetViewControllerVM()
    
    func tableViewCellSelected(index: Int) {
        let mapItem = viewModel.mapPoints?[index]
    }
    
    // Private property and methods
    
}

extension BootomSheetViewControllerPresenter: BootomSheetViewControllerInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
