//
//  BootomSheetViewControllerRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BootomSheetViewControllerRouterProtocol: class {
    static func createModule(points: [MapPoint], delegate: BootomSheetViewControllerVCDelegate?) -> UIViewController
}

class BootomSheetViewControllerRouter: BootomSheetViewControllerRouterProtocol {
    static func createModule(points: [MapPoint], delegate: BootomSheetViewControllerVCDelegate?) -> UIViewController {
        let vc = BootomSheetViewControllerVC()
        let presenter = BootomSheetViewControllerPresenter()
        let interactor = BootomSheetViewControllerInteractor()
        let router = BootomSheetViewControllerRouter()
        
        vc.presenter = presenter
        vc.delegate = delegate
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.mapPoints = points
        interactor.presenter = presenter
        
        return vc
    }
    
}
