//
//  BootomSheetViewControllerViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BootomSheetViewControllerViewInstaller: ViewInstaller {
    // declare your UI elements here
    var tableView: TableView! { get set }
}

extension BootomSheetViewControllerViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .white
        
        tableView = TableView()
        tableView.register(MapInfoTableViewCell.self, forCellReuseIdentifier: String(describing: MapInfoTableViewCell.self))
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(16))
            maker.leading.bottom.trailing.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
