//
//  BootomSheetViewControllerVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BootomSheetViewControllerVCDelegate: class {
    func navigateToSpecificPin(pin: MapPoint)
}

protocol BootomSheetViewControllerVCProtocol: class {
    
}

class BootomSheetViewControllerVC: UIViewController, BootomSheetViewControllerViewInstaller {
    var mainView: UIView { view }
    var tableView: TableView!
    var presenter: BootomSheetViewControllerPresenterProtocol!
    
    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    
    weak var delegate: BootomSheetViewControllerVCDelegate?
    
    fileprivate let cellid = "\(MapInfoTableViewCell.self)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
        
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }
        
        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
        
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
                }
            }
        }
    }
}

extension BootomSheetViewControllerVC: BootomSheetViewControllerVCProtocol, UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.mapPoints?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! MapInfoTableViewCell
        presenter.viewModel.add()
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        let mapItemVM = presenter.viewModel.mapItems[indexPath.row]
        return cell.setup(notificationVM: mapItemVM)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        presenter.tableViewCellSelected(index: indexPath.row)
        let mapItem = presenter.viewModel.mapPoints?[indexPath.row]
        delegate?.navigateToSpecificPin(pin: mapItem!)
        self.dismiss(animated: true, completion: nil)
    }
}
