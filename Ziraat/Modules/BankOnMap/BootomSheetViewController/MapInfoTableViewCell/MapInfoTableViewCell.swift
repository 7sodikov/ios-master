//
//  MapInfoTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/28/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class MapInfoTableViewCell: UITableViewCell, MapInfoTableViewCellInstaller {
    var iconImageView: UIImageView!
    var descriptionLabel: UILabel!
    var distanceLabel: UILabel!
    var arrowImageView: UIImageView!
    var bgView: UIView!
    
    var mainView: UIView { contentView }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(notificationVM: BootomSheetItemVM) -> MapInfoTableViewCell {
        descriptionLabel.text = notificationVM.mapPoints.name
        distanceLabel.text = notificationVM.mapPoints.address
        if notificationVM.mapPoints.type == MapPointType.atm {
            iconImageView.image = UIImage(named: "img_pin_atm")
        } else {
            iconImageView.image = UIImage(named: "img_pin_branch")
        }
        return self
    }
}
