//
//  MapInfoTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 5/28/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MapInfoTableViewCellInstaller: ViewInstaller {
    var bgView : UIView! { get set }
    var iconImageView: UIImageView! { get set }
    var descriptionLabel: UILabel! { get set }
    var distanceLabel: UILabel! { get set }
    var arrowImageView: UIImageView! { get set }
}

extension MapInfoTableViewCellInstaller {
    func initSubviews() {
        bgView = UIView()
        bgView.backgroundColor = .white
        bgView.layer.cornerRadius = Adaptive.val(6)
        bgView.layer.borderWidth = Adaptive.val(2)
        bgView.layer.borderColor = ColorConstants.lightGray.cgColor
        
        iconImageView = UIImageView()
        
        descriptionLabel = UILabel()
        descriptionLabel.font = .gothamNarrow(size: 16, .bold)
        descriptionLabel.textColor = .dashDarkBlue
        descriptionLabel.numberOfLines = 0
        
        distanceLabel = UILabel()
        distanceLabel.font = .gothamNarrow(size: 15, .medium)
        distanceLabel.textColor = .dashDarkBlue
        distanceLabel.numberOfLines = 0
        
        arrowImageView = UIImageView()
        arrowImageView.image = UIImage(named: "img_icon_arr")
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(iconImageView)
        mainView.addSubview(descriptionLabel)
        mainView.addSubview(distanceLabel)
        mainView.addSubview(arrowImageView)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.remakeConstraints { (make) in
            make.leading.equalToSuperview().offset(Adaptive.val(16))
            make.trailing.equalToSuperview().offset(-Adaptive.val(16))
            make.top.equalToSuperview().offset(Adaptive.val(7.5))
            make.bottom.equalToSuperview().offset(-Adaptive.val(7.5))
            make.height.equalTo(Adaptive.val(79))
        }
        
        iconImageView.snp.remakeConstraints { (make) in
            make.width.equalTo(Adaptive.val(43))
            make.height.equalTo(Adaptive.val(55))
            make.leading.equalTo(bgView.snp.leading).offset(Adaptive.val(12))
            make.centerY.equalTo(bgView)
        }
        
        descriptionLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(bgView.snp.top).offset(Adaptive.val(14))
            make.leading.equalTo(iconImageView.snp.trailing).offset(Adaptive.val(17))
            make.trailing.equalTo(arrowImageView.snp.leading).offset(Adaptive.val(10))
        }
        
        distanceLabel.snp.remakeConstraints { (make) in
            make.bottom.equalTo(bgView.snp.bottom).offset(-Adaptive.val(6))
            make.leading.equalTo(descriptionLabel.snp.leading)
            make.trailing.equalTo(descriptionLabel.snp.trailing)
        }
        
        arrowImageView.snp.remakeConstraints { (make) in
            make.width.equalTo(Adaptive.val(12))
            make.height.equalTo(Adaptive.val(21))
            make.centerY.equalTo(bgView)
            make.trailing.equalTo(bgView.snp.trailing).offset(-Adaptive.val(17))
        }
    }
}
