//
//  BankOnMapVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import MapKit

class Annotation: NSObject, MKAnnotation {
    var uid: String
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    var markerImage: UIImage?
    
    init(uid: String, title: String = "", subTitle: String = "", markerImage: UIImage?, coordinate: CLLocationCoordinate2D) {
        self.uid = uid
        self.title = title
        self.subtitle = subTitle
        self.coordinate = coordinate
        self.markerImage = markerImage
        super.init()
    }
    
    public func mapItem() -> MKMapItem {
        let placemark = MKPlacemark(coordinate: coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title

        return mapItem
    }
}

class BankOnMapVM {
    var mapResponse: [MapPoint] = []
    var mapItems: [BankOnMapItem] = []
    
    var mapItem: [MapPoint] {
        return mapResponse
    }
    
    var bankItem: [MapPoint] {
        return mapResponse.filter { $0.type == MapPointType.branch }
    }
    
    var atmItem: [MapPoint] {
        return mapResponse.filter { $0.type == MapPointType.atm }
    }
}

class BankOnMapItem {
    var mapItem: MapPoint
    
    init(mapItem: MapPoint) {
        self.mapItem = mapItem
    }
}
