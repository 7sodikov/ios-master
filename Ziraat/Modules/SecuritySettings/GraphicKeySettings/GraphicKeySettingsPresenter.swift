//
//  GraphicKeySettingsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol GraphicKeySettingsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: GraphicKeySettingsVM { get }
}

protocol GraphicKeySettingsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class GraphicKeySettingsPresenter: GraphicKeySettingsPresenterProtocol {
    weak var view: GraphicKeySettingsVCProtocol!
    var interactor: GraphicKeySettingsInteractorProtocol!
    var router: GraphicKeySettingsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = GraphicKeySettingsVM()
    
    // Private property and methods
    
}

extension GraphicKeySettingsPresenter: GraphicKeySettingsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
