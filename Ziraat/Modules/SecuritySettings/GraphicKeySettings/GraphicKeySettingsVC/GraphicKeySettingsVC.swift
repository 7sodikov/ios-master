//
//  GraphicKeySettingsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol GraphicKeySettingsVCProtocol: class {
    
}

class GraphicKeySettingsVC: BaseViewController, GraphicKeySettingsViewInstaller {
    var backView: UIImageView!
    var logoImageView: UIImageView!
    var enterPinLabel: UILabel!
    var pinKeyboardView: GraphicKeyCellKeyboard!
    var forgotPasscodeButton: UIButton!
    var mainView: UIView { view }
    var presenter: GraphicKeySettingsPresenterProtocol!
    var lockView: YLSwipeLockView!
    var lockContainerView: UIView!
    var createMode = false
    var currentGraphic : String?
    var errorCount = 0
    
    var didCorrectGraphic :((Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        lockView.delegate = self
        if createMode {
            
        } else{
            currentGraphic = KeychainManager.graphicKey
        }
        
    }
    
    @objc private func successEnterGraphicKey(){
        if self.didCorrectGraphic != nil{
            self.didCorrectGraphic!(true)
        }
    }
    
    @objc private func successInstallGraphicKey(){
        KeychainManager.graphicKey = currentGraphic
        if self.didCorrectGraphic != nil{
            self.didCorrectGraphic!(true)
        }
    }
    func errorGraphicKey(){
        lockContainerView.shake({ (complete) in
            self.currentGraphic = nil
        })
        enterPinLabel.text = RS.lbl_incorrect.localized()
    }
}

extension GraphicKeySettingsVC: GraphicKeySettingsVCProtocol {
    
}

extension GraphicKeySettingsVC : YLSwipeLockViewDelegate{
    func swipeView(_ swipeView: YLSwipeLockView!, didEndSwipeWithPassword password: String!) -> YLSwipeLockViewState {
        //print(password)
        if createMode{
            if currentGraphic == nil{
                currentGraphic = password
                enterPinLabel.text = RS.btn_repeat.localized()
                
            } else{
                if  currentGraphic == password{
                    enterPinLabel.text = RS.lbl_success.localized()
                    perform(#selector(successInstallGraphicKey), with: nil, afterDelay: 0.5)
                    return .selected
                } else{
                    // repeat error
                    errorGraphicKey()
                    return .warning
                    
                }
            }
            return .normal;
        }
        
        if password == currentGraphic{
            perform(#selector(successEnterGraphicKey), with: nil, afterDelay: 0.5)
        } else{
            errorGraphicKey()
            if (errorCount == 3) {
                if self.didCorrectGraphic != nil{
                    self.didCorrectGraphic!(false)
                }
            }
            errorCount += 1
            return .warning
        }
        return .normal
    }
}
