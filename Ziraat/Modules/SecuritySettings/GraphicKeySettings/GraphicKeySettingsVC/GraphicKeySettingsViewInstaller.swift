//
//  GraphicKeySettingsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol GraphicKeySettingsViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var logoImageView: UIImageView! { get set }
    var enterPinLabel: UILabel! { get set }
    var pinKeyboardView: GraphicKeyCellKeyboard! { get set }
    var forgotPasscodeButton: UIButton! { get set }
    var lockView: YLSwipeLockView! {get set}
    var lockContainerView: UIView! {get set}
}


extension GraphicKeySettingsViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_background")
        
        logoImageView = UIImageView()
        logoImageView.image = UIImage(named: "img_logo_with_text")
        
        enterPinLabel = UILabel()
        enterPinLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(15))
        enterPinLabel.textColor = .white
        enterPinLabel.text = RS.lbl_draw_pattern.localized()
        
        pinKeyboardView = GraphicKeyCellKeyboard()
        
        forgotPasscodeButton = UIButton()
        func attributedStr(with color: UIColor) -> NSAttributedString {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: EZFontType.regular.sfProDisplay(size: Adaptive.val(15))!,
                .foregroundColor: color,
                .underlineStyle: NSUnderlineStyle.single.rawValue]
            return NSMutableAttributedString(string: RS.lbl_forgot_pattern.localized(), attributes: attributes)
        }
        let normalAttributeString = attributedStr(with: .white)
        let highlightedAttributeString = attributedStr(with: UIColor.white.withAlphaComponent(0.5))
        forgotPasscodeButton.setAttributedTitle(normalAttributeString, for: .normal)
        forgotPasscodeButton.setAttributedTitle(highlightedAttributeString, for: .highlighted)
        
        
        lockView = YLSwipeLockView(frame: .zero)
        lockContainerView = UIView()
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(logoImageView)
        mainView.addSubview(enterPinLabel)
        mainView.addSubview(pinKeyboardView)
        mainView.addSubview(forgotPasscodeButton)
        mainView.addSubview(lockContainerView)
        lockContainerView.addSubview(lockView)
        
        pinKeyboardView.isHidden = true
        forgotPasscodeButton.isHidden = true
        
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        logoImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.Logo.width)
            maker.height.equalTo(Size.Logo.height)
            maker.top.equalToSuperview().offset(Adaptive.val(112))
            maker.centerX.equalToSuperview()
        }
        
        enterPinLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(logoImageView.snp.bottom).offset(Adaptive.val(60))
            maker.centerX.equalToSuperview()
        }
        
        pinKeyboardView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(enterPinLabel.snp.bottom).offset(Adaptive.val(62))
            maker.leading.trailing.equalToSuperview()
            maker.height.equalTo(Adaptive.val(372))
        }
        
        lockContainerView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(enterPinLabel.snp.bottom).offset(Adaptive.val(62))
            maker.leading.equalTo(30)
            maker.right.equalTo(-30)
            maker.height.equalTo(Adaptive.val(372))
        }
        lockView.snp.remakeConstraints { (make) in
            make.left.bottom.right.top.equalToSuperview()
        }

        forgotPasscodeButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(pinKeyboardView.snp.bottom).offset(Adaptive.val(35))
            maker.centerX.equalToSuperview()
        }    }
}

fileprivate struct Size {
    struct Logo {
        static let width = Adaptive.val(155)
        static let height = Adaptive.val(46)
    }
}
