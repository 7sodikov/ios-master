//
//  GraphicKeySettingsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol GraphicKeySettingsInteractorProtocol: class {
    
}

class GraphicKeySettingsInteractor: GraphicKeySettingsInteractorProtocol {
    weak var presenter: GraphicKeySettingsInteractorToPresenterProtocol!
    
}
