//
//  GraphicKeySettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol GraphicKeySettingsRouterProtocol: class {
    static func createModule(createMode: Bool) -> UIViewController
}

class GraphicKeySettingsRouter: GraphicKeySettingsRouterProtocol {
    
    static func createModule(createMode: Bool) -> UIViewController {
        let vc = GraphicKeySettingsVC()
        vc.createMode = createMode
        let presenter = GraphicKeySettingsPresenter()
        let interactor = GraphicKeySettingsInteractor()
        let router = GraphicKeySettingsRouter()
        
        vc.didCorrectGraphic = { complete in
            if complete{
                appDelegate.router.navigate(to: .mainPage)
            } else{
                UDManager.logout()
                appDelegate.router.navigate(to: .loginInit)
            }
            
        }
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
