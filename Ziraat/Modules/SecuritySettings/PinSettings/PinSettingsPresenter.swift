//
//  PinSettingsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PinSettingsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: PinSettingsVM { get }
    func pinCorrect(with navigationCtrl: Any)
    func forgotPasscodePressed(with navigationCtrl: Any)
    func pinIncorrect(with navigationCtrl: Any)
}

protocol PinSettingsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class PinSettingsPresenter: PinSettingsPresenterProtocol {
    weak var view: PinSettingsVCProtocol!
    var interactor: PinSettingsInteractorProtocol!
    var router: PinSettingsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PinSettingsVM()
    
    // Private property and methods
    
    func pinCorrect(with navigationCtrl: Any) {
        router.navigateToHomeVC(in: navigationCtrl)
    }
    
    func forgotPasscodePressed(with navigationCtrl: Any) {
        router.navigateToHomeVC(in: navigationCtrl)
    }
    
    func pinIncorrect(with navigationCtrl: Any) {
        router.navigateToLogin(in: navigationCtrl)
    }
}

extension PinSettingsPresenter: PinSettingsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
