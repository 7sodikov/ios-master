//
//  PinSettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PinSettingsRouterProtocol: class {
    static func createModule(createPin: Bool) -> UIViewController
    func navigateToHomeVC(in navigationCtrl: Any)
    func navigateToLogin(in navigationCtrl: Any)
}

class PinSettingsRouter: PinSettingsRouterProtocol {
    static let delegate = PinSettingsDelegate()

    static func createModule(createPin: Bool) -> UIViewController {
        let vc = PinSettingsVC()
        let presenter = PinSettingsPresenter()
        let interactor = PinSettingsInteractor()
        let router = PinSettingsRouter()
        
        vc.presenter = presenter
        vc.createPin = createPin
        vc.delegate = delegate
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToHomeVC(in navigationCtrl: Any) {
        appDelegate.router.navigate(to: .mainPage)
    }
    
    func navigateToLogin(in navigationCtrl: Any) {
        appDelegate.router.navigate(to: .loginInit)
    }

}

class PinSettingsDelegate: PinSettingsVCDelegate {
    func pinDidEnteredCorrectPin(viewController: PinSettingsVC) {
        appDelegate.router.navigate(to: .mainPage)
    }
    
    func pinDidEnteredIncorrectPinThreeTimes(viewController: PinSettingsVC) {
        UDManager.logout()
        appDelegate.router.navigate(to: .loginInit)
    }
    
    func pinDidConfirmedNewPin(viewController: PinSettingsVC) {
        appDelegate.router.navigate(to: .mainPage)
    }
    
    func pinDidAuthBiometrics(viewController: PinSettingsVC) {
        appDelegate.router.navigate(to: .mainPage)
    }
    
    func pinDidAuthErrorBiomterics(viewController: PinSettingsVC) {
        
    }
}
