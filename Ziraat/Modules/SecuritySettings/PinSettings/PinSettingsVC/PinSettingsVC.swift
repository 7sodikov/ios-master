//
//  PinSettingsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import LocalAuthentication

protocol PinSettingsVCDelegate: AnyObject {
    func pinDidEnteredCorrectPin(viewController:PinSettingsVC)
    func pinDidEnteredIncorrectPinThreeTimes(viewController:PinSettingsVC)
    func pinDidConfirmedNewPin(viewController:PinSettingsVC)
    func pinDidAuthBiometrics(viewController:PinSettingsVC)
    func pinDidAuthErrorBiomterics(viewController:PinSettingsVC)
}

protocol PinSettingsVCProtocol: BaseViewControllerProtocol {
    
}

class PinSettingsVC: BaseViewController, PinSettingsViewInstaller {
    var backView: UIImageView!
    var logoImageView: UIImageView!
    var enterPinLabel: UILabel!
    var pinDotOneImageView: UIImageView!
    var pinDotTwoImageView: UIImageView!
    var pinDotThreeImageView: UIImageView!
    var pinDotFourImageView: UIImageView!
    var pinDotsStackView: UIStackView!
    var buttonStackView: UIStackView!
    var firstRowStackView: UIStackView!
    var oneButton: UIButton!
    var twoButton: UIButton!
    var threeButton: UIButton!
    var secondRowStackView: UIStackView!
    var fourButton: UIButton!
    var fiveButton: UIButton!
    var sixButton: UIButton!
    var thirdRowStackView: UIStackView!
    var sevenButton: UIButton!
    var eightButton: UIButton!
    var nineButton: UIButton!
    var forthStackView: UIStackView!
    var noneButton: UIButton!
    var zeroButton: UIButton!
    var deleteButton: UIButton!
    
    var mainView: UIView { view }
    
    var presenter: PinSettingsPresenterProtocol!
    
    private var errorCount = 0
    private var pin = ""
    private var confirmPin = ""
    
    public var createPin = false
    public weak var delegate:PinSettingsVCDelegate?
    public var removeBackButton = false
    
    var userData:AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        for i  in 0..<self.pinDotsStackView.subviews.count {
            if let imageView = self.pinDotsStackView.subviews[i] as? UIImageView {
                imageView.image = UIImage(named: "imp_pin_selected")
            }
        }
        
        for stack in self.buttonStackView.subviews {
            if let stack = stack as? UIStackView {
                for button in stack.subviews {
                    if let button = button as? UIButton {
                        button.setTitleColor(.white, for: .normal)
                        button.setTitleColor(ColorConstants.highlightedGray, for: .selected)
                        button.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(45))
                        button.setBackgroundColor(.red, for: .selected)
                        button.addTarget(self, action: #selector(self.handlesPinumber), for: UIControl.Event.touchUpInside)
                    }
                }
            }
        }
        
        self.updateFlowers()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UDManager.bioAuthentication && !self.createPin {
            let myContext = LAContext()
            let myLocalizedReasonString = RS.lbl_biometric_bypass.localized()
            
            var authError: NSError?
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    DispatchQueue.main.async {
                        if success {
                            self.delegate?.pinDidAuthBiometrics(viewController: self)
                        } else {
                            self.delegate?.pinDidAuthErrorBiomterics(viewController: self)
                        }
                    }
                }
            } else {
                self.delegate?.pinDidAuthErrorBiomterics(viewController: self)
            }
        }
    }
    
    @objc func handlesPinumber(_ sender:UIButton) {

        if let numberText = sender.titleLabel?.text, let _ = Int(numberText) {
            if pin.count < 4 {
                pin += numberText
            }
        } else {
            if self.pin != "" {
                pin.removeLast()
            }
        }
        
        self.updateFlowers()
        
        if pin.count == 4 {
            if self.createPin {
                if self.confirmPin.count == 0 {
                    self.confirmPin = self.pin
                    self.enterPinLabel.text = RS.lbl_confirm_pin.localized()
                    self.pin = ""
                    self.updateFlowers()
                } else {
                    if self.confirmPin == self.pin {
                        KeychainManager.pin = self.pin
                        self.delegate?.pinDidConfirmedNewPin(viewController: self)
                    } else {
                        self.enterPinLabel.text = RS.lbl_invalid_pin.localized()
                        self.enterPinLabel.textColor = ColorConstants.mainRed
                        
                        CATransaction.begin()
                        let animation = CABasicAnimation(keyPath: "position")
                        animation.duration = 0.07
                        animation.repeatCount = 4
                        animation.autoreverses = true
                        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.pinDotsStackView.center.x - 10, y: self.pinDotsStackView.center.y))
                        animation.toValue = NSValue(cgPoint: CGPoint(x: self.pinDotsStackView.center.x + 10, y: self.pinDotsStackView.center.y))
                        CATransaction.setCompletionBlock {
                            self.enterPinLabel.text = RS.lbl_enter_pin.localized()
                            self.enterPinLabel.textColor = .white
                            self.pin = ""
                            self.confirmPin = ""
                            self.updateFlowers()
                        }
                        self.pinDotsStackView.layer.add(animation, forKey: "position")
                        CATransaction.commit()
                    }
                }
            } else {
                if self.pin == KeychainManager.pin {
                    self.delegate?.pinDidEnteredCorrectPin(viewController: self)
                } else {
                    self.errorCount += 1
                    
                    CATransaction.begin()
                    let animation = CABasicAnimation(keyPath: "position")
                    animation.duration = 0.07
                    animation.repeatCount = 4
                    animation.autoreverses = true
                    animation.fromValue = NSValue(cgPoint: CGPoint(x: self.pinDotsStackView.center.x - 10, y: self.pinDotsStackView.center.y))
                    animation.toValue = NSValue(cgPoint: CGPoint(x: self.pinDotsStackView.center.x + 10, y: self.pinDotsStackView.center.y))
                    CATransaction.setCompletionBlock {
                        if self.errorCount == 3 {
                            self.delegate?.pinDidEnteredIncorrectPinThreeTimes(viewController: self)
                        }
                        self.pin = ""
                        self.updateFlowers()
                    }
                    self.pinDotsStackView.layer.add(animation, forKey: "position")
                    CATransaction.commit()
                }
            }
        }
    }
    
    private func updateFlowers() {
        for i  in 0..<self.pinDotsStackView.subviews.count {
            if let imageView = self.pinDotsStackView.subviews[i] as? UIImageView {
                if i < pin.count {
                    imageView.image = UIImage(named: "imp_pin_selected")
                } else {
                    imageView.image = UIImage(named: "img_pin")
                }
            }
        }
    }
    
    @objc func forgotPasscodePressed(_ sender: UIButton) {
        presenter.forgotPasscodePressed(with: navigationController as Any)
    }
   
}

extension PinSettingsVC: PinSettingsVCProtocol {
    
}
