//
//  SecuritySettingsListRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol SecuritySettingsListRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToPin(in navigationCtrl: Any)
    func navigateToGraphicKey(_ view : Any)
    func navigateToMainPage()
}

class SecuritySettingsListRouter: SecuritySettingsListRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = SecuritySettingsListVC()
        let presenter = SecuritySettingsListPresenter()
        let interactor = SecuritySettingsListInteractor()
        let router = SecuritySettingsListRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPin(in navigationCtrl: Any) {
        let pinCtrl = PinSettingsRouter.createModule(createPin: true)
        
        (navigationCtrl as? UINavigationController)?.pushViewController(pinCtrl, animated: true)
    }
    
    func navigateToGraphicKey(_ view : Any) {
        if let controller = view as? UIViewController{
            let module = GraphicKeySettingsRouter.createModule(createMode: true)
            controller.navigationController?.pushViewController(module, animated: true)
        }
    }
    
    func navigateToMainPage() {
        appDelegate.router.navigate(to: .mainPage)
    }
}
