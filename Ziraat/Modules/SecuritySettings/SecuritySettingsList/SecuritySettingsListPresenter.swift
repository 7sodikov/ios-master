//
//  SecuritySettingsListPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SecuritySettingsListPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: SecuritySettingsListVM { get }
    func pinCodeButtonClicked(with navigationCtrl: Any)
    func graphicKeyButtonClicked(with navigationCtrl: Any)
    func skipButtonClicked()
}

protocol SecuritySettingsListInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didShield(with response: ResponseResult<SingleMessageResponse, AppError>)
}

class SecuritySettingsListPresenter: SecuritySettingsListPresenterProtocol {
    weak var view: SecuritySettingsListVCProtocol!
    var interactor: SecuritySettingsListInteractorProtocol!
    var router: SecuritySettingsListRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = SecuritySettingsListVM()
    
    // Private property and methods
    func pinCodeButtonClicked(with navigationCtrl: Any) {
        router.navigateToPin(in: navigationCtrl)
    }
    
    func graphicKeyButtonClicked(with navigationCtrl: Any) {
        router.navigateToGraphicKey(view as Any)
    }
    
    func skipButtonClicked() {
        view.preloader(show: true)
        interactor.shield()
    }
}

extension SecuritySettingsListPresenter: SecuritySettingsListInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didShield(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            router.navigateToMainPage()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
}
