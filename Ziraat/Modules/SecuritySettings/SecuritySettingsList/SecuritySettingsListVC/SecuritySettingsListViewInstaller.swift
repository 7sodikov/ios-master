//
//  SecuritySettingsListViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

protocol SecuritySettingsListViewInstaller: ViewInstaller {
    var bgImageView: UIImageView! { get set }
    var logoImageView: UIImageView! { get set }
    var pinCodeButton: UIButton! { get set }
    var graphicKeyButton: UIButton! { get set }
    var skipButton: UIButton! { get set }
    var activityIndicator: MDCActivityIndicator! { get set }
}

extension SecuritySettingsListViewInstaller {
    private func pinCodeButton(with title: String) -> UIButton {
        let btn = UIButton(frame: .zero)
        btn.layer.masksToBounds = true
        btn.setTitleColor(.white, for: .normal)
        btn.setTitleColor(UIColor.white.withAlphaComponent(0.75), for: .highlighted)
        btn.setBackgroundColor(UIColor.black.withAlphaComponent(0.4), for: .normal)
        btn.setBackgroundColor(UIColor.black.withAlphaComponent(0.3), for: .highlighted)
        btn.layer.cornerRadius = Size.PinCodeButton.height/2
        btn.setTitle(title, for: .normal)
        btn.titleLabel?.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(24))
        btn.contentHorizontalAlignment = .left
        btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: Size.PinCodeButton.height/2, bottom: 0, right:Size.PinCodeButton.height/2)
        return btn
    }
    
    func initSubviews() {
        bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "img_background")
        
        logoImageView = UIImageView()
        logoImageView.image = UIImage(named: "img_logo_with_text")
        
        pinCodeButton = pinCodeButton(with: RS.lbl_pin_code.localized())
        
        graphicKeyButton = pinCodeButton(with: RS.lbl_graphic_key.localized())
        
        skipButton = UIButton()
        skipButton.setTitle(RS.btn_skip.localized(), for: .normal)
        skipButton.titleLabel?.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(18))
        skipButton.setTitleColor(.white, for: .normal)
        skipButton.setTitleColor(ColorConstants.highlightedWhite, for: .highlighted)
        
        activityIndicator = MDCActivityIndicator()
        activityIndicator.cycleColors = [.white]
        activityIndicator.tintColor = .white
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(bgImageView)
        mainView.addSubview(logoImageView)
        mainView.addSubview(pinCodeButton)
        mainView.addSubview(graphicKeyButton)
        mainView.addSubview(skipButton)
        mainView.addSubview(activityIndicator)
    }
    
    func addSubviewsConstraints() {
        bgImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        logoImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.Logo.width)
            maker.height.equalTo(Size.Logo.height)
            maker.top.equalToSuperview().offset(Adaptive.val(112))
            maker.centerX.equalToSuperview()
        }
        
        pinCodeButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(logoImageView.snp.bottom).offset(Adaptive.val(55))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.height.equalTo(Size.PinCodeButton.height)
        }
        
        graphicKeyButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(pinCodeButton.snp.bottom).offset(Adaptive.val(16))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.height.equalTo(Size.PinCodeButton.height)
        }
        
        skipButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(graphicKeyButton.snp.bottom).offset(Adaptive.val(28))
            maker.centerX.equalToSuperview()
        }
        
        activityIndicator.snp.remakeConstraints { (maker) in
            maker.center.equalTo(skipButton)
            maker.width.height.equalTo(skipButton.snp.height).multipliedBy(1.2)
        }
    }
}

fileprivate struct Size {
    struct Logo {
        static let width = Adaptive.val(155)
        static let height = Adaptive.val(46)
    }
    
    struct PinCodeButton {
        static let height = Adaptive.val(54)
    }
    
    static let paddingLeftRight = Adaptive.val(12)
}
