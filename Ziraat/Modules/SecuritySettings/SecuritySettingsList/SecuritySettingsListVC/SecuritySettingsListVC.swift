//
//  SecuritySettingsListVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

protocol SecuritySettingsListVCProtocol: BaseViewControllerProtocol {
    
}

class SecuritySettingsListVC: BaseViewController, SecuritySettingsListViewInstaller {
    var bgImageView: UIImageView!
    var logoImageView: UIImageView!
    var pinCodeButton: UIButton!
    var graphicKeyButton: UIButton!
    var skipButton: UIButton!
    var activityIndicator: MDCActivityIndicator!
    var mainView: UIView { view }
    
    var presenter: SecuritySettingsListPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        
        pinCodeButton.addTarget(self, action: #selector(pinCodeButtonClicked(_:)), for: .touchUpInside)
        graphicKeyButton.addTarget(self, action: #selector(graphicKeyButtonClicked(_:)), for: .touchUpInside)
        skipButton.addTarget(self, action: #selector(skipButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        if show {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            pinCodeButton.isEnabled = false
            graphicKeyButton.isEnabled = false
            skipButton.isHidden = true
        } else {
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
            pinCodeButton.isEnabled = true
            graphicKeyButton.isEnabled = true
            skipButton.isHidden = false
        }
    }
    
    @objc func pinCodeButtonClicked(_ sender: UIButton) {
        presenter.pinCodeButtonClicked(with: navigationController as Any)
    }
    
    @objc func graphicKeyButtonClicked(_ sender: UIButton) {
        presenter.graphicKeyButtonClicked(with: navigationController as Any)
    }
    
    @objc func skipButtonClicked(_ sender: UIButton) {
        presenter.skipButtonClicked()
    }
}

extension SecuritySettingsListVC: SecuritySettingsListVCProtocol {
    
}
