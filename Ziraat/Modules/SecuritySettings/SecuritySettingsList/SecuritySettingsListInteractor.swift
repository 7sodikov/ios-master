//
//  SecuritySettingsListInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SecuritySettingsListInteractorProtocol: class {
    func shield()
}

class SecuritySettingsListInteractor: SecuritySettingsListInteractorProtocol {
    weak var presenter: SecuritySettingsListInteractorToPresenterProtocol!
    func shield() {
    
    }
}
