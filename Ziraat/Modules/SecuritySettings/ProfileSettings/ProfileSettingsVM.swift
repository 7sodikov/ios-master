//
//  ProfileSettingsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class ProfileSettingsVM {
    var userInfo: UserResponse?
    
    var successfulDate: String {
        let date = userInfo?.lastLoginDay
        let formattedDate = date?.replacingOccurrences(of: "-", with: "/")
        return formattedDate ?? ""
    }
    
    var failedDate: String {
        let date = userInfo?.lastLoginFailedDay
        let formattedDate = date?.replacingOccurrences(of: "-", with: "/")
        return formattedDate ?? ""
    }
    
    var successfulTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: userInfo?.lastLoginTime ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let hour = myCalendar.component(.hour, from: date!)
        let minute = myCalendar.component(.minute, from: date!)
        let time = String(format:"%02d:%02d", hour, minute)
        return String(time)
    }
    
    var failedTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: userInfo?.lastLoginFailedTime ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let hour = myCalendar.component(.hour, from: date!)
        let minute = myCalendar.component(.minute, from: date!)
        let time = String(format:"%02d:%02d", hour, minute)
        return String(time)
    }
}
