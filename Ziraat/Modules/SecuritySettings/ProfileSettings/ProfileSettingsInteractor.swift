//
//  ProfileSettingsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ProfileSettingsInteractorProtocol: class {
    func userInfo()
}
 
class ProfileSettingsInteractor: ProfileSettingsInteractorProtocol {
    weak var presenter: ProfileSettingsInteractorToPresenterProtocol!
    func userInfo() {
        NetworkService.User.getUserMeInfo() { (result) in
            self.presenter.didGetUserInfo(with: result)
        }
    }
}
