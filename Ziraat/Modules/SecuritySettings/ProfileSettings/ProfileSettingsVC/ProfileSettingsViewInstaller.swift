//
//  ProfileSettingsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ProfileSettingsViewInstaller: ViewInstaller {
    var backImageView: UIImageView! { get set }
    var profilePicImageView: UIImageView! { get set }
    var profileButton: UIButton! { get set }
    var fullNameLabel: UILabel! { get set }
    var lastSuccessfulLabel: UILabel! { get set }
    var lastSuccessfulDateLabel: UILabel! { get set }
    var lastSuccessfulTimeLabel: UILabel! { get set }
    var separatorLineView: UIView! { get set }
    var lastFailedLabel: UILabel! { get set }
    var lastFailedDateLabel: UILabel! { get set }
    var lastFailedTimeLabel: UILabel! { get set }
    var loginRecordsView: EZProfileSettingsSelection! { get set }
    var loginRecordsButton: UIButton! { get set }
    var changePinView: EZProfileSettingsSelection! { get set }
    var changePinButton: UIButton! { get set }
    var accessLimitationView: EZProfileSettingsSelection! { get set }
    var accessLimitationButton: UIButton! { get set }
    var transactionLimitView: EZProfileSettingsSelection! { get set }
    var transactionLimitButton: UIButton! { get set }
    var customerRepresentativeView: EZProfileSettingsSelection! { get set }
    var customerRepresentativeButton: UIButton! { get set }
    var actionsStackView: UIStackView! { get set }
    var languageSelectionLabel: UILabel! { get set }
    var languageButtonsView: EZLanguageSelectionControl! { get set }
    var languageStackView: UIStackView! { get set }
}

extension ProfileSettingsViewInstaller {
    func initSubviews() {
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_dash_light_background")
        
        profilePicImageView = UIImageView()
        profilePicImageView.image = UIImage(named: "img_profile_pic")
        
        profileButton = UIButton()
        
        fullNameLabel = UILabel()
        fullNameLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(20))
        fullNameLabel.textColor = ColorConstants.highlightedGray
        fullNameLabel.numberOfLines = 0
        
        lastSuccessfulLabel = UILabel()
        lastSuccessfulLabel.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(12))
        lastSuccessfulLabel.textColor = .black
        lastSuccessfulLabel.text = RS.lbl_last_successful_entry.localized()
        
        lastSuccessfulDateLabel = UILabel()
        lastSuccessfulDateLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        lastSuccessfulDateLabel.textColor = .black
        
        lastSuccessfulTimeLabel = UILabel()
        lastSuccessfulTimeLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        lastSuccessfulTimeLabel.textColor = .black
        
        separatorLineView = UIView()
        separatorLineView.backgroundColor = ColorConstants.lightestGray
        
        lastFailedLabel = UILabel()
        lastFailedLabel.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(12))
        lastFailedLabel.textColor = .black
        lastFailedLabel.text = RS.lbl_last_unsuccessful_entry.localized()
        
        lastFailedDateLabel = UILabel()
        lastFailedDateLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        lastFailedDateLabel.textColor = .black
        
        lastFailedTimeLabel = UILabel()
        lastFailedTimeLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        lastFailedTimeLabel.textColor = .black
        
        loginRecordsView = EZProfileSettingsSelection().setTitle(title: RS.lbl_login_records.localized())
        
        loginRecordsButton = UIButton()
        
        changePinView = EZProfileSettingsSelection().setTitle(title: RS.lbl_change_pass.localized())
        
        changePinButton = UIButton()
        
        accessLimitationView = EZProfileSettingsSelection().setTitle(title: RS.lbl_access_limit.localized())
        
        accessLimitationButton = UIButton()
        
        transactionLimitView = EZProfileSettingsSelection().setTitle(title: RS.lbl_transaction_limit.localized())
        
        transactionLimitButton = UIButton()
        
        customerRepresentativeView = EZProfileSettingsSelection().setTitle(title: RS.lbl_customer_representative.localized())
        
        customerRepresentativeButton = UIButton()
        
        actionsStackView = UIStackView()
        actionsStackView.axis = .vertical
        actionsStackView.distribution = .fillEqually
        actionsStackView.spacing = Adaptive.val(10)
        
        languageSelectionLabel = UILabel()
        languageSelectionLabel.text = RS.lbl_select_lang.localized()
        languageSelectionLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        languageSelectionLabel.textColor = .black
        
        languageButtonsView = EZLanguageSelectionControl()
        languageButtonsView.addSegment(with: "TR", language: .turkish)
        languageButtonsView.addSegment(with: "EN", language: .english)
        languageButtonsView.addSegment(with: "RU", language: .russian)
        languageButtonsView.addSegment(with: "UZ", language: .uzbek)
        languageButtonsView.selectedIndex = 0
        
        languageStackView = UIStackView()
        languageStackView.axis = .horizontal
        languageStackView.alignment = .center
    }
    
    func localizeText() {
        loginRecordsView.actionTitleLabel.text = RS.lbl_login_records.localized()
        changePinView.actionTitleLabel.text = RS.lbl_change_pass.localized()
        accessLimitationView.actionTitleLabel.text = RS.lbl_access_limit.localized()
        transactionLimitView.actionTitleLabel.text = RS.lbl_transaction_limit.localized()
        customerRepresentativeView.actionTitleLabel.text = RS.lbl_customer_representative.localized()
        
        lastSuccessfulLabel.text = RS.lbl_last_successful_entry.localized()
        lastFailedLabel.text = RS.lbl_last_unsuccessful_entry.localized()
        languageSelectionLabel.text = RS.lbl_select_lang.localized()
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(profilePicImageView)
        mainView.addSubview(profileButton)
        mainView.addSubview(fullNameLabel)
        
        mainView.addSubview(lastSuccessfulLabel)
        mainView.addSubview(lastSuccessfulDateLabel)
        mainView.addSubview(lastSuccessfulTimeLabel)
        
        mainView.addSubview(separatorLineView)
        
        mainView.addSubview(lastFailedLabel)
        mainView.addSubview(lastFailedDateLabel)
        mainView.addSubview(lastFailedTimeLabel)
        
        actionsStackView.addArrangedSubview(loginRecordsView)
        actionsStackView.addArrangedSubview(changePinView)
        actionsStackView.addArrangedSubview(accessLimitationView)
        actionsStackView.addArrangedSubview(transactionLimitView)
        actionsStackView.addArrangedSubview(customerRepresentativeView)
        mainView.addSubview(actionsStackView)
        
        mainView.addSubview(loginRecordsButton)
        mainView.addSubview(changePinButton)
        mainView.addSubview(accessLimitationButton)
        mainView.addSubview(transactionLimitButton)
        mainView.addSubview(customerRepresentativeButton)

        mainView.addSubview(languageSelectionLabel)
        mainView.addSubview(languageButtonsView)
        mainView.addSubview(languageStackView)
    }
    
    func addSubviewsConstraints() {
        backImageView.snp.remakeConstraints { (maker) in
            maker.left.right.bottom.top.equalToSuperview()
        }
        
        profilePicImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(60.5))
            maker.height.equalTo(Adaptive.val(61))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.top.equalToSuperview().offset(Adaptive.val(110))
        }
        
        profileButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(profilePicImageView)
        }
        
        fullNameLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(profilePicImageView)
            maker.leading.equalTo(profilePicImageView.snp.trailing).offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
        lastSuccessfulLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(profilePicImageView.snp.bottom).offset(Adaptive.val(31))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
        }
        
        lastSuccessfulTimeLabel.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(lastSuccessfulLabel.snp.bottom)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
        lastSuccessfulDateLabel.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(lastSuccessfulLabel.snp.bottom)
            maker.trailing.equalTo(lastSuccessfulTimeLabel.snp.leading).offset(-Size.paddingLeftRight)
        }
        
        separatorLineView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(lastSuccessfulLabel.snp.bottom).offset(Adaptive.val(18))
            maker.height.equalTo(Adaptive.val(1))
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
        }
        
        lastFailedLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(separatorLineView.snp.bottom).offset(Adaptive.val(18))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
        }
        
        lastFailedTimeLabel.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(lastFailedLabel.snp.bottom)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
        lastFailedDateLabel.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(lastFailedLabel.snp.bottom)
            maker.trailing.equalTo(lastFailedTimeLabel.snp.leading).offset(-Size.paddingLeftRight)
        }
        
        actionsStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(lastFailedLabel.snp.bottom).offset(Adaptive.val(27))
            maker.leading.equalToSuperview().offset(Adaptive.val(22))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(250))
        }
        
        languageSelectionLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(actionsStackView.snp.bottom).offset(Adaptive.val(13))
            maker.trailing.equalTo(languageButtonsView.snp.leading).offset(-Adaptive.val(18))
        }
        
        languageButtonsView.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(languageSelectionLabel)
            maker.trailing.equalToSuperview().offset(-Adaptive.val(50))
        }

        loginRecordsButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(loginRecordsView)
        }
        
        changePinButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(changePinView)
        }
        
        accessLimitationButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(accessLimitationView)
        }
        
        transactionLimitButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(transactionLimitView)
        }
        
        customerRepresentativeButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(customerRepresentativeView)
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(16)
}
