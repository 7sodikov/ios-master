//
//  ResetPasswordFromSettingsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResetPasswordFromSettingsInteractorProtocol: class {
    func changePassword(oldPassword: String, newPassword: String, repeatPassword: String)
}

class ResetPasswordFromSettingsInteractor: ResetPasswordFromSettingsInteractorProtocol {
    weak var presenter: ResetPasswordFromSettingsInteractorToPresenterProtocol!
    
    func changePassword(oldPassword: String, newPassword: String, repeatPassword: String) {
        NetworkService.Auth.changePassword(oldPassword: oldPassword, newPassword: newPassword, repeatPassword: repeatPassword) { (result) in
            self.presenter.didChangePassword(with: result)
        }
    }
}
