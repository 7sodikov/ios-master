//
//  ResetPasswordFromSettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ResetPasswordFromSettingsRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateLoginVC()
}

class ResetPasswordFromSettingsRouter: ResetPasswordFromSettingsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ResetPasswordFromSettingsVC()
        let presenter = ResetPasswordFromSettingsPresenter()
        let interactor = ResetPasswordFromSettingsInteractor()
        let router = ResetPasswordFromSettingsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateLoginVC() {
        appDelegate.router.navigate(to: .login)
    }
}
