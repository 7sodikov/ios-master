//
//  ResetPasswordFromSettingsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResetPasswordFromSettingsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ResetPasswordFromSettingsVM { get }
    func viewDidLoad()
    func changePasswordClicked(oldPassword: String, newPassword: String, repeatPassword: String)
    func successAlertDismissed()
}

protocol ResetPasswordFromSettingsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didChangePassword(with response: ResponseResult<SingleMessageResponse, AppError>)
}

class ResetPasswordFromSettingsPresenter: ResetPasswordFromSettingsPresenterProtocol {
    weak var view: ResetPasswordFromSettingsVCProtocol!
    var interactor: ResetPasswordFromSettingsInteractorProtocol!
    var router: ResetPasswordFromSettingsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ResetPasswordFromSettingsVM()
    
    // Private property and methods
    
    func viewDidLoad() {
        
    }
    
    func changePasswordClicked(oldPassword: String, newPassword: String, repeatPassword: String) {
        if newPassword != repeatPassword {
            view.showError(message: RS.al_msg_confirm_pass_doesnt_match.localized())
            return
        }
        
        if newPassword.count < 6 || newPassword.count > 20 {
            view.showError(message: RS.al_msg_pass_length_error.localized())
            return
        }
        
        guard let encryptedOldPass = RSAEncrypter.encrypt(text: oldPassword),
              let encryptedNewPass = RSAEncrypter.encrypt(text: newPassword),
              let encryptedRepeatPass = RSAEncrypter.encrypt(text: repeatPassword) else {
            view.showError(message: RS.al_msg_smth_went_wrong_error.localized())
            return
        }
        
        view.preloader(show: true)
        
        interactor.changePassword(oldPassword: encryptedOldPass,
                                  newPassword: encryptedNewPass,
                                  repeatPassword: encryptedRepeatPass)
    }
    
    func successAlertDismissed() {
        router.navigateLoginVC()
    }
}

extension ResetPasswordFromSettingsPresenter: ResetPasswordFromSettingsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didChangePassword(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            view.showToast(message: result.data.message ?? "")
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
