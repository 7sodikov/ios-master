//
//  ResetPasswordFromSettingsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//


import UIKit
import SnapKit
import EasyTipView

protocol ResetPasswordFromSettingsViewInstaller: ViewInstaller {
    var backgroundView: UIImageView! { get set }
    var currentPasswordLabel: UILabel! { get set }
    var currentPasswordTextField: LoginTextField! { get set }
    var newPasswordLabel: UILabel! { get set }
    var newPasswordTextField: LoginTextField! { get set }
    var newPasswordView: UIView! { get set }
    var infoButton: UIButton! { get set }
    var repeatPasswordLabel: UILabel! { get set }
    var repeatPasswordTextField: LoginTextField! { get set }
    var nextButton: NextButton! { get set }
    var tipView: EasyTipView! { get set }
}

extension ResetPasswordFromSettingsViewInstaller {
    func initSubviews() {
        backgroundView = UIImageView()
        backgroundView.image =  UIImage(named: "img_dash_light_background")
        
        currentPasswordLabel = UILabel()
        currentPasswordLabel.text = RS.lbl_current_password.localized()
        currentPasswordLabel.textColor = .black
        currentPasswordLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        currentPasswordTextField = LoginTextField(with: "- - -", cornerRadius: Size.TextField.height/2, isSecureTextEntry: true)
        
        newPasswordLabel = UILabel()
        newPasswordLabel.text = RS.lbl_new_password.localized()
        newPasswordLabel.textColor = .black
        newPasswordLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        
        newPasswordTextField = LoginTextField(with: "- - -", cornerRadius: Size.TextField.height/2, isSecureTextEntry: true)
        
        newPasswordView = UIView()
        newPasswordView.layer.cornerRadius = Size.TextField.height/2
        newPasswordView.backgroundColor = .white
        
        infoButton = UIButton()
        infoButton.setBackgroundImage(UIImage(named: "img_dark_info"), for: .normal)
        
        repeatPasswordLabel = UILabel()
        repeatPasswordLabel.text = RS.lbl_new_password_again.localized()
        repeatPasswordLabel.textColor = .black
        repeatPasswordLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        repeatPasswordTextField = LoginTextField(with: "- - -", cornerRadius: Size.TextField.height/2, isSecureTextEntry: true)
        
        nextButton = NextButton.systemButton(with: RS.lbl_change_password.localized(),
                                             cornerRadius: Size.TextField.height/2)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedGray, for: .selected)
        nextButton.isUserInteractionEnabled = false
        
        var tipPreferences = EasyTipView.Preferences()
        if let font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(13)) {
            tipPreferences.drawing.font = font
        }
        tipPreferences.drawing.foregroundColor = .white
        tipPreferences.drawing.backgroundColor = ColorConstants.highlightedGray
        tipPreferences.drawing.arrowPosition = EasyTipView.ArrowPosition.any
        tipPreferences.drawing.arrowHeight = Adaptive.val(10)
        tipPreferences.drawing.arrowWidth = Adaptive.val(20)
        tipPreferences.drawing.borderWidth = Adaptive.val(5)
        tipPreferences.drawing.borderColor = UIColor.white.withAlphaComponent(0.7)
        
        tipView = EasyTipView(text: RS.lbl_password_rule.localized(), preferences: tipPreferences)
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundView)
        mainView.addSubview(currentPasswordLabel)
        mainView.addSubview(currentPasswordTextField)
        mainView.addSubview(newPasswordLabel)
        
        mainView.addSubview(newPasswordView)
        newPasswordView.addSubview(newPasswordTextField)
        newPasswordView.addSubview(infoButton)
        
        mainView.addSubview(repeatPasswordLabel)
        mainView.addSubview(repeatPasswordTextField)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        backgroundView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        currentPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.left.equalTo(Size.leftRightPadding)
        }
        
        currentPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(currentPasswordLabel.snp.bottom).offset(Size.interLabelTextFieldSpace)
            maker.left.equalTo(Size.leftRightPadding)
            maker.right.equalTo(-Size.leftRightPadding)
            maker.height.equalTo(Size.TextField.height)
        }
        
        newPasswordView.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(currentPasswordTextField.snp.bottom).offset(Size.interItemSpace)
//            maker.left.equalTo(Size.leftRightPadding)
//            maker.right.equalTo(-Size.leftRightPadding)
//            maker.height.equalTo(Size.TextField.height)
            maker.top.equalTo(newPasswordLabel.snp.bottom).offset(Size.interLabelTextFieldSpace)
            maker.left.right.height.equalTo(currentPasswordTextField)
        }
        
        newPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(currentPasswordTextField.snp.bottom).offset(Size.interItemSpace)
            maker.left.equalTo(Size.leftRightPadding)
        }
        
        newPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.left.bottom.equalTo(newPasswordView)
            maker.trailing.equalTo(infoButton.snp.leading)
        }
        
        infoButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(24))
            maker.centerY.equalTo(newPasswordView)
            maker.trailing.equalTo(newPasswordView.snp.trailing).offset(-Adaptive.val(11))
        }
        
        repeatPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(newPasswordTextField.snp.bottom).offset(Size.interItemSpace)
            maker.left.equalTo(Size.leftRightPadding)
        }
        
        repeatPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(repeatPasswordLabel.snp.bottom).offset(Size.interLabelTextFieldSpace)
            maker.left.right.height.equalTo(currentPasswordTextField)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(repeatPasswordTextField).offset(Adaptive.val(100))
            maker.left.right.equalTo(currentPasswordTextField)
            
            maker.height.equalTo(Size.TextField.height)
        }
    }
}

fileprivate struct Size {
    struct TextField {
        static let height = Adaptive.val(50)
    }
    
    static let interItemSpace = Adaptive.val(20)
    static let interLabelTextFieldSpace = Adaptive.val(10)
    static let leftRightPadding = Adaptive.val(20)
}

