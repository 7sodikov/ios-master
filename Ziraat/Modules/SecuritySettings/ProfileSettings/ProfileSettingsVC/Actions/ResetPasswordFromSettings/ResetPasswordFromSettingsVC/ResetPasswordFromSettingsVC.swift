//
//  ResetPasswordFromSettingsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import Foundation
import EasyTipView

protocol ResetPasswordFromSettingsVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
    func showToast(message: String)
}

class ResetPasswordFromSettingsVC: BaseViewController, ResetPasswordFromSettingsViewInstaller {
    var mainView: UIView { view }
    var backgroundView: UIImageView!
    var logoImageView: UIImageView!
    var currentPasswordLabel: UILabel!
    var currentPasswordTextField: LoginTextField!
    var newPasswordLabel: UILabel!
    var newPasswordTextField: LoginTextField!
    var newPasswordView: UIView!
    var infoButton: UIButton!
    var repeatPasswordTextField: LoginTextField!
    var repeatPasswordLabel: UILabel!
    var nextButton: NextButton!
    var tipView: EasyTipView!
    
    var presenter: ResetPasswordFromSettingsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setNavigation()
        
        nextButton.addTarget(self, action: #selector(changePasswordClicked(_:)), for: .touchUpInside)
        infoButton.addTarget(self, action: #selector(openInfo(_:)), for: .touchUpInside)
        currentPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        newPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        repeatPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector (self.handleTapAnywhere))
        self.mainView.addGestureRecognizer(gesture)
        
        presenter.viewDidLoad()
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        if parent == nil {
            dismissTipView()
        }
    }
    
    @objc private func openInfo(_ sender: UIButton) {
        tipView.show(forView: infoButton)
    }
    
    func dismissTipView(){
        tipView.dismiss()
    }
    
    @objc private func handleTapAnywhere(_ sender: UIGestureRecognizer) {
        tipView.dismiss()
    }
    
    func setNavigation() {
        self.title = RS.lbl_change_password.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        nextButton.isUserInteractionEnabled = isFieldsValid()
    }
    
    private func isFieldsValid() -> Bool {
        return (currentPasswordTextField.text?.isEmpty != true &&
            newPasswordTextField.text?.isEmpty != true &&
            repeatPasswordTextField.text?.isEmpty != true)
    }
    
    @objc private func changePasswordClicked(_ sender: UIButton) {
        presenter.changePasswordClicked(oldPassword: currentPasswordTextField.text ?? "",
                                        newPassword: newPasswordTextField.text ?? "",
                                        repeatPassword: repeatPasswordTextField.text ?? "")
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: false)
    }
}

extension ResetPasswordFromSettingsVC: ResetPasswordFromSettingsVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func showToast(message: String) {
        let alert = AlertController(title: RS.lbl_success.localized(), message: message, preferredStyle: .alert)
        alert.showAutoDismissAlert(in: self, okClick: {
            self.presenter.successAlertDismissed()
        })
    }
}
