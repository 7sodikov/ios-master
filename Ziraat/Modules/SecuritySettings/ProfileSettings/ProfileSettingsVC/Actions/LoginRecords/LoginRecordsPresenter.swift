//
//  LoginRecordsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

enum StatusType: String {
    case successful = "SUCCESS"
    case failed = "FAILED"
}

protocol LoginRecordsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoginRecordsVM { get }
    func viewDidLoad()
    func loadDataForSuccessfullLogin()
    func loadDataForFailedLogin()
}

protocol LoginRecordsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetAuthHistory(with response: ResponseResult<AuthHistoriesResponse, AppError>)
}

class LoginRecordsPresenter: LoginRecordsPresenterProtocol {
    weak var view: LoginRecordsVCProtocol!
    var interactor: LoginRecordsInteractorProtocol!
    var router: LoginRecordsRouterProtocol!
    var limit: Int = 5
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoginRecordsVM()
    
    // Private property and methods
    
    func viewDidLoad() {
        loadDataForSuccessfullLogin()
        view.showReloadButtonForResponses(false, animateReloadButton: false)
    }
    
    func loadDataForSuccessfullLogin() {
        interactor.getAuthHistory(status: StatusType.successful.rawValue, limit: limit)
    }
    
    func loadDataForFailedLogin() {
        interactor.getAuthHistory(status: StatusType.failed.rawValue, limit: limit)
    }
}

extension LoginRecordsPresenter: LoginRecordsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetAuthHistory(with response: ResponseResult<AuthHistoriesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.loginRecords = result.data.history
            if result.data.history.count > 0 {
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(true, animateReloadButton: false)
            }
        case let .failure(error):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
        }
    }
}
