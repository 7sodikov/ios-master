//
//  LoginRecordsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoginRecordsInteractorProtocol: class {
    func getAuthHistory(status: String, limit: Int)
}

class LoginRecordsInteractor: LoginRecordsInteractorProtocol {
    weak var presenter: LoginRecordsInteractorToPresenterProtocol!
    
    func getAuthHistory(status: String, limit: Int) {
        NetworkService.User.authInfo(status: status, limit: limit) {
            (result) in self.presenter.didGetAuthHistory(with: result)
        }
    }
}
