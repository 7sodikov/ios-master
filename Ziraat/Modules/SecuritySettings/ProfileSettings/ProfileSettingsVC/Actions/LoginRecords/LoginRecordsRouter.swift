//
//  LoginRecordsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginRecordsRouterProtocol: class {
    static func createModule() -> UIViewController
}

class LoginRecordsRouter: LoginRecordsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = LoginRecordsVC()
        let presenter = LoginRecordsPresenter()
        let interactor = LoginRecordsInteractor()
        let router = LoginRecordsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
