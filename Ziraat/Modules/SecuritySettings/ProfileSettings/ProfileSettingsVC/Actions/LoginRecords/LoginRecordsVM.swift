//
//  LoginRecordsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class LoginRecordsVM {
    var loginRecords: [AuthHistoryResponse]?
    var recordSuccessfulItem: [LoginRecordVM] = []
    var recordFailedItem: [LoginRecordVM] = []
    
    func addSuccessful() {
        for recordElement in loginRecords ?? [] {
            recordSuccessfulItem.append(LoginRecordVM(record: recordElement))
        }
    }
    
    func addFailed() {
        for recordElement in loginRecords ?? [] {
            recordFailedItem.append(LoginRecordVM(record: recordElement))
        }
    }
}

class LoginRecordVM {
    var recordElement: AuthHistoryResponse
    
    var time: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: recordElement.time)
        let myCalendar = Calendar(identifier: .gregorian)
        let hour = myCalendar.component(.hour, from: date!)
        let minute = myCalendar.component(.minute, from: date!)
        let time = String(format:"%02d:%02d", hour, minute)
        return String(time)
    }
    
    var date: String {
        let date = recordElement.date
        let formattedDate = date.replacingOccurrences(of: "-", with: "/")
        return formattedDate
    }
    
    init(record: AuthHistoryResponse) {
        recordElement = record
    }
}
