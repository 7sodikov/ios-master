//
//  LoginRecordsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginRecordsVCProtocol: class {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class LoginRecordsVC: BaseViewController, LoginRecordsViewInstaller {
    var mainView: UIView { view }
    var backView: UIImageView!
    var segmentControl: EZSegmentedControlLoginRecords!
    var tableView: AutomaticHeightTableView!
    var reloadButtonView: ReloadButtonView!
    
    var isSuccessfulSelected = true
    
    var presenter: LoginRecordsPresenterProtocol!
    
    fileprivate let cellid = "\(LoginRecordsTableViewCell.self)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setNavigation()
        
        presenter.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        segmentControl.addTarget(self, action: #selector(self.segmentedControlChanged(_:)), for: .valueChanged)
    }
    
    func setNavigation() {
        self.title = RS.lbl_login_records.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func segmentedControlChanged(_ sender: EZSegmentedControlLoginRecords) {
        if sender.selectedIndex == 0 {
            isSuccessfulSelected = true
            presenter.viewModel.loginRecords?.removeAll()
            presenter.loadDataForSuccessfullLogin()
        } else if sender.selectedIndex == 1 {
            isSuccessfulSelected = false
            presenter.viewModel.loginRecords?.removeAll()
            presenter.loadDataForFailedLogin()
        }
        tableView.reloadData()
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LoginRecordsVC: LoginRecordsVCProtocol, UITableViewDataSource, UITableViewDelegate {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.loginRecords?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: LoginRecordsHeaderView.self)) as? LoginRecordsHeaderView
        headerView?.backgroundColor = .clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! LoginRecordsTableViewCell
        cell.backgroundColor = .clear
        if isSuccessfulSelected == true {
            presenter.viewModel.addSuccessful()
            let loginRecordElement = presenter.viewModel.recordSuccessfulItem[indexPath.row]
            return cell.setup(history: loginRecordElement)
        } else {
            presenter.viewModel.addFailed()
            let loginRecordElement = presenter.viewModel.recordFailedItem[indexPath.row]
            return cell.setup(history: loginRecordElement)
        }
    }
}
