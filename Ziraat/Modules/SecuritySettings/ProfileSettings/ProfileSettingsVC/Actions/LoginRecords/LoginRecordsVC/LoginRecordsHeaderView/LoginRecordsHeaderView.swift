//
//  LoginRecordsHeaderView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class LoginRecordsHeaderView: UITableViewHeaderFooterView, LoginRecordsHeaderViewInstaller {
    var backView: UIView!
    var dateLabel: UILabel!
    var timeLabel: UILabel!
    var ipLabel: UILabel!
    var stackView: UIStackView!
    
    var mainView: UIView { self }

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    private func initialize() {
        setupSubviews()
    }
}
