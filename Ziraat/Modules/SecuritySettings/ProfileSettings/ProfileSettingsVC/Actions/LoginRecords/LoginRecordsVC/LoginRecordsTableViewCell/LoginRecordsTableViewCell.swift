//
//  LoginRecordsTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class LoginRecordsTableViewCell: UITableViewCell, LoginRecordsTableViewInstaller {
    var mainView: UIView { contentView }
    var backView: UIView!
    var dateLabel: UILabel!
    var timeLabel: UILabel!
    var ipLabel: UILabel!
    var stackView: UIStackView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(history: LoginRecordVM) -> LoginRecordsTableViewCell {
        dateLabel.text = history.date
        timeLabel.text = history.time
        ipLabel.text = history.recordElement.ip
        return self
    }
}
