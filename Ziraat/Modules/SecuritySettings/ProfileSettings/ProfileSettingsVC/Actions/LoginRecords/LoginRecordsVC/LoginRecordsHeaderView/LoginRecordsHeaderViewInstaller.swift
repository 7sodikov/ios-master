//
//  LoginRecordsHeaderViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginRecordsHeaderViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var dateLabel: UILabel! { get set }
    var timeLabel : UILabel! { get set }
    var ipLabel: UILabel! { get set }
    var stackView: UIStackView! { get set }
}

extension LoginRecordsHeaderViewInstaller {
    func initSubviews() {
        backView = UIView()
        backView.backgroundColor = .white
        
        dateLabel = UILabel()
        dateLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(12))
        dateLabel.text = RS.lbl_date.localized()
        dateLabel.textColor = .black
        
        timeLabel = UILabel()
        timeLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(12))
        timeLabel.text = RS.lbl_time.localized()
        timeLabel.textColor = .black
        
        ipLabel = UILabel()
        ipLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(12))
        ipLabel.text = "IP"
        ipLabel.textColor = .black
        
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        stackView.addArrangedSubview(dateLabel)
        stackView.addArrangedSubview(timeLabel)
        stackView.addArrangedSubview(ipLabel)
        mainView.addSubview(stackView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.top.bottom.equalToSuperview()
            maker.leading.equalTo(0)
            maker.trailing.equalTo(0)
        }
        
        stackView.snp.remakeConstraints { (maker) in
            maker.leading.top.equalTo(backView).offset(Adaptive.val(22))
            maker.trailing.bottom.equalTo(backView).offset(-Adaptive.val(22))
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(22)
}
