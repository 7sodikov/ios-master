//
//  LoginRecordsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginRecordsViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var segmentControl: EZSegmentedControlLoginRecords! { get set }
    var tableView: AutomaticHeightTableView! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
}

extension LoginRecordsViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
        segmentControl = EZSegmentedControlLoginRecords()
        segmentControl.addSegment(with: RS.lbl_last_successful_entry.localized())
        segmentControl.addSegment(with: RS.lbl_last_unsuccessful_entry.localized())
        segmentControl.selectedIndex = 0
        
        tableView = AutomaticHeightTableView()
        tableView.register(LoginRecordsTableViewCell.self, forCellReuseIdentifier: String(describing: LoginRecordsTableViewCell.self))
        tableView.register(LoginRecordsHeaderView.self, forHeaderFooterViewReuseIdentifier: String(describing: LoginRecordsHeaderView.self))
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .white
        tableView.separatorColor = .clear
        tableView.layer.cornerRadius = Adaptive.val(10)
        tableView.sizeToFit()
        
        reloadButtonView = ReloadButtonView(frame: .zero)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(segmentControl)
        mainView.addSubview(tableView)
        mainView.addSubview(reloadButtonView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        segmentControl.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(segmentControl.snp.bottom).offset(Adaptive.val(30))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(22)
}
