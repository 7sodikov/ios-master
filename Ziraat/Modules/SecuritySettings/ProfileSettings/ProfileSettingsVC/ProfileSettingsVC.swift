//
//  ProfileSettingsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import FirebaseMessaging

protocol ProfileSettingsVCProtocol: class {
    func setupData()
}

class ProfileSettingsVC: BaseViewController, ProfileSettingsViewInstaller, EZLanguageSelectionControlDelegate {
    var mainView: UIView { view }
    var backImageView: UIImageView!
    var profilePicImageView: UIImageView!
    var profileButton: UIButton!
    var fullNameLabel: UILabel!
    var lastSuccessfulLabel: UILabel!
    var lastSuccessfulDateLabel: UILabel!
    var lastSuccessfulTimeLabel: UILabel!
    var separatorLineView: UIView!
    var lastFailedLabel: UILabel!
    var lastFailedDateLabel: UILabel!
    var lastFailedTimeLabel: UILabel!
    var loginRecordsView: EZProfileSettingsSelection!
    var loginRecordsButton: UIButton!
    var changePinView: EZProfileSettingsSelection!
    var changePinButton: UIButton!
    var accessLimitationView: EZProfileSettingsSelection!
    var accessLimitationButton: UIButton!
    var transactionLimitView: EZProfileSettingsSelection!
    var transactionLimitButton: UIButton!
    var customerRepresentativeView: EZProfileSettingsSelection!
    var customerRepresentativeButton: UIButton!
    var actionsStackView: UIStackView!
    var languageSelectionLabel: UILabel!
    var languageButtonsView: EZLanguageSelectionControl!
    var languageStackView: UIStackView!

    var presenter: ProfileSettingsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setNavigation()
        presenter.viewDidLoad()
                
        profileButton.addTarget(self, action: #selector(unavailableErrorClicked), for: .touchUpInside)
        loginRecordsButton.addTarget(self, action: #selector(loginRecordsClicked), for: .touchUpInside)
        changePinButton.addTarget(self, action: #selector(resetPasswordClicked), for: .touchUpInside)
        accessLimitationButton.addTarget(self, action: #selector(unavailableErrorClicked), for: .touchUpInside)
        transactionLimitButton.addTarget(self, action: #selector(unavailableErrorClicked), for: .touchUpInside)
        customerRepresentativeButton.addTarget(self, action: #selector(unavailableErrorClicked), for: .touchUpInside)
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
        
    @objc private func loginRecordsClicked() {
        presenter.loginRecord()
    }
    
    @objc private func resetPasswordClicked() {
        presenter.resetPassword()
    }
    
    @objc private func unavailableErrorClicked() {
        presenter.unavailableErrorMessage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if LocalizationManager.language == Language.turkish {
            languageButtonsView.selectedIndex = 0
        } else if LocalizationManager.language == Language.english {
            languageButtonsView.selectedIndex = 1
        } else if LocalizationManager.language == Language.russian {
            languageButtonsView.selectedIndex = 2
        } else {
            languageButtonsView.selectedIndex = 3
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        languageButtonsView.delegate = self
        localizeText()
        (self.navigationController as? NavigationController)?.background(isWhite: false)
    }
    
    func setNavigation() {
        self.title = RS.nav_ttl_profile_settings.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    func languageSelected(at language: Language) {
        LocalizationManager.language = language
        appDelegate.window?.rootViewController = DashboardRouter.createModule()
        appDelegate.window?.makeKeyAndVisible()
        if language == Language.english {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_EN")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_RU")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_UZ")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_TR")
        } else if language == Language.russian {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_RU")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_EN")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_UZ")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_TR")
        } else if language == Language.uzbek {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_UZ")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_EN")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_RU")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_TR")
        } else {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_TR")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_EN")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_RU")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_UZ")
        }
    }
}

extension ProfileSettingsVC: ProfileSettingsVCProtocol {
    func setupData() {
        fullNameLabel.text = presenter.viewModel.userInfo?.fullName
        lastSuccessfulDateLabel.text = presenter.viewModel.successfulDate
        lastSuccessfulTimeLabel.text = presenter.viewModel.successfulTime
        lastFailedDateLabel.text = presenter.viewModel.failedDate
        lastFailedTimeLabel.text = presenter.viewModel.failedTime
    }
}
