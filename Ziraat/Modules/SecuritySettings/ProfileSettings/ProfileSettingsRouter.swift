//
//  ProfileSettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ProfileSettingsRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToLoginRecords(view: Any)
    func navigateToResetPassword(view: Any)
    func unavailableError(view: Any)
}

class ProfileSettingsRouter: ProfileSettingsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ProfileSettingsVC()
        let presenter = ProfileSettingsPresenter()
        let interactor = ProfileSettingsInteractor()
        let router = ProfileSettingsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToLoginRecords(view: Any) {
        let viewCtrl = view as? UIViewController
        let loginRecords = LoginRecordsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(loginRecords, animated: true)
    }
    
    func navigateToResetPassword(view: Any) {
        let viewCtrl = view as? UIViewController
        let resetPassword = ResetPasswordRouter.createModule(isRedStyle: false)
//        let resetPassword = ResetPasswordFromSettingsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(resetPassword, animated: true)
    }
    
    func unavailableError(view: Any) {
        let viewCtrl = view as! UIViewController
        let unavailableVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        unavailableVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(unavailableVC, animated: false, completion: nil)
    }
}
