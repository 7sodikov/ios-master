//
//  ProfileSettingsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ProfileSettingsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ProfileSettingsVM { get }
    func loginRecord()
    func resetPassword()
    func unavailableErrorMessage()
    func viewDidLoad()
}

protocol ProfileSettingsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetUserInfo(with response: ResponseResult<UserResponse, AppError>)
}

class ProfileSettingsPresenter: ProfileSettingsPresenterProtocol {
    weak var view: ProfileSettingsVCProtocol!
    var interactor: ProfileSettingsInteractorProtocol!
    var router: ProfileSettingsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ProfileSettingsVM()
    
    // Private property and methods
    
    func viewDidLoad() {
        interactor.userInfo()
    }
    
    func loginRecord() {
        router.navigateToLoginRecords(view: view as Any)
    }
    
    func resetPassword() {
        router.navigateToResetPassword(view: view as Any)
    }
    
    func unavailableErrorMessage() {
        router.unavailableError(view: view as Any)
    }
}

extension ProfileSettingsPresenter: ProfileSettingsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetUserInfo(with response: ResponseResult<UserResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.userInfo = result.data
            view.setupData()
        case let .failure(error):
            break
        }
    }
}
