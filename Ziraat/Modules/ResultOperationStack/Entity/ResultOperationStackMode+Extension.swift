//
//  ResultOperationStackMode+Extension.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResultOperationStackBase {
    typealias Page = ResultOperationStackViewController.Page
    typealias Button = ResultOperationStackViewController.Button
}

extension ResultOperationStackViewController {
    
    enum Page: Int {
        case info = 2
        case confirmation
        case result
        
        func operationStackOrder(of mode: ResultOperationStackMode) -> OperationStackOrderView.StackOrder {
            switch mode {
            case .convertion:
                switch self {
                    
                case .info, .confirmation:
                    return .two
                    
                case .result:
                    return .three
                }
            }
        }
        
        var operationStackOrder: OperationStackOrderView.StackOrder {
            OperationStackOrderView.StackOrder(rawValue: self.rawValue)!
        }
        
        var next: Page {
            if case .result = self {
                return self
            }
            return Page(rawValue: self.rawValue + 1) ?? .result
        }
        
        var back: Page {
            self == .info ? self : Page(rawValue: self.rawValue - 1) ?? .info
        }
        
    }
    
    enum Button: Int {
        case confirm
        case cancel
        case addToFavourite
        case print
        case close
        case backToMain
        
        var title: String {
            switch self {
            
            case .confirm:
                return RS.btn_confirm.localized().uppercased()
            case .cancel:
                return RS.lbl_cancel.localized().uppercased()
            case .addToFavourite:
                return RS.btn_add_to_favourites.localized()
            case .print:
                return RS.btn_print.localized()
            case .close:
                return RS.btn_close.localized()
            case .backToMain:
                return RS.btn_return_to_homepage.localized()
            }
        }
    }
    
}
