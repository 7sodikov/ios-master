//
//  ResultOperationStackMode.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

enum ResultOperationStackMode {
    case convertion(details: [DetailsCellViewModel], token: String)
    
    
    var token: String {
        switch self {
        case .convertion(_, let token):
            return token
        }
    }
    
    var detailsViewModels: [DetailsCellViewModel] {
        switch self {
        case .convertion(let details, _):
            return details
        }
    }
    
    var numberOfOperations: OperationStackOrderView.NumberOfOperation {
        switch self {
        case .convertion:
            return .three
        }
    }
    
    var pages: [ResultOperationStackViewController.Page] {
        switch self {
        case .convertion:
            return [.info, .result]
        }
    }
}
