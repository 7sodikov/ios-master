//
//  ResultOperationStackInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
protocol ResultOperationStackBusinessLogic: ResultOperationStackBase {
    var currentMode: ResultOperationStackMode {get}
    var viewModels: [[TableViewModel]] {get}
    var currentPage: Page {get}
    var needToConfirm: Bool {get}
    var singleMessageResponse: SingleMessageResponse? {get}
    
    func set(smsCode: String)
    func set(page: Page)
    func sendToken(completion: @escaping (Bool)->())
    func operationSmsConfirm(completion: @escaping (Bool)->())
    
    func addFavourets(operationId: Int, title: String, completion: @escaping (Bool)->())
}

class ResultOperationStackInteractor: ResultOperationStackBusinessLogic {
    
    var currentMode: ResultOperationStackMode
    var status: ResultStatusCellViewModel.Status = .failure(message: "unknown error! o_o ")
    var currentPage: Page = .info
    var token: String = ""
    var smsCode: String = ""
    var message: String = ""
    var needToConfirm: Bool = true
    
    var singleMessageResponse: SingleMessageResponse?
    
    init(mode: ResultOperationStackMode) {
        self.currentMode = mode
        self.token = mode.token
    }
    
    func set(page: Page) {
        self.currentPage = page
    }
    
    func set(smsCode: String) {
        self.smsCode = smsCode
    }
    
}

// MARK: - ViewModels
extension ResultOperationStackInteractor {
    var viewModels: [[TableViewModel]] {
        switch currentPage {
        
        case .info:
            return infoViewModels
        case .confirmation:
            return confirmationViewModel.map { [$0] }
        case .result:
            return resultViewModels
        }
    }
    
    private var infoViewModels: [[TableViewModel]] {
        let textViewModel = SingleLabelCellViewModel(text: RS.lbl_transaction_preview.localized())
        return [[textViewModel] + currentMode.detailsViewModels] + [buttonsViewModel]
    }
    
    private var confirmationViewModel: [TableViewModel] {
        
        [ConfirmationCellViewModel(message: message)] + buttonsViewModel
    }
    
    private var resultViewModels: [[TableViewModel]] {
        
        let vm1 = ResultStatusCellViewModel(status)
        let vm2 = ButtonsCellViewModel(leftButtonStyle: .red(title: Button.addToFavourite.title),
                                       rightButtonStyle: .red(title: Button.print.title),
                                       tag: currentPage.rawValue)
        var btn: ResultOperationStackViewController.Button = .close
        if case .failure = self.status {
            btn = .backToMain
        }
        
        let vm3 = ButtonCellViewModel(style: .red(title: btn.title), isEnabled: true, tag: btn.rawValue)
        
        return [[vm1], [vm2, vm3]]
    }
    
    private var buttonsViewModel: [ButtonCellViewModel] {
        let confirm = Button.confirm
        let cancel = Button.cancel
        return [.init(style: .red(title: confirm.title), isEnabled: true, tag: confirm.rawValue),
                .init(style: .bordered(title: cancel.title), isEnabled: true, tag: cancel.rawValue)]
    }
    
}

// MARK: - Networking
extension ResultOperationStackInteractor {
    
    
    func sendToken(completion: @escaping (Bool)->()) {
        func configure(statusCode: Int) {
            needToConfirm = statusCode == 200
            if !needToConfirm {
                status = .success(message: RS.lbl_payment_success.localized())
            }
        }
        NetworkService.Operation.operationTransfer(token: token) { (response) in
            switch response {
            
            case .success(let result):
                configure(statusCode: result.httpStatusCode)
                self.singleMessageResponse = result.data
                completion(true)
            case .failure(let error):
                self.status = .failure(message: error.localizedDescription)
                completion(false)
            }
        }
    }
    
    
    func operationSmsConfirm(completion: @escaping (Bool)->()) {
        
        NetworkService.Operation.operationConfirm(token: token, code: smsCode) { (response) in
            switch response {
            
            case .success:
                self.status = .success(message: RS.lbl_payment_success.localized())
                completion(true)
            case .failure(let error):
                self.status = .failure(message: error.localizedDescription)
                completion(false)
            }
        }
    }
    
    func addFavourets(operationId: Int, title: String, completion: @escaping (Bool)->()) {
        
        NetworkService.Favorite.favoriteAdd(operationId: operationId, title: title) { (response) in
            switch response {
            case .success( _):
                completion(true)
            case .failure(_):
                completion(false)
            }
        }
        
    }
    
}
