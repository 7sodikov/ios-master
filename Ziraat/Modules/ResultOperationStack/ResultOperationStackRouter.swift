//
//  ResultOperationStackRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResultOperationStackRoutingLogic {
    func back()
    func navigatetoPdfChequeVC(operationId: String)
    func backToRoot()
}

class ResultOperationStackRouter: BaseRouter, ResultOperationStackRoutingLogic {
    
    
    init(_ mode: ResultOperationStackMode) {
        let controller = ResultOperationStackViewController()
        super.init(viewController: controller)
        
        let interactor = ResultOperationStackInteractor(mode: mode)
        let presenter = ResultOperationStackPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }

    func navigatetoPdfChequeVC(operationId: String) {
        let pdfView = PdfViewController(operation: operationId, title: viewController.title ?? "")
        viewController.navigationController?.pushViewController(pdfView, animated: true)
//        let statementVC = PdfChequeRouter.createModule(operation: operationId, title: nil, isHidden: true)
//        statementVC.modalPresentationStyle = .overFullScreen
//        viewController.present(statementVC, animated: false, completion: nil)
    }
    
    func backToRoot() {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
}
