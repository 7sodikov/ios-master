//
//  ResultOperationStackViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol ResultOperationStackViewInstaller: ViewInstaller {
    var operationStackOrderView: OperationStackOrderView! {get set}
    var backgroundImage: UIImageView! {get set}
    var tableView: UITableView! {get set}
    
}

extension ResultOperationStackViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        tableView = .init()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.tableFooterView = .init()
        tableView.allowsSelection = false
        
        if let currentMode = parameter as? ResultOperationStackMode {
            operationStackOrderView = .init(numberOfOperation: currentMode.numberOfOperations)
        } else {
            operationStackOrderView = .init()
        }
        operationStackOrderView.setup(current: .two)
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
//        mainView.addSubview(operationStackOrderView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
      
        tableView.constraint { (make) in
            make.top.horizontal.bottom.equalToSuperView()
        }
    
    }
    
    func headerView(text: String) -> UIView {
        
        let view = UIView(frame: .init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 48))
        
        let label = UILabel()
        label.font = .gothamNarrow(size: 15, .book)
        label.numberOfLines = 0
        label.textColor = UIColor(89,96,103)
        label.lineBreakMode = .byWordWrapping
        label.text = text
        
        view.addSubview(label)
        label.constraint { (make) in
            make.horizontal(16).top.equalToSuperView()
        }
        
        return view
        
    }
    
}
