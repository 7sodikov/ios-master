//
//  ResultOperationStackPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit


protocol ResultOperationStackPresentationLogic: PresentationProtocol, TablePresentationLogic, ResultOperationStackBase {
    var currentMode: ResultOperationStackMode {get}
    var currentPage: Page {get}
    
    func willSet(smsCode: String)
    func buttonClicked(isNext: Bool, page: Page)
}


class ResultOperationStackPresenter: ResultOperationStackPresentationLogic {
    
    private unowned let view: ResultOperationStackDisplayLogic
    private let interactor: ResultOperationStackBusinessLogic
    private let router: ResultOperationStackRoutingLogic
    
    private var controller: ResultOperationStackViewController { view as! ResultOperationStackViewController }
    var currentMode: ResultOperationStackMode { interactor.currentMode}
    var currentPage: Page { interactor.currentPage }
    
    init(view: ResultOperationStackDisplayLogic, interactor: ResultOperationStackBusinessLogic, router: ResultOperationStackRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        view.setupTargets()
    }
    
    func viewWillAppear(_ animated: Bool) {
        
    }
    
    func willSet(smsCode: String) {
        interactor.set(smsCode: smsCode)
    }
    
    func buttonClicked(isNext: Bool, page: Page) {
        switch page {
            
        case .info, .confirmation:
            isNext ? sendToken() : confirmate()
        case .result:
            isNext ? showPdfCheque() : addToFavauretes()
        }
    }
    
    private func addToFavauretes() {
        func request(operationId: Int, title: String) {
            interactor.addFavourets(operationId: operationId, title: title) { (success) in
                if success {
                    self.router.backToRoot()
                }
            }
        }
        
        guard let operationId = interactor.singleMessageResponse?.operationId else{
            return
        }
        controller.showInputDialog(title: RS.lbl_success.localized(),
                                   subtitle: RS.lbl_fav_added.localized(),
                                   actionTitle: RS.btn_yes.localized(),
                                   cancelTitle: RS.lbl_cancel.localized(),
                                   inputPlaceholder: RS.lbl_title.localized(),
                                   cancelHandler: nil) { (input) in
            
            request(operationId: operationId, title: input ?? " ")
        }
    }
    
    // TODO: need to send to server name and params for saving
    private func showPdfCheque() {
        guard let receipt = interactor.singleMessageResponse?.receipt else{
            return
        }
        router.navigatetoPdfChequeVC(operationId: receipt)
    }
    
    func confirmate() {
        let controller = NotifyViewController(.confirmationConversion) {
            self.router.backToRoot()
        }
        self.controller.present(controller, animated: false)
    }
    
    private func sendToken() {
        controller.spinner(.start)
        interactor.sendToken { (success) in
            self.controller.spinner(.stop)
            if success {
                self.reloadTableView(at: .result)
            }
        }
    }

    
    func reloadTableView(at page: Page) {
        if .info == interactor.currentPage && page == .info { router.back(); return }
        interactor.set(page: page)
        view.operationStackOrderView.setup(current: page.operationStackOrder(of: interactor.currentMode))
        view.tableView.reloadData()
    }
}


// MARK: -TablePresentationLogic

extension ResultOperationStackPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}


extension UIViewController {
    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
}
