//
//  cViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation


protocol ResultOperationStackDisplayLogic: ResultOperationStackViewInstaller {
    func setupTargets()
}

class ResultOperationStackViewController: BaseViewController, ResultOperationStackDisplayLogic {
    
    var operationStackOrderView: OperationStackOrderView!
    var backgroundImage: UIImageView!
    var tableView: UITableView!
    var mainView: UIView { self.view }
    var parameter: Any? { presenter.currentMode }
    var presenter: ResultOperationStackPresentationLogic!
    
    override func loadView() {
        super.loadView()
        
        navigationController?.transparentBackgorund()
        title = RS.lbl_conversion.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func setupTargets() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(DetailCenteredCell.self)
        tableView.register(TableViewCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(ButtonsCell.self)
        tableView.register(ResultStatusCell.self)
        tableView.register(ConfirmationCell.self)
        tableView.register(DetailsCell.self)
        tableView.register(SingleLabelCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppear(animated)
    }
    
}

extension ResultOperationStackViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.tag = indexPath.row
        cell.set(viewModel: viewModel)
        (cell as? ButtonCell)?.buttonDelegate = self
        (cell as? ConfirmationCell)?.confirmationDelegate = self
        (cell as? ButtonsCell)?.buttonDelegate = self
        (cell as? DetailsCell)?.editDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch presenter.currentPage {
        
        case .info:
            return CGFloat(section * 16)
        case .confirmation:
            return CGFloat(section * 64)
        case .result:
            return CGFloat(64)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath) is DetailsCellViewModel {
            return presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath).estimatedRowHeight
        }
        return UITableView.automaticDimension
    }
}

extension ResultOperationStackViewController: ButtonsCellDelegate  {
    
    func buttonCellsButtonClicked(at tag: Int, isLeft: Bool) {
        if let page = Page(rawValue: tag) {
            presenter.buttonClicked(isNext: !isLeft, page: page)
        }
    }
    
}

extension ResultOperationStackViewController: ButtonCellDelegate {
    
    func buttonClicked(tag: Int) {
        if presenter.currentPage == .result {
            navigationController?.popToRootViewController(animated: true)
        } else if let button = Button(rawValue: tag) {
            presenter.buttonClicked(isNext: button == .confirm, page: .info)
        }
    }
    
}

extension ResultOperationStackViewController: ConfirmationCellDelegate {
    
    func textFiled(text: String) {
        presenter.willSet(smsCode: text)
    }
    
}

extension ResultOperationStackViewController: DetailsCellDelegate {
    
    func editButtonClicked(tag: Int) {
        navigationController?.popViewController(animated: true)
    }
    
}
