//
//  OtpConfirmRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 10/02/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OtpConfirmRouterProtocol: class {
    static func createModule() -> UIViewController
}

class OtpConfirmRouter: OtpConfirmRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = OtpConfirmVC()
        let presenter = OtpConfirmPresenter()
        let interactor = OtpConfirmInteractor()
        let router = OtpConfirmRouter()
        
        //vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
