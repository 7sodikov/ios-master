//
//  OtpConfirmVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 10/02/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OtpConfirmVCProtocol: class {
    
}

class OtpConfirmVC: BaseViewController, PaymentSMSConfirmationViewInstaller {
    var formCoverView: UIView!
    
    var coverStackView: UIStackView!
    
    var titleStackView: UIStackView!
    
    var formStackView: UIStackView!
    
    var timerStackView: UIStackView!
    
    var buttonsVerticalStackView: UIStackView!
    
    var buttonsStackView: UIStackView!
    
    var titleLabel: UILabel!
    
    var smsCodeTextField: SMSConfirmationTextField!
    
    var timerView: TimerView!
    
    var backButton: NextButton!
    
    var nextButton: NextButton!
    
    var mainView: UIView { view }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
    }
}

extension OtpConfirmVC: OtpConfirmVCProtocol {
    
}
