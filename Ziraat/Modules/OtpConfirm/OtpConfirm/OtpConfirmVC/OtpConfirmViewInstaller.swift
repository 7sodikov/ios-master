//
//  OtpConfirmViewInstaller.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 10/02/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OtpConfirmViewInstaller: ViewInstaller {
    // declare your UI elements here
}

extension OtpConfirmViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
    }
}

fileprivate struct Size {
    
}
