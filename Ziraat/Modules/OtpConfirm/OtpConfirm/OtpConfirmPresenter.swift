//
//  OtpConfirmPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 10/02/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OtpConfirmPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: OtpConfirmVM { get }
}

protocol OtpConfirmInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class OtpConfirmPresenter: OtpConfirmPresenterProtocol {
    weak var view: OtpConfirmVCProtocol!
    var interactor: OtpConfirmInteractorProtocol!
    var router: OtpConfirmRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OtpConfirmVM()
    
    // Private property and methods
    
}

extension OtpConfirmPresenter: OtpConfirmInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
