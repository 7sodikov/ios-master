//
//  OtpConfirmInteractor.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 10/02/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OtpConfirmInteractorProtocol: AnyObject {
    
}

class OtpConfirmInteractor: OtpConfirmInteractorProtocol {
    weak var presenter: OtpConfirmInteractorToPresenterProtocol!
    
}
