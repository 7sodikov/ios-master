//
//  DashboardPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/8/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardPresenterProtocol: AnyObject {
    var viewModel: DashboardVM { get }
    func menuButtonClicked()
    func logoutButtonClicked(in navigationCtrl: Any)
    func notificationButtonClicked()
    func unavailableButtonClicked()
    func pageController(didMoveToIndex index: Int, real: Bool)
    var navTitle: String { get set }
}

protocol DashboardInteractorToPresenterProtocol: AnyObject {
}

class DashboardPresenter: DashboardPresenterProtocol {
    unowned var view: DashboardVCProtocol!
    var interactor: DashboardInteractorProtocol!
    var router: DashboardRouterProtocol!
    
    private(set) var viewModel = DashboardVM()
    
    func menuButtonClicked() {
        router.navigateToMenuVC(in: view as Any) {
            self.view.reloadFirstPage()
        }
    }
    
    func logoutButtonClicked(in navigationCtrl: Any) {
        if viewModel.userEntry.currentSelectedPageIndex == 0 {
            router.openLogoutVc(in: navigationCtrl, self)
        } else if viewModel.userEntry.currentSelectedPageIndex == 3 || viewModel.userEntry.currentSelectedPageIndex == 5 {
            pageController(didMoveToIndex: 0, real: true)
            view.navigateToSpecificPage(index: 0) { (vc) in
                vc?.setNeedsStatusBarAppearanceUpdate()
                self.view.reloadCollectionView()
            }
        } else {
            pageController(didMoveToIndex: 0, real: true)
            view.navigateToSpecificPage(index: 0) { (vc) in
                vc?.setNeedsStatusBarAppearanceUpdate()
                self.view.reloadFirstPage()
            }
        }
    }
    
    func notificationButtonClicked() {
        router.navigateToNotificationsList(in: view as Any)
    }
    
    func unavailableButtonClicked() {
        router.unavailableError(in: view as Any)
    }
    
    func pageController(didMoveToIndex index: Int, real: Bool) {
        viewModel.userEntry.currentSelectedPageIndex = index
        view.setupUI(for: index, real: real)
    }
    
    var navTitle: String {
        get {
            return ""
        }
        set {
            
        }
    }
}

extension DashboardPresenter: DashboardInteractorToPresenterProtocol {
    
}

extension DashboardPresenter : DashboardCardsActionProtocol{
    func openCardDetails(_ card: DashboardCardVM) {
        self.router.openCardDetails(view as Any, card)
    }
}

extension DashboardPresenter : PopubMessageProtocol{
    func actionResult(_ action: Bool) {
        if action {
            interactor.logout()
            //            UDManager.clear()
            //            LocalizationManager.language = .english
            KeychainManager.logout()
            appDelegate.router.navigate(to: .logout)
            //            router.navigateToLogout(in: view)
            PushNotificationsManager.unsubscribeFromTopic()
        }
    }
}
