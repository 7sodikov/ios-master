//
//  DashboardFirstPageVCInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardFirstPageVCInstaller: ViewInstaller {
    // declare your UI elements here
    var assetsAndLiabilitiesStackView: UIStackView! { get set }
    var assetsButton: UIButton! { get set }
    var liabilitiesButton: UIButton! { get set }
    var mainSeparatorLineView: UIView! { get set }
    var mainAssetsScrollView: UIScrollView! { get set }
    var circleView: LambdaCircleView! { get set }
    var currencyStackView: UIStackView! { get set }
    var turkLiraView: DashboardComponent! { get set }
    var usdOneView: DashboardComponent! { get set }
    var usdTwoView: DashboardComponent! { get set }
    var eyeView: DashboardComponent! { get set }
    var listTableView: UITableView! { get set }
    
}

extension DashboardFirstPageVCInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        assetsAndLiabilitiesStackView = UIStackView()
        assetsAndLiabilitiesStackView.axis = .horizontal
        assetsAndLiabilitiesStackView.alignment = .fill
        assetsAndLiabilitiesStackView.distribution = .fillEqually
        assetsAndLiabilitiesStackView.spacing = 9
        
        assetsButton = UIButton()
        assetsButton.layer.cornerRadius = Adaptive.val(10)
        assetsButton.clipsToBounds = true
        assetsButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        assetsButton.setBackgroundColor(.white, for: .selected)
        assetsButton.setTitle("Assets", for: .normal)
        assetsButton.setTitleColor(.black, for: .selected)
        assetsButton.titleLabel?.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        
        liabilitiesButton = UIButton()
        liabilitiesButton.layer.cornerRadius = Adaptive.val(10)
        liabilitiesButton.clipsToBounds = true
        liabilitiesButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        liabilitiesButton.setBackgroundColor(ColorConstants.grayishWhite.withAlphaComponent(0.33), for: .normal)
        liabilitiesButton.setTitle("Liabilities", for: .normal)
        liabilitiesButton.setTitleColor(.white, for: .normal)
        liabilitiesButton.titleLabel?.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        
        mainSeparatorLineView = UIView()
        mainSeparatorLineView.backgroundColor = .white
        
        mainAssetsScrollView = UIScrollView()
        mainAssetsScrollView.backgroundColor = .clear
        
        circleView = LambdaCircleView()
        circleView.backgroundColor = .yellow
        
        currencyStackView = UIStackView()
        currencyStackView.axis = .vertical
        currencyStackView.spacing = Adaptive.val(16)
        
        turkLiraView = DashboardComponent()
        turkLiraView.backgroundColor = .white
        turkLiraView.layer.cornerRadius = Adaptive.val(17)
        turkLiraView.componentImage.isHidden = true
        turkLiraView.componentLabel.textColor = .black
        turkLiraView.componentLabel.text = "TL"
        
        usdOneView = DashboardComponent()
        usdOneView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        usdOneView.layer.cornerRadius = Adaptive.val(17)
        usdOneView.componentImage.isHidden = true
        usdOneView.componentLabel.textColor = .white
        usdOneView.componentLabel.text = "USD"
        
        usdTwoView = DashboardComponent()
        usdTwoView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        usdTwoView.layer.cornerRadius = Adaptive.val(17)
        usdTwoView.componentImage.isHidden = true
        usdTwoView.componentLabel.textColor = .white
        usdTwoView.componentLabel.text = "EUR"
        
        eyeView = DashboardComponent()
        
        listTableView = UITableView()
        listTableView.backgroundColor = .clear
        listTableView.separatorColor = ColorConstants.grayishWhite
        listTableView.separatorInset = .init(top: 0, left: Adaptive.val(16), bottom: 0, right: Adaptive.val(16))
        
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
        mainView.addSubview(assetsAndLiabilitiesStackView)
        assetsAndLiabilitiesStackView.addArrangedSubview(assetsButton)
        assetsAndLiabilitiesStackView.addArrangedSubview(liabilitiesButton)
        mainView.addSubview(assetsAndLiabilitiesStackView)
        mainView.addSubview(mainSeparatorLineView)
        mainView.addSubview(mainAssetsScrollView)
        mainView.addSubview(circleView)
        mainView.addSubview(currencyStackView)
        currencyStackView.addArrangedSubview(turkLiraView)
        currencyStackView.addArrangedSubview(usdOneView)
        currencyStackView.addArrangedSubview(usdTwoView)
        mainView.addSubview(eyeView)
        mainView.addSubview(listTableView)
        
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        assetsAndLiabilitiesStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(Adaptive.val(16))
            maker.trailing.equalTo(-Adaptive.val(16))
            maker.top.equalTo(0) 
        }
        
        assetsButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(41))
            maker.leading.top.equalTo(assetsAndLiabilitiesStackView)
            maker.bottom.equalTo(assetsAndLiabilitiesStackView)

        }

        liabilitiesButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(41))
            maker.trailing.bottom.equalTo(assetsAndLiabilitiesStackView)
            maker.top.equalTo(assetsAndLiabilitiesStackView)

        }
        
        mainSeparatorLineView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(1)
            maker.top.equalTo(assetsAndLiabilitiesStackView.snp.bottom)
            maker.leading.trailing.equalToSuperview()
        }
        
        mainAssetsScrollView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(mainSeparatorLineView.snp.bottom)
            maker.leading.bottom.trailing.equalTo(0)
        }
        
        currencyStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(mainAssetsScrollView.snp.top).offset(Adaptive.val(85))
            maker.leading.equalTo(mainAssetsScrollView.snp.leading).offset(Adaptive.val(16))
        }
        
        turkLiraView.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalTo(currencyStackView)
            maker.width.equalTo(Adaptive.val(52))
            maker.height.equalTo(Adaptive.val(34))
        }
        
        usdOneView.snp.remakeConstraints { (maker) in
            maker.leading.trailing.equalTo(currencyStackView)
            maker.width.equalTo(Adaptive.val(52))
            maker.height.equalTo(Adaptive.val(34))
        }
        
        usdTwoView.snp.remakeConstraints { (maker) in
            maker.bottom.leading.trailing.equalTo(currencyStackView)
            maker.width.equalTo(Adaptive.val(52))
            maker.height.equalTo(Adaptive.val(34))
        }
        
        circleView.snp.remakeConstraints { (maker) in
            maker.height.width.equalTo(Adaptive.val(219))
            maker.centerX.equalTo(mainAssetsScrollView)
            maker.top.equalTo(mainAssetsScrollView.snp.top).offset(Adaptive.val(85))
        }
        
        listTableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(circleView.snp.bottom).offset(Adaptive.val(50))
            maker.leading.equalToSuperview()
            maker.bottom.equalToSuperview()
            maker.trailing.equalToSuperview()
        }
        
    }
}


