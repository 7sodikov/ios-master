//
//  DashboardRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/8/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardRouterProtocol: AnyObject {
    static func createModule() -> UIViewController
    func openCardDetails(_ view:Any, _ card: DashboardCardVM)
    func navigateToMenuVC(in navigationCtrl: Any, completion: @escaping ()->Void)
    func openLogoutVc(in navCtrl: Any, _ delegate:PopubMessageProtocol?)
    func openMakeCallVC(in navigationCtrl: Any)
    func unavailableError(in navCtrl: Any)
    func navigateToNotificationsList(in navCtrl: Any)
    func navigateToLogout(in navCtrl: Any)
}

class DashboardRouter: DashboardRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = DashboardVC()
        let navCtrl = NavigationController(rootViewController: vc)
        let presenter = DashboardPresenter()
        let interactor = DashboardInteractor()
        let router = DashboardRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return navCtrl
    }
    
    func navigateToNotificationsList(in navCtrl: Any) {
        let viewCtrl = navCtrl as! UIViewController
        let notifVC = NotificationsListRouter.createModule()
        viewCtrl.navigationController?.pushViewController(notifVC, animated: true)
//        let viewCtrl = navCtrl as! UIViewController
//        let statementVC = NotificationsListRouter.createModule()
//        statementVC.modalPresentationStyle = .overFullScreen
//        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func navigateToMenuVC(in navigationCtrl: Any, completion: @escaping ()->Void) {
        let viewCtrl = navigationCtrl as! UIViewController
        let statementVC = MenuRouter.createModuleInNavController()
        statementVC.modalPresentationStyle = .fullScreen
        viewCtrl.present(statementVC, animated: true)
    }

    func openCardDetails(_ view:Any, _ card: DashboardCardVM){
        let module = CardDetailsRouter.createModule(card)
        if let controller = view as? UIViewController{
            controller.navigationController?.pushViewController(module, animated: true)
        }
    }
    func openLogoutVc(in navCtrl: Any, _ delegate: PopubMessageProtocol?) {
        let viewCtrl = navCtrl as! UIViewController
        let statementVC = PopupMessageRouter.createModule(RS.al_msg_exit.localized(), delegate)
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func openMakeCallVC(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as! UIViewController
        let statementVC = CallRouter.createModule()
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func unavailableError(in navCtrl: Any) {
        let viewCtrl = navCtrl as! UIViewController
        let statementVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func navigateToLogout(in navCtrl: Any) {
        let viewCtrl = navCtrl as! UIViewController
        let statementVC = LogoutRouter.createModule()
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
}
