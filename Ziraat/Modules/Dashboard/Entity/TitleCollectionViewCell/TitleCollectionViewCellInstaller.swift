//
//  TitleCollectionViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol TitleCollectionViewCellInstaller: ViewInstaller {
    var backImageview: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
}

extension TitleCollectionViewCellInstaller {
    func initSubviews() {
        backImageview = UIImageView()
        
        titleLabel = UILabel()
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageview)
        mainView.addSubview(titleLabel)
    }
    
    func addSubviewsConstraints() {
        backImageview.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
//            maker.top.equalToSuperview().offset(Adaptive.val(63))
            maker.center.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
