//
//  TitleCollectionViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

class TitleCollectionViewCell: UICollectionViewCell, TitleCollectionViewCellInstaller {
    var mainView: UIView { contentView }
    var backImageview: UIImageView!
    var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
