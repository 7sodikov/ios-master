//
//  BGImageCollectionCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/3/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class BGImageCollectionCell: UICollectionViewCell, BGImageCollectionCellViewInstaller {
    var mainView: UIView { contentView }
    var backImageview: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
