//
//  BGImageCollectionCellViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/3/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BGImageCollectionCellViewInstaller: ViewInstaller {
    var backImageview: UIImageView! { get set }
}

extension BGImageCollectionCellViewInstaller {
    func initSubviews() {
        backImageview = UIImageView()
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageview)
    }
    
    func addSubviewsConstraints() {
        backImageview.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
    }
}

fileprivate struct Size {
    
}
