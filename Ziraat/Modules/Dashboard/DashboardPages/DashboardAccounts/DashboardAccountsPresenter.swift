//
//  DashboardAccountsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardAccountsPresenterProtocol: AnyObject {
    var viewModel: DashboardAccountsVM { get }
    func viewDidLoad()
    func rightButtonClicked(for cellType: AccountTableCellType, currentCardPage: Int)
    func allAccountsButtonClicked()
    func moneyTransferButtonClicked()
    func accountStatementsButtonClicked(account: AccountResponse)
    func viewWillAppear(animated: Bool)
}

protocol DashboardAccountsInteractorToPresenterProtocol: AnyObject {
    func didGetAllAccounts(with response: ResponseResult<AccountsListResponse, AppError>)
    func didGetAllAccountBalances(with response: ResponseResult<AccountsBalanceResponse, AppError>)
}

class DashboardAccountsPresenter: DashboardAccountsPresenterProtocol {
    weak var view: DashboardAccountsVCProtocol!
    var interactor: DashboardAccountsInteractorProtocol!
    var router: DashboardAccountsRouter!
    
    private(set) var viewModel = DashboardAccountsVM()
    
    func viewDidLoad() {
        view.showReloadButtonForCardsDownload(true, animateReloadButton: true, isAccountsEmpty: false)
    }
    
    func viewWillAppear(animated: Bool) {
        interactor.accountsList()
        interactor.accountsBalance()
    }
    
    func rightButtonClicked(for cellType: AccountTableCellType, currentCardPage: Int) {
        if cellType == .iban {
            guard let cellValue = viewModel.tableCellValue(for: currentCardPage, cellType: cellType) else {
                return
            }
            let text = "\(cellType.title): \(cellValue)"
            view.showShareSheet(for: text)
        }
    }
    
    func allAccountsButtonClicked() {
        router.navigateToMyAccountsListVC(in: view as Any)
    }
    
    func moneyTransferButtonClicked() {
        router.navigateToMoneyTransferVC(in: view as Any)
    }
    
    func accountStatementsButtonClicked(account: AccountResponse) {
        router.navigateToAccountStatementVC(in: view as Any, account: account, currency: "")
    }
    
    // Private property and methods
//    private func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool) {
//        var show = show
//        if viewModel.accounts.count == 0 {
//            show = true
//        }
//        view.showReloadButtonForCardsDownload(show, animateReloadButton: animateReloadButton)
//    }
}

extension DashboardAccountsPresenter: DashboardAccountsInteractorToPresenterProtocol {
    func didGetAllAccounts(with response: ResponseResult<AccountsListResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.accounts = result.data.accounts
            view.showReloadButtonForCardsDownload(false,
                                                  animateReloadButton: false,
                                                  isAccountsEmpty: (viewModel.accounts.count == 0))
        case let .failure(error):
            view.showReloadButtonForCardsDownload(true,
                                                  animateReloadButton: false,
                                                  isAccountsEmpty: false)
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didGetAllAccountBalances(with response: ResponseResult<AccountsBalanceResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.accountBalance = result.data.accounts
            view.reloadViewForBalanceDownload()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
