//
//  DashboardAccountCollectionCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class DashboardAccountCollectionCell: UICollectionViewCell, DashboardAccountCollectionCellViewInstaller {
    var mainView: UIView { contentView }
    var accountImageView: UIImageView!
    var nameLabel: UILabel!
    var numberLabel: UILabel!
    var stackView: UIStackView!
    var labelsStackView: UIStackView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with accountVM: DashboardAccountVM) -> DashboardAccountCollectionCell {
//        accountImageView.kf.setImage(with: URL(string: cardVM.cardBGImageURL), placeholder: nil)
        nameLabel.text = accountVM.account.customerName
        numberLabel.text = accountVM.account.number
        return self
    }
}
