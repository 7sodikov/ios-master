//
//  DashboardAccountCollectionCellViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardAccountCollectionCellViewInstaller: ViewInstaller {
    var accountImageView: UIImageView! { get set }
    var nameLabel: UILabel! { get set }
    var numberLabel: UILabel! { get set }
    
    var stackView: UIStackView! { get set }
    var labelsStackView: UIStackView! { get set }
}

extension DashboardAccountCollectionCellViewInstaller {
    func initSubviews() {
        mainView.layer.masksToBounds = true
        
        accountImageView = UIImageView()
        accountImageView.image = UIImage(named: "icon_account")
        
        let font = UIFont.gothamNarrow(size: 16, .bold) //EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        nameLabel = UILabel()
        nameLabel.textColor = .dashDarkBlue
        nameLabel.font = font
        nameLabel.numberOfLines = 0
        
        numberLabel = UILabel()
        numberLabel.textColor = ColorConstants.borderLine
        numberLabel.font = font
        numberLabel.numberOfLines = 0
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = Adaptive.val(20)
        
        labelsStackView = UIStackView()
        labelsStackView.axis = .vertical
        labelsStackView.distribution = .fill
        labelsStackView.alignment = .center
        labelsStackView.spacing = Adaptive.val(1)
    }
    
    func embedSubviews() {
        labelsStackView.addArrangedSubview(nameLabel)
        labelsStackView.addArrangedSubview(numberLabel)
        stackView.addArrangedSubview(accountImageView)
        stackView.addArrangedSubview(labelsStackView)
        mainView.addSubview(stackView)
    }
    
    func addSubviewsConstraints() {
        accountImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(115))
            maker.height.equalTo(Adaptive.val(140))
        }
        
        stackView.snp.remakeConstraints { (maker) in
            maker.top.left.right.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
