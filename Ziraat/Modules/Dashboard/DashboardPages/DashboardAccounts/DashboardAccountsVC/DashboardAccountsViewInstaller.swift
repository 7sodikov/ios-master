//
//  DashboardAccountsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardAccountsViewInstaller: ViewInstaller {
    var reloadButtonView: ReloadButtonView! { get set }
    var containerView: UIView! { get set }
    var collectionLayout: UPCarouselFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
    var previousButton: UIButton! { get set }
    var nextButton: UIButton! { get set }
    var redInfoIconCoverView: UIView! { get set }
    var redInfoImageView: UIImageView! { get set }
    var redInfoImageTitleLabel: UILabel! { get set }
    var allMyAccountsButton: UIButton! { get set }
    var eyeButton: UIButton! { get set }
    var tableView: AutomaticHeightTableView! { get set }
    var buttonsStackView: UIStackView! { get set }
    var moneyTransferButton: NextButton! { get set }
    var accountActivitiesButton: NextButton! { get set }
}

extension DashboardAccountsViewInstaller {
    func initSubviews() {
        reloadButtonView = ReloadButtonView(frame: .zero)
        reloadButtonView.isHidden = true
        
        containerView = UIView()
        containerView.backgroundColor = .clear
        
        collectionLayout = UPCarouselFlowLayout()
        collectionLayout.scrollDirection = .horizontal
        collectionLayout.itemSize = DashboardAccountsViewSize.cellSize
        let space = (UIScreen.main.bounds.width - DashboardAccountsViewSize.cellSize.width)/2 + Adaptive.val(3)
        collectionLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: space)
        collectionLayout.sideItemScale = 1
        collectionLayout.sideItemAlpha = 1
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.register(DashboardAccountCollectionCell.self,
                                forCellWithReuseIdentifier: String(describing: DashboardAccountCollectionCell.self))
        collectionView.collectionViewLayout = collectionLayout
        
        previousButton = UIButton()
        previousButton.setImage(UIImage(named: "btn_prev"), for: .normal)
        previousButton.isHidden = true
        
        nextButton = UIButton()
        nextButton.setImage(UIImage(named: "btn_next"), for: .normal)
        nextButton.isHidden = true
        
        redInfoIconCoverView = UIView()
        redInfoIconCoverView.backgroundColor = .clear
        redInfoIconCoverView.isHidden = true
        
        redInfoImageView = UIImageView()
        redInfoImageView.image = UIImage(named: "icon_info")
        
        redInfoImageTitleLabel = UILabel()
        redInfoImageTitleLabel.font = .gothamNarrow(size: 18, .bold)//EZFontType.regular.sfProDisplay(size: Adaptive.val(18))
        redInfoImageTitleLabel.text = RS.lbl_no_account.localized()
        redInfoImageTitleLabel.textColor = .black
        redInfoImageTitleLabel.textAlignment = .center
        
        allMyAccountsButton = UIButton()
        allMyAccountsButton.layer.cornerRadius = Adaptive.val(16)
        allMyAccountsButton.backgroundColor = .white
        allMyAccountsButton.setTitle(RS.btn_all_accounts.localized(), for: .normal)
        allMyAccountsButton.titleLabel?.font = .gothamNarrow(size: 16, .bold)//EZFontType.regular.sfProDisplay(size: Adaptive.val(15))
        allMyAccountsButton.setTitleColor(.dashDarkBlue, for: .normal)
        allMyAccountsButton.setTitleColor(ColorConstants.borderLineHighlighted, for: .highlighted)
        allMyAccountsButton.setImage(UIImage(named: "img_icon_arr"), for: .normal)
        allMyAccountsButton.semanticContentAttribute = .forceRightToLeft
        allMyAccountsButton.imageView?.contentMode = .scaleAspectFit
        allMyAccountsButton.imageEdgeInsets = UIEdgeInsets(top: DashboardAccountsViewSize.AllMyAccountsButton.size.height * 0.35,
                                                           left: DashboardAccountsViewSize.AllMyAccountsButton.size.width * 0.15,
                                                           bottom: DashboardAccountsViewSize.AllMyAccountsButton.size.height * 0.35,
                                                           right: 0)
        
        eyeButton = UIButton()
        eyeButton.setBackgroundImage(UIImage(named: "btn_eye_new"), for: .normal)
        eyeButton.setBackgroundImage(UIImage(named: "btn_eye_crossed_new"), for: .selected)
        
        tableView = AutomaticHeightTableView()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0,
                                                left: DashboardAccountsViewSize.TableView.separatorInset,
                                                bottom: 0,
                                                right: DashboardAccountsViewSize.TableView.separatorInset)
        tableView.backgroundColor = .clear
//        tableView.separatorColor = ColorConstants.grayishWhite
        
        tableView.register(DashboardAccountsTableViewCell.self, forCellReuseIdentifier: String(describing: DashboardAccountsTableViewCell.self))
//        tableView.frame.size = tableView.contentSize
        
        buttonsStackView = UIStackView()
        buttonsStackView.spacing = Adaptive.val(17)
        buttonsStackView.alignment = .fill
        buttonsStackView.distribution = .fillEqually
        
        moneyTransferButton = NextButton.systemButton(with: RS.btn_money_transfers.localized(), cornerRadius: Adaptive.val(23))
        moneyTransferButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        moneyTransferButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        moneyTransferButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)//EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        accountActivitiesButton = NextButton.systemButton(with: RS.btn_actions_with_account.localized(), cornerRadius: Adaptive.val(23))
        accountActivitiesButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        accountActivitiesButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        accountActivitiesButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)//EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
    }
    
    func embedSubviews() {
        mainView.addSubview(reloadButtonView)
        mainView.addSubview(containerView)
        containerView.addSubview(collectionView)
        containerView.addSubview(previousButton)
        containerView.addSubview(nextButton)
        
        containerView.addSubview(redInfoIconCoverView)
        redInfoIconCoverView.addSubview(redInfoImageView)
        redInfoIconCoverView.addSubview(redInfoImageTitleLabel)
        
        containerView.addSubview(allMyAccountsButton)
        containerView.addSubview(eyeButton)
        containerView.addSubview(tableView)
        containerView.addSubview(buttonsStackView)
        
        buttonsStackView.addArrangedSubview(moneyTransferButton)
        buttonsStackView.addArrangedSubview(accountActivitiesButton)
    }
    
    func addSubviewsConstraints() {
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
        
        containerView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(63))
            maker.leading.trailing.bottom.equalToSuperview()
        }
        
        collectionView.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalToSuperview()
            maker.height.equalTo(DashboardAccountsViewSize.cellSize.height)
        }
        
        previousButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView)
            maker.leading.equalToSuperview()
            maker.width.equalTo(DashboardAccountsViewSize.NextCardButton.width)
            maker.height.equalTo(collectionView)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView)
            maker.trailing.equalToSuperview()
            maker.width.height.equalTo(previousButton)
        }
        
        redInfoIconCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView)
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(collectionView)
        }
        
        redInfoImageView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(ApplicationSize.paddingLeftRight)
            maker.centerX.equalToSuperview()
            maker.width.equalTo(Adaptive.val(90))
            maker.height.equalTo(Adaptive.val(90))
        }
        
        redInfoImageTitleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(redInfoImageView.snp.bottom).offset(Adaptive.val(10))
            maker.left.right.equalTo(redInfoIconCoverView)
        }
        
        allMyAccountsButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(DashboardAccountsViewSize.AllMyAccountsButton.size.width)
            maker.height.equalTo(DashboardAccountsViewSize.AllMyAccountsButton.size.height)
            maker.top.equalTo(collectionView.snp.bottom).offset(Adaptive.val(20))
            maker.centerX.equalToSuperview()
        }
        
        eyeButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(24))
            maker.top.equalTo(allMyAccountsButton.snp.bottom).offset(Adaptive.val(15))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(30))
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(eyeButton.snp.bottom).offset(Adaptive.val(10))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(14))
            maker.leading.equalToSuperview().offset(Adaptive.val(14))
            maker.bottom.equalTo(buttonsStackView.snp.top).offset(-Adaptive.val(20))
        }
        
        buttonsStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(Adaptive.val(16))
            maker.trailing.equalTo(-Adaptive.val(16))
            maker.bottom.equalTo(-Adaptive.val(20))
        }
        
        moneyTransferButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(47))
        }
        
        accountActivitiesButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(moneyTransferButton)
        }
    }
}

struct DashboardAccountsViewSize {
    static let leftRightPadding = Adaptive.val(20)
    static let cellSize = Adaptive.size(322, 215)
    struct NextCardButton {
        static let width = Adaptive.val(50)
    }
    
    struct AllMyAccountsButton {
        static let size = Adaptive.size(155, 35)
    }
    
    struct TableView {
        static let separatorInset = Adaptive.val(15)
    }
}
