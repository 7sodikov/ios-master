//
//  DashboardAccountsTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardAccountsTableViewCellDelegate: class {
    func rightButtonClicked(for cellType: AccountTableCellType)
}

class DashboardAccountsTableViewCell: UITableViewCell, DashboardAccountsTableViewCellInstaller {
    var mainView: UIView { contentView }
    var actionsLabel: UILabel!
    var balanceLabel: UILabel!
    var rightButton: UIButton!
    var stackView: UIStackView!
    var balanceAndRightBtnHorizontalStackView: UIStackView!
    var balanceAndRightBtnVerticalStackView: UIStackView!
    
    weak var delegate: DashboardAccountsTableViewCellDelegate?
    
    private var cellType: AccountTableCellType!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: AccountTableCellType, value: String?) -> DashboardAccountsTableViewCell {
        self.cellType = cellType
        actionsLabel.text = cellType.title
        balanceLabel.text = value
        rightButton.isHidden = (cellType != .iban)
        rightButton.addTarget(self, action: #selector(rightButtonClicked(_:)), for: .touchUpInside)
        return self
    }
    
    @objc private func rightButtonClicked(_ sender: UIButton) {
        delegate?.rightButtonClicked(for: cellType)
    }
}
