//
//  DashboardAccountsTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardAccountsTableViewCellInstaller: ViewInstaller {
    var actionsLabel: UILabel! { get set }
    var balanceLabel: UILabel! { get set }
    var rightButton: UIButton! { get set }
    var stackView: UIStackView! { get set }
    var balanceAndRightBtnHorizontalStackView: UIStackView! { get set }
    var balanceAndRightBtnVerticalStackView: UIStackView! { get set }
}

extension DashboardAccountsTableViewCellInstaller {
    func initSubviews() {
        actionsLabel = UILabel()
        actionsLabel.textColor = .dashDarkBlue
        actionsLabel.font = .gothamNarrow(size: 18, .book) //EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        
        balanceLabel = UILabel()
        balanceLabel.textColor = .dashDarkBlue
        balanceLabel.font = .gothamNarrow(size: 16, .bold) //EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))
        
        rightButton = UIButton()
        rightButton.setImage(UIImage(named: "btn_share"), for: .normal)
        rightButton.isHidden = true
        
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = Adaptive.val(10)
        
        balanceAndRightBtnHorizontalStackView = UIStackView()
        balanceAndRightBtnHorizontalStackView.axis = .horizontal
        balanceAndRightBtnHorizontalStackView.distribution = .fill
        balanceAndRightBtnHorizontalStackView.alignment = .center
        balanceAndRightBtnHorizontalStackView.spacing = 0//Adaptive.val(3)
        
        balanceAndRightBtnVerticalStackView = UIStackView()
        balanceAndRightBtnVerticalStackView.axis = .vertical
        balanceAndRightBtnVerticalStackView.distribution = .fill
        balanceAndRightBtnVerticalStackView.alignment = .trailing
        balanceAndRightBtnVerticalStackView.spacing = Adaptive.val(3)
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
        
        stackView.addArrangedSubview(actionsLabel)
        stackView.addArrangedSubview(balanceAndRightBtnVerticalStackView)
        
        balanceAndRightBtnVerticalStackView.addArrangedSubview(balanceAndRightBtnHorizontalStackView)
        balanceAndRightBtnHorizontalStackView.addArrangedSubview(balanceLabel)
        balanceAndRightBtnHorizontalStackView.addArrangedSubview(rightButton)
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.top.bottom.equalToSuperview()
            maker.left.equalTo(Adaptive.val(16))
            maker.right.equalTo(-Adaptive.val(16))
        }
        
        rightButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(25))
        }
    }
}

fileprivate struct Size {
    
}
