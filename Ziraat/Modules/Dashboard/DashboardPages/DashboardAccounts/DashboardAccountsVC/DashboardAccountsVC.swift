//
//  DashboardAccountsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardAccountsVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool, isAccountsEmpty: Bool)
    func reloadViewForBalanceDownload()
    func showShareSheet(for text: String)
}

class DashboardAccountsVC: BaseViewController, DashboardAccountsViewInstaller {
    
    var mainView: UIView { view }
    var reloadButtonView: ReloadButtonView!
    var containerView: UIView!
    var collectionLayout: UPCarouselFlowLayout!
    var collectionView: UICollectionView!
    var previousButton: UIButton!
    var nextButton: UIButton!
    var redInfoIconCoverView: UIView!
    var redInfoImageView: UIImageView!
    var redInfoImageTitleLabel: UILabel!
    var allMyAccountsButton: UIButton!
    var eyeButton: UIButton!
    var tableView: AutomaticHeightTableView!
    var buttonsStackView: UIStackView!
    var moneyTransferButton: NextButton!
    var accountActivitiesButton: NextButton!
    
    var account: AccountResponse!
    
    var presenter: DashboardAccountsPresenter!
    
    private var currentCardPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadButtonClicked(_:)), for: .touchUpInside)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        previousButton.addTarget(self, action: #selector(previousNextCardClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(previousNextCardClicked(_:)), for: .touchUpInside)
        allMyAccountsButton.addTarget(self, action: #selector(allMyAccountsButtonClicked(_:)), for: .touchUpInside)
        eyeButton.addTarget(self, action: #selector(visibilityClicked(_:)), for: .touchUpInside)
        moneyTransferButton.addTarget(self, action: #selector(moneyTransferButtonClicked(_:)), for: .touchUpInside)
        accountActivitiesButton.addTarget(self, action: #selector(statementButtonClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadViewForBalanceDownload()
        setStateForVisibilityButton()
        presenter.viewWillAppear(animated: animated)
    }
    
    func setStateForVisibilityButton() {
        if UDManager.hideBalance == true {
            eyeButton.isSelected = true
        } else {
            eyeButton.isSelected = false
        }
    }
    
    @objc private func visibilityClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        UDManager.hideBalance = !UDManager.hideBalance
        reloadViewForBalanceDownload()
    }
    
    @objc private func statementButtonClicked(_ sender: UIButton) {
        presenter.accountStatementsButtonClicked(account: account)
    }
    
    @objc private func moneyTransferButtonClicked(_ sender: UIButton) {
        presenter.moneyTransferButtonClicked()
    }
    
    @objc private func allMyAccountsButtonClicked(_ sender: UIButton) {
        presenter.allAccountsButtonClicked()
    }
    
    @objc private func reloadButtonClicked(_ sender: UIButton) {
        presenter.viewDidLoad()
    }
    
    @objc private func previousNextCardClicked(_ sender: UIButton) {
        if sender == previousButton {
            currentCardPage -= 1
        } else { // nextButton
            currentCardPage += 1
        }
        
        collectionView.scrollToItem(at: IndexPath(item: currentCardPage, section: 0),
                                    at: .centeredHorizontally,
                                    animated: true)
        setPreviousNextButtonState(for: currentCardPage)
    }
    
    private func setPreviousNextButtonState(for page: Int) {
        if presenter.viewModel.accounts.count < 2 {
            previousButton.isHidden = true
            nextButton.isHidden = true
        } else {
            previousButton.isHidden = (page == 0)
            nextButton.isHidden = (page == presenter.viewModel.accounts.count - 1)
        }
        self.tableView.reloadData()
    }
}

extension DashboardAccountsVC: DashboardAccountsVCProtocol {
    func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool, isAccountsEmpty: Bool) {
        redInfoIconCoverView.isHidden = !isAccountsEmpty
        allMyAccountsButton.isHidden = isAccountsEmpty
        
        if isAccountsEmpty {
            reloadButtonView.isHidden = true
            containerView.isHidden = false
            collectionView.isHidden = true
            tableView.isHidden = true
            moneyTransferButton.isHidden = true
            accountActivitiesButton.isHidden = true
        } else {
            reloadButtonView.isHidden = !show
            reloadButtonView.animateIndicator = animateReloadButton
            
            containerView.isHidden = show
            setPreviousNextButtonState(for: currentCardPage)
            collectionView.isHidden = false
            tableView.isHidden = false
            moneyTransferButton.isHidden = false
            accountActivitiesButton.isHidden = false
            collectionView.reloadData()
            tableView.reloadData()
        }
    }
    
    func reloadViewForBalanceDownload() {
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    func showShareSheet(for text: String) {
        let textToShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension DashboardAccountsVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.viewModel.accounts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(DashboardAccountCollectionCell.self)", for: indexPath) as! DashboardAccountCollectionCell
        let accountVM = presenter.viewModel.accountVM(at: indexPath.row)
        account = accountVM.account
        return cell.setup(with: accountVM)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionView {
            currentCardPage = collectionView.mostCenteredCellIndexPath()!.row
            setPreviousNextButtonState(for: currentCardPage)
        } else { // tableView
            
        }
    }
}

extension DashboardAccountsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.accountTableCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = String(describing: DashboardAccountsTableViewCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! DashboardAccountsTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.delegate = self
        let cellType = presenter.viewModel.accountTableCells[indexPath.row]
        let cellValue = presenter.viewModel.tableCellValue(for: currentCardPage,
                                                           cellType: cellType)
        return cell.setup(with: cellType, value: cellValue)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(50)
    }
}

extension DashboardAccountsVC: DashboardAccountsTableViewCellDelegate {
    func rightButtonClicked(for cellType: AccountTableCellType) {
        presenter.rightButtonClicked(for: cellType, currentCardPage: currentCardPage)
    }
}
