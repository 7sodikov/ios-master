//
//  DashboardAccountsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardAccountsInteractorProtocol: class {
    func accountsList()
    func accountsBalance()
}

class DashboardAccountsInteractor: DashboardAccountsInteractorProtocol {
    weak var presenter: DashboardAccountsInteractorToPresenterProtocol!
    
    func accountsList() {
        NetworkService.Accounts.accountsList() { [weak self] (result) in
            self?.presenter.didGetAllAccounts(with: result)
        }
    }
    
    func accountsBalance() {
        NetworkService.Accounts.accountBalance() { [weak self] (result) in
            self?.presenter.didGetAllAccountBalances(with: result)
        }
    }
}
