//
//  DashboardAccountsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/1/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum AccountTableCellType {
    case balance
    case iban
    case availableBalance
    
    var title: String {
        switch self {
        case .balance: return RS.lbl_balance.localized()
        case .iban: return RS.lbl_iban.localized()
        case .availableBalance: return RS.lbl_available_balance.localized()
        }
    }
}

class DashboardAccountVM {
    var account: AccountResponse
    var balance: AccountBalanceResponse?
    
    var currency: String {
        let curType = CurrencyType.type(for: account.currency)
        return curType?.data.label ?? ""
    }
    
    var balanceStr: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(balance?.balance ?? 0)/100
        let balanceNumber = NSNumber(value: b)
        var balanceStr = formatter.string(from: balanceNumber) ?? ""
        if balanceStr.count > 0 {
            balanceStr = "\(balanceStr) \(currency)"
        }
        if UDManager.hideBalance {
            return "* * * * " + currency
        }
        return balanceStr
    }
    
    var saldoStr: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(balance?.balance ?? 0)/100
        let balanceNumber = NSNumber(value: b)
        var saldoStr = formatter.string(from: balanceNumber) ?? ""
        if saldoStr.count > 0 {
            saldoStr = "\(saldoStr) \(currency)"
        }
        if UDManager.hideBalance {
            return "* * * * " + currency
        }
        return saldoStr
    }
    
    init(_ account: AccountResponse, _ balance: AccountBalanceResponse?) {
        self.account = account
        self.balance = balance
    }
}

class DashboardAccountsVM {
    var accounts: [AccountResponse] = []
    var accountBalance: [AccountBalanceResponse] = []
    let accountTableCells: [AccountTableCellType] = [.balance, .availableBalance, .iban]
    
    func accountVM(at index: Int) -> DashboardAccountVM {
        var balance: AccountBalanceResponse?
        if index <= accountBalance.count - 1 {
            balance = accountBalance[index]
        }
        return DashboardAccountVM(accounts[index], balance)
    }
    
    func tableCellValue(for accountIndex: Int, cellType: AccountTableCellType) -> String? {
        guard accounts.count > accountIndex else {
            return nil
        }
        
        let cardVM = self.accountVM(at: accountIndex)
        
        switch cellType {
        case .balance:
            return cardVM.saldoStr
        case .iban:
            return cardVM.account.number
        case .availableBalance:
            return cardVM.balanceStr
        }
    }
}
