//
//  DashboardAccountsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardAccountsRouterProtocol: AnyObject {
    static func createModule() -> BaseMainSwipableViewController
    func navigateToMyAccountsListVC(in navCtrl: Any)
    func navigateToMoneyTransferVC(in navCtrl: Any)
    func navigateToAccountStatementVC(in navCtrl: Any, account: AccountResponse, currency: String)
}

class DashboardAccountsRouter: DashboardAccountsRouterProtocol {
    static func createModule() -> BaseMainSwipableViewController {
        let vc = DashboardAccountsVC()
        let presenter = DashboardAccountsPresenter()
        let interactor = DashboardAccountsInteractor()
        let router = DashboardAccountsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToMyAccountsListVC(in navCtrl: Any) {
        let viewCtrl = navCtrl as? UIViewController
        let myAccountsView = MyAccountsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(myAccountsView, animated: true)

    }
    
    func navigateToMoneyTransferVC(in navCtrl: Any) {
        let viewCtrl = navCtrl as? UIViewController
        let vc = MoneyTransfersRouter(for: nil, historyItem: nil).viewController
        viewCtrl?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAccountStatementVC(in navCtrl: Any, account: AccountResponse, currency: String) {
        let viewCtrl = navCtrl as? UIViewController
        let moneyTransferView = AccountStatementRouter.createModule(data: account, currency: currency)
        viewCtrl?.navigationController?.pushViewController(moneyTransferView, animated: true)
    }
}
