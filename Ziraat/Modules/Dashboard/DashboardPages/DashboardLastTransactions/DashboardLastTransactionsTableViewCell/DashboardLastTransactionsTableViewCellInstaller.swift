//
//  DashboardLastTransactionsTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol DashboardLastTransactionsTableViewCellInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var dateBorderView: UIView! { get set }
    var dayLabel: UILabel! { get set }
    var monthLabel: UILabel! { get set }
    var lineView: UIView! { get set }
    var timeLabel: UILabel! { get set }
    var dateStackView: UIStackView! { get set }
    var senderLabel: UILabel! { get set }
    var senderValueLabel: UILabel! { get set }
    var receiverLabel: UILabel! { get set }
    var receiverValueLabel: UILabel! { get set }
    var amountLabel: UILabel! { get set }
    var amountValueLabel: UILabel! { get set }
    var chequeImageView: UIButton! { get set }
    var repeatButton: UIButton! { get set }
    var titleStackView: UIStackView! { get set }
    var valueStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
}

extension DashboardLastTransactionsTableViewCellInstaller {
    func initSubviews() {
        backView = UIView()
        
        dateBorderView = UIView()
        dateBorderView.layer.borderColor = ColorConstants.mainTextColor.cgColor
        dateBorderView.layer.borderWidth = Adaptive.val(1)
        dateBorderView.layer.cornerRadius = Adaptive.val(6)
        
        dayLabel = UILabel()
        dayLabel.font = .gothamNarrow(size: 14, .bold)
        dayLabel.textColor = .dashDarkBlue
        
        monthLabel = UILabel()
        monthLabel.font = .gothamNarrow(size: 10, .book)
        monthLabel.textColor = .dashDarkBlue
        
        lineView = UIView()
        lineView.backgroundColor = .dashDarkBlue
        
        timeLabel = UILabel()
        timeLabel.font = .gothamNarrow(size: 14, .book)
        timeLabel.textColor = .dashDarkBlue
        
        dateStackView = UIStackView()
        dateStackView.axis = .vertical
        dateStackView.alignment = .center
        dateStackView.distribution = .fillProportionally
        dateStackView.spacing = Adaptive.val(4)
        
        senderLabel = UILabel()
        senderLabel.font = .gothamNarrow(size: 14, .book)
        senderLabel.textColor = .dashDarkBlue
        senderLabel.text = RS.lbl_sender.localized()
                
        senderValueLabel = UILabel()
        senderValueLabel.font = .gothamNarrow(size: 13, .bold)
        senderValueLabel.textColor = .dashDarkBlue
        senderValueLabel.textAlignment = .left
        
        receiverLabel = UILabel()
        receiverLabel.font = .gothamNarrow(size: 14, .book)
        receiverLabel.textColor = .dashDarkBlue
        receiverLabel.text = RS.lbl_receiver.localized()
        
        receiverValueLabel = UILabel()
        receiverValueLabel.font = .gothamNarrow(size: 13, .bold)
        receiverValueLabel.textColor = .dashDarkBlue
        receiverValueLabel.textAlignment = .left
        
        amountLabel = UILabel()
        amountLabel.font = .gothamNarrow(size: 14, .book)
        amountLabel.textColor = .dashDarkBlue
        amountLabel.text = RS.lbl_amount.localized()
        
        amountValueLabel = UILabel()
        amountValueLabel.font = .gothamNarrow(size: 13, .bold)
        amountValueLabel.textColor = .dashDarkBlue
        amountValueLabel.textAlignment = .left
        
        chequeImageView = UIButton()
        chequeImageView.setImage(UIImage(named: "btn_print_lt"), for: .normal)
        chequeImageView.contentHorizontalAlignment = .fill
        chequeImageView.contentVerticalAlignment = .fill
       
        repeatButton = UIButton()
        repeatButton.setImage(UIImage(named: "btn_reload_lt"), for: .normal)
        repeatButton.contentHorizontalAlignment = .fill
        repeatButton.contentVerticalAlignment = .fill
        
        titleStackView = UIStackView()
        titleStackView.axis = .vertical
        titleStackView.alignment = .leading
        titleStackView.distribution = .fillEqually
        titleStackView.spacing = 0
        
        valueStackView = UIStackView()
        valueStackView.axis = .vertical
        valueStackView.alignment = .leading
        valueStackView.distribution = .fillEqually
        valueStackView.spacing = 0
        
        mainStackView = UIStackView()
        mainStackView.axis = .horizontal
        mainStackView.distribution = .fill
        mainStackView.spacing = Adaptive.val(16)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(dateBorderView)
        
        mainView.addSubview(dateStackView)
        dateStackView.addArrangedSubview(dayLabel)
        dateStackView.addArrangedSubview(monthLabel)
        dateStackView.addArrangedSubview(lineView)
        dateStackView.addArrangedSubview(timeLabel)
        
        titleStackView.addArrangedSubview(senderLabel)
        valueStackView.addArrangedSubview(senderValueLabel)
        titleStackView.addArrangedSubview(receiverLabel)
        valueStackView.addArrangedSubview(receiverValueLabel)
        titleStackView.addArrangedSubview(amountLabel)
        valueStackView.addArrangedSubview(amountValueLabel)

        mainView.addSubview(chequeImageView)
        mainView.addSubview(repeatButton)
        
        mainStackView.addArrangedSubview(titleStackView)
        mainStackView.addArrangedSubview(valueStackView)
        mainView.addSubview(mainStackView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        dateBorderView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(60))
            maker.height.equalTo(Adaptive.val(70))
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(10))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(18))
        }
        
        dateStackView.snp.remakeConstraints { (maker) in
            maker.top.leading.equalTo(dateBorderView).offset(Adaptive.val(5))
            maker.bottom.trailing.equalTo(dateBorderView).offset(-Adaptive.val(5))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(dateBorderView.snp.trailing).offset(Adaptive.val(20))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            maker.top.equalTo(dateBorderView.snp.top)
            maker.bottom.equalTo(dateBorderView.snp.bottom).offset(-Adaptive.val(5))
        }
        
        repeatButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(30))
            maker.trailing.equalTo(chequeImageView.snp.leading).offset(-Adaptive.val(15))
            maker.top.equalTo(mainStackView.snp.bottom).offset(Adaptive.val(5))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(10))
        }
        
        chequeImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(30))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(10))
        }
        
        titleStackView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(88))
        }
        
        lineView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(1)
            maker.width.equalTo(timeLabel)
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(30)
}
