//
//  DashboardLastTransactionsTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

class DashboardLastTransactionsTableViewCell: UITableViewCell, DashboardLastTransactionsTableViewCellInstaller {
    var backView: UIView!
    var dateBorderView: UIView!
    var dayLabel: UILabel!
    var monthLabel: UILabel!
    var lineView: UIView!
    var timeLabel: UILabel!
    var dateStackView: UIStackView!
    var senderLabel: UILabel!
    var senderValueLabel: UILabel!
    var receiverLabel: UILabel!
    var receiverValueLabel: UILabel!
    var amountLabel: UILabel!
    var amountValueLabel: UILabel!
    var chequeImageView: UIButton!
    var repeatButton: UIButton!
    var titleStackView: UIStackView!
    var valueStackView: UIStackView!
    var mainStackView: UIStackView!
    
    var mainView: UIView { contentView }
    
    var chequeTapCallback: (() -> Void)?
    var repeatTapCallback: (() -> Void)?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        
        chequeImageView.addTarget(self, action: #selector(didTapCheque(_:)), for: .touchUpInside)
        repeatButton.addTarget(self, action: #selector(didTapRepeat(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc func didTapCheque(_ sender: UIButton) {
        chequeTapCallback?()
    }
    
    @objc func didTapRepeat(_ sender: UIButton) {
        repeatTapCallback?()
    }
    
    func setupOperation(history: DashboardLastTransactionVM) -> DashboardLastTransactionsTableViewCell {
        let historyItem = history.historyItem
        dayLabel.text = history.day
        monthLabel.text = history.month
        timeLabel.text = history.time
        senderValueLabel.text = historyItem.senderOwner
        receiverValueLabel.text = historyItem.receiverOwner ?? "-"
        amountValueLabel.text = "\(history.amount) \(history.currency)"
        return self
    }
    
    func setupPayment(payment: DashboardLastTransactionPaymentVM) -> DashboardLastTransactionsTableViewCell {
        let historyItem = payment.historyPaymentItem
        dayLabel.text = payment.day
        monthLabel.text = payment.month
        timeLabel.text = payment.time
        senderValueLabel.text = historyItem.senderOwner
        receiverValueLabel.text = historyItem.receiverOwner
        amountValueLabel.text = "\(payment.amount) \(payment.currency)"
        return self
    }
}
