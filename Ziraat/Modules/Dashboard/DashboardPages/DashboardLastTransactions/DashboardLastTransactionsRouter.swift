//
//  DashboardLastTransactionsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardLastTransactionsRouterProtocol: class {
    static func createModule() -> BaseMainSwipableViewController
    func navigatetoPdfChequeVC(in navigateCtrl: Any?, operation: String)
    func navigateToOperationForm(for historyItem: HistoryResponse, from view: Any?)
    func navigateToPaymentForm(for historyItem: CLastPayment, from view: Any?)
    func navigateToConvertion(for historyItem: HistoryResponse, from view: Any?)
    func navigateToMoneyTranfer(for historyItem: HistoryResponse, from view: Any?)
}

class DashboardLastTransactionsRouter: DashboardLastTransactionsRouterProtocol {
    static func createModule() -> BaseMainSwipableViewController {
        let vc = DashboardLastTransactionsVC()
        let presenter = DashboardLastTransactionsPresenter()
        let interactor = DashboardLastTransactionsInteractor()
        let router = DashboardLastTransactionsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigatetoPdfChequeVC(in navigateCtrl: Any?, operation: String) {
//        let viewCtrl = navigateCtrl as! UIViewController
//        let statementVC = PdfChequeRouter.createModule(operation: operation, title: "nil", isHidden: false)
//        statementVC.modalPresentationStyle = .overFullScreen
//        viewCtrl.present(statementVC, animated: false, completion: nil)
        
        let viewCtrl = navigateCtrl as? UIViewController
        let operationsView = PdfChequeRouter.createModule(operation: operation, title: RS.lbl_last_transactions.localized(), isHidden: false)
        viewCtrl?.navigationController?.pushViewController(operationsView, animated: true)
    }
    
    func navigateToOperationForm(for historyItem: HistoryResponse, from view: Any?) {
        let viewCtrl = view as? UIViewController
//        let operationsView = OperationsRouter.createModule(for: nil, historyItem: historyItem)
        let operationsView = MoneyTransfersRouter(for: nil, historyItem: historyItem).viewController
        viewCtrl?.navigationController?.pushViewController(operationsView, animated: true)
    }
    
    func navigateToPaymentForm(for historyItem: CLastPayment, from view: Any?) {
        let vc = PaymentDataEntryFormRouter.createModule(for: nil, favoriteItem: nil, payment: historyItem)
//        let vc = NewPaymentsThirdPageRouter.createModule(with: nil, favoriteItem: nil, payment: historyItem)
//        let vc = NewPaymentsRouter.createModule(with: historyItem)
        let navCtrl = NavigationController(rootViewController: vc)
        navCtrl.modalPresentationStyle = .overCurrentContext
        let currentViewCtrl = view as? UIViewController
        currentViewCtrl?.present(navCtrl, animated: true, completion: nil)
        
        
//        let viewCtrl = view as? UIViewController
//        let vc = NewPaymentsRouter.createModule(with: historyItem)
//        viewCtrl?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToConvertion(for historyItem: HistoryResponse, from view: Any?) {
        let currentViewCtrl = view as? UIViewController
        let convertionVC = ConversionNewRouter(history: historyItem).viewController
        currentViewCtrl?.navigationController?.pushViewController(convertionVC, animated: true)
    }
    
    func navigateToMoneyTranfer(for historyItem: HistoryResponse, from view: Any?) {
        let currentViewCtrl = view as? UIViewController
        let convertionVC = MoneyTransfersRouter(for: nil, historyItem: historyItem).viewController
        currentViewCtrl?.navigationController?.pushViewController(convertionVC, animated: true)
    }
}
