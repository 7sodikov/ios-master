//
//  DashboardLastTransactionsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardLastTransactionsInteractorProtocol: AnyObject {
    func getOperationHistoryPayment(size: Int)
    func getOperationHistoryOperation(size: Int)
    func getOperationReceipt(operationId: Int)
}

class DashboardLastTransactionsInteractor: DashboardLastTransactionsInteractorProtocol {
    weak var presenter: DashboardLastTransactionsInteractorToPresenterProtocol!
    
    func getOperationHistoryPayment(size: Int) {
        NetworkService
            .Operation
            .paymentHistory(operationType: .payment, size: size) { [weak self]
            (result) in self?.presenter.didGetOperationHistoryPayment(with: result)
        }
    }
    
    func getOperationHistoryOperation(size: Int) {
        NetworkService
            .Operation
            .operationHistory(operationType: .operation, size: size) { [weak self] (result) in
                self?.presenter.didGetOperationHistoryOperation(with: result)
        }
    }
    
    func getOperationReceipt(operationId: Int) {
        NetworkService
            .Operation
            .operationReceipt(operationId: operationId) { [weak self] (result) in self?.presenter.didGetOperationReceipt(with: result)
        }
    }
}
