//
//  DashboardLastTransactionsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardLastTransactionsPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: DashboardLastTransactionsVM { get }
    func setupTableViewPayment()
    func setupTableViewOperation()
    func viewDidLoad()
    var limit: Int { get }
    func pdfPaymentButtonClicked(index: Int)
    func pdfOperationButtonClicked(index: Int)
    func repeatOperationButtonClicked(index: Int)
    func repeatPaymentButtonClicked(index: Int)
    
//    var pageNumber: Int { get }
//    var totalPages: Int { get }
//    var loading: Bool { get }
//    func loadingMore()
}

protocol DashboardLastTransactionsInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
//    func didGetOperationHistory(with response: ResponseResult<OperationHistoriesResponse, AppError>)
    func didGetOperationHistoryPayment(with response: ResponseResult<[CLastPayment], AppError>)
    func didGetOperationHistoryOperation(with response: ResponseResult<[HistoryResponse], AppError>)
    func didGetOperationReceipt(with response: ResponseResult<OperationReceiptResponse, AppError>)
}

class DashboardLastTransactionsPresenter: DashboardLastTransactionsPresenterProtocol {
    weak var view: DashboardLastTransactionsVCProtocol!
    var interactor: DashboardLastTransactionsInteractorProtocol!
    var router: DashboardLastTransactionsRouterProtocol!
    
    var receiptString: String!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = DashboardLastTransactionsVM()
    var limit: Int = 20

//    var pageNumber: Int = 0
//    var loading: Bool = false
//    var totalPages: Int = 0
    
    // Private property and methods
    
    func viewDidLoad() {
        setupTableViewOperation()
        view.showReloadButtonForResponses(false, animateReloadButton: false)
    }
    
    func setupTableViewPayment() {
        interactor.getOperationHistoryPayment(size: limit)
    }
    
    func setupTableViewOperation() {
        interactor.getOperationHistoryOperation(size: limit)
    }
    
    func pdfPaymentButtonClicked(index: Int) {
        let payment = viewModel.historyPayment?[index]
        interactor.getOperationReceipt(operationId: payment?.operationId ?? 0)
    }
    
    func pdfOperationButtonClicked(index: Int) {
        let operation = viewModel.historyMoneyTransfer?[index]
        interactor.getOperationReceipt(operationId: operation?.operationId ?? 0)
    }
    
    func repeatOperationButtonClicked(index: Int) {
        guard let history = viewModel.historyMoneyTransfer?[index] else {
            return
        }
        switch history.operationMode {
        case .pca, .acp:
            router.navigateToConvertion(for: history, from: view)
        case .a2l, .p2l, .p2a, .a2p, .p2p:
            router.navigateToMoneyTranfer(for: history, from: view)
        default:
            router.navigateToOperationForm(for: history, from: view)
        }
        
    }
    
    func repeatPaymentButtonClicked(index: Int) {
        guard let history = viewModel.historyPayment?[index] else {
            return
        }
        router.navigateToPaymentForm(for: history, from: view)
    }
    
//    func loadingMore() {
//        loading = true
//        setupTableView()
//    }
}

extension DashboardLastTransactionsPresenter: DashboardLastTransactionsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
//    func didGetOperationHistory(with response: ResponseResult<OperationHistoriesResponse, AppError>) {
//        switch response {
//        case let .success(result):
//            if result.httpStatusCode == 200 {
//                totalPages = result.data.totalPages
//                loading = false
////                viewModel.operationHistory = result.data
//                if pageNumber == 0 {
//                    if view.isMoneyTransferSelected == true {
//                        viewModel.historyMoneyTransfer = result.data.history
//                    } else {
////                        viewModel.historyPayment = result.data.history
//                    }
//                }
//                else {
//                    if view.isMoneyTransferSelected == true {
//                        viewModel.historyMoneyTransfer?.append(contentsOf: result.data.history ?? [])
//                    } else {
////                        viewModel.historyPayment?.append(contentsOf: result.data.history ?? [])
//                    }
//                }
//
//                pageNumber += 1 // increment pageNumber for endless scrolling
//            }
//        case let .failure(error):
//            break
//        }
//    }
//
    func didGetOperationHistoryPayment(with response: ResponseResult<[CLastPayment], AppError>) {
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                viewModel.historyPayment = result.data
                if viewModel.historyPayment!.count > 0 {
                    view.showNoRecords(isHidden: true)
                    view.showReloadButtonForResponses(false, animateReloadButton: false)
                } else {
                    view.showNoRecords(isHidden: false)
                }
            }
        case .failure(_):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
        }
    }
    
    func didGetOperationHistoryOperation(with response: ResponseResult<[HistoryResponse], AppError>) {
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                viewModel.historyMoneyTransfer = result.data
                if viewModel.historyMoneyTransfer!.count > 0 {
                    view.showNoRecords(isHidden: true)
                    view.showReloadButtonForResponses(false, animateReloadButton: false)
                } else {
                    view.showNoRecords(isHidden: false)
                }
//                if result.data.count == 0 {
//                    view.showNoRecords()
//                } else {
                    
//                }
            }
        case .failure(_):
            view.showReloadButtonForResponses(true, animateReloadButton: true)
        }
    }
    
    func didGetOperationReceipt(with response: ResponseResult<OperationReceiptResponse, AppError>) {
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                receiptString = result.data.receipt
                router.navigatetoPdfChequeVC(in: view, operation: receiptString)
            }
        case .failure(_):
            break
        }
    }
}
