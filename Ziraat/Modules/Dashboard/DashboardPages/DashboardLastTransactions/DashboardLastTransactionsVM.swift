//
//  DashboardLastTransactionsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum OperationType: String {
    case payment = "payment"
    case operation = "operation"
}

class DashboardLastTransactionsVM {
    var operationHistory: OperationHistoriesResponse!
    var historyMoneyTransfer: [HistoryResponse]?
    var historyPayment: [CLastPayment]?
    var itemsMoneyTransfer: [DashboardLastTransactionVM] = []
    var itemsPayment: [DashboardLastTransactionPaymentVM] = []
    
    func addPayment() {
        for paymentItem in historyPayment ?? [] {
            itemsPayment.append(DashboardLastTransactionPaymentVM(paymentHistory: paymentItem))
        }
    }
    
    func addMoneyTransfer() {
        for operationItem in historyMoneyTransfer ?? [] {
            itemsMoneyTransfer.append(DashboardLastTransactionVM(history: operationItem))
        }
    }
    
    var fromDay: String {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd"
        var dayComponent        = DateComponents()
        dayComponent.day        = -30
        let theCalendar         = Calendar.current
        let previousDay         = theCalendar.date(byAdding: dayComponent, to: Date())!
        let fromDate            = formatter.string(from: previousDay)
        return fromDate
    }
    
    var toDay: String {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd"
        let now                 = Date()
        let toDate              = formatter.string(from:now)
        return toDate
    }
}

class DashboardLastTransactionVM {
    var historyItem: HistoryResponse
    
    var day: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (historyItem.date) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let day = myCalendar.component(.day, from: date!)
        return String(day)
    }
    
    var month: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (historyItem.date) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let month = myCalendar.component(.month, from: date!)
        if month == 1 {
            return RS.lbl_jan.localized()
        } else if month == 2 {
            return RS.lbl_feb.localized()
        } else if month == 3 {
            return RS.lbl_mar.localized()
        } else if month == 4 {
            return RS.lbl_apr.localized()
        } else if month == 5 {
            return RS.lbl_may.localized()
        } else if month == 6 {
            return RS.lbl_jun.localized()
        } else if month == 7 {
            return RS.lbl_jul.localized()
        } else if month == 8 {
            return RS.lbl_aug.localized()
        } else if month == 9 {
            return RS.lbl_sept.localized()
        } else if month == 10 {
            return RS.lbl_oct.localized()
        } else if month == 11 {
            return RS.lbl_nov.localized()
        } else {
            return RS.lbl_dec.localized()
        }
    }
    
    var year: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (historyItem.date) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let year = myCalendar.component(.year, from: date!)
        return String(year)
    }
    
    var time: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: historyItem.time ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let hour = myCalendar.component(.hour, from: date!)
        let minute = myCalendar.component(.minute, from: date!)
        let time = String(format:"%02d:%02d", hour, minute)
        return String(time)
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    var amount: String {
        return formattedMoney(historyItem.amount) ?? "0"
    }
    
    var currency: String {
        let curType = CurrencyType.type(for: historyItem.currency)
        return curType?.data.label ?? ""
    }
    
    init(history: HistoryResponse) {
        self.historyItem = history
    }
}

class DashboardLastTransactionPaymentVM {
    var historyPaymentItem: CLastPayment
    
    var day: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (historyPaymentItem.date) )
        let myCalendar = Calendar(identifier: .gregorian)
        let day = myCalendar.component(.day, from: date!)
        return String(day)
    }
    
    var month: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (historyPaymentItem.date) )
        let myCalendar = Calendar(identifier: .gregorian)
        let month = myCalendar.component(.month, from: date!)
        if month == 1 {
            return RS.lbl_jan.localized()
        } else if month == 2 {
            return RS.lbl_feb.localized()
        } else if month == 3 {
            return RS.lbl_mar.localized()
        } else if month == 4 {
            return RS.lbl_apr.localized()
        } else if month == 5 {
            return RS.lbl_may.localized()
        } else if month == 6 {
            return RS.lbl_jun.localized()
        } else if month == 7 {
            return RS.lbl_jul.localized()
        } else if month == 8 {
            return RS.lbl_aug.localized()
        } else if month == 9 {
            return RS.lbl_sept.localized()
        } else if month == 10 {
            return RS.lbl_oct.localized()
        } else if month == 11 {
            return RS.lbl_nov.localized()
        } else {
            return RS.lbl_dec.localized()
        }
    }
    
    var year: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: historyPaymentItem.date)
        let myCalendar = Calendar(identifier: .gregorian)
        let year = myCalendar.component(.year, from: date!)
        return String(year)
    }
    
    var time: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: historyPaymentItem.time)
        let myCalendar = Calendar(identifier: .gregorian)
        let hour = myCalendar.component(.hour, from: date!)
        let minute = myCalendar.component(.minute, from: date!)
        let time = String(format:"%02d:%02d", hour, minute)
        return String(time)
    }
    
    private func formattedMoney(_ money: Int64?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    var amount: String {
        return formattedMoney(historyPaymentItem.amount) ?? "0"
    }
    
    var currency: String {
        let curType = CurrencyType.type(for: historyPaymentItem.currency)
        return curType?.data.label ?? ""
    }
    
    init(paymentHistory: CLastPayment) {
        self.historyPaymentItem = paymentHistory
    }
}
