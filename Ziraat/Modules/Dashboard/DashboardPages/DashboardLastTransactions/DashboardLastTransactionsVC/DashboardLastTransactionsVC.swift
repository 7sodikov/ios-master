//
//  DashboardLastTransactionsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import Foundation

protocol DashboardLastTransactionsVCProtocol: AnyObject {
    var navigationCtrl: Any { get }
    func showNoRecords(isHidden: Bool)
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class DashboardLastTransactionsVC: BaseMainSwipableViewController, DashboardLastTransactionsViewInstaller {    
    var segmentedControl: EZSegmentedControl!
    var mainSeparatorLineView: UIView!
    var tableView: TableView!
    var noRecordLabel: UILabel!
    var noRecordImageView: UIImageView!
    var reloadButtonView: ReloadButtonView!
    
    var mainView: UIView { view }
    var presenter: DashboardLastTransactionsPresenterProtocol!
        
    fileprivate let cellid = "\(DashboardLastTransactionsTableViewCell.self)"
    
    var isMoneyTransferSelectedInternal = true
            
    override func viewDidLoad() {
        super.viewDidLoad()
                
        setupSubviews()
        
        presenter.viewDidLoad()
                
        tableView.delegate = self
        tableView.dataSource = self
        
        segmentedControl.addTarget(self, action: #selector(self.segmentedControlChanged(_:)), for: .valueChanged)
    }
    
    @objc private func segmentedControlChanged(_ sender: EZSegmentedControl) {
        if sender.selectedIndex == 0 {
            showNoRecords(isHidden: true)
            isMoneyTransferSelectedInternal = true
            noRecordLabel.text = RS.lbl_no_money_transfer.localized()
            presenter.setupTableViewOperation()
        } else if sender.selectedIndex == 1 {
            showNoRecords(isHidden: true)
            isMoneyTransferSelectedInternal = false
            noRecordLabel.text = RS.lbl_no_payments.localized()
            presenter.setupTableViewPayment()
        }
        tableView.reloadData()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        tableView.reloadData()
//    }
}

extension DashboardLastTransactionsVC: DashboardLastTransactionsVCProtocol, UITableViewDelegate, UITableViewDataSource {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
    
    func showNoRecords(isHidden: Bool) {
        noRecordLabel.isHidden = isHidden
        noRecordImageView.isHidden = isHidden
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMoneyTransferSelectedInternal == true {
            return presenter.viewModel.historyMoneyTransfer?.count ?? 0
        } else if isMoneyTransferSelectedInternal == false {
            return presenter.viewModel.historyPayment?.count ?? 0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! DashboardLastTransactionsTableViewCell
        cell.backgroundColor = .clear
        if isMoneyTransferSelectedInternal {
            presenter.viewModel.addMoneyTransfer()
            if presenter.viewModel.itemsMoneyTransfer.count == 0 {
                let cell = UITableViewCell()
                cell.backgroundColor = .clear
                self.tableView.separatorColor = .clear
                noRecordLabel.isHidden = false
                return cell
            } else {
                let backgroundView = UIView()
                backgroundView.backgroundColor = ColorConstants.lightGray.withAlphaComponent(0.3)
                cell.selectedBackgroundView = backgroundView
                cell.chequeTapCallback = { [self] in
                    presenter.pdfOperationButtonClicked(index: indexPath.row)
                }
                cell.repeatTapCallback = {
                    self.presenter.repeatOperationButtonClicked(index: indexPath.row)
                }
                let historyItem = presenter.viewModel.itemsMoneyTransfer[indexPath.row]
                return cell.setupOperation(history: historyItem)
            }
        } else if !isMoneyTransferSelectedInternal {
            presenter.viewModel.addPayment()
            if presenter.viewModel.itemsPayment.count == 0 {
                let cell = UITableViewCell()
                cell.backgroundColor = .clear
                self.tableView.separatorColor = .clear
                noRecordLabel.isHidden = false
                return cell
            } else {
                let backgroundView = UIView()
                backgroundView.backgroundColor = ColorConstants.lightGray.withAlphaComponent(0.3)
                cell.selectedBackgroundView = backgroundView
                cell.chequeTapCallback = { [self] in
                    presenter.pdfPaymentButtonClicked(index: indexPath.row)
                }
                cell.repeatTapCallback = {
                    self.presenter.repeatPaymentButtonClicked(index: indexPath.row) //repeatOperationButtonClicked(index: indexPath.row)
                }
                let historyItem = presenter.viewModel.itemsPayment[indexPath.row]
                return cell.setupPayment(payment: historyItem)
            }
        } else {
            return UITableViewCell()
        }
    }
}
