//
//  PdfChequePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PdfChequePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: PdfChequeVM { get }
    var pdfString: String! { get }
}

protocol PdfChequeInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
//    func didGetOperationReceipt(with response: ResponseResult<OperationReceiptResponse, AppError>)
}

class PdfChequePresenter: PdfChequePresenterProtocol {
    weak var view: PdfChequeVCProtocol!
    var interactor: PdfChequeInteractorProtocol!
    var router: PdfChequeRouterProtocol!
    
    var pdfString: String!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PdfChequeVM()
    
    // Private property and methods
}

extension PdfChequePresenter: PdfChequeInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
//    func didGetOperationReceipt(with response: ResponseResult<OperationReceiptResponse, AppError>) {
//        switch response {
//        case let .success(result):
//            if result.httpStatusCode == 200 {
//                pdfString = result.data.receipt
//            }
//        case let .failure(error):
//            break
//        }
//    }
}
