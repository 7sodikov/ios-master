//
//  PdfChequeRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PdfChequeRouterProtocol: class {
    static func createModule(operation: String, title: String?, isHidden: Bool) -> UIViewController
}

class PdfChequeRouter: PdfChequeRouterProtocol {
    static func createModule(operation: String, title: String?, isHidden: Bool) -> UIViewController {
        let vc = PdfChequeVC()
        let presenter = PdfChequePresenter()
        let interactor = PdfChequeInteractor()
        let router = PdfChequeRouter()
        
        vc.presenter = presenter
        vc.title = title
        vc.isHidden = isHidden
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        vc.operationId = operation
        interactor.presenter = presenter
        
        return vc
    }
    
}
