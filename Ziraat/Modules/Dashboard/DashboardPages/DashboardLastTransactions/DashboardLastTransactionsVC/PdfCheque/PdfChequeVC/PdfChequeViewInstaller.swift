//
//  PdfChequeViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import PDFKit

protocol PdfChequeViewInstaller: ViewInstaller {
    var pdfView: PDFView! { get set }
    var saveButton: NextButton! { get set }
    var closeButton: NextButton! { get set }
}

extension PdfChequeViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .white
        
        pdfView = PDFView()
        pdfView.backgroundColor = .clear
        pdfView.displayMode = .singlePageContinuous
        pdfView.autoScales = true
        pdfView.displayDirection = .vertical
        pdfView.addShadow()
        
        saveButton = NextButton.systemButton(with: RS.btn_send_by_email.localized(), cornerRadius: Size.buttonHeight/2)
        saveButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        saveButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        saveButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        closeButton = NextButton.systemButton(with: RS.btn_close.localized().uppercased(), cornerRadius: Size.buttonHeight/2)
        closeButton.setBackgroundColor(.white, for: .normal)
        closeButton.setTitleColor(ColorConstants.gray, for: .normal)
        closeButton.layer.borderWidth = Adaptive.val(1)
        closeButton.layer.borderColor = ColorConstants.gray.cgColor
        closeButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
    }
    
    func embedSubviews() {
        mainView.addSubview(pdfView)
        mainView.addSubview(saveButton)
        mainView.addSubview(closeButton)
    }
    
    func addSubviewsConstraints() {
        pdfView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(50))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.bottom.equalToSuperview().offset(-ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
        }
        
        saveButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ApplicationSize.paddingLeftRight)
            maker.trailing.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(Size.buttonHeight)
            maker.bottom.equalTo(closeButton.snp.top).offset(-Adaptive.val(10))
        }
        
        closeButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ApplicationSize.paddingLeftRight)
            maker.trailing.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(Size.buttonHeight)
            maker.bottom.equalTo(-Adaptive.val(50))
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(40)
}
