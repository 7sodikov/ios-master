//
//  PdfChequeVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import PDFKit

protocol PdfChequeVCProtocol: class {
    
}

class PdfChequeVC: BaseViewController, PdfChequeViewInstaller {
    var mainView: UIView { view }
    var pdfView: PDFView!
    var saveButton: NextButton!
    var closeButton: NextButton!
    
    var presenter: PdfChequePresenterProtocol!
    
    var operationId: String!
    var isHidden: Bool!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
                                
        let data = operationId.dataFromBase64
        pdfView.document = PDFDocument(data: data!)
        
        saveButton.addTarget(self, action: #selector(saveButtonClicked), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
        
        let dashboardImage = UIImage(named: "img_share_account")!
        let dashboardButton   = UIBarButtonItem(image: dashboardImage,  style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = dashboardButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func saveButtonClicked() {
        let data = operationId.dataFromBase64
        pdfView.document = PDFDocument(data: data!)
        let document = pdfView.document
        
        guard let dataPdf = document?.dataRepresentation() else { return }

        let activityController = UIActivityViewController(activityItems: [dataPdf], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true)
    }
    
    @objc private func closeButtonClicked() {
        self.dismiss(animated: false, completion: nil)
    }
}

extension PdfChequeVC: PdfChequeVCProtocol {
    
}
