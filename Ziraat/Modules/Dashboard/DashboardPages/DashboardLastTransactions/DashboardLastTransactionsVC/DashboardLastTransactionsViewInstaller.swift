//
//  DashboardLastTransactionsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardLastTransactionsViewInstaller: ViewInstaller {
    var segmentedControl: EZSegmentedControl! { get set }
    var mainSeparatorLineView: UIView! { get set }
    var tableView: TableView! { get set }
    var noRecordLabel: UILabel! { get set }
    var noRecordImageView: UIImageView! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
}

extension DashboardLastTransactionsViewInstaller {
    func initSubviews() {
        segmentedControl = EZSegmentedControl()
        segmentedControl.addSegmentDarkVersion(with: RS.btn_money_transfers.localized())
        segmentedControl.addSegmentDarkVersion(with: RS.lbl_payments.localized())
        segmentedControl.selectedIndex = 0
        
        mainSeparatorLineView = UIView()
        mainSeparatorLineView.backgroundColor = .white
        
        tableView = TableView()
        tableView.register(DashboardLastTransactionsTableViewCell.self, forCellReuseIdentifier: String(describing: DashboardLastTransactionsTableViewCell.self))
        tableView.backgroundColor = .clear
        tableView.separatorColor = ColorConstants.mainTextColor
        tableView.tableFooterView = UIView()
        
        noRecordLabel = UILabel()
        noRecordLabel.isHidden = true
        noRecordLabel.text = RS.lbl_no_money_transfer.localized()
        noRecordLabel.textColor = .red
        noRecordLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        
        noRecordImageView = UIImageView()
        noRecordImageView.image = UIImage(named: "img_red_exclam")
        noRecordImageView.isHidden = true
        
        reloadButtonView = ReloadButtonView(frame: .zero)
    }
    
    func embedSubviews() {
        mainView.addSubview(segmentedControl)
        mainView.addSubview(mainSeparatorLineView)
        mainView.addSubview(tableView)
        mainView.addSubview(noRecordLabel)
        mainView.addSubview(noRecordImageView)
        mainView.addSubview(reloadButtonView)
    }
    
    func addSubviewsConstraints() {
        segmentedControl.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(Adaptive.val(16))
            maker.trailing.equalTo(-Adaptive.val(16))
            maker.top.equalToSuperview().offset(Adaptive.val(63))
        }
        
        mainSeparatorLineView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(1))
            maker.top.equalTo(segmentedControl.snp.bottom)
            maker.leading.trailing.equalToSuperview()
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(mainSeparatorLineView.snp.bottom)
            maker.leading.bottom.trailing.equalToSuperview()
        }
        
        noRecordLabel.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
        noRecordImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(89))
            maker.height.equalTo(Adaptive.val(89))
            maker.centerX.equalToSuperview()
            maker.bottom.equalTo(noRecordLabel.snp.top).offset(-Adaptive.val(20))
        }
        
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
