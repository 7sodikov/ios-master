//
//  DashboardCallCentreVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SafariServices

protocol DashboardCallCentreVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
}

class DashboardCallCentreVC: BaseViewController, DashboardCallCentreViewInstaller {
    var callCentreButton: TopIconButton!
    var contactFormButton: TopIconButton!
    var helpButton: TopIconButton!
    var siteMapButton: TopIconButton!
    var legalWarningButton: TopIconButton!
    var nearestBranchButton: TopIconButton!
    var shareButton: TopIconButton!
    var topStackView: UIStackView!
    var bottomStackView: UIStackView!
    var mainStackView: UIStackView!
    
    var mainView: UIView { view }
    var presenter: DashboardCallCentrePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        legalWarningButton.addTarget(self, action: #selector(legalWarningClicked(_:)), for: .touchUpInside)
        nearestBranchButton.addTarget(self, action: #selector(nearestBranchClicked(_:)), for: .touchUpInside)
        contactFormButton.addTarget(self, action: #selector(unavailableActionsClicked(_:)), for: .touchUpInside)
        siteMapButton.addTarget(self, action: #selector(unavailableActionsClicked(_:)), for: .touchUpInside)
        helpButton.addTarget(self, action: #selector(unavailableActionsClicked(_:)), for: .touchUpInside)
        callCentreButton.addTarget(self, action: #selector(makeACall(_:)), for: .touchUpInside)
        shareButton.addTarget(self, action: #selector(shareClicked), for: .touchUpInside)
    }
    
    @objc private func shareClicked() {
        let link = "https://apps.apple.com/uz/app/ziraat-mobile-uzbekistan/id1540767956"
        let vc = UIActivityViewController(activityItems: [link], applicationActivities: nil)
        self.present(vc, animated: true)
    }
    
    @objc private func makeACall(_ sender: UIButton) {
        presenter.callButtonClicked(in: navigationCtrl)
    }
    
    @objc private func legalWarningClicked(_ sender: UIButton) {
        let vc = LegalWarningRouter.createModule()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func unavailableActionsClicked(_ sender: UIButton) {
        presenter.actionButtonClicked()
    }
    
    @objc private func nearestBranchClicked(_ sender: UIButton) {
//        var url = URL(string: "")
//        if LocalizationManager.language == Language.english {
//            url = try? URLConstant.nearestBranchEnglish.asURL()
//        } else if LocalizationManager.language == Language.russian {
//            url = try? URLConstant.nearestBranchRussian.asURL()
//        } else if LocalizationManager.language == Language.uzbek {
//            url = try? URLConstant.nearestBranchUzbek.asURL()
//        } else {
//            url = try? URLConstant.nearestBranchTurkish.asURL()
//        }
//        let svc = SFSafariViewController(url: url!)
//        present(svc, animated: true, completion: nil)
        presenter.nearestBranchClicked()
    }
}

extension DashboardCallCentreVC: DashboardCallCentreVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
}
