//
//  DashboardCallCentreViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardCallCentreViewInstaller: ViewInstaller {
    var callCentreButton: TopIconButton! { get set }
    var contactFormButton: TopIconButton! { get set }
    var helpButton: TopIconButton! { get set }
    var siteMapButton: TopIconButton! { get set }
    var legalWarningButton: TopIconButton! { get set }
    var nearestBranchButton: TopIconButton! { get set }
    var shareButton: TopIconButton! { get set }
    var topStackView: UIStackView! { get set }
    var bottomStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
}

extension DashboardCallCentreViewInstaller {
    func initSubviews() {
        
        let textAndIconColor = UIColor.white
        let textFont = EZFontType.regular.sfuiDisplay(size: Adaptive.val(14))
        
        callCentreButton = TopIconButton()
        callCentreButton.setTitle(RS.nav_ttl_contact_center.localized(), for: .normal)
        callCentreButton.titleLabel?.font = textFont
        callCentreButton.setImage(UIImage(named: "img_call_centre")?.changeColor(textAndIconColor), for: .normal)
        
        contactFormButton = TopIconButton()
        contactFormButton.setTitle(RS.lbl_contact_form.localized(), for: .normal)
        contactFormButton.titleLabel?.font = textFont
        contactFormButton.setImage(UIImage(named: "img_contact_form")?.changeColor(textAndIconColor), for: .normal)
        
        helpButton = TopIconButton()
        helpButton.setTitle(RS.lbl_help_second.localized(), for: .normal)
        helpButton.titleLabel?.font = textFont
        helpButton.setImage(UIImage(named: "img_help")?.changeColor(textAndIconColor), for: .normal)
        
        siteMapButton = TopIconButton()
        siteMapButton.setTitle(RS.lbl_site_map.localized(), for: .normal)
        siteMapButton.titleLabel?.font = textFont
        siteMapButton.setImage(UIImage(named: "img_site_map")?.changeColor(textAndIconColor), for: .normal)
        
        legalWarningButton = TopIconButton()
        legalWarningButton.setImage(UIImage(named: "img_legal_warning")?.changeColor(textAndIconColor), for: .normal)
        legalWarningButton.titleLabel?.font = textFont
        legalWarningButton.setTitle(RS.lbl_legal_warning.localized(), for: .normal)
        
        nearestBranchButton = TopIconButton()
        nearestBranchButton.setTitle(RS.lbl_nearest_branch.localized(), for: .normal)
        nearestBranchButton.titleLabel?.font = textFont
        nearestBranchButton.setImage(UIImage(named: "img_nearest_branch")?.changeColor(textAndIconColor), for: .normal)
        
        shareButton = TopIconButton()
        shareButton.setTitle(RS.btn_share.localized(), for: .normal)
        shareButton.titleLabel?.font = textFont
        shareButton.setImage(UIImage(named: "btn_share1")?.changeColor(textAndIconColor), for: .normal)
        
        topStackView = UIStackView()
        topStackView.axis = .horizontal
        topStackView.spacing = 30
        
        bottomStackView = UIStackView()
        bottomStackView.axis = .horizontal
        bottomStackView.spacing = 30
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.spacing = Adaptive.val(30)
    }
    
    func embedSubviews() {
        
        topStackView.addArrangedSubview(callCentreButton)
        topStackView.addArrangedSubview(legalWarningButton)
        topStackView.addArrangedSubview(nearestBranchButton)
        
        bottomStackView.addArrangedSubview(shareButton)
//        bottomStackView.addArrangedSubview(legalWarningButton)
//        bottomStackView.addArrangedSubview(nearestBranchButton)
        
        mainStackView.addArrangedSubview(topStackView)
        mainStackView.addArrangedSubview(bottomStackView)
        mainView.addSubview(mainStackView)
    }
    
    func addSubviewsConstraints() {
        mainStackView.snp.remakeConstraints { (maker) in
//            maker.top.equalToSuperview().offset(Adaptive.val(66))
//            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
//            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(32)
}
