//
//  CallPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CallPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: CallVM { get }
}

protocol CallInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class CallPresenter: CallPresenterProtocol {
    weak var view: CallVCProtocol!
    var interactor: CallInteractorProtocol!
    var router: CallRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = CallVM()
    
    // Private property and methods
    
}

extension CallPresenter: CallInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
