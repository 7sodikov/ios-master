//
//  CallViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol CallViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var exclamationMarkImageView: UIImageView! { get set }
    var mainLabel: UILabel! { get set }
    var buttonStackView: UIStackView! { get set }
    var yesButton: NextButton! { get set }
    var noButton: NextButton! { get set }
}

extension CallViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .clear
        
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(13)
        
        exclamationMarkImageView = UIImageView()
        exclamationMarkImageView.image = UIImage(named: "img_excl_mark")
        
        mainLabel = UILabel()
        mainLabel.textColor = ColorConstants.gray
        mainLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        mainLabel.text = RS.al_msg_call_cc.localized()
        mainLabel.textAlignment = .center
        mainLabel.contentMode = .scaleToFill
        mainLabel.numberOfLines = 0
        
        buttonStackView = UIStackView()
        buttonStackView.axis = .horizontal
        buttonStackView.alignment = .fill
        buttonStackView.distribution = .fillEqually
        buttonStackView.spacing = Adaptive.val(13)
        
        yesButton = NextButton.systemButton(with: RS.btn_ok.localized(), cornerRadius: Size.buttonHeight/2)
        yesButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        
        noButton = NextButton.systemButton(with: RS.lbl_cancel.localized(), cornerRadius: Size.buttonHeight/2)
        noButton.setBackgroundColor(.white, for: .normal)
        noButton.setTitleColor(ColorConstants.gray, for: .normal)
        noButton.layer.borderWidth = Adaptive.val(1)
        noButton.layer.borderColor = ColorConstants.gray.cgColor
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(exclamationMarkImageView)
        mainView.addSubview(mainLabel)
        mainView.addSubview(buttonStackView)
        buttonStackView.addArrangedSubview(noButton)
        buttonStackView.addArrangedSubview(yesButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(32))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(32))
            maker.centerY.equalToSuperview()
        }
        
        exclamationMarkImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(80))
            maker.height.equalTo(Adaptive.val(80))
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(16))
            maker.centerX.equalTo(backView)
        }
        
        mainLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(12))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(12))
            maker.top.equalTo(exclamationMarkImageView.snp.bottom).offset(Adaptive.val(16))
        }
        
        buttonStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(20))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(20))
            maker.top.equalTo(mainLabel.snp.bottom).offset(Adaptive.val(17))
            maker.height.equalTo(Size.buttonHeight)
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(20))
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(40)
}
