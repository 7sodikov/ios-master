//
//  CallInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CallInteractorProtocol: class {
    
}

class CallInteractor: CallInteractorProtocol {
    weak var presenter: CallInteractorToPresenterProtocol!
    
}
