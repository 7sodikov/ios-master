//
//  CallVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol CallVCProtocol: class {
    
}

class CallVC: UIViewController, CallViewInstaller {
    var mainView: UIView { view }
    var backView: UIView!
    var exclamationMarkImageView: UIImageView!
    var mainLabel: UILabel!
    var buttonStackView: UIStackView!
    var yesButton: NextButton!
    var noButton: NextButton!
    
    var presenter: CallPresenterProtocol!
    
    let callCentreNumber = "781476767"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
                
        yesButton.addTarget(self, action: #selector(callButtonClicked), for: .touchUpInside)
        noButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
    }
    
    @objc func closeButtonClicked() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc private func callButtonClicked() {
        guard let url = URL(string: "tel://\(callCentreNumber)"),
            UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

extension CallVC: CallVCProtocol {
    
}
