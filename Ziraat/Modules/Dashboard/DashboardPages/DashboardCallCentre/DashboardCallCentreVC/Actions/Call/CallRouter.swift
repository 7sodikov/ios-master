//
//  CallRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol CallRouterProtocol: class {
    static func createModule() -> UIViewController
}

class CallRouter: CallRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = CallVC()
        let presenter = CallPresenter()
        let interactor = CallInteractor()
        let router = CallRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
