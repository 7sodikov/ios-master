//
//  LegalWarningPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/20/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LegalWarningPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LegalWarningVM { get }
}

protocol LegalWarningInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class LegalWarningPresenter: LegalWarningPresenterProtocol {
    weak var view: LegalWarningVCProtocol!
    var interactor: LegalWarningInteractorProtocol!
    var router: LegalWarningRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LegalWarningVM()
    
    // Private property and methods
    
}

extension LegalWarningPresenter: LegalWarningInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
