//
//  LegalWarningViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/20/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import WebKit

protocol LegalWarningViewInstaller: ViewInstaller {
    // declare your UI elements here
    var backView: UIImageView! { get set }
    var warningView: WKWebView! { get set }
}

extension LegalWarningViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
        warningView = WKWebView()
        warningView.isOpaque = false
        warningView.backgroundColor = .clear
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
        mainView.addSubview(backView)
        mainView.addSubview(warningView)
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        warningView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.leading.bottom.trailing.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
