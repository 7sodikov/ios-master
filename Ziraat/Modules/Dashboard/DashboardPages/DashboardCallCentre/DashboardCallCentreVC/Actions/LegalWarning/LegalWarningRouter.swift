//
//  LegalWarningRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/20/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LegalWarningRouterProtocol: class {
    static func createModule() -> UIViewController
}

class LegalWarningRouter: LegalWarningRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = LegalWarningVC()
        let presenter = LegalWarningPresenter()
        let interactor = LegalWarningInteractor()
        let router = LegalWarningRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
