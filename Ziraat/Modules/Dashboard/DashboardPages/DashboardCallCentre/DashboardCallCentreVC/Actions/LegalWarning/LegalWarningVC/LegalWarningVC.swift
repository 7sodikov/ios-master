//
//  LegalWarningVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/20/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import WebKit

protocol LegalWarningVCProtocol: class {
    
}

class LegalWarningVC: BaseViewController, LegalWarningViewInstaller {
    var backView: UIImageView!
    var warningView: WKWebView!
    var mainView: UIView { view }
    var presenter: LegalWarningPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
                
        self.title = RS.lbl_legal_warning.localized()
        
        var htmlText = ""
    
        if LocalizationManager.language == Language.turkish {
            htmlText = URLConstant.publicOfferTurkish
        } else if LocalizationManager.language == Language.english {
            htmlText = URLConstant.publicOfferEnglish
        } else if LocalizationManager.language == Language.russian {
            htmlText = URLConstant.publicOfferRussian
        } else {
            htmlText = URLConstant.publicOfferUzbek
        }
                
        let url = URL (string: htmlText)
        let requestObj = URLRequest(url: url!)
//        if #available(iOS 14.0, *) {
//            warningView.pageZoom = 2.0
//        } else {
//            
//        }
        warningView.load(requestObj)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: false)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
}

extension LegalWarningVC: LegalWarningVCProtocol, WKNavigationDelegate {
    
}
