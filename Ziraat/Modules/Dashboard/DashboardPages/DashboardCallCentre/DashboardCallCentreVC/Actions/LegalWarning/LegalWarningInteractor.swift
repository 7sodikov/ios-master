//
//  LegalWarningInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/20/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LegalWarningInteractorProtocol: class {
    
}

class LegalWarningInteractor: LegalWarningInteractorProtocol {
    weak var presenter: LegalWarningInteractorToPresenterProtocol!
    
}
