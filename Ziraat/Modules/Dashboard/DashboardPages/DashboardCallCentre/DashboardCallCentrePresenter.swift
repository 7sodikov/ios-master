//
//  DashboardCallCentrePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardCallCentrePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: DashboardCallCentreVM { get }
    func actionButtonClicked()
    func callButtonClicked(in navigationCtrl: Any)
    func nearestBranchClicked()
}

protocol DashboardCallCentreInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class DashboardCallCentrePresenter: DashboardCallCentrePresenterProtocol {
    weak var view: DashboardCallCentreVCProtocol!
    var interactor: DashboardCallCentreInteractorProtocol!
    var router: DashboardCallCentreRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = DashboardCallCentreVM()
    
    // Private property and methods
    
    func actionButtonClicked() {
        router.navigateToErrorPage(navigationCtrl: view.navigationCtrl)
    }
    
    func callButtonClicked(in navigationCtrl: Any) {
        router.openMakeCallVC(in: navigationCtrl)
    }
    
    func nearestBranchClicked() {
        router.navigateToBankOnMap(in: view)
    }
}

extension DashboardCallCentrePresenter: DashboardCallCentreInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
