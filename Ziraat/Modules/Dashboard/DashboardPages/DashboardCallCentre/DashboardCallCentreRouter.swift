//
//  DashboardCallCentreRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardCallCentreRouterProtocol: class {
    static func createModule() -> BaseMainSwipableViewController
    func navigateToErrorPage(navigationCtrl: Any)
    func openMakeCallVC(in navigationCtrl: Any)
    func navigateToBankOnMap(in navigationCtrl: Any)
}

class DashboardCallCentreRouter: DashboardCallCentreRouterProtocol {
   
    static func createModule() -> BaseMainSwipableViewController {
        let vc = DashboardCallCentreVC()
        let presenter = DashboardCallCentrePresenter()
        let interactor = DashboardCallCentreInteractor()
        let router = DashboardCallCentreRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToErrorPage(navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as! UIViewController
        let statementVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func openMakeCallVC(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as! UIViewController
        let statementVC = CallRouter.createModule()
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func navigateToBankOnMap(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = BankOnMapRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
}
