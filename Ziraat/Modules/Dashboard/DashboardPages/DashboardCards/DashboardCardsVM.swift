//
//  DashboardCardsVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum CardTableCellType {
    case branchName
    case balance
    case accountNumber
    
    var title: String {
        switch self {
        case .branchName: return RS.lbl_branch_name.localized()
        case .balance: return RS.lbl_balance.localized()
        case .accountNumber: return RS.lbl_account_number2.localized()
        }
    }
}

class DashboardCardVM {
    var card: CardResponse
    var balance: CardBalanceResponse?
    
    var cardBGImageURL: String {
        return card.additions?.imgX3 ?? ""
    }
    
    var cardTitle: String? {
        return card.title != nil ? card.title : card.owner
    }
    
    var expiry: String {
        let date = card.expire?.date(fromFormat: "yyMM")
        let dateStr = date?.string(for: "MM/yy")
        return dateStr ?? ""
//        return "\(RS.lbl_exp.localized()): \(dateStr ?? "")"
    }
    
    var currency: String {
        let curType = CurrencyType.type(for: balance?.currency)
        return curType?.data.label ?? ""
    }
    
    var balanceStr: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(balance?.balance ?? 0)/100
        let balanceNumber = NSNumber(value: b)
        var balanceStr = formatter.string(from: balanceNumber) ?? ""
        if balanceStr.count > 0 {
            balanceStr = "\(balanceStr) \(currency)"
        }
        if UDManager.hideBalance{
            return "* * * * " + currency
        }
        return balanceStr
    }
    
    init(_ card: CardResponse, _ balance: CardBalanceResponse?) {
        self.card = card
        self.balance = balance
    }
}

class DashboardCardsVM {
    var cards: [CardResponse] = []
    var cardBalances: [CardBalanceResponse] = []
    let cardTableCells: [CardTableCellType] = [.branchName, .balance, .accountNumber]
    
    func cardVM(at index: Int) -> DashboardCardVM {
        var balance: CardBalanceResponse?
        let card = cards[index]
        for cardBalance in cardBalances {
            if cardBalance.cardId == card.cardId {
                balance = cardBalance
                break
            }
        }
        
        return DashboardCardVM(card, balance)
    }
    
    func tableCellValue(for cardIndex: Int, cellType: CardTableCellType) -> String? {
        guard cards.count > cardIndex else {
            return nil
        }
        
        let cardVM = self.cardVM(at: cardIndex)
        
        switch cellType {
        case .branchName:
            return cardVM.card.branchName
        case .balance:
            return cardVM.balanceStr
        case .accountNumber:
            return cardVM.card.account
        }
    }
}
