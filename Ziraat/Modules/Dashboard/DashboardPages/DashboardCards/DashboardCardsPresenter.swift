//
//  DashboardCardsePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardCardsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: DashboardCardsVM { get }
    func viewDidLoad()
    func btnClicked(_ button: DashboardCardsVC.Button)
    func cardClicked(_ index: Int)
}

protocol DashboardCardsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>)
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>)
}

class DashboardCardsPresenter: DashboardCardsPresenterProtocol {
    weak var view: DashboardCardsVCProtocol!
    var interactor: DashboardCardsInteractor!
    var router: DashboardCardsRouter!
    var delete : DashboardCardsActionProtocol?
    // VIEW -> PRESENTER
    private(set) var viewModel = DashboardCardsVM()
    
    func viewDidLoad() {
        showReloadButtonForCardsDownload(true, animateReloadButton: true)
        interactor.getAllCards()
        interactor.getAllCardBalances()
    }
    
    // Private property and methods
    private func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool) {
        var show = show
        if viewModel.cards.count == 0 {
            show = true
        }
        view.showReloadButtonForCardsDownload(show, animateReloadButton: animateReloadButton, isCardsEmpty: show)
    }
    func btnClicked(_ button: DashboardCardsVC.Button) {
        if case .payment = button {
            let module = PaymentsMenuRouter().viewController
            self.router.openModule(view as Any, module)
        }
        if case .transfer = button {
            let module = MoneyTransfersRouter(for: nil, historyItem: nil).viewController
            self.router.openModule(view as Any, module)
        }
    }
    
    func cardClicked(_ index: Int) {
        let cardVM = self.viewModel.cardVM(at: index)
        self.delete?.openCardDetails(cardVM)
    }
        
}

extension DashboardCardsPresenter: DashboardCardsInteractorToPresenterProtocol {    
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cards = result.data.cards
            if viewModel.cards.count == 0 {
                showReloadButtonForCardsDownload(true, animateReloadButton: false)
            } else {
                showReloadButtonForCardsDownload(false, animateReloadButton: false)
            }
        case let .failure(error):
            showReloadButtonForCardsDownload(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cardBalances = result.data.cards ?? []
            view.reloadViewForBalanceDownload()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
