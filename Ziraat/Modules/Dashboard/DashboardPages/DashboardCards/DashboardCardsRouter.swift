//
//  DashboardCardsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardCardsActionProtocol: class {
    func openCardDetails(_ card: DashboardCardVM)
}

protocol DashboardCardsRouterProtocol: class {
    static func createModule(_ delegate : DashboardCardsActionProtocol) -> BaseMainSwipableViewController
    func openModule(_ view : Any, _ module : Any)
}

class DashboardCardsRouter: DashboardCardsRouterProtocol {
    static func createModule(_ delegate : DashboardCardsActionProtocol) -> BaseMainSwipableViewController {
        let vc = DashboardCardsVC()
        let presenter = DashboardCardsPresenter()
        let interactor = DashboardCardsInteractor()
        let router = DashboardCardsRouter()
        presenter.delete = delegate
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func openModule(_ view: Any, _ module: Any) {
        if let controller = view as? UIViewController {
            if let module = module as? UIViewController{
                controller.navigationController?.pushViewController(module, animated: true)
            }
        }
    }
}
