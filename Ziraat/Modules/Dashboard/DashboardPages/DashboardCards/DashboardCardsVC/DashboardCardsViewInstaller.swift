//
//  DashboardCardsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardCardsViewInstaller: ViewInstaller {
    var reloadButtonView: ReloadButtonView! { get set }
    var containerView: UIView! { get set }
    var collectionLayout: UPCarouselFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
    var previousButton: UIButton! { get set }
    var nextButton: UIButton! { get set }
    var redInfoIconCoverView: UIView! { get set }
    var redInfoImageView: UIImageView! { get set }
    var redInfoImageTitleLabel: UILabel! { get set }
    var tableView: AutomaticHeightTableView! { get set }
    var buttonsStackView: UIStackView! { get set }
    var paymentsBtn: NextButton! { get set }
    var transferBtn: NextButton! { get set }
}

extension DashboardCardsViewInstaller {
    func initSubviews() {
        reloadButtonView = ReloadButtonView(frame: .zero)
        
        containerView = UIView()
        containerView.backgroundColor = .clear
        
        collectionLayout = UPCarouselFlowLayout()
        collectionLayout.scrollDirection = .horizontal
        collectionLayout.itemSize = DashboardCardsViewSize.cellSize
        let space =
            (UIScreen.main.bounds.width - DashboardCardsViewSize.cellSize.width)/2
        collectionLayout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: space)
        collectionLayout.sideItemScale = 1
        collectionLayout.sideItemAlpha = 1
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.register(DashboardCardCollectionCell.self,
                                forCellWithReuseIdentifier: String(describing: DashboardCardCollectionCell.self))
        collectionView.collectionViewLayout = collectionLayout
        
        previousButton = UIButton()
        previousButton.setImage(UIImage(named: "btn_prev")?.changeColor(.white), for: .normal)
        previousButton.isHidden = true
        
        nextButton = UIButton()
        nextButton.setImage(UIImage(named: "btn_next")?.changeColor(.white), for: .normal)
        nextButton.isHidden = true
        
        redInfoIconCoverView = UIView()
        redInfoIconCoverView.backgroundColor = .clear
        redInfoIconCoverView.isHidden = true
        
        redInfoImageView = UIImageView()
        redInfoImageView.image = UIImage(named: "icon_info")
        
        redInfoImageTitleLabel = UILabel()
        redInfoImageTitleLabel.font = .gothamNarrow(size: 18, .bold) //EZFontType.regular.sfProDisplay(size: Adaptive.val(18))
        redInfoImageTitleLabel.text = RS.lbl_no_card.localized()
        redInfoImageTitleLabel.textColor = .black
        redInfoImageTitleLabel.textAlignment = .center
        
        tableView = AutomaticHeightTableView()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0,
                                                left: DashboardCardsViewSize.TableView.separatorInset,
                                                bottom: 0,
                                                right: DashboardCardsViewSize.TableView.separatorInset)
        tableView.backgroundColor = .clear
        tableView.separatorColor = ColorConstants.grayishWhite

        tableView.register(DashboardCardsTableViewCell.self, forCellReuseIdentifier: String(describing: DashboardCardsTableViewCell.self))
//        tableView.frame.size = tableView.contentSize
        
        buttonsStackView = UIStackView()
        buttonsStackView.spacing = Adaptive.val(17)
        buttonsStackView.alignment = .fill
        buttonsStackView.distribution = .fillEqually
        
        paymentsBtn = NextButton.systemButton(with: RS.btn_payment.localized(), cornerRadius: Adaptive.val(23))
        paymentsBtn.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        paymentsBtn.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        paymentsBtn.titleLabel?.font = .gothamNarrow(size: 14, .bold) //EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        transferBtn = NextButton.systemButton(with: RS.btn_money_transfers.localized(), cornerRadius: Adaptive.val(23))
        transferBtn.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        transferBtn.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        transferBtn.titleLabel?.font = .gothamNarrow(size: 14, .bold) //EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
    }
    
    func embedSubviews() {
        mainView.addSubview(reloadButtonView)
        mainView.addSubview(containerView)
        containerView.addSubview(collectionView)
        containerView.addSubview(previousButton)
        containerView.addSubview(nextButton)
        containerView.addSubview(tableView)
        containerView.addSubview(buttonsStackView)
        
        containerView.addSubview(redInfoIconCoverView)
        redInfoIconCoverView.addSubview(redInfoImageView)
        redInfoIconCoverView.addSubview(redInfoImageTitleLabel)
        
        buttonsStackView.addArrangedSubview(paymentsBtn)
        buttonsStackView.addArrangedSubview(transferBtn)
    }
    
    func addSubviewsConstraints() {
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
        
        containerView.snp.remakeConstraints { (maker) in
            maker.left.right.bottom.equalToSuperview()
            maker.top.equalToSuperview().offset(Adaptive.val(63))
        }
        
        collectionView.snp.remakeConstraints { (maker) in
            maker.top.left.right.equalToSuperview()
            maker.height.equalTo(DashboardCardsViewSize.cellSize.height)
        }
        
        previousButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView)
            maker.left.equalToSuperview()
            maker.width.equalTo(DashboardCardsViewSize.NextCardButton.width)
            maker.height.equalTo(collectionView)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView)
            maker.right.equalToSuperview()
            maker.width.height.equalTo(previousButton)
        }
        
        redInfoIconCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView)
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(collectionView)
        }
        
        redInfoImageView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(ApplicationSize.paddingLeftRight)
            maker.centerX.equalToSuperview()
            maker.width.equalTo(Adaptive.val(90))
            maker.height.equalTo(Adaptive.val(90))
        }
        
        redInfoImageTitleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(redInfoImageView.snp.bottom).offset(Adaptive.val(10))
            maker.left.right.equalTo(redInfoIconCoverView)
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView.snp.bottom).offset(Adaptive.val(45))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(14))
            maker.leading.equalToSuperview().offset(Adaptive.val(14))
            maker.bottom.equalTo(buttonsStackView.snp.top).offset(-Adaptive.val(20))
        }
        
        buttonsStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(Adaptive.val(16))
            maker.trailing.equalTo(-Adaptive.val(16))
            maker.bottom.equalTo(-Adaptive.val(20))
        }
        
        paymentsBtn.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(47))
        }
        
        transferBtn.snp.remakeConstraints { (maker) in
            maker.height.equalTo(paymentsBtn)
        }
    }
}

struct DashboardCardsViewSize {
    static let leftRightPadding = Adaptive.val(20)
    static let cellWidth : CGFloat = UIScreen.main.bounds.size.width - 80
    static let cellSize = CGSize(width: cellWidth, height: cellWidth/1.67)
    
    static let cellWidthLittle : CGFloat = (UIScreen.main.bounds.size.width - 80) / 2
    static let cellSizeLittle = CGSize(width: cellWidthLittle, height: cellWidthLittle/1.67)
    
    struct NextCardButton {
        static let width : CGFloat = 50.0 //Adaptive.val(50)
    }
    
    struct TableView {
        static let separatorInset = Adaptive.val(15)
    }
}
