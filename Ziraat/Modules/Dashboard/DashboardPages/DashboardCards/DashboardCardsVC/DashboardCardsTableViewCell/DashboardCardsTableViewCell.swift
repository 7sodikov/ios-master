//
//  DashboardCardsTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class DashboardCardsTableViewCell: UITableViewCell, DashboardCardsTableViewCellInstaller {
    var mainView: UIView { contentView }
    var actionsLabel: UILabel!
    var balanceLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func setup(with cellType: CardTableCellType, value: String?) -> DashboardCardsTableViewCell {
        actionsLabel.text = cellType.title
        balanceLabel.text = value
        return self
    }
}
