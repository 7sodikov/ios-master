//
//  DashboardCardsTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardCardsTableViewCellInstaller: ViewInstaller {
    var actionsLabel: UILabel! { get set }
    var balanceLabel: UILabel! { get set }
}

extension DashboardCardsTableViewCellInstaller {
    func initSubviews() {
        actionsLabel = UILabel()
        actionsLabel.textColor = .white
        actionsLabel.font = .gothamNarrow(size: 16, .book) //EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        actionsLabel.numberOfLines = 2
        
        balanceLabel = UILabel()
        balanceLabel.textColor = .white
        balanceLabel.font = .gothamNarrow(size: 15, .bold) //EZFontType.regular.sfuiDisplay(size: Adaptive.val(16))
        balanceLabel.textAlignment = .right
        balanceLabel.numberOfLines = 3
    }
    
    func embedSubviews() {
        mainView.addSubview(actionsLabel)
        mainView.addSubview(balanceLabel)
    }
    
    func addSubviewsConstraints() {        
        actionsLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalToSuperview()
            maker.leading.equalTo(DashboardCardsViewSize.TableView.separatorInset)
        }
        
        balanceLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalToSuperview()
            maker.leading.equalTo(actionsLabel.snp.trailing).offset(15)
            maker.trailing.equalTo(-DashboardCardsViewSize.TableView.separatorInset)
        }
    }
}

fileprivate struct Size {
    
}
