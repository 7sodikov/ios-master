//
//  DashboardCardsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardCardsVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool, isCardsEmpty: Bool)
    func reloadViewForBalanceDownload()
}

class DashboardCardsVC: BaseViewController, DashboardCardsViewInstaller {
    
    enum Button: Int {
        case payment
        case transfer
    }
    
    var mainView: UIView { view }
    var reloadButtonView: ReloadButtonView!
    var containerView: UIView!
    var collectionLayout: UPCarouselFlowLayout!
    var collectionView: UICollectionView!
    var previousButton: UIButton!
    var nextButton: UIButton!
    var redInfoIconCoverView: UIView!
    var redInfoImageView: UIImageView!
    var redInfoImageTitleLabel: UILabel!
    var tableView: AutomaticHeightTableView!
    var buttonsStackView: UIStackView!
    var transferBtn: NextButton!
    var paymentsBtn: NextButton!
    
    var presenter: DashboardCardsPresenter!
    
    private var currentCardPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadButtonClicked(_:)), for: .touchUpInside)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        previousButton.addTarget(self, action: #selector(previousNextCardClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(previousNextCardClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
        
        paymentsBtn.tag = Button.payment.rawValue
        transferBtn.tag = Button.transfer.rawValue
        
        paymentsBtn.addTarget(self, action: #selector(paymentsClicked), for: .touchUpInside)
        transferBtn.addTarget(self, action: #selector(transferClicked), for: .touchUpInside)
        
    }
    
    @objc private func reloadButtonClicked(_ sender: UIButton) {
        presenter.viewDidLoad()
    }
    
    @objc func paymentsClicked(){
        self.presenter.btnClicked(.payment)
    }
    
    @objc func transferClicked(){
        self.presenter.btnClicked(.transfer)
    }
    
    @objc private func previousNextCardClicked(_ sender: UIButton) {
        if sender == previousButton {
            currentCardPage -= 1
        } else { // nextButton
            currentCardPage += 1
        }
        
        collectionView.scrollToItem(at: IndexPath(item: currentCardPage, section: 0),
                                    at: .centeredHorizontally,
                                    animated: true)
        setPreviousNextButtonState(for: currentCardPage)
    }
    
    private func setPreviousNextButtonState(for page: Int) {
        if presenter.viewModel.cards.count < 2 {
            previousButton.isHidden = true
            nextButton.isHidden = true
        } else {
            previousButton.isHidden = (page == 0)
            nextButton.isHidden = (page == presenter.viewModel.cards.count - 1)
        }
        self.tableView.reloadData()
  }
    override func viewWillAppear(_ animated: Bool) {
        reloadViewForBalanceDownload()
    }
}

extension DashboardCardsVC: DashboardCardsVCProtocol {
    func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool, isCardsEmpty: Bool) {
//        reloadButtonView.isHidden = !show
//        reloadButtonView.animateIndicator = animateReloadButton
//
//        containerView.isHidden = show
//        setPreviousNextButtonState(for: currentCardPage)
//        collectionView.reloadData()
//        tableView.reloadData()
        
        redInfoIconCoverView.isHidden = !isCardsEmpty
        
        if isCardsEmpty {
            reloadButtonView.isHidden = true
            containerView.isHidden = false
            collectionView.isHidden = true
            tableView.isHidden = true
            transferBtn.isHidden = true
            paymentsBtn.isHidden = true
        } else {
            reloadButtonView.isHidden = !show
            reloadButtonView.animateIndicator = animateReloadButton
            containerView.isHidden = show
            setPreviousNextButtonState(for: currentCardPage)
            collectionView.reloadData()
            tableView.reloadData()
            transferBtn.isHidden = false
            paymentsBtn.isHidden = false
            collectionView.isHidden = false
            tableView.isHidden = false
        }
    }
    
    func reloadViewForBalanceDownload() {
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    @objc func showOrHideBalanceClicked(){
        let val = UDManager.hideBalance
        UDManager.hideBalance = !val
        reloadViewForBalanceDownload()
    }
}

extension DashboardCardsVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.viewModel.cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(DashboardCardCollectionCell.self)", for: indexPath) as! DashboardCardCollectionCell
        let cardVM = presenter.viewModel.cardVM(at: indexPath.row)
        cell.cardView.eyeIconContainer.addTarget(self, action: #selector(showOrHideBalanceClicked), for: .touchUpInside)
        return cell.setup(with: cardVM)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionView {
            currentCardPage = collectionView.mostCenteredCellIndexPath()!.row
            setPreviousNextButtonState(for: currentCardPage)
        } else { // tableView
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter.cardClicked(indexPath.row)
    }
}

extension DashboardCardsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.cardTableCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = String(describing: DashboardCardsTableViewCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! DashboardCardsTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        let cellType = presenter.viewModel.cardTableCells[indexPath.row]
        let cellValue = presenter.viewModel.tableCellValue(for: currentCardPage,
                                                           cellType: cellType)
        return cell.setup(with: cellType, value: cellValue)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(70)
    }
}
