//
//  DashboardCardCollectionCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class DashboardCardCollectionCell: UICollectionViewCell, DashboardCardCollectionCellViewInstaller {
    var cardView: CardView!
    
    var mainView: UIView { contentView }
//    var bgImageView: UIImageView!
//    var cardNumberLabel: UILabel!
//    var balanceTitleLabel: UILabel!
//    var balanceLabel: UILabel!
//    var balanceStackView: UIStackView!
//    var balanceHorizontalAlignStackView: UIStackView!
//    var eyeIconButton: UIButton!
//    var reloadButton: UIButton!
//    var holderNameLabel: UILabel!
//    var expiryDateLabel: UILabel!
//    var nameAndExpiryDateContainerStackView: UIStackView!
//    var expiryDateHorizontalAlignStackView: UIStackView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cardVM: DashboardCardVM) -> DashboardCardCollectionCell {
        cardView.dashboardCard = cardVM
//        cardView..kf.setImage(with: URL(string: cardVM.cardBGImageURL), placeholder: nil)
//        cardNumberLabel.text = cardVM.card.pan
//        expiryDateLabel.text = cardVM.expiry
//        holderNameLabel.text = cardVM.card.owner
//        balanceLabel.text = cardVM.balanceStr
//
//        let eyeImageName = UDManager.hideBalance ? "btn_eye" : "img_eye"
//        let icon = UIImage(named: eyeImageName)?.withRenderingMode(.alwaysTemplate)
//        eyeIconButton.setImage(icon, for: .normal)
        
        return self
    }
}
