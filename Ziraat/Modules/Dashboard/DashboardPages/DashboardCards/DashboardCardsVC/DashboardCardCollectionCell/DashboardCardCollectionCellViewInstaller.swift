//
//  DashboardCardCollectionCellViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardCardCollectionCellViewInstaller: ViewInstaller {
//    var bgImageView: UIImageView! { get set }
//    var cardNumberLabel: UILabel! { get set }
//    var balanceTitleLabel: UILabel! { get set }
//    var balanceLabel: UILabel! { get set }
//    var balanceStackView: UIStackView! { get set }
//    var balanceHorizontalAlignStackView: UIStackView! { get set }
//    var eyeIconButton: UIButton! { get set }
//    var reloadButton: UIButton! { get set }
//    var holderNameLabel: UILabel! { get set }
//    var expiryDateLabel: UILabel! { get set }
//    var nameAndExpiryDateContainerStackView: UIStackView! { get set }
//    var expiryDateHorizontalAlignStackView: UIStackView! { get set }
    var cardView : CardView! {get set}
    
}

extension DashboardCardCollectionCellViewInstaller {
    func initSubviews() {
        cardView = CardView(frame: .zero)
//        mainView.layer.cornerRadius = Adaptive.val(14)
//        mainView.layer.masksToBounds = true
//
//        bgImageView = UIImageView()
//        bgImageView.layer.cornerRadius = Adaptive.val(10)
//        bgImageView.backgroundColor = .lightGray
//
//        let font = EZFontType.regular.sfProDisplay(size: Adaptive.val(12))
//        cardNumberLabel = UILabel()
//        cardNumberLabel.textColor = .white
//        cardNumberLabel.font = font
//
//        balanceTitleLabel = UILabel()
//        balanceTitleLabel.text = "\(RS.lbl_balance.localized()):"
//        balanceTitleLabel.textColor = .white
//        balanceTitleLabel.font = font
//
//        balanceLabel = UILabel()
//        balanceLabel.textColor = .white
//        balanceLabel.font = font
//
//        balanceStackView = UIStackView()
//        balanceStackView.axis = .horizontal
//        balanceStackView.distribution = .fill
//        balanceStackView.spacing = Adaptive.val(3)
//
//        balanceHorizontalAlignStackView = UIStackView()
//        balanceHorizontalAlignStackView.axis = .vertical
//        balanceHorizontalAlignStackView.alignment = .leading
//
//        eyeIconButton = UIButton()
//        eyeIconButton.tintColor = .white
//        eyeIconButton.setImage(UIImage(named: "img_eye"), for: .normal)
//
//        holderNameLabel = UILabel()
//        holderNameLabel.textColor = .white
//        holderNameLabel.font = font
//        holderNameLabel.numberOfLines = 2
//
//        expiryDateLabel = UILabel()
//        expiryDateLabel.textColor = .white
//        expiryDateLabel.font = font
//
//        nameAndExpiryDateContainerStackView = UIStackView()
//        nameAndExpiryDateContainerStackView.axis = .horizontal
//        nameAndExpiryDateContainerStackView.distribution = .fill
//        nameAndExpiryDateContainerStackView.alignment = .top
//
//        expiryDateHorizontalAlignStackView = UIStackView()
//        expiryDateHorizontalAlignStackView.axis = .vertical
//        expiryDateHorizontalAlignStackView.alignment = .trailing
    }
    
    func embedSubviews() {
        mainView.addSubview(cardView)
//        mainView.addSubview(bgImageView)
//        mainView.addSubview(cardNumberLabel)
//
//
//        balanceStackView.addArrangedSubview(balanceTitleLabel)
//        balanceStackView.addArrangedSubview(balanceLabel)
//
//        balanceHorizontalAlignStackView.addArrangedSubview(balanceStackView)
//        mainView.addSubview(balanceHorizontalAlignStackView)
//
//        expiryDateHorizontalAlignStackView.addArrangedSubview(expiryDateLabel)
//        nameAndExpiryDateContainerStackView.addArrangedSubview(holderNameLabel)
//        nameAndExpiryDateContainerStackView.addArrangedSubview(expiryDateHorizontalAlignStackView)
//        mainView.addSubview(nameAndExpiryDateContainerStackView)
//        mainView.addSubview(eyeIconButton)
    }
    
    func addSubviewsConstraints() {
        
        cardView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
//
//        eyeIconButton.snp.remakeConstraints { (maker) in
//            maker.centerY.equalTo(balanceLabel)
//            maker.trailing.equalToSuperview().offset(-Adaptive.val(5))
//            maker.height.width.equalTo(Adaptive.val(35))
//        }
//
//        cardNumberLabel.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(DashboardCardsViewSize.cellSize.height * 0.4)
//            maker.leading.equalTo(Size.padding)
//            maker.trailing.equalTo(-Size.padding)
//        }
//
//        balanceHorizontalAlignStackView.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(cardNumberLabel.snp.bottom).offset(Adaptive.val(3))
//            maker.leading.equalTo(Size.padding)
//            maker.trailing.equalTo(-Size.padding)
//        }
//
//        nameAndExpiryDateContainerStackView.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(balanceHorizontalAlignStackView.snp.bottom).offset(Adaptive.val(8))
//            maker.leading.trailing.equalTo(cardNumberLabel)
//        }
    }
}

fileprivate struct Size {
    static let padding = Adaptive.val(10)
    static let interItemSpace = Adaptive.val(20)
    static let fieldTitleFontSize = Adaptive.val(17)
}
