//
//  DashboardCardsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardCardsInteractorProtocol: class {
    func getAllCards()
    func getAllCardBalances()
}

class DashboardCardsInteractor: DashboardCardsInteractorProtocol {
    weak var presenter: DashboardCardsInteractorToPresenterProtocol!
    
    func getAllCards() {
        NetworkService.Card.cardsList { [weak self] (result) in
            self?.presenter.didGetAllCards(with: result)
        }
    }
    
    func getAllCardBalances() {
        NetworkService.Card.cardBalance { [weak self] (result) in
            self?.presenter.didGetAllCardBalances(with: result)
        }
    }
}
