//
//  DashboardCurrencyPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardCurrencyPresenterProtocol: class {
    var viewModel: DashboardCurrenciesVM { get }
    func viewDidLoad()
}

protocol DashboardCurrencyInteractorToPresenterProtocol: class {
    func didGetMainCurrencyList(with response: ResponseResult<CurrencyListResponse, AppError>)
    func didGetAllCurrencyList(with response: ResponseResult<CurrencyListResponse, AppError>)
    func didGetMainExchangeRates(with response: ResponseResult<ExchangeRatesResponse, AppError>)
    func didGetAllExchangeRatesList(with response: ResponseResult<ExchangeRatesResponse, AppError>)
}

class DashboardCurrencyPresenter: DashboardCurrencyPresenterProtocol {
    weak var view: DashboardCurrencyVCProtocol!
    var interactor: DashboardCurrencyInteractorProtocol!
    var router: DashboardCurrencyRouterProtocol!
    
    private(set) var viewModel = DashboardCurrenciesVM()
    
    func viewDidLoad() {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.getMainExchangeRates()
        interactor.getMainCurrencyList()
    }
    
    // Private property and methods
    private var responsesCount = 0
    private var successCount = 0
    private func processResponseCount(isSuccess: Bool) {
        responsesCount += 1
        
        if isSuccess {
            successCount += 1
        }
        
        if responsesCount == 2 {
            if successCount == 2 {
                viewModel.didReceiveAllResponses()
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(true, animateReloadButton: false)
            }
        }
    }
    
    private func processMainResponseCount(isSuccess: Bool) {
        responsesCount += 1
        
        if isSuccess {
            successCount += 1
        }
        
        if responsesCount == 2 {
            if successCount == 2 {
                viewModel.didReceiveMainResponses()
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(true, animateReloadButton: false)
            }
        }
    }
}

extension DashboardCurrencyPresenter: DashboardCurrencyInteractorToPresenterProtocol {
    func didGetMainCurrencyList(with response: ResponseResult<CurrencyListResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.currencies = result.data.currencies ?? []
            processMainResponseCount(isSuccess: true)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            processMainResponseCount(isSuccess: false)
        }
    }
    
    func didGetAllCurrencyList(with response: ResponseResult<CurrencyListResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.currencies = result.data.currencies ?? []
            processResponseCount(isSuccess: true)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            processResponseCount(isSuccess: false)
        }
    }
    
    func didGetMainExchangeRates(with response: ResponseResult<ExchangeRatesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.exchangeRates = result.data.exchangeRates ?? []
            processMainResponseCount(isSuccess: true)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            processMainResponseCount(isSuccess: false)
        }
    }
    
    func didGetAllExchangeRatesList(with response: ResponseResult<ExchangeRatesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.exchangeRates = result.data.exchangeRates ?? []
            processResponseCount(isSuccess: true)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            processResponseCount(isSuccess: false)
        }
    }
    
}
