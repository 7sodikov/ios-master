//
//  DashboardCurrencyViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardCurrencyViewInstaller: ViewInstaller {
    var reloadButtonView: ReloadButtonView! { get set }
    var tableView: UITableView! { get set }
}

extension DashboardCurrencyViewInstaller {
    func initSubviews() {
        reloadButtonView = ReloadButtonView(frame: .zero)
        
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0,
                                                left: DashboardCardsViewSize.TableView.separatorInset,
                                                bottom: 0,
                                                right: DashboardCardsViewSize.TableView.separatorInset)

        tableView.register(DashboardCurrencyTableViewCell.self, forCellReuseIdentifier: String(describing: DashboardCurrencyTableViewCell.self))
        tableView.backgroundColor = .clear
        tableView.separatorColor = ColorConstants.lightestGray
    }
    
    func embedSubviews() {
        mainView.addSubview(reloadButtonView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.leading.bottom.trailing.equalToSuperview()
            maker.top.equalToSuperview().offset(Adaptive.val(63))
        }
    }
}

struct DashboardCurrencyViewSize {
    static let rowHeight = Adaptive.val(90)
}
