//
//  DashboardCurrencyVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardCurrencyVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class DashboardCurrencyVC: BaseViewController, DashboardCurrencyViewInstaller {
    var mainView: UIView { view }
    var reloadButtonView: ReloadButtonView!
    var tableView: UITableView!
    
    var presenter: DashboardCurrencyPresenterProtocol!
    
    fileprivate let cellid: String = "\(DashboardCurrencyTableViewCell.self)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadButtonClicked(_:)), for: .touchUpInside)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        presenter.viewDidLoad()
    }
    
    @objc private func reloadButtonClicked(_ sender: UIButton) {
        presenter.viewDidLoad()
    }
}

extension DashboardCurrencyVC: DashboardCurrencyVCProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
}

extension DashboardCurrencyVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.currencies.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! DashboardCurrencyTableViewCell
        cell.backgroundColor = .clear
        let currencyVM = presenter.viewModel.currenciesVM[indexPath.row]
        return cell.setup(with: currencyVM)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return DashboardCurrencyViewSize.rowHeight
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
    }
}
