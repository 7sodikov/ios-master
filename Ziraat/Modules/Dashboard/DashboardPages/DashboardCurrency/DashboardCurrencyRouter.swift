//
//  DashboardCurrencyRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardCurrencyRouterProtocol: class {
    static func createModule() -> BaseMainSwipableViewController
}

class DashboardCurrencyRouter: DashboardCurrencyRouterProtocol {
    static func createModule() -> BaseMainSwipableViewController {
        let vc = DashboardCurrencyVC()
        let presenter = DashboardCurrencyPresenter()
        let interactor = DashboardCurrencyInteractor()
        let router = DashboardCurrencyRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
