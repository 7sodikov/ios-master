//
//  DashboardCurrencyInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardCurrencyInteractorProtocol: class {
    func getMainCurrencyList()
    func getAllCurrencyList()
    func getMainExchangeRates()
    func getAllExchangeRatesList()
}

class DashboardCurrencyInteractor: DashboardCurrencyInteractorProtocol {
    
    weak var presenter: DashboardCurrencyInteractorToPresenterProtocol!
    
    func getMainCurrencyList() {
        NetworkService.Info.getMainCurrencyList() { [weak self] (result) in
            self?.presenter.didGetMainCurrencyList(with: result)
        }
    }
    
    func getAllCurrencyList() {
        NetworkService.Info.getAllCurrencyList() { [weak self] (result) in
            self?.presenter.didGetAllCurrencyList(with: result)
        }
    }
    
    func getMainExchangeRates() {
        NetworkService.Info.getMainExchangeRates() { [weak self] (result) in
            self?.presenter.didGetMainExchangeRates(with: result)
        }
    }
    
    func getAllExchangeRatesList() {
        NetworkService.Info.getAllExchangeRatesList() { (result) in
            self.presenter.didGetAllExchangeRatesList(with: result)
        }
    }
}
