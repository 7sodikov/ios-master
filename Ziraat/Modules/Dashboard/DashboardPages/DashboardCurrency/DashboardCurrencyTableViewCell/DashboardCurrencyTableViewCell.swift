//
//  DashboardCurrencyTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import Kingfisher

class DashboardCurrencyTableViewCell: UITableViewCell, DashboardCurrencyTableViewCellInstaller {
    var mainView: UIView { contentView }
    var currencyLabel: UILabel!
    var currencyStackView: UIStackView!
    var flagImageView: UIImageView!
    var buyLabel: UILabel!
    var buyBalanceLabel: UILabel!
    var buyStackView: UIStackView!
    var sellLabel: UILabel!
    var sellBalanceLabel: UILabel!
    var sellStackView: UIStackView!
    var cbLabel: UILabel!
    var cbBalanceLabel: UILabel!
    var cbStackView: UIStackView!
    var currenciesStackView: UIStackView!
    var currenciesWithFlagStackView: UIStackView!
    var mainStackView: UIStackView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with currencyVM: DashboardCurrencyVM) -> DashboardCurrencyTableViewCell {
        currencyLabel.text = currencyVM.currency.codeLabel
        flagImageView.kf.setImage(with: URL(string: currencyVM.flagImage ?? ""), placeholder: nil)
        buyBalanceLabel.text = currencyVM.buyCourse
        sellBalanceLabel.text = currencyVM.sellCourse
        cbBalanceLabel.text = currencyVM.cbCourse
        return self
    }
}
