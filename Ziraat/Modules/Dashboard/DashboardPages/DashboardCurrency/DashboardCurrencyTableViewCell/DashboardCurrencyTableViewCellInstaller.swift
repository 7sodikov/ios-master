//
//  DashboardCurrencyTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardCurrencyTableViewCellInstaller: ViewInstaller {
    var currencyLabel: UILabel! { get set }
    var currencyStackView: UIStackView! { get set }
    var flagImageView: UIImageView! { get set }
    var buyLabel: UILabel! { get set }
    var buyBalanceLabel: UILabel! { get set }
    var buyStackView: UIStackView! { get set }
    var sellLabel: UILabel! { get set }
    var sellBalanceLabel: UILabel! { get set }
    var sellStackView: UIStackView! { get set }
    var cbLabel: UILabel! { get set }
    var cbBalanceLabel: UILabel! { get set }
    var cbStackView: UIStackView! { get set }
    var currenciesStackView: UIStackView! { get set }
    var currenciesWithFlagStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
}

extension DashboardCurrencyTableViewCellInstaller {
    func initSubviews() {
        currencyLabel = UILabel()
        currencyLabel.font = .gothamNarrow(size: 14, .bold)
        currencyLabel.textColor = ColorConstants.mainTextColor
        currencyLabel.text = RS.lbl_usd.localized()
        
        currencyStackView = UIStackView()
        currencyStackView.axis = .vertical
        currencyStackView.alignment = .leading
        currencyStackView.distribution = .fill
        
        flagImageView = UIImageView()
        flagImageView.layer.cornerRadius = Size.flagSize/2
        
        buyLabel = UILabel()
        buyLabel.font = .gothamNarrow(size: 14, .book)
        buyLabel.textColor = ColorConstants.mainTextColor
        buyLabel.text = RS.lbl_buy.localized()

        buyBalanceLabel = UILabel()
        buyBalanceLabel.font = .gothamNarrow(size: 16, .bold)
        buyBalanceLabel.textColor = ColorConstants.mainTextColor

        buyStackView = UIStackView()
        buyStackView.axis = .vertical
        buyStackView.alignment = .leading
        buyStackView.distribution = .fillEqually
        buyStackView.spacing = Adaptive.val(1)

        sellLabel = UILabel()
        sellLabel.font = .gothamNarrow(size: 14, .book)
        sellLabel.textColor = ColorConstants.mainTextColor
        sellLabel.text = RS.lbl_sell.localized()

        sellBalanceLabel = UILabel()
        sellBalanceLabel.font = .gothamNarrow(size: 16, .bold)
        sellBalanceLabel.textColor = ColorConstants.mainTextColor

        sellStackView = UIStackView()
        sellStackView.axis = .vertical
        sellStackView.alignment = .leading
        sellStackView.distribution = .fillEqually
        sellStackView.spacing = Adaptive.val(1)

        cbLabel = UILabel()
        cbLabel.numberOfLines = 0
        cbLabel.font = .gothamNarrow(size: 14, .book)
        cbLabel.textColor = ColorConstants.mainTextColor
        cbLabel.text = RS.lbl_cb.localized()

        cbBalanceLabel = UILabel()
        cbBalanceLabel.font = .gothamNarrow(size: 16, .bold)
        cbBalanceLabel.textColor = ColorConstants.mainTextColor

        cbStackView = UIStackView()
        cbStackView.axis = .vertical
        cbStackView.alignment = .leading
        cbStackView.distribution = .fillEqually
        cbStackView.spacing = Adaptive.val(1)

        currenciesStackView = UIStackView()
        currenciesStackView.axis = .horizontal
        currenciesStackView.distribution = .fillEqually
        currenciesStackView.alignment = .fill
        currenciesStackView.spacing = Adaptive.val(5)
        
        currenciesWithFlagStackView = UIStackView()
        currenciesWithFlagStackView.axis = .horizontal
        currenciesWithFlagStackView.distribution = .fill
        currenciesWithFlagStackView.spacing = Adaptive.val(29)
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.distribution = .fill
        mainStackView.alignment = .fill
        mainStackView.spacing = Adaptive.val(5.5)
    }
    
    func embedSubviews() {
        currencyStackView.addArrangedSubview(currencyLabel)
        
        buyStackView.addArrangedSubview(buyLabel)
        buyStackView.addArrangedSubview(buyBalanceLabel)
        currenciesStackView.addArrangedSubview(buyStackView)
        
        sellStackView.addArrangedSubview(sellLabel)
        sellStackView.addArrangedSubview(sellBalanceLabel)
        currenciesStackView.addArrangedSubview(sellStackView)
        
        cbStackView.addArrangedSubview(cbLabel)
        cbStackView.addArrangedSubview(cbBalanceLabel)
        currenciesStackView.addArrangedSubview(cbStackView)
        
        currenciesWithFlagStackView.addArrangedSubview(flagImageView)
        currenciesWithFlagStackView.addArrangedSubview(currenciesStackView)

        mainStackView.addArrangedSubview(currencyStackView)
        mainStackView.addArrangedSubview(currenciesWithFlagStackView)
        mainView.addSubview(mainStackView)
    }
    
    func addSubviewsConstraints() {
        mainStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Size.edges.top)
            maker.left.equalTo(Size.edges.left)
            maker.bottom.equalTo(-Size.edges.bottom)
            maker.right.equalTo(-Size.edges.right)
        }
        
        flagImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.flagSize)
            maker.height.equalTo(Size.flagSize)
        }
    }
}

fileprivate struct Size {
    static let edges = Adaptive.edges(top: 10, left: 15, bottom: 10, right: 15)
    static let flagSize = Adaptive.val(40)
}
