//
//  DashboardCurrencyVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class DashboardCurrencyVM {
    var currency: CurrencyResponse
    
    var flag: ExchangeRateResponse?
    var buyRate: ExchangeRateResponse?
    var sellRate: ExchangeRateResponse?
    var cbRate: ExchangeRateResponse?
    
    var flagImage: String? {
        return buyRate?.flag
    }
    
    var buyCourse: String? {
        return formattedMoney(buyRate?.course)
    }
    
    var sellCourse: String? {
        return formattedMoney(sellRate?.course)
    }
    
    var cbCourse: String? {
        return formattedMoney(cbRate?.course)
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    init(_ currency: CurrencyResponse, _ exchangeRates: [ExchangeRateResponse]) {
        self.currency = currency
        
        for rate in exchangeRates {
            switch rate.courseType {
            case "5": // Покупка
                buyRate = rate
            case "4": // Продажа
                sellRate = rate
            case "1": // Курс ЦБ
                cbRate = rate
            default:
                break
            }
        }
    }
    
}

class DashboardCurrenciesVM {
    var currencies: [CurrencyResponse] = []
    var exchangeRates: [ExchangeRateResponse] = []
    var currenciesVM: [DashboardCurrencyVM] = []
    
    func didReceiveAllResponses() {
        for currency in currencies {
            var rates: [ExchangeRateResponse] = []
            for rate in exchangeRates {
                if currency.code == rate.currency {
                    rates.append(rate)
                    
                }
            }
            currenciesVM.append(DashboardCurrencyVM(currency, rates))
        }
    }

    func didReceiveMainResponses() {
        var newCurrencies: [CurrencyResponse] = []
        for currency in currencies {
            var rates: [ExchangeRateResponse] = []
            for rate in exchangeRates {
                if currency.code == rate.currency {
                    rates.append(rate)
                }
            }
            if rates.count == 3 {
                newCurrencies.append(currency)
                currenciesVM.append(DashboardCurrencyVM(currency, rates))
            }
        }
        currencies = newCurrencies
    }
}
