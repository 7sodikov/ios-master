//
//  DashboardLoanCalculatorViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardLoanCalculatorViewInstaller: ViewInstaller {
    var scrollView: UIScrollView! { get set }
    var contentView: UIView! { get set }
    var titleView: UILabel! { get set }
    var amountView: DashboardLoanCalculatorComponentView! { get set }
    var loanPeriodView: DashboardLoanCalculatorComponentView! { get set }
    var gracePeriodView: DashboardLoanCalculatorComponentView! { get set }
    var interestView: DashboardLoanCalculatorComponentView! { get set }
    var loanTypeView: DashboardLoanCalculatorComponentView! { get set }
    var calculateButton: NextButton! { get set }
}

extension DashboardLoanCalculatorViewInstaller {
    func initSubviews() {
        scrollView = UIScrollView()
        
        contentView = UIView()
        
        let font = UIFont.boldSystemFont(ofSize: (Adaptive.val(16.0)))
        
        titleView = UILabel()
        titleView.text = RS.lbl_loan_calc.localized()
        titleView.textColor = .dashDarkBlue
        titleView.font = .gothamNarrow(size: 16, .bold)
        
        amountView = DashboardLoanCalculatorComponentView()
        amountView.nameLabel.text = RS.lbl_amount.localized()
        amountView.balanceTextField.attributedPlaceholder = NSAttributedString(string: RS.lbl_enter_amount.localized(), attributes: [
            .foregroundColor: UIColor.lightGray,
            .font: font
        ])
        amountView.typeLabel.text = RS.lbl_uzs.localized()
        amountView.typeButton.isHidden = true
        amountView.typeImageView.isHidden = true
        amountView.balanceTextField.keyboardType = .decimalPad
        
        loanPeriodView = DashboardLoanCalculatorComponentView()
        loanPeriodView.nameLabel.text = RS.lbl_period_loan.localized()
        loanPeriodView.balanceTextField.attributedPlaceholder = NSAttributedString(string: RS.lbl_month.localized(), attributes: [
            .foregroundColor: UIColor.lightGray,
            .font: font
        ])
        loanPeriodView.typeLabel.text = RS.lbl_month.localized()
        loanPeriodView.typeButton.isHidden = true
        loanPeriodView.typeImageView.isHidden = true
        
        gracePeriodView = DashboardLoanCalculatorComponentView()
        gracePeriodView.nameLabel.text = RS.lbl_grace_period.localized()
        gracePeriodView.balanceTextField.attributedPlaceholder = NSAttributedString(string: RS.lbl_month.localized(), attributes: [
            .foregroundColor: UIColor.lightGray,
            .font: font
        ])
        gracePeriodView.typeLabel.text = RS.lbl_month.localized()
        gracePeriodView.typeButton.isHidden = true
        gracePeriodView.typeImageView.isHidden = true
        
        interestView = DashboardLoanCalculatorComponentView()
        interestView.nameLabel.text = RS.lbl_percent.localized()
        interestView.balanceTextField.attributedPlaceholder = NSAttributedString(string: RS.txt_f_enter_percent.localized(), attributes: [
            .foregroundColor: UIColor.lightGray,
            .font: font
        ])
        interestView.typeLabel.text = "%"
        interestView.typeButton.isHidden = true
        interestView.typeImageView.isHidden = true
        
        loanTypeView = DashboardLoanCalculatorComponentView()
        loanTypeView.nameLabel.text = RS.lbl_loan_type.localized()
        loanTypeView.balanceTextField.text = RS.lbl_annuity.localized()
        loanTypeView.balanceTextField.textColor = ColorConstants.mainTextColor
        loanTypeView.typeButton.isHidden = false
        loanTypeView.balanceTextField.isUserInteractionEnabled = false
        
        calculateButton = NextButton.systemButton(with: RS.btn_calculate.localized(), cornerRadius: Size.buttonHeight/2)
        calculateButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        calculateButton.setBackgroundColor(ColorConstants.highlightedGray, for: .selected)
    }
    
    func embedSubviews() {
        mainView.addSubview(scrollView)
        scrollView.addSubview(contentView)
        scrollView.addSubview(amountView)
        scrollView.addSubview(loanPeriodView)
        scrollView.addSubview(gracePeriodView)
        scrollView.addSubview(interestView)
        scrollView.addSubview(loanTypeView)
        scrollView.addSubview(calculateButton)
    }
    
    func addSubviewsConstraints() {
        scrollView.constraint { make in
            make.top(Adaptive.val(32)).horizontal.bottom.equalToSuperView()
        }
//        scrollView.snp.remakeConstraints { (make) in
//            make.top.equalTo(self.mainView.snp.top).offset(Adaptive.val(63))
//            make.leading.trailing.bottom.equalToSuperview()
//            maker.top.equalToSuperview().offset(Adaptive.val(63))
//        }
        
        contentView.snp.remakeConstraints { (maker) in
            maker.left.right.equalTo(self.mainView)
            maker.width.top.bottom.equalTo(self.scrollView)
        }
    
        amountView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(contentView.snp.top)
            maker.width.equalTo(scrollView)
        }
        
        loanPeriodView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(amountView.snp.bottom).offset(Size.distance)
            maker.width.equalTo(scrollView)
        }
        
        gracePeriodView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(loanPeriodView.snp.bottom).offset(Size.distance)
            maker.width.equalTo(scrollView)
        }
        
        interestView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(gracePeriodView.snp.bottom).offset(Size.distance)
            maker.width.equalTo(scrollView)
        }
        
        loanTypeView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(interestView.snp.bottom).offset(Size.distance)
            maker.width.equalTo(scrollView)
        }
        
        calculateButton .snp.remakeConstraints { (maker) in
            maker.top.equalTo(loanTypeView.snp.bottom).offset(Adaptive.val(25))
            maker.leading.equalTo(contentView.snp.leading).offset(Adaptive.val(16))
            maker.trailing.equalTo(contentView.snp.trailing).offset(-Adaptive.val(16))
            maker.bottom.equalTo(contentView.snp.bottom)
            maker.height.equalTo(Size.buttonHeight)
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(47)
    static let distance = Adaptive.val(25)
}
