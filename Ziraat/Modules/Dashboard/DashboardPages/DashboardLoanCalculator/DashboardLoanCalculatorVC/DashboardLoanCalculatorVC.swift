//
//  DashboardLoanCalculatorVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardLoanCalculatorVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
}

class DashboardLoanCalculatorVC: BaseViewController, DashboardLoanCalculatorViewInstaller {
    
    enum Mode: Int {
        case withNav
        case noNav
    }
    
    var scrollView: UIScrollView!
    var contentView: UIView!
    var titleView: UILabel!
    var amountView: DashboardLoanCalculatorComponentView!
    var loanPeriodView: DashboardLoanCalculatorComponentView!
    var gracePeriodView: DashboardLoanCalculatorComponentView!
    var interestView: DashboardLoanCalculatorComponentView!
    var loanTypeView: DashboardLoanCalculatorComponentView!
    var calculateButton: NextButton!
    var mainView: UIView { view }
  
    var presenter: DashboardLoanCalculatorPresenterProtocol!
    
    fileprivate let filterCellId = "\(FilterTableViewCell.self)"
    
    let transparentView = UIView()
    let filterTableView = UITableView()
    var selectedView = UIView()
    var dataDropDown = [FilterModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        filterTableView.backgroundColor = .white
        filterTableView.delegate = self
        filterTableView.dataSource = self
        filterTableView.register(FilterTableViewCell.self, forCellReuseIdentifier: filterCellId)
        filterTableView.tableFooterView = UIView()
        filterTableView.separatorColor = .clear
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openDropDown(_:)))
        loanTypeView.typeButton.addGestureRecognizer(tap)
        
        amountView.balanceTextField.delegate = self
        
        calculateButton.addTarget(self, action: #selector(calculateButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        calculateButton.animateIndicator = show
    }
    
    func whiteMode() {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "img_dash_light_background")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
       
        [amountView, loanPeriodView, gracePeriodView, interestView, loanTypeView].forEach { (componet) in
            componet?.nameLabel.textColor = .dashDarkBlue
         }
//        title = RS.lbl_loan_calc.localized()
        self.navigationItem.titleView = titleView
        
        amountView.backView.layer.borderWidth = Adaptive.val(2)
        amountView.backView.layer.borderColor = UIColor.darkGrey.cgColor
        
        loanPeriodView.backView.layer.borderWidth = Adaptive.val(2)
        loanPeriodView.backView.layer.borderColor = UIColor.darkGrey.cgColor
        
        gracePeriodView.backView.layer.borderWidth = Adaptive.val(2)
        gracePeriodView.backView.layer.borderColor = UIColor.darkGrey.cgColor
        
        interestView.backView.layer.borderWidth = Adaptive.val(2)
        interestView.backView.layer.borderColor = UIColor.darkGrey.cgColor
        
        loanTypeView.backView.layer.borderWidth = Adaptive.val(2)
        loanTypeView.backView.layer.borderColor = UIColor.darkGrey.cgColor
        
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    
    @objc private func calculateButtonClicked(_ sender: NextButton) {
        var loanType = 0
        if loanTypeView.balanceTextField.text == RS.lbl_annuity.localized() {
            loanType = 1
        } else {
            loanType = 2
        }
        let amountValue = amountView.balanceTextField.text
        let textAmount = amountValue!.replacingOccurrences(of: " ", with: "")
        let amountInInt = Int(textAmount) ?? 0
        if amountInInt > 0 && !loanPeriodView.balanceTextField.text!.isEmpty && !gracePeriodView.balanceTextField.text!.isEmpty && !interestView.balanceTextField.text!.isEmpty {
            presenter.nextButtonClicked(amount: amountInInt * 100, duration: Int(loanPeriodView.balanceTextField.text ?? "") ?? 0, gracePeriod: Int(gracePeriodView.balanceTextField.text ?? "") ?? 0, rate: Int(interestView.balanceTextField.text ?? "") ?? 0, type: loanType)
        }
        
        if amountInInt == 0 {
            amountView.errorMessage.isHidden = false
        } else {
            amountView.errorMessage.isHidden = true
        }
        
        if loanPeriodView.balanceTextField.text?.isEmpty == true {
            loanPeriodView.errorMessage.isHidden = false
        } else {
            loanPeriodView.errorMessage.isHidden = true
        }
        
        if gracePeriodView.balanceTextField.text?.isEmpty == true {
            gracePeriodView.errorMessage.isHidden = false
        } else {
            gracePeriodView.errorMessage.isHidden = true
        }
        
        if interestView.balanceTextField.text?.isEmpty == true {
            interestView.errorMessage.isHidden = false
        } else {
            interestView.errorMessage.isHidden = true
        }
    }
    
    func addTransparentView(frames: CGRect) {
        let window = UIApplication.shared.keyWindow
        transparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        
        filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width - 100, height: 0)
        self.view.addSubview(filterTableView)
        filterTableView.layer.cornerRadius = Adaptive.val(15)
        
        transparentView.backgroundColor = .clear
        filterTableView.reloadData()
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(removeTransparentView))
        transparentView.addGestureRecognizer(tapgesture)
        transparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.filterTableView.frame = CGRect(x: frames.origin.x + 17, y: frames.origin.y, width: frames.width - 34, height: CGFloat(self.dataDropDown.count * 50) + 30)
        }, completion: nil)
    }
    
    @objc func removeTransparentView() {
        let frames = selectedView.frame
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width, height: 0)
        }, completion: nil)
    }
    
    @objc private func openDropDown(_ gesture: UITapGestureRecognizer) {
        dataDropDown = [FilterModel(name: RS.lbl_annuity.localized()),
                        FilterModel(name: RS.lbl_proportional.localized())]
        selectedView = loanTypeView
        addTransparentView(frames: selectedView.frame)
    }
}

extension DashboardLoanCalculatorVC: DashboardLoanCalculatorVCProtocol, UITableViewDelegate, UITableViewDataSource {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataDropDown.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: filterCellId, for: indexPath) as! FilterTableViewCell
        cell.dataModel = self.dataDropDown[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        loanTypeView.balanceTextField.text = self.dataDropDown[indexPath.row].name
        removeTransparentView()
    }
    
}

extension DashboardLoanCalculatorVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "fr_FR")
        formatter.maximumFractionDigits = 0
        formatter.groupingSeparator = " "
        
        if let groupingSeparator = formatter.groupingSeparator {
            
            if string == groupingSeparator {
                return true
            }
            if let textWithoutGroupingSeparator = textField.text?.replacingOccurrences(of: groupingSeparator, with: "") {
                var totalTextWithoutGroupingSeparators = textWithoutGroupingSeparator + string
                if string.isEmpty { // pressed Backspace key
                    totalTextWithoutGroupingSeparators.removeLast()
                }
                if let numberWithoutGroupingSeparator = formatter.number(from: totalTextWithoutGroupingSeparators),
                   let formattedText = formatter.string(from: numberWithoutGroupingSeparator) {
                    textField.text = formattedText
                    return false
                }
            }
        }
        return true
    }
}

extension String {
    var currency: String {
        let stringWithoutSymbol = self.replacingOccurrences(of: "$", with: "")
        let stringWithoutComma = stringWithoutSymbol.replacingOccurrences(of: ",", with: "")
        
        let styler = NumberFormatter()
        styler.minimumFractionDigits = 0
        styler.maximumFractionDigits = 0
        styler.numberStyle = .decimal
        
        if let result = NumberFormatter().number(from: stringWithoutComma) {
            return styler.string(from: result)!
        }
        
        return self
    }
}
