//
//  DashboardLoanCalculatorPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardLoanCalculatorPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: DashboardLoanCalculatorVM { get }
    func nextButtonClicked(amount: Int, duration: Int, gracePeriod: Int, rate: Int, type: Int)
}

protocol DashboardLoanCalculatorInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didLoanCalculate(with response: ResponseResult<[LoanCalculateResponse], AppError>)
}

class DashboardLoanCalculatorPresenter: DashboardLoanCalculatorPresenterProtocol {
    weak var view: DashboardLoanCalculatorVCProtocol!
    var interactor: DashboardLoanCalculatorInteractorProtocol!
    var router: DashboardLoanCalculatorRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = DashboardLoanCalculatorVM()
    
    // Private property and methods
    func nextButtonClicked(amount: Int, duration: Int, gracePeriod: Int, rate: Int, type: Int) {
        view.preloader(show: true)
        interactor.loanCalculate(amount: amount, period: duration, gracePeriod: gracePeriod, rate: rate, type: type)
    }
}

extension DashboardLoanCalculatorPresenter: DashboardLoanCalculatorInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didLoanCalculate(with response: ResponseResult<[LoanCalculateResponse], AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                router.navigateToSchedule(with: view.navigationCtrl, loanCalculate: result.data)
            }
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
}
