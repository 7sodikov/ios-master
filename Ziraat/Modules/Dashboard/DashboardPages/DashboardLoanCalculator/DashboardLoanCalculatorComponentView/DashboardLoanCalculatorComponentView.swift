//
//  DashboardLoanCalculatorComponentView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class DashboardLoanCalculatorComponentView: UIView, DashboardLoanCalculatorComponentViewInstaller {
    var contentBackView: UIView!
    var backView: UIView!
    var nameLabel: UILabel!
    var typeLabel: UILabel!
    var typeImageView: UIImageView!
    var typeButton: UIButton!
    var balanceTextField: LoginTextField!
    var errorMessage: UILabel!
    var mainView: UIView { self }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }    
}
