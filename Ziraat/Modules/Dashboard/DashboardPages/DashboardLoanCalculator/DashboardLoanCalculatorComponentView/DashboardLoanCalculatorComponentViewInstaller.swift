//
//  DashboardLoanCalculatorComponentViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardLoanCalculatorComponentViewInstaller: ViewInstaller {
    var contentBackView: UIView! { get set }
    var backView: UIView! { get set }
    var nameLabel: UILabel! { get set }
    var typeLabel: UILabel! { get set }
    var typeImageView: UIImageView! { get set }
    var typeButton: UIButton! { get set }
    var balanceTextField: LoginTextField! { get set }
    var errorMessage: UILabel! { get set }
}

extension DashboardLoanCalculatorComponentViewInstaller {
    func initSubviews() {
        contentBackView = UIView()
        contentBackView.backgroundColor = .clear
        
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(6)
        
        nameLabel = UILabel()
        nameLabel.textColor = .white
        nameLabel.font = .gothamNarrow(size: 18, .book) //EZFontType.medium.gothamText(size: Adaptive.val(18))//EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        
        typeLabel = UILabel()
        typeLabel.textColor = .clear //ColorConstants.lightGray
        typeLabel.font = EZFontType.medium.gothamText(size: Adaptive.val(15))//EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        typeLabel.textAlignment = .right
        
        typeImageView = UIImageView()
        typeImageView.image = UIImage(named: "btn_down")
        
        typeButton = UIButton()
        
        balanceTextField = LoginTextField(with: "", keyboard: .decimalPad, cornerRadius: Size.heightTextField/2, isSecureTextEntry: false)
        balanceTextField.textColor = ColorConstants.lightGray
        
        errorMessage = UILabel()
        errorMessage.text = RS.lbl_fill_blank_field.localized()
        errorMessage.isHidden = true
        errorMessage.textColor = .red
        errorMessage.font = .gothamNarrow(size: 14, .book)
    }
    
    func embedSubviews() {
        mainView.addSubview(contentBackView)
        mainView.addSubview(backView)
        mainView.addSubview(nameLabel)
        backView.addSubview(typeLabel)
        backView.addSubview(typeImageView)
        backView.addSubview(typeButton)
        backView.addSubview(balanceTextField)
        mainView.addSubview(errorMessage)
    }
    
    func addSubviewsConstraints() {
        contentBackView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
//            maker.height.equalTo(Adaptive.val(105))
        }
        
        nameLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(contentBackView.snp.leading).offset(Adaptive.val(16))
            maker.top.equalTo(contentBackView.snp.top)//.offset(Adaptive.val(6))
        }
        
        backView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(nameLabel.snp.bottom).offset(Adaptive.val(10))
            maker.leading.equalTo(contentBackView.snp.leading).offset(Adaptive.val(15))
            maker.trailing.equalTo(contentBackView.snp.trailing).offset(-Adaptive.val(15))
            maker.bottom.equalTo(contentBackView.snp.bottom)//.offset(-Adaptive.val(6))
            maker.height.equalTo(Size.heightTextField)
        }
        
        typeLabel.snp.remakeConstraints { (maker) in
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(17))
            maker.centerY.equalTo(backView)
//            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(21))
            maker.width.equalTo(Adaptive.val(50))
        }
        
        typeImageView.snp.remakeConstraints { (maker) in
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(17))
            maker.centerY.equalTo(backView)
            maker.width.equalTo(Adaptive.val(16))
            maker.height.equalTo(Adaptive.val(9))
        }
        
        typeButton.snp.remakeConstraints { (maker) in
//            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(17))
//            maker.centerY.equalTo(backView)
//            maker.width.equalTo(Adaptive.val(16))
//            maker.height.equalTo(Adaptive.val(9))
            maker.edges.equalTo(0)
        }
        
        balanceTextField.snp.remakeConstraints { (maker) in
            maker.top.leading.bottom.equalTo(backView)
            maker.trailing.equalTo(typeLabel.snp.leading)
            maker.height.equalTo(Adaptive.val(50))
        }
        
        errorMessage.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.bottom).offset(Adaptive.val(5))
            maker.leading.equalToSuperview().offset(Adaptive.val(15))
        }
    }
}

fileprivate struct Size {
    static let heightTextField = Adaptive.val(50)
}

