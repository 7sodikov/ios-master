//
//  DashboardLoanTypeComponentView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

class DashboardLoanTypeComponentView: UIView, DashboardLoanTypeComponentViewInstaller {
    var mainView: UIView { self }
    var titleLabel: UILabel!
    var textField: LoginTextField!
    var indicator: MDCActivityIndicator!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
    
    func setPreloader() {
        textField.rightView = indicator
        textField.rightViewMode = .unlessEditing
    }
    
    var animateIndicator: Bool {
        get {
            indicator.isAnimating
        }
        set {
            if newValue {
                indicator.startAnimating()
            } else {
                indicator.stopAnimating()
            }
        }
    }
}
