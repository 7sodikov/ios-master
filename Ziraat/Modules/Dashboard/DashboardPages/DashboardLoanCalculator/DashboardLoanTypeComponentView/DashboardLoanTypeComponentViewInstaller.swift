//
//  DashboardLoanTypeComponentViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

protocol DashboardLoanTypeComponentViewInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var textField: LoginTextField! { get set }
    var indicator: MDCActivityIndicator! { get set }
}

extension DashboardLoanTypeComponentViewInstaller {
    func initSubviews() {
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: 16.5)
        titleLabel.textColor = .dashDarkBlue
        
        textField = LoginTextField(with: "", cornerRadius: LoanCalculatorTableCellSize.TextField.height/2, isSecureTextEntry: false)
        
        indicator = MDCActivityIndicator(frame: CGRect(x: 0, y: 0, width: LoanCalculatorTableCellSize.TextField.height, height: LoanCalculatorTableCellSize.TextField.height/2))
        indicator.cycleColors = [.lightGray]
        indicator.tintColor = .lightGray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.radius = (LoanCalculatorTableCellSize.TextField.height/2) * 0.45
    }
    
    func embedSubviews() {
        mainView.addSubview(titleLabel)
        mainView.addSubview(textField)
    }
    
    func addSubviewsConstraints() {
        titleLabel.snp.remakeConstraints { (maker) in
            let leftRight = ApplicationSize.paddingLeftRight + LoanCalculatorTableCellSize.TextField.height/2
            maker.top.equalTo(ApplicationSize.paddingLeftRight)
            maker.left.equalTo(leftRight)
            maker.right.equalTo(-leftRight)
        }
        
        textField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(5))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(LoanCalculatorTableCellSize.TextField.height)
        }
    }
}

fileprivate struct Size {
    static let heightTextField = Adaptive.val(54)
}

