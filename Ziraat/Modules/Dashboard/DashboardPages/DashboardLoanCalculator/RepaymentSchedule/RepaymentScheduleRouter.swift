//
//  RepaymentScheduleRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol RepaymentScheduleRouterProtocol: class {
    static func createModule(loanCalculate: [LoanCalculateResponse]) -> UIViewController
}

class RepaymentScheduleRouter: RepaymentScheduleRouterProtocol {
    static func createModule(loanCalculate: [LoanCalculateResponse]) -> UIViewController {
        let vc = RepaymentScheduleVC()
        let presenter = RepaymentSchedulePresenter()
        let interactor = RepaymentScheduleInteractor()
        let router = RepaymentScheduleRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.loanCalculate = loanCalculate
        interactor.presenter = presenter
        
        return vc
    }
    
}
