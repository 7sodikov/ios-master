//
//  RepaymentScheduleVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

class RepaymentScheduleVM {
    var loanCalculate: [LoanCalculateResponse]!
    var items: [RepaymentScheduleItemVM] = []

    func add() {
        for loanItem in loanCalculate {
            items.append(RepaymentScheduleItemVM(schedule: loanItem))
        }
    }
}

class RepaymentScheduleItemVM {
    var schedule: LoanCalculateResponse
    
    var date: String {
        return schedule.date
    }
    
    var interest: String? {
        return formattedMoney(schedule.interestAmount)
    }
    
    var mainDebt: String? {
        return formattedMoney(schedule.principalAmount)
    }
    
    var totalAmount: String? {
        return formattedMoney(schedule.totalAmount)
    }
    
    var saldo: String? {
        return formattedMoney(schedule.saldo)
    }
    
    private func formattedMoneyWithoutDevision(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    init(schedule: LoanCalculateResponse) {
        self.schedule = schedule
    }
}




