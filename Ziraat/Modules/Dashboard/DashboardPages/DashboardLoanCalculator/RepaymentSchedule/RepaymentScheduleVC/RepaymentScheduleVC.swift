//
//  RepaymentScheduleVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol RepaymentScheduleVCProtocol: BaseViewControllerProtocol {
    
}

class RepaymentScheduleVC: BaseViewController, RepaymentScheduleViewInstaller {
    var backView: UIImageView!
    var tableView: UITableView!
    var mainView: UIView { view }
    
    var selectedIndexPath: IndexPath?
    var indexPaths: Array<IndexPath> = []

    var presenter: RepaymentSchedulePresenterProtocol!
    
    fileprivate let cellid = "\(RepaymentScheduleTableViewCell.self)"
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        self.title = RS.btn_payoff.localized()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: false)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

extension RepaymentScheduleVC: RepaymentScheduleVCProtocol, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.loanCalculate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! RepaymentScheduleTableViewCell
        presenter.viewModel.add()
        cell.backgroundColor = .clear
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        let scheduleVM = presenter.viewModel.items[indexPath.row]
        return cell.setup(with: scheduleVM)
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath == cellIsSelected {
//            cellIsSelected = nil
//        } else {
//            cellIsSelected = indexPath
//        }
//        tableView.performBatchUpdates(nil, completion: nil)
        
        selectedIndexPath = indexPath
        if !indexPaths.contains(selectedIndexPath!) {
                indexPaths += [selectedIndexPath!]
        } else {
            let index = indexPaths.firstIndex(of: selectedIndexPath!)
            indexPaths.remove(at: index!)
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if cellIsSelected == indexPath {
//            return UITableView.automaticDimension
//        } else {
//            return Adaptive.val(68)
//        }
        if indexPaths.count > 0 {
            if indexPaths.contains(indexPath){
                return UITableView.automaticDimension
            }
            else {
                return Adaptive.val(68)
            }
        }
        return Adaptive.val(68)
    }
    
}

