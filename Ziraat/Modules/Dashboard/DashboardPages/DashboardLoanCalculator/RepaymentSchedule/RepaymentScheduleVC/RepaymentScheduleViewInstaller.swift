//
//  RepaymentScheduleViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol RepaymentScheduleViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var tableView: UITableView! { get set }
}

extension RepaymentScheduleViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")

        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .clear
        tableView.register(RepaymentScheduleTableViewCell.self, forCellReuseIdentifier: String(describing: RepaymentScheduleTableViewCell.self))
        tableView.backgroundColor = .clear
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.leading.bottom.trailing.equalTo(backView)
        }
    }
}

fileprivate struct Size {
    
}
