//
//  RepaymentSchedulePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol RepaymentSchedulePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: RepaymentScheduleVM { get }
}

protocol RepaymentScheduleInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class RepaymentSchedulePresenter: RepaymentSchedulePresenterProtocol {
    weak var view: RepaymentScheduleVCProtocol!
    var interactor: RepaymentScheduleInteractorProtocol!
    var router: RepaymentScheduleRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = RepaymentScheduleVM()
    
    
    // Private property and methods
    private var responsesCount = 0
    private var successCount = 0
    
    private func processMainResponseCount(isSuccess: Bool) {
        responsesCount += 1
        
        if isSuccess {
            successCount += 1
        }
        
        if responsesCount == 1 {
            if successCount == 1 {
                viewModel.add()
//                view.showReloadButtonForResponses(false, animateReloadButton: false)
            } else {
//                view.showReloadButtonForResponses(true, animateReloadButton: false)
            }
        }
    }
    
}

extension RepaymentSchedulePresenter: RepaymentScheduleInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
