//
//  RepaymentScheduleTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol RepaymentScheduleTableViewCellDelegate: class {
    
}

class RepaymentScheduleTableViewCell: UITableViewCell, RepaymentScheduleTableViewCellInstaller {
    var mainView: UIView { contentView }
    var backView: UIView!
    var dateLabel: UILabel!
    var overallAmountLabel: UILabel!
    var arrowButton: UIImageView!
    var arrowTotalButton: UIButton!
    var headerStackView: UIStackView!
    var withArrowStackView: UIStackView!
    var lineView: UIView!
    var lineStackView: UIStackView!
    var mainDebtLabel: UILabel!
    var mainDebtValueLabel: UILabel!
    var mainDebtStackView: UIStackView!
    var interestChargeLabel: UILabel!
    var interestChargeValueLabel: UILabel!
    var interestChargeStackView: UIStackView!
    var totalAmountLabel: UILabel!
    var totalAmountValueLabel: UILabel!
    var totalAmountStackView: UIStackView!
    var mainStackView: UIStackView!
    var superMainStackView: UIStackView!
    
    weak var delegate: RepaymentScheduleTableViewCellDelegate?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.arrowButton.image = UIImage(named: "btn_up")
        } else {
            arrowButton.image = UIImage(named: "btn_down")
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with scheduleType: RepaymentScheduleItemVM) -> RepaymentScheduleTableViewCell {
        let scheduleItem = scheduleType
        dateLabel.text = scheduleItem.date
        interestChargeValueLabel.text = scheduleItem.interest
        mainDebtValueLabel.text = scheduleItem.mainDebt
        totalAmountValueLabel.text = scheduleItem.saldo//scheduleItem.totalAmount
        overallAmountLabel.text = scheduleItem.totalAmount//scheduleItem.saldo
        return self
    }
}
