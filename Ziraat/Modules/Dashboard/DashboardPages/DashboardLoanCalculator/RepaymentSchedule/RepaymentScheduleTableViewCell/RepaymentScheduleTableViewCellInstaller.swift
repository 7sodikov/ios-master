//
//  RepaymentScheduleTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol RepaymentScheduleTableViewCellInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var dateLabel: UILabel! { get set }
    var overallAmountLabel: UILabel! { get set }
    var arrowButton: UIImageView! { get set }
    var arrowTotalButton: UIButton! { get set }
    var headerStackView: UIStackView! { get set }
    var withArrowStackView: UIStackView! { get set }
    var lineView: UIView! { get set }
    var lineStackView: UIStackView! { get set }
    var mainDebtLabel: UILabel! { get set }
    var mainDebtValueLabel: UILabel! { get set }
    var mainDebtStackView: UIStackView! { get set }
    var interestChargeLabel: UILabel! { get set }
    var interestChargeValueLabel: UILabel! { get set }
    var interestChargeStackView: UIStackView! { get set }
    var totalAmountLabel: UILabel! { get set }
    var totalAmountValueLabel: UILabel! { get set }
    var totalAmountStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
    var superMainStackView: UIStackView! { get set }
}

extension RepaymentScheduleTableViewCellInstaller {
    func initSubviews() {
        let titleFont = EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))
        let valueFont = EZFontType.regular.sfuiDisplay(size: Adaptive.val(14))
        
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(10)
        
        dateLabel = UILabel()
        dateLabel.font = valueFont
        dateLabel.textColor = .black
        
        overallAmountLabel = UILabel()
        overallAmountLabel.font = valueFont
        overallAmountLabel.textColor = .black
        overallAmountLabel.textAlignment = .right
        
        arrowButton = UIImageView()
        arrowButton.image = UIImage(named: "btn_down")
        
        arrowTotalButton = UIButton()
        arrowTotalButton.isUserInteractionEnabled = false

        headerStackView = UIStackView()
        headerStackView.axis = .horizontal
        headerStackView.spacing = Adaptive.val(20)
        headerStackView.distribution = .fillEqually
        
        withArrowStackView = UIStackView()
        withArrowStackView.axis = .horizontal
        withArrowStackView.spacing = Adaptive.val(20)
        withArrowStackView.alignment = .fill
        
        lineView = UIView()
        lineView.backgroundColor = ColorConstants.lightGray
        
        lineStackView = UIStackView()
        lineStackView.axis = .vertical
        lineStackView.distribution = .fill
        
        mainDebtLabel = UILabel()
        mainDebtLabel.font = titleFont
        mainDebtLabel.textColor = .black
        mainDebtLabel.text = RS.lbl_main_debt.localized()
        
        mainDebtValueLabel = UILabel()
        mainDebtValueLabel.font = valueFont
        mainDebtValueLabel.textColor = .black
        mainDebtValueLabel.textAlignment = .right
        
        mainDebtStackView = UIStackView()
        mainDebtStackView.axis = .horizontal
        mainDebtStackView.spacing = Adaptive.val(20)
        mainDebtStackView.distribution = .fill
        
        interestChargeLabel = UILabel()
        interestChargeLabel.font = titleFont
        interestChargeLabel.textColor = .black
        interestChargeLabel.text = RS.lbl_interest_charge.localized()
        interestChargeLabel.numberOfLines = 0
        
        interestChargeValueLabel = UILabel()
        interestChargeValueLabel.font = valueFont
        interestChargeValueLabel.textColor = .black
        interestChargeValueLabel.textAlignment = .right
        
        interestChargeStackView = UIStackView()
        interestChargeStackView.axis = .horizontal
        interestChargeStackView.spacing = Adaptive.val(20)
        interestChargeStackView.distribution = .fillEqually
        interestChargeStackView.alignment = .fill
        
        totalAmountLabel = UILabel()
        totalAmountLabel.font = titleFont
        totalAmountLabel.textColor = .black
        totalAmountLabel.text = RS.lbl_total_amount.localized()
        
        totalAmountValueLabel = UILabel()
        totalAmountValueLabel.font = valueFont
        totalAmountValueLabel.textColor = .black
        totalAmountValueLabel.textAlignment = .right
        
        totalAmountStackView = UIStackView()
        totalAmountStackView.axis = .horizontal
        totalAmountStackView.spacing = Adaptive.val(20)
        totalAmountStackView.distribution = .fill
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.spacing = Adaptive.val(12)
        mainStackView.distribution = .fill
        
        superMainStackView = UIStackView()
        superMainStackView.axis = .vertical
        superMainStackView.spacing = Adaptive.val(20)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        
        headerStackView.addArrangedSubview(dateLabel)
        headerStackView.addArrangedSubview(overallAmountLabel)
        withArrowStackView.addArrangedSubview(headerStackView)
        withArrowStackView.addArrangedSubview(arrowButton)
        superMainStackView.addArrangedSubview(withArrowStackView)
        
        superMainStackView.addArrangedSubview(lineStackView)
        lineStackView.addArrangedSubview(lineView)
        
        mainDebtStackView.addArrangedSubview(mainDebtLabel)
        mainDebtStackView.addArrangedSubview(mainDebtValueLabel)
        mainStackView.addArrangedSubview(mainDebtStackView)
        
        interestChargeStackView.addArrangedSubview(interestChargeLabel)
        interestChargeStackView.addArrangedSubview(interestChargeValueLabel)
        mainStackView.addArrangedSubview(interestChargeStackView)
        
        totalAmountStackView.addArrangedSubview(totalAmountLabel)
        totalAmountStackView.addArrangedSubview(totalAmountValueLabel)
        mainStackView.addArrangedSubview(totalAmountStackView)
        
        superMainStackView.addArrangedSubview(mainStackView)
        
        mainView.addSubview(superMainStackView)
        mainView.addSubview(arrowTotalButton)
    }
    
    func addSubviewsConstraints() {
        
        backView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(Adaptive.val(20))
            maker.trailing.equalTo(-Adaptive.val(20))
            maker.top.equalTo(Adaptive.val(10))
            maker.bottom.equalTo(-Adaptive.val(10))
        }

        superMainStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(20))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(20))
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(18))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(18))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.trailing.equalTo(superMainStackView.snp.trailing).offset(-Adaptive.val(41))
        }
        
        lineView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(2))
        }
        
        arrowButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(21))
            maker.height.equalTo(Adaptive.val(12))
        }
        
        arrowTotalButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(withArrowStackView)
        }
    }
}

fileprivate struct Size {
    
}
