//
//  RepaymentScheduleInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol RepaymentScheduleInteractorProtocol: class {
    
}

class RepaymentScheduleInteractor: RepaymentScheduleInteractorProtocol {
    weak var presenter: RepaymentScheduleInteractorToPresenterProtocol!
    
}
