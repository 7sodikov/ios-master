//
//  DashboardLoanCalculatorRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardLoanCalculatorRouterProtocol: AnyObject {
    static func createModule() -> BaseMainSwipableViewController
    func navigateToSchedule(with navigationCtrl: Any, loanCalculate: [LoanCalculateResponse])
}

class DashboardLoanCalculatorRouter: DashboardLoanCalculatorRouterProtocol {
    static func createModule() -> BaseMainSwipableViewController {
        let vc = DashboardLoanCalculatorVC()
        let presenter = DashboardLoanCalculatorPresenter()
        let interactor = DashboardLoanCalculatorInteractor()
        let router = DashboardLoanCalculatorRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    static func createModuleWhiteMode() -> UIViewController {
        let vc = DashboardLoanCalculatorVC()
        vc.whiteMode()
        let presenter = DashboardLoanCalculatorPresenter()
        let interactor = DashboardLoanCalculatorInteractor()
        let router = DashboardLoanCalculatorRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToSchedule(with navigationCtrl: Any, loanCalculate: [LoanCalculateResponse]) {
        let vc = RepaymentScheduleRouter.createModule(loanCalculate: loanCalculate)
        (navigationCtrl as? UINavigationController)?.pushViewController(vc, animated: true)
    }
}
