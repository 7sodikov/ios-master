//
//  DashboardLoanCalculatorInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardLoanCalculatorInteractorProtocol: AnyObject {
    func loanCalculate(amount: Int, period: Int, gracePeriod: Int, rate: Int, type: Int)
}

class DashboardLoanCalculatorInteractor: DashboardLoanCalculatorInteractorProtocol {
    weak var presenter: DashboardLoanCalculatorInteractorToPresenterProtocol!
    
    func loanCalculate(amount: Int, period: Int, gracePeriod: Int, rate: Int, type: Int) {
        NetworkService.Loan.loanCalculate(amount: amount, period: period, gracePeriod: gracePeriod, rate: rate, typeLoan: type) { (result) in
            self.presenter.didLoanCalculate(with: result)
        }
    }
}
