//
//  DashboardMyActivesVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import Charts

protocol DashboardMyActivesVCProtocol: AnyObject {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
    func reloadViewForCurrencyChange()
}

class DashboardMyActivesVC: BaseMainSwipableViewController, DashboardMyActivesViewInstaller {
    var titleLabel: UILabel!
    
    var mainView: UIView { view }
    var segmentedControl: EZSegmentedControl!
    var mainSeparatorLineView: UIView!
    var activesView: ActivesAndPassivesView!
    var liabilitiesView: ActivesAndPassivesView!
    var currenciesSegmentedCtrl: EZVerticalSegmentedControl!
    var visibilityButton: UIButton!
    var infoButton: UIButton!
    var reloadButtonView: ReloadButtonView!
    
    var presenter: DashboardMyActivesPresenter!
    var indexName: Int?
    
    weak var delegate: DashboardMyActivesPresenterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        segmentedControl.addTarget(self, action: #selector(segmentedControlChanged(_:)), for: .valueChanged)
        currenciesSegmentedCtrl.addTarget(self, action: #selector(currenciesSegmentClicked(_:)), for: .valueChanged)
        visibilityButton.addTarget(self, action: #selector(visibilityClicked(_:)), for: .touchUpInside)
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadButtonClicked(_:)), for: .touchUpInside)
        infoButton.addTarget(self, action: #selector(inforButtonClicked(_:)), for: .touchUpInside)
        
        activesView.delegate = self
        liabilitiesView.delegate = self
        
        presenter.viewDidLoad()
    }
    
    func setStateForVisibilityButton() {
        if UDManager.hideBalance == true {
            visibilityButton.isSelected = true
        } else {
            visibilityButton.isSelected = false
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadViewForCurrencyChange()
        setStateForVisibilityButton()
        presenter.viewWillAppear(animated: animated)
    }
    
    @objc private func segmentedControlChanged(_ sender: EZSegmentedControl) {
        activesView.isHidden = (sender.selectedIndex != 0)
        liabilitiesView.isHidden = (sender.selectedIndex == 0)
        
        if sender.selectedIndex == 0 {
            delegate?.changeTitleName(indexNumber: 0)
            activesView.pieChart.animate(xAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
            activesView.pieChart.animate(yAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
            infoButton.isHidden = true
        } else {
            delegate?.changeTitleName(indexNumber: 1)
            liabilitiesView.pieChart.animate(xAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
            liabilitiesView.pieChart.animate(yAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
            infoButton.isHidden = false
        }
    }
    
    @objc private func currenciesSegmentClicked(_ sender: EZVerticalSegmentedControl) {
        presenter.currenciesSegmentClicked(for: sender.selectedIndex)
    }
    
    @objc private func visibilityClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        UDManager.hideBalance = !UDManager.hideBalance
        reloadViewForCurrencyChange()
    }
    
    @objc private func reloadButtonClicked(_ sender: UIButton) {
        presenter.viewDidLoad()
    }
    
    @objc private func inforButtonClicked(_ sender: UIButton) {
        let vc = MyLiabilitiesInfoVC()
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async { [self] in
            activesView.pieChart.animate(xAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
            activesView.pieChart.animate(yAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
        }
    }
}

extension DashboardMyActivesVC: DashboardMyActivesVCProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        segmentedControl.isUserInteractionEnabled = !show
        currenciesSegmentedCtrl.isHidden = show
        visibilityButton.isHidden = show
        activesView.isHidden = !(!show && segmentedControl.selectedIndex == 0)
        liabilitiesView.isHidden = !(!show && segmentedControl.selectedIndex == 1)
        reloadViewForCurrencyChange()
    }
    
    func reloadViewForCurrencyChange() {
        let hide = UDManager.hideBalance
        let activesBalanceFormatted = presenter.viewModel.overallBalanceFormatted(for: 0,
                                                                                  currency: presenter.selectedCurrency,
                                                                                  isBalancesHidden: hide)
        activesView.selectedCurrency = presenter.selectedCurrency
        activesView.isBalancesHidden = hide
        let vms = presenter.viewModel.activesTabSections
        vms.forEach { $0.isExpanded = false }
        activesView.setSectionsAndReload(presenter.viewModel.activesTabSections,
                                         totalBalanceFormatted: activesBalanceFormatted)
        
        let passivesBalanceFormatted = presenter.viewModel.overallBalanceFormatted(for: 1,
                                                                                   currency: presenter.selectedCurrency,
                                                                                   isBalancesHidden: hide)
        liabilitiesView.selectedCurrency = presenter.selectedCurrency
        liabilitiesView.isBalancesHidden = hide
        liabilitiesView.setSectionsAndReload(presenter.viewModel.passivesTabSections,
                                             totalBalanceFormatted: passivesBalanceFormatted)
    }
}

extension DashboardMyActivesVC: ActivesAndPassivesViewDelegate {
    func tapped(section: Int, in activesAndPassivesView: ActivesAndPassivesView) {
        var segment: Int = 0
        if activesAndPassivesView == liabilitiesView {
            segment = 1
        }
        let sectionsToReload = presenter.viewModel.tapped(section: section, in: segment)
        activesAndPassivesView.reload(sections: sectionsToReload)
    }
    
    func reloadView() {
        reloadViewForCurrencyChange()
    }
}
