//
//  DashboardMyActivesViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import Charts

protocol DashboardMyActivesViewInstaller: ViewInstaller {
    var segmentedControl: EZSegmentedControl! { get set }
    var mainSeparatorLineView: UIView! { get set }
    var activesView: ActivesAndPassivesView! { get set }
    var liabilitiesView: ActivesAndPassivesView! { get set }
    var currenciesSegmentedCtrl: EZVerticalSegmentedControl! { get set }
    var visibilityButton: UIButton! { get set }
    var infoButton: UIButton! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
    var titleLabel: UILabel! {get set}
}

extension DashboardMyActivesViewInstaller {
    func initSubviews() {
        segmentedControl = EZSegmentedControl()
        segmentedControl.addSegment(with: RS.segm_actives.localized())
        segmentedControl.addSegment(with: RS.segm_liabilities.localized())
        segmentedControl.selectedIndex = 0
        segmentedControl.setNeedsLayout()
        
        mainSeparatorLineView = UIView()
        mainSeparatorLineView.backgroundColor = .white
        
        activesView = ActivesAndPassivesView()
        activesView.isHidden = true
        
        liabilitiesView = ActivesAndPassivesView()
        liabilitiesView.isHidden = true
        
        currenciesSegmentedCtrl = EZVerticalSegmentedControl()
        currenciesSegmentedCtrl.addSegment(with: CurrencyType.uzs.data.label, image: nil)
        currenciesSegmentedCtrl.addSegment(with: CurrencyType.usd.data.label, image: nil)
        currenciesSegmentedCtrl.addSegment(with: CurrencyType.eur.data.label, image: nil)
        currenciesSegmentedCtrl.selectedIndex = 0
        
        visibilityButton = UIButton()
        visibilityButton.layer.cornerRadius = Size.VisibilityButton.height/2
        visibilityButton.clipsToBounds = true
        visibilityButton.imageView?.contentMode = .scaleAspectFit
        visibilityButton.setImage(UIImage(named: "img_eye"), for: .normal)
        if #available(iOS 13.0, *) {
            visibilityButton.imageView?.contentMode = .center
            visibilityButton.setImage(UIImage(named: "btn_eye_crossed_white")?.withTintColor(ColorConstants.highlightedGray), for: .selected)
        } else {
            visibilityButton.imageView?.contentMode = .scaleAspectFit
            visibilityButton.setImage(UIImage(named: "btn_eye"), for: .selected)
        }
        visibilityButton.setBackgroundColor(UIColor.black.withAlphaComponent(0.4), for: .normal)
        visibilityButton.setBackgroundColor(.white, for: .selected)
        
        infoButton = UIButton()
        infoButton.layer.cornerRadius = Size.VisibilityButton.height/2
        infoButton.clipsToBounds = true
        infoButton.imageView?.contentMode = .scaleAspectFit
        let infoImage = UIImage(named: "img_icon_info")
        infoButton.setImage(infoImage, for: .normal)
        infoButton.setBackgroundColor(UIColor.black.withAlphaComponent(0.4), for: .normal)
        infoButton.setBackgroundColor(.white, for: .selected)
        infoButton.isHidden = true
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .bold)
        titleLabel.text = "Активы / Пассивы"
    }
    
    func localizeText() {
        let intIndex = segmentedControl.stackView.subviews.distance(from: segmentedControl.stackView.subviews.startIndex, to: segmentedControl.selectedIndex)
        if intIndex == 0 {
            segmentedControl.text = RS.segm_actives.localized()
        } else {
            print("not working")
        }
    }
    
    func embedSubviews() {
        mainView.addSubview(segmentedControl)
        mainView.addSubview(mainSeparatorLineView)
        mainView.addSubview(activesView)
        mainView.addSubview(liabilitiesView)
        mainView.addSubview(currenciesSegmentedCtrl)
        mainView.addSubview(visibilityButton)
        mainView.addSubview(infoButton)
        mainView.addSubview(reloadButtonView)
    }
    
    func addSubviewsConstraints() {
        segmentedControl.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(Adaptive.val(16))
            maker.trailing.equalTo(-Adaptive.val(16))
            maker.top.equalToSuperview().offset(Adaptive.val(63))
        }
        
        mainSeparatorLineView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(1))
            maker.top.equalTo(segmentedControl.snp.bottom)
            maker.leading.trailing.equalToSuperview()
        }
        
        activesView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(mainSeparatorLineView.snp.bottom)
            maker.leading.bottom.trailing.equalToSuperview()
        }
        
        liabilitiesView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(mainSeparatorLineView.snp.bottom)
            maker.leading.bottom.trailing.equalToSuperview()
        }
        
        currenciesSegmentedCtrl.snp.remakeConstraints { (maker) in
            maker.top.equalTo(activesView.snp.top).offset(Adaptive.val(50))
            maker.leading.equalTo(Adaptive.val(16))
            maker.width.equalTo(EZVerticalSegmentedControlSize.width)
            maker.height.equalTo(Adaptive.val(134))
        }
        
        visibilityButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(currenciesSegmentedCtrl.snp.bottom).offset(51)
            maker.leading.equalTo(Adaptive.val(16))
            maker.width.equalTo(EZVerticalSegmentedControlSize.width)
            maker.height.equalTo(Size.VisibilityButton.height)
        }
        
        infoButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(mainSeparatorLineView.snp.bottom).offset(Adaptive.val(33))
            maker.width.equalTo(EZVerticalSegmentedControlSize.width)
            maker.height.equalTo(Size.VisibilityButton.height)
            maker.trailing.equalTo(-Adaptive.val(16))
        }
        
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    struct VisibilityButton {
        static let height = Adaptive.val(34)
    }
}
