//
//  DashboardMyActivesTableSectionHeaderView.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardMyActivesTableSectionHeaderViewDelegate: AnyObject {
    func tapped(sectionHeaderView: DashboardMyActivesTableSectionHeaderView)
}

class DashboardMyActivesTableSectionHeaderView: UITableViewHeaderFooterView, DashboardMyActivesTableSectionHeaderViewInstaller {
    var mainView: UIView { self }
    var countLabel: UILabel!
    var countLabelCoverView: UIView!
    var titleLabel: UILabel!
    var balanceLabel: UILabel!
    var arrowCoverView: UIView!
    var arrowImgView: UIImageView!
    var lineView: UIView!
    
    var stackView: UIStackView!
    var titleVerticalStackView: UIStackView!
    var titleHorizontalStackView: UIStackView!
    
    var valueVerticalStackView: UIStackView!
    var valueHorizontalStackView: UIStackView!
    
    lazy private var tapRecognizer: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(didTapHeader))
    }()
    
    weak var delegate: DashboardMyActivesTableSectionHeaderViewDelegate?
    var sectionIndex: Int = 0
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    private func initialize() {
        setupSubviews()
        addGestureRecognizer(tapRecognizer)
    }
    
    func setup(with count: Int, sectionType: DashboardMyActivesSectionType?, balanceStr: String?, isExpanded: Bool) {
        countLabelCoverView.backgroundColor = sectionType?.sectionColor
        countLabel.text = "\(count)"
        titleLabel.text = sectionType?.title
        balanceLabel.text = balanceStr
        isArrowOpen = isExpanded
    }
    
    @objc private func didTapHeader() {
        delegate?.tapped(sectionHeaderView: self)
        isArrowOpen = !isArrowOpen
    }
    
    var isArrowOpen: Bool = false {
        didSet {
            rotate(arrow: arrowImgView, toValue: isArrowOpen ? .pi : 0.0)
        }
    }
    
    func rotate(arrow: UIImageView, toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
//        animation.beginTime = CACurrentMediaTime() + 0.2; //0.01 seconds delay
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        arrow.layer.add(animation, forKey: nil)
    }
}
