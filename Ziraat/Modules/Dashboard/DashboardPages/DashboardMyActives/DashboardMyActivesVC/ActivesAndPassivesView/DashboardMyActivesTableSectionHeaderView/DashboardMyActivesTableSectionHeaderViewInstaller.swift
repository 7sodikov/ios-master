//
//  DashboardMyActivesTableSectionHeaderViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/27/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardMyActivesTableSectionHeaderViewInstaller: ViewInstaller {
    // declare your UI elements here
    var countLabel: UILabel! { get set }
    var countLabelCoverView: UIView! { get set }
    var titleLabel: UILabel! { get set }
    var balanceLabel: UILabel! { get set }
    var arrowCoverView: UIView! { get set }
    var arrowImgView: UIImageView! { get set }
    
    var lineView: UIView! { get set }
    
    var stackView: UIStackView! { get set }
    var titleVerticalStackView: UIStackView! { get set }
    var titleHorizontalStackView: UIStackView! { get set }
    
    var valueVerticalStackView: UIStackView! { get set }
    var valueHorizontalStackView: UIStackView! { get set }
}

extension DashboardMyActivesTableSectionHeaderViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        
        countLabel = UILabel()
        countLabel.textAlignment = .center
        countLabel.textColor = .white
        countLabel.font = .gothamNarrow(size: 14, .bold)//EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))
        
        countLabelCoverView = UIView()
        countLabelCoverView.backgroundColor = .clear
        countLabelCoverView.layer.cornerRadius = Adaptive.val(2)
        
        titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.font = .gothamNarrow(size: 18, .book)//EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        
        balanceLabel = UILabel()
        balanceLabel.textColor = .white
        balanceLabel.font = .gothamNarrow(size: 16, .bold)//EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        
        arrowCoverView = UIView()
        arrowCoverView.backgroundColor = .clear
        
        arrowImgView = UIImageView()
        arrowImgView.backgroundColor = .clear
        arrowImgView.image = UIImage(named: "btn_down")?.changeColor(.white)
        arrowImgView.contentMode = .scaleAspectFit
        
        lineView = UIView()
        lineView.backgroundColor = ColorConstants.borderLine
        
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.spacing = Size.StackView.spacing
        
        titleVerticalStackView = UIStackView()
        titleVerticalStackView.axis = .vertical
        titleVerticalStackView.alignment = .leading
        
        titleHorizontalStackView = UIStackView()
        titleHorizontalStackView.axis = .horizontal
        titleHorizontalStackView.alignment = .center
        titleHorizontalStackView.spacing = Size.StackView.spacing
        
        valueVerticalStackView = UIStackView()
        valueVerticalStackView.axis = .vertical
        valueVerticalStackView.alignment = .trailing
        
        valueHorizontalStackView = UIStackView()
        valueHorizontalStackView.axis = .horizontal
        valueHorizontalStackView.alignment = .center
        valueHorizontalStackView.spacing = Size.StackView.spacing
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
        countLabelCoverView.addSubview(countLabel)
        mainView.addSubview(lineView)
        
        mainView.addSubview(stackView)
        stackView.addArrangedSubview(titleVerticalStackView)
        titleVerticalStackView.addArrangedSubview(titleHorizontalStackView)
        titleHorizontalStackView.addArrangedSubview(countLabelCoverView)
        titleHorizontalStackView.addArrangedSubview(titleLabel)
        
        stackView.addArrangedSubview(valueVerticalStackView)
        valueVerticalStackView.addArrangedSubview(valueHorizontalStackView)
        valueHorizontalStackView.addArrangedSubview(balanceLabel)
        valueHorizontalStackView.addArrangedSubview(arrowCoverView)
        arrowCoverView.addSubview(arrowImgView)
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        
        stackView.snp.remakeConstraints { (maker) in
            maker.top.bottom.equalToSuperview()
            maker.leading.equalTo(Adaptive.val(16))
            maker.trailing.equalTo(-Adaptive.val(16))
        }
        
        countLabelCoverView.snp.remakeConstraints { (maker) in
            maker.width.greaterThanOrEqualTo(Size.CountLabelCover.size)
            maker.height.equalTo(Size.CountLabelCover.size)
        }
        
        countLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(countLabelCoverView.snp.top).offset(Adaptive.val(5))
            maker.left.equalTo(countLabelCoverView.snp.left).offset(Adaptive.val(5))
            maker.bottom.equalTo(countLabelCoverView.snp.bottom).offset(-Adaptive.val(5))
            maker.right.equalTo(countLabelCoverView.snp.right).offset(-Adaptive.val(5))
        }
        
        arrowCoverView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
        
        arrowImgView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(16))
            maker.center.equalToSuperview()
        }
        
        lineView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(1))
            maker.leading.equalTo(Adaptive.val(16))
            maker.trailing.equalTo(-Adaptive.val(16))
            maker.bottom.equalTo(-Adaptive.val(1))
        }
    }
}

fileprivate struct Size {
    struct CountLabelCover {
        static let size = Adaptive.val(27)
    }
    
    struct StackView {
        static let spacing = Adaptive.val(5)
    }
}
