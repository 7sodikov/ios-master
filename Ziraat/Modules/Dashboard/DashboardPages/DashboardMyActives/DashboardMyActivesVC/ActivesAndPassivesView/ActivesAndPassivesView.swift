//
//  ActivesAndPassivesView.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/15/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import Charts

//enum State {
//    case active
//    case liabilities
//}

protocol ActivesAndPassivesViewDelegate: AnyObject {
    func tapped(section: Int, in activesAndPassivesView: ActivesAndPassivesView)
    func reloadView()
}

class ActivesAndPassivesView: UIView, ActivesAndPassivesViewInstaller {
    var pieChart: PieChartView!
    var tableView: UITableView!
    var mainView: UIView { self }
    private let cellid: String = "\(DashboardMyActivesTableViewCell.self)"
    
    var sectionsDataModel = [DashboardMyActivesSectionVMProtocol]()
    var selectedCurrency: CurrencyType = .uzs
    var isBalancesHidden: Bool = false
    var selectedRowIndex: Int = 0
    
    weak var delegate: ActivesAndPassivesViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setSectionsAndReload(_ sections: [DashboardMyActivesSectionVMProtocol], totalBalanceFormatted: NSAttributedString) {
        sectionsDataModel = sections
        drawPieChart(for: sections, totalBalanceFormatted: totalBalanceFormatted)
        tableView.reloadData()
    }
    
    func drawPieChart(for sections: [DashboardMyActivesSectionVMProtocol], totalBalanceFormatted: NSAttributedString) {
        let dataSet = PieChartDataSet()
        dataSet.selectionShift = 3
        dataSet.valueColors = [.clear]
        dataSet.sliceSpace = 1
        var sliceColors = [UIColor]()
        for section in sections {
            let balance = Double(section.sectionBalance(for: selectedCurrency))
            let entry = PieChartDataEntry(value: balance)
            dataSet.append(entry)
            if let color = section.sectionType?.sectionColor {
                sliceColors.append(color)
            }
        }
        dataSet.colors = sliceColors
        
        let chartData = PieChartData(dataSet: dataSet)
        pieChart.data = chartData
        pieChart.usePercentValuesEnabled = true
//        pieChart.chartDescription?.text = ""
        pieChart.centerAttributedText = totalBalanceFormatted
    }
    
    private func rowPieChart(for viewModel: DashboardMyActivesSectionVMProtocol) {
        let tottalBalance = viewModel.overallBalanceFormatted(currency: selectedCurrency, isBalancesHidden: isBalancesHidden)
        let dataSet = PieChartDataSet()
        dataSet.selectionShift = 3
        dataSet.valueColors = [.clear]
        dataSet.sliceSpace = 1
        dataSet.colors = viewModel.rowColors
        for row in 0 ..< viewModel.numberOfRows {
            let amount = viewModel.rowBalance(for: row, currency: selectedCurrency).convert?.amount ?? .zero
            let balance = Double(amount / 100)
            let entry = PieChartDataEntry(value: balance)
            dataSet.append(entry)
        }
        let chartData = PieChartData(dataSet: dataSet)
        pieChart.data = chartData
        pieChart.usePercentValuesEnabled = true
        pieChart.centerAttributedText = tottalBalance
        pieChart.animate(xAxisDuration: 1, easingOption: ChartEasingOption.easeInOutCubic)
    }
    
    func reload(sections: [Int]) {
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(sections), with: .fade)
        tableView.endUpdates()
        
        if let vm = sectionsDataModel.first(where: \.isExpanded) {
            rowPieChart(for: vm)
        } else {
            delegate?.reloadView()
        }
       
    }
}

extension ActivesAndPassivesView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsDataModel.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionItem = sectionsDataModel[section]
        return sectionItem.actualNumberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! DashboardMyActivesTableViewCell
        let sectionItem = sectionsDataModel[indexPath.section]
        let rowBalance = sectionItem.rowBalanceFormatted(for: indexPath.row,
                                                         currency: selectedCurrency,
                                                         isBalancesHidden: isBalancesHidden)
        cell.backgroundColor = .clear
        return cell.setup(with: rowBalance.rowCurrency?.data.label,
                          formattedBalance: rowBalance.formattedBalance,
                          indicatorColor: sectionItem.rowColors[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(35)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionItem = sectionsDataModel[section]
        let balanceStr = sectionItem.sectionBalanceFormatted(for: selectedCurrency,
                                                             isBalancesHidden: isBalancesHidden)
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: DashboardMyActivesTableSectionHeaderView.self)) as? DashboardMyActivesTableSectionHeaderView
        headerView?.delegate = self
        headerView?.sectionIndex = section
        headerView?.setup(with: sectionItem.numberOfRows,
                          sectionType: sectionItem.sectionType,
                          balanceStr: balanceStr,
                          isExpanded: sectionItem.isExpanded)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Adaptive.val(40)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
    }
}

extension ActivesAndPassivesView: DashboardMyActivesTableSectionHeaderViewDelegate {
    func tapped(sectionHeaderView: DashboardMyActivesTableSectionHeaderView) {
        self.selectedRowIndex = sectionHeaderView.sectionIndex
        delegate?.tapped(section: sectionHeaderView.sectionIndex, in: self)
    }
}
