//
//  DashboardMyActivesTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol DashboardMyActivesTableViewCellInstaller: ViewInstaller {
    // declare your UI elements here
    var colorIndicator: UIView! { get set }
    var titleLabel: UILabel! { get set }
    var balanceLabel: UILabel! { get set }
}

extension DashboardMyActivesTableViewCellInstaller {
    func initSubviews() {
        mainView.clipsToBounds = true
        
        colorIndicator = UIView()
        colorIndicator.backgroundColor = .clear
        colorIndicator.layer.cornerRadius = Adaptive.val(2)
        
        titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.font = .gothamNarrow(size: 18, .medium)//EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        
        balanceLabel = UILabel()
        balanceLabel.textAlignment = .right
        balanceLabel.textColor = .white
        balanceLabel.font = .gothamNarrow(size: 16, .bold)//EZFontType.regular.sfuiDisplay(size: Adaptive.val(16))
    }
    
    func embedSubviews() {
        mainView.addSubview(colorIndicator)
        mainView.addSubview(titleLabel)
        mainView.addSubview(balanceLabel)
    }
    
    func addSubviewsConstraints() {        
        colorIndicator.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Size.Indicator.size)
            maker.leading.equalTo(Size.leftRight)
            maker.centerY.equalToSuperview()
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(colorIndicator.snp.trailing).offset(Size.interItemSpace)
            maker.centerY.equalToSuperview()
        }
        
        balanceLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(titleLabel.snp.trailing).offset(Size.interItemSpace)
            maker.trailing.equalTo(-Size.leftRight)
            maker.centerY.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    static let leftRight = Adaptive.val(25)
    static let interItemSpace = Adaptive.val(5)
    struct Indicator {
        static let size = Adaptive.val(25)
    }
}
