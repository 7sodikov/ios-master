//
//  DashboardMyActivesTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class DashboardMyActivesTableViewCell: UITableViewCell, DashboardMyActivesTableViewCellInstaller {
    var mainView: UIView { contentView }
    var colorIndicator: UIView!
    var titleLabel: UILabel!
    var balanceLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with currency: String?, formattedBalance: String?, indicatorColor: UIColor) -> DashboardMyActivesTableViewCell {
        colorIndicator.backgroundColor = indicatorColor
        titleLabel.text = currency
        balanceLabel.text = formattedBalance
        return self
    }
}
