//
//  ActivesAndPassivesViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/15/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import Charts

protocol ActivesAndPassivesViewInstaller: ViewInstaller {
    var pieChart: PieChartView! { get set }
    var tableView: UITableView! { get set }
}

extension ActivesAndPassivesViewInstaller {
    func initSubviews() {
        pieChart = PieChartView()
        pieChart.holeColor = .clear
        pieChart.legend.enabled = false
        pieChart.holeRadiusPercent = 0.72
            
        tableView = UITableView(frame: .zero, style: .grouped)
//        tableView.contentInsetAdjustmentBehavior = .never
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
//        tableView.separatorColor = ColorConstants.grayishWhite
        tableView.separatorInset = .init(top: 0, left: Adaptive.val(15), bottom: 0, right: Adaptive.val(15))
        tableView.tableFooterView = UIView()
        tableView.register(DashboardMyActivesTableViewCell.self, forCellReuseIdentifier: String(describing: DashboardMyActivesTableViewCell.self))
        tableView.register(DashboardMyActivesTableSectionHeaderView.self, forHeaderFooterViewReuseIdentifier: String(describing: DashboardMyActivesTableSectionHeaderView.self))
    }
    
    func embedSubviews() {
        mainView.addSubview(pieChart)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        pieChart.snp.remakeConstraints { (maker) in
            maker.height.width.equalTo(Adaptive.val(230))
            maker.centerX.equalToSuperview()
            maker.top.equalTo(Adaptive.val(50))
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(pieChart.snp.bottom).offset(Adaptive.val(50))
            maker.leading.bottom.trailing.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
