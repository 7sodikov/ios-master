//
//  MyLiabilitiesInfoVCInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/3/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol MyLiabilitiesInfoVCInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var infoImageView: UIImageView! { get set }
    var infoLabel: UILabel! { get set }
    var okButton: NextButton! { get set }
}

extension MyLiabilitiesInfoVCInstaller {
    func initSubviews() {
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(13)
        
        infoImageView = UIImageView()
        infoImageView.image = UIImage(named: "img_info")
        
        infoLabel =  UILabel()
        infoLabel.font = .gothamNarrow(size: 14, .book)//EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        infoLabel.textColor = .black
        infoLabel.numberOfLines = 0
        let attributedString = NSMutableAttributedString(string: RS.al_msg_loan.localized())
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 2
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        infoLabel.attributedText = attributedString
        
        okButton = NextButton.systemButton(with: "OK", cornerRadius: Size.Button.height/2)
        okButton.setBackgroundColor(ColorConstants.red, for: .normal)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(infoImageView)
        mainView.addSubview(infoLabel)
        mainView.addSubview(okButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.top.equalToSuperview().offset(Adaptive.val(269))
        }
        
        infoImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Size.Image.widthHeight)
            maker.centerX.equalToSuperview()
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(13))
        }
        
        infoLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(infoImageView.snp.bottom).offset(Adaptive.val(5))
            maker.centerX.equalToSuperview()
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(17))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(17))
        }
        
        okButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.Button.height)
            maker.leading.equalTo(backView.snp.leading).offset(Size.Button.leftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.Button.leftRight)
            maker.top.equalTo(infoLabel.snp.bottom).offset(Adaptive.val(10))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(21))
        }
    }
}

fileprivate struct Size {
    struct Button {
        static let height = Adaptive.val(34)
        static let leftRight = Adaptive.val(90)
    }
    
    struct Image {
        static let widthHeight = Adaptive.val(70)
    }
    
    static let paddingLeftRight = Adaptive.val(25)
}
