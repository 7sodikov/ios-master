//
//  MyLiabilitiesInfoVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/3/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class MyLiabilitiesInfoVC: UIViewController, MyLiabilitiesInfoVCInstaller {
    var backView: UIView!
    var infoImageView: UIImageView!
    var infoLabel: UILabel!
    var okButton: NextButton!
    var mainView: UIView { view }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        okButton.addTarget(self, action: #selector(okButtonPressed(_:)), for: .touchUpInside)
    }
    
    @objc func okButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
