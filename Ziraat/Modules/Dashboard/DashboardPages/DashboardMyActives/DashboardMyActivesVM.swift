//
//  DashboardMyActivesVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import RandomColorSwift

enum DashboardMyActivesSectionType {
    case accounts
    case cards
    case deposits
    case credits
    
    var title: String {
        switch self {
        case .accounts: return RS.lbl_accounts.localized()
        case .cards: return RS.lbl_cards.localized()
        case .deposits: return RS.lbl_deposits.localized()
        case .credits: return RS.lbl_loans.localized()
        }
    }
    
    var sectionColor: UIColor {
        switch self {
        case .accounts: return ColorConstants.dashboardBlue
        case .cards: return ColorConstants.dashboardGreen
        case .deposits: return ColorConstants.dashboardYellow
        case .credits: return ColorConstants.dashboardPurple
        }
    }
}

protocol DashboardMyActivesSectionVMProtocol: AnyObject {
    var rowColors: [UIColor] { get }
    var sectionType: DashboardMyActivesSectionType? { get }
    func sectionBalance(for currency: CurrencyType) -> Int
    func sectionBalanceFormatted(for currency: CurrencyType, isBalancesHidden: Bool) -> String?
    var numberOfRows: Int { get }
    var actualNumberOfRows: Int { get }
    func rowBalance(for index: Int, currency: CurrencyType) -> (convert: ConvertResponse?, rowCurrency: CurrencyType?)
    func rowBalanceFormatted(for index: Int, currency: CurrencyType, isBalancesHidden: Bool) -> (formattedBalance: String?, rowCurrency: CurrencyType?)
    func overallBalanceFormatted(currency: CurrencyType, isBalancesHidden: Bool) -> NSAttributedString
    var isExpanded: Bool { get set }
}

class DashboardMyActivesSectionVM: DashboardMyActivesSectionVMProtocol {
    // only one of the arrays can have values
    var accountBalances: [AccountBalanceResponse]?
    var cardBalances: [CardBalanceResponse]?
    var depositBalances: [DepositBalanceResponse]?
    var loanBalances: [LoanBalanceResponse]?
    
    lazy var rowColors: [UIColor] = {
        return randomColors(count: numberOfRows, hue: .random, luminosity: .bright)
    }()
    
    var sectionType: DashboardMyActivesSectionType? {
        var sectionType: DashboardMyActivesSectionType?
        if accountBalances != nil {
            sectionType = .accounts
            
        } else if cardBalances != nil {
            sectionType = .cards
            
        } else if depositBalances != nil {
            sectionType = .deposits
            
        } else if loanBalances != nil {
            sectionType = .credits
            
        }
        return sectionType
    }
    
    func sectionBalance(for currency: CurrencyType) -> Int {
        var overall: Int = 0
        
        if let accountBalances = accountBalances {
            for balance in accountBalances {
                guard let converts = balance.convert else { continue }
                for convert in converts {
                    if convert.currency == currency.data.code {
                        overall += convert.amount ?? 0
                        break
                    }
                }
            }
        } else if let cardBalances = cardBalances {
            for balance in cardBalances {
                guard let converts = balance.convert else { continue }
                for convert in converts {
                    if convert.currency == currency.data.code {
                        overall += convert.amount ?? 0
                        break
                    }
                }
            }
        } else if let depositBalances = depositBalances {
            for balance in depositBalances {
                guard let converts = balance.convert else { continue }
                for convert in converts {
                    if convert.currency == currency.data.code {
                        overall += convert.amount ?? 0
                        break
                    }
                }
            }
        } else if let loanBalances = loanBalances {
            for balance in loanBalances {
                guard let converts = balance.convert else { continue }
                for convert in converts {
                    if convert.currency == currency.data.code {
                        overall += convert.amount ?? 0
                        break
                    }
                }
            }
        }
        
        return overall
    }
    
    func sectionBalanceFormatted(for currency: CurrencyType, isBalancesHidden: Bool) -> String? {
        if isBalancesHidden {
            return "**** \(currency.data.label)"
        }
        
        let balance = sectionBalance(for: currency)
        let formatted = (Double(balance)/100).currencyFormattedStr() ?? ""
        return "\(formatted) \(currency.data.label)"
    }
    
    var numberOfRows: Int {
        if let accountBalances = accountBalances {
            return accountBalances.count
            
        } else if let cardBalances = cardBalances {
            return cardBalances.count
            
        } else if let depositBalances = depositBalances {
            return depositBalances.count
            
        } else if let loanBalances = loanBalances {
            return loanBalances.count
        }
        return 0
    }
    
    var actualNumberOfRows: Int {
        if !isExpanded {
            return 0
        }
        
        return numberOfRows
    }
    
    func rowBalance(for index: Int, currency: CurrencyType) -> (convert: ConvertResponse?, rowCurrency: CurrencyType?) {
        if let accountBalances = accountBalances {
            let balance = accountBalances[index]
            let currencyType = CurrencyType.type(for: balance.currency)
            guard let converts = balance.convert else { return (nil, currencyType) }
            for convert in converts {
                if convert.currency == currency.data.code {
                    return (convert, currencyType)
                }
            }
            
        } else if let cardBalances = cardBalances {
            let balance = cardBalances[index]
            let currencyType = CurrencyType.type(for: balance.currency)
            guard let converts = balance.convert else { return (nil, currencyType) }
            for convert in converts {
                if convert.currency == currency.data.code {
                    return (convert, currencyType)
                }
            }
        } else if let depositBalances = depositBalances {
            let balance = depositBalances[index]
            let currencyType = CurrencyType.type(for: balance.currency)
            guard let converts = balance.convert else { return (nil, currencyType) }
            for convert in converts {
                if convert.currency == currency.data.code {
                    return (convert, currencyType)
                }
            }
            
        } else if let loanBalances = loanBalances {
            let balance = loanBalances[index]
            let currencyType = CurrencyType.type(for: balance.currency)
            guard let converts = balance.convert else { return (nil, currencyType) }
            for convert in converts {
                if convert.currency == currency.data.code {
                    return (convert, currencyType)
                }
            }
        }
        return (nil, nil)
    }
    
    func rowBalanceFormatted(for index: Int, currency: CurrencyType, isBalancesHidden: Bool) -> (formattedBalance: String?, rowCurrency: CurrencyType?) {
        let rowBalance = self.rowBalance(for: index, currency: currency)
        let rowCurrency = CurrencyType.type(for: rowBalance.convert?.currency)?.data.label ?? ""
        
        if isBalancesHidden {
            return ("**** \(rowCurrency)", rowBalance.rowCurrency)
        }
        
        if let balance = rowBalance.convert?.amount,
           let formatted = (Double(balance)/100).currencyFormattedStr() {
            return ("\(formatted) \(rowCurrency)", rowBalance.rowCurrency)
        } else {
            return ("0", rowBalance.rowCurrency)
        }
    }
    
    var isExpanded: Bool = false
    
    func overallBalanceFormatted(currency: CurrencyType, isBalancesHidden: Bool) -> NSAttributedString {
        let balance: Int = sectionBalance(for: currency)
        var formatted = (Double(balance)/100).currencyFormattedStr() ?? ""
        if isBalancesHidden {
            formatted = "****"
        }
        
        let totalTitle = RS.lbl_total_ios.localized()
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
        let totalAttrString = NSAttributedString(
            string: "\(totalTitle) \n",
            attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.43),
                         .paragraphStyle: style,
                         .font: UIFont.gothamNarrow(size: 16, .book)]
        )
        let valueAttrString = NSAttributedString(
            string: "\(formatted) \(currency.data.label)",
            attributes: [.foregroundColor: UIColor.white,
                         .paragraphStyle: style,
                         .font: UIFont.gothamNarrow(size: 18, .bold)]
        )
        let finalAttrString = NSMutableAttributedString()
        finalAttrString.append(totalAttrString)
        finalAttrString.append(valueAttrString)
        
        return finalAttrString
    }
}

class DashboardMyActivesVM {
    var accountBalances: [AccountBalanceResponse] = []
    var cardBalances: [CardBalanceResponse] = []
    var depositBalances: [DepositBalanceResponse] = []
    var loanBalances: [LoanBalanceResponse] = []
    
    private(set) var activesTabSections: [DashboardMyActivesSectionVMProtocol] = []
    private(set) var passivesTabSections: [DashboardMyActivesSectionVMProtocol] = []
    
    func didReceiveAllResponses() {
        activesTabSections.removeAll()
        passivesTabSections.removeAll()
        
        if accountBalances.count >= 0 {
            let balancesVM = DashboardMyActivesSectionVM()
            balancesVM.accountBalances = accountBalances
            activesTabSections.append(balancesVM)
        }
        
        if cardBalances.count >= 0 {
            let balancesVM = DashboardMyActivesSectionVM()
            balancesVM.cardBalances = cardBalances
            activesTabSections.append(balancesVM)
        }
        
        if depositBalances.count >= 0 {
            let balancesVM = DashboardMyActivesSectionVM()
            balancesVM.depositBalances = depositBalances
            activesTabSections.append(balancesVM)
        }
        
        if loanBalances.count >= 0 {
            let balancesVM = DashboardMyActivesSectionVM()
            balancesVM.loanBalances = loanBalances
            passivesTabSections.append(balancesVM)
        }
    }
    
    //    func sections(for selectedSegment: Int) -> [DashboardMyActivesSectionVMProtocol] {
    //        if selectedSegment == 0 { // actives
    //            return activesTabSections
    //        } else { // passives
    //            return passivesTabSections
    //        }
    //    }
    
    func overallBalance(for segment: Int, currency: CurrencyType) -> Int {
        (segment == 0 ? activesTabSections : passivesTabSections)
            .map { $0.sectionBalance(for: currency) }.reduce(0, +)
    }
    
    func overallBalanceFormatted(for segment: Int, currency: CurrencyType, isBalancesHidden: Bool) -> NSAttributedString {
        let balance: Int = overallBalance(for: segment, currency: currency)
        var formatted = (Double(balance)/100).currencyFormattedStr() ?? ""
        if isBalancesHidden {
            formatted = "****"
        }
        
        let totalTitle = RS.lbl_total_ios.localized()
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
        let totalAttrString = NSAttributedString(
            string: "\(totalTitle) \n",
            attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.43),
                         .paragraphStyle: style,
                         .font: UIFont.gothamNarrow(size: 16, .book)]
        )
        let valueAttrString = NSAttributedString(
            string: "\(formatted) \(currency.data.label)",
            attributes: [.foregroundColor: UIColor.white,
                         .paragraphStyle: style,
                         .font: UIFont.gothamNarrow(size: 18, .bold)]
        )
        let finalAttrString = NSMutableAttributedString()
        finalAttrString.append(totalAttrString)
        finalAttrString.append(valueAttrString)
        
        return finalAttrString
    }
    
    func tapped(section: Int, in segment: Int) -> [Int] {
        let sections = segment == 0 ? activesTabSections : passivesTabSections
        
        var sectionsToReload: [Int] = []
        
        for i in 0..<sections.count {
            let sectionModel = sections[i]
            
            if sectionModel.isExpanded || i == section {
                sectionsToReload.append(i)
            }
            
            if i == section {
                sectionModel.isExpanded = !sectionModel.isExpanded
            } else {
                sectionModel.isExpanded = false
            }
        }
        return sectionsToReload
    }
}
