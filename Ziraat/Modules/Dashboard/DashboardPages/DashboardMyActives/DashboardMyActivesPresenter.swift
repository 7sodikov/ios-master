//
//  DashboardMyActivesPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardMyActivesPresenterDelegate: AnyObject {
    func changeTitleName(indexNumber: Int)
}

protocol DashboardMyActivesPresenterProtocol: AnyObject {
    var selectedCurrency: CurrencyType { get set }
    func viewDidLoad()
    func currenciesSegmentClicked(for index: Int)
    func visibilityClicked(_ isBalancesHidden: Bool)
    func segmentChanged(navTitle: String)
    func viewWillAppear(animated: Bool)
}

protocol DashboardMyActivesInteractorToPresenterProtocol: AnyObject {
    func didGetAccountBalances(with response: ResponseResult<AccountsBalanceResponse, AppError>)
    func didGetCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>)
    func didGetDepositBalances(with response: ResponseResult<DepositsBalanceResponse, AppError>)
    func didGetLoanBalances(with response: ResponseResult<LoanBalancesResponse, AppError>)
}

class DashboardMyActivesPresenter: DashboardMyActivesPresenterProtocol {
    weak var view: DashboardMyActivesVCProtocol!
    
    var interactor: DashboardMyActivesInteractorProtocol!
    var router: DashboardMyActivesRouter!
    
    private(set) var viewModel = DashboardMyActivesVM()
    var selectedCurrency: CurrencyType = .uzs
    var isBalancesHidden: Bool = false
    
    func viewDidLoad() {
       
    }
    
    func viewWillAppear(animated: Bool) {
        responsesCount = 0
        successCount = 0
        interactor.accountBalance()
        interactor.cardBalance()
        interactor.depositBalance()
        interactor.loanBalance()
        view.showReloadButtonForResponses(true, animateReloadButton: true)
    }
    
    func currenciesSegmentClicked(for index: Int) {
        switch index {
        case 0: selectedCurrency = .uzs
        case 1: selectedCurrency = .usd
        case 2: selectedCurrency = .eur
        default:
            break
        }
        view.reloadViewForCurrencyChange()
    }
    
    func visibilityClicked(_ isBalancesHidden: Bool) {
        self.isBalancesHidden = isBalancesHidden
        view.reloadViewForCurrencyChange()
    }
    
    func segmentChanged(navTitle: String) {
//        delegate?.changeTitleName(title: navTitle)
    }
    
    // Private property and methods
    
    private var responsesCount = 0
    private var successCount = 0
    private func processResponseCount(isSuccess: Bool) {
        responsesCount += 1
        
        if isSuccess {
            successCount += 1
        }
        
        if responsesCount == 4 {
            if successCount == 4 {
                viewModel.didReceiveAllResponses()
                
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(true, animateReloadButton: false)
            }
        }
    }
}

extension DashboardMyActivesPresenter: DashboardMyActivesInteractorToPresenterProtocol {
    func didGetAccountBalances(with response: ResponseResult<AccountsBalanceResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.accountBalances = result.data.accounts
            processResponseCount(isSuccess: true)
        case .failure(_):
            processResponseCount(isSuccess: false)
        }
    }
    
    func didGetCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cardBalances = result.data.cards ?? []
            processResponseCount(isSuccess: true)
        case .failure(_):
            processResponseCount(isSuccess: false)
        }
    }
    
    func didGetDepositBalances(with response: ResponseResult<DepositsBalanceResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.depositBalances = result.data.deposits ?? []
            processResponseCount(isSuccess: true)
        case .failure(_):
            processResponseCount(isSuccess: false)
        }
    }
    
    func didGetLoanBalances(with response: ResponseResult<LoanBalancesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.loanBalances = result.data.loans ?? []
            processResponseCount(isSuccess: true)
        case .failure(_):
            processResponseCount(isSuccess: false)
        }
    }
}
