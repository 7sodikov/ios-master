//
//  DashboardMyActivesInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardMyActivesInteractorProtocol: AnyObject {
    func accountBalance()
    func cardBalance()
    func depositBalance()
    func loanBalance()
}

class DashboardMyActivesInteractor: DashboardMyActivesInteractorProtocol {
    weak var presenter: DashboardMyActivesInteractorToPresenterProtocol!

    func accountBalance() {
        NetworkService.Accounts.accountBalance() { (result) in
            self.presenter?.didGetAccountBalances(with: result)
        }
    }
    
    func cardBalance() {
        NetworkService.Card.cardBalance() { (result) in
            self.presenter?.didGetCardBalances(with: result)
        }
    }
    
    func depositBalance() {
        NetworkService.Deposit.depositBalance(){ result in
            self.presenter?.didGetDepositBalances(with: result)
        }
    }
    
    func loanBalance() {
        NetworkService.Loan.loanBalance(){ (result) in
            self.presenter?.didGetLoanBalances(with: result) // shunga inogda erro bervotti
        }
    }
}
//
//
//protocol DashboardMyActivesInteractorProtocol: AnyObject {
//    func fetchData(completion: @escaping()->())
//}
//
//class DashboardMyActivesInteractor: DashboardMyActivesInteractorProtocol {
//    weak var presenter: DashboardMyActivesInteractorToPresenterProtocol!
//
//    
//    var cardBalances: [CardBalance] = []
//    var depositBalances: [DepositBalance] = []
//    var accountBalances: [AccountBalance] = []
//    var loanBalances: [LoanBalance] = []
//    
//    func fetchData(completion: @escaping()->()) {
//        let group = DispatchGroup()
//        [accountBalance, cardBalance, depositBalance, loanBalance].forEach { fetchAction in
//            group.enter()
//            fetchAction(group.leave)
//         }
//        
//        group.notify(queue: .main) {
//            completion()
//        }
//    }
//
//    func accountBalance(completion: @escaping()->()) {
//        NetworkService.Accounts.accountBalance() { (result) in
//            if let accounts = try? result.get().accounts.map(\.coreObject) {
//                self.accountBalances = accounts
//            }
//            completion()
//        }
//    }
//    
//    func cardBalance(completion: @escaping()->()) {
//        NetworkService.Card.cardBalance() { (result) in
//            self.presenter?.didGetCardBalances(with: result)
//        }
//    }
//    
//    func depositBalance(completion: @escaping()->()) {
//        NetworkService.Deposit.depositBalance(){ result in
//            self.presenter?.didGetDepositBalances(with: result)
//        }
//    }
//    
//    func loanBalance(completion: @escaping()->()) {
//        NetworkService.Loan.loanBalance(){ (result) in
//            self.presenter?.didGetLoanBalances(with: result) // shunga inogda erro bervotti
//        }
//    }
//}
