//
//  DashboardMyActivesRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardMyActivesRouterProtocol: class {
    static func createModule(_ delegate : DashboardMyActivesPresenterDelegate) -> BaseMainSwipableViewController
    func navigateToOTP(in navigationCtrl: Any)
}

class DashboardMyActivesRouter: DashboardMyActivesRouterProtocol {
    static func createModule(_ delegate : DashboardMyActivesPresenterDelegate) -> BaseMainSwipableViewController {
        let vc = DashboardMyActivesVC()
        let presenter = DashboardMyActivesPresenter()
        let interactor = DashboardMyActivesInteractor()
        let router = DashboardMyActivesRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        vc.delegate = delegate
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToOTP(in navigationCtrl: Any) {
        let confirmationCtrl = ResetPasswordRouter.createModule(isRedStyle: true)
        (navigationCtrl as? UINavigationController)?.pushViewController(confirmationCtrl, animated: true)
    }
}
