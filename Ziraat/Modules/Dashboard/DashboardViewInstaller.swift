//
//  DashboardViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/8/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardViewInstaller: ViewInstaller {
    var bgImageView: UIImageView! { get set }
    var bgImageCollectionLayout: UICollectionViewFlowLayout! { get set }
    var bgImageCollectionView: UICollectionView! { get set }
//    var titleCollectionLayout: UICollectionViewFlowLayout! { get set }
//    var titleCollectionView: UICollectionView! { get set }
    var logoImageView: UIButton! { get set }
    var navigationTitleLabel: UILabel! { get set }
    var navImageView: UIImageView! { get set }
    var notificationButton: UIButton! { get set }
    var pageControl: PageControl! { get set }
    var pageControllerCoverView: UIView! { get set }
    var pageController: PageController! { get set }
    var bottomPanel: BottomPanel! { get set }
    var yearLabel: UILabel! { get set }
}

extension DashboardViewInstaller {
    func initSubviews() {
        bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "img_dash_light_background")
        
        bgImageCollectionLayout = UICollectionViewFlowLayout()
        bgImageCollectionLayout.sectionInset = .zero
        bgImageCollectionLayout.scrollDirection = .horizontal
        bgImageCollectionLayout.minimumInteritemSpacing = 0
        bgImageCollectionLayout.minimumLineSpacing = 0
        
        bgImageCollectionView = UICollectionView(frame: .zero, collectionViewLayout: bgImageCollectionLayout)
        bgImageCollectionView.showsHorizontalScrollIndicator = false
        bgImageCollectionView.showsVerticalScrollIndicator = false
        bgImageCollectionView.isScrollEnabled = false
        bgImageCollectionView.isPagingEnabled = true
        bgImageCollectionView.backgroundColor = .clear
        bgImageCollectionView.register(BGImageCollectionCell.self,
                                forCellWithReuseIdentifier: String(describing: BGImageCollectionCell.self))
        bgImageCollectionView.isUserInteractionEnabled = false
        
//        titleCollectionLayout = UICollectionViewFlowLayout()
//        titleCollectionLayout.sectionInset = .zero
//        titleCollectionLayout.scrollDirection = .horizontal
//        titleCollectionLayout.minimumInteritemSpacing = 0
//        titleCollectionLayout.minimumLineSpacing = 0
//
//        titleCollectionView = UICollectionView(frame: .zero, collectionViewLayout: titleCollectionLayout)
//        titleCollectionView.showsHorizontalScrollIndicator = false
//        titleCollectionView.showsVerticalScrollIndicator = false
//        titleCollectionView.isScrollEnabled = false
//        titleCollectionView.isPagingEnabled = true
//        titleCollectionView.backgroundColor = .clear
//        titleCollectionView.register(TitleCollectionViewCell.self,
//                                forCellWithReuseIdentifier: String(describing: TitleCollectionViewCell.self))
//        titleCollectionView.isUserInteractionEnabled = false
        
        logoImageView = UIButton()
        logoImageView.setBackgroundImage(UIImage(named: "img_dash_power"), for: .normal) 
        logoImageView.layoutIfNeeded()
        logoImageView.subviews.first?.contentMode = .scaleAspectFit
        
        navigationTitleLabel = UILabel()
        navigationTitleLabel.textAlignment = .center
        navigationTitleLabel.textColor = .white
        navigationTitleLabel.font = .gothamNarrow(size: 16, .bold) //EZFontType.bold.sfuiDisplay(size: Adaptive.val(18))
        
        navImageView = UIImageView()
        navImageView.image = UIImage(named: "img_logo_with_text")
        navImageView.isHidden = true
        navImageView.contentMode = .scaleAspectFit
        
        notificationButton = UIButton()
        notificationButton.layer.cornerRadius = Size.AvatarImage.widthHeight/2
        notificationButton.setBackgroundImage(UIImage(named: "icon_dark_bell"), for: .normal)
        
        pageControl = PageControl()
        pageControl.isUserInteractionEnabled = false
        pageControl.contentHorizontalAlignment = .leading
                
        pageControllerCoverView = UIView()
        
        pageController = PageController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        
        bottomPanel = BottomPanel()
        bottomPanel.menuImageView.isHidden = false
        bottomPanel.homeImageView.isHidden = true
        bottomPanel.menuLabel.isHidden = false
        
        yearLabel = UILabel()
        yearLabel.font = .gothamNarrow(size: 14, .book) //EZFontType.regular.sfuiDisplay(size: Adaptive.val(12))
        yearLabel.textColor = .black
        yearLabel.text = "2020 - Ziraat Bank Uzbekistan"
        yearLabel.isHidden = true
    }
    
//    func localizeText() {
//        navigationTitleLabel.text = RS.lbl_my_actives.localized()
//        navigationTitleLabel.text = RS.lbl_accounts.localized()
//        navigationTitleLabel.text = RS.lbl_cards.localized()
//        navigationTitleLabel.text = RS.lbl_last_transactions.localized()
//        navigationTitleLabel.text = RS.lbl_loan_calc.localized()
//        navigationTitleLabel.text = RS.lbl_currency.localized()
//    }
    
    func embedSubviews() {
        mainView.addSubview(bgImageView)
        mainView.addSubview(bgImageCollectionView)
//        mainView.addSubview(titleCollectionView)
        mainView.addSubview(logoImageView)
        mainView.addSubview(notificationButton)
        mainView.addSubview(navigationTitleLabel)
        mainView.addSubview(pageControl)
        mainView.addSubview(pageControllerCoverView)
        mainView.addSubview(bottomPanel)
        mainView.addSubview(navImageView)
        mainView.addSubview(yearLabel)
    }
    
    func addSubviewsConstraints() {
        bgImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        bgImageCollectionView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
//        titleCollectionView.snp.remakeConstraints { (maker) in
//            maker.height.equalTo(Adaptive.val(50))
//            maker.left.right.equalToSuperview()
//            maker.top.equalToSuperview().offset(Adaptive.val(50))
//        }
        
        logoImageView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(58))
            maker.leading.equalToSuperview().offset(Adaptive.val(26))
            maker.width.equalTo(Adaptive.val(25))
            maker.height.equalTo(Adaptive.val(26))
        }
        
        navigationTitleLabel.snp.remakeConstraints { (maker) in
            maker.width.equalTo(UIScreen.main.bounds.width * 0.7)
            maker.centerX.equalToSuperview()
            maker.top.equalToSuperview().offset(Adaptive.val(63))
        }
        
        navImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(120))
            maker.height.equalTo(Adaptive.val(28))
            maker.top.equalToSuperview().offset(Adaptive.val(58))
            maker.centerX.equalToSuperview()
        }
        
        notificationButton.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(57))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            maker.width.height.equalTo(Size.AvatarImage.widthHeight)
        }
        
        pageControl.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(90))
//            maker.left.equalTo(Size.leftRightPadding)
            maker.leading.equalToSuperview().offset(-Adaptive.val(10))
            maker.right.equalTo(-Size.leftRightPadding)
            maker.height.equalTo(Adaptive.val(8))
        }
        
        pageControllerCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(navigationTitleLabel.snp.top)
            maker.leading.trailing.equalToSuperview()
            maker.bottom.equalTo(bottomPanel.snp.top)
        }
        
        bottomPanel.snp.remakeConstraints { (maker) in
            maker.leading.bottom.trailing.equalToSuperview()
            maker.height.equalTo(Size.BottomPanel.height)
        }
        
        yearLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(40))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(25))
        }
    }
}

fileprivate struct Size {
    static let leftRightPadding = Adaptive.val(10)
    
    struct AvatarImage {
        static let widthHeight = Adaptive.val(30)
    }
    struct BottomPanel {
        static let height = Adaptive.val(95)
    }
}
