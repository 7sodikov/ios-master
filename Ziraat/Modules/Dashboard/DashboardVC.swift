//
//  DashboardVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/8/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

enum DashboardCellType {
    case pieChart
    case accounts
    case cards
    case transactions
}

protocol DashboardVCProtocol: AnyObject {
    var navigationCtrl: Any { get }
    func setupUI(for pageIndex: Int, real: Bool)
    func navigateToSpecificPage(index: Int, completion: @escaping (_ vc: UIViewController?) -> Void)
    func reloadCollectionView()
    func reloadFirstPage()
}

class DashboardVC: BaseViewController, DashboardViewInstaller {
    var mainView: UIView { view }
    var bgImageView: UIImageView!
    var bgImageCollectionLayout: UICollectionViewFlowLayout!
    var bgImageCollectionView: UICollectionView!
    var logoImageView: UIButton!
    var navigationTitleLabel: UILabel!
    var navImageView: UIImageView!
    var notificationButton: UIButton!
    var pageControl: PageControl!
    var pageControllerCoverView: UIView!
    var pageController: PageController!
    var bottomPanel: BottomPanel!
    var yearLabel: UILabel!
    
    var titleMyActivesIndex: Int?
    
    var colorForStatusBar: UIStatusBarStyle = .lightContent
    var indexForPage: Int = 0
    
    var colors: [UIColor] = [UIColor.red, UIColor.blue, UIColor.green, UIColor.yellow]
    
    var presenter: DashboardPresenterProtocol!
    
    private var lastIndex: Int = 0
    
    var myControllers : [BaseMainSwipableViewController] = [//DashboardMyActivesRouter.createModule(),
        DashboardAccountsRouter.createModule(),
        //DashboardCardsRouter.createModule(self),
        DashboardLastTransactionsRouter.createModule(),
        DashboardLoanCalculatorRouter.createModule(),
        DashboardCurrencyRouter.createModule(),
        DashboardCallCentreRouter.createModule()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myActivesRoute = DashboardMyActivesRouter.createModule(self)
        myControllers.insert(myActivesRoute, at: 0)
        
        let route = DashboardCardsRouter.createModule(self.presenter as! DashboardCardsActionProtocol)
        myControllers.insert(route, at: 2)
        
        for ind in 0 ..< myControllers.count {
            myControllers[ind].initBaseNavLabel(ind)
        }
        
        setupSubviews()
        setNavigation()
        
        bgImageCollectionView.dataSource = self
        bgImageCollectionView.delegate = self
        
        //        titleCollectionView.dataSource = self
        //        titleCollectionView.delegate = self
        
        pageControl.numberOfPages = myControllers.count
        
        indexForPage = pageControl.currentPage
        
        pageController.open(in: self,
                            parentView: pageControllerCoverView,
                            pages: myControllers)
        pageController.pageControllerDelegate = self
        
        bottomPanel.delegate = self
        
        logoImageView.addTarget(self, action: #selector(logoutButtonClicked(_:)), for: .touchUpInside)
        notificationButton.addTarget(self, action: #selector(notitificationButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(true, animated: true)
        reloadFirstPage()
    }
    
    func reloadFirstPage() {
        //        myControllers.removeFirst()
        //        let myActivesRoute = DashboardMyActivesRouter.createModule(self)
        //        myControllers.insert(myActivesRoute, at: 0)
        bgImageCollectionView.reloadData()
    }
    
    func setNavigation() {
        navigationTitleLabel.text = RS.lbl_my_actives.localized()
        navigationTitleLabel.isHidden = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: logoImageView)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
    @objc private func notitificationButtonClicked(_ sender: UIButton) {
        presenter.notificationButtonClicked()
    }
    
    @objc private func logoutButtonClicked(_ sender: UIButton) {
        presenter.logoutButtonClicked(in: navigationCtrl)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return colorForStatusBar
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent);
        
        if parent == nil
        {
            setColorForStatusBar(color: .lightContent, true)
        }
    }
}

extension DashboardVC: DashboardVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func navigateToSpecificPage(index: Int, completion: @escaping (_ vc: UIViewController?) -> Void) {
        var direction: UIPageViewController.NavigationDirection = .forward
        
        if pageControl.currentPage < index {
            direction = .forward;
        } else {
            direction = .reverse;
        }
        
        //        pageController.setViewControllers([myControllers[0]], direction: direction, animated: true) { _ in
        //            self.pageController.setViewControllers([self.myControllers[0]], direction: direction, animated: false){ _ in
        //                self.pageControl.currentPage = index
        //                completion(self.myControllers[0])
        //            }
        //        }
        //        weak var weakPageVc = pageController
                    DispatchQueue.main.async {
                        self.pageController.setViewControllers([self.myControllers[0]], direction: direction, animated: false) { _ in
                            self.pageControl.currentPage = index
                            completion(self.myControllers[0])
                        }
                    }
        
//        pageController.setViewControllers([myControllers[0]], direction: direction, animated: true) { finished in
//            self.pageControl.currentPage = index
//            completion(self.myControllers[0])
//        }
    }
    
    func reloadCollectionView() {
        self.pageController.pageControllerDelegate?.pageController(didScrollToOffsetX: 0, percent: 0.2857)
        self.bgImageCollectionView.scrollRectToVisible(CGRect(x: self.bgImageCollectionView.contentSize.width * 0.2857, y: 0,
                                                              width: self.bgImageCollectionView.frame.width,
                                                              height: self.bgImageCollectionView.frame.height), animated: false)
    }
    
    func setColorForStatusBar(color: UIStatusBarStyle,_ real: Bool) {
        if real {
            colorForStatusBar = color
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    func setupUI(for pageIndex: Int, real: Bool) {
        guard presenter.viewModel.userEntry.currentSelectedPageIndex == pageIndex else {
            return
        }
        if real {
            self.pageControl.currentPage = pageIndex
        }
        //        var bgImage: UIImage?
        if pageIndex % 2 == 0 {
            logoImageView.setBackgroundImage(UIImage(named: "icon_logo_white"), for: .normal)
            //            notificationButton.setBackgroundImage(UIImage(named: "icon_bell"), for: .normal)
            navigationTitleLabel.textColor = .white
            pageControl.pageIndicatorTintColor = UIColor.dashDarkBlue.withAlphaComponent(0.33)
            pageControl.currentPageIndicatorTintColor = .white
        } else {
            logoImageView.setBackgroundImage(UIImage(named: "icon_logo_red"), for: .normal)
            //            notificationButton.setBackgroundImage(UIImage(named: "icon_dark_bell"), for: .normal)
            navigationTitleLabel.textColor = ColorConstants.mainTextColor
            pageControl.pageIndicatorTintColor = UIColor.dashDarkBlue.withAlphaComponent(0.25)
            pageControl.currentPageIndicatorTintColor = .dashDarkBlue
        }
        
        //        UIView.transition(with: bgImageView,
        //                          duration: 0.2,
        //                          options: .transitionCrossDissolve,
        //                          animations: {
        //                            self.bgImageView.image = bgImage
        //        }, completion: nil)
        
        navImageView.isHidden = (pageIndex != 6)
        
        if pageIndex == 0 {
            changeTitleName(indexNumber: titleMyActivesIndex ?? 0)
            logoImageView.setBackgroundImage(UIImage(named: "img_dash_power"), for: .normal)
            setColorForStatusBar(color: .lightContent,real)
        } else if pageIndex == 1 {
            navigationTitleLabel.text = RS.lbl_accounts.localized()
            setColorForStatusBar(color: .default,real)
        } else if pageIndex == 2 {
            navigationTitleLabel.text = RS.lbl_cards.localized()
            setColorForStatusBar(color: .lightContent,real)
        } else if pageIndex == 3 {
            navigationTitleLabel.text = RS.lbl_last_transactions.localized()
            setColorForStatusBar(color: .default,real)
        } else if pageIndex == 4 {
            navigationTitleLabel.text = RS.lbl_loan_calc.localized()
            setColorForStatusBar(color: .lightContent,real)
        } else if pageIndex == 5 {
            navigationTitleLabel.text = RS.lbl_exchange.localized()
            setColorForStatusBar(color: .default,real)
        } else if pageIndex == 6 {
            navigationTitleLabel.text = ""//RS.nav_ttl_contact_center.localized()
            setColorForStatusBar(color: .lightContent,real)
        }
    }
}

extension DashboardVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myControllers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(BGImageCollectionCell.self)", for: indexPath) as! BGImageCollectionCell
        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 4 {
            cell.mainView.backgroundColor = UIColor.dashDarkBlue.withAlphaComponent(0.5)
        } else if indexPath.row == 6 {
            cell.mainView.backgroundColor = UIColor.dashDarkBlue.withAlphaComponent(0.7)
        } else {
            cell.mainView.backgroundColor = .clear
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
}

extension DashboardVC: BottomPanelDelegate {
    func searchButtonClicked() {
        presenter.unavailableButtonClicked()
    }
    
    func rightButtonClicked() {
        presenter.menuButtonClicked()
        if indexForPage == 0 || indexForPage == 2 || indexForPage == 4 || indexForPage == 6 {
            colorForStatusBar = .default
        }
    }
}

extension DashboardVC: PageControllerDelegate, DashboardMyActivesPresenterDelegate {
    func changeTitleName(indexNumber: Int) {
        titleMyActivesIndex = indexNumber
        if titleMyActivesIndex == 0 {
            navigationTitleLabel.text = RS.lbl_my_actives.localized()
        } else if titleMyActivesIndex == 1 {
            navigationTitleLabel.text = RS.lbl_my_passives.localized()
        }
    }
    
    func pageController(didScrollToOffsetX offsetX: CGFloat, percent: CGFloat) {
        bgImageCollectionView
            .scrollRectToVisible(
                CGRect(
                    x: bgImageCollectionView.contentSize.width * percent,
                    y: 0,
                    width: bgImageCollectionView.frame.width,
                    height: bgImageCollectionView.frame.height
                ),
                animated: false
            )
        
    }
    
    func pageController(didMoveToIndex index: Int) {
        presenter.pageController(didMoveToIndex: index, real: true)
        print("TEST INFO deriction w \(UIScreen.main.bounds.width), h \(UIScreen.main.bounds.height)")
        if let label = self.myControllers[index].baseNavigationTitleLabel {
            let curFrame: CGRect = label.frame
            label.frame = CGRect(x: 0, y: curFrame.minY, width: curFrame.width, height: curFrame.height)
        }
    }
    
    func pageController(deriction: Int, didScrollToOffsetX offsetX: CGFloat, percent: CGFloat) {
        
        let windowWidth: CGFloat = UIScreen.main.bounds.width
        let offsetValue: CGFloat = CGFloat(Int(offsetX) % Int(windowWidth))
        let per: CGFloat = offsetValue / windowWidth
        let index: Int = Int(offsetX) / Int(windowWidth)
        if per >= 0.5 {
            self.pageControl.currentPage = index + 1
        } else {
            self.pageControl.currentPage = index
        }
        self.setupUI(for: self.pageControl.currentPage, real: false)
        print("TEST INFO deriction \(deriction), \(offsetX), \(percent), \(per), \(offsetValue)")
        
        if deriction > 0 {
            let curIndex: Int = index
            var nextIndex: Int = index + 1
            if nextIndex < 0 {nextIndex = 0}
            if nextIndex >= self.pageControl.numberOfPages {
                nextIndex = self.pageControl.numberOfPages - 1}
            if let label = self.myControllers[curIndex].baseNavigationTitleLabel {
                let curFrame: CGRect = label.frame
                label.frame = CGRect(x: offsetValue, y: curFrame.minY, width: curFrame.width, height: curFrame.height)
            }
            if let label = self.myControllers[nextIndex].baseNavigationTitleLabel {
                let curFrame: CGRect = label.frame
                label.frame = CGRect(x: 0 - (windowWidth - offsetValue), y: curFrame.minY, width: curFrame.width, height: curFrame.height)
            }
        } else {
            let curIndex: Int = index + 1
            var nextIndex: Int = index
            if nextIndex < 0 {nextIndex = 0}
            if nextIndex >= self.pageControl.numberOfPages {
                nextIndex = self.pageControl.numberOfPages - 1}
            if let label = self.myControllers[curIndex].baseNavigationTitleLabel {
                let curFrame: CGRect = label.frame
                label.frame = CGRect(x: 0 - (windowWidth - offsetValue), y: curFrame.minY, width: curFrame.width, height: curFrame.height)
            }
            if let label = self.myControllers[nextIndex].baseNavigationTitleLabel {
                let curFrame: CGRect = label.frame
                label.frame = CGRect(x: offsetValue, y: curFrame.minY, width: curFrame.width, height: curFrame.height)
            }
        }
        
    }
    
}


extension UILabel {
    func imageIn(size: CGSize) -> UIImage? {
        let label = self
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
    func setTitleFor(pageIndex: Int) {
        if pageIndex % 2 == 0 {
            self.textColor = .white
        } else {
            self.textColor = ColorConstants.mainTextColor
        }
        if pageIndex == 0 {
            self.text = RS.lbl_my_actives.localized()
        } else if pageIndex == 1 {
            self.text = RS.lbl_accounts.localized()
        } else if pageIndex == 2 {
            self.text = RS.lbl_cards.localized()
        } else if pageIndex == 3 {
            self.text = RS.lbl_last_transactions.localized()
        } else if pageIndex == 4 {
            self.text = RS.lbl_loan_calc.localized()
        } else if pageIndex == 5 {
            self.text = RS.lbl_exchange.localized()
        } else if pageIndex == 6 {
            self.text = ""
        }
    }
}


