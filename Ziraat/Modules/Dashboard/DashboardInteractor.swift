//
//  DashboardInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/8/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DashboardInteractorProtocol: class {
    func logout()
}

class DashboardInteractor: DashboardInteractorProtocol {
    weak var presenter: DashboardInteractorToPresenterProtocol!
    func logout() {
        NetworkService.Auth.logout { _ in
            
        }
    }
}
