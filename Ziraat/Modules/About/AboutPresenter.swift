//
//  AboutPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 12/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AboutPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: AboutVM { get }
}

protocol AboutInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class AboutPresenter: AboutPresenterProtocol {
    weak var view: AboutVCProtocol!
    var interactor: AboutInteractorProtocol!
    var router: AboutRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = AboutVM()
    
    // Private property and methods
    
}

extension AboutPresenter: AboutInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
