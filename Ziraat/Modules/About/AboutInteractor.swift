//
//  AboutInteractor.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 12/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AboutInteractorProtocol: class {
    
}

class AboutInteractor: AboutInteractorProtocol {
    weak var presenter: AboutInteractorToPresenterProtocol!
    
}
