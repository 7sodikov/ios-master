//
//  AboutViewInstaller.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 12/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AboutViewInstaller: ViewInstaller {
    var bgView : UIImageView! {get set}
    var infoBtn : UIButton! {get set}
    var textView : ClipboardControlledTextView! {get set}
    var versionContainer : UIView! {get set}
    var versionLabel : UILabel! {get set}
    var numberLabel : UILabel! {get set}
}

extension AboutViewInstaller {
    func initSubviews() {
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        bgView.contentMode = .scaleAspectFill
        bgView.clipsToBounds = true
        
        infoBtn = UIButton()
        let image = UIImage(named: "img_info")
        infoBtn.setImage(image, for: .normal)
        infoBtn.setImage(image, for: .selected)
        infoBtn.setImage(image, for: .highlighted)
        
        textView = ClipboardControlledTextView()
        textView.backgroundColor = .clear
        textView.isScrollEnabled = false
        textView.textAlignment = .center
        textView.dataDetectorTypes = [.phoneNumber, .link]
        textView.font = EZFontType.regular.sfProText(size: 16)
        textView.textColor = .black
        
        versionContainer = UIView()
        versionContainer.layer.cornerRadius = 4
        versionContainer.layer.borderWidth = 1
        versionContainer.layer.borderColor = UIColor.gray.cgColor
        versionContainer.backgroundColor = .white
        
        versionLabel = UILabel()
        versionLabel.text = RS.lbl_version_number.localized()
        versionLabel.font = EZFontType.regular.sfProText(size: 16)
        versionLabel.textColor = .black
        
        numberLabel = UILabel()
        numberLabel.text = RS.lbl_version_number.localized()
        numberLabel.font = EZFontType.regular.sfProText(size: 16)
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        numberLabel.text =  version + "." + build
        numberLabel.textAlignment = .right
        numberLabel.textColor = .black
        
        let text1 = RS.lbl_app_info.localized()
        
        let text2 = RS.lbl_contact_details_ios.localized()
        textView.text = text1 + "\n\n\n" + text2
        textView.isEditable = false
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(infoBtn)
        mainView.addSubview(textView)
        mainView.addSubview(versionContainer)
        
        versionContainer.addSubview(versionLabel)
        versionContainer.addSubview(numberLabel)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.remakeConstraints { (make) in
            make.left.right.bottom.top.equalToSuperview()
        }
        
        infoBtn.snp.remakeConstraints { (make) in
            make.width.height.equalTo(Size.InfoBtn.size)
            make.centerX.equalToSuperview()
            make.top.equalTo(Adaptive.val(90))
        }
        
        textView.snp.remakeConstraints { (make) in
            make.left.equalTo(Adaptive.val(30))
            make.right.equalTo(Adaptive.val(-30))
            make.top.equalTo(infoBtn.snp.bottom).offset(Adaptive.val(40))
            make.bottom.equalTo(versionContainer.snp.top).offset(20).priority(250)
        }
        
        versionContainer.snp.remakeConstraints { (make) in
            make.left.equalTo(Adaptive.val(30))
            make.right.equalTo(Adaptive.val(-30))
            make.height.equalTo(Adaptive.val(40))
            make.bottom.equalTo(Adaptive.val(-40))
        }
        
        versionLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(5)
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
        }
        
        numberLabel.snp.remakeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.equalTo(-5)
            make.width.equalToSuperview().multipliedBy(0.5)
        }
    }
}

fileprivate struct Size {
    struct InfoBtn {
        static let size = Adaptive.val(62)
    }
}
