//
//  AboutVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 12/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AboutVCProtocol: class {
    
}

class AboutVC: BaseViewController, AboutViewInstaller {
    var mainView: UIView { view }
    var bgView : UIImageView!
    
    var infoBtn : UIButton!
    var textView : ClipboardControlledTextView!
    var versionContainer : UIView!
    var versionLabel : UILabel!
    var numberLabel : UILabel!
    var presenter: AboutPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RS.lbl_about_app.localized()
        setupSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: false)
    }
}

extension AboutVC: AboutVCProtocol {
    
}
