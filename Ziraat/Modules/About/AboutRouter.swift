//
//  AboutRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 12/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AboutRouterProtocol: class {
    static func createModule() -> UIViewController
}

class AboutRouter: AboutRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = AboutVC()
        let presenter = AboutPresenter()
        let interactor = AboutInteractor()
        let router = AboutRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
