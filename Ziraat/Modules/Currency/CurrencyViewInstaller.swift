//
//  CurrencyViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CurrencyViewInstaller: ViewInstaller {
    var backgorundImage: UIImageView! {get set}
    var tableView: UITableView! { get set }
    var indicator: UIActivityIndicatorView!{ get set}
}

extension CurrencyViewInstaller {
    
    func initSubviews() {
        backgorundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        } else {
            indicator = UIActivityIndicatorView()
        }
        indicator.hidesWhenStopped = true
        
        tableView = .init()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.tableFooterView = .init()
        tableView.allowsSelection = false
    }
    
    func embedSubviews() {
        mainView.addSubview(backgorundImage)
        mainView.addSubview(tableView)
        mainView.addSubview(indicator)
    }
    
    func addSubviewsConstraints() {
        backgorundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        
        tableView.constraint { (make) in
            make.top(16).horizontal.bottom.equalToSuperView()
        }
        
        indicator.constraint { (make) in
            make.center.equalToSuperView()
        }
    }
    
}
