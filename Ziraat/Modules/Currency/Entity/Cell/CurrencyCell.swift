//
//  CurrencyCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class CurrencyCell: TableViewCell, CurrencyCellViewInstaller {
    
    var currencyLabel: UILabel!
    var currencyStackView: UIStackView!
    var flagImageView: UIImageView!
    var buyLabel: UILabel!
    var buyBalanceLabel: UILabel!
    var buyStackView: UIStackView!
    var sellLabel: UILabel!
    var sellBalanceLabel: UILabel!
    var sellStackView: UIStackView!
    var cbLabel: UILabel!
    var cbBalanceLabel: UILabel!
    var cbStackView: UIStackView!
    var currenciesStackView: UIStackView!
    var currenciesWithFlagStackView: UIStackView!
    var mainStackView: UIStackView!
    
    var mainView: UIView { self.contentView }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        setupSubviews()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? CurrencyCellViewModel else { return }

        currencyLabel.text = viewModel.codeLabel
        flagImageView.kf.setImage(with: URL(string: viewModel.flagUrl), placeholder: nil)
        buyBalanceLabel.text = viewModel.buy
        sellBalanceLabel.text = viewModel.sell
        cbBalanceLabel.text = viewModel.cb
        
    }
    
}
