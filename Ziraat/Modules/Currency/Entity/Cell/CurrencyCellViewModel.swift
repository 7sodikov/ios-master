//
//  CurrencyCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class CurrencyCellViewModel: TableViewModel {

 
    override var cellType: TableViewCell.Type {
        CurrencyCell.self
    }
    
    var flagUrl: String
    var buy: String
    var sell: String
    var cb: String
    var codeLabel: String
        
    init(flagUrl: String, buy: String, sell: String, cb: String, codeLabel: String) {
        self.flagUrl = flagUrl
        self.buy = buy
        self.sell = sell
        self.cb = cb
        self.codeLabel = codeLabel
    }
    
    
}
