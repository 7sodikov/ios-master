//
//  CurrencyViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CurrencyDisplayLogic:  CurrencyViewInstaller {
  
}

class CurrencyViewController: BaseViewController, CurrencyDisplayLogic {
    
    var indicator: UIActivityIndicatorView!
    var backgorundImage: UIImageView!
    var tableView: UITableView!
    var mainView: UIView { self.view }
    var presenter: CurrencyPresentationLogic!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        presenter.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CurrencyCell.self)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = RS.lbl_exchange.localized()
        (self.navigationController as? NavigationController)?.background(isWhite: false)
    }
    
    @objc func refreshControlValueChanged() {
        presenter.fetch()
    }
}

extension CurrencyViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        return cell
    }
    
}
