//
//  CurrencyInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CurrencyBusinessLogic {
    var viewModels: [CurrencyCellViewModel] {get}
    
    var showError: ((String) -> Void)? {get set}
    func fetchRates(completion: @escaping () -> Void)
}

class CurrencyInteractor: CurrencyBusinessLogic {
    
    var rateDictionary: [String: [ExchangeRateResponse]] = .init()
    var currencies: [CurrencyResponse] = []
    
    var showError: ((String) -> Void)?
    
    private var rateVeiwModels: [CurrencyCellViewModel] = []
    
    var viewModels: [CurrencyCellViewModel] {
        rateVeiwModels
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    private func configure() {
        rateVeiwModels = currencies.compactMap { (currency) -> CurrencyCellViewModel? in
            
            guard let code = currency.code, let rates = rateDictionary[code] else {
                return nil
            }
            let courseTypes = Dictionary(grouping: rates, by: \.courseType!)
            
            let codeLabel: String = currency.codeLabel ?? ""
            let flagUrl = courseTypes["5"]?.first?.flag ?? ""
            let buy = formattedMoney(courseTypes["5"]?.first?.course) ?? ""
            let sell = formattedMoney(courseTypes["4"]?.first?.course) ?? ""
            let cb = formattedMoney(courseTypes["1"]?.first?.course) ?? ""
            
            return .init(flagUrl: flagUrl, buy: buy, sell: sell, cb: cb, codeLabel: codeLabel)
        }
    }
    
    func fetchRates(completion: @escaping () -> Void) {
        let queue = DispatchGroup()
        
        queue.enter()
        queue.enter()
        
        
        NetworkService.Info.getMainExchangeRates { (response) in
            switch response {
            case .success(let result):
                let exchangeRate = (result.data.exchangeRates ?? []).filter { $0.currency != nil }
                self.rateDictionary = Dictionary(grouping: exchangeRate, by: {$0.currency!})
            
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            queue.leave()
        }
        
        NetworkService.Info.getMainCurrencyList { (response) in
            switch response {
            
            case .success(let result):
                self.currencies = result.data.currencies ?? []
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            queue.leave()
        }
        
        queue.notify(queue: .main) {
            self.configure()
            completion()
        }
        
    }
}
