//
//  CurrencyPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CurrencyPresentationLogic: TablePresentationLogic {
    func viewDidLoad()
    func fetch()
}

class CurrencyPresenter: CurrencyPresentationLogic {
    
    private unowned let view: CurrencyDisplayLogic
    private var interactor: CurrencyBusinessLogic
    private let router: CurrencyRouter
    var controller: CurrencyViewController { view as! CurrencyViewController }
    
    init(view: CurrencyDisplayLogic, interactor: CurrencyBusinessLogic, router: CurrencyRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        
        bind()
        fetch()
    }
    
    func bind() {
        interactor.showError = { [weak self] detail in
            self?.controller.showError(message: detail)
        }
    }
    
    func fetch() {
        view.indicator.startAnimating()
        interactor.fetchRates { [weak self] in
            self?.view.indicator.stopAnimating()
            self?.view.tableView.reloadData()
        }
    }
    
}

extension CurrencyPresenter {
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.row]
    }
    
}
