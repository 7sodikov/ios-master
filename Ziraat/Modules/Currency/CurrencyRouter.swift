//
//  CurrencyRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CurrencyRoutingLogic {
    
}

class CurrencyRouter: BaseRouter, CurrencyRoutingLogic {

    init() {
        let controller = CurrencyViewController()
        super.init(viewController: controller)
        
        let intractor = CurrencyInteractor()
        let presenter = CurrencyPresenter(view: controller, interactor: intractor, router: self)
        controller.presenter = presenter
    }
        
}
