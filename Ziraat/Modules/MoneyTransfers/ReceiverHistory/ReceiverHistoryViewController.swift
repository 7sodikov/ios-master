//
//  ReceiverHistoryViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 12/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit
import SwipeCellKit

protocol ReceiverHistoryDisplayLogic: ReceiverHistoryViewInstaller {
    
}


class ReceiverHistoryViewController: BaseViewController, ReceiverHistoryDisplayLogic  {
    
    var backgroundImage: UIImageView!
    var searchBar: UISearchBar!
    var tableView: PaginatedTableView!
    var toolBar: UIToolbar!
    var toolbarItem: UIBarButtonItem!
    var mainView: UIView { self.view }
    var presenter: ReceiverHistoryPresentationLogic!
    private var searchRequestWorkItem: DispatchWorkItem?
    
    override func loadView() {
        super.loadView()
        title = RS.lbl_cards.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        setupTargets()
        tableView.loadData(refresh: true)
        
        setupTargets()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutSubView()
    }
    
    override func setupTargets() {
        super.setupTargets()
        tableView.paginatedDelegate = self
        tableView.paginatedDataSource = self
        
        tableView.register(TableViewCell.self)
        tableView.register(ReceiverCell.self)
        
        searchBar.delegate = self
        toolbarItem.target = self
        toolbarItem.action = #selector(cancelSearching)
    }
    
    @objc func cancelSearching() {
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
}

extension ReceiverHistoryViewController: PaginatedTableViewDelegate, PaginatedTableViewDataSource, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.delegate = self
        cell.set(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        presenter.tablePresenter(tableView, didSelectRowAt: indexPath)
    }
    
    func loadMore(_ pageNumber: Int, _ pageSize: Int, onSuccess: ((Bool) -> Void)?, onError: ((Error) -> Void)?) {
        presenter.nextPage { success in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                onSuccess?(success)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let deleteAction = SwipeAction(style: .destructive, title: nil) { (action, indexPath) in
            let alert = AlertController(title: "", message: RS.al_msg_in_process.localized(), preferredStyle: .alert)
            alert.showAutoDismissAlert(in: self, okClick: {})
        }
       
        configure(action: deleteAction, imageName: "img_trash", backgroundColor: .red)
        return [deleteAction]
    }
    
    fileprivate func configure(action: SwipeAction, imageName: String, backgroundColor: UIColor) {
        action.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        action.backgroundColor = backgroundColor
        if #available(iOS 13.0, *) {
            action.image?.withTintColor(.white)
        } else {
            // Fallback on earlier versions
        }
    }
}

extension ReceiverHistoryViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        presenter.willSet(searchIsActive: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        presenter.willSet(searchIsActive: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchRequestWorkItem?.cancel()
        let requestWorkItem =  DispatchWorkItem { [weak self] in
            self?.presenter.willSet(searchIsActive: !searchText.isEmpty)
            self?.presenter.willSearch(text: searchText)
        }
        searchRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250),
                                      execute: requestWorkItem)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
}
