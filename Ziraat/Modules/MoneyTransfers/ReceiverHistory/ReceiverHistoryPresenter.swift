//
//  ReceiverHistoryPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 12/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol ReceiverHistoryPresentationLogic: PresentationProtocol, TablePresentationLogic {
    func willSet(searchIsActive: Bool)
    func willSearch(text: String)
    func nextPage(completion: @escaping(Bool) -> Void)
}


class ReceiverHistoryPresenter: ReceiverHistoryPresentationLogic {
    
    private unowned let view: ReceiverHistoryDisplayLogic
    private let interactor: ReceiverHistoryBusinessLogic
    private let router: ReceiverHistoryRoutingLogic
    weak var delegate: ReceiverHistoryDelegate?
    
    private var controller: ReceiverHistoryViewController { view as! ReceiverHistoryViewController }
    
    
    
    init(view: ReceiverHistoryDisplayLogic, interactor: ReceiverHistoryBusinessLogic, router: ReceiverHistoryRoutingLogic, delegate: ReceiverHistoryDelegate? = nil) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.delegate = delegate
    }
    
    func viewDidLoad() {
        view.setupSubviews()
//         fetchData()
    }
    
    func fetchData(keyword: String?, completion: @escaping(Bool) -> Void) {
        view.tableView.refreshControl?.beginRefreshing()
        interactor.fetchReceiverList(keyword: keyword) { success in
            self.view.tableView.refreshControl?.endRefreshing()
            completion(success)
        }
        
    }
    
    
    func willSet(searchIsActive: Bool) {
        interactor.searchBar(isActive: searchIsActive && !view.searchBar.text!.isEmpty)
    }
    
    func willSearch(text: String) {
        guard !text.isEmpty, interactor.searchIsActive else {
            return
        }
        if interactor.searchIsActive {
            view.tableView.reloadData()
        }
        fetchData(keyword: text) { success in
            if success {
                self.view.tableView.reloadData()
            }
        }
    }
    
    func nextPage(completion: @escaping(Bool) -> Void) {
        interactor.updatePage()
        view.tableView.loadData(refresh: true)
        fetchData(keyword: nil, completion: completion)
    }
    
}


// MARK: -TablePresentationLogic

extension ReceiverHistoryPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
    func tablePresenter(_ table: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexPath.section == 0
            ? delegate?.didSelect(card: interactor.receiverCard(at: indexPath.row))
            : delegate?.didSelect(receiver: interactor.receiver(at: indexPath.row))
        router.pop()
    }
    
}
