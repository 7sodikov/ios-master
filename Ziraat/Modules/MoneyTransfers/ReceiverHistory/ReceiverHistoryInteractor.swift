//
//  ReceiverHistoryInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 12/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ReceiverHistoryBusinessLogic {
    var searchIsActive: Bool {get}
    var isA2P: Bool {get}
    var viewModels: [[ReceiverCellViewModel]]  { get }
    func updatePage()
    func receiver(at row: Int) -> BalanceReceiver
    func receiverCard(at row: Int) -> Card
    func searchBar(isActive: Bool)
    func fetchReceiverList(keyword: String?, completion: @escaping(Bool) -> Void)
}

class ReceiverHistoryInteractor: ReceiverHistoryBusinessLogic {
    
    let isA2P: Bool
    var totalItems, totalPages, currentPage: Int
    var receivers: [BalanceReceiver] = []
    private(set) var searchIsActive: Bool = false
    var searchedReceivers: [BalanceReceiver] = []
    let cards: [Card]
    
    init(cards: [Card], isA2P: Bool) {
        self.isA2P = isA2P
        self.cards = cards
        totalItems = 0
        totalPages = 0
        currentPage = -1
    }
    
    var viewModels: [[ReceiverCellViewModel]] {
        typealias Section = [ReceiverCellViewModel]
        
        let section1: Section = (searchIsActive ? .init() : cards).map {.init(id: $0.cardId, cardLogo: $0.additions.bank, holder: $0.owner, cardNumber: $0.pan)}
        
        let section2: Section = (searchIsActive ? searchedReceivers : receivers).map {
            .init(id: $0.id, cardLogo: $0.additions.bank, holder: $0.owner, cardNumber: $0.pan.makeReadableCardNumber)
        }
        
        return [section1, section2]
    }
    
    func searchBar(isActive: Bool) {
        self.searchIsActive = isActive
    }
    
    func updatePage() {
        self.currentPage += 1
    }
    
    func receiver(at row: Int) -> BalanceReceiver {
        (searchIsActive ? searchedReceivers : receivers)[row]
    }
    
    func receiverCard(at row: Int) -> Card {
        cards[row]
    }
    
    private func balanceReceiver(from card: Card) -> BalanceReceiver {
        let addition = BalanceAdditions(type: card.additions.type, bank: card.additions.type)
        return .init(id: card.cardId, pan: card.pan, owner: card.owner, cardType: card.type, additions: addition)
    }
    
    func fetchReceiverList(keyword: String? = nil, completion: @escaping(Bool) -> Void) {
        func configure(receiverList: ReceiverList) {
            if self.searchIsActive {
                searchedReceivers = receiverList.receivers
            } else {
                receivers.append(
                    contentsOf: receiverList.receivers.compactMap { receiver -> BalanceReceiver? in
                        receivers.contains(where: { $0.id == receiver.id }) ? nil : receiver
                    }
                )
            }
            
            totalItems = receiverList.totalItems
            currentPage = receiverList.currentPage
        }
        guard !isA2P else {
            completion(true)
            return
        }
        let page = searchIsActive ? 0 : currentPage
        let size = searchIsActive ? 100 : 20
        NetworkService.Operation.receiverList(keyword: keyword, type: nil, page: page, size: size) { response in
            switch response {
            case .success(let result):
                configure(receiverList: result.data)
                
                completion(true)
            case .failure(_):
//                self?.showError?(error.localizedDescription)
                completion(false)
            }
        }
    }
    
}
