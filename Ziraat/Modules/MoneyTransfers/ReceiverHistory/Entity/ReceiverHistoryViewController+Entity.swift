//
//  ReceiverHistoryViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 12/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ReceiverHistoryDelegate: AnyObject {
    func didSelect(item: TableViewModel, at indexPath: IndexPath)
    func didSelect(receiver: BalanceReceiver)
    func didSelect(card: Card)
}


extension ReceiverHistoryViewController {
    
    
}
