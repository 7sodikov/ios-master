//
//  ReceiverHistoryRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 12/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ReceiverHistoryRoutingLogic {
  func pop()
}

class ReceiverHistoryRouter: BaseRouter, ReceiverHistoryRoutingLogic {
    
    
    init(cards: [Card], delegate: ReceiverHistoryDelegate? = nil, isA2P: Bool = false) {
        let controller = ReceiverHistoryViewController()
        super.init(viewController: controller)
        
        let interactor = ReceiverHistoryInteractor(cards: cards, isA2P: isA2P)
        let presenter = ReceiverHistoryPresenter(view: controller, interactor: interactor, router: self, delegate: delegate)
        controller.presenter = presenter
    }
    
    func pop() {
        navigationController?.popViewController(animated: true)
    }
}
