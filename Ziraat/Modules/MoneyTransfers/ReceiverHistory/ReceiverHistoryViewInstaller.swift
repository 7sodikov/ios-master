//
//  ReceiverHistoryViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 12/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol ReceiverHistoryViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var searchBar: UISearchBar! {get set}
    var tableView: PaginatedTableView! {get set}
    var toolBar: UIToolbar! {get set}
    var toolbarItem: UIBarButtonItem! {get set}
    func layoutSubView()
}

extension ReceiverHistoryViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        tableView = .init()
        tableView.backgroundColor = .clear
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.tableFooterView = .init()
        tableView.enablePullToRefresh = true
        
        searchBar = UISearchBar()
        searchBar.layer.masksToBounds = true
        searchBar.layoutIfNeeded()
        searchBar.layer.borderColor = UIColor(145, 150, 155).cgColor
        searchBar.layer.borderWidth = 2
        searchBar.layer.cornerRadius = 6
        searchBar.isTranslucent = false
        searchBar.backgroundColor = .white
        
        toolBar = UIToolbar()
        toolbarItem = UIBarButtonItem()
        toolbarItem.title = RS.lbl_cancel.localized()
        toolbarItem.style = .plain
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.items = [space,toolbarItem]
        toolBar.sizeToFit()
        
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(searchBar)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        searchBar.constraint { make in
            make.top(8).horizontal(16).equalToSuperView()
        }
        tableView.constraint { (make) in
            make.top(self.searchBar.bottomAnchor, offset: 16)
                .horizontal.bottom.equalToSuperView()
        }
    }
    
    func layoutSubView() {
        if let searchTextField: UITextField = searchBar.value(forKey: "searchBarTextField") as? UITextField {
            searchTextField.textAlignment = .left
            let image:UIImage = UIImage(named: "btn_search")!
            let imageView:UIImageView = UIImageView.init(image: image)
            searchTextField.leftView = nil
            searchTextField.placeholder = RS.txt_f_search.localized()
            searchTextField.rightView = imageView
            searchTextField.rightViewMode = .always
            searchTextField.backgroundColor = .clear
            searchTextField.inputAccessoryView = toolBar
        }
    }
    
    var border: CALayer {
        let bottomBorder = CALayer()
        let width = UIScreen.main.bounds.width - Adaptive.val(16)
        let color = UIColor(red: 132.0 / 255.0, green: 142.0 / 255.0, blue: 159.0 / 255.0, alpha: 0.15)
        bottomBorder.frame = CGRect(x: Adaptive.val(16), y: 0, width: width, height: 0.5)
        bottomBorder.backgroundColor = color.cgColor
        return bottomBorder
    }
    
}
