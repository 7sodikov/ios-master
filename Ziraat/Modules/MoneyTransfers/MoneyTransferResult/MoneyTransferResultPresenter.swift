//
//  MoneyTransferResultPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 27/04/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol MoneyTransferResultPresentationLogic: PresentationProtocol, MoneyTransferResultBase, TablePresentationLogic {
    var currentPage: Page {get}
    
    func willSet(smsCode: String)
    func buttonClicked(isNext: Bool, page: Page)
}


class MoneyTransferResultPresenter: MoneyTransferResultPresentationLogic {
    
    private unowned let view: MoneyTransferResultDisplayLogic
    private let interactor: MoneyTransferResultBusinessLogic
    private let router: MoneyTransferResultRoutingLogic
    
    private var controller: MoneyTransferResultViewController { view as! MoneyTransferResultViewController }
    var currentPage: Page { interactor.currentPage }
    
    init(view: MoneyTransferResultDisplayLogic, interactor: MoneyTransferResultBusinessLogic, router: MoneyTransferResultRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        view.setupTargets()
    }
    
    func willSet(smsCode: String) {
        interactor.set(smsCode: smsCode)
    }
    
    func buttonClicked(isNext: Bool, page: Page) {
        switch page {
            
        case .info:
            isNext ? sendToken() : confirmate()
        case .confirmation:
            isNext ? confirmateWithSmsCode() : confirmate()
        case .result:
            isNext ? showPdfCheque() : addToFavauretes()
        }
    }
    
    private func confirmateWithSmsCode() {
        controller.spinner(.start)
        interactor.operationSmsConfirm { [weak self] success in
            self?.controller.spinner(.stop)
            if success {
                self?.reloadTableView(at: .result)
            }
        }
    }
    
    private func addToFavauretes() {
        func request(operationId: Int, title: String) {
            interactor.addFavourets(operationId: operationId, title: title) { (success) in
                if success {
                    self.router.backToRoot()
                }
            }
        }
        
        guard let operationId = interactor.confirmationResponse?.operationId else{
            return
        }
        controller.showInputDialog(title: RS.lbl_success.localized(),
                                   subtitle: RS.lbl_fav_added.localized(),
                                   actionTitle: RS.btn_yes.localized(),
                                   cancelTitle: RS.lbl_cancel.localized(),
                                   inputPlaceholder: RS.lbl_title.localized(),
                                   cancelHandler: nil) { (input) in
            
            request(operationId: operationId, title: input ?? " ")
        }
    }
    
    // TODO: need to send to server name and params for saving
    private func showPdfCheque() {
        guard let receipt = interactor.confirmationResponse?.receipt else{
            return
        }
        router.navigatetoPdfChequeVC(operationId: receipt)
    }
    
    func confirmate() {
        let controller = NotifyViewController(.confirmationConversion) {
            self.router.backToRoot()
        }
        self.controller.present(controller, animated: false)
    }
    
    private func sendToken() {
        controller.spinner(.start)
        interactor.sendToken { (success) in
            self.controller.spinner(.stop)
            if success {
                self.reloadTableView(at: .confirmation)
            }
        }
    }

    
    func reloadTableView(at page: Page) {
        if .info == interactor.currentPage && page == .info { router.back(); return }
        interactor.set(page: page)
        view.tableView.reloadData()
    }
}


// MARK: - TablePresentationLogic

extension MoneyTransferResultPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}

// MARK: - Sending mail
extension MoneyTransferResultPresenter {
    
}
