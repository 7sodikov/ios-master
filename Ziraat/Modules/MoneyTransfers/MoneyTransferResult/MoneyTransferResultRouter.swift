//
//  MoneyTransferResultRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 27/04/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MoneyTransferResultRoutingLogic {
    func back()
    func navigatetoPdfChequeVC(operationId: String)
    func backToRoot()
}

class MoneyTransferResultRouter: BaseRouter, MoneyTransferResultRoutingLogic {
    
    
    init(details: [DetailsCellViewModel], token: String) {
        let controller = MoneyTransferResultViewController()
        super.init(viewController: controller)
        
        let interactor = MoneyTransferResultInteractor(details: details, token: token)
        let presenter = MoneyTransferResultPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigatetoPdfChequeVC(operationId: String) {
//        let pdfView = PdfViewController(operation: operationId, title: viewController.title ?? "")
//        viewController.navigationController?.pushViewController(pdfView, animated: true)
        let statementVC = PdfChequeRouter.createModule(operation: operationId, title: nil, isHidden: true)
        statementVC.modalPresentationStyle = .overFullScreen
        viewController.present(statementVC, animated: false, completion: nil)
    }
    
    func backToRoot() {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
}
