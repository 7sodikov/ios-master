//
//  MTResultRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 8/07/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MTResultRoutingLogic {
    func backToRoot()
    func navigatetoPdfChequeVC(operationId: String)
}


class MTResultRouter: BaseRouter, MTResultRoutingLogic {
    
    
    init(resultMode: MTResultMode, operationId: Int, receipt: String) {
        let controller = MTResultViewController()
        super.init(viewController: controller)
        
        let interactor = MTResultInteractor(resultMode: resultMode, operationId: operationId, receipt: receipt)
        let presenter = MTResultPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func backToRoot() {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    
    func navigatetoPdfChequeVC(operationId: String) {
        let pdfView = PdfViewController(operation: operationId, title: viewController.title ?? "")
        viewController.navigationController?.pushViewController(pdfView, animated: true)
    }
    
}
