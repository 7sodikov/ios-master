//
//  MTResultInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 8/07/21
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MTResultBusinessLogic: MTResultBase {
    var operationId: Int {get}
    var receipt: String {get}
    var viewModels: [[TableViewModel]] {get}
    
    func addFavourets(operationId: Int, title: String, completion: @escaping (Bool)->()) 
}

class MTResultInteractor: MTResultBusinessLogic {
    
    var status: ResultStatusCellViewModel.Status = .failure(message: RS.lbl_UnknownError.localized())
    let resultMode: MTResultMode
    let operationId: Int
    let receipt: String
    
    init(resultMode: MTResultMode, operationId: Int, receipt: String) {
        self.resultMode = resultMode
        self.operationId = operationId
        self.receipt = receipt
        setup()
    }
    
//    var viewModels: [[TableViewModel]] {
//        let vm1 = ResultStatusCellViewModel(status)
//        let vm2 = SingleLabelCellViewModel(text: RS.lbl_successful_auto_payment.localized(), textAlignment: .center)
//
//        return [[vm1]] + [buttonsViewModel]
//    }
//
    
    private var buttonsViewModel: [ButtonCellViewModel] {
        let title1 = RS.btn_return_to_list.localized()
        let title2 = RS.btn_return_to_homepage.localized()
    
        return [
            .init(style: .red(title: title1), isEnabled: true, tag: 1),
            .init(style: .red(title: title2), isEnabled: true, tag: 2)
        ]
    }
    
//    MARK: - NONET TRANSFER VM
    var viewModels: [[TableViewModel]] {
        
        let vm1 = ResultStatusCellViewModel(status)
        let vm2 = ButtonsCellViewModel(leftButtonStyle: .red(title: Button.addToFavourite.title),
                                       rightButtonStyle: .red(title: Button.print.title),
                                       tag: 0)
        
        var btn: ResultOperationStackViewController.Button = .close
        var vm3 = ButtonCellViewModel(style: .red(title: btn.title), isEnabled: true, tag: btn.rawValue)
        
        if case .failure = self.status {
            btn = .backToMain
            vm3 = .init(style: .red(title: btn.title), isEnabled: true, tag: btn.rawValue)
            return [[vm1], [vm3]]
        }
        
        return [[vm1], [vm2, vm3]]
    }
    
    
    func setup() {
        switch resultMode {
            
        case .moneyTranfer(let status),
             .conversion(let status):
            self.status = status
        
        
        }
    }
    
    
    func addFavourets(operationId: Int, title: String, completion: @escaping (Bool)->()) {
        
        NetworkService.Favorite.favoriteAdd(operationId: operationId, title: title) { (response) in
            switch response {
            case .success(_):
                completion(true)
            case .failure(let error):
                print(error.localizedDescription)
                completion(false)
            }
        }
        
    }
    
}
