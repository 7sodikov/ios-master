//
//  MTResultViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 8/07/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MTResultBase {
    typealias Button = MTResultViewController.Button
}


extension MTResultViewController {
    
    enum Button: Int, CaseIterable {
        case confirm
        case cancel
        case addToFavourite
        case print
        case close
        case backToMain
        
        var title: String {
            switch self {
            
            case .confirm:
                return RS.btn_confirm.localized().uppercased()
            case .cancel:
                return RS.lbl_cancel.localized().uppercased()
            case .addToFavourite:
                return RS.btn_add_to_favourites.localized()
            case .print:
                return RS.btn_print.localized()
            case .close:
                return RS.btn_close.localized()
            case .backToMain:
                return RS.btn_return_to_homepage.localized()
            }
        }
        
    }
}


enum MTResultMode {
    case moneyTranfer(_ status:ResultStatusCellViewModel.Status)
    case conversion(_ status:ResultStatusCellViewModel.Status)
}
