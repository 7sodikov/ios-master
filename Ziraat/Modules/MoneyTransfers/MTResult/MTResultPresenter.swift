//
//  MTResultPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 8/07/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol MTResultPresentationLogic: PresentationProtocol, MTResultBase, TablePresentationLogic {
 
    func buttonClicked(button: MTResultViewController.Button)
}


class MTResultPresenter: MTResultPresentationLogic {
    
    
    private unowned let view: MTResultDisplayLogic
    private let interactor: MTResultBusinessLogic
    private let router: MTResultRoutingLogic
    
    private var controller: MTResultViewController { view as! MTResultViewController }
    
    
    init(view: MTResultDisplayLogic, interactor: MTResultBusinessLogic, router: MTResultRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        
        view.setupSubviews()
    }
    
    func buttonClicked(button: MTResultViewController.Button) {
        switch button {
    
        case .addToFavourite:
            addToFavauretes()
        case .print:
            router.navigatetoPdfChequeVC(operationId: interactor.receipt)
        case .close,
             .backToMain:
            router.backToRoot()
        
        default:
            break
        }
    }
    
    private func addToFavauretes() {
        func request(operationId: Int, title: String) {
            interactor.addFavourets(operationId: operationId, title: title) { (success) in
                if success {
                    self.router.backToRoot()
                }
            }
        }
        
        controller.showInputDialog(title: RS.lbl_success.localized(),
                                   subtitle: RS.lbl_fav_added.localized(),
                                   actionTitle: RS.btn_yes.localized(),
                                   cancelTitle: RS.lbl_cancel.localized(),
                                   inputPlaceholder: RS.lbl_title.localized(),
                                   cancelHandler: nil) { (input) in

            request(operationId: self.interactor.operationId, title: input ?? " ")
        }
    }
    
}


// MARK: -TablePresentationLogic

extension MTResultPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}
