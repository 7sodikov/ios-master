//
//  MTResultViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 8/07/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol MTResultDisplayLogic: MTResultViewInstaller {
    
}


class MTResultViewController: BaseViewController, MTResultDisplayLogic  {
    
    
    var backgroundImage: UIImageView!
    var tableView: UITableView!
    
    var mainView: UIView { self.view }
    var presenter: MTResultPresentationLogic!
    
    
    override func loadView() {
        super.loadView()
        
        navigationController?.transparentBackgorund()
        title = RS.lbl_money_transfer.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        setupTargets()
    }
    
    override func setupTargets() {
        super.setupTargets()
        tableView.dataSource = self
        tableView.delegate = self
        
        
        tableView.register(TableViewCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(ButtonsCell.self)
        tableView.register(ResultStatusCell.self)
        tableView.register(SingleLabelCell.self)
    }
    
    func update(title: String) {
        self.title = title
    }
    
    
    
}

extension MTResultViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        configure(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        (section == 0 || section == 1) ? 64 : 16
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath) is DetailsCellViewModel {
            return presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath).estimatedRowHeight
        }
        return UITableView.automaticDimension
    }
    
    func configure(cell: UITableViewCell) {
        (cell as? ButtonCell)?.buttonDelegate = self
        (cell as? ButtonsCell)?.buttonDelegate = self
    }
}

extension MTResultViewController: ButtonCellDelegate {
    func buttonClicked(tag: Int) {
        guard let buttonEnum = Button(rawValue: tag) else {
            return
        }
        presenter.buttonClicked(button: buttonEnum)
    }
}


extension MTResultViewController: ButtonsCellDelegate  {
    
    func buttonCellsButtonClicked(at tag: Int, isLeft: Bool) {
        presenter.buttonClicked(button: isLeft ? .addToFavourite : .print)
    }
    
}
