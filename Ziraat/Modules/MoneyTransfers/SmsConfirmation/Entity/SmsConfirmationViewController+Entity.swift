//
//  SmsConfirmationViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 29/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SmsConfirmationBase {
    typealias Button = SmsConfirmationViewController.Button
}


extension SmsConfirmationViewController {
    
    enum Section: Int, CaseIterable {
        case smsCode
        case timer
        case button
    }
    
    enum Button: Int, CaseIterable {
        case confirm
        case cancel
//        case addToFavourite
//        case print
//        case close
//        case backToMain
        
        var title: String {
            switch self {
            
            case .confirm:
                return RS.btn_confirm.localized().uppercased()
            case .cancel:
                return RS.lbl_cancel.localized().uppercased()
//            case .addToFavourite:
//                return RS.btn_add_to_favourites.localized()
//            case .print:
//                return RS.btn_print.localized()
//            case .close:
//                return RS.btn_close.localized()
//            case .backToMain:
//                return RS.btn_return_to_homepage.localized()
            }
        }
        
        var style: ButtonStyle {
            self == .confirm ? .red(title: self.title) : .bordered(title: self.title)
        }
        
    }
   
}

enum SmsConfirmationFor {
    case moneyTransfer(token: String)
    
    var navigationTitle: String {
        switch self {
        case .moneyTransfer:
            return RS.lbl_money_transfer.localized()
        }
    }
}
