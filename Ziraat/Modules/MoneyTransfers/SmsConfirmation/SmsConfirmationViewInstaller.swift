//
//  SmsConfirmationViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 29/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol SmsConfirmationViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var tableView: UITableView! {get set}
    
}

extension SmsConfirmationViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        tableView = .init()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.tableFooterView = .init()
        tableView.allowsSelection = false
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
      
        tableView.constraint { (make) in
            make.top.horizontal.bottom.equalToSuperView()
        }
    
    }
    
}
