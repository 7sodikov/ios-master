//
//  SmsConfirmationInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 29/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SmsConfirmationBusinessLogic: SmsConfirmationBase {
    var smsConfirmationMode: SmsConfirmationFor {get}
    var viewModels: [[TableViewModel]] {get}
    var status: ResultStatusCellViewModel.Status {get}
    var confirmationResponse: P2PConfirmResponse? {get}
    
    func set(smsCode: String)
    func operationSmsConfirm(completion: @escaping (Bool)->())
    func operationSMSResend(completion: @escaping (Bool)->())
}

class SmsConfirmationInteractor: SmsConfirmationBusinessLogic {
    
    let smsConfirmationMode: SmsConfirmationFor
    var smsCode: String = ""
    var message: String = ""
    var token: String = ""
    
    var confirmationResponse: P2PConfirmResponse?
    var status: ResultStatusCellViewModel.Status = .failure(message: RS.lbl_UnknownError.localized())
    
    init(mode smsConfirmation: SmsConfirmationFor, message: String) {
        self.smsConfirmationMode = smsConfirmation
        self.message = message
        
        
        switch smsConfirmation {
        case .moneyTransfer(let token):
            self.token = token
        }
    }

    
    func set(smsCode: String) {
        self.smsCode = smsCode
    }
    
    
    
//    MARK: - viewModels
    var viewModels: [[TableViewModel]] {
        [[ConfirmationCellViewModel(message: message)], buttonsViewModel]
    }
    
    private var buttonsViewModel: [ButtonCellViewModel] {
        Button.allCases.map {
            .init(style: $0.style, isEnabled: true, tag: $0.rawValue)
        }
    }
    
    
    
//    MARK: - networking
    func operationSmsConfirm(completion: @escaping (Bool)->()) {
        NetworkService.Operation.operationConfirm(token: token, code: smsCode) {[weak self] (response) in
            guard let self = self else { return }
            switch response {
            
            case .success(let result):
                self.confirmationResponse = result.data
                self.status = .success(message: RS.lbl_payment_success.localized())
                completion(true)
            case .failure(let error):
                self.status = .failure(message: error.localizedDescription)
                completion(false)
            }
        }
    }
    
    func operationSMSResend(completion: @escaping (Bool)->()) {
        NetworkService.Operation.operationSMSResend(token: token) { [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case  .success(let result):
                self.message = result.data.message ?? ""
                completion(true)
            case .failure(let error):
                print(error.localizedDescription)
                completion(false)
            }
        }
    }
    
}
