//
//  SmsConfirmationPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 29/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol SmsConfirmationPresentationLogic: PresentationProtocol, SmsConfirmationBase, TablePresentationLogic {
    func willSet(smsCode: String)
    func didClicked(on button: Button)
}


class SmsConfirmationPresenter: SmsConfirmationPresentationLogic {
    
    private unowned let view: SmsConfirmationDisplayLogic
    private let interactor: SmsConfirmationBusinessLogic
    private let router: SmsConfirmationRoutingLogic
    
    private var controller: SmsConfirmationViewController { view as! SmsConfirmationViewController }
    
    
    init(view: SmsConfirmationDisplayLogic, interactor: SmsConfirmationBusinessLogic, router: SmsConfirmationRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        controller.title = interactor.smsConfirmationMode.navigationTitle
        
        view.setupSubviews()
        view.setupTargets()
    }
    
    func willSet(smsCode: String) {
        interactor.set(smsCode: smsCode)
    }
    
    func didClicked(on button: SmsConfirmationViewController.Button) {
        switch button {
        case .confirm:
            confirmateOperation()
        case .cancel:
            router.backToRoot()
        }
    }
    
    private func confirmateOperation() {
        switch interactor.smsConfirmationMode {
        case .moneyTransfer:
            confirmateMoneyTransfer()
        }
    }
    
    private func confirmateMoneyTransfer() {
        controller.spinner(.start)
        interactor.operationSmsConfirm { [weak self, weak controller] success in
            guard let self = self, let controller = controller else { return }
            controller.spinner(.stop)
            self.displayResult()
        }
    }
    
    private func displayResult() {
        guard let operationId = interactor.confirmationResponse?.operationId,
              let receipt: String = interactor.confirmationResponse?.receipt else {
            return
        }
        router.diplayConfirmation(status: interactor.status, operationId: operationId, receipt: receipt)
    }
}


// MARK: -TablePresentationLogic

extension SmsConfirmationPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}
