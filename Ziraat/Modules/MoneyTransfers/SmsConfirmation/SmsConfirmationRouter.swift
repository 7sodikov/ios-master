//
//  SmsConfirmationRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 29/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SmsConfirmationRoutingLogic {
 
    func diplayConfirmation(status: ResultStatusCellViewModel.Status, operationId: Int, receipt: String)
    func backToRoot()
}


class SmsConfirmationRouter: BaseRouter, SmsConfirmationRoutingLogic {
    
    
    init(mode smsConfirmation: SmsConfirmationFor, message: String) {
        let controller = SmsConfirmationViewController()
        super.init(viewController: controller)
        
        let interactor = SmsConfirmationInteractor(mode: smsConfirmation, message: message)
        let presenter = SmsConfirmationPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func diplayConfirmation(status: ResultStatusCellViewModel.Status, operationId: Int, receipt: String) {
        let router = MTResultRouter(resultMode: .moneyTranfer(status), operationId: operationId, receipt: receipt)
        push(router, animated: true)
    }
    
    func backToRoot() {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    
}
