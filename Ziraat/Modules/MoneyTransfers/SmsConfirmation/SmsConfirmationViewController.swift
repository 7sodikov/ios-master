//
//  SmsConfirmationViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 29/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol SmsConfirmationDisplayLogic: SmsConfirmationViewInstaller {
    func setupTargets()
}


class SmsConfirmationViewController: BaseViewController, SmsConfirmationDisplayLogic  {
    
    var backgroundImage: UIImageView!
    var tableView: UITableView!
    
    
    
    var mainView: UIView { self.view }
    var presenter: SmsConfirmationPresentationLogic!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func setupTargets() {
        super.setupTargets()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(DetailCenteredCell.self)
        tableView.register(TableViewCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(ButtonsCell.self)
        tableView.register(ResultStatusCell.self)
        tableView.register(ConfirmationCell.self)
        tableView.register(DetailsCell.self)
        tableView.register(SingleLabelCell.self)
    }
    
}

extension SmsConfirmationViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.tag = indexPath.row
        cell.set(viewModel: viewModel)
        (cell as? ButtonCell)?.buttonDelegate = self
        (cell as? ConfirmationCell)?.confirmationDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        64
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath) is DetailsCellViewModel {
            return presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath).estimatedRowHeight
        }
        return UITableView.automaticDimension
    }
}

extension SmsConfirmationViewController: ButtonCellDelegate {
    
    func buttonClicked(tag: Int) {
        guard let buttonEnum = Button(rawValue: tag) else {
            return
        }
        presenter.didClicked(on: buttonEnum)
    }
    
}

extension SmsConfirmationViewController: ConfirmationCellDelegate {
    
    func textFiled(text: String) {
        presenter.willSet(smsCode: text)
    }
    
}
