//
//  MoneyTransferConfirmPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 28/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol MoneyTransferConfirmPresentationLogic: PresentationProtocol, MoneyTransferConfirmBase, TablePresentationLogic {
    var viewModels: [DetailsCellViewModel] {get}
    var confirFor: ConfirmFor {get}
    
    func canEditView(at index: Int)
    func didConfirmOperation()
    func didCancelOperation()
}


class MoneyTransferConfirmPresenter: MoneyTransferConfirmPresentationLogic {
    
    private unowned let view: MoneyTransferConfirmDisplayLogic
    private let interactor: MoneyTransferConfirmBusinessLogic
    private let router: MoneyTransferConfirmRoutingLogic
    
    private var controller: MoneyTransferConfirmViewController { view as! MoneyTransferConfirmViewController }
    var viewModels: [DetailsCellViewModel] { interactor.viewModels}
    var confirFor: ConfirmFor  { interactor.confirFor }
    
    init(view: MoneyTransferConfirmDisplayLogic, interactor: MoneyTransferConfirmBusinessLogic, router: MoneyTransferConfirmRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        view.setupTargets()
    }
    
    func canEditView(at index: Int) {
        router.back()
    }
    
    func didConfirmOperation() {
        switch interactor.confirFor {
        case .moneyTransfer(_, let token),
             .conversion(_,let token):
            sendTokenForMoneyTranfering(token: token)
        }
    }
    
    func didCancelOperation() {
        router.backToRoot()
    }

    fileprivate func prepareResultScreen(token: String) {
        switch self.interactor.confirFor {
        case .moneyTransfer:
            self.openSmsConfirmation(token: token)
        case .conversion:
            guard let operationId = interactor.singleMessageResponse?.operationId,
                  let receipt = interactor.singleMessageResponse?.receipt else {
                return
            }
            self.router.diplayConfirmation(status: interactor.status, operationId: operationId, receipt: receipt)
        }
    }
    
    private func sendTokenForMoneyTranfering(token: String) {
        controller.spinner(.start)
        interactor.sendToken(token: token) { [self] (success) in
            self.controller.spinner(.stop)
            if success {
                prepareResultScreen(token: token)
            }
        }
    }
    
    private func openSmsConfirmation(token: String) {
        guard let message = interactor.singleMessageResponse?.message else {
            return
        }
        router.smsConfirmate(token: token, message: message)
    }
    
}


// MARK: -TablePresentationLogic

extension MoneyTransferConfirmPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        1
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        .init()
    }
    
}
