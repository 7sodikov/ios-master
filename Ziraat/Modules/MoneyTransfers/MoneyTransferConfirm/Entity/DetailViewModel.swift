//
//  DetailViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/28/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct DetailViewModel {
    
    let header, title, value: String
    let canEdit: Bool
    
     init(header: String, title: String, value: String, canEdit: Bool = true) {
        self.header = header
        self.title = title
        self.value = value
        self.canEdit = canEdit
    }
    
    
    var isTitleHidden: Bool { value.isEmpty }
    var isValueHidden: Bool { value.isEmpty }
}
