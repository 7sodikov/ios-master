//
//  MoneyTransferConfirmViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 28/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MoneyTransferConfirmBase {
    
    
}


extension MoneyTransferConfirmViewController {
    

  
    
}

enum ConfirmFor {
    case moneyTransfer(details: [DetailsCellViewModel], token: String)
    case conversion(details: [DetailsCellViewModel], token: String)
    
    var navigationTitle: String {
        
        switch self {
        case .moneyTransfer:
            return RS.lbl_money_transfer.localized()
        case .conversion:
            return RS.lbl_conversion.localized()
        }
    }
}
