//
//  DetailView.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/28/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


class DetailView: UIView {
    var mainContainer: UIView!
    var headerLabel: UILabel!
    var container: UIView!
    var valueStack: UIStackView!
    var titleLabel: UILabel!
    var valueLabel: UILabel!
    var iconButton: UIButton!
    
    init() {
        super.init(frame: .zero)
     
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateValue(update viewModel: DetailsCellViewModel) {
        headerLabel.text = viewModel.headerTitle
        titleLabel.text = viewModel.valueTitle
        valueLabel.text = viewModel.value
        
        titleLabel.isHidden = viewModel.isTitleHidden
        valueLabel.isHidden = viewModel.isValueHidden
        iconButton.isHidden = !viewModel.canEdit
    }
    
    func setupSubviews() {
        initSubviews()
        embedSubviews()
        addSubviewsConstraints()
    }
    
    
    func initSubviews() {
        backgroundColor = .white
        
        headerLabel = UILabel()
        headerLabel.textColor = UIColor(225,5,20)
        headerLabel.font = .systemFont(ofSize: 15, weight: .medium)
        
        container = UIView()
        container.backgroundColor = UIColor(231,233,234)
        container.layer.cornerRadius = 6
        
        titleLabel = UILabel()
        titleLabel.textColor = UIColor(84,95,101)
        titleLabel.font = .systemFont(ofSize: 15, weight: .regular)
        
        valueLabel = UILabel()
        valueLabel.textColor = UIColor(84,95,101)
        valueLabel.font = .systemFont(ofSize: 15, weight: .regular)
        
        valueStack = UIStackView()
        valueStack.axis = .vertical
        valueStack.spacing = 12
        
        iconButton = UIButton()
        iconButton.imageView?.contentMode = .scaleAspectFill
        iconButton.setImage(UIImage(named: "btn_icon_pen"), for: .normal)
        iconButton.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    func embedSubviews() {
        
        addSubview(headerLabel)
        addSubview(container)
        
        container.addSubview(valueStack)
        container.addSubview(iconButton)
        
        valueStack.addArrangedSubview(titleLabel)
        valueStack.addArrangedSubview(valueLabel)
        
    }
    
    func addSubviewsConstraints() {
       
        headerLabel.constraint { (make) in
            make.top(8).leading(24).trailing(-16).equalToSuperView()
        }
        container.constraint { (make) in
            make.top(self.headerLabel.bottomAnchor, offset: 8)
                .horizontal(16).bottom(-8).equalToSuperView()
        }
        iconButton.constraint { (make) in
            make.square(20).top(10).trailing(-10).equalToSuperView()
        }
        valueStack.constraint { make in
            make.height.greaterThanOrEqualToConstant(30)
                .trailing(self.iconButton.leadingAnchor, offset: -8)
                .leading(8).top(10).bottom.equalToSuperView()
        }
    }
}
