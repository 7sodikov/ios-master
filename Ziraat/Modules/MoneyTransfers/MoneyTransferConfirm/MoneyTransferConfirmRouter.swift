//
//  MoneyTransferConfirmRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 28/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MoneyTransferConfirmRoutingLogic {
    func backToRoot()
    func back()
    func smsConfirmate(token: String, message: String)
    func diplayConfirmation(status: ResultStatusCellViewModel.Status, operationId: Int, receipt: String)
}


class MoneyTransferConfirmRouter: BaseRouter, MoneyTransferConfirmRoutingLogic {
    
    
    init(mode confirFor: ConfirmFor) {
        let controller = MoneyTransferConfirmViewController()
        super.init(viewController: controller)
        
        let interactor = MoneyTransferConfirmInteractor(confirFor: confirFor)
        let presenter = MoneyTransferConfirmPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func backToRoot() {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func smsConfirmate(token: String, message: String) {
        let router = SmsConfirmationRouter(mode: .moneyTransfer(token: token), message: message)
        push(router, animated: true)
    }
    
    func diplayConfirmation(status: ResultStatusCellViewModel.Status, operationId: Int, receipt: String) {
        let router = MTResultRouter(resultMode: .moneyTranfer(status), operationId: operationId, receipt: receipt)
        push(router, animated: true)
    }
    
}
