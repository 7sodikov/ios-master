//
//  MoneyTransferConfirmInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 28/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MoneyTransferConfirmBusinessLogic: MoneyTransferConfirmBase {
    var confirFor: ConfirmFor {get}
    var viewModels: [DetailsCellViewModel] {get}
    var status: ResultStatusCellViewModel.Status {get}
    var singleMessageResponse: SingleMessageResponse? {get}
    
    func sendToken(token: String, completion: @escaping (Bool)->())
}

class MoneyTransferConfirmInteractor: MoneyTransferConfirmBusinessLogic {
    
    let confirFor: ConfirmFor
    
    
    var needToConfirm: Bool = true
    var status: ResultStatusCellViewModel.Status = .failure(message: RS.lbl_UnknownError.localized())
    var singleMessageResponse: SingleMessageResponse?
    
    init(confirFor: ConfirmFor) {
        self.confirFor = confirFor
    }
    
    var viewModels: [DetailsCellViewModel] {
        switch confirFor {
        case .moneyTransfer(let details, _),
             .conversion(let details,_):
            return details
        }
    }
    
   
    
    func sendToken(token: String, completion: @escaping (Bool)->()) {
        func configure(statusCode: Int) {
            needToConfirm = statusCode == 200
            if !needToConfirm {
                status = .success(message: RS.lbl_payment_success.localized())
            }
        }
        NetworkService.Operation.operationTransfer(token: token) { (response) in
            switch response {
            
            case .success(let result):
                configure(statusCode: result.httpStatusCode)
                self.singleMessageResponse = result.data
                completion(true)
            case .failure(let error):
                self.status = .failure(message: error.localizedDescription)
                completion(false)
            }
        }
    }
    
}
