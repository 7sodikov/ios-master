//
//  MoneyTransferConfirmViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 28/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol MoneyTransferConfirmDisplayLogic: MoneyTransferConfirmViewInstaller {
    
    func setupTargets()
}


class MoneyTransferConfirmViewController: BaseViewController, MoneyTransferConfirmDisplayLogic  {
    
    
    var backgroundImage: UIImageView!
    var scrollView: UIScrollView!
    var scrollContainer: UIStackView!
    var textLabel: UILabel!
    var detailsContainer: UIStackView!
    var buttonContainer: UIStackView!
    var confirmButton: Button!
    var cancelButton: Button!
    var mainView: UIView { self.view }
    var detailViews: [DetailView] = []
    var presenter: MoneyTransferConfirmPresentationLogic!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    
    override func setupTargets() {
        super.setupTargets()
        
        title = presenter.confirFor.navigationTitle
        
        updateDetailsView()
        
        confirmButton.addTarget(self, action: #selector(confirmButtonClicked), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked), for: .touchUpInside)
        
        detailViews.forEach { detailView in
            detailView.iconButton.addTarget(self, action: #selector(canEditView(_:)), for: .touchUpInside)
        }
    }
    
    @objc func canEditView(_ sender: UIButton) {
        presenter.canEditView(at: sender.tag)
    }
    
    func updateDetailsView() {
        presenter.viewModels.enumerated().forEach { (index, viewModel) in
            let detailView = DetailView()
            detailView.updateValue(update: viewModel)
            detailView.iconButton.tag = index
            detailViews.append(detailView)
            detailsContainer.addArrangedSubview(detailView)
        }
    }
    
    @objc func confirmButtonClicked() {
        presenter.didConfirmOperation()
    }
    
    @objc func cancelButtonClicked() {
        presenter.didCancelOperation()
    }
}



