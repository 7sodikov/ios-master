//
//  MoneyTransferConfirmViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 28/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MoneyTransferConfirmViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var scrollView: UIScrollView! {get set}
    var scrollContainer: UIStackView! {get set}
    var textLabel: UILabel! {get set}
    var detailsContainer: UIStackView! {get set}
    var buttonContainer: UIStackView! {get set}
    var confirmButton: Button! {get set}
    var cancelButton: Button! {get set}
}

extension MoneyTransferConfirmViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        scrollContainer = UIStackView()
        scrollContainer.axis = .vertical
        scrollContainer.spacing = 32
        scrollContainer.translatesAutoresizingMaskIntoConstraints = false
        
        textLabel = UILabel()
        textLabel.font = UIFont.gothamNarrow(size: 15, .book)
        textLabel.numberOfLines = 0
        textLabel.textColor = UIColor(89, 96, 103)
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.text = RS.lbl_transaction_preview.localized()
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        detailsContainer = UIStackView()
        detailsContainer.axis = .vertical
//        detailsContainer.spacing = 16
        detailsContainer.layer.cornerRadius = 6
        detailsContainer.layer.masksToBounds = true
        detailsContainer.backgroundColor = .white
        detailsContainer.addShadow()
        detailsContainer.translatesAutoresizingMaskIntoConstraints = false
        
        buttonContainer = UIStackView()
        buttonContainer.axis = .vertical
        buttonContainer.spacing = 12
        buttonContainer.translatesAutoresizingMaskIntoConstraints = false
        
        confirmButton = Button(method: .network, style: .confirm)
        confirmButton.setup()
        cancelButton = Button(method: .local, style: .cancel)
        cancelButton.setup()
        
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(scrollView)
        
        scrollView.addSubview(textLabel)
        scrollView.addSubview(scrollContainer)
        
        scrollContainer.addArrangedSubview(detailsContainer)
        scrollContainer.addArrangedSubview(buttonContainer)
        
        buttonContainer.addArrangedSubview(confirmButton)
        buttonContainer.addArrangedSubview(cancelButton)
    }
    
    func addSubviewsConstraints() {
        
        NSLayoutConstraint.activate([
            
            backgroundImage.topAnchor.constraint(equalTo: mainView.topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
            
            scrollView.topAnchor.constraint(equalTo: mainView.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
            
            textLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 12),
            textLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16),
            textLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -16),
            
            scrollContainer.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: 12),
            scrollContainer.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 16),
            scrollContainer.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor,constant: -16),
            scrollContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            scrollContainer.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 32),
            
            
        ])
    }
    
}
