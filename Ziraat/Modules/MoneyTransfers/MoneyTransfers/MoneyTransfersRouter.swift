//
//  MoneyTransfersRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/29/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MoneyTransfersRoutingLogic {
    func presentBottomSheet(bottomSheet: BottomSheet)
    func openReceiverHistory(delegate: ReceiverHistoryDelegate, cards: [Card], isA2P: Bool)
    func openPrepareScreen(details: [DetailsCellViewModel], token: String)
}


class MoneyTransfersRouter: BaseRouter, MoneyTransfersRoutingLogic {
    
    
    init(for favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?) {
        let controller = MoneyTransfersViewController()
        super.init(viewController: controller)
        
        let interactor = MoneyTransfersInteractor(for: favoriteItem, historyItem: historyItem)
        let presenter = MoneyTransfersPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func presentBottomSheet(bottomSheet: BottomSheet) {
        let bottomSheetVC = BottomSheetViewController(bottomSheet: bottomSheet)
        bottomSheetVC.modalPresentationStyle = .custom
        viewController.presentPanModal(bottomSheetVC)
    }
    
    func openPrepareScreen(details: [DetailsCellViewModel], token: String) {
//        let router = MoneyTransferResultRouter(details: details, token: token)
//        push(router, animated: true)
        let router = MoneyTransferConfirmRouter(mode: .moneyTransfer(details: details, token: token))
        push(router, animated: true)
    }
    
    func openReceiverHistory(delegate: ReceiverHistoryDelegate, cards: [Card], isA2P: Bool) {
        let router = ReceiverHistoryRouter(cards: cards, delegate: delegate, isA2P: isA2P)
        push(router, animated: true)
//        viewController.present(router.viewController, animated: true)
    }

}
