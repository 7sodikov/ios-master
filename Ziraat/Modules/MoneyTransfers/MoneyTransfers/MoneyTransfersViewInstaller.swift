//
//  MoneyTransfersViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/29/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MoneyTransfersViewInstaller: ViewInstaller, MoneyTransfersBase {
    var backgroundImageView: UIImageView! {get set}
    var segmentedControl: CustomSegmentedControl! {get set}
    var tableView: UITableView! {get set}
    var indicator: UIActivityIndicatorView! {get set}
    
    func indicator(_ action: ConversionNewViewController.IndicatorAnimator)
}

extension MoneyTransfersViewInstaller {
    
    func initSubviews() {
        backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "img_dash_light_background")
        
        let index = (parameter as? Segment)?.rawValue ?? 0
        segmentedControl = .init(selectedIndex: index)
        segmentedControl.backgroundColor = .clear
        segmentedControl.items = Segment.allCases.map(\.title)
        
        tableView = UITableView()
        tableView.tableFooterView = .init()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.allowsSelection = false
        
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        } else {
            indicator = UIActivityIndicatorView()
        }
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImageView)
        mainView.addSubview(segmentedControl)
        mainView.addSubview(tableView)
        mainView.addSubview(indicator)
    }
    
    func addSubviewsConstraints() {
        backgroundImageView.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        
        segmentedControl.constraint { (make) in
            make.height(40).top(16).horizontal.equalToSuperView()
        }
            
        tableView.constraint { (make) in
            make.top(self.segmentedControl.bottomAnchor).horizontal.bottom.equalToSuperView()
        }
        
        indicator.constraint { $0.center.equalToSuperView() }
    }
    
    func indicator(_ action: ConversionNewViewController.IndicatorAnimator) {
        if case .start = action {
            tableView.isHidden = true
            indicator.startAnimating()
        } else {
            tableView.isHidden = false
            indicator.stopAnimating()
        }
    }
    
}
