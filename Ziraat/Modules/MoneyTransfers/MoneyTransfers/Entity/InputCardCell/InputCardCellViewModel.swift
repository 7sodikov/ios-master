//
//  InputCardCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation


class InputCardCellViewModel: TableViewModel {
    
    override var cellType: TableViewCell.Type {
        InputCardCell.self
    }
    
    let headerText: String
    let cardLogo: String
    let cardNumber: String
    let cardOwnerName: String
    let errorText: String?
    var placeholder: String = "XXXX XXXX XXXX XXXX"
    var tag: Int = 0
    var message: String = ""
    
    internal init(headerText: String, cardLogo: String, cardNumber: String, cardOwnerName: String, errorText: String? = nil) {
        self.headerText = headerText
        self.cardLogo = cardLogo
        self.cardNumber = cardNumber
        self.cardOwnerName = cardOwnerName
        self.errorText = errorText
    }
    
    var attributedString: NSAttributedString? {
        var attributes: [NSAttributedString.Key : Any] = [:]
        attributes[.font] = UIFont.gothamNarrow(size: 16, .book)
        
        if let errorText = self.errorText {
            attributes[.foregroundColor] = UIColor.red
            return .init(string: errorText, attributes: attributes)
        }
        
        attributes[.foregroundColor] = UIColor(145, 150, 155)
        return .init(string: cardOwnerName, attributes: attributes)
    }
    
    var isHiddenScanButton: Bool {
        tag == 1
    }
    
}
