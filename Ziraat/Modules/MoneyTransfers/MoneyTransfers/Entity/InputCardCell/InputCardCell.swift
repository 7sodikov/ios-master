//
//  InputCardCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import Kingfisher

protocol InputCardCellDelegate: AnyObject {
    func inputCell(cardNumber: String)
    func inputCell(account: String)
    func historyBtnClicked()
    func scanBtnClicked()
}


class InputCardCell: TableViewCell, InputCardCellViewInsatller {
    
    var headerTitleLabel: UILabel!
    var messageLabel: UILabel!
    var cardContainer: UIView!
    var textField: UITextField!
    var stackView: UIStackView!
    var logoImageView: UIImageView!
    var cardNameLabel: UILabel!
    var buttonStackView: UIStackView!
    var scanButton: UIButton!
    var historyButton: UIButton!
    
    var mainView: UIView { self.contentView }
    
    weak var inputCardCellDelegate: InputCardCellDelegate? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTarget()
    }
    
    func setupTarget() {
        scanButton.addTarget(self, action: #selector(scanBtnClicked), for: .touchUpInside)
        historyButton.addTarget(self, action: #selector(historyBtnClicked), for: .touchUpInside)
        textField.addTarget(self, action: #selector(didEnterCardNumber), for: .editingChanged)
        textField.delegate = self
    }
    
    @objc func historyBtnClicked() {
        inputCardCellDelegate?.historyBtnClicked()
    }
    
    @objc func scanBtnClicked() {
        inputCardCellDelegate?.scanBtnClicked()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? InputCardCellViewModel else { return }
        
        headerTitleLabel.text = viewModel.headerText
        textField.text = viewModel.cardNumber
        cardNameLabel.attributedText = viewModel.attributedString
        scanButton.isHidden = viewModel.isHiddenScanButton
        textField.placeholder = viewModel.placeholder
        messageLabel.text = viewModel.message
        tag = viewModel.tag
        if !viewModel.cardLogo.isEmpty {
            logoImageView.kf.setImage(with: URL(string: viewModel.cardLogo), placeholder: UIImage(named: "img_conver_detail"))
        } else {
            let icon = tag == 1 ? "img_Icones_ZiraatBank2" : "img_conver_detail"
            logoImageView.image = UIImage(named: icon)
        }
        
    }
    
    @objc private func didEnterCardNumber(_ sender: UITextField) {
        guard let text = sender.text?.withoutWhiteSpace else { return }
        if tag == 0 {
            if text.count > 16 {
                textField.deleteBackward()
                return
            }
            
            textField.text = text.makeReadableCardNumber
            inputCardCellDelegate?.inputCell(cardNumber: text)
        } else if tag == 1 {
            if text.count > 20 {
                textField.deleteBackward()
                return
            }
            inputCardCellDelegate?.inputCell(account: text)
        }
    }
    
}

extension InputCardCell: UITextFieldDelegate {
    
//    public func textFieldDidBeginEditing(_ textField: UITextField) {
//        guard let text = textField.text, text.isEmpty else { return }
//        textField.text = nil
//    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newPosition = textField.endOfDocument
        textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
        [4, 9, 14].forEach { if range.contains($0) { textField.text?.removeLast() } }
        return true
    }
}
