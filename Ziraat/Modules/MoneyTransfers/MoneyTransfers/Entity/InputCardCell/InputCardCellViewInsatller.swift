//
//  InputCardCellViewInsatller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol InputCardCellViewInsatller: ViewInstaller {
    var headerTitleLabel: UILabel! {get set}
    
    var cardContainer: UIView! {get set}
    var textField: UITextField! {get set}
    var stackView: UIStackView! {get set}
    var logoImageView: UIImageView! {get set}
    var cardNameLabel: UILabel! {get set}
    var buttonStackView: UIStackView! {get set}
    var scanButton: UIButton! {get set}
    var historyButton: UIButton! {get set}
    var messageLabel: UILabel! {get set}
}

extension InputCardCellViewInsatller {
    
    func initSubviews() {
        headerTitleLabel = UILabel()
        headerTitleLabel.font = .gothamNarrow(size: 16, .book)
        headerTitleLabel.textColor = UIColor(68, 80, 86)
        
        cardContainer = UIView()
        cardContainer.backgroundColor = .white
        cardContainer.layer.cornerRadius = 6
        cardContainer.addShadow()
        
        textField = UITextField()
        textField.placeholder = "XXXX XXXX XXXX XXXX"
        textField.backgroundColor = .clear
        textField.font = .gothamNarrow(size: 16, .book)
        textField.keyboardType = .numberPad
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.distribution = .fill
        
        logoImageView = UIImageView()
        logoImageView.image = UIImage(named: "img_conver_detail")
       
        cardNameLabel = UILabel()
        cardNameLabel.font = .systemFont(ofSize: 14)
        cardNameLabel.textColor = UIColor(red: 145/255, green: 150/255, blue: 155/255, alpha: 1)
        
        buttonStackView = UIStackView()
        buttonStackView.spacing = 16
        
        scanButton = UIButton()
        scanButton.setImage(UIImage(named: "img_card_scan_outline"), for: .normal)
        
        historyButton = UIButton()
        historyButton.setImage(UIImage(named: "img_myCards"), for: .normal)
        if #available(iOS 13.0, *) {
            historyButton.setImage(UIImage(named: "img_myCards")?.withTintColor(.lightGray), for: .highlighted)
        }
        
        messageLabel = UILabel()
        messageLabel.font = .gothamNarrow(size: 16, .book)
        messageLabel.textColor = UIColor(225,5,20)
       
    }
    
    func embedSubviews() {
        mainView.addSubview(headerTitleLabel)
        mainView.addSubview(cardContainer)
        mainView.addSubview(messageLabel)
        
        cardContainer.addSubview(logoImageView)
        cardContainer.addSubview(stackView)
        cardContainer.addSubview(buttonStackView)
        
        buttonStackView.addArrangedSubview(scanButton)
        buttonStackView.addArrangedSubview(historyButton)
        
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(cardNameLabel)
    }
    
    func addSubviewsConstraints() {
        headerTitleLabel.constraint { (make) in
            make.top(16).horizontal(16).equalToSuperView()
        }
        
        cardContainer.constraint { (make) in
            make.height(74)
                .top(self.headerTitleLabel.bottomAnchor, offset: 8)
                .horizontal(16).equalToSuperView()
        }
        
        logoImageView.constraint { (make) in
            make.square(36).centerY.leading(8).equalToSuperView()
        }
        
        buttonStackView.constraint { (make) in
            make.centerY.trailing(-16).equalToSuperView()
        }
        
        [scanButton, historyButton].forEach { (button) in
            button.constraint { $0.square(24) }
        }

        stackView.constraint { (make) in
            make.leading(self.logoImageView.trailingAnchor, offset: 8)
                .trailing(self.buttonStackView.leadingAnchor, offset: -16)
                .centerY.equalToSuperView()
        }
        messageLabel.constraint { (make) in
            make.top(self.cardContainer.bottomAnchor, offset: 4)
                .horizontal(16).bottom.equalToSuperView()
        }
        
    }
    
}
