//
//  TypeCreditMfoCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol TypeCreditMfoCellDelegate: AnyObject {
    func didEnter(mfo: String)
    func didClickedCreditType()
}

class TypeCreditMfoCell: TableViewCell, TypeCreditMfoViewInstaller {
    
    var mfoTitle: UILabel!
    var mfoTextField: TextField!
    var mfoStackView: UIStackView!
    
    var creditTypeTitle: UILabel!
    var creditTypeContainer: UIView!
    var creditTypeButton: UIButton!
    var creditTypeTextField: TextField!
    var creditTypeStackView: UIStackView!
    var messageLabel: UILabel!
    
    var mainView: UIView { self.contentView }
    
    weak var creditMfoDelegate: TypeCreditMfoCellDelegate? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTarget()
    }
    
    func setupTarget() {
        mfoTextField.addTarget(self, action: #selector(didEnterMfo), for: .editingChanged)
        creditTypeTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(creditTypeCklicked)))
        creditTypeButton.addTarget(self, action: #selector(creditTypeCklicked), for: .touchUpInside)
    }
    
    @objc func creditTypeCklicked() {
        creditMfoDelegate?.didClickedCreditType()
    }
    
   
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? TypeCreditMfoViewModel else { return }
        
        mfoTextField.text = viewModel.mfo
        creditTypeTextField.text = viewModel.typeCredit
        messageLabel.isHidden = !viewModel.showMessage
    }
    
    @objc private func didEnterMfo(_ sender: UITextField) {
        guard let mfo = sender.text?.withoutWhiteSpace else { return }
        
        if mfo.count > 5 {
            mfoTextField.deleteBackward()
            return
        }
        creditMfoDelegate?.didEnter(mfo: mfo)
    }
    
}
