//
//  TypeCreditMfoViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol TypeCreditMfoViewInstaller: ViewInstaller {
    var mfoTitle: UILabel! {get set}
    var mfoTextField: TextField! {get set}
    var mfoStackView: UIStackView! {get set}
    
    var creditTypeTitle: UILabel! {get set}
    var creditTypeContainer: UIView! {get set}
    var creditTypeButton: UIButton! {get set}
    var creditTypeTextField: TextField! {get set}
    var creditTypeStackView: UIStackView! {get set}
    
    var messageLabel: UILabel! {get set}
}

extension TypeCreditMfoViewInstaller {
    
    func initSubviews() {
        mfoTitle = UILabel()
        mfoTitle.font = .gothamNarrow(size: 16, .book)
        mfoTitle.textColor = UIColor.buttonBlack
        mfoTitle.text = RS.lbl_mfo.localized()
        
        mfoTextField = TextField()
        mfoTextField.layer.cornerRadius = 6
        mfoTextField.backgroundColor = .white
        mfoTextField.addShadow()
        let marginView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 5))
        mfoTextField.addLeftSide(view: marginView)
        mfoTextField.placeholder = RS.lbl_mfo.localized()
        
        mfoStackView = UIStackView()
        mfoStackView.axis = .vertical
        mfoStackView.spacing = 14
        
        creditTypeTitle = UILabel()
        creditTypeTitle.font = .gothamNarrow(size: 16, .book)
        creditTypeTitle.textColor = UIColor.buttonBlack
        creditTypeTitle.text = RS.lbl_repayment_type.localized()
        
        creditTypeTextField = TextField()
        creditTypeTextField.addLeftSide(view: UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 5)))
        creditTypeTextField.isUserInteractionEnabled = false
        
        creditTypeContainer = UIView()
        creditTypeContainer.backgroundColor = .white
        creditTypeContainer.layer.cornerRadius = 6
        
        creditTypeContainer.addShadow()
        
        creditTypeStackView = UIStackView()
        creditTypeStackView.axis = .vertical
        creditTypeStackView.spacing = 14
        
        creditTypeButton = UIButton()
        creditTypeButton.setImage(UIImage(named: "img_jam_chevron-left")?.withRenderingMode(.alwaysTemplate), for: .normal)
        creditTypeButton.tintColor = UIColor.buttonBlack
        
        messageLabel = UILabel()
        messageLabel.font = .gothamNarrow(size: 16, .book)
        messageLabel.textColor = UIColor(225,5,20)
        messageLabel.text = RS.lbl_tip.localized()
    }
    
    func embedSubviews() {
        mainView.addSubview(mfoStackView)
        mainView.addSubview(creditTypeStackView)
        mainView.addSubview(messageLabel)
        
        mfoStackView.addArrangedSubview(mfoTitle)
        mfoStackView.addArrangedSubview(mfoTextField)
        
        creditTypeStackView.addArrangedSubview(creditTypeTitle)
        creditTypeStackView.addArrangedSubview(creditTypeContainer)
        
        creditTypeContainer.addSubview(creditTypeTextField)
        creditTypeContainer.addSubview(creditTypeButton)
    }
    
    func addSubviewsConstraints() {
        mfoStackView.constraint { (make) in
            make.height(84).width(Adaptive.val(105))
            make.leading(16).top.equalToSuperView()
        }
        mfoTextField.constraint{ $0.height(50) }
        
        creditTypeStackView.constraint { (make) in
            make.height(84).leading(self.mfoStackView.trailingAnchor, offset: 10)
                .trailing(-16).top.equalToSuperView()
        }
        creditTypeTextField.constraint { $0.height(50) }
        
        creditTypeButton.constraint { (make) in
            make.square(24).trailing(-9).centerY.equalToSuperView()
        }
        creditTypeTextField.constraint { (make) in
            make.trailing(self.creditTypeButton.leadingAnchor, offset: -8).leading.vertical.equalToSuperView()
        }
        messageLabel.constraint { (make) in
            make.top(self.mfoStackView.bottomAnchor, offset: 4)
                .horizontal(16).bottom.equalToSuperView()
        }
    }
}
