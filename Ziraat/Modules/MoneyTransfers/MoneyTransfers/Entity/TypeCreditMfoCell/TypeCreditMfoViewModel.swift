//
//  TypeCreditMfoViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class TypeCreditMfoViewModel: TableViewModel {
    
    override var cellType: TableViewCell.Type {
        TypeCreditMfoCell.self
    }
    
    let mfo, typeCredit: String
    var showMessage: Bool = false
    
    internal init(mfo: String, typeCredit: String) {
        self.mfo = mfo
        self.typeCredit = typeCredit
    }
    
}
