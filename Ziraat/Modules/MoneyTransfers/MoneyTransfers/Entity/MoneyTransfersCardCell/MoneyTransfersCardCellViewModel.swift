//
//  MoneyTransfersCardCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//


class MoneyTransfersCardCellViewModel: TableViewModel {

    override var cellType: TableViewCell.Type {
        MoneyTransfersCardCell.self
    }
    
    let headerTitle: String
    let cardViewType: CardViewType
    let tag: Int
    
    internal init(headerTitle: String, cardViewType: CardViewType, tag: Int) {
        self.headerTitle = headerTitle
        self.cardViewType = cardViewType
        self.tag = tag
    }
    
}


extension MoneyTransfersCardCellViewModel {
    
    enum CardViewType {
        case card(card: Card)
        case account(account: Account)
        case none
    }
    
}
