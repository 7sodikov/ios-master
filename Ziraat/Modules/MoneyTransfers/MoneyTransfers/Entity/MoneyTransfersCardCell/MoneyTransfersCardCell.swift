//
//  MoneyTransfersCardCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MoneyTransfersCardCellDelegate: class {
    func cardButtonClicked()
    func selectCard(cardViewType: MoneyTransfersCardCellViewModel.CardViewType)
}


class MoneyTransfersCardCell: TableViewCell, MoneyTransfersCardCellViewInstaller {
    typealias CardViewType = MoneyTransfersCardCellViewModel.CardViewType
    
    var headerTitleLabel: UILabel!
    var cardView: ConversionCardCell.CardView!
    
    var mainView: UIView { self.contentView }
    
    private var viewType: CardViewType? = nil
    weak var moneyTransfersDelegate: MoneyTransfersCardCellDelegate? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTarget()
    }
    
    func setupTarget() {
        cardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnCard)))
        cardView.button.addTarget(self, action: #selector(cardButtonClicked), for: .touchUpInside)
    }
    
    @objc func cardButtonClicked() {
        moneyTransfersDelegate?.cardButtonClicked()
    }
    
    @objc func didTapOnCard() {
        guard let cardViewType = self.viewType else { return }
        moneyTransfersDelegate?.selectCard(cardViewType: cardViewType)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? MoneyTransfersCardCellViewModel else { return }
        
        headerTitleLabel.text = viewModel.headerTitle
        viewType = viewModel.cardViewType
        
        configure(viewModel.cardViewType)
    }
    
    private func configure(_ viewType: CardViewType) {
        func format(amount: Double, currency: CurrencyType) -> String {
            amount.description.makeReadableAmount + " " + currency.data.label
        }
        
        switch viewType {
        case let .card(card):
            cardView.set(imageUrl: card.additions.type,
                         cardNumber: card.pan.makeReadableCardNumber,
                         amount: format(amount: card.balance, currency: card.currency))
        case let .account(account):
            cardView.set(imageUrl: .init(),
                         cardNumber: account.number,
                         amount: format(amount: account.balance, currency: account.currency))
        case .none:
            break
        }
    }
    
}
