//
//  MoneyTransfersCardCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol MoneyTransfersCardCellViewInstaller: ViewInstaller {
    typealias CardView = ConversionCardCell.CardView
    var headerTitleLabel: UILabel! {get set}
    var cardView: CardView! {get set}
}

extension MoneyTransfersCardCellViewInstaller {
    
    func initSubviews() {
        headerTitleLabel = UILabel()
        headerTitleLabel.font = .gothamNarrow(size: 16, .book)
        headerTitleLabel.textColor = UIColor(68, 80, 86)
        
        cardView = .init()
    }
    
    func embedSubviews() {
        mainView.addSubview(headerTitleLabel)
        mainView.addSubview(cardView)
    }
    
    func addSubviewsConstraints() {
        headerTitleLabel.constraint { (make) in
            make.top(16).horizontal(16).equalToSuperView()
        }
        cardView.constraint { (make) in
            make.height(74)
                .top(self.headerTitleLabel.bottomAnchor, offset: 8)
                .horizontal(16).bottom.equalToSuperView()
        }
    }
    
}

