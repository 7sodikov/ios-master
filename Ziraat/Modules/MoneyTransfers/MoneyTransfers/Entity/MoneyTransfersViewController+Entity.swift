//
//  MoneyTransfersViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/29/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import Rswift

protocol MoneyTransfersBase {
//    typealias Row = MoneyTransfersViewController.Row
    typealias Segment = MoneyTransfersViewController.Segment
    typealias CreditType = MoneyTransfersViewController.CreditType
    typealias InputRow = MoneyTransfersViewController.InputRow
}


extension MoneyTransfersViewController {
    
    enum Segment: Int, CaseIterable {
        case cards
        case account
        case credit
        
        var title: String {
            let dic: [Segment: StringResource] = [
                .cards: RS.lbl_universal_to_card,
                .account: RS.lbl_universal_to_account,
                .credit: RS.lbl_universal_loan_repayment
            ]
            return dic[self]!.localized().uppercased()
        }
        
        func operationType(cardOrAccount: Any) -> OperationPrepareType {
            switch self {
                
            case .cards:
                return cardOrAccount is Card ? .p2p : .a2p
            case .account:
                return .p2a
            case .credit:
                return cardOrAccount is Card ? .p2l : .a2l
            }
        }
        
        static func controlIndex(operationType: OperationPrepareType) -> Segment {
            switch operationType {
            case .p2a:
                return .account
            case .p2l, .a2l:
                return .credit
            default:
                return .cards
            }
        }
    }
    
    enum CreditType: Int, CaseIterable {
        case schedule
        case early
        
        init?(title: String) {
            if title == CreditType.schedule.title {
                self.init(rawValue: CreditType.schedule.rawValue)
            } else if title == CreditType.early.title {
                self.init(rawValue: CreditType.early.rawValue)
            }
            self.init(rawValue: -1)
        }
        
        var title: String {
            switch self {
            case .schedule:
                return RS.btn_payoff.localized()
            case .early:
                return RS.lbl_early_repayment.localized()
            }
        }
    }
    
    enum InputRow: Int {
        case cardAmount
        case accountMfo
        case accountAmount
        case creditNumber
        case creditAmount
        case creditMfo
        
        static func amountTag(segment: Segment) -> InputRow {
            switch segment {
            case .account:
                return .accountAmount
            case .cards:
                return  .cardAmount
            case .credit:
                return  .creditAmount
            }
        }
    }
    
}
