//
//  MoneyTransfersInteractor+Networking.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/28/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

extension MoneyTransfersInteractor {
    
    func fetchCardOwner(completion: @escaping (Bool)->Void) {
        NetworkService.Card.cardOwner(cardId: nil, pan: cardNumber) { (response) in
            switch response {
            case .success(let data):
                self.cardOwnerName = data.data.ownerName
                self.cardLogo = data.data.additions.type
                self.cardValid = true
                completion(true)
            case .failure(let error):
                self.showError?(error.localizedDescription)
                completion(false)
            }
        }
    }
    
    func fetchAccountOwner(completion: @escaping (Bool)->Void) {
        
        NetworkService.Accounts.accountOwner(accountNumber: accountNumber, branch: accountMfo) { (response) in
            switch response {
            case .success(let owner):
                self.accountOwner = owner.data.ownerName
                self.cardLogo = owner.data.additions.type
                self.accountValid = true
                completion(true)
            case .failure(let error):
                self.showError?(error.localizedDescription)
                completion(false)
            }
        }
    }
    
    func fetchLoanOwner(completion: @escaping(Bool) -> Void) {
        guard let loanId = UInt64(creditId) else {
            showNotify?("incorrect loan ID o_o")
            return
        }
        NetworkService.Loan.loanOwner(loanId: loanId, branch: creditMfo) { [weak self] (response) in
            switch response {
            case .success(let result):
                self?.creditOwner = result.data.ownerName
                self?.creditNumberValid = true
                completion(true)
            case .failure(let error):
                self?.showError?(error.localizedDescription)
                completion(false)
            }
            
        }
    }
    
    func fetchData(completion: @escaping ()->Void) {
        func configure() {
            cards = cards.map { (item) -> Card in
                var card = item
                if let cardBalance = cardBalances.first(where:{$0.cardId == card.cardId}) {
                    card.update(balance: cardBalance.balance, currency: cardBalance.currency)
                }
                return card
            }.filter(\.isUzb)
            
            if isSenderCard,
               let cardId = self.senderCardId,
               !cardId.isEmpty ,
               let card = cards.first(where:{$0.cardId == cardId}) {
                sender = card
            } else if let cardId = self.senderCardId,
                      !cardId.isEmpty,
                      let account = accounts.first(where:{$0.number == cardId}) {
                sender = account
            } else if let card = cards.first {
                sender = card
            } else if let account = accounts.first {
                sender = account
            }
        }
        
        let group = DispatchGroup()
        
        let items = [fetchCardList, fetchCardBalance, fetchAccountList]
        items.forEach { (itemAction) in
            group.enter()
            itemAction(group.leave)
        }
        
        group.notify(queue: .main) {
            configure()
            completion()
        }
    }
    
    private func fetchCardList(completion: @escaping()->Void) {
        NetworkService.Card.cardsList { (response) in
            switch response {
            
            case .success(let result):
                self.cards = result.data.cards.compactMap(\.card)
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    private func fetchCardBalance(completion: @escaping()->Void) {
        NetworkService.Card.cardBalance { (response) in
            switch response {
            case .success(let result):
                self.cardBalances = result.data.cards?.map(\.cardBalance) ?? []
                
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    private func fetchAccountList(completion: @escaping()->Void) {
        NetworkService.Accounts.accountsList { (response) in
            switch response {
            case .success(let result):
                self.accounts = result.data.accounts.map(\.account).filter(\.isUZB)
                
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    func operationPrepare(completion: @escaping (Bool) -> Void) {
        guard let sender = self.sender else { return }
        let operationType = currentSegment.operationType(cardOrAccount: sender)
        let amount = (Double(self.amount) ?? 0) * 100
        let cardId = (sender as? Card)?.cardId
        let senderCardId = UInt64(cardId ?? "-1")
        
        switch operationType {
        case .p2p:
            let uniqueId: String? = receiverHistory?.id ?? (cardNumber.isPan ? nil : cardNumber)
            operationPrepareRequest(
                operationType: .p2p,
                amount: amount,
                senderCardId: senderCardId,
                receiverId: self.receiverHistoryCardId == nil ? nil : UInt64(self.receiverHistoryCardId!),
                receiverCard: uniqueId,
                completion: completion
            )
            
        case .a2p:
            let receiverCard: String? = receiverHistory?.id ?? (cardNumber.isPan ? nil : cardNumber)
            let receiverCardId = self.receiverHistoryCardId == nil ? nil : UInt64(self.receiverHistoryCardId!)
            let accountNumber = (sender as? Account)?.number
            let accountMfo = (sender as? Account)?.branch
            operationPrepareRequest(
                operationType: .a2p,
                amount: amount,
                accountNumber: accountNumber,
                accountBranch: accountMfo,
                receiverId: receiverCardId,
                receiverCard: receiverCard,
                completion: completion
            )
        case .p2a:
            operationPrepareRequest(
                operationType: .p2a,
                amount: amount,
                senderCardId: senderCardId,
                receiverCard: accountNumber,
                receiverBranch: accountMfo,
                completion: completion
            )
        case .p2l:
            operationPrepareRequest(
                operationType: .p2l,
                amount: amount,
                senderCardId: senderCardId,
                receiverId: UInt64(creditId),
                receiverBranch: creditMfo,
                paramBool: creditType == .schedule,
                completion: completion
            )
        case .a2l:
            let accountNumber = (sender as? Account)?.number
            let accountMfo = (sender as? Account)?.branch
            operationPrepareRequest(
                operationType: .a2l,
                amount: amount,
                accountNumber: accountNumber,
                accountBranch: accountMfo,
                receiverId: UInt64(creditId),
                receiverBranch: creditMfo,
                paramBool: creditType == .schedule,
                completion: completion
            )
        default:
            break
        }
    }
    
    private func operationPrepareRequest(
        operationType: OperationPrepareType,
        amount: Double,
        senderCardId: UInt64? = nil,
        accountNumber: String? = nil,
        accountBranch: String? = nil,
        receiverId: UInt64? = nil,
        receiverCard: String? = nil,
        receiverBranch: String? = nil,
        paramBool: Bool? = nil,
        completion: @escaping (Bool) -> Void
    ) {
        NetworkService.Operation.operationPrepare(
            prepareType: operationType,
            sender: accountNumber,
            senderId: senderCardId,
            senderBranch: accountBranch,
            receiver: receiverCard,
            receiverId: receiverId,
            receiverBranch: receiverBranch,
            paramNum: nil, paramStr: nil,
            paramBool: paramBool,
            amount: amount) { (response) in
            
            switch response {
            
            case .success(let result):
                self.operationPrepare = result.data
                completion(true)
            case .failure(let error):
                self.showError?(error.localizedDescription)
                completion(false)
            }
        }
    }
    
}

fileprivate extension String {
    
    var isPan: Bool {
        self.contains("*")
    }
    
}
