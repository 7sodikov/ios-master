//
//  MoneyTransfersPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/29/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol MoneyTransfersPresentationLogic: PresentationProtocol, MoneyTransfersBase, TablePresentationLogic {
    var currentSegment: Segment { get }
    
    func wellNext()
    func showHistory()
    func willScanCard()
    func prepareCreditTypes()
    func willSet(cardNumber: String)
    func willSet(text: String, row: InputRow)
    func willSet(account: String)
    func willSet(segmentdedControl: Int)
    func selectCard(cardViewType: MoneyTransfersCardCellViewModel.CardViewType)
}


class MoneyTransfersPresenter: MoneyTransfersPresentationLogic {
    
    
    private unowned let view: MoneyTransfersDisplayLogic
    private var interactor: MoneyTransfersBusinessLogic
    private let router: MoneyTransfersRoutingLogic
    
    private var controller: MoneyTransfersViewController { view as! MoneyTransfersViewController }
    var currentSegment: Segment { interactor.currentSegment }
    
    init(view: MoneyTransfersDisplayLogic, interactor: MoneyTransfersBusinessLogic, router: MoneyTransfersRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        
        view.setupSubviews()
        binding()
        interactor.update()
        fetchData()
    }
    
    func viewWillAppear(_ animated: Bool) {
        view.segmentedControl.setIndex(index: Segment.controlIndex(operationType: interactor.operationType).rawValue)
    }
    
    private func binding() {
        interactor.showError = { message in
            self.controller.showError(message: message)
        }
        interactor.showNotify = { message in
            self.controller.spinner(.stop)
            self.showNotify(message: message)
        }
        interactor.reload = { self.view.tableView.reloadData() }
        interactor.willFetchAccountOwner = { self.fetchAccountOwner() }
        interactor.willFetchLoanOwner = { self.fetchCreditOwner() }
        interactor.willFetchCardOwner = { self.fetchCardOwner() }
    }
    
    func willSet(cardNumber: String) {
        interactor.set(cardNumber: cardNumber)
    }
    
    func wellNext() {
        if interactor.canNext {
            operationPrepare()
        } else {
            view.tableView.reloadData()
        }
    }
    
    func operationPrepare() {
        controller.spinner(.start)
        interactor.operationPrepare { (success) in
            self.controller.spinner(.stop)
            if success {
                guard let operation = self.interactor.operationPrepare else {
                    return
                }
                self.router.openPrepareScreen(details: self.interactor.prepareDetails, token: operation.token)
            }
        }
    }
    
    func showHistory() {
        switch interactor.currentSegment {
        
        case .cards:
            router.openReceiverHistory(
                delegate: self,
                cards: interactor.receiverCards,
                isA2P: currentSegment == .cards && interactor.sender is Account
            )
        case .account:
            presentAccounts()
        case .credit:
            break
        }
    }
    
    func willScanCard() {
        let controller = ScanViewController.createViewController(withDelegate: self)
        controller?.allowSkip = true
        controller?.stringDataSource = self
        self.controller.present(controller!, animated: true, completion: nil)
    }
    
    func willSet(account: String) {
        interactor.set(account: account)
    }
    
    func willSet(text: String, row: InputRow) {
        interactor.set(text: text, row: row)
    }
    
    func willSet(segmentdedControl index: Int) {
        interactor.set(segmentdedControl: index)
        view.tableView.reloadData()
    }
    
    func presentAccounts() {
        let bottomSheet = interactor.accountBottomSheet
        bottomSheet.delegate = self
        router.presentBottomSheet(bottomSheet: bottomSheet)
    }
    
    func prepareCreditTypes() {
        let bottomSheet = interactor.creditTypeBottomSheet
        bottomSheet.delegate = self
        router.presentBottomSheet(bottomSheet: bottomSheet)
    }
    
    func selectCard(cardViewType: MoneyTransfersCardCellViewModel.CardViewType) {
        let bottomSheet = interactor.cardBottomSheet
        bottomSheet.delegate = self
        router.presentBottomSheet(bottomSheet: bottomSheet)
    }
    
    func fetchData() {
        view.indicator(.start)
        interactor.fetchData { [weak self] in
            guard self != nil, let view = self?.view else { return }
            view.indicator(.stop)
            if (self?.interactor.canReload ?? false) {
                view.tableView.reloadData()
            }
        }
    }
    
    func fetchAccountOwner() {
        controller.spinner(.start)
        interactor.fetchAccountOwner { (success) in
            self.controller.spinner(.stop)
            if success {
                self.view.tableView.reloadData()
            }
        }
    }
    
    func fetchCardOwner() {
        controller.spinner(.start)
        interactor.fetchCardOwner { (success) in
            self.controller.spinner(.stop)
            if success {
                self.view.tableView.reloadData()
            }
        }
    }
    
    func fetchCreditOwner() {
        controller.spinner(.start)
        interactor.fetchLoanOwner { [weak self] success in
            self?.controller.spinner(.stop)
            if success {
                self?.view.tableView.reloadData()
            }
        }
    }
    
    func showNotify(message: String) {
        let notify = NotifyViewController(.custom(message: message)) {}
        controller.present(notify, animated: true, completion: nil)
        
    }
}


// MARK: -TablePresentationLogic
extension MoneyTransfersPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
    func tablePresenter(_ table: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
    }
    
}

//MARK: - Bottom Sheet
extension MoneyTransfersPresenter: BottomSheetDelegate {
    
    func bottomSheet(didSelectCardAt indexPath: IndexPath, card: Any, with tag: Int) {
        if card is Card { // sender
            self.interactor.setSender(cardOrAccount: card)
        } else if card is Account { // reciever
            (currentSegment == .account
                ? self.interactor.setReceiver
                : self.interactor.setSender)(card)
        }
        view.tableView.reloadData()
    }
    
    func didSelect(item viewModel: TableViewModel, at indexPath: IndexPath, with tag: Int) {
        if viewModel is SingleLabelCellViewModel {
            interactor.set(creditType: CreditType.allCases[indexPath.row])
            view.tableView.reloadData()
        }
    }
    
    
}

// MARK: - Scan card
extension MoneyTransfersPresenter: ScanDelegate, ScanStringsDataSource {
    func userDidCancel(_ scanViewController: ScanViewController) {
        scanViewController.dismiss(animated: true, completion: nil)
    }
    func userDidScanCard(_ scanViewController: ScanViewController, creditCard: CreditCard) {
        scanViewController.dismiss(animated: true, completion: {
            self.willSet(cardNumber: creditCard.number)
        })
    }
    
    func userDidSkip(_ scanViewController: ScanViewController) {}
    func scanCard() -> String { .init() }
    func skipButton() -> String { .init() }
    func positionCard() -> String { .init() }
    
    func backButton() -> String {
        RS.lbl_cancel.localized()
    }
    
}

extension MoneyTransfersPresenter: ReceiverHistoryDelegate {
    func didSelect(item: TableViewModel, at indexPath: IndexPath) {
        
    }
    
    func didSelect(receiver: BalanceReceiver) {
        interactor.updateReceiver(receiver: receiver)
        view.tableView.reloadData()
    }
    
    func didSelect(card: Card) {
        self.interactor.setReceiver(account: card)
        view.tableView.reloadData()
    }
    
}
