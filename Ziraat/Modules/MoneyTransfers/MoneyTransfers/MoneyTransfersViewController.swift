//
//  MoneyTransfersViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/29/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol MoneyTransfersDisplayLogic: MoneyTransfersViewInstaller {
    
}


class MoneyTransfersViewController: BaseViewController, MoneyTransfersDisplayLogic, UIViewControllerTransitioningDelegate  {
    
    var backgroundImageView: UIImageView!
    var segmentedControl: CustomSegmentedControl!
    var tableView: UITableView!
    var indicator: UIActivityIndicatorView!
    
    var mainView: UIView { self.view }
    var parameter: Any? { presenter.currentSegment }
    var presenter: MoneyTransfersPresentationLogic!
    
    override func loadView() {
        super.loadView()
        navigationController?.transparentBackgorund()
        title = RS.btn_money_transfer.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        setupTargets()
    }
    
    override func setupTargets() {
        super.setupTargets()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(TableViewCell.self)
        tableView.register(InputCell.self)
        tableView.register(MoneyTransfersCardCell.self)
        tableView.register(InputCardCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(TypeCreditMfoCell.self)
        
        segmentedControl.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear(animated)
    }
    
}

extension MoneyTransfersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        configure(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 0.01)))
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch presenter.currentSegment {
        case .cards:
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }
            else if section == 1 {
                return 0
            }
            else if section == 2 {
                return 5
            }
            else if section == 3 {
                return 20
            }
        case .account:
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }
            else if section == 1 {
                return 5
            }
            else if section == 2 {
                return 10
            }
            else if section == 3 {
                return 5
            }
            else if section == 4 {
                return 10
            }
        case .credit:
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }
            else if section == 1 {
                return 15
            }
            else if section == 2 {
                return 0
            }
            else if section == 3 {
                return 0
            }
            else if section == 4 {
                return 10
            }
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        .init(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 0.01)))
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch presenter.currentSegment {
        case .cards:
            if section == 0 {
                return 0
            }
            else if section == 1 {
                return 8
            }
            else if section == 2 {
                return 10
            }
        case .account:
            if section == 0 {
                return 0
            }
            else if section == 1 {
                return 5
            }
            else if section == 2 {
                return 0
            }
        case .credit:
            if section == 0 {
                return 5
            }
            else if section == 1 {
                return 0
            }
            else if section == 2 {
                return 3
            }
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    private func configure(cell: TableViewCell) {
        (cell as? InputCardCell)?.inputCardCellDelegate = self
        (cell as? ButtonCell)?.buttonDelegate = self
        (cell as? InputCell)?.inputCellDelegate = self
        (cell as? MoneyTransfersCardCell)?.moneyTransfersDelegate = self
        (cell as? TypeCreditMfoCell)?.creditMfoDelegate = self
    }
}

extension MoneyTransfersViewController: InputCardCellDelegate {
    
    func inputCell(account: String) {
        presenter.willSet(account: account)
    }
    
    func inputCell(cardNumber: String) {
        presenter.willSet(cardNumber: cardNumber)
    }
    
    func historyBtnClicked() {
        presenter.showHistory()
    }
    
    func scanBtnClicked() {
        presenter.willScanCard()
    }
    
}

extension MoneyTransfersViewController: ButtonCellDelegate {
    func buttonClicked(tag: Int) {
        presenter.wellNext()
    }

}

extension MoneyTransfersViewController: InputCellDelegate {

    func input(text: String, tag: Int, uid: String?) {
        guard let row = InputRow(rawValue: tag) else { return }
        presenter.willSet(text: text, row: row)
    }
    
}

extension MoneyTransfersViewController: MoneyTransfersCardCellDelegate {
    func cardButtonClicked() {
        
    }
    
    func selectCard(cardViewType: MoneyTransfersCardCellViewModel.CardViewType) {
        presenter.selectCard(cardViewType: cardViewType)
    }
    
    
}

extension MoneyTransfersViewController: CustomSegmentedControlDelegate {
    
    func changeToIndex(index: Int) {
        guard let _ = Segment(rawValue: index) else {return}
        presenter.willSet(segmentdedControl: index)
    }
    
}

extension MoneyTransfersViewController: TypeCreditMfoCellDelegate {
    
    func didEnter(mfo: String) {
        presenter.willSet(text: mfo, row: .creditMfo)
    }
    
    func didClickedCreditType() {
        presenter.prepareCreditTypes()
    }
    
}
