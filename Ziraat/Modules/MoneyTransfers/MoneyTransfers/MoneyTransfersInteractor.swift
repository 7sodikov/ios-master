//
//  MoneyTransfersInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/29/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import Rswift

protocol MoneyTransfersBusinessLogic: MoneyTransfersBase {
    var currentSegment: Segment { get }
    var receiverCards: [Card] {get}
    var viewModels: [[TableViewModel]] {get}
    var prepareDetails: [DetailsCellViewModel] {get}
    var cardBottomSheet: BottomSheet {get}
    var accountBottomSheet: BottomSheet {get}
    var creditTypeBottomSheet: BottomSheet {get}
    var operationPrepare: OperationPrepareResponse? {get}
    var operationType: OperationPrepareType {get}
    var canReload: Bool {get}
    var canNext: Bool {get}
    var sender: Any? {get}
    
    
    var showError: ((String)->Void)? {get set}
    var showNotify: ((String)->Void)? {get set}
    var reload: (()->())? {get set}
    var willFetchAccountOwner: (()->())? {get set}
    var willFetchLoanOwner: (()->Void)? {get set}
    var willFetchCardOwner: (()->Void)? {get set}
    
    func update()
    func set(cardNumber: String)
    func set(text: String, row: InputRow)
    func set(creditType: CreditType)
    func set(account: String)
    func updateReceiver(receiver: BalanceReceiver)
    func setSender(cardOrAccount: Any)
    func setReceiver(account: Any)
    func set(segmentdedControl index: Int)
    func fetchData(completion: @escaping()->Void)
    func fetchCardOwner(completion: @escaping (Bool)->Void)
    func fetchLoanOwner(completion: @escaping(Bool) -> Void)
    func fetchAccountOwner(completion: @escaping (Bool)->Void)
    func operationPrepare(completion: @escaping (Bool) -> Void)
}

class MoneyTransfersInteractor: MoneyTransfersBusinessLogic {
    
    
    var favoriteItem: FavoriteListOperationsResponse?
    var historyItem: HistoryResponse?
    var receiverHistory: BalanceReceiver?
    
    var reload: (()->())? = nil
    var willFetchAccountOwner: (()->())? = nil
    var showError: ((String)->Void)? = nil
    var showNotify: ((String)->Void)? = nil
    var willFetchLoanOwner: (()->Void)?
    var willFetchCardOwner: (()->Void)?
    
    var currentSegment: Segment = .cards {
        didSet {
            reload?()
            guard let item = sender else { return }
            operationType = currentSegment.operationType(cardOrAccount: item)
        }
    }
    
    var cards: [Card] = .init()
    var accounts: [Account] = .init()
    var cardBalances: [CardBalance] = .init()
    
    var operationPrepare: OperationPrepareResponse? = nil
    
    var operationType: OperationPrepareType = .p2p
    var nextClicked: Bool = false
    var sender: Any? = nil
    var amount: String = ""
    
    var isSenderCard: Bool = true
    var receiverHistoryCardId: String? = nil
    var senderCardId: String? = nil
    var cardNumber: String = ""
    var cardOwnerName: String = ""
    var cardLogo: String = ""
    var cardValid: Bool = false
    
    var accountNumber: String = ""
    var accountOwner = ""
    var accountMfo: String = ""
    var accountLogo: String = ""
    var accountValid: Bool = false
    
    var creditId: String = ""
    var creditOwner: String = .init()
    var creditMfo: String = .init()
    var creditType: CreditType = .schedule
    var creditNumberValid: Bool = false
    
    var canReload: Bool {
        !cards.isEmpty && !accounts.isEmpty && !cardBalances.isEmpty
    }
    
    var receiverCards: [Card] {
        let cardId = (sender as? Card)?.cardId ?? ""
        let isA2P = sender is Account
        var receiverCards = cards.filter({ $0.cardId != cardId })
        if isA2P {
            receiverCards = receiverCards
                .filter { $0.pan.starts(with: "860020") || $0.pan.starts(with: "986029") }
        }
        return receiverCards
    }
    
    var canNext: Bool {
        nextClicked = true
        let amountValid = Double(amount) != nil && Double(amount)! >= 1000
        switch currentSegment {
        
        case .cards:
            return cardValid && amountValid
        case .account:
            return accountValid && amountValid
        case .credit:
            return creditNumberValid && amountValid
        }
    }
    
    
    init(for favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?) {
        self.historyItem = historyItem
        self.favoriteItem = favoriteItem
        if let history = historyItem {
            let type = (history.operationMode?.rawValue) ?? "p2p"
            operationType = OperationPrepareType(rawValue: type) ?? .p2p
            currentSegment = Segment.controlIndex(operationType: operationType)
        }
//        update()
    }
    
    
    func update() {
        if let history = historyItem {
            amount = (history.amount / 100).description
            isSenderCard = operationType == .p2p || operationType == .p2l
            senderCardId = isSenderCard ? history.senderId?.description : history.sender
            
            switch operationType {
            case .p2p, .a2p:
                if !isSenderCard {
                    accountNumber = history.sender ?? ""
                    accountMfo = history.senderBranch ?? ""
                    accountLogo = history.icon ?? ""
                } else {
                    cardOwnerName = history.receiverOwner ?? ""
                    cardLogo = history.icon ?? ""
                    set(cardNumber: history.receiver ?? "")
                }
            case .p2l, .a2l:
                creditOwner = history.receiverOwner ?? ""
                set(text: history.receiverId?.description ?? "", row: .creditNumber)
                set(text: history.receiverBranch ?? "", row: .creditMfo)
                isSenderCard = operationType == .p2l
            case .p2a:
                accountOwner = history.receiverOwner ?? ""
                set(account: history.receiver ?? "")
                set(text: history.receiverBranch ?? "", row: .accountMfo)

            default:
                break
            }
            
        }
    }
    
    func updateReceiver(receiver: BalanceReceiver) {
        self.receiverHistory = receiver
        cardOwnerName = receiver.owner
        cardNumber = receiver.pan
        cardLogo = receiver.additions.type
        cardValid = true
        receiverHistoryCardId = nil
    }
    
    func set(cardNumber: String) {
        self.cardNumber = cardNumber
        validateFetchingCardOwner()
    }
    
    func set(account: String) {
        accountNumber = account
        validateFetchingAccountOwner()
    }

    private func validateFetchingAccountOwner() {
        if accountMfo.count == 5 && accountNumber.count == 20 {
            willFetchAccountOwner?()
        }
    }
    
    private func validateFetchingCardOwner() {
        guard cardNumber.count == 16 else { return }
        willFetchCardOwner?()
    }
    
    private func validateFetchingCreditOwner() {
        if creditMfo.count == 5 {
            willFetchLoanOwner?()
        }
    }
    
    func set(text: String, row: InputRow) {
        switch row {
        
        case .cardAmount, .accountAmount,  .creditAmount:
            amount = text
        case .accountMfo:
            accountMfo = text
            validateFetchingAccountOwner()
        case .creditNumber:
            creditId = text
            validateFetchingCreditOwner()
        case .creditMfo:
            self.creditMfo = text
            validateFetchingCreditOwner()
        }
    }
    
    func set(creditType: CreditType) {
        self.creditType = creditType
    }
    
    func setSender(cardOrAccount: Any) {
        self.sender = cardOrAccount
        self.senderCardId = ""
        operationType = currentSegment.operationType(cardOrAccount: cardOrAccount)
    }
    
    func setReceiver(account: Any) {
        if let account = account as? Account {
            accountNumber = account.number
            accountOwner = account.customerName ?? ""
            accountMfo = account.branch ?? ""
            willFetchAccountOwner?()
        } else if let card = account as? Card {
            cardOwnerName = card.owner
            cardLogo = card.additions.type
            cardNumber = card.pan
            receiverHistoryCardId = card.cardId
            cardValid = true
            isSenderCard = true
        }
        receiverHistory = nil
        operationType = currentSegment.operationType(cardOrAccount: account)
    }
    
    func set(segmentdedControl index: Int) {
        guard let segment = Segment(rawValue: index) else { return }
        if currentSegment != segment {
            clear()
        }
        currentSegment = segment
    }
    
    func clear() {
        amount = ""
        accountMfo = ""
        nextClicked = false
        cardLogo = ""
        accountLogo = ""
    }
    
    func clearReceiver() {
        
    }
}

//  MARK: - View models
extension MoneyTransfersInteractor {
    
    var creditTypeBottomSheet: BottomSheet {
//        let header = RS.lbl_repayment_type.localized()
        let rows = CreditType.allCases.map { SingleLabelCellViewModel(text: $0.title) }
        let dataSource = BottomSheetSection(header: "", rows: rows)
        
        return .init(viewHeight: 100, dataSource: [dataSource], with: 2)
    }
    
    var cardBottomSheet: BottomSheet {
        
        let cardsHeader = RS.lbl_cards.localized()
        let cardsRow = CardsRouletteModel(header: cardsHeader, viewModels: cardViewModels)
        let cardSection = BottomSheetSection(header: "", rows: [cardsRow])
        
        let accountHeader = RS.lbl_accounts.localized()
        let accountRow = CardsRouletteModel(header: accountHeader, viewModels: accountViewModels)
        let accountSection = BottomSheetSection(header: "", rows: [accountRow])
        
        var dataSource = [cardSection]
        var height: CGFloat = 180
        if [Segment.cards, .credit].contains(currentSegment) {
            dataSource.append(accountSection)
            height += 240
        }
        
        return .init(viewHeight: height, dataSource: dataSource, with: 0)
    }
    
    var accountBottomSheet: BottomSheet {
        let accountHeader = RS.lbl_accounts.localized()
        let accountRow = CardsRouletteModel(header: accountHeader, viewModels: accountViewModels)
        let accountSection = BottomSheetSection(header: "", rows: [accountRow])
        return .init(viewHeight: 250, dataSource: [accountSection], with: 1)
    }
    
    var prepareDetails: [DetailsCellViewModel] {
        operationPrepare?.prepareDetails ?? []
    }
    
    private var cardViewModels: [CardCellViewModel] {
        cards.map { .init(card: $0) }
    }
    
    private var accountViewModels: [AccountCardCellViewModel] {
        accounts.map{ .init(account: $0, isBalanceHidden: false) }
    }
    
    var viewModels: [[TableViewModel]]  {
        var viewModels: [[TableViewModel]] = []
        viewModels.append(senderViewModel)
        
        switch currentSegment {
        
        case .cards:
            viewModels.append(cardReciever)
        case .account:
            viewModels.append(accountReciever)
            viewModels.append(accountMfoViewModel)
        case .credit:
            viewModels.append(creditInputCell)
            viewModels.append(creditMfoViewModel)
        }
        viewModels.append(inputCellViewModel)
        viewModels.append(buttonViewModel)
        return viewModels
    }
    
    private var senderViewModel: [MoneyTransfersCardCellViewModel] {
        typealias CardViewType = MoneyTransfersCardCellViewModel.CardViewType
        
        let header = RS.lbl_payer.localized()
        var viewType: CardViewType = .none
        
        if let card = sender as? Card {
            viewType = .card(card: card)
        } else if let account = sender as? Account {
            viewType = .account(account: account)
        }
        
        return [.init(headerTitle: header, cardViewType: viewType, tag: currentSegment.rawValue)]
    }
    
    private var cardReciever: [InputCardCellViewModel] {
        let header = RS.lbl_receiver.localized()
        let cardNumber = self.cardNumber.makeReadableCardNumber
        let cardOwner = self.cardOwnerName
        let vm = InputCardCellViewModel(headerText: header, cardLogo: cardLogo, cardNumber: cardNumber, cardOwnerName: cardOwner)
        vm.tag = currentSegment.rawValue
        vm.message = nextClicked && !cardValid ? RS.lbl_tip.localized() : ""
        return [vm]
    }
    
    private var accountReciever: [InputCardCellViewModel] {
        typealias VM = InputCardCellViewModel
        let header = RS.lbl_receiver.localized()
        let vm = VM(headerText: header, cardLogo: accountLogo, cardNumber: accountNumber, cardOwnerName: accountOwner)
        vm.placeholder = RS.lbl_receiver_account.localized()
        vm.tag = currentSegment.rawValue
        vm.message = nextClicked && !accountValid ? RS.lbl_tip.localized() : ""
        return [vm]
    }
    
    private var inputCellViewModel: [InputCellViewModel] {
        typealias VM = InputCellViewModel
        let header = RS.lbl_sum.localized()
        let amount = self.amount.makeReadableAmount
        let placeHolder = RS.txt_f_enter_sum.localized()
        let vm = VM(headerTitle: header, inputType: .amount(amount, placeholder: placeHolder))
        vm.tag = InputRow.amountTag(segment: currentSegment).rawValue
        let message = String(format: RS.txt_f_Sum_should_not_be_less_than_.localized(), "1 000")
        vm.notify = nextClicked && (self.amount.isEmpty || (Double(self.amount) ?? .zero) < 1000) ? .error(message) : .none
        return [vm]
    }
    
    private var accountMfoViewModel: [InputCellViewModel] {
        typealias VM = InputCellViewModel
        let header = RS.lbl_mfo.localized()
        let vm = VM(headerTitle: header, inputType: .mfo(accountMfo, placeholder: header))
        vm.notify = nextClicked && accountMfo.isEmpty ? .error(RS.lbl_tip.localized()) : .none
        vm.tag = InputRow.accountMfo.rawValue
        return [vm]
    }
    
    private var buttonViewModel: [ButtonCellViewModel] {
        [.init(style: .red(title: RS.btn_further.localized()), isEnabled: true, tag: 0)]
    }
    
    private var creditMfoViewModel: [TypeCreditMfoViewModel] {
        let vm = TypeCreditMfoViewModel(mfo: creditMfo, typeCredit: creditType.title)
        vm.showMessage = creditMfo.isEmpty && nextClicked
        return [vm]
    }
    
    private var creditInputCell: [InputCellViewModel] {
        typealias VM = InputCellViewModel
        let header = RS.lbl_loan_id.localized()
        let pr = RS.txt_f_enter_loan_id.localized()
        let vm = VM(headerTitle: header, inputType: .text(creditId, placeholder: pr))
        vm.tag = InputRow.creditNumber.rawValue
        vm.notify = !creditNumberValid && nextClicked ?.error(RS.lbl_tip.localized()) : .notify(creditOwner)
        return [vm]
    }
}
