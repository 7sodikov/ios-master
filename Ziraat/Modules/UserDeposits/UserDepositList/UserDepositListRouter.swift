//
//  UserDepositListRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol UserDepositListRouterProtocol: class {
    static func createModule() -> UIViewController
    func openDetails(_ view :Any, _ deposite:DepositResponse?)
}

class UserDepositListRouter: UserDepositListRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = UserDepositListVC()
        let presenter = UserDepositListPresenter()
        let interactor = UserDepositListInteractor()
        let router = UserDepositListRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    func openDetails(_ view :Any, _ deposite:DepositResponse?){
        let module = UserDepositDetailsRouter.createModule(deposite)
        (view as? UIViewController)?.navigationController?.pushViewController(module, animated: true)
    }
    
}
