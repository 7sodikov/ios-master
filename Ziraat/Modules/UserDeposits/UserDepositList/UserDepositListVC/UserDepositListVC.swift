//
//  UserDepositListVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol UserDepositListVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class UserDepositListVC: BaseViewController, UserDepositListViewInstaller {
    var warningMessageView: NoItemsAvailableView!
    
    var bgView: UIImageView!
    
    var collectionView: UICollectionView!
    
    var reloadButtonView: ReloadButtonView!
    
    var mainView: UIView { view }
    var presenter: UserDepositListPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadClicked), for: .touchUpInside)
        presenter.getList()
    }
    
    private func setupNavigation() {
        self.title = RS.nav_ttl_my_deposits.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil) 
    }
    
    @objc  func reloadClicked(){
        presenter.getList()
    }
}

extension UserDepositListVC: UserDepositListVCProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
//        reloadButtonView.isHidden = !show
        warningMessageView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        collectionView.isHidden = show
        collectionView.reloadData()
    }
}
extension UserDepositListVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.viewModel.response?.deposits?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserDepositListCell", for: indexPath) as! UserDepositListCell
        let item = presenter.viewModel.response?.deposits?[indexPath.row]
        cell.setup(item)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = presenter.viewModel.response?.deposits?[indexPath.row]
        presenter.openDetails(item)
    }
    

}
