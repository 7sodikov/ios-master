//
//  UserDepositListViewInstaller.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol UserDepositListViewInstaller: ViewInstaller {
    var bgView : UIImageView! {get set}
    var collectionView : UICollectionView! {get set}
    var warningMessageView: NoItemsAvailableView! { get set }
    var reloadButtonView : ReloadButtonView! {get set}
}

extension UserDepositListViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let padding : CGFloat = 15.0
        let width = UIScreen.main.bounds.size.width - 2 * padding
        layout.itemSize = CGSize(width: width, height: 100)
        layout.minimumLineSpacing = 15
//        layout.sectionInset = .zero

        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.register(UserDepositListCell.self, forCellWithReuseIdentifier: "UserDepositListCell")
        
        warningMessageView = NoItemsAvailableView.setupText(title: RS.lbl_no_deposit.localized())
        warningMessageView.isHidden = true
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        reloadButtonView.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(collectionView)
        mainView.addSubview(warningMessageView)
        mainView.addSubview(reloadButtonView)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(85)
        }
        
        warningMessageView.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(214))
            make.centerX.equalToSuperview()
        }
        
        reloadButtonView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
        }
    }
}

fileprivate struct Size {
    
}
