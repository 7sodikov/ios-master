//
//  UserDepositListInteractor.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol UserDepositListInteractorProtocol: class {
    func getList()
}

class UserDepositListInteractor: UserDepositListInteractorProtocol {
    weak var presenter: UserDepositListInteractorToPresenterProtocol!
    func getList() {
        NetworkService.Deposit.depositList(completion: { (result) in
            self.presenter.resultDepositeList(result)
        })
    }
}
