//
//  UserDepositListPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol UserDepositListPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: UserDepositListVM { get }
    func getList()
    func openDetails(_ deposite:DepositResponse?)
}

protocol UserDepositListInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func resultDepositeList(_ response: ResponseResult<DepositsResponse, AppError>)
    
}

class UserDepositListPresenter: UserDepositListPresenterProtocol {
    weak var view: UserDepositListVCProtocol!
    var interactor: UserDepositListInteractorProtocol!
    var router: UserDepositListRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = UserDepositListVM()
    
    // Private property and methods
    func getList() {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.getList()
    }
    func openDetails(_ deposite: DepositResponse?) {
        router.openDetails(view as Any, deposite)
    }
    
}

extension UserDepositListPresenter: UserDepositListInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func resultDepositeList(_ response: ResponseResult<DepositsResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.response = result.data
            if result.data.count == 0 {
                view.showReloadButtonForResponses(true, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            }
        case let .failure(error):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
            break
        }
    }
}
