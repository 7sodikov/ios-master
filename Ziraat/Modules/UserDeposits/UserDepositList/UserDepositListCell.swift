//// Header

import UIKit

class UserDepositListCell: UICollectionViewCell {

    let  containerView = UIView()
    let  nameLabel  = UILabel(frame: .zero)
    let  numberView = HorizontalLabelsView(frame: .zero)
    let  openView = HorizontalLabelsView(frame: .zero)
    let  availableView = HorizontalLabelsView(frame: .zero)
    //lbl_available_amount
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(containerView)
        containerView.layer.cornerRadius = 6
        containerView.backgroundColor = .white
        containerView.clipsToBounds = true
        containerView.snp.makeConstraints { (make) in
            make.left.right.bottom.top.equalToSuperview()
        }
        
        containerView.addSubview(nameLabel)
        nameLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(15))
        nameLabel.numberOfLines = 2
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(10)
            make.height.greaterThanOrEqualTo(20)
        }
        
        let boldFont =  EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))
        containerView.addSubview(numberView)
        numberView.leftLabel.text = RS.lbl_iban.localized()
        numberView.leftLabel.font = boldFont
        numberView.changeAligment(true)
        
        containerView.addSubview(openView)
        openView.leftLabel.text = RS.lbl_open_date.localized()
        openView.leftLabel.font = boldFont
        openView.changeAligment(true)
        
        containerView.addSubview(availableView)
        availableView.leftLabel.text = RS.lbl_available_amount.localized()
        availableView.leftLabel.font = boldFont
        
        availableView.changeAligment(true)
        
        numberView.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.top.equalTo(nameLabel.snp.bottom).offset(5)
            make.right.equalTo(-35)
        }
        
        openView.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.right.equalTo(-35)
            make.top.equalTo(numberView.snp.bottom).offset(5)
        }

        availableView.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel.snp.left)
            make.right.equalTo(-35)
            make.top.equalTo(openView.snp.bottom).offset(5)
            make.bottom.equalTo(-10)
        }
        
        
        
    }
    func setup(_ deposit: DepositResponse?){
        nameLabel.text = deposit?.name
        numberView.rightLabel.text = deposit?.accounts.first?.number
        openView.rightLabel.text = deposit?.accounts.first?.dateOpen
        let balance = deposit?.accounts.first?.balance.currencyFormattedStr()
        let code = CurrencyType.type(for: deposit?.currency)?.data.label
        availableView.rightLabel.text = (balance ?? "") + " " + (code ?? "")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
