//
//  UserDepositDetailsPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol UserDepositDetailsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: UserDepositDetailsVM { get }
}

protocol UserDepositDetailsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class UserDepositDetailsPresenter: UserDepositDetailsPresenterProtocol {
    weak var view: UserDepositDetailsVCProtocol!
    var interactor: UserDepositDetailsInteractorProtocol!
    var router: UserDepositDetailsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = UserDepositDetailsVM()
    
    // Private property and methods
    
}

extension UserDepositDetailsPresenter: UserDepositDetailsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
