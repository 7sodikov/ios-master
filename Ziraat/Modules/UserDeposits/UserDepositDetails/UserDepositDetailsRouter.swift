//
//  UserDepositDetailsRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol UserDepositDetailsRouterProtocol: class {
    static func createModule(_ deposite: DepositResponse?) -> UIViewController
}

class UserDepositDetailsRouter: UserDepositDetailsRouterProtocol {
    static func createModule(_ deposite: DepositResponse?) -> UIViewController{
        let vc = UserDepositDetailsVC()
        let presenter = UserDepositDetailsPresenter()
        let interactor = UserDepositDetailsInteractor()
        let router = UserDepositDetailsRouter()
        
        vc.presenter = presenter
        vc.presenter.viewModel.deposite = deposite
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    
}
