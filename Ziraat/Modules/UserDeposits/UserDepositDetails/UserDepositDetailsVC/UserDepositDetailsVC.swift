//
//  UserDepositDetailsVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol UserDepositDetailsVCProtocol: AnyObject {
    
}

class UserDepositDetailsVC: BaseViewController, UserDepositDetailsViewInstaller {
    var bgView: UIImageView!
    
    var infoLabel: UILabel!
    
    var accountLabel: UILabel!
    
    var holderLabel: UILabel!
    
    var detailsView: CardDetailsView!
    
    var mainView: UIView { view }
    var presenter: UserDepositDetailsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = RS.nav_ttl_deposit_details.localized()
        setupSubviews()
        infoLabel.text = presenter.viewModel.deposite?.accounts.first?.name
        accountLabel.text = presenter.viewModel.deposite?.accounts.first?.number
        holderLabel.text =  presenter.viewModel.deposite?.accounts.first?.customerName
        detailsView.userDeposite = presenter.viewModel.deposite
        
    }
}

extension UserDepositDetailsVC: UserDepositDetailsVCProtocol {
    
}
