//
//  UserDepositDetailsViewInstaller.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol UserDepositDetailsViewInstaller: ViewInstaller {
    var bgView: UIImageView! { get set }
    var infoLabel: UILabel! { get set }
    var accountLabel: UILabel! { get set }
    var holderLabel: UILabel! { get set }
    var detailsView : CardDetailsView! {get set}
}

extension UserDepositDetailsViewInstaller {
    func initSubviews() {
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        detailsView = CardDetailsView(frame: .zero)
        
        infoLabel = UILabel()
        infoLabel.numberOfLines = 3
        infoLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(15))
        
        accountLabel = UILabel()
        accountLabel.numberOfLines = 3
        accountLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(15))
        accountLabel.textColor = ColorConstants.gray
        
        holderLabel = UILabel()
        holderLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(15))
        holderLabel.textColor = .black
        holderLabel.numberOfLines = 2
        
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(infoLabel)
        mainView.addSubview(accountLabel)
        mainView.addSubview(holderLabel)
        mainView.addSubview(detailsView)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.remakeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        infoLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(Adaptive.val(20))
            make.top.equalTo(Adaptive.val(80))
            make.right.equalTo(-Adaptive.val(-20))
        }
        accountLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(Adaptive.val(20))
            make.height.equalTo(25)
            make.top.equalTo(infoLabel.snp.bottom).offset(5)
            make.right.equalTo(-Adaptive.val(-20))
        }
        holderLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(Adaptive.val(20))
            make.top.equalTo(accountLabel.snp.bottom).offset(5)
            make.right.equalTo(-Adaptive.val(-20))
        }
        
        let left = Adaptive.val(20)
        detailsView.snp.remakeConstraints { (make) in
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.top.equalTo(holderLabel.snp.bottom).offset(10)
        }
        //let btnLeft = Adaptive.val(30)
        
       
    }
}

fileprivate struct Size {
    struct Button {
        static let big = Adaptive.val(45)
        static let small = Adaptive.val(45)
    }
}
