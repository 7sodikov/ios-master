//
//  LoanBankProductsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanBankProductsRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigatetoDetailsVC(in navigateCtrl: Any, loan: LoanListResponse?)
}

class LoanBankProductsRouter: LoanBankProductsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = LoanBankProductsVC()
        let presenter = LoanBankProductsPresenter()
        let interactor = LoanBankProductsInteractor()
        let router = LoanBankProductsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigatetoDetailsVC(in navigateCtrl: Any, loan: LoanListResponse?) {
        let viewCtrl = navigateCtrl as! UIViewController
        let detailsVC = DetailsRouter.createModule(loan: loan)
        viewCtrl.navigationController?.pushViewController(detailsVC, animated: true)
    }
}
