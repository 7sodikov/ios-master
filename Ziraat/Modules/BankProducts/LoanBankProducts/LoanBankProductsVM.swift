//
//  LoanBankProductsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class LoanBankProductsVM {
    var loanList: [LoanListResponse]?
    var mainArrayOfElements: [LoanBankProductVM] = []
    
    func addElements() {
        for loanItemList in loanList ?? [] {
            mainArrayOfElements.append(LoanBankProductVM(element: loanItemList))
        }
    }
}

class LoanBankProductVM {
    var loanElement: LoanListResponse
    
    init(element: LoanListResponse) {
        self.loanElement = element
    }
    
    var period: String? {
        return "\(loanElement.term ?? 0) \(loanElement.termUnit ?? "")"
    }
    
    var minAmount: String? {
        return "От \(formattedMoney(loanElement.minAmount) ?? "0") \(loanElement.currency ?? "")"
    }
    
    var maxAmount: String? {
        return "\(formattedMoney(loanElement.maxAmount) ?? "0") \(loanElement.currency ?? "")"
    }
    
    var firstRatePeriod: String? {
        let rate = loanElement.rates?[0]
        return "\(rate?.minTerm ?? 0) - \(rate?.maxTerm ?? 0) \(rate?.maxTermUnit ?? "")"
    }

    var secondRateInterest: String? {
        let rate = loanElement.rates?[1]
        return "\(rate?.rate ?? 0) %"
    }
    
    var thirdRatePeriod: String? {
        let rate = loanElement.rates?[2]
        return "\(rate?.minTerm ?? 0) - \(rate?.maxTerm ?? 0) \(rate?.maxTermUnit ?? "")"
    }
    
    var thirdRateInterest: String? {
        let rate = loanElement.rates?[2]
        return "\(rate?.rate ?? 0) %"
    }
    
    var initialFee: String? {
        return "\(loanElement.initialFee ?? 0) %"
    }
        
    private func formattedMoney(_ money: Double?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
}
