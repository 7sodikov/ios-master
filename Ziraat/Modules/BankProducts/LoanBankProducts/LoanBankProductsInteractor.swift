//
//  LoanBankProductsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanBankProductsInteractorProtocol: class {
    func loanList()
}

class LoanBankProductsInteractor: LoanBankProductsInteractorProtocol {
    weak var presenter: LoanBankProductsInteractorToPresenterProtocol!
    func loanList() {
        NetworkService.BankProducts.loanList() { (result) in
            self.presenter.didGetLoanList(with: result)
        }
    }
}
