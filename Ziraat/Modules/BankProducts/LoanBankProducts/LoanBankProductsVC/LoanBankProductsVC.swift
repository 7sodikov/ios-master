//
//  LoanBankProductsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanBankProductsVCProtocol: class {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class LoanBankProductsVC: BaseViewController, LoanBankProductsViewInstaller {
    var mainView: UIView { view }
    var backImageView: UIImageView!
    var tableView: AutomaticHeightTableView!
    var reloadButtonView: ReloadButtonView!
    
    var presenter: LoanBankProductsPresenterProtocol!
    
    fileprivate let cellid = "\(LoanBankProductsTableViewCell.self)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()

        setupSubviews()
        setupNavigation()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func setupNavigation() {
        self.title = RS.lbl_loans.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LoanBankProductsVC: LoanBankProductsVCProtocol, UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.loanList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! LoanBankProductsTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        presenter.viewModel.addElements()
        let loanElementVM = presenter.viewModel.mainArrayOfElements[indexPath.row]
        cell.tapCallback = { [self] in
            presenter.moreButtonClicked(index: indexPath.row)
        }
        return cell.setup(loanElementVM: loanElementVM)
    }
    
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
}
