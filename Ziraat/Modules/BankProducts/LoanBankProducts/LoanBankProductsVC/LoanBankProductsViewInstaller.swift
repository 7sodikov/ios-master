//
//  LoanBankProductsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanBankProductsViewInstaller: ViewInstaller {
    var backImageView: UIImageView! { get set }
    var tableView: AutomaticHeightTableView! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
}

extension LoanBankProductsViewInstaller {
    func initSubviews() {
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_dash_light_background")
        
        tableView = AutomaticHeightTableView()
        tableView.register(LoanBankProductsTableViewCell.self, forCellReuseIdentifier: String(describing: LoanBankProductsTableViewCell.self))
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        tableView.sizeToFit()
        tableView.showsVerticalScrollIndicator = false
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        reloadButtonView.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(tableView)
        mainView.addSubview(reloadButtonView)
    }
    
    func addSubviewsConstraints() {
        backImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.bottom.equalTo(backImageView)
        }
        
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(22)
}
