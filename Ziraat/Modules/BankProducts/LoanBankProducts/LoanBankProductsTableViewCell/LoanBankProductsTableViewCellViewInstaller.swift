//
//  LoanBankProductsTableViewCellViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanBankProductsTableViewCellViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var backImageView: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
    var amountImageView: UIImageView! { get set }
    var maxAmountLabel: UILabel! { get set }
    var separatorLineOneView: UIView! { get set }
    var periodImageView: UIImageView! { get set }
    var periodLabel: UILabel! { get set }
    var separatorLineTwoView: UIView! { get set }
    var interestImageView: UIImageView! { get set }
    var termView: EZHorizontalControl! { get set }
    var separatorLineThreeView: UIView! { get set }
    var firstPaymentImageView: UIImageView! { get set }
    var firstPaymentLabel: UILabel! { get set }
    var firstPaymentInterestLabel: UILabel! { get set }
    var moreButton: UIButton! { get set }
}

extension LoanBankProductsTableViewCellViewInstaller {
    func initSubviews() {
        
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(10)
        
        backImageView = UIImageView()
        backImageView.clipsToBounds = true
        backImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        backImageView.layer.cornerRadius = Adaptive.val(10)
                
        titleLabel = UILabel()
        titleLabel.textColor = .black
        titleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(20))
        
        amountImageView = UIImageView()
        amountImageView.image = UIImage(named: "img_lp_amount")
        
        maxAmountLabel = UILabel()
        maxAmountLabel.textColor = .black
        maxAmountLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        
        separatorLineOneView = UIView()
        separatorLineOneView.backgroundColor = .lightGray
        
        periodImageView = UIImageView()
        periodImageView.image = UIImage(named: "img_lp_period")
        
        periodLabel = UILabel()
        periodLabel.textColor = .black
        periodLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))

        separatorLineTwoView = UIView()
        separatorLineTwoView.backgroundColor = .lightGray
        
        interestImageView = UIImageView()
        interestImageView.image = UIImage(named: "img_lp_interest")
        
        termView = EZHorizontalControl().setControl()
        
        separatorLineThreeView = UIView()
        separatorLineThreeView.backgroundColor = .lightGray
        
        firstPaymentImageView = UIImageView()
        firstPaymentImageView.image = UIImage(named: "img_lp_payment")
        
        firstPaymentLabel = UILabel()
        firstPaymentLabel.textColor = .black
        firstPaymentLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        firstPaymentLabel.text = RS.lbl_initial_fee.localized()
        
        firstPaymentInterestLabel = UILabel()
        firstPaymentInterestLabel.textColor = .black
        firstPaymentInterestLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        firstPaymentInterestLabel.text = "21 %"
        
        moreButton = UIButton()
        func attributedStr(with color: UIColor) -> NSAttributedString {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))!,
                .foregroundColor: color]
            return NSMutableAttributedString(string: RS.btn_detail.localized(), attributes: attributes)
        }
        let normalAttributeString = attributedStr(with: ColorConstants.mainRed)
        let highlightedAttributeString = attributedStr(with: ColorConstants.mainRed.withAlphaComponent(0.5))
        moreButton.setAttributedTitle(normalAttributeString, for: .normal)
        moreButton.setAttributedTitle(highlightedAttributeString, for: .highlighted)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(backImageView)
        mainView.addSubview(titleLabel)
        mainView.addSubview(amountImageView)
        mainView.addSubview(maxAmountLabel)
        mainView.addSubview(separatorLineOneView)
        mainView.addSubview(periodImageView)
        mainView.addSubview(periodLabel)
        mainView.addSubview(separatorLineTwoView)
        mainView.addSubview(interestImageView)
        mainView.addSubview(termView)
        mainView.addSubview(separatorLineThreeView)
        mainView.addSubview(firstPaymentImageView)
        mainView.addSubview(firstPaymentLabel)
        mainView.addSubview(firstPaymentInterestLabel)
        mainView.addSubview(moreButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(5.5))
            maker.bottom.equalTo(-Adaptive.val(5.5))
            maker.leading.equalTo(0)
            maker.trailing.equalTo(0)
        }
        
        backImageView.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalTo(backView)
            maker.height.equalTo(Adaptive.val(200))
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(backImageView.snp.bottom).offset(Adaptive.val(14))
        }
        
        amountImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(20))
            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(23))
            maker.leading.equalTo(Adaptive.val(21))
        }
        
        maxAmountLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(amountImageView.snp.trailing).offset(Adaptive.val(13))
            maker.centerY.equalTo(amountImageView)
        }
        
        separatorLineOneView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(1))
            maker.leading.equalTo(Adaptive.val(20))
            maker.trailing.equalTo(-Adaptive.val(20))
            maker.top.equalTo(maxAmountLabel.snp.bottom).offset(Adaptive.val(13))
        }
        
        periodImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(20))
            maker.top.equalTo(separatorLineOneView.snp.bottom).offset(Adaptive.val(14))
            maker.leading.equalTo(Adaptive.val(21))
        }
        
        periodLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(periodImageView)
            maker.leading.equalTo(periodImageView.snp.trailing).offset(Adaptive.val(13))
        }
        
        separatorLineTwoView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(1))
            maker.leading.equalTo(Adaptive.val(20))
            maker.trailing.equalTo(-Adaptive.val(20))
            maker.top.equalTo(periodImageView.snp.bottom).offset(Adaptive.val(13))
        }
        
        interestImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(20))
            maker.top.equalTo(separatorLineTwoView.snp.bottom).offset(Adaptive.val(14))
            maker.leading.equalTo(Adaptive.val(21))
        }
        
        termView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(separatorLineTwoView.snp.bottom).offset(Adaptive.val(14))
            maker.leading.equalTo(Adaptive.val(13))
            maker.trailing.equalTo(-Adaptive.val(21))
        }
        
        separatorLineThreeView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(1))
            maker.leading.equalTo(Adaptive.val(20))
            maker.trailing.equalTo(-Adaptive.val(20))
            maker.top.equalTo(termView.snp.bottom).offset(Adaptive.val(18))
        }
        
        firstPaymentImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(20))
            maker.top.equalTo(separatorLineThreeView.snp.bottom).offset(Adaptive.val(18))
            maker.leading.equalTo(Adaptive.val(21))
        }
        
        firstPaymentLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(firstPaymentImageView)
            maker.leading.equalTo(firstPaymentImageView.snp.trailing).offset(Adaptive.val(13))
        }
        
        firstPaymentInterestLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(firstPaymentLabel)
            maker.trailing.equalTo(-Adaptive.val(21))
        }
        
        moreButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(firstPaymentInterestLabel.snp.bottom).offset(Adaptive.val(18))
            maker.trailing.equalTo(-Adaptive.val(20))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(20))
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(16)
}
