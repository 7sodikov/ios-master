//
//  LoanBankProductsTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import Kingfisher

class LoanBankProductsTableViewCell: UITableViewCell, LoanBankProductsTableViewCellViewInstaller {
    var mainView: UIView { contentView }
    var backView: UIView!
    var backImageView: UIImageView!
    var titleLabel: UILabel!
    var amountImageView: UIImageView!
    var maxAmountLabel: UILabel!
    var separatorLineOneView: UIView!
    var periodImageView: UIImageView!
    var periodLabel: UILabel!
    var separatorLineTwoView: UIView!
    var interestImageView: UIImageView!
    var termView: EZHorizontalControl!
    var separatorLineThreeView: UIView!
    var firstPaymentImageView: UIImageView!
    var firstPaymentLabel: UILabel!
    var firstPaymentInterestLabel: UILabel!
    var moreButton: UIButton!
    
    var tapCallback: (() -> Void)?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        
        moreButton.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc func didTap(_ sender: Any) {
        tapCallback?()
    }
    
    func setup(loanElementVM: LoanBankProductVM) -> LoanBankProductsTableViewCell {
        let loan = loanElementVM.loanElement
        backImageView.kf.setImage(with: URL(string: loan.backgroundImgUrl ?? ""), placeholder: nil)
        backImageView.contentMode = .scaleAspectFill
        titleLabel.text = loan.name
        maxAmountLabel.text = loanElementVM.maxAmount
        periodLabel.text = loanElementVM.period
        firstPaymentInterestLabel.text = loanElementVM.initialFee
        termView.stackView.arrangedSubviews.forEach { termView.stackView.removeArrangedSubview($0);$0.removeFromSuperview() }
        for (index, item) in loan.rates!.enumerated() {
            if index == 0 {
                termView.addSegment(with: "\(item.minTerm ?? 0) - \(item.maxTerm ?? 0) \(item.maxTermUnit ?? "")", interest: "\(item.rate ?? 0) %")
            } else if index == 1 {
                termView.addSegment(with: "\(item.minTerm ?? 0) - \(item.maxTerm ?? 0) \(item.maxTermUnit ?? "")", interest: "\(item.rate ?? 0) %")
            } else if index == 2 {
                termView.addSegment(with: "\(item.minTerm ?? 0) - \(item.maxTerm ?? 0) \(item.maxTermUnit ?? "")", interest: "\(item.rate ?? 0) %")
            } else {
                termView.addSegment(with: "\(item.minTerm ?? 0) - \(item.maxTerm ?? 0) \(item.maxTermUnit ?? "")", interest: "\(item.rate ?? 0) %")
            }
        }
        return self
    }
}
