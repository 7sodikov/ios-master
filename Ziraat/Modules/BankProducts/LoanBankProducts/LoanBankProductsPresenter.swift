//
//  LoanBankProductsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanBankProductsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoanBankProductsVM { get }
    func viewDidLoad()
    func moreButtonClicked(index: Int)
}

protocol LoanBankProductsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetLoanList(with response: ResponseResult<[LoanListResponse], AppError>)
}

class LoanBankProductsPresenter: LoanBankProductsPresenterProtocol {
    weak var view: LoanBankProductsVCProtocol!
    var interactor: LoanBankProductsInteractorProtocol!
    var router: LoanBankProductsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoanBankProductsVM()
    
    func viewDidLoad() {
        interactor.loanList()
    }
    
    func moreButtonClicked(index: Int) {
        let loan = viewModel.loanList?[index]
        router.navigatetoDetailsVC(in: view as Any, loan: loan)
    }
    
    // Private property and methods
    
}

extension LoanBankProductsPresenter: LoanBankProductsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetLoanList(with response: ResponseResult<[LoanListResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.loanList = result.data
            view.showReloadButtonForResponses(false, animateReloadButton: false)
        case let .failure(error):
            break
        }
    }
}
