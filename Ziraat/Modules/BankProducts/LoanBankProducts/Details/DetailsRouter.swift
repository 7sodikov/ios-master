//
//  DetailsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DetailsRouterProtocol: class {
    static func createModule(loan: LoanListResponse?) -> UIViewController
}

class DetailsRouter: DetailsRouterProtocol {
    static func createModule(loan: LoanListResponse?) -> UIViewController {
        let vc = DetailsVC()
        let presenter = DetailsPresenter()
        let interactor = DetailsInteractor()
        let router = DetailsRouter()
        
        vc.presenter = presenter
        vc.loanDetails = loan
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
