//
//  DetailsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DetailsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: DetailsVM { get }
}

protocol DetailsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class DetailsPresenter: DetailsPresenterProtocol {
    weak var view: DetailsVCProtocol!
    var interactor: DetailsInteractorProtocol!
    var router: DetailsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = DetailsVM()
    
    // Private property and methods
    
}

extension DetailsPresenter: DetailsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
