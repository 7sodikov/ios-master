//
//  DetailsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DetailsInteractorProtocol: class {
    
}

class DetailsInteractor: DetailsInteractorProtocol {
    weak var presenter: DetailsInteractorToPresenterProtocol!
    
}
