//
//  DetailsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DetailsVCProtocol: class {
    
}

class DetailsVC: BaseViewController, DetailsViewInstaller {
    var mainView: UIView { view }
    var backImageView: UIImageView!
    var scrollView: UIScrollView!
    var contentView: UIView!
    var goalTitleLabel: UILabel!
    var goalBodyLabel: UILabel!
    var documentsTitleLabel: UILabel!
    var documentsBodyLabel: UILabel!
    var additionalInfoTitleLabel: UILabel!
    var additionalInfoBodyLabel: UILabel!
    
    var presenter: DetailsPresenterProtocol!
    
    var loanDetails: LoanListResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        setup()
    }
    
    func setup() {
        goalBodyLabel.text = loanDetails?.goal
        documentsBodyLabel.text = loanDetails?.documents
        additionalInfoBodyLabel.text = loanDetails?.additionalInfo
    }
    
    func setupNavigation() {
        self.title = RS.btn_detail.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetailsVC: DetailsVCProtocol {
    
}
