//
//  DepositBankProductsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DepositBankProductsRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigatetoDetailsVC(in navigateCtrl: Any, deposit: DepositsListResponse?)
}

class DepositBankProductsRouter: DepositBankProductsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = DepositBankProductsVC()
        let presenter = DepositBankProductsPresenter()
        let interactor = DepositBankProductsInteractor()
        let router = DepositBankProductsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigatetoDetailsVC(in navigateCtrl: Any, deposit: DepositsListResponse?) {
        let viewCtrl = navigateCtrl as! UIViewController
        let detailsVC = DetailsDepositRouter.createModule(deposit: deposit)
        viewCtrl.navigationController?.pushViewController(detailsVC, animated: true)
    }
}
