//
//  DepositBankProductsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DepositBankProductsVCProtocol: class {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class DepositBankProductsVC: BaseViewController, DepositBankProductsViewInstaller {
    var mainView: UIView { view }
    var backImageView: UIImageView!
    var tableView: AutomaticHeightTableView!
    var reloadButtonView: ReloadButtonView!
    
    fileprivate let cellid = "\(DepositBankProductsTableViewCell.self)"
    
    var presenter: DepositBankProductsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func setupNavigation() {
        self.title = RS.lbl_deposits.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension DepositBankProductsVC: DepositBankProductsVCProtocol, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.depositList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! DepositBankProductsTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        presenter.viewModel.addElements()
        let depositElementVM = presenter.viewModel.mainArrayOfElements[indexPath.row]
        cell.tapCallback = { [self] in
            presenter.moreButtonClicked(index: indexPath.row)
        }
        return cell.setup(depositElementVM: depositElementVM)
    }
    
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
}
