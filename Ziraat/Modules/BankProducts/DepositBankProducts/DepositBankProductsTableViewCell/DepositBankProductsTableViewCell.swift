//
//  DepositBankProductsTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class DepositBankProductsTableViewCell: UITableViewCell, DepositBankProductsTableViewCellViewInstaller {
    var backView: UIView!
    var backImageView: UIImageView!
    var titleLabel: UILabel!
    var maxAmountLabel: UILabel!
    var periodImageView: UIImageView!
    var periodLabel: UILabel!
    var separatorLineTwoView: UIView!
    var interestImageView: UIImageView!
    var rateTitleLabel: UILabel!
    var rateLabel: UILabel!
    var moreButton: UIButton!
    var mainView: UIView { contentView }
    
    var tapCallback: (() -> Void)?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        
        moreButton.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc func didTap(_ sender: Any) {
        tapCallback?()
    }
    
    func setup(depositElementVM: DepositBankProductVM) -> DepositBankProductsTableViewCell {
        let deposit = depositElementVM.depositElement
        backImageView.kf.setImage(with: URL(string: deposit.backgroundImgUrl ?? "" ), placeholder: nil)
        titleLabel.text = deposit.name
        maxAmountLabel.text = depositElementVM.minAmount
        periodLabel.text = depositElementVM.period
        rateLabel.text = deposit.rate
        return self
    }
}
