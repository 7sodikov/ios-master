//
//  DepositBankProductsTableViewCellViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DepositBankProductsTableViewCellViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var backImageView: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
    var maxAmountLabel: UILabel! { get set }
    var periodImageView: UIImageView! { get set }
    var periodLabel: UILabel! { get set }
    var separatorLineTwoView: UIView! { get set }
    var interestImageView: UIImageView! { get set }
    var rateTitleLabel: UILabel! { get set }
    var rateLabel: UILabel! { get set }
    var moreButton: UIButton! { get set }
}

extension DepositBankProductsTableViewCellViewInstaller {
    func initSubviews() {
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(10)
        
        backImageView = UIImageView()
        backImageView.clipsToBounds = true
        backImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        backImageView.layer.cornerRadius = Adaptive.val(10)
                
        titleLabel = UILabel()
        titleLabel.textColor = .black
        titleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(20))
        
        maxAmountLabel = UILabel()
        maxAmountLabel.textColor = .black
        maxAmountLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        maxAmountLabel.numberOfLines = 0
        
        periodImageView = UIImageView()
        periodImageView.image = UIImage(named: "img_lp_period")
        
        periodLabel = UILabel()
        periodLabel.textColor = .black
        periodLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))

        separatorLineTwoView = UIView()
        separatorLineTwoView.backgroundColor = .lightGray
        
        interestImageView = UIImageView()
        interestImageView.image = UIImage(named: "img_lp_interest")
        
        rateTitleLabel = UILabel()
        rateTitleLabel.textColor = .black
        rateTitleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        rateTitleLabel.text = RS.lbl_interest_rate_dep.localized()
        
        rateLabel = UILabel()
        rateLabel.textColor = .black
        rateLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        
        moreButton = UIButton()
        func attributedStr(with color: UIColor) -> NSAttributedString {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))!,
                .foregroundColor: color]
            return NSMutableAttributedString(string: RS.btn_detail.localized(), attributes: attributes)
        }
        let normalAttributeString = attributedStr(with: ColorConstants.mainRed)
        let highlightedAttributeString = attributedStr(with: ColorConstants.mainRed.withAlphaComponent(0.5))
        moreButton.setAttributedTitle(normalAttributeString, for: .normal)
        moreButton.setAttributedTitle(highlightedAttributeString, for: .highlighted)

    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(backImageView)
        mainView.addSubview(titleLabel)
        mainView.addSubview(maxAmountLabel)
        mainView.addSubview(periodImageView)
        mainView.addSubview(periodLabel)
        mainView.addSubview(separatorLineTwoView)
        mainView.addSubview(interestImageView)
        mainView.addSubview(rateTitleLabel)
        mainView.addSubview(rateLabel)
        mainView.addSubview(moreButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(5.5))
            maker.bottom.equalTo(-Adaptive.val(5.5))
            maker.leading.equalTo(0)
            maker.trailing.equalTo(0)
        }
        
        backImageView.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalTo(backView)
            maker.height.equalTo(Adaptive.val(200))
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(backImageView.snp.bottom).offset(Adaptive.val(14))
        }
        
        maxAmountLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(24))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(20))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(20))
        }
        
        periodImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(20))
            maker.top.equalTo(maxAmountLabel.snp.bottom).offset(Adaptive.val(14))
            maker.leading.equalTo(Adaptive.val(21))
        }
        
        periodLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(periodImageView)
            maker.leading.equalTo(periodImageView.snp.trailing).offset(Adaptive.val(13))
        }
        
        separatorLineTwoView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(1))
            maker.leading.equalTo(Adaptive.val(20))
            maker.trailing.equalTo(-Adaptive.val(20))
            maker.top.equalTo(periodImageView.snp.bottom).offset(Adaptive.val(13))
        }
        
        interestImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(20))
            maker.top.equalTo(separatorLineTwoView.snp.bottom).offset(Adaptive.val(14))
            maker.leading.equalTo(Adaptive.val(21))
        }
        
        rateTitleLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(interestImageView)
            maker.leading.equalTo(interestImageView.snp.trailing).offset(Adaptive.val(13))
        }
        
        rateLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(rateTitleLabel.snp.top)
            maker.trailing.equalToSuperview().offset(-Adaptive.val(20))
        }
    
        moreButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(interestImageView.snp.bottom).offset(Adaptive.val(30))
            maker.trailing.equalTo(-Adaptive.val(20))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(20))
        }

    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(16)
}
