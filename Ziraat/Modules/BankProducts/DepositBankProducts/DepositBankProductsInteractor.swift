//
//  DepositBankProductsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DepositBankProductsInteractorProtocol: class {
    func depositList()
}

class DepositBankProductsInteractor: DepositBankProductsInteractorProtocol {
    weak var presenter: DepositBankProductsInteractorToPresenterProtocol!
    func depositList() {
        NetworkService.BankProducts.depositList() { (result) in
            self.presenter.didGetDepositList(with: result)
        }
    }
}
