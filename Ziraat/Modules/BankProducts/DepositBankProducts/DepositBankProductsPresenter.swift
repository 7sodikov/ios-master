//
//  DepositBankProductsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DepositBankProductsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: DepositBankProductsVM { get }
    func viewDidLoad()
    func moreButtonClicked(index: Int)
}

protocol DepositBankProductsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetDepositList(with response: ResponseResult<[DepositsListResponse], AppError>)
}

class DepositBankProductsPresenter: DepositBankProductsPresenterProtocol {
    weak var view: DepositBankProductsVCProtocol!
    var interactor: DepositBankProductsInteractorProtocol!
    var router: DepositBankProductsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = DepositBankProductsVM()
    
    func viewDidLoad() {
        interactor.depositList()
    }
    
    func moreButtonClicked(index: Int) {
        let depositDetails = viewModel.depositList?[index]
        router.navigatetoDetailsVC(in: view as Any, deposit: depositDetails)
    }
    
    // Private property and methods
    
}

extension DepositBankProductsPresenter: DepositBankProductsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetDepositList(with response: ResponseResult<[DepositsListResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.depositList = result.data
            if result.data.count == 0 {
                view.showReloadButtonForResponses(true, animateReloadButton: true)
            } else {
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            }
        case let .failure(error):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
        }
    }
}
