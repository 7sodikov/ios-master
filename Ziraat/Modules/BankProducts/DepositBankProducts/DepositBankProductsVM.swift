//
//  DepositBankProductsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class DepositBankProductsVM {
    var depositList: [DepositsListResponse]?
    var mainArrayOfElements: [DepositBankProductVM] = []
    
    func addElements() {
        for depositItemList in depositList ?? [] {
            mainArrayOfElements.append(DepositBankProductVM(element: depositItemList))
        }
    }
}

class DepositBankProductVM {
    var depositElement: DepositsListResponse
    
    init(element: DepositsListResponse) {
        self.depositElement = element
    }
    
    var period: String? {
        return "\(depositElement.term ?? 0) \(depositElement.termUnit ?? "")"
    }
    
    var minAmount: String? {
        return "\(RS.lbl_min_amount_deposit.localized()) \(formattedMoney(depositElement.minAmount) ?? "0") \(depositElement.currency ?? "")"
    }

//    var maxAmount: String? {
//        return "До \(formattedMoney(loanElement.maxAmount) ?? "0") \(loanElement.currency ?? "")"
//    }
//
//    var firstRatePeriod: String? {
//        let rate = loanElement.rates?[0]
//        return "\(rate?.minTerm ?? 0) - \(rate?.maxTerm ?? 0) \(rate?.maxTermUnit ?? "")"
//    }
//
//    var secondRateInterest: String? {
//        let rate = loanElement.rates?[1]
//        return "\(rate?.rate ?? 0) %"
//    }
//
//    var thirdRatePeriod: String? {
//        let rate = loanElement.rates?[2]
//        return "\(rate?.minTerm ?? 0) - \(rate?.maxTerm ?? 0) \(rate?.maxTermUnit ?? "")"
//    }
//
//    var thirdRateInterest: String? {
//        let rate = loanElement.rates?[2]
//        return "\(rate?.rate ?? 0) %"
//    }
//
//    var initialFee: String? {
//        return "\(loanElement.initialFee ?? 0) %"
//    }
        
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
}
