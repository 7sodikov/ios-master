//
//  DetailsDepositPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DetailsDepositPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: DetailsDepositVM { get }
}

protocol DetailsDepositInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class DetailsDepositPresenter: DetailsDepositPresenterProtocol {
    weak var view: DetailsDepositVCProtocol!
    var interactor: DetailsDepositInteractorProtocol!
    var router: DetailsDepositRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = DetailsDepositVM()
    
    // Private property and methods
    
}

extension DetailsDepositPresenter: DetailsDepositInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
