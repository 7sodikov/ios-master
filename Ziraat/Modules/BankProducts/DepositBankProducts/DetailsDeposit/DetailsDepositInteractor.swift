//
//  DetailsDepositInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol DetailsDepositInteractorProtocol: class {
    
}

class DetailsDepositInteractor: DetailsDepositInteractorProtocol {
    weak var presenter: DetailsDepositInteractorToPresenterProtocol!
    
}
