//
//  DetailsDepositViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DetailsDepositViewInstaller: ViewInstaller {
    var backImageView: UIImageView! { get set }
    var scrollView: UIScrollView! { get set }
    var contentView: UIView! { get set }
    var documentsTitleLabel: UILabel! { get set }
    var documentsBodyLabel: UILabel! { get set }
    var additionalInfoTitleLabel: UILabel! { get set }
    var additionalInfoBodyLabel: UILabel! { get set }
}

extension DetailsDepositViewInstaller {
    func initSubviews() {
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_dash_light_background")
        
        scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        
        contentView = UIView()
        
        documentsTitleLabel = UILabel()
        documentsTitleLabel.textColor = .black
        documentsTitleLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        documentsTitleLabel.text = RS.lbl_documents.localized()
        
        documentsBodyLabel = UILabel()
        documentsBodyLabel.textColor = .black
        documentsBodyLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        documentsBodyLabel.numberOfLines = 0
        
        additionalInfoTitleLabel = UILabel()
        additionalInfoTitleLabel.textColor = .black
        additionalInfoTitleLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        additionalInfoTitleLabel.text = RS.lbl_additional_info.localized()
        
        additionalInfoBodyLabel = UILabel()
        additionalInfoBodyLabel.textColor = .black
        additionalInfoBodyLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        additionalInfoBodyLabel.numberOfLines = 0
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(scrollView)
        scrollView.addSubview(contentView)
        scrollView.addSubview(documentsTitleLabel)
        scrollView.addSubview(documentsBodyLabel)
        scrollView.addSubview(additionalInfoTitleLabel)
        scrollView.addSubview(additionalInfoBodyLabel)
    }
    
    func addSubviewsConstraints() {
        backImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        scrollView.snp.remakeConstraints { (maker) in
            maker.leading.bottom.trailing.equalTo(0)
            maker.top.equalToSuperview().offset(Adaptive.val(110))
        }
        
        contentView.snp.remakeConstraints { (maker) in
            maker.left.right.equalTo(self.mainView)
            maker.width.top.bottom.equalTo(self.scrollView)
        }
        
        documentsTitleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(scrollView)
            maker.leading.equalTo(contentView).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(contentView).offset(-Size.paddingLeftRight)
        }
        
        documentsBodyLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(documentsTitleLabel.snp.bottom).offset(Size.paddingTopBottom)
            maker.leading.equalTo(contentView).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(contentView).offset(-Size.paddingLeftRight)
        }
        
        additionalInfoTitleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(documentsBodyLabel.snp.bottom).offset(Size.paddingTopBottom)
            maker.leading.equalTo(contentView).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(contentView).offset(-Size.paddingLeftRight)
        }
        
        additionalInfoBodyLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(additionalInfoTitleLabel.snp.bottom).offset(Size.paddingTopBottom)
            maker.leading.equalTo(contentView).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(contentView).offset(-Size.paddingLeftRight)
            maker.bottom.equalTo(contentView.snp.bottom)
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(20)
    static let paddingTopBottom = Adaptive.val(15)
}
