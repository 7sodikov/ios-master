//
//  DetailsDepositVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DetailsDepositVCProtocol: class {
    
}

class DetailsDepositVC: BaseViewController, DetailsDepositViewInstaller {
    var mainView: UIView { view }
    var backImageView: UIImageView!
    var scrollView: UIScrollView!
    var contentView: UIView!
    var documentsTitleLabel: UILabel!
    var documentsBodyLabel: UILabel!
    var additionalInfoTitleLabel: UILabel!
    var additionalInfoBodyLabel: UILabel!
    
    var presenter: DetailsDepositPresenterProtocol!
    
    var depositItem: DepositsListResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        setup()
    }
    
    func setup() {
        documentsBodyLabel.text = depositItem?.documents
        additionalInfoBodyLabel.text = depositItem?.additionalInfo
    }
    
    func setupNavigation() {
        self.title = RS.btn_detail.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetailsDepositVC: DetailsDepositVCProtocol {
    
}
