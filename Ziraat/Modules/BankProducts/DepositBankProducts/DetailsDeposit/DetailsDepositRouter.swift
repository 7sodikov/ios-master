//
//  DetailsDepositRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DetailsDepositRouterProtocol: class {
    static func createModule(deposit: DepositsListResponse?) -> UIViewController
}

class DetailsDepositRouter: DetailsDepositRouterProtocol {
    static func createModule(deposit: DepositsListResponse?) -> UIViewController {
        let vc = DetailsDepositVC()
        let presenter = DetailsDepositPresenter()
        let interactor = DetailsDepositInteractor()
        let router = DetailsDepositRouter()
        
        vc.presenter = presenter
        vc.depositItem = deposit
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
