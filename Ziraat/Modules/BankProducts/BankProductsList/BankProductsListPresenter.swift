//
//  BankProductsListPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BankProductsListPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: BankProductsListVM { get }
    func loanClicked()
    func depositClicked()
}

protocol BankProductsListInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class BankProductsListPresenter: BankProductsListPresenterProtocol {
    weak var view: BankProductsListVCProtocol!
    var interactor: BankProductsListInteractorProtocol!
    var router: BankProductsListRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = BankProductsListVM()
    
    // Private property and methods
    
    func loanClicked() {
        router.navigateToLoans(from: view as Any)
    }
    
    func depositClicked() {
        router.navigateToDeposits(from: view as Any)
    }
}

extension BankProductsListPresenter: BankProductsListInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
