//
//  BankProductsListViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BankProductsListViewInstaller: ViewInstaller {
    var backImageView: UIImageView! { get set }
    var loanProductView: BankProductElement! { get set }
    var depositProductView: BankProductElement! { get set }
}

extension BankProductsListViewInstaller {
    func initSubviews() {
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_dash_light_background")
        
        loanProductView = BankProductElement.systemView(title: RS.lbl_loans.localized(), image: UIImage(named: "btn_bp_loan")!)
        
        depositProductView = BankProductElement.systemView(title: RS.lbl_deposits.localized(), image: UIImage(named: "btn_bp_deposit")!)
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(loanProductView)
        mainView.addSubview(depositProductView)
    }
    
    func addSubviewsConstraints() {
        backImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        loanProductView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(127))
            maker.leading.equalToSuperview().offset(Adaptive.val(31))
        }
        
        depositProductView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(127))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(31))
        }
    }
}

fileprivate struct Size {
    
}
