//
//  BankProductsListVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BankProductsListVCProtocol: class {
    
}

class BankProductsListVC: BaseViewController, BankProductsListViewInstaller {
    var mainView: UIView { view }
    var backImageView: UIImageView!
    var loanProductView: BankProductElement!
    var depositProductView: BankProductElement!
    var presenter: BankProductsListPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        
        loanProductView.mainButton.addTarget(self, action: #selector(loanButtonClicked), for: .touchUpInside)
        depositProductView.mainButton.addTarget(self, action: #selector(depositButtonClicked), for: .touchUpInside)
    }
    
    func setupNavigation() {
        self.title = RS.lbl_bank_products.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc func loanButtonClicked() {
        presenter.loanClicked()
    }
    
    @objc func depositButtonClicked() {
        presenter.depositClicked()
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension BankProductsListVC: BankProductsListVCProtocol {
    
}
