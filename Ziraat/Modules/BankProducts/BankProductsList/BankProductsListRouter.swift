//
//  BankProductsListRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BankProductsListRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToLoans(from view: Any?)
    func navigateToDeposits(from view: Any?)
}

class BankProductsListRouter: BankProductsListRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = BankProductsListVC()
        let presenter = BankProductsListPresenter()
        let interactor = BankProductsListInteractor()
        let router = BankProductsListRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToLoans(from view: Any?) {
        let viewCtrl = view as? UIViewController
        let loanListView = LoanBankProductsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(loanListView, animated: true)
    }
    
    func navigateToDeposits(from view: Any?) {
        let viewCtrl = view as? UIViewController
        let depositListView = DepositBankProductsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(depositListView, animated: true)
    }
}
