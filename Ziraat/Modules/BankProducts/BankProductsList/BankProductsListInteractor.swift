//
//  BankProductsListInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BankProductsListInteractorProtocol: class {
    
}

class BankProductsListInteractor: BankProductsListInteractorProtocol {
    weak var presenter: BankProductsListInteractorToPresenterProtocol!
    
}
