//
//  AppUpdaterPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation


protocol AppUpdaterPresentationLogic {
    func viewDidLoad()
    
}

class AppUpdaterPresenter: AppUpdaterPresentationLogic {
    
    private unowned let view: AppUpdaterDisplayLogic
    private var interactor: AppUpdaterBusinessLogic
    private let router: AppUpdaterRouter
    var controller: AppUpdaterViewController { view as! AppUpdaterViewController }
    
    init(view: AppUpdaterDisplayLogic, interactor: AppUpdaterBusinessLogic, router: AppUpdaterRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        
        bind()
    }
    
    func bind() {
       
    }
    
    
}
