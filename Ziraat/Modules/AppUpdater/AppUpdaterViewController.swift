//
//  AppUpdaterViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol AppUpdaterDisplayLogic:  AppUpdaterViewInstaller {
  
}

class AppUpdaterViewController: BaseViewController, AppUpdaterDisplayLogic {
    
    var stackView: UIStackView!
    var backgroundImage: UIImageView!
    var textLabel: UILabel!
    var updateButton: UIButton!
    var closeButton: UIButton!
    var centerImage: UIImageView!
    var mainView: UIView { self.view }
    var presenter: AppUpdaterPresentationLogic!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        presenter.viewDidLoad()
        
        // http erroni code 426 ga tekshirish kerak. agar shu bo'lsa AppUpdaterViewController ni present qilish kerak
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @objc func refreshControlValueChanged() {
        
    }
}
