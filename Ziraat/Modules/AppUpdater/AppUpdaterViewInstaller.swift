//
//  AppUpdaterViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AppUpdaterViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var textLabel: UILabel! {get set}
    var updateButton: UIButton! {get set}
    var closeButton: UIButton! {get set}
    var centerImage: UIImageView! {get set}
    var stackView: UIStackView! {get set}
}

extension AppUpdaterViewInstaller {
    
    
    
    private func button(backColor: UIColor, titleColor: UIColor, title: String) -> UIButton {
        let button = UIButton()
        button.layer.masksToBounds = true
        button.setTitleColor(.white, for: .normal)
        button.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        button.layer.cornerRadius = Adaptive.val(20)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(20))
        button.setTitleColor(titleColor, for: .normal)
        
        return button
    }
    
    func initSubviews() {
        backgroundImage = UIImageView()
        backgroundImage.image = UIImage(named: "img_dash_light_background")
        
        centerImage = UIImageView()
        centerImage.image = UIImage(named: "img_ziraat_onay")// boshqa qo'yish kerak figmada yo'q ekan Davron aka tashab beradi
        
        textLabel = UILabel()
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.textAlignment = .center
        textLabel.text = "This version of Ziraat Mobile Uzbekistan is outdated and is no longer supported. Please update the app to the latest version."
        
        updateButton = button(backColor: ColorConstants.mainRed, titleColor: .white, title: "Update")
        closeButton = button(backColor: ColorConstants.grayishWhite, titleColor: .black, title: "Close")
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 8
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(centerImage)
        mainView.addSubview(textLabel)
        mainView.addSubview(stackView)
        
        stackView.addArrangedSubview(updateButton)
        stackView.addArrangedSubview(closeButton)
    }
    
    func addSubviewsConstraints() {
        
    }
    
}
