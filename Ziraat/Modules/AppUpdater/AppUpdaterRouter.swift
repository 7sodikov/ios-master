//
//  AppUpdaterRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation



protocol AppUpdaterRoutingLogic {
    
}

class AppUpdaterRouter: BaseRouter, AppUpdaterRoutingLogic {

    init() {
        let controller = AppUpdaterViewController()
        super.init(viewController: controller)
        
        let intractor = AppUpdaterInteractor()
        let presenter = AppUpdaterPresenter(view: controller, interactor: intractor, router: self)
        controller.presenter = presenter
    }
        
}
