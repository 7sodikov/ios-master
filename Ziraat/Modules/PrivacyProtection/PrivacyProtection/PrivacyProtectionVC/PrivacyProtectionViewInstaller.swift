//
//  PrivacyProtectionViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 2/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PrivacyProtectionViewInstaller: ViewInstaller {
    // declare your UI elements here
}

extension PrivacyProtectionViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
    }
}

fileprivate struct Size {
    
}
