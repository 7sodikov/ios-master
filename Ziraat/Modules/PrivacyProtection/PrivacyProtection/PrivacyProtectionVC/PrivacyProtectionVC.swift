//
//  PrivacyProtectionVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 2/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PrivacyProtectionVCProtocol: class {
    
}

class PrivacyProtectionVC: UIViewController, PrivacyProtectionViewInstaller {
    var mainView: UIView { view }
    var presenter: PrivacyProtectionPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
    }
    
    override func viewWillLayoutSubviews() {
           super.viewWillLayoutSubviews()

           // In my opinion, it looks a bit better to position the label centred in the top two thirds of the screen rather than in the middle
           let labelHeight = view.bounds.height * 0.66
           (privacyLabel.frame, _) = view.bounds.divided(atDistance: labelHeight, from: .minYEdge)
       }

       private lazy var privacyLabel: UILabel = {
           let label = UILabel()

           label.font = UIFont.preferredFont(forTextStyle: .title1)
           label.adjustsFontForContentSizeCategory = true
           label.adjustsFontSizeToFitWidth = true

           label.textAlignment = .center
           label.textColor = .gray

           label.numberOfLines = 0
           label.text = "Content hidden\nto protect\nyour privacy"

           self.view.addSubview(label)

           return label
       }()
}

extension PrivacyProtectionVC: PrivacyProtectionVCProtocol {
    
}
