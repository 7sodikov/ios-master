//
//  PrivacyProtectionPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 2/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PrivacyProtectionPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: PrivacyProtectionVM { get }
}

protocol PrivacyProtectionInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class PrivacyProtectionPresenter: PrivacyProtectionPresenterProtocol {
    weak var view: PrivacyProtectionVCProtocol!
    var interactor: PrivacyProtectionInteractorProtocol!
    var router: PrivacyProtectionRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PrivacyProtectionVM()
    
    // Private property and methods
    
}

extension PrivacyProtectionPresenter: PrivacyProtectionInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
