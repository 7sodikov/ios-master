//
//  PrivacyProtectionInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 2/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PrivacyProtectionInteractorProtocol: class {
    
}

class PrivacyProtectionInteractor: PrivacyProtectionInteractorProtocol {
    weak var presenter: PrivacyProtectionInteractorToPresenterProtocol!
    
}
