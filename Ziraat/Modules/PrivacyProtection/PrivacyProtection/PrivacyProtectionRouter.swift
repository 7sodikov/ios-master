//
//  PrivacyProtectionRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 2/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PrivacyProtectionRouterProtocol: class {
    static func createModule() -> UIViewController
}

class PrivacyProtectionRouter: PrivacyProtectionRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = PrivacyProtectionVC()
        let presenter = PrivacyProtectionPresenter()
        let interactor = PrivacyProtectionInteractor()
        let router = PrivacyProtectionRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
