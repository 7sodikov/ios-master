//
//  LoginConfirmationRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginConfirmationRouterProtocol: class {
    static func createModule(token: String,
                             userName: String,
                             password: String, data: AuthLoginResponse) -> UIViewController
    func navigateToHomeVC(in navigationCtrl: Any)
//    func navigateToResetPasswordVC(in navigationCtrl: Any)
}

class LoginConfirmationRouter: LoginConfirmationRouterProtocol {
    static func createModule(token: String,
                             userName: String,
                             password: String, data: AuthLoginResponse) -> UIViewController {
        let vc = LoginConfirmationVC()
        let presenter = LoginConfirmationPresenter()
        presenter.userData = (token, userName, password)
        let interactor = LoginConfirmationInteractor()
        let router = LoginConfirmationRouter()
        presenter.message = userName
        presenter.data = data
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToHomeVC(in navigationCtrl: Any) {
//        let securtyCtrl = SecuritySettingsListRouter.createModule()
//        (navigationCtrl as? UINavigationController)?.pushViewController(securtyCtrl, animated: true)
        appDelegate.router.navigate(to: .mainPage)
    }
    
//    func navigateToResetPasswordVC(in navigationCtrl: Any) {
//        let confirmationCtrl = ResetPasswordRouter.createModule()
//
//        (navigationCtrl as? UINavigationController)?.pushViewController(confirmationCtrl, animated: true)
//    }
}
