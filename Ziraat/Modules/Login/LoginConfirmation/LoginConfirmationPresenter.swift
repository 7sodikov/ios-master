//
//  LoginConfirmationPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoginConfirmationPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoginConfirmationVM { get }
    func loginButtonClicked(with smsCode: String?)
    func timerReachedItsEnd()
    func viewDidLoad()
}

protocol LoginConfirmationInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didLoginConfirm(with response: ResponseResult<LoginConfirmResponse, AppError>)
    func didLoginResendSMS(with response: ResponseResult<LoginResendSMSResponse, AppError>)
}

class LoginConfirmationPresenter: LoginConfirmationPresenterProtocol {
    weak var view: LoginConfirmationVCProtocol!
    var interactor: LoginConfirmationInteractorProtocol!
    var router: LoginConfirmationRouterProtocol!
    var userData: (token: String, userName: String, password: String)!
    var message: String!
    var data: AuthLoginResponse?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoginConfirmationVM()
    
    func loginButtonClicked(with smsCode: String?) {
        if isTimerRunning {
            if let smsCode = smsCode, smsCode.count > 0 {
                view.tipView(show: false)
                view.preloader(show: true)
                interactor.loginConfirm(token: userData.token,
                                        userName: userData.userName,
                                        password: userData.password,
                                        smsCode: smsCode)
            } else {
                view.tipView(show: true)
            }
        } else {
            view.preloader(show: true)
            interactor.loginResendSMS(token: userData.token)
        }
    }
    
    func timerReachedItsEnd() {
        isTimerRunning = false
        view.set(buttonTitle: RS.btn_resend_sms.localized())
    }
    
    // Private property and methods
    
    private var isTimerRunning = true
    
    func viewDidLoad() {
        let fullName = "\(data?.firstName ?? "") \(data?.middleName ?? "")"
        view.phoneMessage(title: data?.message ?? "", name: fullName)
    }
}

extension LoginConfirmationPresenter: LoginConfirmationInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didLoginConfirm(with response: ResponseResult<LoginConfirmResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            view.stopTimer()
            KeychainManager.sessionToken = result.data.token
            router.navigateToHomeVC(in: view.navigationCtrl)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didLoginResendSMS(with response: ResponseResult<LoginResendSMSResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            isTimerRunning = true
            view.startTimer()
            view.set(buttonTitle: RS.btn_login.localized())
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
