//
//  LoginConfirmationInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoginConfirmationInteractorProtocol: class {
    func loginConfirm(token: String, userName: String, password: String, smsCode: String)
    func loginResendSMS(token: String)
}

class LoginConfirmationInteractor: LoginConfirmationInteractorProtocol {
    weak var presenter: LoginConfirmationInteractorToPresenterProtocol!
    
    func loginConfirm(token: String, userName: String, password: String, smsCode: String) {
        NetworkService.Auth.loginConfirm(token: token, code: smsCode, username: userName,
                                         password: password) { (result) in
                                            self.presenter.didLoginConfirm(with: result)
        }
    }
    
    func loginResendSMS(token: String) {
        NetworkService.Auth.loginResendSMS(token: token) { (result) in
            self.presenter.didLoginResendSMS(with: result)
        }
    }
}
