//
//  LoginConfirmationVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import EasyTipView

protocol LoginConfirmationVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
    func tipView(show: Bool)
    func startTimer()
    func stopTimer()
    func set(buttonTitle: String)
    func phoneMessage(title: String, name: String)
}

class LoginConfirmationVC: BaseViewController, LoginConfirmationViewInstaller {
    var mainView: UIView { view }
    var backgroundImage: UIImageView!
    var dearLabel: UILabel!
    var smsSentLabel: UILabel!
    var yourCodeView: UIView!
    var yourCodeTextField: SMSConfirmationTextField!
    var hideButton: UIButton!
    var tipView: EasyTipView!
    var timerView: TimerView!
    var loginButton: NextButton!

    var presenter: LoginConfirmationPresenterProtocol!
    var router: LoginConfirmationRouter!
    
    var fullName: String!
    var iconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = " "
        let logo = UIImage(named: "img_logo_with_text")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        setupSubviews()
        
        loginButton.addTarget(self, action: #selector(loginButtonClicked(_:)), for: .touchUpInside)
        hideButton.addTarget(self, action: #selector(hideButtonClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
        
        timerView.startTimer(with: timerValue)
        timerView.timerReachedItsEnd = {
            self.presenter.timerReachedItsEnd()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    func phoneMessage(title: String, name: String) {
        smsSentLabel.text = "\(title)"
        dearLabel.text = "\(RS.lbl_user_ios.localized()) \(name)"
    }
    
    @objc private func hideButtonClicked(_ sender: UIButton) {
        if(iconClick == true) {
            yourCodeTextField.isSecureTextEntry = false
        } else {
            yourCodeTextField.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    @objc private func loginButtonClicked(_ sender: NextButton) {
        presenter.loginButtonClicked(with: yourCodeTextField.text)
        #warning("you're gonna kill me for this, but I had to do that")
        tipView.isHidden = true
    }
    
    override func preloader(show: Bool) {
        loginButton.animateIndicator = show
    }
}

extension LoginConfirmationVC: LoginConfirmationVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func tipView(show: Bool) {
        tipView.show(forView: yourCodeTextField)
    }
    
    func startTimer() {
        timerView.startTimer(with: timerValue)
    }
    
    func stopTimer() {
        timerView.stopTimer()
    }
    
    func set(buttonTitle: String) {
        loginButton.setTitle(buttonTitle, for: .normal)
    }
}
