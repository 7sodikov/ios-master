//
//  ResetPasswordVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol ResetPasswordVCProtocol: BaseViewControllerProtocol {
    func scroll(to page: Int)
    func show(_ message: String, withStatus status: Int, onDismiss perform: (() -> Void)?)
    func showSuccessAlert()
}

enum ResetPasswordCollectionCellType: Int {
    case email, verificationCode, password
    
    var title: String {
        switch self {
        case .email: return RS.lbl_forgot_pass()
        case .verificationCode: return RS.lbl_we_sent_code()
        case .password: return RS.lbl_set_pass()
        }
    }
    
    func description(interpolationStr: String) -> String? {
        switch self {
        case .email: return RS.lbl_enter_email_to_reset_pass()
        case .verificationCode: return RS.lbl_enter_check_code(interpolationStr)
        case .password: return nil
        }
    }
    
    var placeholder: String {
        switch self {
        case .email: return RS.placeho_email_addr()
        case .verificationCode: return RS.placeho_verif_cod()
        case .password: return RS.placeho_pass()
        }
    }
    
    var validationText: String? {
        switch self {
        case .email: return RS.lbl_email_inval()
        case .verificationCode: return nil
        case .password: return nil
        }
    }
}

class ResetPasswordVC: BaseViewController, ResetPasswordVCProtocol {
    var presenter: ResetPasswordPresenterProtocol!
    
    private lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = .zero
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        collection.isScrollEnabled = false
        collection.dataSource = self
        collection.delegate = self
        collection.isPagingEnabled = true
        collection.backgroundColor = .lightGray
        collection.register(ResetPasswordCollectionCell.self, forCellWithReuseIdentifier: "Cell")
//        collection.layer.borderWidth = 1
//        collection.layer.borderColor = UIColor.red.cgColor
        return collection
    }()
    
    private lazy var nextBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle(RS.btn_next().uppercased(), for: .normal)
        btn.setTitleColor(UIColor.orange.withAlphaComponent(0.6), for: .highlighted)
        btn.addTarget(self, action: #selector(nextClicked), for: .touchUpInside)
        return btn
    }()
    
    private let cellItems: [ResetPasswordCollectionCellType] = [.email, .verificationCode, .password]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        view.addSubview(nextBtn)
        IQKeyboardManager.shared.disabledToolbarClasses = [ResetPasswordVC.self]
        // IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        collectionView.snp.remakeConstraints { maker in
            maker.top.equalToSuperview().offset(84)
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
            maker.height.equalTo(200)
        }
        
        nextBtn.snp.remakeConstraints { maker in
            maker.width.equalTo(200)
            maker.height.equalTo(50)
            maker.right.equalToSuperview().offset(-20)
        }
        
        let constr = KeyboardLayoutConstraint(item: nextBtn, attribute: .bottom, relatedBy: .equal,
                                              toItem: view, attribute: .bottom,
                                              multiplier: 1.0, constant: -20)
        constr.listenToKeyboard()
        KeyboardLayoutConstraint.activate([constr])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func nextClicked() {
        view.endEditing(true)
        presenter.nextClicked()
    }
    
    func scroll(to page: Int) {
        collectionView.scrollToItem(at: IndexPath(item: page, section: 0),
                                    at: .centeredHorizontally, animated: true)
    }
    
    func show(_ message: String, withStatus status: Int, onDismiss perform: (() -> Void)?) {
        // UIView.reportUser(message, withStatus: status, onDismiss: perform)
    }
    
    func showSuccessAlert() {
//        alertInfo(title: RS.al_ttl_info(), message: RS.al_msg_pass_reset_success()) { _ in
//            self.navigationController?.popViewController(animated: true)
//        }
    }
}

extension ResetPasswordVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ResetPasswordCollectionCell
        let item = cellItems[indexPath.row]
        var email: String? = nil
        if indexPath.row == 1 {
            email = presenter.viewModel.email
        }
        
        cell.setup(for: item, interpolationStr: email)
        cell.textFieldDidChange = { text in
            self.presenter.textFieldDidChange(in: indexPath, text: text)
        }
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        (cell as? ResetPasswordCollectionCell)?.becomeResponder()
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
}
