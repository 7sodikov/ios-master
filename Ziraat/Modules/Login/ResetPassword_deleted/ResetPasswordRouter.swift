//
//  ResetPasswordRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ResetPasswordRouterProtocol: class {
    static func createModule() -> UIViewController
}

class ResetPasswordRouter: ResetPasswordRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ResetPasswordVC()
        let presenter = ResetPasswordPresenter()
        let interactor = ResetPasswordInteractor()
        let router = ResetPasswordRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
