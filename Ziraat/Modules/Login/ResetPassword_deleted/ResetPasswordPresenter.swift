//
//  ResetPasswordPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResetPasswordPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ResetPasswordVM { get }
    func nextClicked()
    func textFieldDidChange(in indexPath: IndexPath, text: String?)
}

protocol ResetPasswordInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didSendCode(model: UserInfo?, error: AppError?)
    func didSendConfirmationCode(error: AppError?)
    func didSetNewPassword(error: AppError?)
}

class ResetPasswordPresenter: ResetPasswordPresenterProtocol {
    
    weak var view: ResetPasswordVCProtocol!
    var interactor: ResetPasswordInteractorProtocol!
    var router: ResetPasswordRouterProtocol!
    private var currentPage: Int = 0
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ResetPasswordVM()
    
    func nextClicked() {
        if currentPage == 0 { // email entry
            guard let email = viewModel.email else { return }
            if email.isEmail {
                view.preloader(show: true)
                interactor.resetPassSendCode()
            }
            
        } else if currentPage == 1 { // confirmation code entry
            guard let code = viewModel.confirmationCode else { return }
            if !code.isEmpty {
                view.preloader(show: true)
                interactor.resetPassCheckCode()
            }
        } else if currentPage == 2 { // new password entry
            guard let newPass = viewModel.newPassword else { return }
            if !newPass.isEmpty {
                view.preloader(show: true)
                interactor.resetPassSetPassword()
            }
        }
    }
    
    func textFieldDidChange(in indexPath: IndexPath, text: String?) {
        if indexPath.row == 0 { // email entry
            viewModel.email = text
            
        } else if indexPath.row == 1 { // confirmation code entry
            viewModel.confirmationCode = text
            
        } else if indexPath.row == 2 { // new password entry
            viewModel.newPassword = text
        }
    }
}

extension ResetPasswordPresenter: ResetPasswordInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didSendCode(model: UserInfo?, error: AppError?) {
        view.preloader(show: false)
        if model != nil {
            currentPage += 1
            view.scroll(to: currentPage)
        } else {
            // view.show(error?.error.message ?? "", withStatus: .failure, onDismiss: nil)
        }
    }
    
    func didSendConfirmationCode(error: AppError?) {
        view.preloader(show: false)
        if let error = error {
            // view.show(error.error.message, withStatus: .failure, onDismiss: nil)
        } else {
            currentPage += 1
            view.scroll(to: currentPage)
        }
    }
    
    func didSetNewPassword(error: AppError?) {
        view.preloader(show: false)
        if let error = error {
            // view.show(error.error.message, withStatus: .failure, onDismiss: nil)
        } else {
            view.showSuccessAlert()
        }
    }
}
