//
//  ResetPasswordCollectionCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

class ResetPasswordCollectionCell: UICollectionViewCell {
    var textFieldDidChange: ((_ text: String?) -> Void)?
    
    private lazy var titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.font = UIFont.sfProDisplayBold(22)
        lbl.textColor = .black
        return lbl
    }()
    
    private lazy var descriptionLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.font = UIFont.sfFontRegular(17)
        lbl.textColor = .gray
        return lbl
    }()
    
    private lazy var txtField: UITextField = {
        let f = UITextField()
        f.autocapitalizationType = .none
        f.autocorrectionType = .no
        f.font = UIFont.sfFontRegular(17)
        f.textColor = .gray
        f.addTarget(self, action: #selector(txtFieldDidChange), for: .editingChanged)
        return f
    }()
    
    private lazy var validationLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.font = UIFont.sfFontRegular(17)
        lbl.textColor = .red
        lbl.isHidden = true
        return lbl
    }()
    
    private lazy var bottomSeparartor: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        return v
    }()
    
    private var cellType: ResetPasswordCollectionCellType!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    private func initialize() {
        contentView.addSubview(titleLbl)
        contentView.addSubview(descriptionLbl)
        contentView.addSubview(txtField)
        contentView.addSubview(bottomSeparartor)
        contentView.addSubview(validationLbl)
        
        titleLbl.snp.remakeConstraints { maker in
            maker.top.equalToSuperview()
            maker.left.equalToSuperview().offset(20)
            maker.right.equalToSuperview().offset(-20)
        }
        
        descriptionLbl.snp.remakeConstraints { maker in
            maker.top.equalTo(titleLbl.snp.bottom).offset(10)
            maker.left.equalTo(titleLbl.snp.left)
            maker.right.equalTo(titleLbl.snp.right)
        }
        
        txtField.snp.remakeConstraints { maker in
            maker.top.equalTo(descriptionLbl.snp.bottom).offset(20)
            maker.left.equalTo(descriptionLbl.snp.left)
            maker.right.equalTo(descriptionLbl.snp.right)
            maker.height.equalTo(44)
        }
        
        bottomSeparartor.snp.remakeConstraints { maker in
            maker.top.equalTo(txtField.snp.bottom)
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
            maker.height.equalTo(1)
        }
        
        validationLbl.snp.remakeConstraints { maker in
            maker.top.equalTo(bottomSeparartor.snp.bottom).offset(10)
            maker.left.equalTo(titleLbl.snp.left)
            maker.right.equalTo(titleLbl.snp.right)
        }
    }
    
    func setup(for cellType: ResetPasswordCollectionCellType, interpolationStr: String?) {
        self.cellType = cellType
        titleLbl.text = cellType.title
        descriptionLbl.text = cellType.description(interpolationStr: interpolationStr ?? "")
        txtField.placeholder = cellType.placeholder
        validationLbl.text = cellType.validationText
        // txtField.placeholderColor = .lightGray
    }
    
    @objc private func txtFieldDidChange() {
        if cellType == .email {
            validationLbl.isHidden = (txtField.text?.isEmail == true)
        }
        textFieldDidChange?(txtField.text)
    }
}
