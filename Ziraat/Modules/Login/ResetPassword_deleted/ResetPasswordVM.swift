//
//  ResetPasswordVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ResetPasswordVM {
    var email: String?
    var confirmationCode: String?
    var newPassword: String?
    var id: String?
}
