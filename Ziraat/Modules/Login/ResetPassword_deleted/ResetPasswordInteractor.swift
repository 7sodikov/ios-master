//
//  ResetPasswordInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResetPasswordInteractorProtocol: class {
    func resetPassSendCode()
    func resetPassCheckCode()
    func resetPassSetPassword()
}

class ResetPasswordInteractor: ResetPasswordInteractorProtocol {
    weak var presenter: ResetPasswordPresenterProtocol!
    func resetPassSendCode() {
//        AuthenticationApiServer.shared.resetPassSendCode(email: viewModel.email ?? "") { model, error in
//            if let model = model {
//                self.viewModel.id = model.id
//            }
//            self.presenter.didSendCode(model: model, error: error)
//        }
    }
    
    func resetPassCheckCode() {
//        AuthenticationApiServer.shared.resetPassCheckCode(email: viewModel.email ?? "",
//                                                          code: viewModel.confirmationCode ?? "")
//        { error in
//            self.presenter.didSendConfirmationCode(error: error)
//        }
    }
    
    func resetPassSetPassword() {
//        AuthenticationApiServer.shared.resetPassSetPassword(email: viewModel.email ?? "",
//                                                            code: viewModel.confirmationCode ?? "",
//                                                            id: viewModel.id ?? "",
//                                                            newPassword: viewModel.newPassword ?? "")
//        { error in
//            self.presenter.didSetNewPassword(error: error)
//        }
    }
}
