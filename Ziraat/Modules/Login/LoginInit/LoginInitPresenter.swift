//
//  LoginInitPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import SafariServices

protocol LoginInitPresenterProtocol: class {
    var viewModel: LoginInitVM { get }
    func nextButtonClicked(with navigationCtrl: UINavigationController)
    func settingsClicked()
    func stackButtonClicked(buttonEnum button: LoginInitVC.Button)
}

protocol LoginInitInteractorToPresenterProtocol: class {
    
}

class LoginInitPresenter: LoginInitPresenterProtocol {
    weak var view: LoginInitVCProtocol!
    var interactor: LoginInitInteractorProtocol!
    var router: LoginInitRouterProtocol!
    var controller: LoginInitVC { view as! LoginInitVC }
    
    private(set) var viewModel = LoginInitVM()
    
    func nextButtonClicked(with navigationCtrl: UINavigationController) {
        router.navigateToLogin(in: navigationCtrl)
    }
    
    func settingsClicked() {
        self.router.presentSettings(controller, self)
    }
    
    func stackButtonClicked(buttonEnum button: LoginInitVC.Button) {
        var vc: UIViewController
        switch button {
            
        case .calculator:
            vc = DashboardLoanCalculatorRouter.createModuleWhiteMode()
        case .bank:
//            guard let urlString = bankUrlString, let url = try? urlString.asURL() else { return }
//            vc = SFSafariViewController(url: url)
            vc = BankOnMapRouter.createModule()
        case .currency:
            vc = CurrencyRouter().viewController
        case .warning:
            vc = LegalWarningRouter.createModule()
        case .callCenter:
            router.pesentCallCenter(from: controller)
            return
        }
        
        router.openVC(vc: vc, from: controller.navigationController!)
    }
    
    private var bankUrlString: String? {
        switch LocalizationManager.language {
        
        case .english:
            return URLConstant.nearestBranchEnglish
        case .russian:
            return URLConstant.nearestBranchRussian
        case .uzbek:
            return URLConstant.nearestBranchUzbek
        case .turkish:
            return URLConstant.nearestBranchTurkish
        case .lanDesc, .none:
            return nil
        }
    }
    
    
}

extension LoginInitPresenter: LoginInitInteractorToPresenterProtocol {
    
}

extension LoginInitPresenter : BottomPanelSetttingsDelegate {
    func actionClicked(_ index: Int) {
        view.changeLoginSettingsImage()
        
        if index == 0 {
            router.openAbout(controller)
        }
        if index == 1 {
            router.shareApp(controller)
        }
    }
    
}
