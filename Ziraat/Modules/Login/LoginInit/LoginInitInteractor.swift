//
//  LoginInitInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoginInitInteractorProtocol: AnyObject {
    
}

class LoginInitInteractor: LoginInitInteractorProtocol {
    weak var presenter: LoginInitInteractorToPresenterProtocol!
    
}
