//
//  LoginInitViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol LoginInitViewInstaller: ViewInstaller {
    var backgroundView: UIImageView! { get set }
    var logoImageView: UIImageView! {get set}
    var settingsButton: UIButton! {get set}
    var welcomeLabel: UILabel! { get set }
    var nextButton: NextButton! { get set }
    
    var hStack1: UIStackView! {get set}
    var hStack2: UIStackView! {get set}
    var vStack: UIStackView! {get set}
    var buttons: [TopIconButton]! {get set}
}

extension LoginInitViewInstaller {
    func initSubviews() {
        backgroundView = UIImageView()
        backgroundView.image =  UIImage(named: "img_background")
        
        logoImageView = .init()
        logoImageView.image = UIImage(named: "img_logo_with_text")
        logoImageView.contentMode = .scaleAspectFit
        
        settingsButton = .init()
        settingsButton.setImage(UIImage(named: "img_login_settings"), for: .normal)
        
        welcomeLabel = UILabel()
//        welcomeLabel.font = .gothamNarrow(size: 16, .medium)//EZFontType.regular.sfuiDisplay(size: Adaptive.val(16))
        welcomeLabel.textColor = .white
        welcomeLabel.numberOfLines = 0
        welcomeLabel.lineBreakMode = .byWordWrapping
        welcomeLabel.textAlignment = .center
        welcomeLabel.text = RS.lbl_welcome.localized()
        
        nextButton = NextButton.systemButton(with: RS.btn_login.localized(), cornerRadius: LoginInitSize.TextField.height/2)
        nextButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        
        buttons = LoginInitVC.Button.allCases.map { (be) -> TopIconButton in
            let button = TopIconButton()
            button.setImage(UIImage(named: be.icon)?.withRenderingMode(.alwaysTemplate), for: .normal)
            button.tag = be.rawValue
            button.setTitle(be.title, for: .normal)
            button.backgroundColor = .clear
            button.tintColor = .white
            button.setTitleColor(.white, for: .normal)
            return button
        }
        
        hStack1 = .init()
        hStack1.spacing = 30
        
        hStack2 = .init()
        hStack2.spacing = 30
        
        vStack = .init()
        vStack.axis = .vertical
        vStack.spacing = 30
        
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundView)
        mainView.addSubview(logoImageView)
        mainView.addSubview(settingsButton)
        mainView.addSubview(welcomeLabel)
        mainView.addSubview(nextButton)
        mainView.addSubview(vStack)
        
        let topButtons = LoginInitVC.Button.topButtons.map(\.rawValue)
        buttons.filter { topButtons.contains($0.tag) }.forEach {
            hStack1.addArrangedSubview($0)
        }
        
        let bottomButtons = LoginInitVC.Button.bottomButtons.map(\.rawValue)
        buttons.filter { bottomButtons.contains($0.tag) }.forEach {
            hStack2.addArrangedSubview($0)
        }
        
        vStack.addArrangedSubview(hStack1)
        vStack.addArrangedSubview(hStack2)
    }
    
    func addSubviewsConstraints() {
        backgroundView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    
        logoImageView.constraint { (make) in
            make.height(Adaptive.val(45)).width(Adaptive.val(133))
                .top(self.mainView.topAnchor, offset: Adaptive.val(40))
                .centerX.equalToSuperView()
        }
    
        settingsButton.constraint { (make) in
            make.square(28)
                .centerY(self.logoImageView.centerYAnchor)
                .trailing(-16).equalToSuperView()
        }
        
        welcomeLabel.constraint { (maker) in
            maker.top(self.logoImageView.bottomAnchor, offset: 16)
                .horizontal(16).equalToSuperView()
        }
        
        nextButton.constraint { (maker) in
            maker.height(LoginInitSize.TextField.height)
                .top(self.welcomeLabel.bottomAnchor, offset: Adaptive.val(158))
                .horizontal(16).equalToSuperView()
        }
        
        vStack.constraint { (make) in
            make.bottom(self.mainView.bottomAnchor, offset: -Adaptive.val(72))
                .centerX.equalToSuperView()
        }
        
        buttons.forEach { (button) in
            button.constraint { $0.width(button.width).height(button.height) }
        }
    }
}

struct LoginInitSize {
    struct TextField {
        static let height = Adaptive.val(50)
    }
}

fileprivate extension String {
    func height(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [NSAttributedString.Key.font: font],
                                            context: nil)
        return ceil(boundingBox.height)
    }
}
