//
//  LoginInitVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginInitVCProtocol: AnyObject {

    func changeLoginSettingsImage()
}

class LoginInitVC: UIViewController, LoginInitVCProtocol, LoginInitViewInstaller {
    
    var hStack1: UIStackView!
    var hStack2: UIStackView!
    var vStack: UIStackView!
    var buttons: [TopIconButton]!
    
    
    var logoImageView: UIImageView!
    var settingsButton: UIButton!
    
    var mainView: UIView { view }
    var backgroundView: UIImageView!
    var welcomeLabel: UILabel!
    var nextButton: NextButton!
    
    var presenter: LoginInitPresenterProtocol!
    
    var isSelected = false
    
    var loginInitSettingsOn: UIBarButtonItem!
    var loginInitSettingsOFF: UIBarButtonItem!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
        settingsButton.addTarget(self, action: #selector(loginSettingsClicked(_:)), for: .touchUpInside)
        buttons.forEach { (button) in
            button.addTarget(self, action: #selector(stackButtonClicked), for: .touchUpInside)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func changeLoginSettingsImage() {
        settingsButton.tag = settingsButton.tag ^ 1
        let image = UIImage(named: (settingsButton.tag == 0 ? "img_login_settings" : "img_dash_settings_selected"))
        settingsButton.setImage(image, for: .normal)
    }
    
    @objc func stackButtonClicked(_ sender: UIButton) {
        if let button = Button(rawValue: sender.tag) {
            presenter.stackButtonClicked(buttonEnum: button)
        }
    }
    
    @objc private func loginSettingsClicked(_ sender: UIButton) {
        changeLoginSettingsImage()
        presenter.settingsClicked()
    }
    
    @objc func nextButtonClicked(_ sender: NextButton) {
        guard let navVC = self.navigationController else { return }
        presenter.nextButtonClicked(with: navVC)
    }
}

fileprivate struct Size {
    struct TextField {
        static let height = Adaptive.val(50)
    }
}
