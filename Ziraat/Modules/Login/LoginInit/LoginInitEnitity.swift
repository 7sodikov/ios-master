//
//  LoginInitEnitity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/6/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import Rswift

extension LoginInitVC {
    
    enum Button: Int, CaseIterable {
        case calculator
        case bank
        case currency
        case warning
        case callCenter
        
        static var topButtons: [Button] = [.calculator, .bank, .currency]
        static var bottomButtons: [Button] = [.warning, .callCenter]
        
        var title: String {
            let dic: [Button: Rswift.StringResource] = [
                .calculator: RS.lbl_loan_calc,
                .bank:       RS.lbl_nearest_branch,
                .currency:   RS.lbl_exchange,
                .warning:    RS.lbl_legal_warning,
                .callCenter: RS.nav_ttl_contact_center,
            ]
            return dic[self]!.localized()
        }
        
        var icon: String {
            let dic: [Button: String] = [
                .calculator: "img_contact_form",
                .bank: "img_banks",
                .currency: "img_login_currency",
                .warning: "img_legal_warning",
                .callCenter: "img_call_centre",
            ]
            return dic[self]!
        }
    }
}
