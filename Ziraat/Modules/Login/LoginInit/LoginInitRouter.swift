//
//  LoginInitRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginInitRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToLogin(in navigationCtrl: UINavigationController)
    func presentSettings(_ view : UIViewController, _ delegate : BottomPanelSetttingsDelegate)
    func openAbout(_ view : UIViewController)
    func shareApp(_ view : UIViewController)
    func openVC(vc: UIViewController, from parentVC: UINavigationController)
    func pesentCallCenter(from viewController: UIViewController)
}

class LoginInitRouter: LoginInitRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = LoginInitVC()
        let navCtrl = NavigationController(rootViewController: vc)
        let presenter = LoginInitPresenter()
        let interactor = LoginInitInteractor()
        let router = LoginInitRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return navCtrl
    }
    
    func navigateToLogin(in navigationCtrl: UINavigationController) {
        let confirmationCtrl = LoginRouter.createModule()
        navigationCtrl.pushViewController(confirmationCtrl, animated: true)
    }
    
    func presentSettings(_ view : UIViewController, _ delegate : BottomPanelSetttingsDelegate){
        let module = BottomPanelSettingsRouter.createModule(delegate)
            module.modalPresentationStyle = .overCurrentContext
            view.present(module, animated: false, completion: nil)
    }
    
    func openAbout(_ view : UIViewController){
        let module = AboutRouter.createModule()
        view.navigationController?.pushViewController(module, animated: true)
    }
    
    func shareApp(_ view : UIViewController){
        guard let URL = try? URLConstant.appstoreURL.asURL() else { return }
        
        let bName = Utils.bundleDisplayName() ?? ""
        let vc = UIActivityViewController(activityItems: [bName, URL], applicationActivities: [])
        
        if let popovercontroller = vc.popoverPresentationController{
            popovercontroller.sourceView = view.view
            popovercontroller.sourceRect = view.view.bounds
        }
        view.present(vc, animated:true, completion:nil)
        
    }
    
    func openVC(vc: UIViewController, from parentVC: UINavigationController) {
        parentVC.pushViewController(vc, animated: true)
    }
    
    func pesentCallCenter(from viewController: UIViewController) {
        let callCenter = CallRouter.createModule()
        callCenter.modalPresentationStyle = .overCurrentContext
        viewController.present(callCenter, animated: true)
    }
    
}
