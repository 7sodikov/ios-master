//
//  ResetPasswordPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResetPasswordPresenterProtocol: class {
    // VIEW -> PRESENTER
    func viewDidLoad()
    var viewModel: ResetPasswordVM { get }
    func changePasswordClicked(oldPassword: String, newPassword: String, repeatPassword: String)
    func successAlertDismissed()
    func publicOfferCheckboxClicked(_ isOn: Bool)
    var isRedStyle: Bool { get }
}

protocol ResetPasswordInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didChangePassword(with response: ResponseResult<SingleMessageResponse, AppError>)
}

class ResetPasswordPresenter: ResetPasswordPresenterProtocol {
    
    unowned var view: ResetPasswordVCProtocol!
    var loginInitData: LoginInitResponse?
    var interactor: ResetPasswordInteractorProtocol!
    var router: ResetPasswordRouterProtocol!
    var isRedStyle = true
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ResetPasswordVM()
    
    func viewDidLoad() {
        
    }
    
    func changePasswordClicked(oldPassword: String, newPassword: String, repeatPassword: String) {
        if newPassword != repeatPassword {
            view.showError(message: RS.al_msg_confirm_pass_doesnt_match.localized())
            return
        }
        
        if newPassword.count < 6 || newPassword.count > 20  {
            view.showError(message: RS.al_msg_pass_length_error.localized())
            return
        }
        
        if !isPublicOfferAccepted {
            view.showError(message: RS.al_msg_public_offer_error.localized())
            return
        }
        
        guard let encryptedOldPass = RSAEncrypter.encrypt(text: oldPassword),
              let encryptedNewPass = RSAEncrypter.encrypt(text: newPassword),
              let encryptedRepeatPass = RSAEncrypter.encrypt(text: repeatPassword) else {
            view.showError(message: RS.al_msg_smth_went_wrong_error.localized())
            return
        }
        
        view.preloader(show: true)
        interactor.changePassword(oldPassword: encryptedOldPass,
                                  newPassword: encryptedNewPass,
                                  repeatPassword: encryptedRepeatPass)
    }
    
    func successAlertDismissed() {
        router.navigateLoginVC()
    }
    
    func publicOfferCheckboxClicked(_ isOn: Bool) {
        isPublicOfferAccepted = isOn
        if isOn {
            router.navigateToPublicOffer(in: view as Any)
        }
    }
    
    // Private property and methods
    private var isPublicOfferAccepted = false
}

extension ResetPasswordPresenter: ResetPasswordInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didChangePassword(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            view.showToast(message: result.data.message ?? "")
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
