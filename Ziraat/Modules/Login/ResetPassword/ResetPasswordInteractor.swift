//
//  ResetPasswordInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ResetPasswordInteractorProtocol: class {
    func changePassword(oldPassword: String, newPassword: String, repeatPassword: String)
}

class ResetPasswordInteractor: ResetPasswordInteractorProtocol {
    weak var presenter: ResetPasswordInteractorToPresenterProtocol!
    
    func changePassword(oldPassword: String, newPassword: String, repeatPassword: String) {
        NetworkService.Auth.changePassword(oldPassword: oldPassword, newPassword: newPassword, repeatPassword: repeatPassword) { (result) in
            self.presenter.didChangePassword(with: result)
        }
    }
}
