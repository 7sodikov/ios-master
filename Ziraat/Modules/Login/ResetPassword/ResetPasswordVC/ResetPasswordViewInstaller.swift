//
//  ResetPasswordViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import EasyTipView

protocol ResetPasswordViewInstaller: ViewInstaller {
    var backgroundView: UIImageView! { get set }
    var logoImageView: UIImageView!{ get set }
    var changePasswordLabel: UILabel! { get set }
    var currentPasswordLabel: UILabel! { get set }
    var currentPasswordTextField: LoginTextField! { get set }
    var currentPasswordEyeButton: UIButton! { get set }
    var infoButton: UIButton! { get set }
    var newPasswordLabel: UILabel! { get set }
    var newPasswordTextField: LoginTextField! { get set }
    var newPasswordEyeButton: UIButton! { get set }
    var repeatPasswordLabel: UILabel! { get set }
    var repeatPasswordTextField: LoginTextField! { get set }
    var repeatPasswordEyeButton: UIButton! { get set }
    var publicOfferCheckBox: EZCheckBox! { get set }
    var nextButton: NextButton! { get set }
    var tipView: EasyTipView! { get set }
}

extension ResetPasswordViewInstaller {
    func initSubviews() {
        backgroundView = UIImageView()
        backgroundView.image =  UIImage(named: "img_background")
        
        logoImageView = UIImageView()
        logoImageView.image = UIImage(named: "img_logo")
        
        changePasswordLabel = UILabel()
        changePasswordLabel.text = RS.lbl_change_password.localized()
        changePasswordLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        changePasswordLabel.textColor = .white
        changePasswordLabel.numberOfLines = 0
        changePasswordLabel.textAlignment = .center
        
        currentPasswordLabel = createTextFieldTitleLabel(with: RS.lbl_current_password.localized())
        
        currentPasswordTextField = LoginTextField(with: "- - -", cornerRadius: Size.TextField.height/2, isSecureTextEntry: true)
        currentPasswordTextField.rightViewMode = .always
        
        currentPasswordEyeButton = createEyeButton(with: 1)
        
        infoButton = UIButton()
        infoButton.setBackgroundImage(UIImage(named: "img_dark_info"), for: .normal)
        
        newPasswordLabel = createTextFieldTitleLabel(with: RS.lbl_new_password.localized())
        
        newPasswordTextField = LoginTextField(with: "- - -", cornerRadius: Size.TextField.height/2, isSecureTextEntry: true)
        newPasswordTextField.rightViewMode = .always
        
        newPasswordEyeButton = createEyeButton(with: 2)
        
        repeatPasswordLabel = createTextFieldTitleLabel(with: RS.lbl_new_password_again.localized())
        
        repeatPasswordTextField = LoginTextField(with: "- - -", cornerRadius: Size.TextField.height/2, isSecureTextEntry: true)
        repeatPasswordTextField.rightViewMode = .always
        
        repeatPasswordEyeButton = createEyeButton(with: 3)
        
        publicOfferCheckBox = EZCheckBox(frame: .zero)
        publicOfferCheckBox.titleLabel.text = RS.check_box_public_offer_agreement.localized()
        publicOfferCheckBox.isOn = false
        
        nextButton = NextButton.systemButton(with: RS.lbl_change_password.localized(),
                                             cornerRadius: Size.TextField.height/2)
        nextButton.isUserInteractionEnabled = false
        
        var tipPreferences = EasyTipView.Preferences()
        if let font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(13)) {
            tipPreferences.drawing.font = font
        }
        tipPreferences.drawing.foregroundColor = .white
        tipPreferences.drawing.backgroundColor = ColorConstants.highlightedGray
        tipPreferences.drawing.arrowPosition = EasyTipView.ArrowPosition.any
        tipPreferences.drawing.arrowHeight = Adaptive.val(10)
        tipPreferences.drawing.arrowWidth = Adaptive.val(20)
        tipPreferences.drawing.borderWidth = Adaptive.val(5)
        tipPreferences.drawing.borderColor = UIColor.white.withAlphaComponent(0.7)
        
        tipView = EasyTipView(text: RS.lbl_password_rule.localized(), preferences: tipPreferences)
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundView)
        mainView.addSubview(logoImageView)
        mainView.addSubview(changePasswordLabel)
        mainView.addSubview(currentPasswordLabel)
        mainView.addSubview(currentPasswordTextField)
        currentPasswordTextField.rightView = currentPasswordEyeButton
        mainView.addSubview(infoButton)
        mainView.addSubview(newPasswordLabel)
        mainView.addSubview(newPasswordTextField)
        newPasswordTextField.rightView = newPasswordEyeButton
        mainView.addSubview(repeatPasswordLabel)
        mainView.addSubview(repeatPasswordTextField)
        repeatPasswordTextField.rightView = repeatPasswordEyeButton
        mainView.addSubview(publicOfferCheckBox)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        backgroundView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        logoImageView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(50))
            maker.height.equalTo(Adaptive.val(33))
            maker.width.equalTo(Adaptive.val(142))
            maker.centerX.equalToSuperview()
        }
        
        changePasswordLabel.snp.remakeConstraints { (maker) in
            maker.left.equalToSuperview().offset(Adaptive.val(60))
            maker.right.equalToSuperview().inset(Adaptive.val(60))
            maker.top.equalTo(logoImageView).offset(Adaptive.val(100))
        }
        
        currentPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(changePasswordLabel.snp.bottom).offset(Size.interItemSpace)
            maker.left.equalTo(Size.leftRightPadding)
        }
        
        currentPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(currentPasswordLabel.snp.bottom).offset(Size.interLabelTextFieldSpace)
            maker.left.equalTo(Size.leftRightPadding)
            maker.right.equalTo(-Size.leftRightPadding)
            maker.height.equalTo(Size.TextField.height)
        }
        
        currentPasswordEyeButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.EyeButton.size.width)
            maker.height.equalTo(Size.EyeButton.size.height)
        }
        
        infoButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(24))
            maker.centerY.equalTo(newPasswordLabel)
            maker.trailing.equalTo(newPasswordLabel.snp.trailing).offset(Adaptive.val(31))
        }
        
        newPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(currentPasswordTextField.snp.bottom).offset(Size.interItemSpace)
            maker.left.equalTo(Size.leftRightPadding)
        }
        
        newPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(newPasswordLabel.snp.bottom).offset(Size.interLabelTextFieldSpace)
            maker.left.right.height.equalTo(currentPasswordTextField)
        }
        
        newPasswordEyeButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.EyeButton.size.width)
            maker.height.equalTo(Size.EyeButton.size.height)
        }
        
        repeatPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(newPasswordTextField.snp.bottom).offset(Size.interItemSpace)
            maker.left.equalTo(Size.leftRightPadding)
        }
        
        repeatPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(repeatPasswordLabel.snp.bottom).offset(Size.interLabelTextFieldSpace)
            maker.left.right.height.equalTo(currentPasswordTextField)
        }
        
        repeatPasswordEyeButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.EyeButton.size.width)
            maker.height.equalTo(Size.EyeButton.size.height)
        }
        
        publicOfferCheckBox.snp.remakeConstraints { (maker) in
            maker.top.equalTo(repeatPasswordTextField.snp.bottom).offset(Size.interItemSpace)
            maker.left.equalTo(repeatPasswordTextField)
            maker.height.equalTo(Adaptive.val(20))
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(publicOfferCheckBox.snp.bottom).offset(Size.interItemSpace)
            maker.left.right.equalTo(currentPasswordTextField)
            
            maker.height.equalTo(Size.TextField.height)
        }
    }
    
    private func createTextFieldTitleLabel(with text: String) -> UILabel {
        let lbl = UILabel()
        lbl.text = text
        lbl.textColor = .white
        lbl.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        return lbl
    }
    
    private func createEyeButton(with tag: Int) -> UIButton {
        let btn = UIButton()
        btn.tag = tag
        btn.tintColor = .black
        btn.setImage(UIImage(named: "img_eye")?.changeColor(.black), for: .normal)
        btn.setImage(UIImage(named: "btn_eye_crossed_white")?.changeColor(.black), for: .selected)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }
}

fileprivate struct Size {
    struct TextField {
        static let height = Adaptive.val(50)
    }
    struct EyeButton {
        static let size = Adaptive.size(50, 20)
    }
    static let interItemSpace = Adaptive.val(20)
    static let interLabelTextFieldSpace = Adaptive.val(10)
    static let leftRightPadding = Adaptive.val(20)
}
