//
//  ResetPasswordVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import Foundation
import EasyTipView

protocol ResetPasswordVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
    func showToast(message: String)
}

class ResetPasswordVC: BaseViewController, ResetPasswordViewInstaller {
    
    var mainView: UIView { view }
    var backgroundView: UIImageView!
    var logoImageView: UIImageView!
    var changePasswordLabel: UILabel!
    var currentPasswordLabel: UILabel!
    var currentPasswordTextField: LoginTextField!
    var currentPasswordEyeButton: UIButton!
    var infoButton: UIButton!
    var newPasswordLabel: UILabel!
    var newPasswordTextField: LoginTextField!
    var newPasswordEyeButton: UIButton!
    var repeatPasswordTextField: LoginTextField!
    var repeatPasswordEyeButton: UIButton!
    var publicOfferCheckBox: EZCheckBox!
    var repeatPasswordLabel: UILabel!
    var nextButton: NextButton!
    var tipView: EasyTipView!
    
    var presenter: ResetPasswordPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setNavigation()
        
        nextButton.addTarget(self, action: #selector(changePasswordClicked(_:)), for: .touchUpInside)
        infoButton.addTarget(self, action: #selector(openInfo(_:)), for: .touchUpInside)
        
        currentPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        newPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        repeatPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        currentPasswordEyeButton.addTarget(self, action: #selector(textFieldEyeButtonClicked(_:)), for: .touchUpInside)
        newPasswordEyeButton.addTarget(self, action: #selector(textFieldEyeButtonClicked(_:)), for: .touchUpInside)
        repeatPasswordEyeButton.addTarget(self, action: #selector(textFieldEyeButtonClicked(_:)), for: .touchUpInside)
        
        publicOfferCheckBox.addTarget(self, action: #selector(publicOfferCheckboxClicked(_:)), for: .valueChanged)
        
        if presenter.isRedStyle {
            navigationItem.leftBarButtonItem = nil
            
            backgroundView.image = UIImage(named: "img_background")
            logoImageView.isHidden = false
            changePasswordLabel.isHidden = false
            currentPasswordLabel.textColor = .white
            newPasswordLabel.textColor = .white
            repeatPasswordLabel.textColor = .white
            publicOfferCheckBox.color = .white
        } else {
            backgroundView.image = UIImage(named: "img_dash_light_background")
            logoImageView.isHidden = true
            changePasswordLabel.isHidden = true
            currentPasswordLabel.textColor = .black
            newPasswordLabel.textColor = .black
            repeatPasswordLabel.textColor = .black
            publicOfferCheckBox.color = .black
            
            nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
            nextButton.setBackgroundColor(ColorConstants.highlightedGray, for: .selected)
        }
        
        presenter.viewDidLoad()
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        if parent == nil {
            dismissTipView()
        }
    }
    
    @objc private func openInfo(_ sender: UIButton) {
        tipView.show(forView: infoButton)
    }
    
    func dismissTipView(){
        tipView.dismiss()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        tipView.dismiss()
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        nextButton.isUserInteractionEnabled = isFieldsValid()
    }
    
    @objc private func textFieldEyeButtonClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.tag == 1 {
            currentPasswordTextField.isSecureTextEntry = !sender.isSelected
            
        } else if sender.tag == 2 {
            newPasswordTextField.isSecureTextEntry = !sender.isSelected
            
        } else if sender.tag == 3 {
            repeatPasswordTextField.isSecureTextEntry = !sender.isSelected
        }
    }
    
    @objc private func publicOfferCheckboxClicked(_ sender: EZCheckBox) {
        presenter.publicOfferCheckboxClicked(sender.isOn)
    }
    
    private func isFieldsValid() -> Bool {
        return (currentPasswordTextField.text?.isEmpty != true &&
            newPasswordTextField.text?.isEmpty != true &&
            repeatPasswordTextField.text?.isEmpty != true)
    }
    
    @objc private func changePasswordClicked(_ sender: UIButton) {
        presenter.changePasswordClicked(oldPassword: currentPasswordTextField.text ?? "",
                                        newPassword: newPasswordTextField.text ?? "",
                                        repeatPassword: repeatPasswordTextField.text ?? "")
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if presenter.isRedStyle {
            (self.navigationController as? NavigationController)?.background(isWhite: true)
        } else {
            (self.navigationController as? NavigationController)?.background(isWhite: false)
        }
    }
    
    func setNavigation() {
        if presenter.isRedStyle {
            
        } else {
            self.title = RS.lbl_change_password.localized()
            
            let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
            self.navigationItem.rightBarButtonItem = homeButton
        }
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ResetPasswordVC: ResetPasswordVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func showToast(message: String) {
        let alert = AlertController(title: RS.lbl_success.localized(), message: message, preferredStyle: .alert)
        alert.showAutoDismissAlert(in: self, okClick: {
            self.presenter.successAlertDismissed()
        })
    }
}

//extension ResetPasswordVC: LoginTextFieldDelegate {
//
//    func textChanged(textField: LoginTextField) {
//        guard let password = newPasswordTextField.safeText,
//            let confirmPassword = repeatPasswordTextField.safeText,
//            let _ = self.currentPasswordTextField.safeText else {
//                self.nextButton.isEnabled = false
//                return
//        }
//
//        if password.count < 8 && confirmPassword.count < 8 {
//            self.nextButton.isEnabled = false
//            return
//        }
//
//        if password.count == confirmPassword.count {
//            if password != confirmPassword {
//                self.nextButton.isEnabled = false
//                return
//            }
//
//            let capitalLetterRegEx  = ".*[A-z]+.*"
//            let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
//            let capitalresult = texttest.evaluate(with: password)
//
//            if !capitalresult {
//                self.nextButton.isEnabled = false
//                return
//            }
//
//            let numberRegEx  = ".*[0-9]+.*"
//            let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
//            let numberresult = texttest1.evaluate(with: password)
//
//            if !numberresult {
//                self.nextButton.isEnabled = false
//                return
//            }
//
//        } else {
//            self.nextButton.isEnabled = false
//            return
//        }
//
//        self.nextButton.isEnabled = true
//    }
//}
