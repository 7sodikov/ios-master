//
//  ResetPasswordRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ResetPasswordRouterProtocol: class {
    static func createModule(isRedStyle: Bool) -> UIViewController
    static func createModuleWithNavigationController(isRedStyle: Bool) -> UIViewController
    func navigateLoginVC()
    func navigateToPublicOffer(in view: Any)
}

class ResetPasswordRouter: ResetPasswordRouterProtocol {
    
    static func createModule(isRedStyle: Bool) -> UIViewController {
        let vc = ResetPasswordVC()
        let presenter = ResetPasswordPresenter()
        let interactor = ResetPasswordInteractor()
        let router = ResetPasswordRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.isRedStyle = isRedStyle
        interactor.presenter = presenter
        
        return vc
    }
    
    static func createModuleWithNavigationController(isRedStyle: Bool) -> UIViewController {
        let module = createModule(isRedStyle: isRedStyle)
        let navCtrl = NavigationController(rootViewController: module)
        return navCtrl
    }
    
    func navigateLoginVC() {
        appDelegate.router.navigate(to: .login)
    }
    
    func navigateToPublicOffer(in view: Any) {
        let viewCtrl = view as! UIViewController
        let legalWarningVC = LegalWarningRouter.createModule()
        viewCtrl.navigationController?.pushViewController(legalWarningVC, animated: true)
    }
}
