 //
//  ForgotPasswordRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordRouterProtocol: class {
    static func createModule() -> UIViewController
}

class ForgotPasswordRouter: ForgotPasswordRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ForgotPasswordVC()
        let presenter = ForgotPasswordPresenter()
        let interactor = ForgotPasswordInteractor()
        let router = ForgotPasswordRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
}
