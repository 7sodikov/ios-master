//
//  ForgotPasswordPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ForgotPasswordVM { get }
    
}

protocol ForgotPasswordInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class ForgotPasswordPresenter: ForgotPasswordPresenterProtocol {
    weak var view: ForgotPasswordVCProtocol!
    var interactor: ForgotPasswordInteractorProtocol!
    var router: ForgotPasswordRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ForgotPasswordVM()
    
    // Private property and methods
}

extension ForgotPasswordPresenter: ForgotPasswordInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}

extension ForgotPasswordPresenter: ForgotPasswordFirstPageDelegate {
    func forgotPasswordFirstPageFinished(with loginInitData: LoginInitResponse?) {
        viewModel.loginInitData = loginInitData
        let nextPage = view.getController(for: 1) as? ForgotPasswordSecondPageVC
        let nextPagePresenter = nextPage?.presenter as? ForgotPasswordSecondPagePresenter
        nextPagePresenter?.loginInitData = loginInitData
        view.scroll(to: 1, direction: .forward)
    }
}

extension ForgotPasswordPresenter: ForgotPasswordSecondPageDelegate {
    func forgotPasswordSecondPageFinished(with messageResendPasswordData: LoginResendSMSResponse?) {
        let nextPage = view.getController(for: 2) as? ForgotPasswordThirdPageVC
        let nextPagePresenter = nextPage?.presenter as? ForgotPasswordThirdPagePresenter
        nextPagePresenter?.loginInitData = viewModel.loginInitData
        nextPagePresenter?.messageResendPasswordData = messageResendPasswordData
        view.scroll(to: 2, direction: .forward)
    }
}

extension ForgotPasswordPresenter: ForgotPasswordThirdPageDelegate {
    func forgotPasswordThirdPageFinished() {
        let nextPage = view.getController(for: 3) as? ForgotPasswordForthPageVC
        let nextPagePresenter = nextPage?.presenter as? ForgotPasswordForthPagePresenter
        nextPagePresenter?.loginInitData = viewModel.loginInitData
        view.scroll(to: 3, direction: .forward)
    }
}

//func dataEntryPageFinished(with prepareResponse: OperationPrepareResponse) {
//    let nextPage = view.getController(for: 1) as? OperationPageDataPreviewVC
//    let nextPagePresenter = nextPage?.presenter as? OperationPageDataPreviewPresenter
//    nextPagePresenter?.prepareResponse = prepareResponse
//    view.scroll(to: 1, direction: .forward)
//}
