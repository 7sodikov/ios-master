//
//  ForgotPasswordForthPageViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import EasyTipView

protocol ForgotPasswordForthPageViewInstaller: ViewInstaller {
    var createPasswordLabel: UILabel! { get set }
    var passwordDescriptionLabel: UILabel! { get set }
    var infoButton: UIButton! { get set }
    var infoBackView: UIView! { get set }
    var tipView: EasyTipView? { get set }
    var newPasswordLabel: UILabel! { get set }
    var newPasswordView: UIView! { get set }
    var newPasswordTextField: LoginTextField! { get set }
    var newPasswordHideButton: UIButton! { get set }
    var repeatPasswordLabel: UILabel! { get set }
    var repeatPasswordView: UIView! { get set }
    var repeatPasswordTextField: LoginTextField! { get set }
    var repeatPasswordHideButton: UIButton! { get set }
    var publicOfferView: UIView! { get set }
    var publicOfferCheckBox: EZCheckBox! { get set }
    var nextButton: NextButton! { get set }
    var alert: AlertController! { get set }
}

extension ForgotPasswordForthPageViewInstaller {
    func initSubviews() {
        createPasswordLabel = UILabel()
        createPasswordLabel.text = RS.lbl_create_password.localized()
        createPasswordLabel.textColor = .white
        createPasswordLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        
        passwordDescriptionLabel = UILabel()
        passwordDescriptionLabel.text = RS.lbl_enter_new_password.localized()
        passwordDescriptionLabel.numberOfLines = 0
        passwordDescriptionLabel.textColor = .white
        passwordDescriptionLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(14))
        
        newPasswordLabel = UILabel()
        newPasswordLabel.text = RS.lbl_new_password.localized()
        newPasswordLabel.textColor = .white
        newPasswordLabel.font = EZFontType.medium.sfuiDisplay(size: ForgotPasswordSize.fieldTitleFontSize)
        
        infoBackView = UIView()
        infoBackView.backgroundColor = .clear
        infoBackView.isHidden = true
        
        infoButton = UIButton()
        infoButton.setImage(UIImage(named: "img_icon_info"), for: .normal)
        
        var tipPreferences = EasyTipView.Preferences()
        if let font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(13)) {
            tipPreferences.drawing.font = font
        }
        tipPreferences.drawing.foregroundColor = .black
        tipPreferences.drawing.backgroundColor = .white
        tipPreferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        tipPreferences.drawing.arrowHeight = Adaptive.val(10)
        tipPreferences.drawing.arrowWidth = Adaptive.val(20)
        tipPreferences.drawing.borderWidth = Adaptive.val(5)
        tipPreferences.drawing.borderColor = UIColor.white.withAlphaComponent(0.7)
        
        tipView = EasyTipView(text: RS.lbl_password_rule.localized(), preferences: tipPreferences)
        
        newPasswordView = UIView()
        newPasswordView.layer.cornerRadius = ForgotPasswordSize.TextField.height/2
        newPasswordView.backgroundColor = .white
        
        newPasswordTextField = LoginTextField(with: RS.lbl_new_password.localized(), cornerRadius: ForgotPasswordSize.TextField.height/2, isSecureTextEntry: true)
        
        newPasswordHideButton = UIButton()
        if #available(iOS 13.0, *) {
            newPasswordHideButton.setImage(UIImage(named: "btn_eye")?.withTintColor(.black), for: .normal)
        } else {
            newPasswordHideButton.imageView?.tintColor = .black
        }
        
        repeatPasswordLabel = UILabel()
        repeatPasswordLabel.text = RS.lbl_repeat_new_password.localized()
        repeatPasswordLabel.textColor = .white
        repeatPasswordLabel.font = EZFontType.medium.sfuiDisplay(size: ForgotPasswordSize.fieldTitleFontSize)
        
        repeatPasswordView = UIView()
        repeatPasswordView.layer.cornerRadius = ForgotPasswordSize.TextField.height/2
        repeatPasswordView.backgroundColor = .white
        
        repeatPasswordTextField = LoginTextField(with: RS.lbl_repeat_new_password.localized(), cornerRadius: ForgotPasswordSize.TextField.height/2, isSecureTextEntry: true)
        
        repeatPasswordHideButton = UIButton()
        if #available(iOS 13.0, *) {
            repeatPasswordHideButton.setImage(UIImage(named: "btn_eye")?.withTintColor(.black), for: .normal)
        } else {
            repeatPasswordHideButton.imageView?.tintColor = .black
        }
        
        publicOfferView = UIView()
        
        publicOfferCheckBox = EZCheckBox(frame: .zero)
        publicOfferCheckBox.titleLabel.text = RS.check_box_public_offer_agreement.localized()
        publicOfferCheckBox.isOn = false
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: ForgotPasswordSize.TextField.height/2)
        
        alert = AlertController(title: RS.lbl_success.localized(), message: RS.al_msg_success.localized(), preferredStyle: .alert)
    }
    
    func embedSubviews() {
        mainView.addSubview(createPasswordLabel)
        mainView.addSubview(passwordDescriptionLabel)
        mainView.addSubview(infoBackView)
        mainView.addSubview(infoButton)
        
        mainView.addSubview(newPasswordLabel)
        mainView.addSubview(newPasswordView)
        newPasswordView.addSubview(newPasswordTextField)
        newPasswordView.addSubview(newPasswordHideButton)
        
        mainView.addSubview(repeatPasswordLabel)
        mainView.addSubview(repeatPasswordView)
        repeatPasswordView.addSubview(repeatPasswordTextField)
        repeatPasswordView.addSubview(repeatPasswordHideButton)
        
        mainView.addSubview(publicOfferView)
        publicOfferView.addSubview(publicOfferCheckBox)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        createPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(ForgotPasswordSize.padding)
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
        }
        
        passwordDescriptionLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(createPasswordLabel.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.equalTo(createPasswordLabel)
        }
        
        newPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(passwordDescriptionLabel.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.equalTo(createPasswordLabel)
        }
        
        infoBackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        infoButton.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(newPasswordLabel.snp.bottom)
            maker.trailing.equalTo(newPasswordView)
        }
        
        newPasswordView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(newPasswordLabel.snp.bottom).offset(ForgotPasswordSize.fieldTitleSpace)
            maker.leading.equalToSuperview().offset(Adaptive.val(20))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(20))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        newPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.leading.bottom.equalTo(newPasswordView)
            maker.trailing.equalTo(newPasswordHideButton.snp.leading)
        }
        
        newPasswordHideButton.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(newPasswordView)
            maker.trailing.equalTo(newPasswordView.snp.trailing).offset(-Adaptive.val(21))
            maker.height.equalTo(Adaptive.val(25))
            maker.width.equalTo(Adaptive.val(25))
        }
        
        repeatPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(newPasswordTextField.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.equalTo(createPasswordLabel)
        }
        
        repeatPasswordView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(repeatPasswordLabel.snp.bottom).offset(ForgotPasswordSize.fieldTitleSpace)
            maker.leading.equalToSuperview().offset(Adaptive.val(20))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(20))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        repeatPasswordTextField.snp.remakeConstraints { (maker) in
            maker.top.leading.bottom.equalTo(repeatPasswordView)
            maker.trailing.equalTo(repeatPasswordHideButton.snp.leading)
        }
        
        repeatPasswordHideButton.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(repeatPasswordView)
            maker.trailing.equalTo(repeatPasswordView.snp.trailing).offset(-Adaptive.val(21))
            maker.height.equalTo(Adaptive.val(25))
            maker.width.equalTo(Adaptive.val(25))
        }
        
        publicOfferView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(repeatPasswordTextField.snp.bottom).offset(Adaptive.val(20))
            maker.left.equalTo(repeatPasswordTextField)
            maker.height.equalTo(Adaptive.val(20))
        }
        
        publicOfferCheckBox.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(publicOfferView)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
            maker.bottom.equalTo(-Adaptive.val(50))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
    }
}
