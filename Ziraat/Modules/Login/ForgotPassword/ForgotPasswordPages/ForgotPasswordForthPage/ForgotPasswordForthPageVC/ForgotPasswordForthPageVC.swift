//
//  ForgotPasswordForthPageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import EasyTipView

protocol ForgotPasswordForthPageVCProtocol: BaseViewControllerProtocol {
    func showToastMessage()
    func dismissTipView()
}

class ForgotPasswordForthPageVC: BaseViewController, ForgotPasswordForthPageViewInstaller {
    var mainView: UIView { view }
    
    var createPasswordLabel: UILabel!
    var passwordDescriptionLabel: UILabel!
    var infoBackView: UIView!
    var infoButton: UIButton!
    var tipView: EasyTipView?
    var newPasswordLabel: UILabel!
    var newPasswordView: UIView!
    var newPasswordTextField: LoginTextField!
    var newPasswordHideButton: UIButton!
    var repeatPasswordLabel: UILabel!
    var repeatPasswordView: UIView!
    var repeatPasswordTextField: LoginTextField!
    var repeatPasswordHideButton: UIButton!
    var publicOfferView: UIView!
    var publicOfferCheckBox: EZCheckBox!
    var nextButton: NextButton!
    var alert: AlertController!
    
    var iconClick = true
    
    var presenter: ForgotPasswordForthPagePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        newPasswordTextField.addTarget(self, action: #selector(newPasswordFieldDidChange(_:)), for: .editingChanged)
        repeatPasswordTextField.addTarget(self, action: #selector(repeatPasswordFieldDidChange(_:)), for: .editingChanged)
        
        infoButton.addTarget(self, action: #selector(openInfo(_:)), for: .touchUpInside)
        newPasswordHideButton.addTarget(self, action: #selector(newPasswordHideButtonClicked(_:)), for: .touchUpInside)
        repeatPasswordHideButton.addTarget(self, action: #selector(repeatPasswordHideButtonClicked(_:)), for: .touchUpInside)

        let gesture = UITapGestureRecognizer(target: self, action: #selector (self.handleTapAnywhere))
        self.infoBackView.addGestureRecognizer(gesture)
        
        publicOfferCheckBox.addTarget(self, action: #selector(publicOfferCheckboxClicked(_:)), for: .valueChanged)
                
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
    
    @objc private func newPasswordHideButtonClicked(_ sender: UIButton) {
        if iconClick {
            newPasswordTextField.isSecureTextEntry = false
        } else {
            newPasswordTextField.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    @objc private func publicOfferCheckboxClicked(_ sender: EZCheckBox) {
        presenter.publicOfferCheckboxClicked(sender.isOn)
    }
    
    @objc private func repeatPasswordHideButtonClicked(_ sender: UIButton) {
        if iconClick {
            repeatPasswordTextField.isSecureTextEntry = false
        } else {
            repeatPasswordTextField.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    @objc private func newPasswordFieldDidChange(_ textField: LoginTextField) {
        presenter.viewModel.userEntry.newPassword = textField.text
    }
    
    @objc private func repeatPasswordFieldDidChange(_ textField: LoginTextField) {
        presenter.viewModel.userEntry.repeatPassword = textField.text
    }
    
    @objc private func openInfo(_ sender: UIButton) {
        tipView?.show(forView: infoButton)
        infoBackView.isHidden = false
    }
    
    func dismissTipView(){
        tipView?.dismiss()
    }
    
    @objc private func handleTapAnywhere(_ sender: UIGestureRecognizer) {
        tipView?.dismiss()
        infoBackView.isHidden = true
    }
    
    @objc private func nextButtonClicked(_ sender: NextButton) {
        presenter.nextButtonClicked()
    }
}

extension ForgotPasswordForthPageVC: ForgotPasswordForthPageVCProtocol {
    func showToastMessage() {
        alert.showAutoDismissAlert(in: self, okClick: {
            self.presenter.successAlertDismissed()
        })
        
//        showToast(message: RS.al_msg_success.localized(), font: EZFontType.medium.sfuiDisplay(size: Adaptive.val(15)) ?? .systemFont(ofSize: 15.0))
    }
}
