//
//  ForgotPasswordForthPageRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordForthPageRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateLoginVC()
    func navigateToPublicOffer(in view: Any)
}

class ForgotPasswordForthPageRouter: ForgotPasswordForthPageRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ForgotPasswordForthPageVC()
        let presenter = ForgotPasswordForthPagePresenter()
        let interactor = ForgotPasswordForthPageInteractor()
        let router = ForgotPasswordForthPageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateLoginVC() {
        appDelegate.router.navigate(to: .login)
    }
    
    func navigateToPublicOffer(in view: Any) {
        let viewCtrl = view as! UIViewController
        let legalWarningVC = LegalWarningRouter.createModule()
        viewCtrl.navigationController?.pushViewController(legalWarningVC, animated: true)
    }
}
