//
//  ForgotPasswordForthPageInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordForthPageInteractorProtocol: class {
    func passwordRecoveryStepFour(for token: String, newPassword: String, repeatPassword: String)
}

class ForgotPasswordForthPageInteractor: ForgotPasswordForthPageInteractorProtocol {
    weak var presenter: ForgotPasswordForthPageInteractorToPresenterProtocol!
    
    func passwordRecoveryStepFour(for token: String, newPassword: String, repeatPassword: String) {
        NetworkService.Auth.passwordRecoveryStepFour(token: token, newPassword: newPassword, repeatPassword: repeatPassword) { (result) in
            self.presenter.didPasswordRecoveryStepFour(with: result)
        }
    }
}
