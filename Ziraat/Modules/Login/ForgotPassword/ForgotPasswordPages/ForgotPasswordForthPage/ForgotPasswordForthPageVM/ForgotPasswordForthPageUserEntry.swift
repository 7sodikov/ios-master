//
//  ForgotPasswordForthPageUserEntry.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ForgotPasswordForthPageUserEntry {
    var newPassword: String?
    var repeatPassword: String?
}
