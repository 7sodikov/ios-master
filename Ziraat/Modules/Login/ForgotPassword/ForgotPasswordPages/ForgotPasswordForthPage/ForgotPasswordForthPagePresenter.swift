//
//  ForgotPasswordForthPagePresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordForthPagePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ForgotPasswordForthPageVM { get }
    func nextButtonClicked()
    func successAlertDismissed()
    func publicOfferCheckboxClicked(_ isOn: Bool)
}

protocol ForgotPasswordForthPageInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryStepFour(with response: ResponseResult<LogoutResponse, AppError>)
}

class ForgotPasswordForthPagePresenter: ForgotPasswordForthPagePresenterProtocol {
    weak var view: ForgotPasswordForthPageVCProtocol!
    var interactor: ForgotPasswordForthPageInteractorProtocol!
    var router: ForgotPasswordForthPageRouterProtocol!
    var loginInitData: LoginInitResponse?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ForgotPasswordForthPageVM()
    
    func nextButtonClicked() {
        guard let newPassword = viewModel.userEntry.newPassword, newPassword.count > 0,
            let repeatPassword = viewModel.userEntry.repeatPassword, repeatPassword.count > 0 else {
                view.showError(message: RS.lbl_fill_all_fields.localized())
                return
        }
        
        if newPassword != repeatPassword {
            view.showError(message: RS.lbl_passwords_not_match.localized())
            return
        }
        
        if !isPublicOfferAccepted {
            view.showError(message: RS.al_msg_public_offer_error.localized())
            return
        }
        
        guard let encryptedNewPass = RSAEncrypter.encrypt(text: newPassword),
              let encryptedRepeatPass = RSAEncrypter.encrypt(text: repeatPassword) else {
            view.showError(message: RS.al_msg_smth_went_wrong_error.localized())
            return
        }
        
        view.preloader(show: true)
        interactor.passwordRecoveryStepFour(for: loginInitData?.token ?? "",
                                            newPassword: encryptedNewPass,
                                            repeatPassword: encryptedRepeatPass)
    }
    
    func successAlertDismissed() {
        router.navigateLoginVC()
    }
    
    func publicOfferCheckboxClicked(_ isOn: Bool) {
        isPublicOfferAccepted = isOn
        if isOn {
            router.navigateToPublicOffer(in: view as Any)
        }
    }
    
    // Private property and methods
    private var isPublicOfferAccepted = false
}

extension ForgotPasswordForthPagePresenter: ForgotPasswordForthPageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryStepFour(with response: ResponseResult<LogoutResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            view.showToastMessage()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
