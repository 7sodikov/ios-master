//
//  ForgotPasswordFirstPageViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import InputMask

protocol ForgotPasswordFirstPageViewInstaller: ViewInstaller {
    var personalInformationLabel: UILabel! { get set }
    var identityLabel: UILabel! { get set }
    var identityFieldListener: MaskedTextFieldDelegate! { get set }
    var identityTextField: LoginTextField! { get set }
    var phoneCodeLabel: UILabel! { get set }
    var phoneCodeDropdownList: EZDropdownList! { get set }
    var phoneNumberFieldListener: MaskedTextFieldDelegate! { get set }
    var phoneNumberTextField: LoginTextField! { get set }
    var securityCodeLabel: UILabel! { get set }
    var captchaView: CaptchaView! { get set }
    var securityCodeTextField: LoginTextField! { get set }
    var phoneStackView: UIStackView! { get set }
    var securityStackView: UIStackView! { get set }
    var nextButton: NextButton! { get set }
}

extension ForgotPasswordFirstPageViewInstaller {
    func initSubviews() {
        personalInformationLabel = UILabel()
        personalInformationLabel.text = RS.lbl_personal_info.localized()
        personalInformationLabel.textColor = .white
        personalInformationLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        
        identityLabel = UILabel()
        identityLabel.text = RS.lbl_identity_number.localized()
        identityLabel.textColor = .white
        identityLabel.font = EZFontType.medium.sfuiDisplay(size: ForgotPasswordSize.fieldTitleFontSize)
        
//        identityFieldListener = MaskedTextFieldDelegate()
//        identityFieldListener.affinityCalculationStrategy = .prefix
//        identityFieldListener.primaryMaskFormat = "[AA] [0000000]"
        identityTextField = LoginTextField(with: RS.lbl_identity_number.localized(), keyboard: .default, cornerRadius: ForgotPasswordSize.TextField.height/2, isSecureTextEntry: false)
//        identityTextField.delegate = identityFieldListener
        
        phoneCodeLabel = UILabel()
        phoneCodeLabel.text = RS.lbl_phone_code.localized()
        phoneCodeLabel.textColor = .white
        phoneCodeLabel.font = EZFontType.medium.sfuiDisplay(size: ForgotPasswordSize.fieldTitleFontSize)
        
        phoneCodeDropdownList = EZDropdownList(frame: .zero)
        phoneCodeDropdownList.selectedIndex = 0
        phoneCodeDropdownList.layer.cornerRadius = ForgotPasswordSize.TextField.height/2
//        phoneCodeDropdownList.itemFont = UIFont.boldSystemFont(ofSize: (Adaptive.val(16.0)))
//        phoneCodeDropdownList.arrowSize = Adaptive.size(25, 11)
//        phoneCodeDropdownList.edgeInsets = Adaptive.edges(top: 0, left: 10, bottom: 0, right: 10)
        phoneCodeDropdownList.allowNonSelection = false
        phoneCodeDropdownList.placeholder = "998"
        
        phoneNumberFieldListener = MaskedTextFieldDelegate()
        phoneNumberFieldListener.affinityCalculationStrategy = .prefix
        phoneNumberFieldListener.primaryMaskFormat = "[00] [000] [00] [00]"
        phoneNumberTextField = LoginTextField(with: RS.lbl_phone_code.localized(), keyboard: .numberPad, cornerRadius: ForgotPasswordSize.TextField.height/2, isSecureTextEntry: false)
        phoneNumberTextField.delegate = phoneNumberFieldListener
        
        securityCodeLabel = UILabel()
        securityCodeLabel.text = RS.lbl_security_code.localized()
        securityCodeLabel.textColor = .white
        securityCodeLabel.font = EZFontType.medium.sfuiDisplay(size: ForgotPasswordSize.fieldTitleFontSize)
        
        captchaView = CaptchaView()
        
        securityCodeTextField = LoginTextField(with: RS.lbl_security_code.localized(), cornerRadius: ForgotPasswordSize.TextField.height/2, isSecureTextEntry: false)
        securityCodeTextField.autocapitalizationType = .none
        
        phoneStackView = UIStackView()
        phoneStackView.spacing = Adaptive.val(15)
        
        securityStackView = UIStackView()
        securityStackView.axis = .horizontal
        securityStackView.spacing = Adaptive.val(7)
        securityStackView.alignment = .center
        securityStackView.distribution = .fillEqually
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: ForgotPasswordSize.TextField.height/2)
    }
    
    func embedSubviews() {
        mainView.addSubview(personalInformationLabel)
        mainView.addSubview(identityLabel)
        mainView.addSubview(identityTextField)
        mainView.addSubview(phoneCodeLabel)
        mainView.addSubview(securityCodeLabel)
        
        phoneStackView.addArrangedSubview(phoneCodeDropdownList)
        phoneStackView.addArrangedSubview(phoneNumberTextField)
        mainView.addSubview(phoneStackView)
        
        securityStackView.addArrangedSubview(captchaView)
        securityStackView.addArrangedSubview(securityCodeTextField)
        mainView.addSubview(securityStackView)
        
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        personalInformationLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(ForgotPasswordSize.padding)
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
        }
        
        identityLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(personalInformationLabel.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.equalTo(personalInformationLabel)
        }
        
        identityTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(identityLabel.snp.bottom).offset(ForgotPasswordSize.fieldTitleSpace)
            maker.left.right.equalTo(personalInformationLabel)
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        phoneCodeLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(identityTextField.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.left.right.equalTo(personalInformationLabel)
        }
        
        phoneCodeDropdownList.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(100))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        phoneNumberTextField.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(100))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        phoneStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(phoneCodeLabel.snp.bottom).offset(ForgotPasswordSize.fieldTitleSpace)
            maker.left.right.equalTo(personalInformationLabel)
        }
        
        securityCodeLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(phoneStackView.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.left.right.equalTo(personalInformationLabel)
        }
        
        captchaView.snp.remakeConstraints { (maker) in
//            maker.width.equalTo(Adaptive.val(110))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        securityCodeTextField.snp.remakeConstraints { (maker) in
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        securityStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(securityCodeLabel.snp.bottom).offset(ForgotPasswordSize.fieldTitleSpace)
            maker.left.right.equalTo(personalInformationLabel)
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
            maker.bottom.equalTo(-Adaptive.val(50))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
    }
}
