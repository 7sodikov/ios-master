//
//  ForgotPasswordFirstPageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol ForgotPasswordFirstPageVCProtocol: BaseViewControllerProtocol {
    func showNewReceivedCaptcha()
}

class ForgotPasswordFirstPageVC: BaseViewController, ForgotPasswordFirstPageViewInstaller {
    var mainView: UIView { view }
    var personalInformationLabel: UILabel!
    var identityLabel: UILabel!
    var identityFieldListener: MaskedTextFieldDelegate!
    var identityTextField: LoginTextField!
    var phoneCodeLabel: UILabel!
    var phoneCodeDropdownList: EZDropdownList!
    var phoneNumberFieldListener: MaskedTextFieldDelegate!
    var phoneNumberTextField: LoginTextField!
    var securityCodeLabel: UILabel!
    var captchaView: CaptchaView!
    var spaceView: UIView!
    var securityCodeTextField: LoginTextField!
    var phoneStackView: UIStackView!
    var securityStackView: UIStackView!
    var nextButton: NextButton!
    var presenter: ForgotPasswordFirstPagePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        phoneCodeDropdownList.delegate = self
        phoneCodeDropdownList.setupProperties()
        
//        identityFieldListener.listener = self
        identityTextField.delegate = self
        phoneNumberFieldListener.listener = self
        
        identityTextField.addTarget(self, action: #selector(identityFieldDidChange(_:)), for: .editingChanged)
        securityCodeTextField.addTarget(self, action: #selector(captchaFieldDidChange(_:)), for: .editingChanged)
        captchaView.reloadCaptchaButton.addTarget(self, action: #selector(reloadCaptchaClicked), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
    
    @objc private func identityFieldDidChange(_ textField: LoginTextField) {
        presenter.viewModel.userEntry.pinFL = textField.text
    }
    
    @objc private func captchaFieldDidChange(_ textField: LoginTextField) {
        presenter.viewModel.userEntry.captcha = textField.text
    }
    
    @objc private func reloadCaptchaClicked() {
        presenter.renewCaptchaClicked()
    }
    
    @objc private func nextButtonClicked(_ sender: NextButton) {
        presenter.nextButtonClicked()
    }
}

extension ForgotPasswordFirstPageVC: ForgotPasswordFirstPageVCProtocol {
    func showNewReceivedCaptcha() {
        captchaView.captchaImageView.image = presenter.viewModel.loginInitData?.captchaImg.imageFromBase64
    }
}

extension ForgotPasswordFirstPageVC: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        if textField == identityTextField {
            presenter.viewModel.userEntry.pinFL = value
        } else if textField == phoneNumberTextField {
            presenter.viewModel.userEntry.phoneNumber = value
        }
    }
}

extension ForgotPasswordFirstPageVC: EZDropdownListDelegate {
    func ezDropdownListItems(_ dropdownList: EZDropdownList) -> [String] {
        return presenter.viewModel.phoneCodes
    }
    
    func ezDropdownList(_ dropdownList: EZDropdownList, didSelectItemAt index: Int) {
        presenter.viewModel.userEntry.phoneCode = presenter.viewModel.phoneCodes[index]
    }
    
    func ezDropdownListNonSelectionClicked(_ dropdownList: EZDropdownList) {
        presenter.viewModel.userEntry.phoneCode = nil
    }
}
