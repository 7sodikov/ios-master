//
//  ForgotPasswordFirstPageInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordFirstPageInteractorProtocol: class {
    func passwordRecoveryInit()
    func passwordRecoveryResendCaptcha(for token: String)
    func passwordRecoveryStepOne(for token: String, phone: String, pinfl: String, captcha: String)
}

class ForgotPasswordFirstPageInteractor: ForgotPasswordFirstPageInteractorProtocol {
    weak var presenter: ForgotPasswordFirstPageInteractorToPresenterProtocol!
    
    func passwordRecoveryInit() {
        NetworkService.Auth.passwordRecoveryInit { (result) in
            self.presenter.didPasswordRecoveryInit(with: result)
        }
    }
    
    func passwordRecoveryResendCaptcha(for token: String) {
        NetworkService.Auth.passwordRecoveryResendCaptcha(token: token) { (result) in
            self.presenter.didPasswordRecoveryResendCaptcha(with: result)
        }
    }
    
    func passwordRecoveryStepOne(for token: String, phone: String, pinfl: String, captcha: String) {
        NetworkService.Auth.passwordRecoveryStepOne(token: token, phone: phone, pinfl: pinfl, captcha: captcha) { (result) in
            self.presenter.didPasswordRecoveryStepOne(with: result)
        }
    }
}
