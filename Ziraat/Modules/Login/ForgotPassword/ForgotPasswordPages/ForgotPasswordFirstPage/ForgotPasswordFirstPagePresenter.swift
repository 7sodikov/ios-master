//
//  ForgotPasswordFirstPagePresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordFirstPageDelegate: class {
    func forgotPasswordFirstPageFinished(with loginInitData: LoginInitResponse?)
}

protocol ForgotPasswordFirstPagePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ForgotPasswordFirstPageVM { get }
    func viewDidLoad()
    func renewCaptchaClicked()
    func nextButtonClicked()
    func openDropDown(with navigationCtrl: Any)
}

protocol ForgotPasswordFirstPageInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryInit(with response: ResponseResult<LoginInitResponse, AppError>)
    func didPasswordRecoveryResendCaptcha(with response: ResponseResult<LoginInitResponse, AppError>)
    func didPasswordRecoveryStepOne(with response: ResponseResult<LogoutResponse, AppError>)
}

class ForgotPasswordFirstPagePresenter: ForgotPasswordFirstPagePresenterProtocol {
    weak var view: ForgotPasswordFirstPageVCProtocol!
    var interactor: ForgotPasswordFirstPageInteractorProtocol!
    var router: ForgotPasswordFirstPageRouterProtocol!
    weak var delegate: ForgotPasswordFirstPageDelegate?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ForgotPasswordFirstPageVM()
    
    func viewDidLoad() {
        interactor.passwordRecoveryInit()
    }
    
    func renewCaptchaClicked() {
        if let token = viewModel.loginInitData?.token {
            interactor.passwordRecoveryResendCaptcha(for: token)
        } else {
            interactor.passwordRecoveryInit()
        }
    }
    
    func nextButtonClicked() {
        guard viewModel.userEntry.isFullPhoneNumberValid,
            let pinFL = viewModel.userEntry.pinFL, pinFL.count >= 9,
            let captcha = viewModel.userEntry.captcha, captcha.count > 0 else {
                view.showError(message: RS.lbl_fill_all_fields.localized())
                return
        }
        view.preloader(show: true)
        interactor.passwordRecoveryStepOne(for: viewModel.loginInitData?.token ?? "",
                                           phone: viewModel.userEntry.fullPhoneNumber,
                                           pinfl: pinFL,
                                           captcha: captcha)
    }
    
    func openDropDown(with navigationCtrl: Any) {
        router.openDropDownList(in: navigationCtrl)
    }
    
    // Private property and methods
    
}

extension ForgotPasswordFirstPagePresenter: ForgotPasswordFirstPageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryInit(with response: ResponseResult<LoginInitResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.loginInitData = result.data
            KeychainManager.rsaPublicKey = result.data.pubKey
            view.showNewReceivedCaptcha()
        case .failure:
            break
        }
    }
    
    func didPasswordRecoveryResendCaptcha(with response: ResponseResult<LoginInitResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.loginInitData = result.data
            KeychainManager.rsaPublicKey = result.data.pubKey
            view.showNewReceivedCaptcha()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didPasswordRecoveryStepOne(with response: ResponseResult<LogoutResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            viewModel.resendPasswordData = result.data
            router.navigateToNextPage(with: viewModel.loginInitData, delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
