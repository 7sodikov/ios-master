//
//  ForgotPasswordFirstPageVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ForgotPasswordFirstPageVM {
    let userEntry = ForgotPasswordFirstPageUserEntry()
    var loginInitData: LoginInitResponse?
    var resendPasswordData: LogoutResponse?
    let phoneCodes = ["998"]
}
