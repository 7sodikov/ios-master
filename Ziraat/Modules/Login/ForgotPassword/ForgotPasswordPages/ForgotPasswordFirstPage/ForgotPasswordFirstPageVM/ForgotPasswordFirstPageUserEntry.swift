//
//  ForgotPasswordFirstPageUserEntry.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ForgotPasswordFirstPageUserEntry {
    var pinFL: String?
    var phoneCode: String? = "998"
    var phoneNumber: String?
    var captcha: String?
    
    var fullPhoneNumber: String {
        return (phoneCode ?? "") + (phoneNumber ?? "")
    }
    
    var isFullPhoneNumberValid: Bool {
        return (phoneCode != nil && phoneNumber?.count == 9)
    }
}
