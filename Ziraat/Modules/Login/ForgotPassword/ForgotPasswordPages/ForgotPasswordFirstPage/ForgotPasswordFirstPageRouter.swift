//
//  ForgotPasswordFirstPageRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordFirstPageRouterProtocol: class {
    static func createModule(with delegate: ForgotPasswordPresenter?) -> UIViewController
    func openDropDownList(in navigationCtrl: Any)
    func navigateToNextPage(with loginInitData: LoginInitResponse?, delegate: ForgotPasswordFirstPageDelegate?)
}

class ForgotPasswordFirstPageRouter: ForgotPasswordFirstPageRouterProtocol {
    static func createModule(with delegate: ForgotPasswordPresenter?) -> UIViewController {
        let vc = ForgotPasswordFirstPageVC()
        let presenter = ForgotPasswordFirstPagePresenter()
        let interactor = ForgotPasswordFirstPageInteractor()
        let router = ForgotPasswordFirstPageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func openDropDownList(in navigationCtrl: Any) {
        let dropDownList = ForgotPasswordDropDownRouter.createModule()
        (navigationCtrl as? UINavigationController)?.pushViewController(dropDownList, animated: true)
    }
    
    func navigateToNextPage(with loginInitData: LoginInitResponse?, delegate: ForgotPasswordFirstPageDelegate?) {
        delegate?.forgotPasswordFirstPageFinished(with: loginInitData)
    }
}
