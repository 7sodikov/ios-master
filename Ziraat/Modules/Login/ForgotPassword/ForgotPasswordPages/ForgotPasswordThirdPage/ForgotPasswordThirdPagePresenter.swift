//
//  ForgotPasswordThirdPagePresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordThirdPageDelegate: class {
    func forgotPasswordThirdPageFinished()
}

protocol ForgotPasswordThirdPagePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ForgotPasswordThirdPageVM { get }
    func viewDidLoad()
    func timerReachedItsEnd()
    func nextButtonClicked()
}

protocol ForgotPasswordThirdPageInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryResendSMS(with response: ResponseResult<LoginResendSMSResponse, AppError>)
    func didPasswordRecoveryStepThree(with response: ResponseResult<LogoutResponse, AppError>)
}

class ForgotPasswordThirdPagePresenter: ForgotPasswordThirdPagePresenterProtocol {
    weak var view: ForgotPasswordThirdPageVCProtocol!
    var interactor: ForgotPasswordThirdPageInteractorProtocol!
    var router: ForgotPasswordThirdPageRouterProtocol!
    
    weak var delegate: ForgotPasswordThirdPageDelegate?
    var loginInitData: LoginInitResponse?
    var messageResendPasswordData: LoginResendSMSResponse?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ForgotPasswordThirdPageVM()
    
    func viewDidLoad() {
        view.showSmsSentLabel(text: messageResendPasswordData?.message)
    }
    
    func timerReachedItsEnd() {
        isTimerRunning = false
        view.set(buttonTitle: RS.btn_resend_sms.localized())
    }
    
    func nextButtonClicked() {
        if isTimerRunning {
            if let smsCode = viewModel.userEntry.smsCode, smsCode.count == 6 {
                view.preloader(show: true)
                interactor.passwordRecoveryStepThree(for: loginInitData?.token ?? "",
                                                     code: smsCode)
            } else {
                view.showError(message: RS.lbl_fill_all_fields.localized())
            }
        } else {
            view.preloader(show: true)
            interactor.passwordRecoveryResendSMS(token: loginInitData?.token ?? "")
        }
    }
    
    // Private property and methods
    
    private var isTimerRunning = true
}

extension ForgotPasswordThirdPagePresenter: ForgotPasswordThirdPageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryResendSMS(with response: ResponseResult<LoginResendSMSResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            isTimerRunning = true
            view.startTimer()
            view.set(buttonTitle: RS.btn_further.localized())
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didPasswordRecoveryStepThree(with response: ResponseResult<LogoutResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            view.stopTimer()
            router.navigateToNextPage(delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
