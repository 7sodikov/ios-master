//
//  ForgotPasswordThirdPageRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordThirdPageRouterProtocol: class {
    static func createModule(with delegate: ForgotPasswordPresenter?) -> UIViewController
    func navigateToNextPage(delegate: ForgotPasswordThirdPageDelegate?)
}

class ForgotPasswordThirdPageRouter: ForgotPasswordThirdPageRouterProtocol {
    static func createModule(with delegate: ForgotPasswordPresenter?) -> UIViewController {
        let vc = ForgotPasswordThirdPageVC()
        let presenter = ForgotPasswordThirdPagePresenter()
        let interactor = ForgotPasswordThirdPageInteractor()
        let router = ForgotPasswordThirdPageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToNextPage(delegate: ForgotPasswordThirdPageDelegate?) {
        delegate?.forgotPasswordThirdPageFinished()
    }
}
