//
//  ForgotPasswordThirdPageInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordThirdPageInteractorProtocol: class {
    func passwordRecoveryStepThree(for token: String, code: String)
    func passwordRecoveryResendSMS(token: String)
}

class ForgotPasswordThirdPageInteractor: ForgotPasswordThirdPageInteractorProtocol {
    weak var presenter: ForgotPasswordThirdPageInteractorToPresenterProtocol!
    
    func passwordRecoveryStepThree(for token: String, code: String) {
        NetworkService.Auth.passwordRecoveryStepThree(token: token, code: code) { (result) in
            self.presenter.didPasswordRecoveryStepThree(with: result)
        }
    }
    
    func passwordRecoveryResendSMS(token: String) {
        NetworkService.Auth.passwordRecoveryResendSMS(token: token) { (result) in
            self.presenter.didPasswordRecoveryResendSMS(with: result)
        }
    }
}
