//
//  ForgotPasswordThirdPageViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol ForgotPasswordThirdPageViewInstaller: ViewInstaller {
    var smsCodeLabel: UILabel! { get set }
    var enterSmsCodeLabel: UILabel! { get set }
    var smsCodeTextField: SMSConfirmationTextField! { get set }
    var eyeButton: UIButton! { get set }
    var timerView: TimerView! { get set }
    var nextButton: NextButton! { get set }
}

extension ForgotPasswordThirdPageViewInstaller {
    func initSubviews() {
        smsCodeLabel = UILabel()
        smsCodeLabel.text = RS.lbl_sms_code.localized()
        smsCodeLabel.numberOfLines = 0
        smsCodeLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        smsCodeLabel.textAlignment = .center
        smsCodeLabel.textColor = .white
        
        enterSmsCodeLabel = UILabel()
        enterSmsCodeLabel.numberOfLines = 0
        enterSmsCodeLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        enterSmsCodeLabel.textAlignment = .center
        enterSmsCodeLabel.textColor = .white
        
        smsCodeTextField = SMSConfirmationTextField(cornerRadius: ForgotPasswordSize.TextField.height/2,
                                                    isSecureTextEntry: true)
        smsCodeTextField.textAlignment = .center
        smsCodeTextField.rightViewMode = .always
        
        eyeButton = UIButton()
        eyeButton.tintColor = .black
        eyeButton.setImage(UIImage(named: "img_eye")?.changeColor(.black), for: .normal)
        eyeButton.setImage(UIImage(named: "btn_eye_crossed_white")?.changeColor(.black), for: .selected)
        eyeButton.imageView?.contentMode = .scaleAspectFit
        
        timerView = TimerView(frame: CGRect(x: 0, y: 0, width: ApplicationSize.Timer.width, height: ApplicationSize.Timer.width))
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: ForgotPasswordSize.TextField.height/2)
    }
    
    func embedSubviews() {
        mainView.addSubview(smsCodeLabel)
        mainView.addSubview(enterSmsCodeLabel)
        mainView.addSubview(smsCodeTextField)
        smsCodeTextField.rightView = eyeButton
        mainView.addSubview(timerView)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        smsCodeLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(17))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(17))
            maker.top.equalToSuperview().offset(Adaptive.val(17))
        }
        
        enterSmsCodeLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(ForgotPasswordSize.padding)
            maker.trailing.equalToSuperview().offset(-ForgotPasswordSize.padding)
            maker.top.equalTo(smsCodeLabel.snp.bottom).offset(Adaptive.val(35))
        }
        
        smsCodeTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(enterSmsCodeLabel.snp.bottom).offset(Adaptive.val(20))
            maker.leading.trailing.equalTo(smsCodeLabel)
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        timerView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(smsCodeTextField.snp.bottom).offset(Adaptive.val(70))
            maker.centerX.equalToSuperview()
            maker.width.height.equalTo(ApplicationSize.Timer.width)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
            maker.bottom.equalTo(-Adaptive.val(50))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        eyeButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.EyeButton.size.width)
            maker.height.equalTo(Size.EyeButton.size.height)
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(50)
    
    struct EyeButton {
        static let size = Adaptive.size(50, 20)
    }
}
