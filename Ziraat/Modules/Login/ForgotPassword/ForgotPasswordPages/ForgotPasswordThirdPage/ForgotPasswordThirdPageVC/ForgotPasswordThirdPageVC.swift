//
//  ForgotPasswordThirdPageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordThirdPageVCProtocol: BaseViewControllerProtocol {
    func startTimer()
    func stopTimer()
    func set(buttonTitle: String)
    func showSmsSentLabel(text: String?)
}

class ForgotPasswordThirdPageVC: BaseViewController, ForgotPasswordThirdPageViewInstaller {
    var mainView: UIView { view }
    var smsCodeLabel: UILabel!
    var enterSmsCodeLabel: UILabel!
    var smsCodeTextField: SMSConfirmationTextField!
    var eyeButton: UIButton!
    var timerView: TimerView!
    var nextButton: NextButton!
    
    var presenter: ForgotPasswordThirdPagePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        smsCodeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        timerView.startTimer(with: timerValue)
        timerView.timerReachedItsEnd = {
            self.presenter.timerReachedItsEnd()
        }
        
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
        presenter.viewDidLoad()
        eyeButton.addTarget(self, action: #selector(textFieldEyeButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
    
    @objc private func textFieldEyeButtonClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        smsCodeTextField.isSecureTextEntry = !sender.isSelected
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        presenter.viewModel.userEntry.smsCode = sender.text ?? ""
    }
    
    @objc private func nextButtonClicked(_ sender: NextButton) {
        presenter.nextButtonClicked()
    }
}

extension ForgotPasswordThirdPageVC: ForgotPasswordThirdPageVCProtocol {
    func startTimer() {
        timerView.startTimer(with: timerValue)
    }
    
    func stopTimer() {
        timerView.stopTimer()
    }
    
    func set(buttonTitle: String) {
        nextButton.setTitle(buttonTitle, for: .normal)
    }
    
    func showSmsSentLabel(text: String?) {
        enterSmsCodeLabel.text = text
    }
}
