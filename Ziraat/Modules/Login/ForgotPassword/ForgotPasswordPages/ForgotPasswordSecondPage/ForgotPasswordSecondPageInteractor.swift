//
//  ForgotPasswordSecondPageInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordSecondPageInteractorProtocol: class {
    func passwordRecoveryStepTwo(for token: String, card: String, expiry: String)
}

class ForgotPasswordSecondPageInteractor: ForgotPasswordSecondPageInteractorProtocol {
    weak var presenter: ForgotPasswordSecondPageInteractorToPresenterProtocol!
    
    func passwordRecoveryStepTwo(for token: String, card: String, expiry: String) {
        NetworkService.Auth.passwordRecoveryStepTwo(token: token, card: card, expiry: expiry) { (result) in
            self.presenter.didPasswordRecoveryStepTwo(with: result)
        }
    }
}
