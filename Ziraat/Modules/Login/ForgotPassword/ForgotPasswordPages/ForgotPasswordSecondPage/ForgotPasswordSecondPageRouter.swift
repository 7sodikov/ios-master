//
//  ForgotPasswordSecondPageRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordSecondPageRouterProtocol: class {
    static func createModule(with delegate: ForgotPasswordPresenter?) -> UIViewController
    func navigateToNextPage(with messageResendPasswordData: LoginResendSMSResponse?, delegate: ForgotPasswordSecondPageDelegate?)
}

class ForgotPasswordSecondPageRouter: ForgotPasswordSecondPageRouterProtocol {
    static func createModule(with delegate: ForgotPasswordPresenter?) -> UIViewController {
        let vc = ForgotPasswordSecondPageVC()
        let presenter = ForgotPasswordSecondPagePresenter()
        let interactor = ForgotPasswordSecondPageInteractor()
        let router = ForgotPasswordSecondPageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToNextPage(with messageResendPasswordData: LoginResendSMSResponse?, delegate: ForgotPasswordSecondPageDelegate?) {
        delegate?.forgotPasswordSecondPageFinished(with: messageResendPasswordData)
    }
}
