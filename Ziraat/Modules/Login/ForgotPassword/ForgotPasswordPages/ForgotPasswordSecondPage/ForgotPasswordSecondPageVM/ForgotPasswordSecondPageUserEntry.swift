//
//  ForgotPasswordSecondPageUserEntry.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ForgotPasswordSecondPageUserEntry {
    var cardNumber: String?
    var cardExpiryDate: String?
}
