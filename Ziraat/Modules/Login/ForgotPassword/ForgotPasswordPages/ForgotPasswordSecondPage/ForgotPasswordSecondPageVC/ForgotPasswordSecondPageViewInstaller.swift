//
//  ForgotPasswordSecondPageViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import InputMask

protocol ForgotPasswordSecondPageViewInstaller: ViewInstaller {
    var cardInformationLabel: UILabel! { get set }
    var cardNumerLabel: UILabel! { get set }
    var cardNumberFieldListener: MaskedTextFieldDelegate! { get set }
    var cardNumberTextField: LoginTextField! { get set }
    var expiryDateLabel: UILabel! { get set }
    var expiryDateFieldListener: MaskedTextFieldDelegate! { get set }
    var expiryDateTextField: LoginTextField! { get set }
    var nextButton: NextButton! { get set }
}

extension ForgotPasswordSecondPageViewInstaller {
    func initSubviews() {
        cardInformationLabel = UILabel()
        cardInformationLabel.text = RS.lbl_personal_info.localized()
        cardInformationLabel.textColor = .white
        cardInformationLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))

        cardNumerLabel = UILabel()
        cardNumerLabel.text = RS.lbl_card_number.localized()
        cardNumerLabel.textColor = .white
        cardNumerLabel.font = EZFontType.medium.sfuiDisplay(size: ForgotPasswordSize.fieldTitleFontSize)
        
        cardNumberFieldListener = MaskedTextFieldDelegate()
        cardNumberFieldListener.affinityCalculationStrategy = .prefix
        cardNumberFieldListener.primaryMaskFormat = "[0000] [0000] [0000] [0000]"
        cardNumberTextField = LoginTextField(with: "XXXX XXXX XXXX XXXX", keyboard: .numberPad, cornerRadius: ForgotPasswordSize.TextField.height/2, isSecureTextEntry: false)
        cardNumberTextField.delegate = cardNumberFieldListener
        
        expiryDateLabel = UILabel()
        expiryDateLabel.text = RS.lbl_card_expiry.localized()
        expiryDateLabel.textColor = .white
        expiryDateLabel.font = EZFontType.medium.sfuiDisplay(size: ForgotPasswordSize.fieldTitleFontSize)
        
        expiryDateFieldListener = MaskedTextFieldDelegate()
        expiryDateFieldListener.primaryMaskFormat = "[00]/[00]"
        expiryDateTextField = LoginTextField(with: "MM/YY", keyboard: .numberPad, cornerRadius: ForgotPasswordSize.TextField.height/2, isSecureTextEntry: false)
        expiryDateTextField.delegate = expiryDateFieldListener
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: ForgotPasswordSize.TextField.height/2)
    }
    
    func embedSubviews() {
        mainView.addSubview(cardInformationLabel)
        mainView.addSubview(cardNumerLabel)
        mainView.addSubview(cardNumberTextField)
        mainView.addSubview(expiryDateLabel)
        mainView.addSubview(expiryDateTextField)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        cardInformationLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(ForgotPasswordSize.padding)
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
        }
        
        cardNumerLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(cardInformationLabel.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.equalTo(cardInformationLabel)
        }
        
        cardNumberTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(cardNumerLabel.snp.bottom).offset(ForgotPasswordSize.fieldTitleSpace)
            maker.left.right.equalTo(cardInformationLabel)
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        expiryDateLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(cardNumberTextField.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.equalTo(cardInformationLabel)
        }
        
        expiryDateTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(expiryDateLabel.snp.bottom).offset(ForgotPasswordSize.fieldTitleSpace)
            maker.left.equalTo(cardInformationLabel)
            maker.width.equalTo(Adaptive.val(100))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
            maker.bottom.equalTo(-Adaptive.val(50))
            maker.height.equalTo(ForgotPasswordSize.TextField.height)
        }
    }
}
