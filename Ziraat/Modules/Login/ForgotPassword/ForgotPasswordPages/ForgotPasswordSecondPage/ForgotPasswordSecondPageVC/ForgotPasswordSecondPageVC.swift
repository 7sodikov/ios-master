//
//  ForgotPasswordSecondPageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol ForgotPasswordSecondPageVCProtocol: BaseViewControllerProtocol {
    
}

class ForgotPasswordSecondPageVC: BaseViewController, ForgotPasswordSecondPageViewInstaller {
    var mainView: UIView { view }
    var cardInformationLabel: UILabel!
    var cardNumerLabel: UILabel!
    var cardNumberFieldListener: MaskedTextFieldDelegate!
    var cardNumberTextField: LoginTextField!
    var expiryDateLabel: UILabel!
    var expiryDateFieldListener: MaskedTextFieldDelegate!
    var expiryDateTextField: LoginTextField!
    var nextButton: NextButton!
    
    var presenter: ForgotPasswordSecondPagePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        cardNumberFieldListener.listener = self
        expiryDateFieldListener.listener = self
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
    
    @objc private func nextButtonClicked(_ sender: NextButton) {
        presenter.nextButtonClicked()
    }
}

extension ForgotPasswordSecondPageVC: ForgotPasswordSecondPageVCProtocol {
    
}

extension ForgotPasswordSecondPageVC: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        if textField == cardNumberTextField {
            presenter.viewModel.userEntry.cardNumber = value
            
        } else if textField == expiryDateTextField {
            presenter.viewModel.userEntry.cardExpiryDate = value
        }
    }
}

