//
//  ForgotPasswordSecondPagePresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordSecondPageDelegate: class {
    func forgotPasswordSecondPageFinished(with messageResendPasswordData: LoginResendSMSResponse?)
}

protocol ForgotPasswordSecondPagePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ForgotPasswordSecondPageVM { get }
    func nextButtonClicked()
}

protocol ForgotPasswordSecondPageInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryStepTwo(with response: ResponseResult<LoginResendSMSResponse, AppError>)
}

class ForgotPasswordSecondPagePresenter: ForgotPasswordSecondPagePresenterProtocol {
    weak var view: ForgotPasswordSecondPageVCProtocol!
    var interactor: ForgotPasswordSecondPageInteractorProtocol!
    var router: ForgotPasswordSecondPageRouterProtocol!
    weak var delegate: ForgotPasswordSecondPageDelegate?
    var loginInitData: LoginInitResponse?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ForgotPasswordSecondPageVM()
    
    func nextButtonClicked() {
        guard let cardNumber = viewModel.userEntry.cardNumber, cardNumber.count == 16,
              let cardExpiryDate = viewModel.userEntry.cardExpiryDate, cardExpiryDate.count == 4 else {
            view.showError(message: RS.lbl_fill_all_fields.localized())
            return
        }
        view.preloader(show: true)
        interactor.passwordRecoveryStepTwo(for: loginInitData?.token ?? "",
                                           card: cardNumber,
                                           expiry: cardExpiryDate)
    }
    
    // Private property and methods
    
}

extension ForgotPasswordSecondPagePresenter: ForgotPasswordSecondPageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didPasswordRecoveryStepTwo(with response: ResponseResult<LoginResendSMSResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            viewModel.messageResendPasswordData = result.data
            router.navigateToNextPage(with: result.data, delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
