//
//  ForgotPasswordViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol ForgotPasswordVCInstaller: ViewInstaller {
    var backgroundImageView: UIImageView! { get set }
    var forgotPasswordLabel: UILabel! { get set }
    var pageControl: ForgotPasswordPageControl! { get set }
    var pageControllerCoverView: UIView! { get set }
    var pageController: PageController! { get set }
}

extension ForgotPasswordVCInstaller {
    func initSubviews() {
        backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "img_background")
        
        forgotPasswordLabel = UILabel()
        forgotPasswordLabel.text = RS.lbl_forgot_password.localized()
        forgotPasswordLabel.font = EZFontType.bold.sfProDisplay(size: Adaptive.val(17))
        forgotPasswordLabel.textColor = .white
        
        pageControl = ForgotPasswordPageControl()
        pageControl.isBlack = true
//        pageControl.layer.borderWidth = 1
//        pageControl.layer.borderColor = UIColor.black.cgColor
        
        pageControllerCoverView = UIView()
        
        pageController = PageController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImageView)
        mainView.addSubview(forgotPasswordLabel)
        mainView.addSubview(pageControl)
        mainView.addSubview(pageControllerCoverView)
    }
    
    func addSubviewsConstraints() {
        backgroundImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        forgotPasswordLabel.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(44))
            maker.centerX.equalToSuperview()
        }
        
        pageControl.snp.remakeConstraints { (maker) in
            maker.top.equalTo(forgotPasswordLabel.snp.bottom).offset(Adaptive.val(20))
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
            maker.height.equalTo(ForgotPasswordSize.PageControl.height)
        }
        
        pageControllerCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(pageControl.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.bottom.equalToSuperview()
        }
    }
}

struct ForgotPasswordSize {
    static let padding = Adaptive.val(20)
    static let interItemSpace = Adaptive.val(20)
    static let fieldTitleSpace = Adaptive.val(8)
    static let fieldTitleFontSize = Adaptive.val(17)
    
    struct PageControl {
        static let height = Adaptive.val(45)
    }
    
    struct TextField {
        static let height = Adaptive.val(50)
    }
}
