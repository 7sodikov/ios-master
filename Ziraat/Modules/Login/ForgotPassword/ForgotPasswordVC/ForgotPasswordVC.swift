//
//  ForgotPasswordVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

enum ForgotPasswordCellType: Int {
    case phoneNumber
    case cardNumber
    case smsConfirm
    case newPassword
}

protocol ForgotPasswordVCProtocol: BaseViewControllerProtocol {    
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection)
    func getController(for pageIndex: Int) -> Any?
}

class ForgotPasswordVC: BaseViewController, ForgotPasswordVCInstaller {
    var mainView: UIView { view }
    var backgroundImageView: UIImageView!
    var forgotPasswordLabel: UILabel!
    var pageControl: ForgotPasswordPageControl!
    var pageControllerCoverView: UIView!
    var pageController: PageController!
    var controllers: [UIViewController] = []
    
    var cellTypes: [ForgotPasswordCellType] = [.phoneNumber, .cardNumber,
                                               .smsConfirm, .newPassword]
    
    var presenter: ForgotPasswordPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        pageControl.itemsCount = cellTypes.count
                
        controllers.append(contentsOf: [ForgotPasswordFirstPageRouter.createModule(with: presenter as? ForgotPasswordPresenter),
                                        ForgotPasswordSecondPageRouter.createModule(with: presenter as? ForgotPasswordPresenter),
                                        ForgotPasswordThirdPageRouter.createModule(with: presenter as? ForgotPasswordPresenter),
                                        ForgotPasswordForthPageRouter.createModule()])
        
        pageController.open(in: self,
                            parentView: pageControllerCoverView,
                            pages: controllers, isSwipeEnabled: false)
        pageController.pageControllerDelegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        if parent == nil {
            if let vc = controllers.last as? ForgotPasswordForthPageVC {
                vc.dismissTipView()
            }
        }
    }
}

extension ForgotPasswordVC: ForgotPasswordVCProtocol {
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection) {
        pageControl.currentItem = pageIndex
        pageController.scroll(to: pageIndex, direction: direction)
    }
    
    func getController(for pageIndex: Int) -> Any? {
        if pageIndex >= controllers.count {
            return nil
        }
        return controllers[pageIndex]
    }
}

extension ForgotPasswordVC: PageControllerDelegate {
    func pageController(didScrollToOffsetX offsetX: CGFloat, percent: CGFloat) {
        
    }
    
    func pageController(didMoveToIndex index: Int) {
        pageControl.currentItem = index
    }
}
