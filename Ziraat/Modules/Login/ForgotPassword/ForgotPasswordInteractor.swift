//
//  ForgotPasswordInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordInteractorProtocol: class {
    
}

class ForgotPasswordInteractor: ForgotPasswordInteractorProtocol {
    weak var presenter: ForgotPasswordInteractorToPresenterProtocol!
    
}
