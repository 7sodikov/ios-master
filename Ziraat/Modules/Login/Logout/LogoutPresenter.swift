//
//  LogoutPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LogoutPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LogoutVM { get }
    func navigateToLogininit()
}

protocol LogoutInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class LogoutPresenter: LogoutPresenterProtocol {
    weak var view: LogoutVCProtocol!
    var interactor: LogoutInteractorProtocol!
    var router: LogoutRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LogoutVM()
    
    // Private property and methods
    
    func navigateToLogininit() {
        router.navigateToLoginInit()
    }
}

extension LogoutPresenter: LogoutInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
