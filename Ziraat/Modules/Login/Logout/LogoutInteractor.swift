//
//  LogoutInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LogoutInteractorProtocol: class {
    
}

class LogoutInteractor: LogoutInteractorProtocol {
    weak var presenter: LogoutInteractorToPresenterProtocol!
    
}
