//
//  LogoutRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LogoutRouterProtocol: AnyObject {
    static func createModule() -> UIViewController
    func navigateToLoginInit()
}

class LogoutRouter: LogoutRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = LogoutVC()
        let presenter = LogoutPresenter()
        let interactor = LogoutInteractor()
        let router = LogoutRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToLoginInit() {
        appDelegate.router.navigate(to: .loginInit)
    }
}
