//
//  LogoutVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LogoutVCProtocol: class {
    
}

class LogoutVC: UIViewController, LogoutViewInstaller {
    var backImageView: UIImageView!
    var ziraatLogoImageView: UIImageView!
    var gratingLabel: UILabel!
    var reenterButton: NextButton!
    var mainView: UIView { view }
    
    var presenter: LogoutPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        reenterButton.addTarget(self, action: #selector(reenterButtonClicked), for: .touchUpInside)
    }
    
    @objc func reenterButtonClicked() {
        UDManager.clear()
        LocalizationManager.language = .english
        presenter.navigateToLogininit()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}

extension LogoutVC: LogoutVCProtocol {
    
}
