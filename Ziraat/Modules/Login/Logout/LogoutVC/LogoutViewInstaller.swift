//
//  LogoutViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LogoutViewInstaller: ViewInstaller {
    var backImageView: UIImageView! { get set }
    var ziraatLogoImageView: UIImageView! { get set }
    var gratingLabel: UILabel! { get set }
    var reenterButton: NextButton! { get set }
}

extension LogoutViewInstaller {
    func initSubviews() {
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_background")
        
        ziraatLogoImageView = UIImageView()
        ziraatLogoImageView.image = UIImage(named: "img_logo_with_text")
        
        gratingLabel = UILabel()
        gratingLabel.text = RS.lbl_usage.localized()
        gratingLabel.font = .gothamNarrow(size: 16, .bold)
        gratingLabel.textColor = .white
        gratingLabel.textAlignment = .center
        gratingLabel.numberOfLines = 0
        gratingLabel.lineBreakMode = .byWordWrapping
        
        reenterButton = NextButton.systemButton(with: RS.lbl_reenter.localized(), cornerRadius: Size.buttonHeight/2)
        reenterButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(ziraatLogoImageView)
        mainView.addSubview(gratingLabel)
        mainView.addSubview(reenterButton)
    }
    
    func addSubviewsConstraints() {
        backImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        ziraatLogoImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(156))
            maker.height.equalTo(Adaptive.val(45))
            maker.centerX.equalToSuperview()
            maker.top.equalToSuperview().offset(Adaptive.val(44))
        }
        
        gratingLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.top.equalTo(ziraatLogoImageView.snp.bottom).offset(Adaptive.val(60))
        }
        
        reenterButton.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.height.equalTo(Size.buttonHeight)
            maker.top.equalTo(gratingLabel.snp.bottom).offset(Adaptive.val(248))
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(50)
    static let paddingLeftRight = Adaptive.val(15)
}
