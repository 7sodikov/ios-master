//
//  LoginVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import EasyTipView

protocol LoginVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
    func showNewReceivedCaptcha()
    func setTextField()
    func checkRememberMeSwitch(isOn: Bool, userName: String?)
}

class LoginVC: BaseViewController, LoginViewInstaller {
    var mainView: UIView { view }
    var backgroundImageView: UIImageView!
    var imageView: UIImageView!
    var welcomeLabel: UILabel!
    var segmentedControl: LoginSegmentedControl!
    var usernameTextField: LoginTextField!
    var rememberMeLabel: UILabel!
    var rememberMeSwitch: UISwitch!
    var switchStackView: UIStackView!
    var passwordView: UIView!
    var hideButton: UIButton!
//    var captchaView: CaptchaView!
//    var captchaStackView: UIStackView!
    var passwordTextField: LoginTextField!
    var forgotPasswordButton: UIButton!
//    var captchaTextField: LoginTextField!
    var nextButton: NextButton!
    var userNameTipView: EasyTipView!
    var fillUpView: FillFieldUpView!
    var fillDownView: FillFieldUpView!
    
    var iconClick = true
    
    private var rememberMeFlag: Bool = false

    var presenter: LoginPresenterProtocol!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.navigationBar.topItem?.title = " "
//        let logo = UIImage(named: "img_logo_with_text")
//        let imageView = UIImageView(image:logo)
//        self.navigationItem.titleView = imageView
        
        setupSubviews()
        
        presenter.checkRememberMe()
        
//        if KeychainManager.userName == nil {
//            rememberMeSwitch.isOn = false
//        } else {
//            usernameTextField.text = KeychainManager.userName
//            rememberMeSwitch.isOn = true
//        }

//        rememberMeFlag = UDManager.rememberUser
//        rememberMeSwitch.isOn = rememberMeFlag
//
//        if rememberMeFlag {
//            let userName = UDManager.userName
//            usernameTextField.text = userName
//        }
        
        usernameTextField.delegate = self
        
        presenter.viewDidLoad()
        
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        rememberMeSwitch.addTarget(self, action: #selector(toggleCheckBox(_:)), for: .valueChanged)
        
        forgotPasswordButton.addTarget(self, action: #selector(forgotPasswordClicked(_:)), for: .touchUpInside)
//        captchaView.reloadCaptchaButton.addTarget(self, action: #selector(renewCaptchaClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
        hideButton.addTarget(self, action: #selector(hideButtonClicked(_:)), for: .touchUpInside)
        
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//        captchaTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
//        usernameTextField.text = "998997057826"
//        passwordTextField.text = "357357"
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    func checkRememberMeSwitch(isOn: Bool, userName: String?) {
        rememberMeSwitch.isOn = isOn
        usernameTextField.text = userName
    }
    
    func setTextField() {
        presenter.saveUserName(userName: usernameTextField.text ?? "", isOn: rememberMeSwitch.isOn)
    }

    @objc func toggleCheckBox(_ sender: UIButton) {
        rememberMeFlag = !rememberMeFlag
        presenter.toggleMessage(rememberMe: rememberMeFlag, userName: usernameTextField.text ?? "")
    }

    @objc private func hideButtonClicked(_ sender: UIButton) {
        if(iconClick == true) {
                    passwordTextField.isSecureTextEntry = false
            if #available(iOS 13.0, *) {
                hideButton.setImage(UIImage(named: "img_eye")?.withTintColor(.black), for: .normal)
            } else {
                hideButton.imageView?.tintColor = .black
            }
        } else {
            if #available(iOS 13.0, *) {
                hideButton.setImage(UIImage(named: "btn_eye")?.withTintColor(.black), for: .normal)
            } else {
                hideButton.imageView?.tintColor = .black
            }
            passwordTextField.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    @objc private func renewCaptchaClicked(_ sender: UIButton) {
        presenter.renewCaptchaClicked()
    }
    
    @objc private func nextButtonClicked(_ sender: NextButton) {
        if let userName = usernameTextField.text,
           let password = passwordTextField.text,
           !userName.isEmpty,
           !password.isEmpty {
            
            presenter.nextButtonClicked(userName: userName, password: password)
            KeychainManager.userName = rememberMeSwitch.isOn ? userName : nil
        }
        fillDownView.isHidden = !usernameTextField.text!.isEmpty
        fillUpView.isHidden = !passwordTextField.text!.isEmpty 
    
//        
//        if passwordTextField.text?.isEmpty == true {
//            userNameTipView.show(forView: passwordTextField)
//        }
        
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        usernameTextField.text = usernameTextField.text?.trimmingCharacters(in: .whitespaces)
        passwordTextField.text = passwordTextField.text?.trimmingCharacters(in: .whitespaces)
//        captchaTextField.text = captchaTextField.text?.trimmingCharacters(in: .whitespaces)
        nextButton.isUserInteractionEnabled = true//isFieldsValied()
    }
    
    @objc private func forgotPasswordClicked(_ sender: UIButton) {
        presenter.forgotPasswordClicked(with: navigationController as Any)
    }
    
    private func isFieldsValied() -> Bool {
        usernameTextField.text = usernameTextField.text?.trimmingCharacters(in: .whitespaces)
        passwordTextField.text = passwordTextField.text?.trimmingCharacters(in: .whitespaces)
//        captchaTextField.text = captchaTextField.text?.trimmingCharacters(in: .whitespaces)
        return (usernameTextField.text?.isEmpty != true &&
            passwordTextField.text?.isEmpty != true)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: true)
    }
}

extension LoginVC: LoginVCProtocol, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 12
    }
    
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func showNewReceivedCaptcha() {
//        captchaView.captchaImageView.image = presenter.loginInitData?.captchaImg.imageFromBase64
    }
}
