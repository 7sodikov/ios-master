//
//  LoginViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import EasyTipView

protocol LoginViewInstaller: ViewInstaller {
    var backgroundImageView: UIImageView! { get set }
    var imageView: UIImageView! { get set }
    var welcomeLabel: UILabel! { get set }
    var segmentedControl: LoginSegmentedControl! { get set }
    var usernameTextField: LoginTextField! { get set }
    var rememberMeLabel: UILabel! { get set }
    var rememberMeSwitch: UISwitch! { get set }
    var switchStackView: UIStackView! { get set }
    var passwordView: UIView! { get set }
    var passwordTextField: LoginTextField! { get set }
    var hideButton: UIButton! { get set }
    var forgotPasswordButton: UIButton! { get set }
//    var captchaView: CaptchaView! { get set }
//    var captchaStackView: UIStackView! { get set }
//    var captchaTextField: LoginTextField! { get set }
    var nextButton: NextButton! { get set }
    var userNameTipView: EasyTipView! { get set }
    var fillUpView: FillFieldUpView! { get set }
    var fillDownView: FillFieldUpView! { get set }
}

extension LoginViewInstaller {
    func initSubviews() {
        backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "img_background")
        
        imageView = UIImageView()
        imageView.image = UIImage(named: "img_logo_with_text")
        imageView.contentMode = .scaleAspectFit
        
        welcomeLabel = UILabel()
        welcomeLabel.text = RS.lbl_welcome.localized()
        welcomeLabel.font = EZFontType.medium.gothamText(size: Adaptive.val(16))
        welcomeLabel.textColor = .white
        welcomeLabel.numberOfLines = 0
        welcomeLabel.textAlignment = .center
        
        segmentedControl = LoginSegmentedControl(frame: .zero)
        
        usernameTextField = LoginTextField(with: RS.lbl_phone_number.localized(), keyboard: .numberPad, cornerRadius: Size.TextField.height/2, isSecureTextEntry: false)
        usernameTextField.font = EZFontType.medium.gothamText(size: Adaptive.val(16))
        
        rememberMeLabel = UILabel()
        rememberMeLabel.font = .gothamNarrow(size: 13, .book) //EZFontType.medium.gothamText(size: Adaptive.val(13))
        rememberMeLabel.textColor = .white
        rememberMeLabel.textAlignment = .right
        rememberMeLabel.contentMode = .center
        rememberMeLabel.text = RS.lbl_remember_me.localized()
        
        rememberMeSwitch = UISwitch()
        rememberMeSwitch.isOn = false
        rememberMeSwitch.tintColor = .white
        
        switchStackView = UIStackView()
        switchStackView.alignment = .center
        switchStackView.spacing = Adaptive.val(8)
        switchStackView.axis = .horizontal
        
        passwordView = UIView()
        passwordView.layer.cornerRadius = Size.TextField.height/2
        passwordView.backgroundColor = .white
        
        passwordTextField = LoginTextField(with: RS.placeho_pass.localized(), cornerRadius: Size.TextField.height/2, isSecureTextEntry: true)
        passwordTextField.autocapitalizationType = .none
        passwordTextField.isSecureTextEntry = true
        passwordTextField.font = EZFontType.medium.gothamText(size: Adaptive.val(16))
        
        hideButton = UIButton()
        hideButton.tintColor = .black
//        hideButton.setImage(UIImage(named: "img_eye"), for: .normal)
        let eyeImageName = hideButton.isSelected ? "img_eye" : "btn_eye"
        let icon = UIImage(named: eyeImageName)?.withRenderingMode(.alwaysTemplate)
        hideButton.setImage(icon, for: .normal)
        
        forgotPasswordButton = UIButton()
        func attributedStr(with color: UIColor) -> NSAttributedString {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.gothamNarrow(size: 13, .book),
                .foregroundColor: color,
                .underlineStyle: NSUnderlineStyle.single.rawValue]
            return NSMutableAttributedString(string: RS.btn_forgot_pass.localized(), attributes: attributes)
        }
        let normalAttributeString = attributedStr(with: .white)
        let highlightedAttributeString = attributedStr(with: UIColor.white.withAlphaComponent(0.5))
        forgotPasswordButton.setAttributedTitle(normalAttributeString, for: .normal)
        forgotPasswordButton.setAttributedTitle(highlightedAttributeString, for: .highlighted)
        
//        captchaView = CaptchaView()
//        captchaView.backgroundColor = .white
//        captchaView.layer.cornerRadius = Size.TextField.height/2
//
//        captchaStackView = UIStackView()
//        captchaStackView.axis = .horizontal
//        captchaStackView.distribution = .fillEqually
//        captchaStackView.alignment = .fill
//        captchaStackView.spacing = Adaptive.val(15)
//
//        captchaTextField = LoginTextField(with: RS.placeho_enter_captcha.localized(), cornerRadius: Size.TextField.height/2, isSecureTextEntry: false)
//        captchaTextField.autocapitalizationType = .none
//        captchaTextField.font = EZFontType.medium.gothamText(size: Adaptive.val(16))

        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: Size.TextField.height/2)
        nextButton.isUserInteractionEnabled = false
        nextButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        
        var tipPreferences = EasyTipView.Preferences()
        if let font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(13)) {
            tipPreferences.drawing.font = font
        }
        tipPreferences.drawing.foregroundColor = .white
        tipPreferences.drawing.backgroundColor = ColorConstants.highlightedGray
        tipPreferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        tipPreferences.drawing.arrowHeight = Adaptive.val(10)
        tipPreferences.drawing.arrowWidth = Adaptive.val(20)
        tipPreferences.drawing.borderWidth = Adaptive.val(5)
        tipPreferences.drawing.borderColor = UIColor.white.withAlphaComponent(0.7)
        
        userNameTipView = EasyTipView(text: "Пожалуйста, введите правильный номер", preferences: tipPreferences)
        
        fillUpView = FillFieldUpView.fillView(with: "Пожалуйста, введите правильный пароль", arrow: .up)
        fillUpView.isHidden = true
        
        fillDownView = FillFieldUpView.fillView(with: "Пожалуйста, введите правильный номер", arrow: .down)
        fillDownView.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImageView)
        mainView.addSubview(imageView)
        mainView.addSubview(welcomeLabel)
        mainView.addSubview(segmentedControl)
        mainView.addSubview(usernameTextField)
        
        switchStackView.addArrangedSubview(rememberMeLabel)
        switchStackView.addArrangedSubview(rememberMeSwitch)
        mainView.addSubview(switchStackView)
        
        mainView.addSubview(passwordView)
        passwordView.addSubview(passwordTextField)
        passwordView.addSubview(hideButton)
        mainView.addSubview(forgotPasswordButton)
//        mainView.addSubview(captchaTextField)
        mainView.addSubview(nextButton)
//        mainView.addSubview(captchaStackView)
//        captchaStackView.addArrangedSubview(captchaView)
//        captchaStackView.addArrangedSubview(captchaTextField)
        
        mainView.addSubview(fillUpView)
        mainView.addSubview(fillDownView)
    }
    
    func addSubviewsConstraints() {
        backgroundImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        imageView.snp.remakeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.top.equalToSuperview().offset(Adaptive.val(44))
        }
        
        welcomeLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(15))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            maker.top.equalTo(imageView.snp.bottom).offset(Adaptive.val(16))
        }
        
        segmentedControl.snp.remakeConstraints { (maker) in
            maker.left.equalToSuperview().offset(Adaptive.val(20))
            maker.right.equalToSuperview().inset(Adaptive.val(20))
            maker.top.equalTo(welcomeLabel.snp.bottom).offset(Adaptive.val(22))
        }
        
        usernameTextField.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.TextField.height)
            maker.left.equalToSuperview().offset(Adaptive.val(15))
            maker.right.equalToSuperview().inset(Adaptive.val(15))
            maker.top.equalTo(segmentedControl.snp.bottom).offset(Adaptive.val(55))
        }
        
        switchStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(usernameTextField.snp.bottom).offset(Size.interItemSpace/2)
            maker.trailing.equalToSuperview().offset(Adaptive.val(-15))
        }
        
        passwordView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(switchStackView.snp.bottom).offset(Size.interItemSpace/2)
            maker.left.right.height.equalTo(usernameTextField)
        }
        
        passwordTextField.snp.remakeConstraints { (maker) in
            maker.top.leading.bottom.equalTo(passwordView)
            maker.trailing.equalTo(hideButton.snp.leading)
        }
        
        hideButton.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(passwordView)
            maker.trailing.equalTo(passwordView.snp.trailing).offset(-Adaptive.val(21))
            maker.height.equalTo(Adaptive.val(25))
            maker.width.equalTo(Adaptive.val(25))
        }
        
//        captchaStackView.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(passwordTextField.snp.bottom).offset(Size.interItemSpace)
//            maker.leading.equalToSuperview().offset(Size.interItemSpace)
//            maker.trailing.equalToSuperview().offset(-Size.interItemSpace)
//            maker.height.equalTo(Adaptive.val(54))
//        }
                
        nextButton.snp.remakeConstraints { (maker) in
            maker.left.right.height.equalTo(usernameTextField)
            maker.top.equalTo(passwordTextField.snp.bottom).offset(Adaptive.val(50))
        }
        
        forgotPasswordButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(nextButton.snp.bottom).offset(Adaptive.val(15))
            maker.trailing.equalTo(passwordView.snp.trailing)
        }
        
        fillUpView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(passwordTextField.snp.bottom)
            maker.leading.trailing.equalTo(passwordView)
            maker.height.equalTo(Adaptive.val(46))
        }
        
        fillDownView.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(usernameTextField.snp.top)
            maker.leading.trailing.equalTo(usernameTextField)
            maker.height.equalTo(Adaptive.val(46))
        }
        
//        rememberMeSwitch.snp.remakeConstraints { (maker) in
//            maker.width.equalTo(Adaptive.val(35))
//            maker.height.equalTo(Adaptive.val(20))
//        }
//
//        rememberMeLabel.snp.remakeConstraints { (maker) in
//            maker.height.equalTo(Adaptive.val(20))
//        }
    }
}

fileprivate struct Size {
    struct TextField {
        static let height = Adaptive.val(50)
    }
    
    static let interItemSpace = Adaptive.val(20)
}
