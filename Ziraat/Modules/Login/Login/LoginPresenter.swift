//
//  LoginPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoginPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoginVM { get }
    var loginInitData: LoginInitResponse? { get }
    func viewDidLoad()
    func nextButtonClicked(userName: String, password: String)
    func renewCaptchaClicked()
    func forgotPasswordClicked(with navigationCtrl: Any)
    func toggleMessage(rememberMe: Bool, userName: String)
    func saveUserName(userName: String, isOn: Bool)
    func checkRememberMe()
}

protocol LoginInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didLoginInit(with response: ResponseResult<LoginInitResponse, AppError>)
    func didLogin(with response: ResponseResult<AuthLoginResponse, AppError>, userName: String, password: String)
    func didResendCaptcha(with response: ResponseResult<LoginInitResponse, AppError>)
    
}

class LoginPresenter: LoginPresenterProtocol {
    
    weak var view: LoginVCProtocol!
    var loginInitData: LoginInitResponse?
    var interactor: LoginInteractorProtocol!
    var router: LoginRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoginVM()
    
    func viewDidLoad() {
        interactor.loginInit()
    }
    
    func nextButtonClicked(userName: String, password: String) {
        guard let encryptedUserName = RSAEncrypter.encrypt(text: userName),
              let encryptedPassword = RSAEncrypter.encrypt(text: password) else {
            view.showError(message: RS.al_msg_smth_went_wrong_error.localized())
            return
        }
        
        view.preloader(show: true)
        view.setTextField()
        interactor.login(userName: encryptedUserName, password: encryptedPassword,
                         token: loginInitData?.token ?? "")
    }
    
    func renewCaptchaClicked() {
        if let token = loginInitData?.token {
            interactor.resendCaptcha(for: token)
        } else {
            interactor.loginInit()
        }
    }
    
    func forgotPasswordClicked(with navigationCtrl: Any) {
        router.navigateToResetPasswordVC(in: navigationCtrl)
    }
    
    func toggleMessage(rememberMe: Bool, userName: String) {
        UserDefaults.standard.set(rememberMe, forKey: "rememberMe")
        if rememberMe {
            UserDefaults.standard.set(userName, forKey:"userName")
        } else {
            UserDefaults.standard.removeObject(forKey: "userName")
        }
    }
    
    func saveUserName(userName: String, isOn: Bool) {
        if userName != nil && isOn == true {
            KeychainManager.userName = userName
        } else {
            KeychainManager.userName = nil
        }
    }
    
    func checkRememberMe() {
        if KeychainManager.userName == nil {
            view.checkRememberMeSwitch(isOn: false, userName: nil)
        } else {
            view.checkRememberMeSwitch(isOn: true, userName: KeychainManager.userName)
        }
    }
    
    // Private property and methods
    
}

extension LoginPresenter: LoginInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didLoginInit(with response: ResponseResult<LoginInitResponse, AppError>) {
        switch response {
        case let .success(result):
            loginInitData = result.data
            KeychainManager.rsaPublicKey = result.data.pubKey
            view.showNewReceivedCaptcha()
        case .failure:
            break
        }
    }
    
    func didLogin(with response: ResponseResult<AuthLoginResponse, AppError>,
                  userName: String, password: String) {
        view.preloader(show: false)
        
        switch response {
        case let .success(result):
            if result.httpStatusCode == 202 {
//                UDManager.sessionToken = result.data.token
//                router.navigateToHomeVC()
                
            } else { // 200
                router.navigateLoginConfirmationVC(in: view.navigationCtrl,
                                                   token: loginInitData?.token ?? "",
                                                   userName: userName, password: password, data: result.data)
            }
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didResendCaptcha(with response: ResponseResult<LoginInitResponse, AppError>) {
        switch response {
        case let .success(result):
            loginInitData = result.data
            KeychainManager.rsaPublicKey = result.data.pubKey
            view.showNewReceivedCaptcha()
            
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
