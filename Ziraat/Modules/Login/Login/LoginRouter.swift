//
//  LoginRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginRouterProtocol: class {
    static func createModule() -> UIViewController
    static func createModuleInNavController() -> UIViewController
    func navigateLoginConfirmationVC(in navigationCtrl: Any,
                                     token: String,
                                     userName: String, password: String, data: AuthLoginResponse)
    func navigateToResetPasswordVC(in navigationCtrl: Any)
    func navigateToHomeVC()
}

class LoginRouter: LoginRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = LoginVC()
        let presenter = LoginPresenter()
        let interactor = LoginInteractor()
        let router = LoginRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    static func createModuleInNavController() -> UIViewController {
        let navCtrl = NavigationController(rootViewController: createModule())
        return navCtrl
    }
    
    func navigateLoginConfirmationVC(in navigationCtrl: Any,
                                     token: String,
                                     userName: String, password: String, data: AuthLoginResponse) {
        let confirmationCtrl = LoginConfirmationRouter.createModule(token: token,
                                                                    userName: userName,
                                                                    password: password, data: data)
        
        (navigationCtrl as? UINavigationController)?.pushViewController(confirmationCtrl, animated: true)
    }
    
    func navigateToResetPasswordVC(in navigationCtrl: Any) {
        let confirmationCtrl = ForgotPasswordRouter.createModule()
        (navigationCtrl as? UINavigationController)?.pushViewController(confirmationCtrl, animated: true)
    }
    
    func navigateToHomeVC() {
        appDelegate.router.navigate(to: .mainPage)
    }
}
