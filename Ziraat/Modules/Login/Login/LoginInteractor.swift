//
//  LoginInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoginInteractorProtocol: AnyObject {
    func loginInit()
    func login(userName: String, password: String, token: String)
    func resendCaptcha(for token: String)
}

class LoginInteractor: LoginInteractorProtocol {
    weak var presenter: LoginInteractorToPresenterProtocol!
    
    func loginInit() {
        NetworkService.Auth.loginInit { [weak self] (result) in
            self?.presenter.didLoginInit(with: result)
        }
    }
    
    func login(userName: String, password: String, token: String) {
        NetworkService.Auth.login(token: token, username: userName,
                                  password: password) { (result) in
                                    self.presenter.didLogin(with: result, userName: userName, password: password)
        }
    }
    
    func resendCaptcha(for token: String) {
        NetworkService.Auth.loginResendCaptcha(token: token) { (result) in
            self.presenter.didResendCaptcha(with: result)
        }
    }
    
}
