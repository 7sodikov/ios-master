//
//  JailbreakBlockerVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol JailbreakBlockerVCProtocol: class {
    
}

class JailbreakBlockerVC: UIViewController, JailbreakBlockerViewInstaller {
    var mainView: UIView { view }
    var backgroundView: UIImageView!
    var warningIconCoverView: UIView!
    var warningIconImageView: UIImageView!
    var warningLabel: UILabel!
    
    var presenter: JailbreakBlockerPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
    }
}

extension JailbreakBlockerVC: JailbreakBlockerVCProtocol {
    
}
