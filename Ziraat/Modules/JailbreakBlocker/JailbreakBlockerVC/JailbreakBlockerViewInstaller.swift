//
//  JailbreakBlockerViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol JailbreakBlockerViewInstaller: ViewInstaller {
    var backgroundView: UIImageView! { get set }
    var warningIconCoverView: UIView! { get set }
    var warningIconImageView: UIImageView! { get set }
    var warningLabel: UILabel! { get set }
}

extension JailbreakBlockerViewInstaller {
    func initSubviews() {
        backgroundView = UIImageView()
        backgroundView.image =  UIImage(named: "img_background")
        backgroundView.contentMode = .scaleAspectFill
        
        warningIconCoverView = UIView()
        warningIconCoverView.backgroundColor = .white
        warningIconCoverView.layer.cornerRadius = Size.WarningIcon.size/2
        
        warningIconImageView = UIImageView()
        warningIconImageView.image = UIImage(named: "btn_close")?
            .changeColor(ColorConstants.mainRed)
        warningIconImageView.contentMode = .scaleAspectFit
        warningIconImageView.clipsToBounds = true
        
        warningLabel = UILabel()
        warningLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        warningLabel.textColor = .white
        warningLabel.numberOfLines = 0
        warningLabel.textAlignment = .center
        
        warningLabel.text = RS.lbl_jailbreak_message.localized()
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundView)
        mainView.addSubview(warningIconCoverView)
        warningIconCoverView.addSubview(warningIconImageView)
        mainView.addSubview(warningLabel)
    }
    
    func addSubviewsConstraints() {
        backgroundView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        warningIconCoverView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Size.WarningIcon.size)
            maker.centerX.equalToSuperview()
            maker.bottom.equalTo(warningLabel.snp.top).offset(Adaptive.val(-30))
        }
        
        warningIconImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Size.WarningIcon.size * 0.5)
            maker.center.equalToSuperview()
        }
        
        warningLabel.snp.remakeConstraints { (maker) in
            maker.left.equalToSuperview().offset(Adaptive.val(60))
            maker.right.equalToSuperview().inset(Adaptive.val(60))
            maker.centerY.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    struct WarningIcon {
        static let size = Adaptive.val(70)
    }
}
