//
//  JailbreakBlockerPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol JailbreakBlockerPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: JailbreakBlockerVM { get }
}

protocol JailbreakBlockerInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class JailbreakBlockerPresenter: JailbreakBlockerPresenterProtocol {
    weak var view: JailbreakBlockerVCProtocol!
    var interactor: JailbreakBlockerInteractorProtocol!
    var router: JailbreakBlockerRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = JailbreakBlockerVM()
    
    // Private property and methods
    
}

extension JailbreakBlockerPresenter: JailbreakBlockerInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
