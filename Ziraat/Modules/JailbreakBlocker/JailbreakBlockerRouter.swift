//
//  JailbreakBlockerRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol JailbreakBlockerRouterProtocol: class {
    static func createModule() -> UIViewController
}

class JailbreakBlockerRouter: JailbreakBlockerRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = JailbreakBlockerVC()
        let presenter = JailbreakBlockerPresenter()
        let interactor = JailbreakBlockerInteractor()
        let router = JailbreakBlockerRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
