//
//  JailbreakBlockerInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol JailbreakBlockerInteractorProtocol: class {
    
}

class JailbreakBlockerInteractor: JailbreakBlockerInteractorProtocol {
    weak var presenter: JailbreakBlockerInteractorToPresenterProtocol!
    
}
