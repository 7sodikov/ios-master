//
//  MenuRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MenuRouterProtocol: AnyObject {
    static func createModule() -> UIViewController
    static func createModuleInNavController() -> UIViewController
    func navigateToHome(in navigationCtrl: Any)
    func navigateToAccount(in navigationCtrl: Any)
    func navigateToCard(in navigationCtrl: Any)
    func navigateToMoneyTransfer(in navigationCtrl: Any)
    func navigateToConversion(in navigationCtrl: Any, prepareType: OperationPrepareType)
    func navigateToPayment(in navigationCtrl: Any)
    func navigateToLoan(in navigationCtrl: Any)
    func navigateToDeposit(in navigationCtrl: Any)
    func navigateToFavorites(in navigationCtrl: Any)
    func navigateToBankProducts(in navigationCtrl: Any)
    func navigateToBankOnMap(in navigationCtrl: Any)
    func navigateToSecuritySettings(in navigationCtrl: Any)
    func navigateToExit(in navigationCtrl: Any, delegate: PopubMessageProtocol?)
    func unavailableError(in navCtrl: Any)
    func navigateToNotificationsList(in navCtrl: Any)
}

class MenuRouter: MenuRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = MenuVC()
        let presenter = MenuPresenter()
        let interactor = MenuInteractor()
        let router = MenuRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToHome(in navigationCtrl: Any) {
        let dashboardCtrl = DashboardRouter.createModule()
        (navigationCtrl as? UINavigationController)?.pushViewController(dashboardCtrl, animated: true)
//        let viewCtrl = navigationCtrl as? UIViewController
//        let serviceListView = DashboardRouter.createModule()
//        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToAccount(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = MyAccountsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToCard(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = MyCardsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToMoneyTransfer(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
//        let operationsView = OperationsRouter.createModule(for: nil, historyItem: nil)
        let operationsView = MoneyTransfersRouter(for: nil, historyItem: nil).viewController
        viewCtrl?.navigationController?.pushViewController(operationsView, animated: true)
    }
    
    func navigateToConversion(in navigationCtrl: Any, prepareType: OperationPrepareType) {
        let viewCtrl = navigationCtrl as? UIViewController
//        let serviceListView = ConversionRouter.createModule(for: prepareType)
        let serviceListView = ConversionNewRouter().viewController
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToPayment(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
//        let vc = ServiceCategoriesRouter.createModule()
        let vc = PaymentsMenuRouter().viewController
        viewCtrl?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToLoan(in navigationCtrl: Any) {
        let module = LoansRouter.createModule()
        let viewCtrl = navigationCtrl as? UIViewController
        viewCtrl?.navigationController?.pushViewController(module, animated: true)
    }
    
    func navigateToDeposit(in navigationCtrl: Any) {
        let module = UserDepositListRouter.createModule()
        let viewCtrl = navigationCtrl as? UIViewController
        viewCtrl?.navigationController?.pushViewController(module, animated: true)
        
    }
    
    func navigateToFavorites(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = FavoritesListRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToBankProducts(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = BankProductsListRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToBankOnMap(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = BankOnMapRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToSecuritySettings(in navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = ProfileSettingsRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToExit(in navigationCtrl: Any, delegate: PopubMessageProtocol?) {
        let viewCtrl = navigationCtrl as! UIViewController
        let statementVC = PopupMessageRouter.createModule(RS.al_msg_exit.localized(), delegate)
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    static func createModuleInNavController() -> UIViewController {
        let navCtrl = NavigationController(rootViewController: createModule())
        return navCtrl
    }
    
    func unavailableError(in navCtrl: Any) {
        let viewCtrl = navCtrl as! UIViewController
        let statementVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func navigateToNotificationsList(in navCtrl: Any) {
        let viewCtrl = navCtrl as! UIViewController
        let statementVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
}
