//
//  MenuPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MenuPresenterProtocol: class {
    var viewModel: MenuVM { get }
    func homeButtonClicked()
    func tableViewDidSelectRow(at indexPath: IndexPath)
    func searchButtonClicked()
    func notificationsButtonClicked()
}

protocol MenuInteractorToPresenterProtocol: class {
    
}

class MenuPresenter: MenuPresenterProtocol {
    weak var view: MenuVCProtocol!
    var interactor: MenuInteractorProtocol!
    var router: MenuRouterProtocol!
    
    private(set) var viewModel = MenuVM()
    
    func homeButtonClicked() {
        router.navigateToHome(in: view)
    }
    
    func tableViewDidSelectRow(at indexPath: IndexPath) {
        let info = viewModel.actions[indexPath.row]
        
        switch info.actionType {
        
        case .accounts:
            router.navigateToAccount(in: view)
        case .cards:
            router.navigateToCard(in: view as Any)
        case .moneyTransfer:
            router.navigateToMoneyTransfer(in: view as Any)
        case .conversion:
            router.navigateToConversion(in: view as Any, prepareType: .a2p)
        case .payments:
            router.navigateToPayment(in: view as Any)
        case .loans:
            router.navigateToLoan(in: view as Any)
        case .deposits:
            router.navigateToDeposit(in: view as Any)
        case .favorites:
            router.navigateToFavorites(in: view as Any)
        case .bankProducts:
            router.navigateToBankProducts(in: view as Any)
        case .bankOnMap:
            router.navigateToBankOnMap(in: view as Any)
//            break 
        case .securitySettings:
            router.navigateToSecuritySettings(in: view as Any)
        case .exit:
            router.navigateToExit(in: view as Any, delegate: self)
        }
    }
    
    func searchButtonClicked() {
        router.unavailableError(in: view as Any)
    }
    
    func notificationsButtonClicked() {
        router.navigateToNotificationsList(in: view as Any)
    }
}

extension MenuPresenter: MenuInteractorToPresenterProtocol {
    
}

extension MenuPresenter : PopubMessageProtocol{
    func actionResult(_ action: Bool) {
        if action {
            interactor.logout()
//            UDManager.clear()
//            LocalizationManager.language = .english
            KeychainManager.logout()
            appDelegate.router.navigate(to: .logout)
            PushNotificationsManager.unsubscribeFromTopic()
        }
    }
}
