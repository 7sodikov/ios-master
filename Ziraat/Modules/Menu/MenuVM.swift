//
//  MenuVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Rswift
import UIKit

class MenuVM {
    var actions: [MenuCellVM] {
        ActionType.allCases.map { MenuCellVM(name: $0.name, image: UIImage(named: $0.icon)!, type: $0) }
//        return [
//        MenuCellVM(name: RS.lbl_accounts.localized(), image: UIImage(named: "img_menu_acc")!, type: .accounts),
//        MenuCellVM(name: RS.lbl_cards.localized(), image: UIImage(named: "img_menu_cards")!, type: .cards),
//        MenuCellVM(name: RS.lbl_money_transfer.localized(), image: UIImage(named: "img_menu_pay")!, type: .moneyTransfer),
//        MenuCellVM(name: RS.lbl_conversion.localized().localized(), image: UIImage(named: "img_conversion")!, type: .conversion),
//        MenuCellVM(name: RS.lbl_payments.localized(), image: UIImage(named: "img_payment")!, type: .payments),
//        MenuCellVM(name: RS.lbl_loans.localized(), image: UIImage(named: "img_menu_cred")!, type: .loans),
//        MenuCellVM(name: RS.lbl_deposits.localized(), image: UIImage(named: "img_menu_depos")!, type: .deposits),
//        MenuCellVM(name: RS.lbl_favorites.localized(), image: UIImage(named: "img_favor")!, type: .favorites),
//        MenuCellVM(name: RS.lbl_bank_products.localized(), image: UIImage(named: "img_bank_services")!, type: .bankProducts),
//        MenuCellVM(name: RS.lbl_nearest_branch.localized(), image: UIImage(named: "img_nearest_ziraat")!, type: .bankOnMap),
//        MenuCellVM(name: RS.lbl_security_settings.localized(), image: UIImage(named: "img_sec_settings")!, type: .securitySettings),
//        MenuCellVM(name: RS.lbl_exit.localized(), image: UIImage(named: "img_exit")!, type: .exit)]
    }
}

enum ActionType: Int, CaseIterable {
    case accounts
    case cards
    case moneyTransfer
    case conversion
    case payments
    case loans
    case deposits
    case favorites
    case bankProducts
    case bankOnMap
    case securitySettings
    case exit
    
    var icon: String {
        let dic: [ActionType: String] = [
            .accounts: "img_menu_acc",
            .cards: "img_menu_cards",
            .moneyTransfer: "img_menu_pay",
            .conversion: "img_conversion",
            .payments: "img_payment",
            .loans: "img_menu_cred",
            .deposits: "img_menu_depos",
            .favorites: "img_favor",
            .bankProducts: "img_bank_services",
            .bankOnMap: "img_nearest_ziraat",
            .securitySettings: "img_sec_settings",
            .exit: "img_exit",
        ]
        return dic[self]!
    }
    
    var name: String {
        let dic: [ActionType: StringResource] = [
            .accounts: RS.lbl_accounts,
            .cards: RS.lbl_cards,
            .moneyTransfer: RS.lbl_money_transfer,
            .conversion: RS.lbl_conversion,
            .payments: RS.lbl_payments,
            .loans: RS.lbl_loans,
            .deposits: RS.lbl_deposits,
            .favorites: RS.lbl_favorites,
            .bankProducts: RS.lbl_bank_products,
            .bankOnMap: RS.lbl_nearest_branch,
            .securitySettings: RS.lbl_security_settings,
            .exit: RS.lbl_exit,
        ]
        return dic[self]!.localized()
    }
}

class MenuCellVM {
    
    var image: UIImage
    var name: String
    var actionType: ActionType

    init(name: String, image: UIImage, type: ActionType) {
        self.name = name
        self.image = image
        self.actionType = type
    }
}
