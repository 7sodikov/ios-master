//
//  MenuInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MenuInteractorProtocol: AnyObject {
    func logout()
}

class MenuInteractor: MenuInteractorProtocol {
    weak var presenter: MenuInteractorToPresenterProtocol!
    
    func logout() {
        NetworkService.Auth.logout { _ in
            
        }
    }
}
