//
//  MenuVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MenuVCProtocol: class {
    func reload()
    var navigationCtrl: Any { get }
}

class MenuVC: UIViewController, MenuViewInstaller {
    var uzbLabel: UILabel!
    var logoLabelStackView: UIStackView!
    var logoImageView: UIImageView!
    var logoLabel: UILabel!
    var bellImage: UIButton!
    var backgroundMainView: UIView!
    var tableView: UITableView!
    var bottomPanel: BottomPanel!
    var mainView: UIView { view }
    var presenter: MenuPresenterProtocol!
    
//    var colorForStatusBar: UIStatusBarStyle = .default
    
    fileprivate let cellid = "\(MenuTableViewCell.self)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        setupNavigation()
                
        bottomPanel.delegate = self
        
        bellImage.addTarget(self, action: #selector(openNotificationsettings), for: .touchUpInside)
                
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    private func setupNavigation() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.setImage(UIImage(named:"menuIcon"), for: .normal)
        menuBtn.addTarget(self, action: #selector(self.openNotificationsettings), for: UIControl.Event.touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem
    }
    
    @objc private func openNotificationsettings() {
        presenter.notificationsButtonClicked()
    }
    
    @objc private func bellButtonClicked(_ sender: UIButton) {
        presenter.notificationsButtonClicked()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        setNeedsStatusBarAppearanceUpdate()
    }
}

extension MenuVC: BottomPanelDelegate {
    func searchButtonClicked() {
        presenter.searchButtonClicked()
    }
    
    func rightButtonClicked() {
//        let vc = DashboardVC()
        self.dismiss(animated: true, completion: nil)
    }
}

extension MenuVC: MenuVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func reload() {
        tableView.reloadData()
        localizeText()
    }
}

extension MenuVC: UITableViewDelegate, UITableViewDataSource {
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.actions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(41)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! MenuTableViewCell
//        cell.backgroundColor = .white
//        let backgroundView = UIView()
//        backgroundView.backgroundColor = ColorConstants.lightGray.withAlphaComponent(0.3)
        cell.selectionStyle = .none
        cell.data = presenter.viewModel.actions[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.tableViewDidSelectRow(at: indexPath)
    }
}
