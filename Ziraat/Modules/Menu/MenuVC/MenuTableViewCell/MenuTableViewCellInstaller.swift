//
//  MenuTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol MenuTableViewCellInstaller: ViewInstaller {
//    var backgroundMainView: UIView! { get set }
    var menuActionImage: UIImageView! { get set }
    var menuActionLabel: UILabel! { get set }
    var arrowImage: UIImageView! { get set }
    var indicatorLabel: UILabel! { get set }
}

extension MenuTableViewCellInstaller {
    func initSubviews() {
//        backgroundMainView = UIView()
//        backgroundMainView.backgroundColor = .yellow
        
        menuActionImage = UIImageView()
        menuActionImage.contentMode = .scaleAspectFit
        
        menuActionLabel = UILabel()
        menuActionLabel.textColor = .lightGrey
        menuActionLabel.font = .gothamNarrow(size: 20, .medium) //EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        
        arrowImage = UIImageView()
        arrowImage.image = UIImage(named: "img_arr_menu")
        arrowImage.contentMode = .scaleAspectFit
        
        indicatorLabel = UILabel()
        indicatorLabel.textColor = .white
        indicatorLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(13))
        indicatorLabel.backgroundColor = .red
        indicatorLabel.layer.cornerRadius = Adaptive.val(10)
        indicatorLabel.text = "1"
        indicatorLabel.textAlignment = .center
        indicatorLabel.clipsToBounds = true
    }
    
    func embedSubviews() {
//        mainView.addSubview(backgroundMainView)
        mainView.addSubview(menuActionImage)
        mainView.addSubview(menuActionLabel)
        mainView.addSubview(arrowImage)
        mainView.addSubview(indicatorLabel)
    }
    
    func addSubviewsConstraints() {
//        backgroundMainView.snp.remakeConstraints { (maker) in
////            maker.height.equalTo(Adaptive.val(41))
//            maker.edges.equalTo(0)
//        }

        menuActionImage.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(20))
            maker.height.equalTo(Adaptive.val(20))
            maker.leading.equalToSuperview().offset(Adaptive.val(20))
            maker.centerY.equalTo(arrowImage)
        }
        
        menuActionLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(menuActionImage.snp.trailing).offset(Adaptive.val(10))
            maker.centerY.equalTo(arrowImage)
        }
        
        arrowImage.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(8))
            maker.height.equalTo(Adaptive.val(13.5))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(23.5))
            maker.top.equalToSuperview().offset(Adaptive.val(13.5))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(13.5))
        }
        
//        indicatorLabel.snp.remakeConstraints { (maker) in
//            maker.width.equalTo(Adaptive.val(20))
//            maker.height.equalTo(Adaptive.val(20))
//            maker.top.equalToSuperview().offset(Adaptive.val(13))
//            maker.trailing.equalToSuperview().offset(-Adaptive.val(44))
//        }
    }
}


