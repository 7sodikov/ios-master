//
//  MenuTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell, MenuTableViewCellInstaller {
    var indicatorLabel: UILabel!
    var backgroundMainView: UIView!
    var menuActionImage: UIImageView!
    var menuActionLabel: UILabel!
    var arrowImage: UIImageView!
    var mainView: UIView { contentView }
    
    var data: MenuCellVM? {
        didSet {
            if let data = self.data {
                self.menuActionImage.image = data.image
                self.menuActionLabel.text = data.name
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        self.indicatorLabel.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
