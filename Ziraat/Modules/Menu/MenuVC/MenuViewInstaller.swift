//
//  MenuViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MenuViewInstaller: ViewInstaller {
    var logoImageView: UIImageView! { get set }
    var logoLabel: UILabel! { get set }
    var uzbLabel: UILabel! { get set }
    var logoLabelStackView: UIStackView! { get set }
    var bellImage: UIButton! { get set }
    var tableView: UITableView! { get set }
    var bottomPanel: BottomPanel! { get set }
}

extension MenuViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .white
        
        logoImageView = UIImageView()
        logoImageView.image = UIImage(named: "img_logo_red_title")
        logoImageView.contentMode = .scaleAspectFit
        
        logoLabel = UILabel()
        logoLabel.textColor = .black
        logoLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(23))
        logoLabel.text = "Ziraat Bank"
        logoLabel.numberOfLines = 2
        
        uzbLabel = UILabel()
        uzbLabel.textColor = .black
        uzbLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(9.5))
        uzbLabel.text = "Uzbekistan"
        uzbLabel.textAlignment = .center
        
        logoLabelStackView = UIStackView()
        logoLabelStackView.axis = .vertical
        logoLabelStackView.alignment = .center
        
        bellImage = UIButton()
        bellImage.setImage(UIImage(named: "img_prof_without"), for: .normal)
        bellImage.contentMode = .scaleAspectFit
        bellImage.imageView?.adjustsImageSizeForAccessibilityContentSizeCategory = true
        
        tableView = UITableView()
        tableView.register(MenuTableViewCell.self, forCellReuseIdentifier: String(describing: MenuTableViewCell.self))
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.separatorColor = .lightGrey //ColorConstants.separatorGrayLine
        tableView.separatorInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        bottomPanel = BottomPanel()
        bottomPanel.homeImageView.isHidden = true
        bottomPanel.menuImageView.isHidden = false
        bottomPanel.menuImageView.image = UIImage(named: "btn_close")
        bottomPanel.menuLabel.isHidden = false
        bottomPanel.menuLabel.text = RS.lbl_close.localized()
    }
    
    func embedSubviews() {
        mainView.addSubview(logoImageView)
        logoLabelStackView.addArrangedSubview(logoLabel)
        logoLabelStackView.addArrangedSubview(uzbLabel)
        mainView.addSubview(logoLabelStackView)
        mainView.addSubview(bellImage)
        mainView.addSubview(tableView)
        mainView.addSubview(bottomPanel)
    }
    
    func addSubviewsConstraints() {
//        logoLabelStackView.snp.remakeConstraints { (maker) in
//            maker.centerX.equalToSuperview()
//            maker.top.equalToSuperview().offset(Adaptive.val(45))
//        }
//
//        logoLabel.snp.remakeConstraints { (maker) in
//            maker.top.leading.trailing.equalTo(logoLabelStackView)
//        }
//
//        uzbLabel.snp.remakeConstraints { (maker) in
//            maker.bottom.leading.trailing.equalTo(logoLabelStackView)
//        }
        
        logoImageView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(47.5))
//            maker.leading.equalToSuperview().offset(Adaptive.val(97))
            maker.centerX.equalToSuperview()
            maker.width.equalTo(160)
            maker.height.equalTo(37)
        }
        
        bellImage.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(logoImageView)
            maker.trailing.equalToSuperview().offset(-Adaptive.val(23))
            maker.width.height.equalTo(Adaptive.val(30))
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(logoImageView.snp.bottom)
            maker.leading.trailing.equalToSuperview()
        }
        
        bottomPanel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(tableView.snp.bottom)
            maker.leading.bottom.trailing.equalToSuperview()
            maker.height.equalTo(Size.BottomPanel.height)
        }
        
    }
}

fileprivate struct Size {
    struct BottomPanel {
        static let height = Adaptive.val(95)
    }
}
