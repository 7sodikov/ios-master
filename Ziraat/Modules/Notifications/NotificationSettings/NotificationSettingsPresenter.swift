//
//  NotificationSettingsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import FirebaseMessaging

protocol NotificationSettingsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: NotificationSettingsVM { get }
    func viewDidLoad()
    func setNotification(isTrue: Bool)
    var subscription: [String]? { get }
}

protocol NotificationSettingsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetSubscription(with response: ResponseResult<[String], AppError>)
}

class NotificationSettingsPresenter: NotificationSettingsPresenterProtocol {
    weak var view: NotificationSettingsVCProtocol!
    var interactor: NotificationSettingsInteractorProtocol!
    var router: NotificationSettingsRouterProtocol!
    var subscriptionItems: [String]?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = NotificationSettingsVM()
    
    func viewDidLoad() {
        interactor.getSubscription()
    }
    
    func setNotification(isTrue: Bool) {
//        if isTrue == true {
//            for item in subscription {
//                Messaging.messaging().subscribe(toTopic: item)
//            }
//            
//        } else {
//            for item in subscription {
//                Messaging.messaging().unsubscribe(fromTopic: item)
//            }
//        }
    }
    
    var subscription: [String]? {
        return subscriptionItems
    }
    
    // Private property and methods
    
}

extension NotificationSettingsPresenter: NotificationSettingsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetSubscription(with response: ResponseResult<[String], AppError>) {
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                subscriptionItems = result.data
            }
            break
        case let .failure(error):
            break
        }
    }
}
