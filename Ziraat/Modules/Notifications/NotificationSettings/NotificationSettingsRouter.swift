//
//  NotificationSettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationSettingsRouterProtocol: class {
    static func createModule() -> UIViewController
}

class NotificationSettingsRouter: NotificationSettingsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = NotificationSettingsVC()
        let presenter = NotificationSettingsPresenter()
        let interactor = NotificationSettingsInteractor()
        let router = NotificationSettingsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
