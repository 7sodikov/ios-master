//
//  NotificationSettingsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import FirebaseMessaging

protocol NotificationSettingsVCProtocol: class {
    
}

class NotificationSettingsVC: BaseViewController, NotificationSettingsViewInstaller {
    var backView: UIImageView!
    var turnOffLabel: UILabel!
    var notificationSwitch: UISwitch!
    var mainView: UIView { view }
    var presenter: NotificationSettingsPresenterProtocol!
    var subscription = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        presenter.viewDidLoad()
        
        self.title = RS.lbl_notification_settings.localized()
        
        notificationSwitch.isOn = UDManager.notificationOnOff
        notificationSwitch.addTarget(self, action: #selector(handlesTurnOffSwitch(_:)), for: .valueChanged)
    }
    
    @objc func handlesTurnOffSwitch(_ sender: UISwitch) {
        let lanString = LocalizationManager.language
        UDManager.notificationOnOff = sender.isOn
        
        if sender.isOn {
//            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_EN"){ error in
//                        if error == nil{
//                            print("Subscribed to topic")
//                        }
//                        else{
//                            print("Not Subscribed to topic")
//                        }
//                }
            
            if LocalizationManager.language == Language.english {
                Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_EN")
            } else if LocalizationManager.language == Language.russian {
                Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_RU")
            } else if LocalizationManager.language == Language.uzbek {
                Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_UZ")
            } else {
                Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_TR")
            }
        } else {
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_EN")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_RU")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_UZ")
            Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_TR")
        }
    }
}

extension NotificationSettingsVC: NotificationSettingsVCProtocol {
    
}
