//
//  NotificationSettingsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationSettingsViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var turnOffLabel: UILabel! { get set }
    var notificationSwitch: UISwitch! { get set }
}

extension NotificationSettingsViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
        turnOffLabel = UILabel()
        turnOffLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        turnOffLabel.textColor = .black
        turnOffLabel.text = RS.lbl_notification.localized()
        
        notificationSwitch = UISwitch()
        notificationSwitch.onTintColor = ColorConstants.mainRed
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(turnOffLabel)
        mainView.addSubview(notificationSwitch)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        turnOffLabel.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(152))
            maker.leading.equalToSuperview().offset(Adaptive.val(28))
        }
        
        notificationSwitch.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(turnOffLabel)
            maker.trailing.equalToSuperview().offset(-Adaptive.val(28))
        }
    }
}

fileprivate struct Size {
    
}
