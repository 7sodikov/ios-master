//
//  NotificationSettingsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NotificationSettingsInteractorProtocol: class {
    func getSubscription()
}

class NotificationSettingsInteractor: NotificationSettingsInteractorProtocol {
    weak var presenter: NotificationSettingsInteractorToPresenterProtocol!
    func getSubscription() {
        NetworkService.Notifications.notificationTopic() { (result) in
            self.presenter.didGetSubscription(with: result)
        }
    }
}
