//
//  NotificationDescriptionRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationDescriptionRouterProtocol: class {
    static func createModule(item: NotificationItemResponse?, any: [AnyHashable: Any]?) -> UIViewController
}

class NotificationDescriptionRouter: NotificationDescriptionRouterProtocol {
    static func createModule(item: NotificationItemResponse?, any: [AnyHashable: Any]?) -> UIViewController {
        let vc = NotificationDescriptionVC()
        let presenter = NotificationDescriptionPresenter()
        let interactor = NotificationDescriptionInteractor()
        let router = NotificationDescriptionRouter()
        
        vc.presenter = presenter
        vc.notification = item
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
}
