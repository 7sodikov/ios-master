//
//  NotificationDescriptionVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import Kingfisher
import UserNotifications

protocol NotificationDescriptionVCProtocol: class {
    
}

class NotificationDescriptionVC: BaseViewController, NotificationDescriptionViewInstaller {
    var backView: UIImageView!
    var headingImageView: UIImageView!
    var bodyTextLabel: UILabel!
    var mainView: UIView { view }
    var presenter: NotificationDescriptionPresenterProtocol!
    
    var notification: NotificationItemResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setup()
        
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.headingImageView?.transform = CGAffineTransform(scaleX: 1, y: 0.9)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 0.5, animations: {() -> Void in
                self.headingImageView?.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        })
    }
    
    private func setup() {
        self.title = notification?.title
        if notification?.headingImage == "" || notification?.headingImage == nil {
            headingImageView.isHidden = true
            self.bodyTextLabel.snp.remakeConstraints { (maker) in
                maker.top.equalToSuperview().offset(Adaptive.val(110))
                maker.leading.equalToSuperview().offset(Adaptive.val(15))
                maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            }
        } else {
            headingImageView.kf.setImage(with: URL(string: (notification?.headingImage)!), placeholder: nil)
            self.bodyTextLabel.snp.remakeConstraints { (maker) in
                maker.top.equalTo(self.headingImageView.snp.bottom).offset(Adaptive.val(10))
                maker.leading.equalToSuperview().offset(Adaptive.val(15))
                maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
            }
        }
        bodyTextLabel.text = notification?.fullText
    }
}

extension NotificationDescriptionVC: NotificationDescriptionVCProtocol {
    
}
