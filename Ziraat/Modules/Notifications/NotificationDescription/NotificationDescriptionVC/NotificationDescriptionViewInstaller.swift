//
//  NotificationDescriptionViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationDescriptionViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var headingImageView: UIImageView! { get set }
    var bodyTextLabel: UILabel! { get set }
}

extension NotificationDescriptionViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
        headingImageView = UIImageView()
//        headingImageView.isHidden = true
        headingImageView.contentMode = .scaleAspectFit
        
        bodyTextLabel = UILabel()
        bodyTextLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        bodyTextLabel.textColor = .black
        bodyTextLabel.numberOfLines = 0
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(headingImageView)
        mainView.addSubview(bodyTextLabel)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        headingImageView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(100))
            maker.leading.trailing.equalTo(backView)
            maker.height.equalTo(Adaptive.val(200))
        }
        
//        bodyTextLabel.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(headingImageView.snp.bottom).offset(Adaptive.val(10))
//            maker.leading.equalToSuperview().offset(Adaptive.val(15))
//            maker.trailing.equalToSuperview().offset(-Adaptive.val(15))
//        }
    }
}

fileprivate struct Size {
    static let paddingLeftright = Adaptive.val(15)
}
