//
//  NotificationDescriptionPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NotificationDescriptionPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: NotificationDescriptionVM { get }
}

protocol NotificationDescriptionInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class NotificationDescriptionPresenter: NotificationDescriptionPresenterProtocol {
    weak var view: NotificationDescriptionVCProtocol!
    var interactor: NotificationDescriptionInteractorProtocol!
    var router: NotificationDescriptionRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = NotificationDescriptionVM()
    
    // Private property and methods
    
}

extension NotificationDescriptionPresenter: NotificationDescriptionInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
