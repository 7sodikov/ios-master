//
//  NotificationTableViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationTableViewInstaller: ViewInstaller {
    var bgView : UIView! { get set }
    var dayLabel: UILabel! { get set }
    var monthLabel: UILabel! { get set }
    var yearLabel: UILabel! { get set }
    var dateStackView: UIStackView! { get set }
    var descriptionLabel: UILabel! { get set }
    var shortTextLabel: UILabel! { get set }
}

extension NotificationTableViewInstaller {
    func initSubviews() {
        bgView = UIView()
        bgView.backgroundColor = .white
        bgView.layer.cornerRadius = Adaptive.val(5)
        
        dayLabel = UILabel()
        dayLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(24))
        dayLabel.textColor = .black
        dayLabel.textAlignment = .center
        dayLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .horizontal)
        dayLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .vertical)
        dayLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .horizontal)
        dayLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        
        monthLabel = UILabel()
        monthLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(15))
        monthLabel.textColor = ColorConstants.gray
        monthLabel.textAlignment = .center
        monthLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .horizontal)
        monthLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .vertical)
        monthLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .horizontal)
        monthLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .vertical)

        yearLabel = UILabel()
        yearLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(15))
        yearLabel.textColor = ColorConstants.gray
        yearLabel.textAlignment = .center
        yearLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        yearLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .vertical)
        yearLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
        yearLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .vertical)

        dateStackView = UIStackView()
        dateStackView.axis = .vertical
        dateStackView.alignment = .center
        dateStackView.distribution = .fillProportionally
        dateStackView.spacing = Adaptive.val(5)
        
        descriptionLabel = UILabel()
        descriptionLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(16))
        descriptionLabel.textColor = .black
//        descriptionLabel.numberOfLines = 0
        
        shortTextLabel = UILabel()
        shortTextLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        shortTextLabel.textColor = ColorConstants.gray
        shortTextLabel.numberOfLines = 2
        shortTextLabel.lineBreakMode = .byTruncatingTail
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        
        dateStackView.addArrangedSubview(dayLabel)
        dateStackView.addArrangedSubview(monthLabel)
        dateStackView.addArrangedSubview(yearLabel)
        mainView.addSubview(dateStackView)
        
        mainView.addSubview(descriptionLabel)
        mainView.addSubview(shortTextLabel)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.remakeConstraints { (make) in
            make.leading.equalToSuperview().offset(Adaptive.val(16))
            make.trailing.equalToSuperview().offset(-Adaptive.val(16))
            make.top.equalToSuperview().offset(Adaptive.val(5))
            make.bottom.equalToSuperview().offset(-Adaptive.val(5))
        }
        
        dateStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(bgView.snp.top).offset(Adaptive.val(14))
            maker.leading.equalTo(bgView.snp.leading).offset(Adaptive.val(12))
            maker.bottom.equalTo(bgView.snp.bottom).offset(-Adaptive.val(12))
            maker.width.equalTo(yearLabel)
        }
        
        descriptionLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(bgView.snp.top).offset(Adaptive.val(20))
            make.leading.equalTo(dateStackView.snp.trailing).offset(Adaptive.val(15))
            make.trailing.equalTo(bgView.snp.trailing).offset(-Adaptive.val(15))
        }
        
        shortTextLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(Adaptive.val(5))
            make.leading.equalTo(descriptionLabel.snp.leading)
            make.trailing.equalTo(bgView.snp.trailing).offset(-Adaptive.val(15))
            make.bottom.equalTo(bgView.snp.bottom).offset(-Adaptive.val(20))
        }
    }
}
