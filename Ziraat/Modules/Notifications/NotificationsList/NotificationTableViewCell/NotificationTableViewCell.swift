//
//  NotificationTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell, NotificationTableViewInstaller {
    var bgView: UIView!
    var dayLabel: UILabel!
    var monthLabel: UILabel!
    var yearLabel: UILabel!
    var dateStackView: UIStackView!
    var descriptionLabel: UILabel!
    var shortTextLabel: UILabel!
    var mainView: UIView { contentView }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(notificationVM: NotificationItemVM) -> NotificationTableViewCell {
        let notification = notificationVM.notificationItem
        dayLabel.text = notificationVM.day
        monthLabel.text = notificationVM.month
        yearLabel.text = notificationVM.year
        descriptionLabel.text = notification.title
        shortTextLabel.text = notification.shortText
        return self
    }
}
