//
//  NotificationsListInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NotificationsListInteractorProtocol: class {
    func getNotificationsList(page: Int, size: Int)
}

class NotificationsListInteractor: NotificationsListInteractorProtocol {
    weak var presenter: NotificationsListInteractorToPresenterProtocol!
    func getNotificationsList(page: Int, size: Int) {
        NetworkService.Notifications.notificationList(page: page, size: size) { (result) in
            self.presenter.didGetNotificationsList(with: result)
        }
    }
}
