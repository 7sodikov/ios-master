//
//  NotificationsListRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationsListRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToNotificationSettings(in navCtrl: Any)
    func navigateToDescription(in navCtrl: Any, notificationItem: NotificationItemResponse)
}

class NotificationsListRouter: NotificationsListRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = NotificationsListVC()
        let presenter = NotificationsListPresenter()
        let interactor = NotificationsListInteractor()
        let router = NotificationsListRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToNotificationSettings(in navCtrl: Any) {
        let viewCtrl = navCtrl as! UIViewController
        let notifVC = NotificationSettingsRouter.createModule()
        viewCtrl.navigationController?.pushViewController(notifVC, animated: true)
    }
    
    func navigateToDescription(in navCtrl: Any, notificationItem: NotificationItemResponse) {
        let viewCtrl = navCtrl as! UIViewController
        let notifVC = NotificationDescriptionRouter.createModule(item: notificationItem, any: nil)
        viewCtrl.navigationController?.pushViewController(notifVC, animated: true)
    }
}
