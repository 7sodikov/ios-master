//
//  NotificationsListVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class NotificationsListVM {
    var notifications: [NotificationItemResponse]?
    var notificationItems: [NotificationItemVM] = []
    
    func add() {
        for notification in notifications ?? []{
            notificationItems.append(NotificationItemVM(item: notification))
        }
    }
}

class NotificationItemVM {
    var notificationItem: NotificationItemResponse
    
    var day: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (notificationItem.createDate) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let day = myCalendar.component(.day, from: date!)
        return String(day)
    }
    
    var month: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (notificationItem.createDate) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let month = myCalendar.component(.month, from: date!)
        if month == 1 {
            return RS.lbl_jan.localized()
        } else if month == 2 {
            return RS.lbl_feb.localized()
        } else if month == 3 {
            return RS.lbl_mar.localized()
        } else if month == 4 {
            return RS.lbl_apr.localized()
        } else if month == 5 {
            return RS.lbl_may.localized()
        } else if month == 6 {
            return RS.lbl_jun.localized()
        } else if month == 7 {
            return RS.lbl_jul.localized()
        } else if month == 8 {
            return RS.lbl_aug.localized()
        } else if month == 9 {
            return RS.lbl_sept.localized()
        } else if month == 10 {
            return RS.lbl_oct.localized()
        } else if month == 11 {
            return RS.lbl_nov.localized()
        } else {
            return RS.lbl_dec.localized()
        }
    }
    
    var year: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (notificationItem.createDate) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let year = myCalendar.component(.year, from: date!)
        return String(year)
    }
    
    init(item: NotificationItemResponse) {
        self.notificationItem = item
    }
}
