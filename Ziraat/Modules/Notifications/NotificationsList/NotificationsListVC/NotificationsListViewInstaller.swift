//
//  NotificationsListViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationsListViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var tableView: TableView! { get set }
}

extension NotificationsListViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
        tableView = TableView()
        tableView.register(NotificationTableViewCell.self, forCellReuseIdentifier: String(describing: NotificationTableViewCell.self))
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.leading.bottom.trailing.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
