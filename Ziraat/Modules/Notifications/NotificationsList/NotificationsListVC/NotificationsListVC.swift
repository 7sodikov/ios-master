//
//  NotificationsListVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NotificationsListVCProtocol: class {
    func reloadDataTableView()
}

class NotificationsListVC: BaseViewController, NotificationsListViewInstaller {
    var backView: UIImageView!
    var tableView: TableView!
    var mainView: UIView { view }
    
    var refreshControl = UIRefreshControl()
//    var isLoading = false
    
    var presenter: NotificationsListPresenterProtocol!
    
    fileprivate let cellid = "\(NotificationTableViewCell.self)"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()

        setupSubviews()
        setupNavigation()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Loading")
           refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
           tableView.addSubview(refreshControl)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @objc func refresh(_ sender: AnyObject) {
        presenter.viewDidLoad()
        tableView.reloadData()
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.stopRefreshing), userInfo: nil, repeats: true)
    }
    
    @objc private func stopRefreshing() {
        refreshControl.endRefreshing()
    }
    
    private func setupNavigation() {
        self.title = RS.lbl_notification.localized()
        
        let notificationButton = UIBarButtonItem(image: UIImage(named: "btn_notification")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openNotificationsettings))
        self.navigationItem.rightBarButtonItem = notificationButton
    }
    
    @objc private func openNotificationsettings() {
        presenter.notificationSettingsClicked()
    }
}

extension NotificationsListVC: NotificationsListVCProtocol, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.notifications?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! NotificationTableViewCell
        presenter.viewModel.add()
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        let notificatioItemVM = presenter.viewModel.notificationItems[indexPath.row]
        return cell.setup(notificationVM: notificatioItemVM)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.tableViewCellSelected(index: indexPath.row)
    }
    
    func reloadDataTableView() {
        tableView.reloadData()
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//
//        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading {
//            loadMoreData()
//        }
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if offsetY > contentHeight - scrollView.frame.size.height {
            // if totalPage Number is reached dont continue requesting !!!
            if !presenter.loading && presenter.totalPages > 0 && presenter.totalPages >  presenter.pageNumber  {
                self.tableView.animatePreloader = true
                self.tableView.showFooter = true
                presenter.loadingMore()
                tableView.reloadData()
            } else {
                tableView.animatePreloader = false
                tableView.showFooter = false
            }
        }
    }
    
//    func loadMoreData() {
//        if !self.isLoading {
//            self.isLoading = true
//            DispatchQueue.global().async {
//                // Fake background loading task for 2 seconds
//                sleep(2)
//                // Download more data here
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                    self.isLoading = false
//                }
//            }
//        }
//    }
}
