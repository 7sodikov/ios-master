//
//  NotificationsListPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NotificationsListPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: NotificationsListVM { get }
    func viewDidLoad()
    func notificationSettingsClicked()
    func tableViewCellSelected(index: Int)
    var pageNumber: Int { get }
    var totalPages: Int { get }
    var loading: Bool { get }
    func loadingMore()
}

protocol NotificationsListInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didGetNotificationsList(with response: ResponseResult<NotificationsListResponse, AppError>)
}

class NotificationsListPresenter: NotificationsListPresenterProtocol {
    weak var view: NotificationsListVCProtocol!
    var interactor: NotificationsListInteractorProtocol!
    var router: NotificationsListRouterProtocol!
    
    var limit: Int = 20
    var pageNumber: Int = 0
    var loading: Bool = false
    var totalPages: Int = 0
    
    // VIEW -> PRESENTER
    private(set) var viewModel = NotificationsListVM()
    
    func viewDidLoad() {
        interactor.getNotificationsList(page: 0, size: 50)
    }
    
    func notificationSettingsClicked() {
        router.navigateToNotificationSettings(in: view as Any)
    }
    
    func setupTableView() {
        interactor.getNotificationsList(page: pageNumber, size: limit)
    }
    
    func tableViewCellSelected(index: Int) {
        let notificationItem = viewModel.notifications?[index]
        router.navigateToDescription(in: view as Any, notificationItem: notificationItem!)
    }
    
    func loadingMore() {
        loading = true
        setupTableView()
    }
    
    // Private property and methods
    
}

extension NotificationsListPresenter: NotificationsListInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetNotificationsList(with response: ResponseResult<NotificationsListResponse, AppError>) {
//        view.preloader(show: false)
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                totalPages = result.data.totalPages ?? 0
                loading = false
                viewModel.notifications = result.data.content
                pageNumber += 1 // increment pageNumber for endless scrolling
                view.reloadDataTableView()
            }
            break
        case let .failure(error):
            break
        }
    }
}
