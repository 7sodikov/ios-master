//
//  CollectionPanModelViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import PanModal

protocol CollectionPanModelViewDelegate: class {
    func card(didSelectViewModel viewModel: CollectionViewModel, at indexPath: IndexPath, with tag: Int)
}

class CollectionPanModelViewController: BasePresentController {
    var headerLabel: UILabel!
    var collectionView: UICollectionView!
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var allCards: [CollectionViewModel] = []
    let headerTitle: String
    weak var cardDelegate: CollectionPanModelViewDelegate? = nil
    
    init(allCards: [CollectionViewModel], delegate: CollectionPanModelViewDelegate?, headerTitle: String) {
        self.headerTitle = headerTitle
        self.allCards = allCards
        self.cardDelegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupTargets()
    }
    
    override func setupViews() {
        super.setupViews()
        
        topLabel.isHidden = true
        
        headerLabel = UILabel()
        headerLabel.font = .gothamNarrow(size: 16, .book)
        headerLabel.textColor = UIColor(68, 80, 86)
        headerLabel.isHidden = headerTitle.isEmpty
        headerLabel.text = headerTitle
        view.addSubview(headerLabel)
        headerLabel.constraint { (make) in
            make.top(38).horizontal(16).equalToSuperView()
        }
        
        collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.minimumInteritemSpacing = 16
        collectionViewFlowLayout.minimumLineSpacing = 16
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 104)
        collectionViewFlowLayout.scrollDirection = .horizontal
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.backgroundColor = .white
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(collectionView)
        var height: CGFloat = 112
        if allCards.first is AccountCardCellViewModel {
            height += 64
        }
        collectionView.constraint { (make) in
            make.height(height).top(self.headerLabel.bottomAnchor, offset: 16).horizontal.equalToSuperView()
        }
    }
    
    override func setupTargets() {
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(AccountCardCell.self)
        collectionView.register(CardCell.self)
        collectionView.register(CollectionViewCell.self)
    }
    
    override func setupTableView() {}
}


extension CollectionPanModelViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        allCards.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let card = allCards[indexPath.row]
        let cell = collectionView.reusable(card.cellType, for: indexPath)
        cell.configure(viewModel: card)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        allCards[indexPath.row].setSelected(true)
        let cell = collectionView.cellForItem(at: indexPath)
        cardDelegate?.card(didSelectViewModel: allCards[indexPath.row], at: indexPath, with: cell?.tag ?? 0)
//        collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally], animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.dismiss(animated: true)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        allCards[indexPath.row].setSelected(false)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        allCards[indexPath.row].itemSize
    }
    
}


extension CollectionPanModelViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return collectionView
    }
    
    var shortFormHeight: PanModalHeight {
        .contentHeight(320)
    }
    
    var longFormHeight: PanModalHeight {
         .contentHeight(400)
    }
}

fileprivate enum DIM {
    
    struct Collection {
        static let height: CGFloat = 160
        static var itemSize: CGSize {
            CGSize(width: 255, height: 144)
        }
    }
    
}
