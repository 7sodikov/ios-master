//
//  ConversionNewPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol ConversionNewPresentationLogic: PresentationProtocol, ConversionNewBase, TablePresentationLogic {
    
    func willChangeCards()
    func selectCard(cardOrAccount: Any)
    func willSet(amount: String)
    func changeAmountCurrency()
    func nextButtonClicked()
}


class ConversionNewPresenter: ConversionNewPresentationLogic {
    
    private let view: ConversionNewDisplayLogic
    private var interactor: ConversionNewBusinessLogic
    private let router: ConversionNewRoutingLogic
    
    private var controller: ConversionNewViewController { view as! ConversionNewViewController }
    
    init(view: ConversionNewDisplayLogic, interactor: ConversionNewBusinessLogic, router: ConversionNewRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        
        view.setupSubviews()
        interactor.showError = { message in
            self.controller.showError(message: message)
        }
        interactor.showNotify = { message in
            self.showNotify(message: message)
        }
        fetchConvertionItems()
    }
    
    func viewWillAppear(_ animated: Bool) {
//        interactor.clear()
    }
    
    func fetchConvertionItems() {
        view.indicator(.start)
        interactor.fetchData { [weak self] in
            guard let view = self?.view else {return}
            view.indicator(.stop)
            if (self?.interactor.canReload ?? false) {
                view.tableView.reloadData()
            }
        }
    }
    
    func willChangeCards() {
        interactor.changeCards()
        reload(sections: .card, .calculate)
    }
    
    func selectCard(cardOrAccount: Any) {
        
        switch cardOrAccount {
        
        case is Card:
            router.presentCollectionPan(viewModels: interactor.cardViewModels, delegate: self, title: RS.lbl_cards.localized())
        case is Account:
            router.presentCollectionPan(viewModels: interactor.accountViewModels, delegate: self, title: RS.lbl_accounts.localized())
        default:
            break
        } 
    }
    
    func willSet(amount: String) {
        interactor.set(amount: amount)
        reload(sections: .calculate, .button)
    }
    
    func changeAmountCurrency() {
        interactor.changeAmountCurrency()
        reload(sections: .amount,.calculate)
    }
    
    func reload(sections: Section...) {
        let rowValues = sections.map(\.rawValue)
        view.tableView.reloadSections(.init(rowValues), with: .automatic)
    }
    
    func nextButtonClicked() {
        if interactor.isButtonEnabled {
            view.showSpinner()
            interactor.exchangePrepare { (success) in
                self.view.removeSpinner()
                if success {
                    self.showResultScreen()
                }
            }
        } else {
            interactor.showRequiredFields = true
            view.tableView.reloadData()
        }
        
    }
    
    func showResultScreen() {
        guard let token = interactor.exchangeResponse?.token else { return }
//        let router = ResultOperationStackRouter(.convertion(details: interactor.conversionDetails, token: token))
//        controller.navigationController?.pushViewController(router.viewController, animated: true)
        router.confimConvertion(details: interactor.conversionDetails, token: token)
    }
    
    func showNotify(message: String) {
        let notify = NotifyViewController(.custom(message: message)) {}
        controller.present(notify, animated: true, completion: nil)
        
    }
    
}


// MARK: -TablePresentationLogic

extension ConversionNewPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}

extension ConversionNewPresenter: CollectionPanModelViewDelegate {
    func card(didSelectViewModel viewModel: CollectionViewModel, at indexPath: IndexPath, with tag: Int) {
        interactor.didSelctCardOrAccount(viewModel: viewModel, at: indexPath)
        view.tableView.reloadData()
    }
}
