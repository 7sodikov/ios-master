//
//  ConversionNewInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import Rswift

protocol ConversionNewBusinessLogic: ConversionNewBase {
    var accountViewModels: [AccountCardCellViewModel] {get}
    var cardViewModels: [CardCellViewModel] {get}
    var viewModels: [[TableViewModel]] {get}
    var conversionDetails: [DetailsCellViewModel] {get}
    var canReload: Bool {get}
    var resultStatus: ResultStatusCellViewModel.Status {get}
    var exchangeResponse: ExchangePrepareResponse? {get}
    var showError: ((String)->Void)? {get set}
    var showNotify: ((String)->Void)? {get set}
    var isButtonEnabled: Bool {get}
    var showRequiredFields: Bool {get set}
    
    func clear()
    func set(amount: String)
    func changeAmountCurrency()
    func changeCards()
    func didSelctCardOrAccount(viewModel: CollectionViewModel, at indexPaht: IndexPath)
    func fetchData(completion: @escaping()->Void)
    func exchangePrepare(completion: @escaping (Bool)->())
}

class ConversionNewInteractor: ConversionNewBusinessLogic {
    
    var showError: ((String)->Void)? = nil
    var showNotify: ((String)->Void)? = nil
    
    var exchangeResponse: ExchangePrepareResponse? = nil
    var resultStatus: ResultStatusCellViewModel.Status = .success(message: RS.lbl_payment_success.localized())
    
    var history: HistoryResponse? = nil
    var cards: [Card] = .init()
    var accounts: [Account] = .init()
    var cardBalances: [CardBalance] = .init()
    var exchangeRates: [ExchangeRate] = .init()
    
    var isUzsAmountCurrency: Bool = false
    var amount: String = ""
    var fromAccount: Any? = nil
    var toAccount: Any? = nil
    
    var showRequiredFields = false
    var canReload: Bool {
        !cards.isEmpty && !accounts.isEmpty && !cardBalances.isEmpty && !exchangeRates.isEmpty
    }
    
    var convertedAmount: Double {
        let result = (isUzsAmountCurrency) ? totalAmount.foreign : totalAmount.uzb
        return round(result * 100) / 100
    }
    
    var totalAmount: (uzb: Double, foreign: Double) {
        let rate = self.rate
        guard let amountD = Double(amount), let buyRate = rate.buy, let cellRate = rate.cell else {
            return (.zero, .zero)
        }
        
        if isUzsAmountCurrency {
            let foreignAmount = currency.from == .uzs
                ? amountD / (Double(cellRate.course) / 100)
                : amountD / (Double(buyRate.course) / 100)
            return (amountD, foreignAmount)
        }
        let uzsAmount = currency.from == .uzs
            ? amountD * (Double(cellRate.course) / 100)
            : amountD * (Double(buyRate.course) / 100)
        
        return (uzsAmount, amountD)
    }
    
    var currency: (from: CurrencyType, to: CurrencyType, foreign: CurrencyType) {
        let from = currencyCode(account: fromAccount)
        let to = currencyCode(account: toAccount)
        let foreign = from == .uzs ? to : from
        return (from, to, foreign)
    }
    
    var isButtonEnabled: Bool {
        [Double(amount), fromAccount, toAccount].allSatisfy(\.isNotNull) && convertedAmount > 0
    }
    
    var rate: (buy: ExchangeRate?, cell: ExchangeRate?) {
        let rates = exchangeRates.filter{ $0.currency == currency.foreign.data.code }
        let buyRate = rates.first(where: { $0.courseType == "5" })
        let cellRate = rates.first(where: { $0.courseType == "4" })
        return (buyRate, cellRate)
    }
    
    
    init(history: HistoryResponse?) {
        self.history = history
        if let amount = history?.amount {
            self.amount = (Double(amount) / 100).description
        }
    }
    
    
    func clear() {
        amount = ""
        fromAccount = nil
        toAccount = nil
    }
    
    func changeCards() {
        let swapobjc = fromAccount
        fromAccount = toAccount
        toAccount = swapobjc
    }
    
    func didSelctCardOrAccount(viewModel: CollectionViewModel, at indexPaht: IndexPath) {
        switch viewModel {
        case is CardCellViewModel:
            if fromAccount is Card {
                fromAccount = (viewModel as? CardCellViewModel)?.card
            } else if toAccount is Card {
                toAccount = (viewModel as? CardCellViewModel)?.card
            }
            
        case is AccountCardCellViewModel:
            if fromAccount is Account {
                fromAccount = (viewModel as? AccountCardCellViewModel)?.account
            }
            
            if toAccount is Account {
                toAccount = (viewModel as? AccountCardCellViewModel)?.account
            }
        default: break
        }
    }
    
    func set(amount: String) {
        self.amount = amount
    }
    
    func changeAmountCurrency() {
        isUzsAmountCurrency.toggle()
    }
    
    private func currencyCode(account: Any?) -> CurrencyType {
        ((account as? Card)?.currency ?? (account as? Account)?.currency) ?? .uzs
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
}

// MARK: - ViewModels

extension ConversionNewInteractor {
    
    var conversionDetails: [DetailsCellViewModel] {
        guard let exchange = exchangeResponse else {
            return .init()
        }
        let cource: Double = currency.from == .uzs
            ? (Double(rate.cell?.course ?? 0) / 100)
            : (Double(rate.buy?.course ?? 0) / 100)
        
        let exchangeRate = cource.description.makeReadableAmount + " " + currency.foreign.data.label
        let arr = calculateViewModel.text.split(separator: "=").map { String($0) }
        return exchange.detaisViewModels(foreignCyrrencyAmount: arr[0],
                                         exchangeRate: exchangeRate,
                                         totalAmount: arr[1])
    }
    
    var accountViewModels: [AccountCardCellViewModel] {
        let currency = ((fromAccount as? Card) ?? (toAccount as? Card))?.currency ?? .uzs
        return accounts
            .filter { $0.currency != currency }
            .map { .init(account: $0, isBalanceHidden: false) }
    }
    
    var cardViewModels: [CardCellViewModel] {
        let currency = ((fromAccount as? Account) ?? (toAccount as? Account))?.currency ?? .uzs
        return cards
            .filter { $0.currency != currency }
            .map { .init(card: $0) }
    }
    
    var viewModels: [[TableViewModel]]  {
        cardViewModel.isEmpty
            ? []
            : Section.allCases.map { (section) -> [TableViewModel] in
                switch section {
                
                case .card:
                    return cardViewModel
                case .amount:
                    return [amountViewModel].map {
                        $0.showMessage = showRequiredFields
                        return $0
                    }
                case .currency:
                    return [currencyViewModel]
                case .calculate:
                    return [calculateViewModel]
                case .button:
                    return [buttonViewModel]
                }
            }
    }
    
    private var cardViewModel: [ConversionCardCellViewModel] {
        guard let topItem = fromAccount, let bottomItem = toAccount else {
            return .init()
        }
        return [.init(topCard: topItem, bottomCard: bottomItem)]
    }
    
    private var amountViewModel: ConversionAmountCellViewModel {
        let currency = isUzsAmountCurrency ? CurrencyType.uzs : self.currency.foreign
        return .init(amount: amount, currency: currency.data.label)
    }
    
    private var currencyViewModel: ConvertionCurrencyCellViewModel {
        let rate = self.rate
        let buy = formattedMoney(rate.buy?.course) ?? .init()
        let cell = formattedMoney(rate.cell?.course) ?? ""
        let currencyLabel = self.currency.foreign.data.label
        let flag = rate.cell?.flag ?? ""
        
        return .init(uzsToUsd: cell, usdToUzs: buy, currency: currencyLabel, flag: flag)
    }
    
    private var calculateViewModel: SingleLabelCellViewModel {
        var result = amount.isEmpty ? "0.0" : amount.makeReadableAmount
        if isUzsAmountCurrency {
            result += " \(CurrencyType.uzs.data.label)"
                + " = "
                + convertedAmount.description.makeReadableAmount
                + " \(self.currency.foreign.data.label)"
        } else {
            result += " \(self.currency.foreign.data.label)"
                + " = "
                + convertedAmount.description.makeReadableAmount
                + " \(CurrencyType.uzs.data.label)"
        }
        return .init(text: result)
    }
    
    private var buttonViewModel: ButtonCellViewModel {
        .init(style: .red(title: RS.btn_further.localized()), isEnabled: true, tag: 0)
    }
    
}

// MARK: - Networking

extension ConversionNewInteractor {
    
    func exchangePrepare(completion: @escaping (Bool)->()) {
        
        var operationType: OperationPrepareType = .p2a
        var sender: String? = nil
        var senderId: String? = nil
        var senderBranch: String? = nil
        var receiver: String? = nil
        var receiverId: String? = nil
        var receiverBranch: String? = nil
        let paramNum: Int64? = nil
        let paramStr: String? = nil
        let paramBool: Bool? = nil
        var amount: Double = self.totalAmount.foreign
        
        amount *= 100
        
        if let account = fromAccount as? Account, let card = toAccount as? Card {
            operationType = .acp
            sender = account.number
            senderBranch = account.branch
            receiverId = card.cardId
        } else if let card = fromAccount as? Card, let account = toAccount as? Account  {
            operationType = .pca
            senderId = card.cardId
            receiver = account.number
            receiverBranch = card.branch
        }
        
        NetworkService.Exchange.exchangePrepare(prepareType: operationType,
                                                sender: sender,
                                                senderId: senderId,
                                                senderBranch: senderBranch,
                                                receiver: receiver,
                                                receiverId: receiverId,
                                                receiverBranch: receiverBranch,
                                                paramNum: paramNum,
                                                paramStr: paramStr,
                                                paramBool: paramBool,
                                                amount: amount) { (response) in
            switch response {
            case let .success(result):
                self.exchangeResponse = result.data
                completion(true)
            case let .failure(error):
                if case let .standard(description, code) = error, code == 422 {
                    self.showNotify?(description ?? "")
                } else {
                    self.showError?(error.localizedDescription)
                }
               
                completion(false)
            }
        }
    }
    
    func fetchData(completion: @escaping ()->Void) {
        func configure() {
            cards = cards.map { (item) -> Card in
                var card = item
                if let cardBalance = cardBalances.first(where: { $0.cardId == card.cardId }) {
                    card.update(balance: cardBalance.balance, currency: cardBalance.currency)
                }
                return card
            }
            switch history?.operationMode {
            case .pca:
                fromAccount = cards.first(where: { $0.cardId == history?.senderId?.description })
                toAccount = accounts.first(where: { $0.number == history?.receiver })
            case .acp:
                toAccount = cards.first(where: { $0.cardId == history?.receiverId?.description })
                fromAccount = accounts.first(where: { $0.number == history?.sender })
            default:
                let from = cards.first
                let to = accounts.first(where: { $0.currency != from?.currency })
                
                fromAccount = from
                toAccount = to
            }
        }
        
        let group = DispatchGroup()
        
        let items = [fetchExchangeRates, fetchCardList, fetchCardBalance, fetchAccountList]
        items.forEach { (itemAction) in
            group.enter()
            itemAction(group.leave)
        }
        
        group.notify(queue: .main) {
            configure()
            completion()
        }
    }
    
    private func fetchExchangeRates(completion: @escaping()->Void) {
        NetworkService.Info.getMainExchangeRates { (response) in
            
            switch response {
            case .success(let result):
                self.exchangeRates = result.data.exchangeRates?.map(\.exchangeRate) ?? []
                
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    private func fetchCardList(completion: @escaping()->Void) {
        NetworkService.Card.cardsList { (response) in
            switch response {
            
            case .success(let result):
                self.cards = result.data.cards.compactMap(\.card)
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    private func fetchCardBalance(completion: @escaping()->Void) {
        NetworkService.Card.cardBalance { (response) in
            switch response {
            case .success(let result):
                self.cardBalances = result.data.cards?.map(\.cardBalance) ?? []
                
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    private func fetchAccountList(completion: @escaping()->Void) {
        NetworkService.Accounts.accountsList { (response) in
            switch response {
            case .success(let result):
                self.accounts = result.data.accounts.map(\.account)
                
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
}

fileprivate extension Optional {
    var isNotNull: Bool {
        self != nil
    }
}
