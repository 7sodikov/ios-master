//
//  ConversionNewViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ConversionNewViewInstaller: ViewInstaller {
    var backgroundImageView: UIImageView! {get set}
    var topLeftButton: UIButton! {get set}
    var headerLabel: UILabel! {get set}
    var topRightButton: UIButton! {get set}
    var tableView: UITableView! {get set}
    var indicator: UIActivityIndicatorView! {get set}
    
    func indicator(_ action: ConversionNewViewController.IndicatorAnimator)
}

extension ConversionNewViewInstaller {
    
    func initSubviews() {
        backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "img_dash_light_background")
        
        topLeftButton = .init()
        topLeftButton.setImage(UIImage(named: "btn_prev"), for: .normal)
        
        headerLabel = UILabel()
        headerLabel.font = EZFontType.regular.gothamText(size: 16)
        headerLabel.textAlignment = .center
        
        topRightButton = UIButton()
        topRightButton.setImage(UIImage(named: "img_icon_home"), for: .normal)
        
        tableView = UITableView()
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = .init()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.allowsSelection = false
        tableView.isHidden = true
        tableView.rowHeight = UITableView.automaticDimension
        
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        } else {
            indicator = UIActivityIndicatorView()
        }

    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImageView)
        mainView.addSubview(tableView)
        mainView.addSubview(indicator)
    }
    
    func addSubviewsConstraints() {
        backgroundImageView.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
            
        tableView.constraint { (make) in
            make.top(Adaptive.val(32)).horizontal.bottom.equalToSuperView()
        }
        
        indicator.constraint { $0.center.equalToSuperView() }
    }
    
    func indicator(_ action: ConversionNewViewController.IndicatorAnimator) {
        if case .start = action {
            tableView.isHidden = true
            indicator.startAnimating()
        } else {
            tableView.isHidden = false
            indicator.stopAnimating()
        }
    }
}
