//
//  ConversionNewRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ConversionNewRoutingLogic {
    
    func confimConvertion(details: [DetailsCellViewModel], token: String)
    func presentCollectionPan(viewModels: [CollectionViewModel], delegate: CollectionPanModelViewDelegate, title: String)
}


class ConversionNewRouter: BaseRouter, ConversionNewRoutingLogic {
    
    
    init(history: HistoryResponse? = nil) {
        let controller = ConversionNewViewController()
        super.init(viewController: controller)
        
        let interactor = ConversionNewInteractor(history: history)
        let presenter = ConversionNewPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func presentCollectionPan(viewModels: [CollectionViewModel], delegate: CollectionPanModelViewDelegate, title: String) {
        let controller = CollectionPanModelViewController(allCards: viewModels, delegate: delegate, headerTitle: title)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = viewController as? ConversionNewViewController
        viewController.present(controller, animated: true)
    }
    
    func confimConvertion(details: [DetailsCellViewModel], token: String) {
        let router = MoneyTransferConfirmRouter(mode: .conversion(details: details, token: token))
        push(router, animated: true)
    }
    
}
