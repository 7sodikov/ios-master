//
//  ConversionCardCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ConversionCardCellDelegate: class {
    func changeButtonClicked()
    func topCardButtonClicked(accountOrCard: Any)
    func bottomCardButtonClicked(accountOrCard: Any)
}


class ConversionCardCell: TableViewCell, ConversionCardCellViewInstaller {
    
    var topCard: CardView!
    var bottomCard: CardView!
    var changeButton: UIButton!
    var directionIcon: UIImageView!
    
    var mainView: UIView { self.contentView }
    
    private var viewModel: ConversionCardCellViewModel? = nil
    weak var conversionDelegate: ConversionCardCellDelegate? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTarget()
    }
    
    private func setupTarget() {
        topCard.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(topCardButtonClicked)))
        bottomCard.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bottomCardButtonClicked)))
        changeButton.addTarget(self, action: #selector(changeButtonClicked), for: .touchUpInside)
        topCard.button.addTarget(self, action: #selector(topCardButtonClicked), for: .touchUpInside)
        bottomCard.button.addTarget(self, action: #selector(bottomCardButtonClicked), for: .touchUpInside)
    }
    
    @objc func changeButtonClicked() {
        self.conversionDelegate?.changeButtonClicked()
    }
    
    @objc func topCardButtonClicked() {
        if let topItem = viewModel?.topCard {
            conversionDelegate?.topCardButtonClicked(accountOrCard: topItem)
        }
    }
    
    @objc func bottomCardButtonClicked() {
        if let bottomItem = viewModel?.bottomCard {
            conversionDelegate?.bottomCardButtonClicked(accountOrCard: bottomItem)
        }
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ConversionCardCellViewModel else { return }
        self.viewModel = viewModel
        
        configureTop(account: viewModel.topCard)
        configureBottom(account: viewModel.bottomCard)
    }
    
    private func configureTop(account: Any) {
        if let card = account as? Card {
            topCard.configure(with: card)
        } else
        
        if let account = account as? Account {
            topCard.configure(with: account)
        }
    }
    
    private func configureBottom(account: Any) {
        if let card = account as? Card {
            bottomCard.configure(with: card)
        } else
        
        if let account = account as? Account {
            bottomCard.configure(with: account)
        }
    }
    
}


extension ConversionCardCell {
    
    class CardView: UIView {
        
        var logoImageView: UIImageView = .init()
        var topLabel: UILabel = .init()
        var bottomLabel: UILabel = .init()
        var button: UIButton = .init()
        
        private var stackView: UIStackView = .init()
        
        init() {
            super.init(frame: .zero)
            
            layer.borderWidth = 0.3
            layer.borderColor = UIColor.lightGray.cgColor
            layer.shadowColor = UIColor.lightGray.cgColor
            layer.shadowOpacity = 0.5
            layer.shadowOffset = .zero
            layer.shadowRadius = 6
            
            setupSubviews()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        func set(imageUrl: String, cardNumber number: String, amount: String) {
            if !imageUrl.isEmpty {
                logoImageView.kf.setImage(with: URL(string: imageUrl), placeholder: nil)
            } else {
                logoImageView.image = UIImage(named: "img_Icones_ZiraatBank2")
            }
            
            topLabel.text = number
            bottomLabel.text = amount
        }
        
        private func setupSubviews() {
            backgroundColor = .white
            layer.cornerRadius = 6
            
            topLabel.font = .gothamNarrow(size: 16, .book)
            bottomLabel.font = .gothamNarrow(size: 18, .bold)
            button.setImage(UIImage(named: "btn_up_down"), for: .normal)
            if #available(iOS 13.0, *) {
                button.setImage(UIImage(named: "btn_up_down")?.withTintColor(.lightGray), for: .highlighted)
            }
            
            stackView.axis = .vertical
            stackView.spacing = 5
            
            stackView.addArrangedSubview(topLabel)
            stackView.addArrangedSubview(bottomLabel)
            
            addSubview(logoImageView)
            addSubview(stackView)
            addSubview(button)
            
            logoImageView.constraint { (make) in
                make.square(28).centerY.leading(16).equalToSuperView()
            }
            
            button.constraint { (make) in
                make.square(24).centerY.trailing(-16).equalToSuperView()
            }
            
            stackView.constraint { (make) in
                make.leading(self.logoImageView.trailingAnchor, offset: 12)
                    .trailing(self.button.leadingAnchor, offset: -12)
                    .centerY.equalToSuperView()
            }
        }
        
    }
    
}


fileprivate extension ConversionCardCell.CardView {
    
    func configure(with card: Card) {
        set(imageUrl: card.additions.type,
            cardNumber: card.pan.makeReadableCardNumber,
            amount: format(amount: card.balance, currency: card.currency))
    }
    
    func configure(with account: Account) {
        set(imageUrl: .init(),
            cardNumber: account.number,
            amount: format(amount: account.balance, currency: account.currency))
    }
    
    private func format(amount: Double, currency: CurrencyType) -> String {
        amount.description.makeReadableAmount + " " + currency.data.label
    }
    
}
