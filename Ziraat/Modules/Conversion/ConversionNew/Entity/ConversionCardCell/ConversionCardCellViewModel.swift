//
//  ConversionCardCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation


class ConversionCardCellViewModel: TableViewModel {

    override var cellType: TableViewCell.Type {
        ConversionCardCell.self
    }
    
    let topCard, bottomCard: Any
    
    internal init(topCard: Any, bottomCard: Any) {
        self.topCard = topCard
        self.bottomCard = bottomCard
    }
    
}


extension ConversionCardCellViewModel {
    
    struct Item {
        let logo, id, number, amount: String
        let type: ItemType
        
        internal init(logo: String, id: String, number: String, amount: String, type: ItemType) {
            self.logo = logo
            self.id = id
            self.number = number
            self.amount = amount
            self.type = type
        }
    }
    
    enum ItemType {
        case card
        case account
    }
    
}
