//
//  ConversionCardCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol ConversionCardCellViewInstaller: ViewInstaller {
    typealias Card = ConversionCardCell.CardView

    var topCard: Card! {get set}
    var bottomCard: Card! {get set}
    var changeButton: UIButton! {get set}
    var directionIcon: UIImageView! {get set}
}

extension ConversionCardCellViewInstaller {
    
    func initSubviews() {
        topCard = .init()
        bottomCard = .init()
        
        changeButton = .init()
        changeButton.setImage(UIImage(named: "btn_converse"), for: .normal)
        
        directionIcon = .init()
        directionIcon.image = UIImage(named: "btn_down")
    }
    
    func embedSubviews() {
        mainView.addSubview(topCard)
        mainView.addSubview(directionIcon)
        mainView.addSubview(changeButton)
        mainView.addSubview(bottomCard)
    }
    
    func addSubviewsConstraints() {
        topCard.constraint { (make) in
            make.height(74).top.horizontal(16).equalToSuperView()
        }
        
        directionIcon.constraint { (make) in
            make.height(Adaptive.val(21)).height(Adaptive.val(12)).center.equalToSuperView()
        }
        
        changeButton.constraint { (make) in
            make.square(Adaptive.val(40))
                .top(self.topCard.bottomAnchor, offset: 16)
                .trailing(-16).equalToSuperView()
        }
        
        bottomCard.constraint {
            $0.height(74).top(self.changeButton.bottomAnchor, offset: 16)
                .horizontal(16).bottom.equalToSuperView()
        }
    }
    
}
