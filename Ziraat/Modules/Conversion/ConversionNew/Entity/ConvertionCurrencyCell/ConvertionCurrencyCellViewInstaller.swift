//
//  ConvertionCurrencyCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/17/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ConvertionCurrencyCellViewInstaller: ViewInstaller {
    typealias CurrencyExchangeView = ConvertionCurrencyCell.CurrencyExchangeView
    var container: UIView! {get set}
    var topExchangeView: CurrencyExchangeView! {get set}
    var bottomExchangeView: CurrencyExchangeView! {get set}
    var seperator: UIView! {get set}
    var stackView: UIStackView! {get set}
}

extension ConvertionCurrencyCellViewInstaller {
    
    func initSubviews() {
        container = .init()
        container.backgroundColor = .white
        container.layer.cornerRadius = 6
        container.layer.borderWidth = 0.3
        container.layer.borderColor = UIColor.lightGray.cgColor
        container.layer.shadowColor = UIColor.lightGray.cgColor
        container.layer.shadowOpacity = 0.3
        container.layer.shadowOffset = .zero
        container.layer.shadowRadius = 6
        
        topExchangeView = .init()
        bottomExchangeView = .init()
        
        seperator = UIView()
        seperator.backgroundColor = UIColor(171, 181, 183)
        seperator.translatesAutoresizingMaskIntoConstraints = false
        
        stackView = UIStackView()
        stackView.axis = .vertical
    }
    
    func embedSubviews() {
        mainView.addSubview(container)
        container.addSubview(stackView)
        stackView.addArrangedSubview(topExchangeView)
        stackView.addArrangedSubview(bottomExchangeView)
        stackView.addSubview(seperator)
    }
    
    func addSubviewsConstraints() {
        container.constraint { (make) in
            make.height(105).vertical.horizontal(16).equalToSuperView()
        }
        
        let width = UIScreen.main.bounds.width - 64
        seperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        seperator.widthAnchor.constraint(equalToConstant: width).isActive = true
        seperator.constraint { (make) in
            make.height(1).horizontal(16).centerY.equalToSuperView()
        }
    
        bottomExchangeView.heightAnchor.constraint(equalToConstant: 52).isActive = true
        stackView.constraint { (make) in
            make.edges.equalToSuperView()
        }
        
    }
    
    func localizeText() {
        
    }
    
}
