//
//  ConvertionCurrencyCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/17/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation


class ConvertionCurrencyCellViewModel: TableViewModel {
    
//    typealias Item = (amount: String, currency: String, flag: String)
    
    override var cellType: TableViewCell.Type {
        ConvertionCurrencyCell.self
    }
    
//    var from, to: Item
    
//    internal init(from: Item, to: Item) {
//        self.from = from
//        self.to = to
//    }
    
    let uzsToUsd, usdToUzs, currency, flag: String
    
    init(uzsToUsd: String, usdToUzs: String, currency: String, flag: String) {
        self.uzsToUsd = uzsToUsd
        self.usdToUzs = usdToUzs
        self.currency = currency
        self.flag = flag
    }
    
    var amount: (uzsToUsd: String, usdToUzs: String) {
        let f = uzsToUsd + " UZS"
        let t = usdToUzs + " UZS"
        return (f, t)
    }
    
    var uzsFlag: (image: UIImage?, currency: String) {
        (UIImage(named: "img_uzs"), " UZS")
    }
    
    var foreignFlag: (flag: String, currency: String) {
        (flag, currency)
    }
}
