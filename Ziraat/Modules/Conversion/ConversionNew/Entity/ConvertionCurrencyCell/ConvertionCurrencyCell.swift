//
//  ConvertionCurrencyCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/17/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import Kingfisher

class ConvertionCurrencyCell: TableViewCell, ConvertionCurrencyCellViewInstaller {
    
    var container: UIView!
    var topExchangeView: CurrencyExchangeView!
    var bottomExchangeView: CurrencyExchangeView!
    var seperator: UIView!
    var stackView: UIStackView!
    var mainView: UIView { self }
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTarget()
        localizeText()
    }
    
    private func setupTarget() {
       
    }
    
    @objc func currencyButtonClicked() {
       
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ConvertionCurrencyCellViewModel else { return }
        
       configure(viewModel: viewModel)
    }
    
    private func configure(viewModel: ConvertionCurrencyCellViewModel) {
        
        topExchangeView.fromCurrencyLogo
            .imageView.image = viewModel.uzsFlag.image
        topExchangeView.fromCurrencyLogo.titleLabel.text = viewModel.uzsFlag.currency
        
        topExchangeView.toCurrencyLogo
            .imageView.kf.setImage(with: URL(string: viewModel.foreignFlag.flag), placeholder: nil)
        topExchangeView.toCurrencyLogo.titleLabel.text = viewModel.foreignFlag.currency
        topExchangeView.valueLabel.text = viewModel.amount.uzsToUsd
        
        
        bottomExchangeView.toCurrencyLogo
            .imageView.image = viewModel.uzsFlag.image
        bottomExchangeView.toCurrencyLogo.titleLabel.text = viewModel.uzsFlag.currency
        
        bottomExchangeView.fromCurrencyLogo
            .imageView.kf.setImage(with: URL(string: viewModel.foreignFlag.flag), placeholder: nil)
        bottomExchangeView.fromCurrencyLogo.titleLabel.text = viewModel.foreignFlag.currency
        bottomExchangeView.valueLabel.text = viewModel.amount.usdToUzs
    }

  
}

extension ConvertionCurrencyCell {
    
    class FlagView: UIView {
        var imageView = UIImageView()
        var titleLabel = UILabel()
        
        override init(frame: CGRect) {
            super.init(frame: .zero)
            
            imageView.layer.masksToBounds = true
            imageView.isUserInteractionEnabled = true
            imageView.layer.cornerRadius = 10
            imageView.layer.borderWidth = 0.3
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.layer.shadowColor = UIColor.lightGray.cgColor
            imageView.layer.shadowOpacity = 0.3
            imageView.layer.shadowOffset = .zero
            imageView.layer.shadowRadius = 6
            
            titleLabel.font = .gothamNarrow(size: 16, .book)
            titleLabel.textColor = .black
            titleLabel.textAlignment = .left
            
            let stackView = UIStackView(arrangedSubviews: [imageView, titleLabel])
            stackView.spacing = 4
            
            addSubview(stackView)
            imageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
            imageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
            stackView.constraint { (make) in
                make.width(64).height(20).horizontal.vertical.equalToSuperView()
            }
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func set(flag: UIImage, label: String) {
            imageView.image = flag
            titleLabel.text = label
        }
        
        func set(flagUrl: String, label: String) {
            imageView.kf.setImage(with: URL(string: flagUrl), placeholder: nil)
            titleLabel.text = label
        }
        
    }
    
    class CurrencyExchangeView: UIView {

        internal var fromCurrencyLogo: FlagView = .init()
        internal var toCurrencyLogo = FlagView()
        internal var directionIcon = UIImageView()
        internal var stackView = UIStackView()
        
        var valueLabel: UILabel = .init()
        
        init() {
            super.init(frame: .zero)
            
           setupSubviews()
        }
        
        private func setupSubviews() {
            backgroundColor = .clear
            
            directionIcon.image = UIImage(named: "btn_next")
            
            stackView.spacing = Adaptive.val(20)
            stackView.backgroundColor = .clear
            
            valueLabel.font = .gothamNarrow(size: 16, .medium)
            valueLabel.textAlignment = .right
            valueLabel.textColor = .black
            
            addSubview(fromCurrencyLogo)
            addSubview(directionIcon)
            addSubview(toCurrencyLogo)
            addSubview(stackView)
            addSubview(valueLabel)
            
            fromCurrencyLogo.constraint { (make) in
                make.leading(16).centerY.equalToSuperView()
            }
            
            directionIcon.constraint { (make) in
                make.height(14).width(9)
                    .leading(self.fromCurrencyLogo.trailingAnchor, offset: Adaptive.val(20))
                    .centerY(self.fromCurrencyLogo).equalToSuperView()
            }
            
            toCurrencyLogo.constraint { (make) in
                make.leading(self.directionIcon.trailingAnchor, offset: Adaptive.val(20))
                    .centerY(self.fromCurrencyLogo).equalToSuperView()
            }
            
            valueLabel.constraint { (make) in
                make.leading(self.toCurrencyLogo.trailingAnchor, offset: Adaptive.val(20))
                    .centerY(self.fromCurrencyLogo).trailing(-16).equalToSuperView()
                
            }
            
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func set(
            from: (flag: String, currency: String),
            to: (flag: String, currency: String),
            amount: String
        ) {
            
            fromCurrencyLogo.set(flagUrl: from.flag, label: from.currency)
            toCurrencyLogo.set(flagUrl: to.flag, label: to.currency)
            valueLabel.text = amount
        }
    }
    
}
