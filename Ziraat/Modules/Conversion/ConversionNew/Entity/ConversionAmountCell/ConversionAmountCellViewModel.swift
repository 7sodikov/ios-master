//
//  ConversionAmountCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class ConversionAmountCellViewModel: TableViewModel {
    
    override var cellType: TableViewCell.Type {
        ConversionAmountCell.self
    }
    
    var amount: String
    var currency: String
    var headerTitle: String
    var showMessage: Bool = false
    
    init(amount: String, currency: String, headerTitle: String = RS.lbl_amount.localized()) {
        self.headerTitle = headerTitle
        self.currency = currency
        self.amount = amount
    }
}
