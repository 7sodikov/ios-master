//
//  ConversionAmountCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ConversionAmountCellDelegate: class {
    func changeCurrency()
    func input(amount: String)
}

class ConversionAmountCell: TableViewCell, ConversionAmountCellViewInstaller {
    var textContainer: UIView!
    var stackView: UIStackView!
    var titleLabel: UILabel!
    var container: UIView!
    var textField: UITextField!
    var currencyButton: UIButton!
    var messageLabel: UILabel!
    var mainView: UIView { self.contentView }
    
    var amount: String? = nil
    weak var amountCellDelegate: ConversionAmountCellDelegate? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTarget()
//        localizeText()
    }
    
    private func setupTarget() {
        currencyButton.addTarget(self, action: #selector(currencyButtonClicked), for: .touchUpInside)
        textField.addTarget(self, action: #selector(didEnterAmount(_:)), for: .editingChanged)
//        textField.becomeFirstResponder()
    }
    
    @objc func currencyButtonClicked(_ sender: UIButton) {
        self.amountCellDelegate?.changeCurrency()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ConversionAmountCellViewModel else { return }
        amount = viewModel.amount
        titleLabel.text = viewModel.headerTitle
        textField.text = viewModel.amount
        currencyButton.setTitle(viewModel.currency, for: .normal)
        messageLabel.isHidden = !viewModel.showMessage
    }
    
    @objc private func didEnterAmount(_ sender: UITextField) {
        guard var amount = sender.text?.withoutWhiteSpace.replacingOccurrences(of: ",", with: ".") else { return }
        textField.text = amount.makeReadableAmount
        
        if let index = amount.lastIndex(of: "."), amount[index...].count > 3 {
            amount.removeLast()
            textField.text = amount.makeReadableAmount
        }
        amountCellDelegate?.input(amount: amount)
    }
    
}
