//
//  ConversionAmountCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ConversionAmountCellViewInstaller: ViewInstaller {
    var titleLabel: UILabel! {get set}
    var container: UIView! {get set}
    var textContainer: UIView! {get set}
    var textField: UITextField! {get set}
    var currencyButton: UIButton! {get set}
    var stackView: UIStackView! {get set}
    var messageLabel: UILabel! {get set}
}

extension ConversionAmountCellViewInstaller {
    
    func initSubviews() {
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 18, .book)
        
        container = .init()
        container.backgroundColor = .clear
        
        textContainer = UIView()
        textContainer.backgroundColor = .white
        textContainer.layer.cornerRadius = 6
        textContainer.layer.borderWidth = 0.3
        textContainer.layer.borderColor = UIColor.lightGray.cgColor
        textContainer.layer.shadowColor = UIColor.lightGray.cgColor
        textContainer.layer.shadowOpacity = 0.3
        textContainer.layer.shadowOffset = .zero
        textContainer.layer.shadowRadius = 6
        
        textField = UITextField()
        textField.layer.borderWidth = 0
        textField.backgroundColor = .clear
        textField.placeholder = RS.lbl_amount.localized()
        textField.keyboardType = .decimalPad
        textField.font = .gothamNarrow(size: 16, .medium)
        
        currencyButton = UIButton()
        currencyButton.layer.cornerRadius = 3
        currencyButton.semanticContentAttribute = .forceRightToLeft
        currencyButton.setImage(UIImage(named: "btn_changeRate"), for: .normal)
        currencyButton.backgroundColor = UIColor(225,5,20)
        currencyButton.setTitleColor(.white, for: .normal)
        currencyButton.imageEdgeInsets.left = 10
        currencyButton.titleEdgeInsets.left = -10
        
        messageLabel = UILabel()
        messageLabel.font = .gothamNarrow(size: 14, .book)
        messageLabel.textColor = UIColor(225,5,20)
        messageLabel.text = RS.lbl_tip.localized()
        
        stackView = UIStackView()
        stackView.spacing = 12
        stackView.axis = .vertical
        stackView.backgroundColor = .clear
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(container)
        
        container.addSubview(textContainer)
        container.addSubview(messageLabel)
        
        textContainer.addSubview(textField)
        textContainer.addSubview(currencyButton)
    }
    
    func addSubviewsConstraints() {
        stackView.constraint { $0.horizontal(16).vertical.equalToSuperView() }
        container.constraint({ $0.height(68) })
        textContainer.constraint { $0.height(50).top.horizontal.equalToSuperView() }
        textField.constraint { (make) in
            make.trailing(self.currencyButton.leadingAnchor, offset: -16)
                .centerY.leading(16).equalToSuperView()
        }
        currencyButton.constraint { $0.width(82).vertical(4).trailing(-4).equalToSuperView() }
        messageLabel.constraint { (make) in
            make.top(self.textContainer.bottomAnchor).leading.trailing.bottom.equalToSuperView()
        }
    }
    
    func localizeText() {
        titleLabel.text = RS.lbl_amount.localized()
    }
    
}
