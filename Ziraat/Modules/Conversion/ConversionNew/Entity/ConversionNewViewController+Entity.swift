//
//  ConversionNewViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ConversionNewBase {
    typealias Section = ConversionNewViewController.Section
    typealias IndicatorAnimator  = ConversionNewViewController.IndicatorAnimator
}


extension ConversionNewViewController {
    
    enum Section: Int, CaseIterable {
        case card
        case amount
        case currency
        case calculate
        case button
    }

    enum IndicatorAnimator {
        case start, stop
    }
}
