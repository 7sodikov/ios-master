//
//  ConversionNewViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol ConversionNewDisplayLogic: ConversionNewViewInstaller {
    
    func setupTargets()
    func showSpinner()
    func removeSpinner()
}


class ConversionNewViewController: BaseViewController, ConversionNewDisplayLogic, UIViewControllerTransitioningDelegate {
    
    var backgroundImageView: UIImageView!
    var topLeftButton: UIButton!
    var headerLabel: UILabel!
    var topRightButton: UIButton!
    var tableView: UITableView!
    var indicator: UIActivityIndicatorView!
    
    var mainView: UIView { self.view }
    var presenter: ConversionNewPresentationLogic!
    
    override func loadView() {
        super.loadView()
        
        navigationController?.transparentBackgorund()
        title = RS.lbl_conversion.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        setupTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        presenter.viewWillAppear(animated)
    }
    
    override func setupTargets() {
        super.setupTargets()
        tableView.delegate = self
        tableView.dataSource = self
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        
        tableView.register(TableViewCell.self)
        tableView.register(ConversionAmountCell.self)
        tableView.register(ConversionCardCell.self)
        tableView.register(ConvertionCurrencyCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(SingleLabelCell.self)
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentController = PresentationController(presentedViewController: presented, presenting: presenting)
        if presented is CollectionPanModelViewController {
            presentController.presentContainerRect = CGRect(x: 0, y: UIScreen.main.bounds.height - 320, width: UIScreen.main.bounds.width, height: 320)
        }
        return presentController
    }
    
}


extension ConversionNewViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        configure(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0  {
            //16
            return CGFloat.leastNormalMagnitude
        }
        else if section == 1 {
            return 5
        }
        else if section == 2 {
            return 5
        }
        else if section == 3 {
            return 0
        }
        
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    private func configure(cell: TableViewCell) {
        (cell as? ConversionCardCell)?.conversionDelegate = self
        (cell as? ButtonCell)?.buttonDelegate = self
        (cell as? ConversionAmountCell)?.amountCellDelegate = self
    }
    
}

extension ConversionNewViewController: ConversionCardCellDelegate {
    func topCardButtonClicked(accountOrCard: Any) {
        presenter.selectCard(cardOrAccount: accountOrCard)
    }
    
    func bottomCardButtonClicked(accountOrCard: Any) {
        presenter.selectCard(cardOrAccount: accountOrCard)
    }
    
    
    func changeButtonClicked() {
        presenter.willChangeCards()
    }
    
}

extension ConversionNewViewController: ButtonCellDelegate {
    
    func buttonClicked(tag: Int) {
        presenter.nextButtonClicked()
    }
    
}

extension ConversionNewViewController: ConversionAmountCellDelegate {
    
    func changeCurrency() {
        presenter.changeAmountCurrency()
    }

    func input(amount: String) {
        presenter.willSet(amount: amount)
    }
}

