//
//  OperationsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationsRouterProtocol: AnyObject {
    static func createModule(for favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?) -> UIViewController
}

class OperationsRouter: OperationsRouterProtocol {
    static func createModule(for favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?) -> UIViewController {
        let vc = OperationsVC()
        let presenter = OperationsPresenter()
        let interactor = OperationsInteractor()
        let router = OperationsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.favoriteItem = favoriteItem
        presenter.historyItem = historyItem
        interactor.presenter = presenter
        
        return vc
    }
    
}
