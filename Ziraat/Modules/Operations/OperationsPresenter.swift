//
//  OperationsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: OperationsVM { get }
    var operationType: OperationPrepareType! { get }
    var favoriteItem: FavoriteListOperationsResponse? { get }
    var historyItem: HistoryResponse? { get }
}

protocol OperationsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class OperationsPresenter: OperationsPresenterProtocol {
    weak var view: OperationsVCProtocol!
    var interactor: OperationsInteractorProtocol!
    var router: OperationsRouterProtocol!
    var operationType: OperationPrepareType!
    var favoriteItem: FavoriteListOperationsResponse?
    var historyItem: HistoryResponse?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationsVM()
    
    // Private property and methods
    
}

extension OperationsPresenter: OperationsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}

extension OperationsPresenter: OperationPageDataEntryDelegate {
    func dataEntryPageFinished(with prepareResponse: OperationPrepareResponse, operationPrepareType: OperationPrepareType) {
        let nextPage = view.getController(for: 1) as? OperationPageDataPreviewVC
        let nextPagePresenter = nextPage?.presenter as? OperationPageDataPreviewPresenter
        nextPagePresenter?.prepareResponse = prepareResponse
        nextPagePresenter?.operationPrepareType = operationPrepareType
        view.scroll(to: 1, direction: .forward)
    }
}

extension OperationsPresenter: OperationPageDataPreviewDelegate {
    func dataPreviewPageBackButtonClicked() {
        view.scroll(to: 0, direction: .reverse)
    }
    
    func dataPreviewPageFinished(with prepareResponse: OperationPrepareResponse) {
        let nextPage = view.getController(for: 2) as? OperationPageSMSConfirmationVC
        let nextPagePresenter = nextPage?.presenter as? OperationPageSMSConfirmationPresenter
        nextPagePresenter?.prepareResponse = prepareResponse
        nextPage?.startTimer()
        view.scroll(to: 2, direction: .forward)
    }
}

extension OperationsPresenter: OperationPageSMSConfirmationDelegate {
    func smsConfirmationPageBackButtonClicked() {
        view.scroll(to: 1, direction: .reverse)
    }
    
    func smsConfirmationPageFinished(with smsConfirmResponse: P2PConfirmResponse) {
        let nextPage = view.getController(for: 3) as? OperationPageCompletionVC
        let nextPagePresenter = nextPage?.presenter as? OperationPageCompletionPresenter
        nextPagePresenter?.smsConfirmResponse = smsConfirmResponse
        view.scroll(to: 3, direction: .forward)
    }
}

extension OperationsPresenter: OperationPageCompletionDelegate {
    func completionPageCloseClicked() {
        view.closeButtonClicked()
    }
}
