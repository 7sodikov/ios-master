//
//  APDeleteConfirmmPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 22/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol APDeleteConfirmmPresentationLogic: PresentationProtocol, APDeleteConfirmmBase, TablePresentationLogic {
    
    func buttonClicked(isNext: Bool)
}


class APDeleteConfirmmPresenter: APDeleteConfirmmPresentationLogic {
    
    
    private unowned let view: APDeleteConfirmmDisplayLogic
    private let interactor: APDeleteConfirmmBusinessLogic
    private let router: APDeleteConfirmmRoutingLogic
    
    private var controller: APDeleteConfirmmViewController { view as! APDeleteConfirmmViewController }
    
    init(view: APDeleteConfirmmDisplayLogic, interactor: APDeleteConfirmmBusinessLogic, router: APDeleteConfirmmRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        view.setupTargets()
        
    }
    
    func buttonClicked(isNext: Bool) {
        isNext
            ? deleteAutoPayment()
            : self.router.back()
    }
    
    func deleteAutoPayment() {
        controller.spinner(.start)
        interactor.deleteAutoPayment {
            self.controller.spinner(.stop)
            self.router.backToList()
        }
    }
    
    
    func confirmate() {
        let controller = NotifyViewController(.confirmationConversion) {
            self.router.backToRoot()
        }
        self.controller.present(controller, animated: false)
    }
    
    func reloadTableView(at page: Page) {
        view.tableView.reloadData()
    }
}


// MARK: - TablePresentationLogic

extension APDeleteConfirmmPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}

// MARK: - Sending mail
extension APDeleteConfirmmPresenter {
    
}
