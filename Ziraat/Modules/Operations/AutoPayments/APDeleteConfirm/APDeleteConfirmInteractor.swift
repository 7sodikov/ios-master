//
//  APDeleteConfirmmInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 22/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APDeleteConfirmmBusinessLogic: APDeleteConfirmmBase {
    var viewModels: [[TableViewModel]] {get}
    func deleteAutoPayment(completion: @escaping () -> ())
}

class APDeleteConfirmmInteractor: APDeleteConfirmmBusinessLogic {
    
    let autoPayment: CAutoPayment
    var status: ResultStatusCellViewModel.Status = .aPDeleteWarning
    
    
    init(autoPayment: CAutoPayment) {
        self.autoPayment = autoPayment
    }
    
    
}

// MARK: - ViewModels
extension APDeleteConfirmmInteractor {
    var viewModels: [[TableViewModel]] {
        let vm1 = ResultStatusCellViewModel(status)
        return [[vm1]] + [buttonsViewModel]
    }
    
    private var buttonsViewModel: [ButtonCellViewModel] {
        let delete = RS.lbl_delete_auto_payment.localized()
        let cancel = RS.btn_cancel_operation.localized()
        return [
            .init(style: .red(title: delete), isEnabled: true, tag: 0),
            .init(style: .bordered(title: cancel), isEnabled: true, tag: 1)
        ]
    }
}

// MARK: - Networking
extension APDeleteConfirmmInteractor {
    
    func deleteAutoPayment(completion: @escaping () -> ()) {
        guard let id = autoPayment.autoPayID else {
            completion()
            return
        }
        NetworkService.AutoPayment().deleteAutoPayment(autoPayId: id) { _ in
            completion()
        }
    }
}

