//
//  APDeleteConfirmmViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 22/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol APDeleteConfirmmDisplayLogic: APDeleteConfirmmViewInstaller, APDeleteConfirmmBase {
    func setupTargets()
}


class APDeleteConfirmmViewController: BaseViewController, APDeleteConfirmmDisplayLogic  {
    
    var backgroundImage: UIImageView!
    var tableView: UITableView!
    
    var mainView: UIView { self.view }
    var presenter: APDeleteConfirmmPresentationLogic!
    
    
    override func loadView() {
        super.loadView()
        title = RS.lbl_automatic_payment.localized()
        navigationController?.transparentBackgorund()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func setupTargets() {
        super.setupTargets()
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(DetailCenteredCell.self)
        tableView.register(TableViewCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(ButtonsCell.self)
        tableView.register(ResultStatusCell.self)
        tableView.register(ConfirmationCell.self)
        tableView.register(DetailsCell.self)
        tableView.register(SingleLabelCell.self)
    }
    
}

extension APDeleteConfirmmViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        configure(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        section == 0 ? 64 : 16
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath) is DetailsCellViewModel {
            return presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath).estimatedRowHeight
        }
        return UITableView.automaticDimension
    }
    
    func configure(cell: UITableViewCell) {
        (cell as? ButtonCell)?.buttonDelegate = self
        (cell as? ButtonsCell)?.buttonDelegate = self
    }
}

extension APDeleteConfirmmViewController: ButtonsCellDelegate  {
    
    func buttonCellsButtonClicked(at tag: Int, isLeft: Bool) {
        presenter.buttonClicked(isNext: !isLeft)
    }
    
}

extension APDeleteConfirmmViewController: ButtonCellDelegate {
    
    func buttonClicked(tag: Int) {
        if let button = Button(rawValue: tag) {
            presenter.buttonClicked(isNext: button == .confirm)
        }
    }
    
}

