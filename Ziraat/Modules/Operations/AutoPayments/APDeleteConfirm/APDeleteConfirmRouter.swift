//
//  APDeleteConfirmmRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 22/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APDeleteConfirmmRoutingLogic {
    func back()
    func navigatetoPdfChequeVC(operationId: String)
    func backToRoot()
    func navigateToResultScreeen(autopaymentdata: AutoPaymentData)
    func backToList()
}

class APDeleteConfirmmRouter: BaseRouter, APDeleteConfirmmRoutingLogic {
    
    
    init(autoPayment: CAutoPayment) {
        let controller = APDeleteConfirmmViewController()
        super.init(viewController: controller)
        
        let interactor = APDeleteConfirmmInteractor(autoPayment: autoPayment)
        let presenter = APDeleteConfirmmPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigatetoPdfChequeVC(operationId: String) {
        let pdfView = PdfViewController(operation: operationId, title: viewController.title ?? "")
        viewController.navigationController?.pushViewController(pdfView, animated: true)
    }
    
    func backToRoot() {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    
    func navigateToResultScreeen(autopaymentdata: AutoPaymentData) {
        let router = APResultRouter(dataCollector: autopaymentdata)
        push(router, animated: true)
    }
    
    func backToList() {
        let controllers = navigationController?.viewControllers ?? []
        navigationController?.popToViewController(controllers[2], animated: true)
    }
}
