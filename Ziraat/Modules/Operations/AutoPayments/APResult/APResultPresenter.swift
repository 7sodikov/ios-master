//
//  APResultPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 17/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol APResultPresentationLogic: PresentationProtocol, TablePresentationLogic {
    
}


class APResultPresenter: APResultPresentationLogic {
    
    private unowned let view: APResultDisplayLogic
    private let interactor: APResultBusinessLogic
    private let router: APResultRoutingLogic
    
    private var controller: APResultViewController { view as! APResultViewController }
    
    
    init(view: APResultDisplayLogic, interactor: APResultBusinessLogic, router: APResultRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        
        view.setupSubviews()
        view.setupTargets()
        if let title = interactor.dataCollector.service?.title {
            view.update(title: title)
        }
        createAutoPayment()
    }
    
    func createAutoPayment() {
        controller.spinner(.start)
        interactor.createAutoPay {
            self.controller.spinner(.stop)
            self.view.tableView.isHidden = false
            self.view.tableView.reloadData()
        }
    }
}


// MARK: -TablePresentationLogic

extension APResultPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}
