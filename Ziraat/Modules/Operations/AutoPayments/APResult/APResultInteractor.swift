//
//  APResultInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 17/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APResultBusinessLogic {
    var dataCollector: AutoPaymentData {get}
    var viewModels: [[TableViewModel]] {get}
    
    func createAutoPay(completion: @escaping()->())
}

class APResultInteractor: APResultBusinessLogic {
    
    let dataCollector: AutoPaymentData
    var status: ResultStatusCellViewModel.Status = .failure(message: RS.lbl_UnknownError.localized())
    
    
    init(dataCollector autopaymentdata: AutoPaymentData) {
        self.dataCollector = autopaymentdata
    }
    
    var viewModels: [[TableViewModel]] {
        let vm1 = ResultStatusCellViewModel(status)
//        let vm2 = SingleLabelCellViewModel(text: RS.lbl_successful_auto_payment.localized(), textAlignment: .center)

        return [[vm1]] + [buttonsViewModel]
    }
    
    
    private var buttonsViewModel: [ButtonCellViewModel] {
        let title1 = RS.btn_return_to_list.localized()
        let title2 = RS.btn_return_to_homepage.localized()
    
        return [
            .init(style: .red(title: title1), isEnabled: true, tag: 1),
            .init(style: .red(title: title2), isEnabled: true, tag: 2)
        ]
    }
    
    
    func createAutoPay(completion: @escaping()->()) {
        guard let payerIdString = dataCollector.selectedCard?.cardId,
              let payerId = Int(payerIdString),
              let payerNumber = dataCollector.selectedCard?.pan,
              let payerBranch = dataCollector.selectedCard?.branch,
              let serviceId = dataCollector.service?.serviceID,
              let serviceType = dataCollector.service?.serviceType
               else {
            completion()
            return
        }
        let title = dataCollector.category.title
        let frequency = dataCollector.frequency
        let dayOf = dataCollector.dayOf
        let amount = dataCollector.amount
        let feilds: [String: String] = dataCollector.fields.reduce(into: .init()) { result, dic in
            let (uid, (_, value)) = dic
            let key = dataCollector.service?.fields.first(where: {$0.uid == uid})?.name ?? ""
            result[key] = value
        }
        
        NetworkService.AutoPayment().createAutoPayment(
            params: .init(
                title: title,
                payerID: payerId,
                payerNumber: payerNumber,
                payerBranch: payerBranch,
                serviceID: serviceId.description,
                serviceType: serviceType,
                frequency: frequency.rawValue,
                dayOf: dayOf,
                amount: amount,
                fields: feilds
            )
        ) { response in
            switch response {
            
            case .success:
                self.status = .success(message: RS.lbl_successful_auto_payment.localized())
            case .failure(let error):
                self.status = .failure(message: error.localizedDescription)
            }
            completion()
        }
    }
}
