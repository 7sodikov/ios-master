//
//  APResultRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 17/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APResultRoutingLogic {
    
}


class APResultRouter: BaseRouter, APResultRoutingLogic {
    
    
    init(dataCollector autopaymentdata: AutoPaymentData) {
        let controller = APResultViewController()
        super.init(viewController: controller)
        
        let interactor = APResultInteractor(dataCollector: autopaymentdata)
        let presenter = APResultPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    
    
}
