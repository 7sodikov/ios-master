//
//  APResultViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 17/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol APResultDisplayLogic: APResultViewInstaller {
    func update(title: String)
    func setupTargets()
}


class APResultViewController: BaseViewController, APResultDisplayLogic  {
    
    
    var backgroundImage: UIImageView!
    var tableView: UITableView!
    
    var mainView: UIView { self.view }
    var presenter: APResultPresentationLogic!
    
    
    override func loadView() {
        super.loadView()
        
        navigationController?.transparentBackgorund()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func setupTargets() {
        super.setupTargets()
        tableView.dataSource = self
        tableView.delegate = self
        
        
        tableView.register(TableViewCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(ResultStatusCell.self)
        tableView.register(SingleLabelCell.self)
    }
    
    func update(title: String) {
        self.title = title
    }
    
    
}

extension APResultViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        configure(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        section == 0 ? 64 : 16
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath) is DetailsCellViewModel {
            return presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath).estimatedRowHeight
        }
        return UITableView.automaticDimension
    }
    
    func configure(cell: UITableViewCell) {
        (cell as? ButtonCell)?.buttonDelegate = self
    }
}

extension APResultViewController: ButtonCellDelegate {
    func buttonClicked(tag: Int) {
        if tag == 1 {
            let controllers = navigationController?.viewControllers ?? []
            navigationController?.popToViewController(controllers[2], animated: true)
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }
}
