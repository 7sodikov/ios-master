//
//  APSupplierFactoryViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 30/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol APSupplierFactoryDisplayLogic: APSupplierFactoryViewInstaller {
    func setupTargets()
    func update(navigationTitle: String)
}


class APSupplierFactoryViewController: BaseViewController, APSupplierFactoryDisplayLogic  {
    
    var backgroundImage: UIImageView!
    var operationStackOrderView: OperationStackOrderView!
    var tableView: UITableView!
    var tableFooterView: UIView!
    var nextButton: Button!
    var mainView: UIView { self.view }
    var presenter: APSupplierFactoryPresenter!
    
    override func loadView() {
        super.loadView()
        title = RS.lbl_automatic_payment.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    func update(navigationTitle: String) {
        title = navigationTitle
    }
    
    override func setupTargets() {
        super.setupTargets()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(TableViewCell.self)
        tableView.register(InputCell.self)
        tableView.register(MaskedInputCell.self)
    
        nextButton.addTarget(self, action: #selector(nextButtonClicked), for: .touchUpInside)
    }
    
    @objc func nextButtonClicked() {
        presenter.willNext()
    }
    
}

extension APSupplierFactoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        (cell as? InputCell)?.inputCellDelegate = self
        (cell as? MaskedInputCell)?.maskedInputCellDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        presenter.tablePresenter(tableView, didDeselectRowAt: indexPath)
    }
}


extension APSupplierFactoryViewController: InputCellDelegate {
    
    func input(text: String, tag: Int, uid: String?) {
        presenter.willSet(value: text, uid: uid)
    }
    
    func presentPicker(at ukey: Int?) {
        guard let rawValue = ukey, let frequency = PickerType(rawValue: rawValue) else {return}
        presenter.willPresentBottomSheet(pickerType: frequency)
    }
}

extension APSupplierFactoryViewController: MaskedInputCellDelegate {
    
    func maskedInput(text: String, maskType: MaskType, tag: Int, uid: String?) {
        presenter.willSet(text: text, maskType: maskType, uid: uid)
    }
}
