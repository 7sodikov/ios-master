//
//  APSupplierFactoryViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 30/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APSupplierFactoryBase {
    typealias PickerType = APSupplierFactoryViewController.PickerType
}


extension APSupplierFactoryViewController {
    
    
    enum PickerType: Int {
        case frequencyType
        case frequencyDay
    }
}
