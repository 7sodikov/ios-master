//
//  APSupplierFactoryViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 30/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol APSupplierFactoryViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var operationStackOrderView: OperationStackOrderView! {get set}
    var tableView: UITableView! {get set}
    var tableFooterView: UIView! {get set}
    var nextButton: Button! {get set}
}

extension APSupplierFactoryViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64))
        tableFooterView.backgroundColor = .clear
        tableFooterView.isUserInteractionEnabled = true
        
        nextButton = .init(method: .network, style: .next)
        nextButton.setup()
        
        tableView = .init()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
//        tableView.tableFooterView = tableFooterView
        
        operationStackOrderView = .init()
        operationStackOrderView.setup(current: .three)
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(operationStackOrderView)
        mainView.addSubview(tableView)
        
        mainView.addSubview(nextButton)
//        tableFooterView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        operationStackOrderView.constraint { make in
            make.top(16).centerX.equalToSuperView()
        }
        
        nextButton.constraint { make in
            make.bottom(-16).centerX.equalToSuperView()
        }
        
        tableView.constraint { (make) in
            make.top(self.operationStackOrderView.bottomAnchor, offset: 16)
                .bottom(self.nextButton.topAnchor, offset: -16)
                .horizontal.equalToSuperView()
        }
    }
    
}
