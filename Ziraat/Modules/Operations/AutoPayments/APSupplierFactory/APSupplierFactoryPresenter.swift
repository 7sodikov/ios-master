//
//  APSupplierFactoryPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 30/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol APSupplierFactoryPresentationLogic: PresentationProtocol, APSupplierFactoryBase, TablePresentationLogic {
    func willPresentBottomSheet(pickerType: PickerType)
    func willSet(text: String, maskType: MaskType, uid: String?)
    func willSet(value: String, uid: String?)
    
    func willNext()
}


class APSupplierFactoryPresenter: APSupplierFactoryPresentationLogic {
    
    private unowned let view: APSupplierFactoryDisplayLogic
    private var interactor: APSupplierFactoryBusinessLogic
    private let router: APSupplierFactoryRoutingLogic
    
    private var controller: APSupplierFactoryViewController { view as! APSupplierFactoryViewController }
    
    
    init(view: APSupplierFactoryDisplayLogic, interactor: APSupplierFactoryBusinessLogic, router: APSupplierFactoryRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        if let title = interactor.service?.title {
            view.update(navigationTitle: title)
        }
        view.setupSubviews()
        view.setupTargets()
        bind()
    }
    
    private func bind() {
        interactor.showError = { errorText in
            self.controller.showError(message: errorText)
        }
    }
    
    func willPresentBottomSheet(pickerType: PickerType) {
        switch pickerType {
        case .frequencyType:
            break
            
        case .frequencyDay:
            let bottomSheet = interactor.frequencyDaysBottomSheet
            bottomSheet.delegate = self
            router.presentBottomSheet(bottomSheet: bottomSheet)
        }
    }
    
    func willSet(text: String, maskType: MaskType, uid: String?) {
        interactor.set(value: text, maskType: maskType, uid: uid)
    }
    
    func willSet(value: String, uid: String?) {
        interactor.set(value: value, uid: uid)
    }
    
    func willNext() {
        let autoPaymentData = interactor.updatedDataCollector()
        router.openAPTransfer(dataCollector: autoPaymentData)
    }
}


// MARK: -TablePresentationLogic

extension APSupplierFactoryPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
    func tablePresenter(_ table: UITableView, didDeselectRowAt indexPath: IndexPath) {
       
        
    }
    
}

extension APSupplierFactoryPresenter: BottomSheetDelegate {
    func bottomSheet(didSelectCardAt indexPath: IndexPath, card: Any, with tag: Int) {}
    
    
    func didSelect(item viewModel: TableViewModel, at indexPath: IndexPath, with tag: Int) {
        interactor.setFrequencyDay(at: indexPath)
        view.tableView.reloadData()
    }
    
}
