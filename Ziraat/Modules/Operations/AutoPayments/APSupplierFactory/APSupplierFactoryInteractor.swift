//
//  APSupplierFactoryInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 30/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APSupplierFactoryBusinessLogic: APSupplierFactoryBase {
    var service: AutoPayService? {get}
    var frequencyDaysBottomSheet: BottomSheet {get}
    var viewModels: [[TableViewModel]] {get}
    var showError: ((String) -> Void)? {get set}
    
    func setFrequencyDay(at indexPath: IndexPath)
    func set(value: String, maskType: MaskType, uid: String?)
    func set(value: String, uid: String?)
    func updatedDataCollector() -> AutoPaymentData
}

class APSupplierFactoryInteractor: APSupplierFactoryBusinessLogic {
 
    var dataCollector: AutoPaymentData
    let service: AutoPayService?
    var showError: ((String) -> Void)?
    
    var dic: Dictionary<String, String> = .init()
    
    var amountKey = "AmountKey"
    var frequency: String = RS.lbl_Monthly.localized()
    var selectedDay: Int = 0
    var frequencyDays: [String]
    var amount: String = ""
    
    
    init(dataCollector: AutoPaymentData) {
        self.dataCollector = dataCollector
        self.service = dataCollector.service
        let daysInMonth = Date().daysInMonth
        self.frequencyDays = (1...daysInMonth).map(\.description)
        self.frequencyDays.append("0")
    }
    
    func setFrequencyDay(at indexPath: IndexPath) {
        selectedDay = Int(frequencyDays[indexPath.row]) ?? 0
    }
    
    func set(value: String, maskType: MaskType, uid: String?) {
        guard let uuid = uid else { return }
        dic[uuid] = value
    }
    
    func set(value: String, uid: String?) {
        guard let uuid = uid else { return }
        if uid == amountKey {
            self.amount = value
        } else {
            dic[uuid] = value
        }
    }
    
    func updatedDataCollector() -> AutoPaymentData {
        dataCollector.fields = dic.reduce(into: .init()) { (result, item) in
            let (key, value) = item
            let title = service?.fields.first(where: { $0.uid == key })?.title ?? ""
            result[key] = (title, value)
        }
        if let amount = Int(self.amount) {
            dataCollector.amount = amount * 100
        }
        dataCollector.frequency = .monthly
        dataCollector.dayOf = selectedDay
        return dataCollector
    }
}

extension APSupplierFactoryInteractor {
    
    var frequencyDaysBottomSheet: BottomSheet {
        let rows = frequencyDays.map { day -> SingleLabelCellViewModel in
            return .init(text: day == "0" ? RS.lbl_EndOfMonth.localized() : day, textAlignment: .center)
        }
        let dataSource = BottomSheetSection(header: RS.lbl_SelectFrequencyOfPayment.localized(), rows: rows)
        let height = UIScreen.main.bounds.height / 3
        return .init(viewHeight: height, dataSource: [dataSource], with: 1)
    }
    
    var viewModels: [[TableViewModel]]  {
        [
            serverFieldViewModels,
            frequencyTypeViewModel,
            frequencyViewModel,
            amountViewModel
        ]
    }
    
    private var serverFieldViewModels: [TableViewModel] {
        service?.fields.map { field -> TableViewModel in
            var inputType: InputCellViewModel.Input = .text("", placeholder: "")
            switch field.type {

            case .MONEY:
                let amount = (dic[field.uid] ?? "").makeReadableAmount
                inputType = .amount(amount, placeholder: field.title)
                
            case .DATEPOPUP,
                 .REGEXBOX,
                 .STRING:
                inputType = .text(dic[field.uid] ?? "", placeholder: field.title)
                
            case .PHONE:
                let vm = MaskedInputCellViewModel(uid: field.uid,
                                                  title: field.title,
                                                  value: dic[field.uid] ?? "",
                                                  maskType: .phone)
                return vm
            }
            return InputCellViewModel(uid: field.uid, headerTitle: field.title, inputType: inputType)
        } ?? .init()
    }
    
    private var frequencyTypeViewModel: [InputCellViewModel] {
        [
            .init(headerTitle: RS.lbl_Frequency.localized(), inputType: .picker(title: RS.lbl_Frequency.localized(), value: RS.lbl_Monthly.localized(), ukey: PickerType.frequencyType.rawValue))
        ]
    }
    
    private var frequencyViewModel: [InputCellViewModel] {
        let value = selectedDay == .zero ? RS.lbl_EndOfMonth.localized() : selectedDay.description
        return [
            .init(headerTitle: RS.lbl_DayOfMonth.localized(), inputType: .picker(title: RS.lbl_DayOfMonth.localized(), value: value, ukey: PickerType.frequencyDay.rawValue), notify: .none)
        ]
    }
    
    private var amountViewModel: [InputCellViewModel] {
        [
            .init(uid: amountKey, headerTitle: RS.lbl_amount.localized(), inputType: .amount(amount, placeholder: ""))
        ]
    }
    
    
}

extension Date {
    var daysInMonth: Int {
        let calendar = Calendar.current
        if let interval = calendar.dateInterval(of: .month, for: self) {
            return calendar.dateComponents([.day], from: interval.start, to: interval.end).day ?? -1
        }
        return -1
    }
    
    
}
