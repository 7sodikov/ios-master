//
//  APSupplierFactoryRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 30/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APSupplierFactoryRoutingLogic {
    func presentBottomSheet(bottomSheet: BottomSheet)
    func openAPTransfer(dataCollector autoPaymentData: AutoPaymentData)
}


class APSupplierFactoryRouter: BaseRouter, APSupplierFactoryRoutingLogic {
    
    
    init(dataCollector: AutoPaymentData) {
        let controller = APSupplierFactoryViewController()
        super.init(viewController: controller)
        
        let interactor = APSupplierFactoryInteractor(dataCollector: dataCollector)
        let presenter = APSupplierFactoryPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    
    func presentBottomSheet(bottomSheet: BottomSheet) {
        let bottomSheetVC = BottomSheetViewController(bottomSheet: bottomSheet)
        bottomSheetVC.modalPresentationStyle = .custom
        viewController.presentPanModal(bottomSheetVC)
    }
    
    func openAPTransfer(dataCollector autoPaymentData: AutoPaymentData) {
        push(APTransferFactoryRouter(dataCollector: autoPaymentData), animated: true)
    }
}
