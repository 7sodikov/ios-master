//
//  SuppliersViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol SuppliersDisplayLogic: SuppliersViewInstaller {
    func setupTargets()
    func update(navigationTitle: String)
}


class SuppliersViewController: BaseViewController, SuppliersDisplayLogic  {
    
    var backgroundImage: UIImageView!
    var operationStackOrderView: OperationStackOrderView!
    var searchBarTitle: UILabel!
    var searchBar: UISearchBar!
    var tableView: UITableView!
    var parameter: Any? {
        presenter.selectionMode
    }
    var mainView: UIView { self.view }
    var presenter: SuppliersPresentationLogic!
    
    override func loadView() {
        super.loadView()
        title = RS.lbl_automatic_payment.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutSubView()
    }
    
    override func setupTargets() {
        super.setupTargets()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(TableViewCell.self)
        tableView.register(DisclosureBorderCell.self)
        
        searchBar.delegate = self
    }
    
    func update(navigationTitle: String){
        title = navigationTitle
    }
    
}

extension SuppliersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        presenter.tablePresenter(tableView, didSelectRowAt: indexPath)
    }
}

extension SuppliersViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.wilSearch(with: searchText)
    }
}
