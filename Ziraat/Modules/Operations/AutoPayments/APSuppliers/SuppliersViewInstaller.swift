//
//  SuppliersViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol SuppliersViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var operationStackOrderView: OperationStackOrderView! {get set}
    var searchBarTitle: UILabel! {get set}
    var searchBar: UISearchBar! {get set}
    var tableView: UITableView! {get set}
    
    func layoutSubView()
}

extension SuppliersViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        tableView = .init()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.tableFooterView = .init()
        tableView.allowsSelection = true

        operationStackOrderView = .init()
        if let mode = parameter as? SuppliersMode, case .services = mode {
            operationStackOrderView.setup(current: .two)
        } else {
            operationStackOrderView.setup(current: .one)
        }
        
        
        searchBarTitle = UILabel()
        searchBarTitle.font = .gothamNarrow(size: 16, .book)
        searchBarTitle.textColor = .black
        searchBarTitle.text = RS.lbl_keywords.localized()
        
        searchBar = UISearchBar()
        searchBar.layer.masksToBounds = true
        searchBar.layoutIfNeeded()
        searchBar.addShadow()
//        searchBar.layer.borderColor = UIColor(145, 150, 155).cgColor
        searchBar.layer.borderWidth = 2
        searchBar.layer.cornerRadius = 6
        searchBar.isTranslucent = false
        searchBar.backgroundColor = .white
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(operationStackOrderView)
        mainView.addSubview(searchBarTitle)
        mainView.addSubview(searchBar)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        operationStackOrderView.constraint { make in
            make.top.centerX.equalToSuperView()
        }
        searchBarTitle.constraint { make in
            make.top(self.operationStackOrderView.bottomAnchor, offset: 16)
                .horizontal(20).equalToSuperView()
        }
        searchBar.constraint { make in
            make.height(50)
                .top(self.searchBarTitle.bottomAnchor, offset: 16)
                .horizontal(16).equalToSuperView()
        }
        tableView.constraint { (make) in
            make.top(self.searchBar.bottomAnchor, offset: 16)
                .horizontal.bottom.equalToSuperView()
        }
    
    }
    
    func layoutSubView() {
        if let searchTextField: UITextField = searchBar.value(forKey: "searchBarTextField") as? UITextField {
            searchTextField.textAlignment = .left
            let image:UIImage = UIImage(named: "btn_search")!
            let imageView:UIImageView = UIImageView.init(image: image)
            searchTextField.leftView = nil
            searchTextField.placeholder = RS.txt_f_search.localized()
            searchTextField.rightView = imageView
            searchTextField.rightViewMode = .always
            searchTextField.backgroundColor = .clear
        }
    }
    
}
