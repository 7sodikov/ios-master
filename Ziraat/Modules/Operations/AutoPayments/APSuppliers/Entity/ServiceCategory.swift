//
//  ServiceCategory.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct ServiceCategory {
    var id: Int
    var title, logo: String
    var autoPay: Bool 
    
    var viewModel: DisclosureBorderViewModel {
        .init(uid: id.description, iconName: logo, title: title)
    }
}

