//
//  AutoPayService.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/31/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct AutoPayService {
    let serviceID, minAmount, maxAmount: Int
    let title: String
    let logoURL: String
    let serviceType: Int
    let autoPay: Bool
    let fields: [Field]
    
    var viewModel: DisclosureBorderViewModel {
        .init(uid: serviceID.description, iconName: logoURL, title: title)
    }
    
    func findField(uid: String) -> Field? {
        fields.first(where: { $0.uid == uid })
    }
}


// MARK: - AutoPayService Field
extension AutoPayService {
    struct Field {
        let name, title: String
        let fieldRequired, readOnly: Bool
        let type: FieldType
        let mask: String?
        
        let uid: String = UUID().uuidString
    }

}

enum FieldType: String {
    case MONEY
    case DATEPOPUP
    case REGEXBOX
    case PHONE
    case STRING
}
