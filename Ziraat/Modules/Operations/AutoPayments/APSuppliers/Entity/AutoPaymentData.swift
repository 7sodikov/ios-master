//
//  AutoPaymentData.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct AutoPaymentData {
    var category: (id: Int, title: String)
    var service: AutoPayService?
    var frequency: Frequency = .monthly
    var dayOf: Int = -1
    var amount: Int = 0
    var fields: Dictionary<String, (title: String, value: String)> = ["":("","")]
    var selectedCard: Card?
}

struct AutoPaymentParams {
    
    let title: String
    let payerId: Int
    let payerNumber: String
    let payerBranch: String
    let serviceId: String
    let frequency: String
    let dayOf: Int
    let amount: Int
    let fields: [String: String]
}
