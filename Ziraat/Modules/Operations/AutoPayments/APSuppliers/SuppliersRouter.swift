//
//  SuppliersRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SuppliersRoutingLogic {
    func nextStepSuppliers(with dataCollector: AutoPaymentData)
    func navigateSupplierFactory(dataCollector: AutoPaymentData)
}


class SuppliersRouter: BaseRouter, SuppliersRoutingLogic {
    
    
    init(selectionMode: SuppliersMode, dataCollector: AutoPaymentData? = nil) {
        let controller = SuppliersViewController()
        super.init(viewController: controller)
        
        let interactor = SuppliersInteractor(selectionMode: selectionMode, dataCollector: dataCollector)
        let presenter = SuppliersPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func nextStepSuppliers(with dataCollector: AutoPaymentData) {
        let router = SuppliersRouter(selectionMode: .services, dataCollector: dataCollector)
        push(router, animated: true)
    }
    
    func navigateSupplierFactory(dataCollector: AutoPaymentData) {
        let router = APSupplierFactoryRouter(dataCollector: dataCollector)
        push(router, animated: true)
    }
    
}
