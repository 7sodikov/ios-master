//
//  SuppliersPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol SuppliersPresentationLogic: PresentationProtocol, SuppliersBase, TablePresentationLogic {
    var selectionMode: SuppliersMode {get}
    func wilSearch(with text: String)
}


class SuppliersPresenter: SuppliersPresentationLogic {
    
    private unowned let view: SuppliersDisplayLogic
    private var interactor: SuppliersBusinessLogic
    private let router: SuppliersRoutingLogic
    
    private var controller: SuppliersViewController { view as! SuppliersViewController }
    var selectionMode: SuppliersMode {
        interactor.selectionMode
    }
    
    
    init(view: SuppliersDisplayLogic, interactor: SuppliersBusinessLogic, router: SuppliersRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        if case .services = interactor.selectionMode, let collector = interactor.dataCollector {
            view.update(navigationTitle: collector.category.title)
        }
        view.setupSubviews()
        view.setupTargets()
        bind()
        fetchData()
    }
    
    private func bind() {
        interactor.showError = { errorText in
            self.controller.showError(message: errorText)
        }
    }
    
    func wilSearch(with text: String) {
        interactor.search(with: text)
        view.tableView.reloadData()
    }
    
    func fetchData() {
        switch interactor.selectionMode {
        case .categorySevice:
            fetchCategories()
        case .services:
            guard let categoryId = interactor.dataCollector?.category.id else { return }
            fetchServices(with: categoryId)
        }
    }
    
    func fetchCategories() {
        controller.spinner(.start)
        interactor.fetchCategories { [weak self] in
            self?.controller.spinner(.stop)
            self?.view.tableView.reloadData()
        }
    }
    
    func fetchServices(with categoryId: Int) {
        controller.spinner(.start)
        interactor.fetchServices(with: categoryId) { [weak self] in
            self?.controller.spinner(.stop)
            self?.view.tableView.reloadData()
        }
    }
}


// MARK: -TablePresentationLogic

extension SuppliersPresenter {
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.row]
    }
    
    func tablePresenter(_ table: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch interactor.selectionMode {
        case .categorySevice:
            let category = interactor.filteredCategories[indexPath.row]
            let collector = AutoPaymentData(category: (category.id, category.title))
            router.nextStepSuppliers(with: collector)
        case .services:
            let service = interactor.filteredServices[indexPath.row]
            if var collector = interactor.dataCollector {
                collector.service = service
                router.navigateSupplierFactory(dataCollector: collector)
            }
            
        }
        
    }
}
