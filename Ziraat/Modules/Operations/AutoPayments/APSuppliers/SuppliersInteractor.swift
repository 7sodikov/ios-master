//
//  SuppliersInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/05/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SuppliersBusinessLogic: SuppliersBase {
    var selectionMode: SuppliersMode {get}
    var dataCollector: AutoPaymentData? {get}
    var showError: ((String)->Void)? {get set}
    var viewModels: [TableViewModel] {get}
    var filteredCategories: [ServiceCategory] {get}
    var filteredServices: [AutoPayService] {get}
    
    func search(with text: String)
    func fetchCategories(completion: @escaping() -> Void)
    func fetchServices(with categoryId: Int, completion: @escaping()->())
}

class SuppliersInteractor: SuppliersBusinessLogic {
    
    let selectionMode: SuppliersMode
    var dataCollector: AutoPaymentData?
    
    var showError: ((String) -> Void)?
    
    var categories: [ServiceCategory] = []
    var filteredCategories: [ServiceCategory] = []
    
    var services: [AutoPayService] = []
    var filteredServices: [AutoPayService] = []
    
    var viewModels: [TableViewModel] {
        switch selectionMode {
        case .categorySevice:
            return filteredCategories.map(\.viewModel)
        case .services:
            return filteredServices.map(\.viewModel)
        }
    }
    
    init(selectionMode: SuppliersMode, dataCollector: AutoPaymentData?) {
        self.selectionMode = selectionMode
        self.dataCollector = dataCollector
    }
    
    func search(with text: String) {
        switch selectionMode {
            
        case .categorySevice:
            filteredCategories = text.isEmpty ? categories : categories.filter {$0.title.contains(text)}
        case .services:
            filteredServices = services.filter {text.isEmpty || $0.title.contains(text)}
        }
        
    }
    
    
    func fetchCategories(completion: @escaping() -> Void) {
        
        NetworkService.AutoPayment().fetchServiceCategories { response in
            switch response {
            case .success(let result):
                let categories = result.data.compactMap(\.coreObject)
                self.categories = categories
                self.filteredCategories = categories
                
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
        
    }
    
    func fetchServices(with categoryId: Int, completion: @escaping()->()) {
        NetworkService.AutoPayment().fetchServices(categoryId: categoryId.description) { response in
            switch response {
            
            case .success(let result):
                let services = result.data.map(\.coreObject)
                self.services = services
                self.filteredServices = services
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
}
