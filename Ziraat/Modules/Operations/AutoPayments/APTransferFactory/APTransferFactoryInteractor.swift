//
//  APTransferFactoryInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APTransferFactoryBusinessLogic: APTransferFactoryBase {
    var dataCollector: AutoPaymentData {get}
    var viewModels: [CardCellViewModel] {get}
    var showError: ((String)->Void)? {get set}
    
    func updatedDataCollector() -> AutoPaymentData
    func setSelectedCard(at indexPath: IndexPath)
    func fetchData(completion: @escaping ()->Void)
}

class APTransferFactoryInteractor: APTransferFactoryBusinessLogic {
    
    var dataCollector: AutoPaymentData
    var cards: [Card] = .init()
    var cardBalances: [CardBalance] = .init()
    var selectedCardId: String = ""
    
    var showError: ((String)->Void)?
    
    var viewModels: [CardCellViewModel] {
        cards.map { .init(card: $0, isSelected: selectedCardId == $0.cardId) }
    }
    
    
    init(dataCollector: AutoPaymentData) {
        self.dataCollector = dataCollector
    }
    
    
    func setSelectedCard(at indexPath: IndexPath) {
        selectedCardId = cards[indexPath.row].cardId
    }
    
    func updatedDataCollector() -> AutoPaymentData {
        dataCollector.selectedCard = cards.first(where: { $0.cardId == selectedCardId })
        return dataCollector
    }
    
    func fetchData(completion: @escaping ()->Void) {
        func configure() {
            cards = cards.compactMap { (item) -> Card in
                var card = item
                if let cardBalance = cardBalances.first(where:{$0.cardId == card.cardId}) {
                    card.update(balance: cardBalance.balance, currency: cardBalance.currency)
                }
                return card
            }.filter(\.isUzb)
        }
        
        let group = DispatchGroup()
        
        [fetchCardList, fetchCardBalance].forEach { (itemAction) in
            group.enter()
            itemAction(group.leave)
        }
        
        group.notify(queue: .main) {
            configure()
            completion()
        }
    }
    
    private func fetchCardList(completion: @escaping()->Void) {
        NetworkService.Card.cardsList { (response) in
            switch response {
            
            case .success(let result):
                self.cards = result.data.cards.compactMap(\.card)
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    private func fetchCardBalance(completion: @escaping()->Void) {
        NetworkService.Card.cardBalance { (response) in
            switch response {
            case .success(let result):
                self.cardBalances = result.data.cards?.map(\.cardBalance) ?? []
                
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
}
