//
//  APTransferFactoryPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol APTransferFactoryPresentationLogic: PresentationProtocol, APTransferFactoryBase {
    
    func willNext()
    func willSetSelectedCard(at indexPath: IndexPath)
    func collectionView(numberOfItemsInSection section: Int) -> Int
    func collectionView(viewModelAtIndexPath indexPath: IndexPath) -> CollectionViewModel
}


class APTransferFactoryPresenter: APTransferFactoryPresentationLogic {
    
    private unowned let view: APTransferFactoryDisplayLogic
    private var interactor: APTransferFactoryBusinessLogic
    private let router: APTransferFactoryRoutingLogic
    
    private var controller: APTransferFactoryViewController { view as! APTransferFactoryViewController }
    
    
    init(view: APTransferFactoryDisplayLogic, interactor: APTransferFactoryBusinessLogic, router: APTransferFactoryRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        view.setupTargets()
        if let title = interactor.dataCollector.service?.title {
            view.update(title: title)
        }
        
        interactor.showError = { message in
            self.controller.showError(message: message)
        }
        fetchData()
    }
    
    func fetchData() {
        controller.spinner(.start)
        interactor.fetchData { [weak self] in
            guard let self = self else { return }
            self.controller.spinner(.stop)
            self.view.collectionView.reloadData()
        }
    }
    
    func willSetSelectedCard(at indexPath: IndexPath) {
        interactor.setSelectedCard(at: indexPath)
    }
    
    func willNext() {
        let autoPaymentData = interactor.updatedDataCollector()
        router.openResultScreen(dataCollector: autoPaymentData)
    }
}


// MARK: - CollectionViewPresentationLogic

extension APTransferFactoryPresenter {
    
    func collectionView(numberOfItemsInSection section: Int) -> Int {
        interactor.viewModels.count
    }
    
    func collectionView(viewModelAtIndexPath indexPath: IndexPath) -> CollectionViewModel {
        interactor.viewModels[indexPath.item]
    }
    
}
