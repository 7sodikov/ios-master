//
//  APTransferFactoryViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol APTransferFactoryViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var operationStackOrderView: OperationStackOrderView! {get set}
    var collectionView: UICollectionView! {get set}
    var layout: UICollectionViewFlowLayout! {get set}
    var nextButton: Button! {get set}
}

extension APTransferFactoryViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        operationStackOrderView = .init()
        operationStackOrderView.setup(current: .four)
        let margin = Adaptive.val(15)
        layout = UICollectionViewFlowLayout()
        layout.itemSize = .init(width: Adaptive.val(164), height: Adaptive.val(98))
        layout.minimumLineSpacing = margin
        layout.minimumInteritemSpacing = margin
        layout.sectionInset = .init(top: 0, left: margin, bottom: 0, right: margin)
        layout.scrollDirection = .vertical
        
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        
        nextButton = .init(method: .network, style: .nextGray)
        nextButton.setup()
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(operationStackOrderView)
        mainView.addSubview(collectionView)
        
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
//        let leading = (UIScreen.main.bounds.width - (layout.itemSize.width + 32))
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        operationStackOrderView.constraint { make in
            make.top(16).centerX.equalToSuperView()
        }
        
        nextButton.constraint { make in
            make.bottom(-16).centerX.equalToSuperView()
        }
        
        collectionView.constraint { (make) in
            make.top(self.operationStackOrderView.bottomAnchor, offset: 32)
                .bottom(self.nextButton.topAnchor, offset: -16)
                .leading.trailing.equalToSuperView()
        }
    }
    
}
