//
//  APTransferFactoryRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APTransferFactoryRoutingLogic {
 
    func openResultScreen(dataCollector autoPaymentData: AutoPaymentData)
}


class APTransferFactoryRouter: BaseRouter, APTransferFactoryRoutingLogic {
    
    
    init(dataCollector: AutoPaymentData) {
        let controller = APTransferFactoryViewController()
        super.init(viewController: controller)
        
        let interactor = APTransferFactoryInteractor(dataCollector: dataCollector)
        let presenter = APTransferFactoryPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func openResultScreen(dataCollector autoPaymentData: AutoPaymentData) {
        
        let router = APConfirmRouter(dataCollector: autoPaymentData)
        push(router, animated: true)
    }
    
}
