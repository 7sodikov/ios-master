//
//  APTransferFactoryViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 10/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol APTransferFactoryDisplayLogic: APTransferFactoryViewInstaller {
    func setupTargets()
    func update(title: String)
}


class APTransferFactoryViewController: BaseViewController, APTransferFactoryDisplayLogic  {
    
    
    var backgroundImage: UIImageView!
    var operationStackOrderView: OperationStackOrderView!
    var collectionView: UICollectionView!
    var layout: UICollectionViewFlowLayout!
    var nextButton: Button!
    var mainView: UIView { self.view }
    var presenter: APTransferFactoryPresentationLogic!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        
    }
    
    override func setupTargets() {
        super.setupTargets()
        
        collectionView.register(CollectionViewCell.self)
        collectionView.register(CardCell.self)
        collectionView.register(AccountCardCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        nextButton.addTarget(self, action: #selector(willNext), for: .touchUpInside)
    }
    
    func update(title: String) {
        self.title = title
    }
    
    @objc func willNext() {
        presenter.willNext()
    }
    
}

extension APTransferFactoryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.collectionView(numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let vm = presenter.collectionView(viewModelAtIndexPath: indexPath)
        let cell = collectionView.reusable(vm.cellType, for: indexPath)
        cell.configure(viewModel: vm)
        return cell
    }
    
    
}

extension APTransferFactoryViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.willSetSelectedCard(at: indexPath)
        collectionView.reloadData()
        nextButton.updateStyle(.next)
    }
    
}

extension APTransferFactoryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        presenter.collectionView(viewModelAtIndexPath: indexPath).itemSize
    }
    
}
