//
//  AutoPaymentsInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AutoPaymentsBusinessLogic: AutoPaymentsBase {
    var viewModels: [AutoPaymentCellViewModel] {get}
    var showError: ((String) -> Void)? {get set}
    
    func autoPayment(at indexPath: IndexPath) -> APDetailsMode
    func changedSegment(button: SegmentButton)
    func fetchAutoPayments(completion: @escaping() -> Void)
    func fetchAutoPaymens(completion: @escaping() -> Void)
    func fetchAutoPaymensHistory(completion: @escaping() -> Void)
}

class AutoPaymentsInteractor: AutoPaymentsBusinessLogic {
    
    
    var showError: ((String) -> Void)?
    
    var currentSegment: SegmentButton = .statusPayment
    var autoPayments: [CAutoPayment] = []
    var autoPaymentHistories: [CAutoPaymentHistory] = []
    
    var viewModels: [AutoPaymentCellViewModel] {
       
        switch currentSegment {
            
        case .statusPayment:
            return autoPaymentHistories.compactMap {
                guard let autoPay = $0.autoPay else {return nil}
                return .init(
                    name: autoPay.title ?? "",
                    status: $0.status ?? .failed,
                    cardNumber: autoPay.numberCardText,
                    frequency: autoPay.frequencyText,
                    amount: autoPay.amountText
                )
            }
        case .allPayments:
            return autoPayments.map {
                .init(
                    name: $0.title ?? "",
                    status: $0.status ?? .failed,
                    cardNumber: $0.numberCardText,
                    frequency: $0.frequencyText,
                    amount: $0.amountText
                )
            }
        }
    }
    
    func autoPayment(at indexPath: IndexPath) -> APDetailsMode {
        switch currentSegment {
        case .statusPayment:
            return .made(autoPaymentHistory: autoPaymentHistories[indexPath.row])
        case .allPayments:
            return .willMake(autoPayment: autoPayments[indexPath.row])
        }
    }
    
    func changedSegment(button: SegmentButton) {
        currentSegment = button
    }
    
    func fetchAutoPayments(completion: @escaping() -> Void) {
        let group = DispatchGroup()
        
        [fetchAutoPaymens, fetchAutoPaymensHistory].forEach {
            group.enter()
            $0(group.leave)
        }
        
        group.notify(queue: .main) {
            completion()
        }
    }
    
    func fetchAutoPaymens(completion: @escaping() -> Void) {
        NetworkService.AutoPayment().fetchAutopays(status: nil) { response in
            switch response {
            case .success(let result):
                self.autoPayments = result.data.filter(\.isValid)
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
    func fetchAutoPaymensHistory(completion: @escaping() -> Void) {
        NetworkService.AutoPayment().fetchAutopayHistories(autopayId: nil, status: nil) { response in
            switch response {
            case .success(let result):
                self.autoPaymentHistories = result.data.filter(\.isValid)
            case .failure(let error):
                self.showError?(error.localizedDescription)
            }
            completion()
        }
    }
    
}
