//
//  AutoPaymentsViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AutoPaymentsViewInstaller: ViewInstaller {
    var backgroundImageView: UIImageView! {get set}
    var newAutoPaymentButton: UIButton! {get set}
    var statusPaymentButton: UIButton! {get set}
    var allAutoPaymentsButton: UIButton! {get set}
    var searchTextFiled: TextField! {get set}
    var filterButton: UIButton! {get set}
    var tableView: UITableView! {get set}
    var whiteBorder: UIView! {get set}
    var resultLabel: UILabel! {get set}
    
}

extension AutoPaymentsViewInstaller {
     typealias SegmentButton = AutoPaymentsViewController.SegmentButton
    
    func initSubviews() {
        backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "img_dash_light_background")
        
        newAutoPaymentButton = UIButton()
        newAutoPaymentButton.layer.cornerRadius = 25
        newAutoPaymentButton.titleLabel?.font = .gothamNarrow(size: 15, .book)
        newAutoPaymentButton.backgroundColor = .buttonRed
        newAutoPaymentButton.setTitleColor(.white, for: .normal)
        newAutoPaymentButton.setTitle(RS.nav_ttl_new_auto_payment.localized(), for: .normal)
        
        statusPaymentButton = UIButton()
        statusPaymentButton.titleLabel?.font = .gothamNarrow(size: 15, .book)
        statusPaymentButton.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        statusPaymentButton.layer.cornerRadius = 6
        statusPaymentButton.tag = SegmentButton.statusPayment.rawValue
        statusPaymentButton.setTitle(RS.btn_payment_status.localized(), for: .normal)
        statusPaymentButton.backgroundColor = UIColor.buttonRed
        
        allAutoPaymentsButton = UIButton()
        allAutoPaymentsButton.titleLabel?.font = .gothamNarrow(size: 15, .book)
        allAutoPaymentsButton.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        allAutoPaymentsButton.tag = SegmentButton.allPayments.rawValue
        allAutoPaymentsButton.layer.cornerRadius = 6
        allAutoPaymentsButton.layer.borderWidth = 2
        allAutoPaymentsButton.titleLabel?.numberOfLines = 2
        allAutoPaymentsButton.layer.borderColor = UIColor(145, 150, 155).cgColor
        allAutoPaymentsButton.setTitle(RS.btn_my_instructions.localized(), for: .normal)
        allAutoPaymentsButton.setTitleColor(UIColor(29, 29, 29).withAlphaComponent(0.5), for: .normal)
        whiteBorder = UIView()
        whiteBorder.backgroundColor = .white
        
        searchTextFiled = TextField()
        searchTextFiled.addShadow()
        searchTextFiled.placeholder = RS.txt_f_search.localized()
        searchTextFiled.layer.cornerRadius = 6
        searchTextFiled.layer.borderWidth = 2
        searchTextFiled.layer.borderColor = UIColor(145, 150, 155).cgColor
        let imageView = UIImageView(image: UIImage(named: "img_jam_chevron-left"))
        searchTextFiled.addRightSide(view: imageView, by: .init(top: 0, left: 0, bottom: 0, right: 15))
        searchTextFiled.addLeftSide(view: UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 8)))
        searchTextFiled.backgroundColor = .white
        
        filterButton = UIButton()
        filterButton.layer.borderColor = UIColor(68, 80, 86).cgColor
        filterButton.layer.borderWidth = 2
        filterButton.layer.cornerRadius = 6
        filterButton.backgroundColor = .white
        filterButton.setImage(UIImage(named: "btn_filter"), for: .normal)
        
        tableView = UITableView()
        tableView.tableFooterView = .init()
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.separatorStyle = .none
        
        resultLabel = UILabel()
        resultLabel.font = .gothamNarrow(size: 15, .book)
        resultLabel.textColor = UIColor(84, 95, 101)
        resultLabel.numberOfLines = 0
        resultLabel.lineBreakMode = .byWordWrapping
        resultLabel.isHidden = true
        resultLabel.text = RS.lbl_status_will_be_displayed.localized()
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImageView)
        mainView.addSubview(newAutoPaymentButton)
        mainView.addSubview(statusPaymentButton)
        mainView.addSubview(allAutoPaymentsButton)
//        mainView.addSubview(searchTextFiled)
//        mainView.addSubview(filterButton)
        mainView.addSubview(tableView)
        mainView.addSubview(resultLabel)
        
//        allAutoPaymentsButton.addSubview(whiteBorder)
    }
    
    func addSubviewsConstraints() {
        let width = (UIScreen.main.bounds.width - 32) / 2
        backgroundImageView.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        newAutoPaymentButton.constraint { (make) in
            make.height(50).top(Adaptive.val(48)).horizontal(16).equalToSuperView()
        }
        statusPaymentButton.constraint { (make) in
            make.top(self.newAutoPaymentButton.bottomAnchor, offset: 16)
                .width(width).height(50)
                .leading(16).equalToSuperView()
        }
        allAutoPaymentsButton.constraint { (make) in
            make.top(self.statusPaymentButton)
                .width(width).height(50)
                .trailing(-16).equalToSuperView()
        }
//        whiteBorder.constraint { make in
//            make.width(2).leading.vertical.equalToSuperView()
//        }
//        filterButton.constraint { (make) in
//            make.top(self.statusPaymentButton.bottomAnchor, offset: 16)
//                .square(50)
//                .trailing(-16).equalToSuperView()
//        }
//        searchTextFiled.constraint { (make) in
//            make.height(50)
//                .top(self.filterButton)
//                .trailing(self.filterButton.leadingAnchor, offset: -10)
//                .leading(16).equalToSuperView()
//        }
        tableView.constraint { (make) in
            make.top(self.statusPaymentButton.bottomAnchor, offset: 16)
                .horizontal.bottom.equalToSuperView()
        }
        resultLabel.constraint { make in
            make.centerY.horizontal(32).equalToSuperView()
        }
    }
    
    
    
    func changeSegmentButton(at segmentButton: SegmentButton) {
        switch segmentButton {
        case .allPayments:
            allAutoPaymentsButton.layer.borderWidth = 0
            allAutoPaymentsButton.backgroundColor = UIColor.buttonRed
            allAutoPaymentsButton.setTitleColor(UIColor.white, for: .normal)
            
            statusPaymentButton.backgroundColor = UIColor.white
            statusPaymentButton.layer.borderWidth = 2
            statusPaymentButton.layer.borderColor = UIColor(145, 150, 155).cgColor
            statusPaymentButton.setTitleColor(UIColor(29, 29, 29).withAlphaComponent(0.5), for: .normal)
            
        case .statusPayment:
            statusPaymentButton.layer.borderWidth = 0
            statusPaymentButton.backgroundColor = UIColor.buttonRed
            statusPaymentButton.setTitleColor(UIColor.white, for: .normal)
            
            allAutoPaymentsButton.backgroundColor = UIColor.white
            allAutoPaymentsButton.layer.borderWidth = 2
            allAutoPaymentsButton.layer.borderColor = UIColor(145, 150, 155).cgColor
            allAutoPaymentsButton.setTitleColor(UIColor(29, 29, 29).withAlphaComponent(0.5), for: .normal)
            
        }
    }
    
    
}

fileprivate extension UIButton {
    
    func drawBorder(edges: [UIRectEdge], borderWidth: CGFloat, color: UIColor, margin: CGFloat, cornerRadius: CGFloat) {
        for item in edges {
            let borderLayer: CALayer = CALayer()
            borderLayer.borderColor = color.cgColor
            borderLayer.borderWidth = borderWidth
//            borderLayer.cornerRadius = cornerRadius
            switch item {
            case .top:
                borderLayer.frame = CGRect(x: margin, y: 0, width: frame.width - (margin*2), height: borderWidth)
            case .left:
                borderLayer.frame =  CGRect(x: 0, y: margin, width: borderWidth, height: frame.height - (margin*2))
            case .bottom:
                borderLayer.frame = CGRect(x: margin, y: frame.height - borderWidth, width: frame.width - (margin*2), height: borderWidth)
            case .right:
                borderLayer.frame = CGRect(x: frame.width - borderWidth, y: margin, width: borderWidth, height: frame.height - (margin*2))
            case .all:
                drawBorder(edges: [.top, .left, .bottom, .right], borderWidth: borderWidth, color: color, margin: margin, cornerRadius: cornerRadius)
            default:
                break
            }
            self.layer.addSublayer(borderLayer)
        }
    }
}
