//
//  AutoPaymentsViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit
import Rswift

protocol AutoPaymentsDisplayLogic: AutoPaymentsViewInstaller {
    
}


class AutoPaymentsViewController: BaseViewController, AutoPaymentsDisplayLogic  {
    
    var resultLabel: UILabel!
    var backgroundImageView: UIImageView!
    var newAutoPaymentButton: UIButton!
    var statusPaymentButton: UIButton!
    var allAutoPaymentsButton: UIButton!
    var whiteBorder: UIView!
    var searchTextFiled: TextField!
    var filterButton: UIButton!
    var tableView: UITableView!

    var mainView: UIView { self.view }
    var presenter: AutoPaymentsPresentationLogic!
    
    override func loadView() {
        super.loadView()
        title = RS.nav_ttl_new_auto_payment.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        setupTargets()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear(animated)
    }
   
    override func setupTargets() {
        super.setupTargets()
        
        tableView.delegate = self
        tableView.dataSource = self
        
//        tableView.register(DisclosureCell.self)
        tableView.register(TableViewCell.self)
        tableView.register(AutoPaymentCell.self)
        
        newAutoPaymentButton.addTarget(self, action: #selector(newAutoPaymentButtonClicked), for: .touchUpInside)
        [statusPaymentButton, allAutoPaymentsButton].forEach { button in
            button?.addTarget(self, action: #selector(paymentsButtonChanged), for: .touchUpInside)
        }
    }
    
    @objc func paymentsButtonChanged(_ sender: UIButton) {
        guard let button = SegmentButton(rawValue: sender.tag) else { return }
        changeSegmentButton(at: button)
        presenter.changedSegment(button: button)
    }
    
    
    @objc func newAutoPaymentButtonClicked() {
        presenter.willOpenAutoPayment()
    }
    
}


extension AutoPaymentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        presenter.tablePresenter(tableView, didSelectRowAt: indexPath)
    }
}

