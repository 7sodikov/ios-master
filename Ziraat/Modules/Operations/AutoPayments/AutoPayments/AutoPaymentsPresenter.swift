//
//  AutoPaymentsPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol AutoPaymentsPresentationLogic: PresentationProtocol, AutoPaymentsBase, TablePresentationLogic {
    func willOpenAutoPayment()
    func changedSegment(button: SegmentButton)
}


class AutoPaymentsPresenter: AutoPaymentsPresentationLogic {
    
    private unowned let view: AutoPaymentsDisplayLogic
    private var interactor: AutoPaymentsBusinessLogic
    private let router: AutoPaymentsRoutingLogic
    
    private var controller: AutoPaymentsViewController { view as! AutoPaymentsViewController }
    
    
    init(view: AutoPaymentsDisplayLogic, interactor: AutoPaymentsBusinessLogic, router: AutoPaymentsRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        interactor.showError = { errorText in
            self.controller.showError(message: errorText)
        }
    }
    
    func viewWillAppear(_ animated: Bool) {
        fetchData()
    }
    
    func fetchData() {
        controller.spinner(.start)
        interactor.fetchAutoPayments { [weak self] in
            guard let controller = self?.controller, let view = self?.view else {
                return
            }
            controller.spinner(.stop)
            view.resultLabel.isHidden = !(self?.interactor.viewModels.isEmpty ?? false)
            view.tableView.reloadData()
        }
    }
    
    func willOpenAutoPayment() {
        router.openNewAutoPayment()
    }
    
    func changedSegment(button: SegmentButton) {
        interactor.changedSegment(button: button)
        view.resultLabel.isHidden = !interactor.viewModels.isEmpty
        view.tableView.reloadData()
    }
    
}


// MARK: -TablePresentationLogic

extension AutoPaymentsPresenter {
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.row]
    }
    
    func tablePresenter(_ table: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewMode = interactor.autoPayment(at: indexPath)
        router.openDetails(detailsMode: viewMode)
    }
    
}
