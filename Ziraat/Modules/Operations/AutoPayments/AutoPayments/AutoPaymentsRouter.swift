//
//  AutoPaymentsRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AutoPaymentsRoutingLogic {
 
    func openNewAutoPayment()
    func openDetails(detailsMode: APDetailsMode)
}


class AutoPaymentsRouter: BaseRouter, AutoPaymentsRoutingLogic {
    
    
    init() {
        let controller = AutoPaymentsViewController()
        super.init(viewController: controller)
        
        let interactor = AutoPaymentsInteractor()
        let presenter = AutoPaymentsPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func openNewAutoPayment() {
        let router = SuppliersRouter(selectionMode: .categorySevice)
        push(router, animated: true)
    }
    
    func openDetails(detailsMode: APDetailsMode) {
        push(APDetailsRouter(detailsMode: detailsMode), animated: true)
    }
    
}
