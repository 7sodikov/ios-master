//
//  AutoPaymentsViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AutoPaymentsBase {
    typealias SegmentButton = AutoPaymentsViewController.SegmentButton
}


extension AutoPaymentsViewController {
    
    enum SegmentButton: Int {
        case statusPayment
        case allPayments
        
        
        
    }
   
}

enum AutoPaymentStatus: String, Codable {
    case prepared = "PREPARED"
    case paid = "PAYED"
    case failed = "FAILED"
    case active = "ACTIVE"
    case inactive = "INACTIVE"
    
    var color: UIColor {
        switch self {
        case .paid,
             .active:
            return UIColor(57, 202, 0)
        case .failed,
             .inactive:
            return UIColor(225, 5, 20)
        case .prepared:
            return UIColor(246, 146, 4)
        }
    }
    
    var title: String {
        switch self {
            
        case .prepared:
            return RS.lbl_prepared.localized()
        case .paid:
            return RS.lbl_paid.localized()
        case .failed:
            return RS.lbl_failed.localized()
        case .active:
            return RS.lbl_active.localized()
        case .inactive:
            return RS.lbl_inactive.localized()
        }
    }
    
    var changedStatus: String {
        switch self {
        case .active:
            return AutoPaymentStatus.inactive.rawValue
        case .inactive:
            return AutoPaymentStatus.active.rawValue
        default:
            return ""
        }
    }
}

enum Frequency: String, Codable {
    case monthly = "MONTHLY"
    
    var localized: String {
        switch self {
        case .monthly:
            return RS.lbl_Monthly.localized()
       
        }
    }
}
