//
//  APDetailsViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 21/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol APDetailsDisplayLogic: APDetailsViewInstaller {
    func setupTargets()
    func updateSettingsButton()
}


class APDetailsViewController: BaseViewController, APDetailsDisplayLogic  {
    
    var settingsButton: UIButton!
    var backgroundImage: UIImageView!
    var tableView: UITableView!
    
    var mainView: UIView { self.view }
    var presenter: APDetailsPresentationLogic!
    
    
    override func loadView() {
        super.loadView()
        
        navigationController?.transparentBackgorund()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func setupTargets() {
        let attributes = [NSAttributedString.Key.font: UIFont.gothamNarrow(size: 16, .book)]
        UINavigationBar.appearance().titleTextAttributes = attributes
        title = RS.lbl_automatic_payment.localized()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        
        tableView.register(TableViewCell.self)
        tableView.register(HorizontalDetailCell.self)
        
        let home = UIBarButtonItem(image: UIImage(named: "img_icon_home"), style: .plain, target: self, action: #selector(goHome))
        settingsButton = UIButton()
        settingsButton.tintColor = .black
        settingsButton.setImage(UIImage(named: "img_login_settings")?.withRenderingMode(.alwaysTemplate), for: .normal)
        settingsButton.addTarget(self, action: #selector(settingsButtonClicked), for: .touchUpInside)
        settingsButton.constraint { $0.square(28) }
        let settings = UIBarButtonItem(customView: settingsButton)
        navigationItem.setRightBarButtonItems([home, settings], animated: true)
    }
    
    func updateSettingsButton() {
        settingsButton.tag = settingsButton.tag ^ 1
        let image = UIImage(named: (settingsButton.tag == 0 ? "img_login_settings" : "img_dash_settings_selected"))?.withRenderingMode(.alwaysTemplate)
        settingsButton.setImage(image, for: .normal)
    }
    
    @objc func settingsButtonClicked() {
        updateSettingsButton()
        presenter.showBottomSheet()
    }
}


extension APDetailsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        64
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        header(text: presenter.autoPayment.title ?? "")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath) is DetailsCellViewModel {
            return presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath).estimatedRowHeight
        }
        return UITableView.automaticDimension
    }
    
}
