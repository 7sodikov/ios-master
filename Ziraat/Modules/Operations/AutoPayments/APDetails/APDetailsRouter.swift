//
//  APDetailsRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 21/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APDetailsRoutingLogic {
    func openDeleteConfirmation(autoPayment: CAutoPayment)
    func presentBottomSheet(bottomSheet: BottomSheet)
    func back()
}


class APDetailsRouter: BaseRouter, APDetailsRoutingLogic {
    
    
    init(detailsMode: APDetailsMode) {
        let controller = APDetailsViewController()
        super.init(viewController: controller)
        
        let interactor = APDetailsInteractor(detailsMode: detailsMode)
        let presenter = APDetailsPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func presentBottomSheet(bottomSheet: BottomSheet) {
        let bottomSheetVC = BottomSheetViewController(bottomSheet: bottomSheet)
        bottomSheetVC.modalPresentationStyle = .custom
        viewController.presentPanModal(bottomSheetVC)
    }
    
    func back() {
        navigationController?.popViewController(animated: true)
    }
    
    func openDeleteConfirmation(autoPayment: CAutoPayment) {
        push(APDeleteConfirmmRouter(autoPayment: autoPayment), animated: true)
    }
}
