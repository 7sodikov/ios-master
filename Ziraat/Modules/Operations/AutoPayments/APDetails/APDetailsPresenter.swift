//
//  APDetailsPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 21/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol APDetailsPresentationLogic: PresentationProtocol, TablePresentationLogic, APDetailsBase {
    var autoPayment: CAutoPayment {get}
    
    func showBottomSheet()
}


class APDetailsPresenter: APDetailsPresentationLogic {
    
    private unowned let view: APDetailsDisplayLogic
    private let interactor: APDetailsBusinessLogic
    private let router: APDetailsRoutingLogic
    
    private var controller: APDetailsViewController { view as! APDetailsViewController }
    
    var autoPayment: CAutoPayment { interactor.autoPayment }
    
    init(view: APDetailsDisplayLogic, interactor: APDetailsBusinessLogic, router: APDetailsRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        view.setupTargets()
    }
    
    func confirmChangeStatus() {
        let nfyController = NotifyViewController(.changeStatus) {
            self.changeStatus()
        }
        self.controller.present(nfyController, animated: true)
    }
    
    func changeStatus(){
        controller.spinner(.start)
        interactor.changeStatusAutoPayment {
            self.controller.spinner(.stop)
            self.router.back()
        }
    }
    
    func deleteAutoPayment() {
        router.openDeleteConfirmation(autoPayment: interactor.autoPayment)
    }
}


// MARK: -TablePresentationLogic

extension APDetailsPresenter {
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.row]
    }
    
}


extension APDetailsPresenter: BottomSheetDelegate {
    
    func showBottomSheet() {
        let bottomSheet = interactor.bottomSheet
        bottomSheet.delegate = self
        router.presentBottomSheet(bottomSheet: bottomSheet)
    }
    
    func didSelect(item viewModel: TableViewModel, at indexPath: IndexPath, with tag: Int) {
        guard let bottomSheetRow = BottomSheetRow(rawValue: indexPath.row) else {
            return
        }
        
        switch bottomSheetRow {
        
        case .changeStatus:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.confirmChangeStatus()
            }
        case .delete:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.deleteAutoPayment()
            }
            
        case .close:
            break
        }
    }
    
    func panModalWillDismiss() {
        view.updateSettingsButton()
    }
}
