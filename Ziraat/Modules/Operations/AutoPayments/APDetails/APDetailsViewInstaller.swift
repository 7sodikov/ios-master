//
//  APDetailsViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 21/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol APDetailsViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var tableView: UITableView! {get set}

}

extension APDetailsViewInstaller {
    
    func initSubviews() {
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        
        tableView = .init()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.tableFooterView = .init()
        tableView.allowsSelection = false
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
      
        tableView.constraint { (make) in
            make.top.horizontal.bottom.equalToSuperView()
        }
    
    }
    
    func header(text: String) -> UIView {
        let view = UIView(frame: .init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64))
        view.backgroundColor = .clear
        
        let label = UILabel()
        label.textColor = .black
        label.font = .gothamNarrow(size: 18, .book)
        label.text = text
        
        view.addSubview(label)
        label.constraint { make in
            make.horizontal(16).centerY.equalToSuperView()
        }
        return view
    }
    
}
