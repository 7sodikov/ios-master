//
//  APDetailsInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 21/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APDetailsBusinessLogic: APDetailsBase {
    var bottomSheet: BottomSheet {get}
    var autoPayment: CAutoPayment {get}
    var viewModels: [HorizontalDetailCellViewModel] {get}
    
    func deleteAutoPayment(completion: @escaping()->())
    func changeStatusAutoPayment(completion: @escaping()->())
}

class APDetailsInteractor: APDetailsBusinessLogic {
    
    
    let detailsMode: APDetailsMode
    
    var viewModels: [HorizontalDetailCellViewModel] {
        typealias VM = HorizontalDetailCellViewModel
        
        let fileds = (autoPayment.fields ?? []).map {
            VM(title: $0.title, value: $0.value)
        }
        
        let value = autoPayment.dayOf == .zero ? RS.lbl_EndOfMonth.localized() : autoPayment.dayOf?.description ?? ""
        let amount = ((autoPayment.amount ?? 0) / 100).description.makeReadableAmount
        
        let vm1 = VM(title: RS.lbl_Periodicity.localized(), value: RS.lbl_Monthly.localized())
        
        let vm2 = VM(title: RS.lbl_DayOfMonth.localized(), value: value)
        let vm3 = VM(title: RS.lbl_status.localized(), value: autoPayment.status?.title ?? "")
        let vm4 = VM(title: RS.lbl_card_number.localized(), value: autoPayment.payerNumber ?? "")
        let vm5 = VM(title: RS.lbl_payer.localized(), value: autoPayment.payerOwner ?? "")
        let vm6 = VM(title: RS.lbl_amount.localized(), value: amount)
        
        return fileds + [vm1, vm2, vm3, vm4, vm5, vm6]
    }
    
    var bottomSheet: BottomSheet {
        let rows = BottomSheetRow.allCases.map { row -> DisclosureViewModel in
            let vm = DisclosureViewModel(iconName: row.icon, title: row.title)
            vm.accessoryType = .none
            return vm
        }
        let section = BottomSheetSection(header: "", rows: rows)
        
        return .init(viewHeight: 164, dataSource: [section], with: 0)
    }
    
    var autoPayment: CAutoPayment {
        switch detailsMode {
        case .made(let autoPaymentHistory):
            return autoPaymentHistory.autoPay!
        case .willMake(let autoPayment):
            return autoPayment
        }
    }
    
    init(detailsMode: APDetailsMode) {
        self.detailsMode = detailsMode
    }
    
    
    func changeStatusAutoPayment(completion: @escaping () -> ()) {
        guard let id = autoPayment.autoPayID,
              let status = autoPayment.status else {
            completion()
            return
        }
        NetworkService.AutoPayment().changeStatusAutoPayment(autoPayId: id, status: status.changedStatus) { _ in
            completion()
        }
    }
    
    func deleteAutoPayment(completion: @escaping () -> ()) {
        guard let id = autoPayment.autoPayID else {
            completion()
            return
        }
        NetworkService.AutoPayment().deleteAutoPayment(autoPayId: id) { _ in
            completion()
        }
    }
}
