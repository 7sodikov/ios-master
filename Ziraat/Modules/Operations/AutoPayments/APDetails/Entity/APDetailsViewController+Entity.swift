//
//  APDetailsViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 21/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APDetailsBase {
    typealias BottomSheetRow = APDetailsViewController.Row
}


extension APDetailsViewController {
    
    enum Row: Int, CaseIterable {
        case changeStatus
        case delete
        case close
        
        var title: String {
            switch self {
            case .changeStatus:
                return RS.lbl_change_auto_pay_status.localized()
            case .delete:
                return RS.lbl_delete_auto_payment.localized()
            case .close:
                return RS.lbl_close.localized()
            }
        }
        
        var icon: String {
            switch self {
            case .changeStatus:
                return "lbl_talimat_Guncelle"
            case .delete:
                return  "lbl_talimat_iptal_et"
            case .close:
                return "btn_close"
            }
        }
    }
}

enum APDetailsMode {
    case willMake(autoPayment: CAutoPayment)
    case made(autoPaymentHistory: CAutoPaymentHistory)
}
