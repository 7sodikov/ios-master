//
//  APConfirmInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 14/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APConfirmBusinessLogic: APConfirmBase {
    var dataCollector: AutoPaymentData {get}
    var viewModels: [[TableViewModel]] {get}
}

class APConfirmInteractor: APConfirmBusinessLogic {
    
    let dataCollector: AutoPaymentData
    var status: ResultStatusCellViewModel.Status = .failure(message: RS.lbl_UnknownError.localized())
    
    
    init(dataCollector autopaymentdata: AutoPaymentData) {
        self.dataCollector = autopaymentdata
    }
    
    
}

// MARK: - ViewModels
extension APConfirmInteractor {
    var viewModels: [[TableViewModel]] {
        let textViewModel = SingleLabelCellViewModel(text: RS.lbl_transaction_preview.localized())
        return [[textViewModel] + details] + [buttonsViewModel]
    }
    
    var details: [DetailsCellViewModel] {
        let vm1 = payerViewModel
        vm1.tag = APNavigationStack.transferFractory.rawValue
        
        let vm2 = categoryViewModel
        vm2.tag = APNavigationStack.categories.rawValue
        
        let vm3 = serviceViewModel
        vm3.tag = APNavigationStack.services.rawValue
        
        let vms = (fieldViewModels + [frequencyViewModel, dayOfMonthViewModel, amountViewModel]).map { vm -> DetailsCellViewModel in
            vm.tag = APNavigationStack.supplierFactory.rawValue
            return vm
        }
        
        return [vm1, vm2, vm3] + vms
    }
    
    private var payerViewModel: DetailsCellViewModel {
        let header = RS.lbl_payer.localized()
        let valueTitle = dataCollector.selectedCard?.owner ?? ""
        let value = dataCollector.selectedCard?.pan.makeReadableCardNumber ?? ""
        return .init(headerTitle: header, valueTitle: valueTitle, value: value)
    }
    
    private var categoryViewModel: DetailsCellViewModel {
        let title = RS.lbl_Service.localized()
        let value = dataCollector.category.title
        return .init(headerTitle: title, valueTitle: "", value: value)
    }
    
    private var serviceViewModel: DetailsCellViewModel {
        let title = RS.lbl_Organızation.localized()
        let value = dataCollector.service?.title ?? ""
        return .init(headerTitle: title, valueTitle: "", value: value)
    }
    
    private var fieldViewModels: [DetailsCellViewModel] {
        dataCollector.fields.values.map { (title, value)  -> DetailsCellViewModel in
            .init(headerTitle: title, valueTitle: "", value: value)
        }
    }
    
    private var frequencyViewModel: DetailsCellViewModel {
        let title = RS.lbl_Frequency.localized()
        let value = dataCollector.frequency.localized
        return .init(headerTitle: title, valueTitle: "", value: value)
    }
    
    private var dayOfMonthViewModel: DetailsCellViewModel {
        let title = RS.lbl_DayOfMonth.localized()
        let value = dataCollector.dayOf == .zero ? RS.lbl_EndOfMonth.localized() : dataCollector.dayOf.description
        return .init(headerTitle: title, valueTitle: "", value: value)
    }
    
    private var amountViewModel: DetailsCellViewModel {
        let title = RS.lbl_amount.localized()
        let value = (dataCollector.amount / 100).description.makeReadableAmount
        return .init(headerTitle: title, valueTitle: "", value: value)
    }
    
    private var buttonsViewModel: [ButtonCellViewModel] {
        let confirm = Button.confirm
        let cancel = Button.cancel
        return [
            .init(style: .red(title: confirm.title), isEnabled: true, tag: 0),
            .init(style: .bordered(title: cancel.title), isEnabled: true, tag: 1)
        ]
    }
}

// MARK: - Networking
extension APConfirmInteractor {
    
    
}

