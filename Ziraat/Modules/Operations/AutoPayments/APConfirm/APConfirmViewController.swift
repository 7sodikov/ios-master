//
//  APConfirmViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 14/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol APConfirmDisplayLogic: APConfirmViewInstaller, APConfirmBase {
    func setupTargets()
    func update(title: String)
}


class APConfirmViewController: BaseViewController, APConfirmDisplayLogic  {
    
    var backgroundImage: UIImageView!
    var tableView: UITableView!
    
    var mainView: UIView { self.view }
    var presenter: APConfirmPresentationLogic!
    
    
    override func loadView() {
        super.loadView()
        
        navigationController?.transparentBackgorund()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func setupTargets() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(DetailCenteredCell.self)
        tableView.register(TableViewCell.self)
        tableView.register(ButtonCell.self)
        tableView.register(ButtonsCell.self)
        tableView.register(ResultStatusCell.self)
        tableView.register(ConfirmationCell.self)
        tableView.register(DetailsCell.self)
        tableView.register(SingleLabelCell.self)
    }
    
    func update(title: String) {
        self.title = title
    }
    
    
}

extension APConfirmViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.set(viewModel: viewModel)
        configure(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        CGFloat(section * 16)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        .init()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath) is DetailsCellViewModel {
            return presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath).estimatedRowHeight
        }
        return UITableView.automaticDimension
    }
    
    func configure(cell: UITableViewCell) {
        (cell as? ButtonCell)?.buttonDelegate = self
        (cell as? ButtonsCell)?.buttonDelegate = self
        (cell as? DetailsCell)?.editDelegate = self
    }
}

extension APConfirmViewController: ButtonsCellDelegate  {
    
    func buttonCellsButtonClicked(at tag: Int, isLeft: Bool) {
        presenter.buttonClicked(isNext: !isLeft)
    }
    
}

extension APConfirmViewController: ButtonCellDelegate {
    
    func buttonClicked(tag: Int) {
        if let button = Button(rawValue: tag) {
            presenter.buttonClicked(isNext: button == .confirm)
        }
    }
    
}

extension APConfirmViewController: DetailsCellDelegate {
    
    func editButtonClicked(tag: Int) {
        presenter.willNavigateToNeed(at: tag)
    }
    
}

