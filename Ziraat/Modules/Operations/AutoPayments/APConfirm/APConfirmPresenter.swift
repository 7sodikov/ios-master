//
//  APConfirmPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 14/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol APConfirmPresentationLogic: PresentationProtocol, APConfirmBase, TablePresentationLogic {
    
    func willNavigateToNeed(at tag: Int)
    func buttonClicked(isNext: Bool)
}


class APConfirmPresenter: APConfirmPresentationLogic {
    
    
    private unowned let view: APConfirmDisplayLogic
    private let interactor: APConfirmBusinessLogic
    private let router: APConfirmRoutingLogic
    
    private var controller: APConfirmViewController { view as! APConfirmViewController }
    
    init(view: APConfirmDisplayLogic, interactor: APConfirmBusinessLogic, router: APConfirmRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        view.setupSubviews()
        view.setupTargets()
        if let title = interactor.dataCollector.service?.title {
            view.update(title: title)
        }
    }
    
    func willNavigateToNeed(at tag: Int) {
        guard let navStackItem = APNavigationStack(rawValue: tag) else { return }
        router.navigate(to: navStackItem)
    }
    
    func buttonClicked(isNext: Bool) {
        isNext
            ? router.navigateToResultScreeen(autopaymentdata: interactor.dataCollector)
            : confirmate()
    }
    
    func confirmate() {
        let controller = NotifyViewController(.confirmationConversion) {
            self.router.backToRoot()
        }
        self.controller.present(controller, animated: false)
    }
    
    func reloadTableView(at page: Page) {
        view.tableView.reloadData()
    }
}


// MARK: - TablePresentationLogic

extension APConfirmPresenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels[section].count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.section][indexPath.row]
    }
    
}

// MARK: - Sending mail
extension APConfirmPresenter {
    
}
