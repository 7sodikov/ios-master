//
//  APConfirmRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 14/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APConfirmRoutingLogic {
    func back()
    func navigatetoPdfChequeVC(operationId: String)
    func backToRoot()
    func navigate(to navStackItem: APNavigationStack)
    func navigateToResultScreeen(autopaymentdata: AutoPaymentData)
}

class APConfirmRouter: BaseRouter, APConfirmRoutingLogic {
    
    
    init(dataCollector autopaymentdata: AutoPaymentData) {
        let controller = APConfirmViewController()
        super.init(viewController: controller)
        
        let interactor = APConfirmInteractor(dataCollector: autopaymentdata)
        let presenter = APConfirmPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func navigatetoPdfChequeVC(operationId: String) {
        let pdfView = PdfViewController(operation: operationId, title: viewController.title ?? "")
        viewController.navigationController?.pushViewController(pdfView, animated: true)
    }
    
    func backToRoot() {
        viewController.navigationController?.popToRootViewController(animated: true)
    }
    
    func navigate(to navStackItem: APNavigationStack) {
        backTo(nScreeen: navStackItem.nBack)
    }
    
    func navigateToResultScreeen(autopaymentdata: AutoPaymentData) {
        let router = APResultRouter(dataCollector: autopaymentdata)
        push(router, animated: true)
    }
}
