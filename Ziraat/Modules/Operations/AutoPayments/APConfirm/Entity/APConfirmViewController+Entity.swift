//
//  APConfirmViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 14/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol APConfirmBase {
    
    typealias Page = APConfirmViewController.Page
    typealias Button = APConfirmViewController.Button
}


extension APConfirmViewController {
    
    enum Page: Int {
        case info = 2
        case result
        
        var operationStackOrder: OperationStackOrderView.StackOrder {
            switch self {
            
            case .info:
                return .two
            case .result:
                return .four
            }
        }
        
        
        var next: Page {
            if case .result = self {
                return self
            }
            return Page(rawValue: self.rawValue + 1) ?? .result
        }
        
        var back: Page {
            self == .info ? self : Page(rawValue: self.rawValue - 1) ?? .info
        }
        
    }
    
    enum Button: Int {
        case confirm
        case cancel
        case addToFavourite
        case print
        case close
        case backToMain
        
        var title: String {
            switch self {
            
            case .confirm:
                return RS.btn_confirm.localized().uppercased()
            case .cancel:
                return RS.lbl_cancel.localized().uppercased()
            case .addToFavourite:
                return RS.btn_add_to_favourites.localized()
            case .print:
                return RS.btn_print.localized()
            case .close:
                return RS.btn_close.localized()
            case .backToMain:
                return RS.btn_return_to_homepage.localized()
            }
        }
    }
    
}

enum APNavigationStack: Int, CaseIterable {
    case transferFractory = 0
    case categories = 1
    case services = 2
    case supplierFactory = 3
    
    var screenType: UIViewController.Type {
        switch self {
            
        case .categories:
            return SuppliersViewController.self
        case .services:
            return SuppliersViewController.self
        case .supplierFactory:
            return APSupplierFactoryViewController.self
        case .transferFractory:
            return APTransferFactoryViewController.self
        }
    }
    
    var nBack: Int {
        switch self {
        
        case .transferFractory:
            return 2
        case .categories:
            return 5
        case .services:
            return 4
        case .supplierFactory:
            return 3
        }
    }
    
}
