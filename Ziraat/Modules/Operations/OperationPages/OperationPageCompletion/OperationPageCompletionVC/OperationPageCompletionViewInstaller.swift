//
//  OperationPageCompletionViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import PDFKit

protocol OperationPageCompletionViewInstaller: ViewInstaller {
    var receiptPDFView: PDFView! { get set }
    var successfulImageView: UIImageView! { get set }
    var successfulLabel: UILabel! { get set }
    var addToFavoriteButton: NextButton! { get set }
    var printButton: NextButton! { get set }
    var buttonsStackView: UIStackView! { get set }
    var closeButton: NextButton! { get set }
}

extension OperationPageCompletionViewInstaller {
    func initSubviews() {
        receiptPDFView = PDFView()
        receiptPDFView.displayMode = .singlePageContinuous
        receiptPDFView.autoScales = true
        receiptPDFView.displayDirection = .vertical
        receiptPDFView.layer.borderWidth = 1
        receiptPDFView.layer.borderColor = UIColor.black.cgColor
        receiptPDFView.isHidden = true
        
        successfulImageView = UIImageView()
        successfulImageView.image = UIImage(named: "img_success")
        
        successfulLabel = UILabel()
        successfulLabel.font = .gothamNarrow(size: 16, .book)
        successfulLabel.textColor = ColorConstants.gray
        successfulLabel.textAlignment = .center
        successfulLabel.text = RS.lbl_payment_success.localized()
        
        addToFavoriteButton = NextButton.systemButton(with: RS.btn_add_to_favourites.localized(), cornerRadius: Size.NextButton.height/2)
        addToFavoriteButton.titleLabel?.lineBreakMode = .byWordWrapping
        addToFavoriteButton.titleLabel?.textAlignment = .center
        addToFavoriteButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        addToFavoriteButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        addToFavoriteButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        printButton = NextButton.systemButton(with: RS.btn_print.localized(), cornerRadius: Size.NextButton.height/2)
        printButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        printButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        printButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        buttonsStackView = UIStackView()
        buttonsStackView.spacing = Adaptive.val(17)
        buttonsStackView.alignment = .fill
        buttonsStackView.distribution = .fillEqually
        
        closeButton = NextButton.systemButton(with: RS.btn_close.localized().uppercased(), cornerRadius: Size.NextButton.height/2)
        closeButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        closeButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        closeButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
    }
    
    func embedSubviews() {
        mainView.addSubview(receiptPDFView)
        mainView.addSubview(successfulImageView)
        mainView.addSubview(successfulLabel)
        mainView.addSubview(buttonsStackView)
        buttonsStackView.addArrangedSubview(addToFavoriteButton)
        buttonsStackView.addArrangedSubview(printButton)
        mainView.addSubview(closeButton)
    }
    
    func addSubviewsConstraints() {
        receiptPDFView.snp.remakeConstraints { (maker) in
            maker.top.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.bottom.equalTo(buttonsStackView.snp.top).offset(-ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
        }
        
        successfulImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(80))
            maker.centerX.equalToSuperview()
//            maker.bottom.equalTo(successfulLabel.snp.top).offset(-Adaptive.val(15))
            maker.top.equalTo(Adaptive.val(100))
        }
        
        successfulLabel.snp.remakeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(successfulImageView.snp.bottom).offset(Adaptive.val(15))
        }
        
        addToFavoriteButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.NextButton.height)
        }
        
        printButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(addToFavoriteButton)
        }
        
        buttonsStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ApplicationSize.paddingLeftRight)
            maker.trailing.equalTo(-ApplicationSize.paddingLeftRight)
            maker.bottom.equalTo(closeButton.snp.top).offset(-Adaptive.val(20))
        }
        
        closeButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ApplicationSize.paddingLeftRight)
            maker.trailing.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(addToFavoriteButton)
            maker.bottom.equalTo(-Adaptive.val(50))
        }
    }
}

fileprivate struct Size {
    struct NextButton {
        static let height = Adaptive.val(54)
    }
}
