//
//  OperationPageCompletionVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import PDFKit

protocol OperationPageCompletionVCProtocol: BaseViewControllerProtocol {
    func show(receipt: String)
    func showFavoriteAddedAlert()
}

class OperationPageCompletionVC: BaseViewController, OperationPageCompletionViewInstaller {
    var mainView: UIView { view }
    var receiptPDFView: PDFView!
    var successfulImageView: UIImageView!
    var successfulLabel: UILabel!
    var addToFavoriteButton: NextButton!
    var printButton: NextButton!
    var buttonsStackView: UIStackView!
    var closeButton: NextButton!
    
    private var receiptStr: String?
    
    var presenter: OperationPageCompletionPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        
        if let imageData = receiptStr?.dataFromBase64 {
            receiptPDFView.document = PDFDocument(data: imageData)
        }
        
        addToFavoriteButton.addTarget(self, action: #selector(addToFavoriteButtonClicked(_:)), for: .touchUpInside)
        printButton.addTarget(self, action: #selector(printButtonClicked(_:)), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc private func addToFavoriteButtonClicked(_ sender: UIButton) {
        presenter.addToFavoriteButtonClicked()
    }
    
    @objc private func printButtonClicked(_ sender: UIButton) {
        presenter.printButtonClicked()
    }
    
    @objc private func closeButtonClicked(_ sender: UIButton) {
        presenter.closeButtonClicked()
    }
}

extension OperationPageCompletionVC: OperationPageCompletionVCProtocol {
    func show(receipt: String) {
        receiptStr = receipt
        if let imageData = receiptStr?.dataFromBase64 {
            receiptPDFView?.document = PDFDocument(data: imageData)
        }
    }
    
    func showFavoriteAddedAlert() {
        let alert = UIAlertController(title: RS.lbl_success.localized(),
                                      message: RS.lbl_fav_added.localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: RS.btn_yes.localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
