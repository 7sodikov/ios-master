//
//  OperationPageCompletionRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageCompletionRouterProtocol: class {
    static func createModule(with delegate: OperationsPresenter?) -> UIViewController
    func closePToPTransferScreen(with delegate: OperationPageCompletionDelegate?)
    func openFavoriteCreationView(from controller: Any?, delegate: FavoriteAddDelegate?, operationId: Int)
    func navigatetoPdfChequeVC(in navigateCtrl: Any, operation: String)
}

class OperationPageCompletionRouter: OperationPageCompletionRouterProtocol {
    static func createModule(with delegate: OperationsPresenter?) -> UIViewController {
        let vc = OperationPageCompletionVC()
        let presenter = OperationPageCompletionPresenter()
        let interactor = OperationPageCompletionInteractor()
        let router = OperationPageCompletionRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func openFavoriteCreationView(from controller: Any?, delegate: FavoriteAddDelegate?, operationId: Int) {
        let ctrl = FavoriteAddRouter.createModule(for: operationId, delegate: delegate)
        ctrl.modalPresentationStyle = .overCurrentContext
        (controller as? UIViewController)?.present(ctrl, animated: true, completion: nil)
    }
    
    func closePToPTransferScreen(with delegate: OperationPageCompletionDelegate?) {
        delegate?.completionPageCloseClicked()
    }
    
    func navigatetoPdfChequeVC(in navigateCtrl: Any, operation: String) {
        let viewCtrl = navigateCtrl as! UIViewController
        let statementVC = PdfChequeRouter.createModule(operation: operation, title: RS.lbl_transfer_to_card.localized(), isHidden: false)
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
}
