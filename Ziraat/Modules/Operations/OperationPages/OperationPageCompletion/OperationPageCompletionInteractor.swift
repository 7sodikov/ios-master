//
//  OperationPageCompletionInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageCompletionInteractorProtocol: class {
    
}

class OperationPageCompletionInteractor: OperationPageCompletionInteractorProtocol {
    weak var presenter: OperationPageCompletionInteractorToPresenterProtocol!
    
}
