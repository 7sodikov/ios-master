//
//  OperationPageCompletionPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageCompletionDelegate: class {
    func completionPageCloseClicked()
}

protocol OperationPageCompletionPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: OperationPageCompletionVM { get }
    func closeButtonClicked()
    func addToFavoriteButtonClicked()
    func printButtonClicked()
}

protocol OperationPageCompletionInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class OperationPageCompletionPresenter: OperationPageCompletionPresenterProtocol {
    weak var view: OperationPageCompletionVCProtocol!
    var interactor: OperationPageCompletionInteractorProtocol!
    var router: OperationPageCompletionRouterProtocol!
    weak var delegate: OperationPageCompletionDelegate?
    var smsConfirmResponse: P2PConfirmResponse! {
        didSet {
            view.show(receipt: smsConfirmResponse.receipt)
        }
    }
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationPageCompletionVM()
    
    func closeButtonClicked() {
        router.closePToPTransferScreen(with: delegate)
    }
    
    func addToFavoriteButtonClicked() {
        router.openFavoriteCreationView(from: view, delegate: self, operationId: smsConfirmResponse.operationId)
    }
    
    func printButtonClicked() {
        router.navigatetoPdfChequeVC(in: view as Any, operation: smsConfirmResponse.receipt)
    }
    
    // Private property and methods
    
}

extension OperationPageCompletionPresenter: OperationPageCompletionInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}

extension OperationPageCompletionPresenter: FavoriteAddDelegate {
    func favoriteDidAddSuccessfully() {
        view.showFavoriteAddedAlert()
    }
}
