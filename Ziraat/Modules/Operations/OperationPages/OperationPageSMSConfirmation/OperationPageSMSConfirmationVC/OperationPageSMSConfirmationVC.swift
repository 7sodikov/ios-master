//
//  OperationPageSMSConfirmationVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageSMSConfirmationVCProtocol: BaseViewControllerProtocol {
    func nextButton(enabled: Bool)
    func startTimer()
    func stopTimer()
    func resendSMSButton(enabled: Bool)
    func resendSMSButtonPreloader(show: Bool)
}

class OperationPageSMSConfirmationVC: BaseViewController, OperationPageSMSConfirmationViewInstaller {
    var mainView: UIView { view }
    var textFieldTitleLabel: UILabel!
    var textField: SMSConfirmationTextField!
    var timerView: TimerView!
    var resendSMSButton: NextButton!
    var nextButton: NextButton!
    var buttonsStackView: UIStackView!
    
    var presenter: OperationPageSMSConfirmationPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        timerView.startTimer(with: timerValue)
        timerView.timerReachedItsEnd = {
            self.presenter.timerReachedItsEnd()
        }
        resendSMSButton.addTarget(self, action: #selector(resendSMSButtonClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
        resendSMSButton.isEnabled = !show
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        presenter.textFieldDidChange(with: sender.text)
    }
    
    @objc private func resendSMSButtonClicked(_ sender: UIButton) {
        presenter.resendSMSButtonClicked()
    }
    
    @objc private func nextButtonClicked(_ sender: UIButton) {
        presenter.nextButtonClicked()
    }
}

extension OperationPageSMSConfirmationVC: OperationPageSMSConfirmationVCProtocol {
    func nextButton(enabled: Bool) {
        nextButton.isEnabled = enabled
    }
    
    func startTimer() {
        timerView?.startTimer(with: timerValue)
    }
    
    func stopTimer() {
        timerView.stopTimer()
    }
    
    func resendSMSButton(enabled: Bool) {
        resendSMSButton.isEnabled = enabled
    }
    
    func resendSMSButtonPreloader(show: Bool) {
        resendSMSButton.animateIndicator = show
    }
}
