//
//  OperationPageSMSConfirmationViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageSMSConfirmationViewInstaller: ViewInstaller {
    var textFieldTitleLabel: UILabel! { get set }
    var textField: SMSConfirmationTextField! { get set }
    var timerView: TimerView! { get set }
    var resendSMSButton: NextButton! { get set }
    var nextButton: NextButton! { get set }
    var buttonsStackView: UIStackView! { get set }
}

extension OperationPageSMSConfirmationViewInstaller {
    func initSubviews() {
        textFieldTitleLabel = UILabel()
        textFieldTitleLabel.font = EZFontType.regular.sfProText(size: 16.5)
        textFieldTitleLabel.textColor = .black
        textFieldTitleLabel.text = RS.lbl_sms_code.localized()
        
        textField = SMSConfirmationTextField(cornerRadius: Size.TextField.height/2,
                                             isSecureTextEntry: false)
        
        timerView = TimerView(frame: CGRect(x: 0, y: 0, width: ApplicationSize.Timer.width, height: ApplicationSize.Timer.width))
        timerView.titleColor = .black
        timerView.shapeColor = ColorConstants.mainRed
        timerView.backShapeColor = ColorConstants.mainRed.withAlphaComponent(0.40)
        
        resendSMSButton = NextButton.systemButton(with: RS.btn_resend_sms.localized(), cornerRadius: Size.TextField.height/2)
        resendSMSButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        resendSMSButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        resendSMSButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        resendSMSButton.isEnabled = false
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: Size.TextField.height/2)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        nextButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        nextButton.isEnabled = false
        
        buttonsStackView = UIStackView()
        buttonsStackView.spacing = Adaptive.val(17)
        buttonsStackView.alignment = .fill
        buttonsStackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(textFieldTitleLabel)
        mainView.addSubview(textField)
        mainView.addSubview(timerView)
        mainView.addSubview(buttonsStackView)
        
        buttonsStackView.addArrangedSubview(resendSMSButton)
        buttonsStackView.addArrangedSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        textFieldTitleLabel.snp.remakeConstraints { (maker) in
            let leftRight = ApplicationSize.paddingLeftRight + Size.TextField.height/2
            maker.top.equalTo(ApplicationSize.paddingLeftRight)
            maker.left.equalTo(leftRight)
            maker.right.equalTo(-leftRight)
        }
        
        textField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(textFieldTitleLabel.snp.bottom).offset(Adaptive.val(5))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(Size.TextField.height)
        }
        
        timerView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(textField.snp.bottom).offset(Adaptive.val(70))
            maker.centerX.equalToSuperview()
            maker.width.height.equalTo(ApplicationSize.Timer.width)
        }
        
        resendSMSButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.TextField.height)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(resendSMSButton)
        }
        
        buttonsStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ApplicationSize.paddingLeftRight)
            maker.trailing.equalTo(-ApplicationSize.paddingLeftRight)
            maker.bottom.equalTo(-Adaptive.val(50))
        }
    }
}

fileprivate struct Size {
    struct TextField {
        static let height = Adaptive.val(54)
    }
}
