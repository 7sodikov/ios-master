//
//  OperationPageSMSConfirmationRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageSMSConfirmationRouterProtocol: class {
    static func createModule(with delegate: OperationsPresenter?) -> UIViewController
//    func navigateToPreviousPage(with delegate: OperationPageSMSConfirmationDelegate?)
    func navigateToNextPage(with smsConfirmResponse: P2PConfirmResponse, delegate: OperationPageSMSConfirmationDelegate?)
}

class OperationPageSMSConfirmationRouter: OperationPageSMSConfirmationRouterProtocol {
    static func createModule(with delegate: OperationsPresenter?) -> UIViewController {
        let vc = OperationPageSMSConfirmationVC()
        let presenter = OperationPageSMSConfirmationPresenter()
        let interactor = OperationPageSMSConfirmationInteractor()
        let router = OperationPageSMSConfirmationRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
//    func navigateToPreviousPage(with delegate: OperationPageSMSConfirmationDelegate?) {
//        delegate?.smsConfirmationPageBackButtonClicked()
//    }
    
    func navigateToNextPage(with smsConfirmResponse: P2PConfirmResponse, delegate: OperationPageSMSConfirmationDelegate?) {
        delegate?.smsConfirmationPageFinished(with: smsConfirmResponse)
    }
}
