//
//  OperationPageSMSConfirmationInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageSMSConfirmationInteractorProtocol: class {
    func operationConfirm(for token: String, code: String)
    func operationSMSResend(for token: String)
}

class OperationPageSMSConfirmationInteractor: OperationPageSMSConfirmationInteractorProtocol {
    weak var presenter: OperationPageSMSConfirmationInteractorToPresenterProtocol!
    func operationConfirm(for token: String, code: String) {
        NetworkService.Operation.operationConfirm(token: token, code: code) { [weak self] (result) in
            self?.presenter.didOperationConfirm(with: result)
        }
    }
    
    func operationSMSResend(for token: String) {
        NetworkService.Operation.operationSMSResend(token: token) { [weak self] (result) in
            self?.presenter.didOperationSMSResend(with: result)
        }
    }
}
