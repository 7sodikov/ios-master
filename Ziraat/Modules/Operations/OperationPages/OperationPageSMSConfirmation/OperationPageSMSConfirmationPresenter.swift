//
//  OperationPageSMSConfirmationPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageSMSConfirmationDelegate: class {
    func smsConfirmationPageBackButtonClicked()
    func smsConfirmationPageFinished(with smsConfirmResponse: P2PConfirmResponse)
}

protocol OperationPageSMSConfirmationPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: OperationPageSMSConfirmationVM { get }
    func textFieldDidChange(with value: String?)
    func resendSMSButtonClicked()
    func nextButtonClicked()
    func timerReachedItsEnd()
}

protocol OperationPageSMSConfirmationInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didOperationConfirm(with response: ResponseResult<P2PConfirmResponse, AppError>)
    func didOperationSMSResend(with response: ResponseResult<SingleMessageResponse, AppError>)
}

class OperationPageSMSConfirmationPresenter: OperationPageSMSConfirmationPresenterProtocol {
    weak var view: OperationPageSMSConfirmationVCProtocol!
    var interactor: OperationPageSMSConfirmationInteractorProtocol!
    var router: OperationPageSMSConfirmationRouterProtocol!
    weak var delegate: OperationPageSMSConfirmationDelegate?
    var prepareResponse: OperationPrepareResponse!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationPageSMSConfirmationVM()
    
    func textFieldDidChange(with value: String?) {
        viewModel.smsCode = value
        view.nextButton(enabled: viewModel.smsCode?.count == 6)
    }
    
    func resendSMSButtonClicked() {
        view.resendSMSButtonPreloader(show: true)
        interactor.operationSMSResend(for: prepareResponse.token)
    }
    
    func nextButtonClicked() {
        view.preloader(show: true)
        interactor.operationConfirm(for: prepareResponse.token, code: viewModel.smsCode ?? "")
    }
    
    func timerReachedItsEnd() {
        view.resendSMSButton(enabled: true)
    }
    
    // Private property and methods
    
}

extension OperationPageSMSConfirmationPresenter: OperationPageSMSConfirmationInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didOperationConfirm(with response: ResponseResult<P2PConfirmResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            view.stopTimer()
            router.navigateToNextPage(with: result.data, delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
    
    func didOperationSMSResend(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.resendSMSButtonPreloader(show: false)
        switch response {
        case .success:
            view.startTimer()
            view.resendSMSButton(enabled: false)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
}
