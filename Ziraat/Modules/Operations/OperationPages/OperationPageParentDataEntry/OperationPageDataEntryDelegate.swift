//
//  OperationPageDataEntryDelegate.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageDataEntryDelegate: class {
    func dataEntryPageFinished(with prepareResponse: OperationPrepareResponse, operationPrepareType: OperationPrepareType)
}
