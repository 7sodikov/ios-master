//
//  OperationPageParentDataEntryViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageParentDataEntryViewInstaller: ViewInstaller {
    var tableView: UITableView! { get set }
    var nextButton: NextButton! { get set }
}

extension OperationPageParentDataEntryViewInstaller {
    func initSubviews() {
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.register(OperationPageDataEntryChooseMyCardTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataEntryChooseMyCardTableCell.self))
        tableView.register(OperationPageDataEntryReceiversCardTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataEntryReceiversCardTableCell.self))
        tableView.register(OperationPageDataEntryAmountTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataEntryAmountTableCell.self))
        tableView.register(OperationPageDataEntryReceiversAccountNumberTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataEntryReceiversAccountNumberTableCell.self))
        tableView.register(OperationPageDataEntryReceiversAccountMFOTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataEntryReceiversAccountMFOTableCell.self))
        tableView.register(OperationPageDataEntryLoanNumberTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataEntryLoanNumberTableCell.self))
        tableView.register(OperationPageDataEntryLoanPaymentTypeTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataEntryLoanPaymentTypeTableCell.self))
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: Size.NextButton.height/2)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        nextButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        nextButton.isEnabled = false
    }
    
    func embedSubviews() {
        mainView.addSubview(tableView)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        tableView.snp.remakeConstraints { (maker) in
            maker.top.left.right.equalToSuperview()
            maker.bottom.equalTo(nextButton.snp.top).offset(-Adaptive.val(20))
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(-Adaptive.val(50))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(Size.NextButton.height)
        }
    }
}

fileprivate struct Size {
    struct NextButton {
        static let height = Adaptive.val(54)
    }
}
