//
//  OperationPageParentDataEntryVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageParentDataEntryVCProtocol: BaseViewControllerProtocol {
    func nextButton(enable: Bool)
    func reloadTable()
}

class OperationPageParentDataEntryVC: BaseViewController, OperationPageParentDataEntryViewInstaller {
    var mainView: UIView { view }
    var tableView: UITableView!
    var nextButton: NextButton!
    
//    var presenter: OperationPageParentDataEntryPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
    }
}

extension OperationPageParentDataEntryVC: OperationPageParentDataEntryVCProtocol {
    func nextButton(enable: Bool) {
        nextButton.isEnabled = enable
    }
    
    func reloadTable() {
        tableView.reloadData()
    }
}
