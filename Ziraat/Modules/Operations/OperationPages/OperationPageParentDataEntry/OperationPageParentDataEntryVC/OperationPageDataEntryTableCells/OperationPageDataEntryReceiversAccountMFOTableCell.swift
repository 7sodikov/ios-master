//
//  OperationPageDataEntryReceiversAccountMFOTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/8/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol OperationPageDataEntryReceiversAccountMFOTableCellDelegate: class {
    func didEnter(accountMFO: String?)
}

class OperationPageDataEntryReceiversAccountMFOTableCell: LoanCalculatorTableCell {
    let textFieldListener = MaskedTextFieldDelegate()
    weak var delegate: OperationPageDataEntryReceiversAccountMFOTableCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textFieldListener.affinityCalculationStrategy = .prefix
        textFieldListener.primaryMaskFormat = "[00000]"
        textFieldListener.listener = self
        textField.keyboardType = .numberPad
        textField.delegate = textFieldListener
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataEntryTableCellType, value: String?) -> OperationPageDataEntryReceiversAccountMFOTableCell {
        titleLabel.text = cellType.title
        textField.set(placeholder: cellType.textFieldPlaceholder)
        textFieldListener.put(text: value ?? "", into: textField)
        return self
    }
}

extension OperationPageDataEntryReceiversAccountMFOTableCell: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        delegate?.didEnter(accountMFO: value)
    }
}
