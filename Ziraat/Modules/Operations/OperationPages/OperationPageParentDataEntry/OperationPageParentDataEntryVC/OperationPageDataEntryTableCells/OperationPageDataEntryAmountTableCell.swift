//
//  OperationPageDataEntryAmountTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageDataEntryAmountTableCellDelegate: class {
    func didEnter(amount: Double?)
}

class OperationPageDataEntryAmountTableCell: LoanCalculatorTableCell {
    weak var delegate: OperationPageDataEntryAmountTableCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataEntryTableCellType, value: Double?) -> OperationPageDataEntryAmountTableCell {
        titleLabel.text = cellType.title
        textField.set(placeholder: cellType.textFieldPlaceholder)
        if let value = value {
            textField.text = "\(value)"
        }
        
        return self
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        guard let amountString = sender.text?.digits else {
            delegate?.didEnter(amount: nil)
            return
        }
        
        guard let amount = Int64(amountString) else {
            delegate?.didEnter(amount: nil)
            return
        }
        
        textField.text = amount.currencyFormattedStr()
        let doubleAmount = Double(amount)
        delegate?.didEnter(amount: doubleAmount)
    }
}
