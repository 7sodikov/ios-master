//
//  OperationPageDataEntryReceiversAccountNumberTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/8/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol OperationPageDataEntryReceiversAccountNumberTableCellDelegate: class {
    func didEnter(accountNumber: String?, complete: Bool)
}

class OperationPageDataEntryReceiversAccountNumberTableCell: LoanCalculatorTableCell {
    let textFieldListener = MaskedTextFieldDelegate()
    weak var delegate: OperationPageDataEntryReceiversAccountNumberTableCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textFieldListener.affinityCalculationStrategy = .prefix
        textFieldListener.primaryMaskFormat = "[00000000000000000000]"
        textFieldListener.listener = self
        textField.keyboardType = .numberPad
        textField.delegate = textFieldListener
        setPreloader()
        textField.rightViewMode = .always
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataEntryTableCellType, value: String?) -> OperationPageDataEntryReceiversAccountNumberTableCell {
        titleLabel.text = cellType.title
        textField.set(placeholder: cellType.textFieldPlaceholder)
        textFieldListener.put(text: value ?? "", into: textField)
        return self
    }
}

extension OperationPageDataEntryReceiversAccountNumberTableCell: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        delegate?.didEnter(accountNumber: value, complete: complete)
    }
}
