//
//  OperationPageDataEntryLoanPaymentTypeTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class OperationPageDataEntryLoanPaymentTypeTableCell: LoanCalculatorTableCell {
    
    lazy var paymentTypeDropdownList: EZDropdownList = {
        let ddList = EZDropdownList(frame: .zero)
        ddList.layer.cornerRadius = LoanCalculatorTableCellSize.TextField.height/2
        ddList.allowNonSelection = true
        return ddList
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textField.isHidden = true
        mainView.addSubview(paymentTypeDropdownList)
        paymentTypeDropdownList.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(textField)
        }
        paymentTypeDropdownList.setupProperties()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataEntryTableCellType, selectedIndex: Int) -> OperationPageDataEntryLoanPaymentTypeTableCell {
        titleLabel.text = cellType.title
        paymentTypeDropdownList.placeholder = cellType.textFieldPlaceholder
        paymentTypeDropdownList.selectedIndex = selectedIndex
        return self
    }
}
