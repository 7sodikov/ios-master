//
//  OperationPageDataEntryChooseMyCardTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class OperationPageDataEntryChooseMyCardTableCell: LoanCalculatorTableCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textField.isUserInteractionEnabled = false
        setPreloader()
        textField.rightViewMode = .always
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataEntryTableCellType, value: String?) -> OperationPageDataEntryChooseMyCardTableCell {
        titleLabel.text = cellType.title
        textField.set(placeholder: cellType.textFieldPlaceholder)
        textField.text = value
        return self
    }
}
