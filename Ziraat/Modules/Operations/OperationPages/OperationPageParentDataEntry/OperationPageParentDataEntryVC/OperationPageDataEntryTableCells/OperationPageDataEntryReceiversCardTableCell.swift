//
//  OperationPageDataEntryReceiversCardTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol OperationPageDataEntryReceiversCardTableCellDelegate: class {
    func didEnter(receiversCard: String, complete: Bool)
}

class OperationPageDataEntryReceiversCardTableCell: LoanCalculatorTableCell {
    let textFieldListener = MaskedTextFieldDelegate()
    weak var delegate: OperationPageDataEntryReceiversCardTableCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textFieldListener.affinityCalculationStrategy = .prefix
        textFieldListener.primaryMaskFormat = "[0000] [0000] [0000] [0000]"
        textFieldListener.listener = self
        textField.keyboardType = .numberPad
        textField.delegate = textFieldListener
        setPreloader()
        textField.rightViewMode = .always
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataEntryTableCellType, value: String?, receiverOwnerName: String?) -> OperationPageDataEntryReceiversCardTableCell {
        titleLabel.text = cellType.title
        textField.set(placeholder: cellType.textFieldPlaceholder)
        textFieldListener.put(text: value ?? "", into: textField)
        subtitleLabel.text = receiverOwnerName
        return self
    }
}

extension OperationPageDataEntryReceiversCardTableCell: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        delegate?.didEnter(receiversCard: value, complete: complete)
    }
}
