//
//  OperationPageDataEntryLoanNumberTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol OperationPageDataEntryLoanNumberTableCellDelegate: class {
    func didEnter(loanId: String?)
}

class OperationPageDataEntryLoanNumberTableCell: LoanCalculatorTableCell {
    let textFieldListener = MaskedTextFieldDelegate()
    weak var delegate: OperationPageDataEntryLoanNumberTableCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        textFieldListener.affinityCalculationStrategy = .prefix
        textFieldListener.primaryMaskFormat = "[0…]"
        textFieldListener.listener = self
        textField.keyboardType = .numberPad
        textField.delegate = textFieldListener
        setPreloader()
        textField.rightViewMode = .always
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataEntryTableCellType, value: String?) -> OperationPageDataEntryLoanNumberTableCell {
        titleLabel.text = cellType.title
        textField.set(placeholder: cellType.textFieldPlaceholder)
        textFieldListener.put(text: value ?? "", into: textField)
        return self
    }
}

extension OperationPageDataEntryLoanNumberTableCell: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        delegate?.didEnter(loanId: value)
    }
}
