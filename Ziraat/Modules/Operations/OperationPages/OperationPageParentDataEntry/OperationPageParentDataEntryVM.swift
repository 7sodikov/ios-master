//
//  OperationPageParentDataEntryVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum OperationPageDataEntryTableCellType {
    case payerCard
    case receiverCardNumber
    case receiverAccount
    case receiverAccountMFO
    case loanId
    case loanPaymentType
    case amount
    
    var title: String {
        switch self {
        case .payerCard: return RS.lbl_payer.localized()
        case .receiverCardNumber: return RS.lbl_receiver.localized()
        case .receiverAccount: return RS.lbl_account_number2.localized()
        case .receiverAccountMFO: return RS.lbl_mfo.localized()
        case .loanId: return RS.lbl_loan_id.localized()
        case .loanPaymentType: return RS.lbl_repayment_type.localized()
        case .amount: return RS.lbl_amount.localized()
        }
    }
    
    var textFieldPlaceholder: String {
        switch self {
        case .payerCard: return RS.lbl_choose_card.localized()
        case .receiverCardNumber: return "XXXX XXXX XXXX XXXX"
        case .receiverAccount: return RS.lbl_account_number2.localized()
        case .receiverAccountMFO: return "XXXXX"
        case .loanId: return RS.lbl_loan_id.localized()
        case .loanPaymentType: return RS.lbl_repayment_type.localized()
        case .amount: return "1000"
        }
    }
}

class OperationPageParentDataEntryVM {
    var tableCells: [OperationPageDataEntryTableCellType] = []
    
    func index(for cell: OperationPageDataEntryTableCellType) -> Int {
        for i in 0..<tableCells.count {
            let item = tableCells[i]
            if item == cell {
                return i
            }
        }
        return -1
    }
}
