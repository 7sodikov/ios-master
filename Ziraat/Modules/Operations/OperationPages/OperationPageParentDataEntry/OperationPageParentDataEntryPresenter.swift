//
//  OperationPageParentDataEntryPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageParentDataEntryPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: OperationPageParentDataEntryVM { get }
}

protocol OperationPageParentDataEntryInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class OperationPageParentDataEntryPresenter: OperationPageParentDataEntryPresenterProtocol {
    weak var view: OperationPageParentDataEntryVCProtocol!
    var interactor: OperationPageParentDataEntryInteractorProtocol!
    var router: OperationPageParentDataEntryRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationPageParentDataEntryVM()
    
    // Private property and methods
    
}

extension OperationPageParentDataEntryPresenter: OperationPageParentDataEntryInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
