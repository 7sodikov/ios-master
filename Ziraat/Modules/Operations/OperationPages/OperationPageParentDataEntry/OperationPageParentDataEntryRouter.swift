//
//  OperationPageParentDataEntryRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageParentDataEntryRouterProtocol: class {
    static func createModule() -> UIViewController
}

class OperationPageParentDataEntryRouter: OperationPageParentDataEntryRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = OperationPageParentDataEntryVC()
        let presenter = OperationPageParentDataEntryPresenter()
        let interactor = OperationPageParentDataEntryInteractor()
        let router = OperationPageParentDataEntryRouter()
        
//        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
