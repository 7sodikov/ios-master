//
//  OperationPageP2ADataEntryInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageP2ADataEntryInteractorProtocol: class {
    func getAllCards(isRequestedByButtonClick: Bool)
    func getAllCardBalances()
    func operationPrepare(for userEntries: OperationPageP2ADataEntryUserEntry)
    func accountOwner(for accountNumber: String, branch: String)
}

class OperationPageP2ADataEntryInteractor: OperationPageP2ADataEntryInteractorProtocol {
    weak var presenter: OperationPageP2ADataEntryInteractorToPresenterProtocol!
    
    func getAllCards(isRequestedByButtonClick: Bool) {
        NetworkService.Card.cardsList { [weak self] (result) in
            self?.presenter.didGetAllCards(with: result, isRequestedByButtonClick: isRequestedByButtonClick)
        }
    }
    
    func getAllCardBalances() {
        NetworkService.Card.cardBalance { [weak self] (result) in
            self?.presenter.didGetAllCardBalances(with: result)
        }
    }
    
    func operationPrepare(for userEntries: OperationPageP2ADataEntryUserEntry) {
        var amount = userEntries.amount ?? 0
        amount *= 100
        
        NetworkService.Operation.operationPrepare(prepareType: .p2a,
                                                  sender: nil,
                                                  senderId: userEntries.payer.payerCardId,
                                                  senderBranch: nil,
                                                  receiver: userEntries.receiversAccount,
                                                  receiverId: nil,
                                                  receiverBranch: userEntries.receiversAccountMFO,
                                                  paramNum: nil,
                                                  paramStr: nil,
                                                  paramBool: nil,
                                                  amount: amount) { [weak self] (result) in
            self?.presenter.didOperationPrepare(with: result)
        }
    }
    
    func accountOwner(for accountNumber: String, branch: String) {
        NetworkService.Accounts.accountOwner(accountNumber: accountNumber, branch: branch) { [weak self] (result) in
            self?.presenter.didAccountOwner(with: result)
        }
    }
}
