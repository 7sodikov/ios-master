//
//  OperationPageP2ADataEntryPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageP2ADataEntryPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: OperationPageP2ADataEntryVM { get }
    func viewDidLoad()
    func myCardsClicked()
    
    func didEnter(receiversAccountNumber: String?)
    func didEnter(receiversAccountMFO: String?)
    func didEnter(amount: Double?)
    func nextButtonClicked()
}

protocol OperationPageP2ADataEntryInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool)
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>)
    func didOperationPrepare(with response: ResponseResult<OperationPrepareResponse, AppError>)
    func didAccountOwner(with response: ResponseResult<CardOwnerResponse, AppError>)
}

class OperationPageP2ADataEntryPresenter: OperationPageP2ADataEntryPresenterProtocol {
    weak var view: OperationPageP2ADataEntryVCProtocol!
    var interactor: OperationPageP2ADataEntryInteractorProtocol!
    var router: OperationPageP2ADataEntryRouterProtocol!
    weak var delegate: OperationPageDataEntryDelegate?
    var favoriteItem: FavoriteListOperationsResponse? {
        didSet {
            fillFromFavoriteItem()
        }
    }
    var historyItem: HistoryResponse? {
        didSet {
            fillFromHistoryItem()
        }
    }
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationPageP2ADataEntryVM()
    
    func viewDidLoad() {
        view.nextButton(enable: viewModel.isAllFieldsValid)
        view.cardsDownloadPreloader(show: true)
        downloadMyCardsWithBalances(isRequestedByButtonClick: false)
    }
    
    func myCardsClicked() {
        if viewModel.cards.count > 0 {
            openCardsList()
        } else {
            view.cardsDownloadPreloader(show: true)
            downloadMyCardsWithBalances(isRequestedByButtonClick: true)
        }
    }
    
    func didEnter(receiversAccountNumber: String?) {
        viewModel.userEntry.receiversAccount = receiversAccountNumber
        view.nextButton(enable: viewModel.isAllFieldsValid)
        fetchAccountOwnerIfNeeded()
    }
    
    func didEnter(receiversAccountMFO: String?) {
        viewModel.userEntry.receiversAccountMFO = receiversAccountMFO
        view.nextButton(enable: viewModel.isAllFieldsValid)
        fetchAccountOwnerIfNeeded()
    }
    
    func didEnter(amount: Double?) {
        viewModel.userEntry.amount = amount
        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func nextButtonClicked() {
        view.preloader(show: true)
        interactor.operationPrepare(for: viewModel.userEntry)
    }
    
    // Private property and methods
    private func fetchAccountOwnerIfNeeded() {
        if viewModel.userEntry.receiversAccount?.count == 20 &&
            viewModel.userEntry.receiversAccountMFO?.count == 5 {
            view.receiversAccountPreloader(show: true)
            interactor.accountOwner(for: viewModel.userEntry.receiversAccount ?? "",
                                    branch: viewModel.userEntry.receiversAccountMFO ?? "")
            
        } else {
            view.receiverAccountOwnerName(show: false)
        }
    }
    
    private func downloadMyCardsWithBalances(isRequestedByButtonClick: Bool) {
        interactor.getAllCards(isRequestedByButtonClick: isRequestedByButtonClick)
        interactor.getAllCardBalances()
    }
    
    private func openCardsList() {
        var cardsList: [EZSelectControllerCellModel] = []
        for i in 0..<viewModel.cards.count {
            let cardVM = viewModel.cardVM(at: i)
            let selectionItem = EZSelectControllerCellModel()
            selectionItem.title = cardVM.card.pan
            selectionItem.subTitle = cardVM.balanceStr
            cardsList.append(selectionItem)
        }
        router.openCardSelectionView(from: view, delegate: self)
    }
    
    private func fillFromFavoriteItem() {
        guard let fillerItem = favoriteItem, let senderID = fillerItem.senderID else { return }
        
        viewModel.userEntry.receiversAccount = fillerItem.receiver
        viewModel.userEntry.receiversAccountMFO = fillerItem.receiverBranch
        viewModel.userEntry.amount = Double(fillerItem.amount/100)
        viewModel.userEntry.payer.selectedPayerNumber = fillerItem.sender
        viewModel.userEntry.payer.payerCardId = UInt64(senderID)
        viewModel.receiverAccountOwnerText = fillerItem.receiverOwner
    }
    
    private func fillFromHistoryItem() {
        guard let fillerItem = historyItem else { return }
        
        viewModel.userEntry.receiversAccount = fillerItem.receiver
        viewModel.userEntry.receiversAccountMFO = fillerItem.receiverBranch
        viewModel.userEntry.amount = Double(fillerItem.amount/100)
        viewModel.userEntry.payer.selectedPayerNumber = fillerItem.sender
        viewModel.userEntry.payer.payerCardId = fillerItem.senderId == nil ? nil : UInt64(fillerItem.senderId!)
        viewModel.receiverAccountOwnerText = fillerItem.receiverOwner
    }
}

extension OperationPageP2ADataEntryPresenter: OperationPageP2ADataEntryInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool) {
        switch response {
        case let .success(result):
            viewModel.cards = result.data.cards
            view.cardsDownloadPreloader(show: false)
            if isRequestedByButtonClick {
                openCardsList()
            }
        case let .failure(error):
            view.cardsDownloadPreloader(show: false)
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cardBalances = result.data.cards ?? []
        case .failure(_):
//            view.showError(message: error.localizedDescription)
            break
        }
    }
    
    func didOperationPrepare(with response: ResponseResult<OperationPrepareResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            router.navigateToNextPage(with: result.data, operationPrepareType: .p2a, delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
    
    func didAccountOwner(with response: ResponseResult<CardOwnerResponse, AppError>) {
        view.receiversAccountPreloader(show: false)
        switch response {
        case let .success(result):
            viewModel.receiverAccountOwnerText = result.data.ownerName
            view.receiverAccountOwnerName(show: true)
        case let .failure(error):
            viewModel.receiverAccountOwnerText = error.localizedDescription
            view.receiverAccountOwnerName(show: true)
            break
        }
    }
}

extension OperationPageP2ADataEntryPresenter: EZSelectControllerDelegate {
    func numberOfSections(in ezSelectController: EZSelectController) -> Int {
        return 1
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cards.count
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleAndSubtitleForRowAt indexPath: IndexPath) -> (title: String, subtitle: String) {
        let cardVM = viewModel.cardVM(at: indexPath.row)
        return (cardVM.card.pan ?? "", cardVM.balanceStr)
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, didSelectRowAt indexPath: IndexPath) {
        let cardVM = viewModel.cardVM(at: indexPath.row)
        viewModel.userEntry.set(payer: cardVM.card)
        view.reloadTable()
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleForHeaderInSection section: Int) -> String? {
        return RS.lbl_cards.localized()
    }
}
