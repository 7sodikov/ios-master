//
//  OperationPageP2ADataEntryRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageP2ADataEntryRouterProtocol: class {
    static func createModule(with favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?, delegate: OperationPageDataEntryDelegate?) -> UIViewController
    func navigateToNextPage(with prepareResponse: OperationPrepareResponse, operationPrepareType: OperationPrepareType, delegate: OperationPageDataEntryDelegate?)
    func openCardSelectionView(from controller: Any?,
                               delegate: EZSelectControllerDelegate)
}

class OperationPageP2ADataEntryRouter: OperationPageP2ADataEntryRouterProtocol {
    static func createModule(with favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?, delegate: OperationPageDataEntryDelegate?) -> UIViewController {
        let vc = OperationPageP2ADataEntryVC()
        let presenter = OperationPageP2ADataEntryPresenter()
        let interactor = OperationPageP2ADataEntryInteractor()
        let router = OperationPageP2ADataEntryRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        presenter.favoriteItem = favoriteItem
        presenter.historyItem = historyItem
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToNextPage(with prepareResponse: OperationPrepareResponse, operationPrepareType: OperationPrepareType, delegate: OperationPageDataEntryDelegate?) {
        delegate?.dataEntryPageFinished(with: prepareResponse, operationPrepareType: operationPrepareType)
    }
    
    func openCardSelectionView(from controller: Any?,
                               delegate: EZSelectControllerDelegate) {
        let selectionCtrl = EZSelectController()
        selectionCtrl.delegate = delegate
//        let navCtrl = selectionCtrl.wrapIntoNavigation()
        (controller as? UIViewController)?.present(selectionCtrl, animated: true, completion: nil)
    }
}
