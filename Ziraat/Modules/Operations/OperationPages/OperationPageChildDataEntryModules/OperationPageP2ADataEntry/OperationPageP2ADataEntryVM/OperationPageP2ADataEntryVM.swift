//
//  OperationPageP2ADataEntryVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class OperationPageP2ADataEntryVM: OperationPageParentDataEntryVM {
    let userEntry = OperationPageP2ADataEntryUserEntry()
    var cards: [CardResponse] = []
    var cardBalances: [CardBalanceResponse] = []
    var receiverAccountOwnerText: String?
    
    override init() {
        super.init()
        super.tableCells = [.payerCard, .receiverAccount, .receiverAccountMFO, .amount]
    }
    
    func cardVM(at index: Int) -> DashboardCardVM {
        var balance: CardBalanceResponse?
        let card = cards[index]
        for cardBalance in cardBalances {
            if cardBalance.cardId == card.cardId {
                balance = cardBalance
                break
            }
        }
        
        return DashboardCardVM(card, balance)
    }
    
    var isAllFieldsValid: Bool {
        guard let payerCardId = userEntry.payer.payerCardId else { return false }
        guard let receiversAccount = userEntry.receiversAccount else { return false }
        guard let receiversAccountMFO = userEntry.receiversAccountMFO else { return false }
        guard let amount = userEntry.amount else { return false }
        
        return payerCardId > 0 &&
            receiversAccount.count == 20 &&
            receiversAccountMFO.count == 5 &&
            amount >= 1000
    }
}
