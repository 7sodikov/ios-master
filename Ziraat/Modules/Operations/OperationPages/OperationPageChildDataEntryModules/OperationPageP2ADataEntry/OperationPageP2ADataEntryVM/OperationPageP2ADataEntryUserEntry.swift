//
//  OperationPageP2ADataEntryUserEntry.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class OperationPageP2ADataEntryUserEntry {
    var payer = OperationPagePA2PDataEntryUserEntryPayer() // Card
    var receiversAccount: String?
    var receiversAccountMFO: String?
    var amount: Double?
    
    func set(payer: CardResponse) {
        self.payer.selectedPayerNumber = payer.pan
        self.payer.payerCardId = payer.cardId
    }
}
