//
//  OperationPageP2ADataEntryVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageP2ADataEntryVCProtocol: OperationPageParentDataEntryVCProtocol {
    func cardsDownloadPreloader(show: Bool)
    func receiversAccountPreloader(show: Bool)
    func receiverAccountOwnerName(show: Bool)
}

class OperationPageP2ADataEntryVC: OperationPageParentDataEntryVC, OperationPageP2ADataEntryViewInstaller {
    
    var presenter: OperationPageP2ADataEntryPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
    }
    
    @objc func nextButtonClicked(_ sender: UIButton) {
        presenter.nextButtonClicked()
    }
    
    private func receiverAccountCell() -> OperationPageDataEntryReceiversAccountNumberTableCell? {
        let receiverCardCellIndex = presenter.viewModel.index(for: .receiverAccount)
        if receiverCardCellIndex < 0 {
            return nil
        }
        
        let receiverCardCell = tableView.cellForRow(at: IndexPath(row: receiverCardCellIndex, section: 0)) as? OperationPageDataEntryReceiversAccountNumberTableCell
        return receiverCardCell
    }
}

extension OperationPageP2ADataEntryVC: OperationPageP2ADataEntryVCProtocol {
    func cardsDownloadPreloader(show: Bool) {
        let myCardCellIndex = presenter.viewModel.index(for: .payerCard)
        if myCardCellIndex < 0 {
            return
        }
        
        let myCardCell = tableView.cellForRow(at: IndexPath(row: myCardCellIndex, section: 0)) as? OperationPageDataEntryChooseMyCardTableCell
        myCardCell?.animateIndicator = show
    }
    
    func receiversAccountPreloader(show: Bool) {
        let cell = receiverAccountCell()
        cell?.animateIndicator = show
    }
    
    func receiverAccountOwnerName(show: Bool) {
        let cell = receiverAccountCell()
        cell?.animateIndicator = false
        cell?.subtitleLabel.isHidden = !show
        cell?.subtitleLabel.text = presenter.viewModel.receiverAccountOwnerText
    }
}

extension OperationPageP2ADataEntryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.tableCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = presenter.viewModel.tableCells[indexPath.row]
        switch cellType {
        case .payerCard:
            let cellid = String(describing: OperationPageDataEntryChooseMyCardTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryChooseMyCardTableCell
            cell.backgroundColor = .clear
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.payer.selectedPayerNumber)
        case .receiverAccount:
            let cellid = String(describing: OperationPageDataEntryReceiversAccountNumberTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryReceiversAccountNumberTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.receiversAccount)
        case .receiverAccountMFO:
            let cellid = String(describing: OperationPageDataEntryReceiversAccountMFOTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryReceiversAccountMFOTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.receiversAccountMFO)
        case .amount:
            let cellid = String(describing: OperationPageDataEntryAmountTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryAmountTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.amount)
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(110)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellType = presenter.viewModel.tableCells[indexPath.row]
        if cellType == .payerCard {
            presenter.myCardsClicked()
        }
    }
}

extension OperationPageP2ADataEntryVC: OperationPageDataEntryReceiversAccountNumberTableCellDelegate {
    func didEnter(accountNumber: String?, complete: Bool) {
        presenter.didEnter(receiversAccountNumber: accountNumber)
    }
}

extension OperationPageP2ADataEntryVC: OperationPageDataEntryReceiversAccountMFOTableCellDelegate {
    func didEnter(accountMFO: String?) {
        presenter.didEnter(receiversAccountMFO: accountMFO)
    }
}

extension OperationPageP2ADataEntryVC: OperationPageDataEntryAmountTableCellDelegate {
    func didEnter(amount: Double?) {
        presenter.didEnter(amount: amount)
    }
}
