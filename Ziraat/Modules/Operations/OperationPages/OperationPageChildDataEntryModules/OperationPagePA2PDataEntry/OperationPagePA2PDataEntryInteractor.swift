//
//  OperationPagePA2PDataEntryInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPagePA2PDataEntryInteractorProtocol: class {
    func getAllCards(isRequestedByButtonClick: Bool)
    func getAllCardBalances()
    func accountsList(isRequestedByButtonClick: Bool)
    func accountBalance()
    func operationPrepare(for userEntries: OperationPagePA2PDataEntryUserEntry, prepareType: OperationPrepareType)
    func cardOwner(for pan: String)
}

class OperationPagePA2PDataEntryInteractor: OperationPagePA2PDataEntryInteractorProtocol {
    weak var presenter: OperationPagePA2PDataEntryInteractorToPresenterProtocol!
    
    func getAllCards(isRequestedByButtonClick: Bool) {
        NetworkService.Card.cardsList { [weak self] (result) in
            self?.presenter.didGetAllCards(with: result, isRequestedByButtonClick: isRequestedByButtonClick)
        }
    }
    
    func getAllCardBalances() {
        NetworkService.Card.cardBalance { [weak self] (result) in
            self?.presenter.didGetAllCardBalances(with: result)
        }
    }
    
    func accountsList(isRequestedByButtonClick: Bool) {
        NetworkService.Accounts.accountsList { [weak self] (result) in
            self?.presenter.didAccountsList(with: result, isRequestedByButtonClick: isRequestedByButtonClick)
        }
    }
    
    func accountBalance() {
        NetworkService.Accounts.accountBalance { [weak self] (result) in
            self?.presenter.didAccountBalance(with: result)
        }
    }
    
    func operationPrepare(for userEntries: OperationPagePA2PDataEntryUserEntry, prepareType: OperationPrepareType) {
        var amount = userEntries.amount ?? 0
        amount *= 100
        
        if prepareType == .p2p {
            NetworkService.Operation.operationPrepare(prepareType: .p2p,
                                                      sender: nil,
                                                      senderId: userEntries.payer.payerCardId,
                                                      senderBranch: nil,
                                                      receiver: userEntries.receiversCard,
                                                      receiverId: nil,
                                                      receiverBranch: nil,
                                                      paramNum: nil,
                                                      paramStr: nil,
                                                      paramBool: nil,
                                                      amount: amount) { [weak self] (result) in
                self?.presenter.didOperationPrepare(with: result)
            }
        } else { // .a2p
            NetworkService.Operation.operationPrepare(prepareType: .a2p,
                                                      sender: userEntries.payer.payerAccountNumber, // payerAccount?.number,
                                                      senderId: nil,
                                                      senderBranch: userEntries.payer.payerAccountBranch, //payerAccount?.branch,
                                                      receiver: userEntries.receiversCard,
                                                      receiverId: nil,
                                                      receiverBranch: nil,
                                                      paramNum: nil,
                                                      paramStr: nil,
                                                      paramBool: nil,
                                                      amount: amount) { [weak self] (result) in
                self?.presenter.didOperationPrepare(with: result)
            }
        }
    }
    
    func cardOwner(for pan: String) {
        NetworkService.Card.cardOwner(cardId: nil, pan: pan) { [weak self] (result) in
            self?.presenter.didCardOwner(with: result)
        }
    }
}
