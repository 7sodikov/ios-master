//
//  OperationPagePA2PDataEntryUserEntry.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class OperationPagePA2PDataEntryUserEntryPayer {
    var selectedPayerNumber: String? // Card.pan || AccountResponse.number
    var payerCardId: UInt64?
    var payerCardOwner: String?
    var payerAccountNumber: String?
    var payerAccountBranch: String?
}

class OperationPagePA2PDataEntryUserEntry {
    var payer = OperationPagePA2PDataEntryUserEntryPayer() // Card & AccountResponse
    var receiversCard: String?
    var amount: Double?
    var prepareType: OperationPrepareType?
    
    func set(payer: Any) {
        if payer is CardResponse {
            let card = (payer as? CardResponse)
            self.prepareType = .p2p
            self.payer.selectedPayerNumber = card?.pan
            self.payer.payerCardId = card?.cardId
            
        } else if payer is AccountResponse {
            let account = (payer as? AccountResponse)
            self.prepareType = .a2p
            self.payer.selectedPayerNumber = account?.number
            self.payer.payerAccountNumber = account?.number
            self.payer.payerAccountBranch = account?.branch
        }
    }
}
