//
//  OperationPagePA2PDataEntryVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class OperationPagePA2PDataEntryVM: OperationPageParentDataEntryVM {
    let userEntry = OperationPagePA2PDataEntryUserEntry()
    var cards: [CardResponse] = []
    var cardBalances: [CardBalanceResponse] = []
    var accounts: [AccountResponse] = []
    var accountBalances: [AccountBalanceResponse] = []
    var receiverCardOwnerText: String?
    
    override init() {
        super.init()
        super.tableCells = [.payerCard, .receiverCardNumber, .amount]
    }
    
    func cardVM(at index: Int) -> DashboardCardVM {
        var balance: CardBalanceResponse?
        let card = cards[index]
        for cardBalance in cardBalances {
            if cardBalance.cardId == card.cardId {
                balance = cardBalance
                break
            }
        }
        
        return DashboardCardVM(card, balance)
    }
    
    func accountVM(at index: Int) -> DashboardAccountVM {
        var balance: AccountBalanceResponse?
        let account = accounts[index]
        for accountBalance in accountBalances {
            if accountBalance.number == account.number {
                balance = accountBalance
                break
            }
        }
        
        return DashboardAccountVM(account, balance)
    }
    
    var isAllFieldsValid: Bool {
        var isPayerValid = false
        
        if let prepareType = userEntry.prepareType {
            if prepareType == .p2p {
                let payerCardId = userEntry.payer.payerCardId ?? 0
                isPayerValid = payerCardId > 0
            }
            else if prepareType == .a2p {
                let payerAccountNumber = userEntry.payer.payerAccountNumber ?? ""
                isPayerValid = payerAccountNumber.count > 0
            }
        }
        
        guard let receiversCard = userEntry.receiversCard else { return false }
        guard let amount = userEntry.amount else { return false }
        
        return isPayerValid && receiversCard.count == 16 && amount >= 1000
    }
}
