//
//  OperationPagePA2PDataEntryPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPagePA2PDataEntryPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: OperationPagePA2PDataEntryVM { get }
    func viewDidLoad()
    func myCardsClicked()
    func didEnter(receiversCard: String, complete: Bool)
    func didEnter(amount: Double?)
    func nextButtonClicked()
}

protocol OperationPagePA2PDataEntryInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool)
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>)
    func didAccountsList(with response: ResponseResult<AccountsListResponse, AppError>, isRequestedByButtonClick: Bool)
    func didAccountBalance(with response: ResponseResult<AccountsBalanceResponse, AppError>)
    func didOperationPrepare(with response: ResponseResult<OperationPrepareResponse, AppError>)
    func didCardOwner(with response: ResponseResult<CardOwnerResponse, AppError>)
}

class OperationPagePA2PDataEntryPresenter: OperationPagePA2PDataEntryPresenterProtocol {
    weak var view: OperationPagePA2PDataEntryVCProtocol!
    var interactor: OperationPagePA2PDataEntryInteractorProtocol!
    var router: OperationPagePA2PDataEntryRouterProtocol!
    weak var delegate: OperationPageDataEntryDelegate?
    var favoriteItem: FavoriteListOperationsResponse? {
        didSet {
            fillFromFavoriteItem()
        }
    }
    var historyItem: HistoryResponse? {
        didSet {
            fillFromHistoryItem()
        }
    }
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationPagePA2PDataEntryVM()
    
    func viewDidLoad() {
        view.nextButton(enable: viewModel.isAllFieldsValid)
        view.cardsDownloadPreloader(show: true)
        downloadMyCardsWithBalances(isRequestedByButtonClick: false)
    }
    
    func myCardsClicked() {
        let canOpen = (viewModel.cards.count > 0 && viewModel.cardBalances.count > 0) ||
            (viewModel.accounts.count > 0 && viewModel.accountBalances.count > 0)
        
        if canOpen {
            router.openCardSelectionView(from: view, delegate: self)
        } else {
            view.cardsDownloadPreloader(show: true)
            downloadMyCardsWithBalances(isRequestedByButtonClick: true)
        }
    }
    
    func didEnter(receiversCard: String, complete: Bool) {
        viewModel.userEntry.receiversCard = receiversCard
        view.nextButton(enable: viewModel.isAllFieldsValid)
        if complete {
            view.receiversCardPreloader(show: true)
            interactor.cardOwner(for: receiversCard)
            
        } else {
            view.receiverCardOwnerName(show: false)
        }
    }
    
    func didEnter(amount: Double?) {
        viewModel.userEntry.amount = amount
        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func nextButtonClicked() {
        guard let prepareType = viewModel.userEntry.prepareType else { return }
        view.preloader(show: true)
        interactor.operationPrepare(for: viewModel.userEntry, prepareType: prepareType)
    }
    
    // Private property and methods
    
    private var responsesCount: Int = 0
    private var isRequestedByButtonClick = false
    private var firstCatchedError: AppError?
    
    private func downloadMyCardsWithBalances(isRequestedByButtonClick: Bool) {
        self.isRequestedByButtonClick = isRequestedByButtonClick
        responsesCount = 0
        firstCatchedError = nil
        interactor.getAllCards(isRequestedByButtonClick: isRequestedByButtonClick)
        interactor.getAllCardBalances()
        interactor.accountsList(isRequestedByButtonClick: isRequestedByButtonClick)
        interactor.accountBalance()
    }
    
    private func processResponsesCount(_ error: AppError?) {
        if error != nil {
            if firstCatchedError == nil {
                firstCatchedError = error
            }
        }
        
        if responsesCount == 4 {
            view.cardsDownloadPreloader(show: false)
            if let catchedError = firstCatchedError {
                view.showError(message: catchedError.localizedDescription)
            } else {
                if isRequestedByButtonClick {
                    router.openCardSelectionView(from: view, delegate: self)
                }
            }
        }
    }
    
    private func fillFromFavoriteItem() {
        guard let fillerItem = favoriteItem else { return }
        
        viewModel.userEntry.receiversCard = fillerItem.receiver
        viewModel.userEntry.amount = Double(fillerItem.amount/100)
        viewModel.userEntry.payer.selectedPayerNumber = fillerItem.sender
        viewModel.receiverCardOwnerText = fillerItem.receiverOwner
        
        if fillerItem.operationMode == OperationPrepareType.p2p.rawValue, fillerItem.senderID != nil {
            viewModel.userEntry.prepareType = .p2p
            viewModel.userEntry.payer.payerCardId = UInt64(fillerItem.senderID!)
        }
        else if fillerItem.operationMode == OperationPrepareType.a2p.rawValue {
            viewModel.userEntry.prepareType = .a2p
            viewModel.userEntry.payer.payerAccountNumber = fillerItem.sender
            viewModel.userEntry.payer.payerAccountBranch = fillerItem.senderBranch
        }
    }
    
    private func fillFromHistoryItem() {
        guard let fillerItem = historyItem else { return }
        
        viewModel.userEntry.receiversCard = fillerItem.receiver
        viewModel.userEntry.amount = Double(fillerItem.amount/100)
        viewModel.userEntry.payer.selectedPayerNumber = fillerItem.sender
        viewModel.receiverCardOwnerText = fillerItem.receiverOwner
        
        if fillerItem.operationMode?.rawValue == OperationPrepareType.p2p.rawValue {
            viewModel.userEntry.prepareType = .p2p
            viewModel.userEntry.payer.payerCardId = fillerItem.senderId == nil ? nil : UInt64(fillerItem.senderId!)
        }
        else if fillerItem.operationMode?.rawValue == OperationPrepareType.a2p.rawValue {
            viewModel.userEntry.prepareType = .a2p
            viewModel.userEntry.payer.payerAccountNumber = fillerItem.sender
            viewModel.userEntry.payer.payerAccountBranch = fillerItem.senderBranch
        }
    }
}

extension OperationPagePA2PDataEntryPresenter: OperationPagePA2PDataEntryInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool) {
        responsesCount += 1
        switch response {
        case let .success(result):
            viewModel.cards = result.data.cards
            processResponsesCount(nil)
        case let .failure(error):
            processResponsesCount(error)
        }
    }
    
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>) {
        responsesCount += 1
        switch response {
        case let .success(result):
            viewModel.cardBalances = result.data.cards ?? []
            processResponsesCount(nil)
        case let .failure(error):
            processResponsesCount(error)
        }
    }
    
    func didAccountsList(with response: ResponseResult<AccountsListResponse, AppError>, isRequestedByButtonClick: Bool) {
        responsesCount += 1
        switch response {
        case let .success(result):
            viewModel.accounts = result.data.accounts
            processResponsesCount(nil)
        case let .failure(error):
            processResponsesCount(error)
        }
    }
    
    func didAccountBalance(with response: ResponseResult<AccountsBalanceResponse, AppError>) {
        responsesCount += 1
        switch response {
        case let .success(result):
            viewModel.accountBalances = result.data.accounts
            processResponsesCount(nil)
        case let .failure(error):
            processResponsesCount(error)
            break
        }
    }
    
    func didOperationPrepare(with response: ResponseResult<OperationPrepareResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            guard let prepareType = viewModel.userEntry.prepareType else { return }
            router.navigateToNextPage(with: result.data, operationPrepareType: prepareType, delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
    
    func didCardOwner(with response: ResponseResult<CardOwnerResponse, AppError>) {
        view.receiversCardPreloader(show: false)
        switch response {
        case let .success(result):
            viewModel.receiverCardOwnerText = result.data.ownerName
            view.receiverCardOwnerName(show: true)
        case let .failure(error):
            viewModel.receiverCardOwnerText = error.localizedDescription
            view.receiverCardOwnerName(show: true)
            break
        }
    }
}

extension OperationPagePA2PDataEntryPresenter: EZSelectControllerDelegate {
    func numberOfSections(in ezSelectController: EZSelectController) -> Int {
        return 2
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.cards.count
        } else {
            return viewModel.accounts.count
        }
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleAndSubtitleForRowAt indexPath: IndexPath) -> (title: String, subtitle: String) {
        if indexPath.section == 0 {
            let cardVM = viewModel.cardVM(at: indexPath.row)
            return (cardVM.card.pan ?? "", cardVM.balanceStr)
        } else {
            let accountVM = viewModel.accountVM(at: indexPath.row)
            return (accountVM.account.number, accountVM.balanceStr)
        }
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let cardVM = viewModel.cardVM(at: indexPath.row)
            viewModel.userEntry.set(payer: cardVM.card)
        } else {
            let cardVM = viewModel.accountVM(at: indexPath.row)
            viewModel.userEntry.set(payer: cardVM.account)
        }
        view.reloadTable()
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return RS.lbl_cards.localized()
        } else {
            return RS.lbl_accounts.localized()
        }
    }
}
