//
//  OperationPagePA2PDataEntryVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPagePA2PDataEntryVCProtocol: OperationPageParentDataEntryVCProtocol {
    func cardsDownloadPreloader(show: Bool)
    func receiversCardPreloader(show: Bool)
    func receiverCardOwnerName(show: Bool)
}

class OperationPagePA2PDataEntryVC: OperationPageParentDataEntryVC, OperationPagePA2PDataEntryViewInstaller {
    
    var presenter: OperationPagePA2PDataEntryPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
    }
    
    @objc func nextButtonClicked(_ sender: UIButton) {
        presenter.nextButtonClicked()
    }
    
    private func receiverCardCell() -> OperationPageDataEntryReceiversCardTableCell? {
        let receiverCardCellIndex = presenter.viewModel.index(for: .receiverCardNumber)
        if receiverCardCellIndex < 0 {
            return nil
        }
        
        let receiverCardCell = tableView.cellForRow(at: IndexPath(row: receiverCardCellIndex, section: 0)) as? OperationPageDataEntryReceiversCardTableCell
        return receiverCardCell
    }
}

extension OperationPagePA2PDataEntryVC: OperationPagePA2PDataEntryVCProtocol {
    func cardsDownloadPreloader(show: Bool) {
        let myCardCellIndex = presenter.viewModel.index(for: .payerCard)
        if myCardCellIndex < 0 {
            return
        }
        
        let myCardCell = tableView.cellForRow(at: IndexPath(row: myCardCellIndex, section: 0)) as? OperationPageDataEntryChooseMyCardTableCell
        myCardCell?.animateIndicator = show
    }
    
    func receiversCardPreloader(show: Bool) {
        let cell = receiverCardCell()
        cell?.animateIndicator = show
    }
    
    func receiverCardOwnerName(show: Bool) {
        let cell = receiverCardCell()
        cell?.animateIndicator = false
        cell?.subtitleLabel.isHidden = !show
        cell?.subtitleLabel.text = presenter.viewModel.receiverCardOwnerText
    }
}

extension OperationPagePA2PDataEntryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.tableCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = presenter.viewModel.tableCells[indexPath.row]
        switch cellType {
        case .payerCard:
            let cellid = String(describing: OperationPageDataEntryChooseMyCardTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryChooseMyCardTableCell
            cell.backgroundColor = .clear
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.payer.selectedPayerNumber)
        case .receiverCardNumber:
            let cellid = String(describing: OperationPageDataEntryReceiversCardTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryReceiversCardTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell.setup(with: cellType,
                              value: presenter.viewModel.userEntry.receiversCard,
                              receiverOwnerName: presenter.viewModel.receiverCardOwnerText)
        case .amount:
            let cellid = String(describing: OperationPageDataEntryAmountTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryAmountTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.amount)
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(110)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellType = presenter.viewModel.tableCells[indexPath.row]
        if cellType == .payerCard {
            presenter.myCardsClicked()
        }
    }
}

extension OperationPagePA2PDataEntryVC: OperationPageDataEntryReceiversCardTableCellDelegate {
    func didEnter(receiversCard: String, complete: Bool) {
        presenter.didEnter(receiversCard: receiversCard, complete: complete)
    }
}

extension OperationPagePA2PDataEntryVC: OperationPageDataEntryAmountTableCellDelegate {
    func didEnter(amount: Double?) {
        presenter.didEnter(amount: amount)
    }
}
