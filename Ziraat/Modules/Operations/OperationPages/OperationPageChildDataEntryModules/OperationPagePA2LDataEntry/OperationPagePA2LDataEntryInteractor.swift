//
//  OperationPagePA2LDataEntryInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPagePA2LDataEntryInteractorProtocol: class {
    func getAllCards(isRequestedByButtonClick: Bool)
    func getAllCardBalances()
    func accountsList(isRequestedByButtonClick: Bool)
    func accountBalance()
    func operationPrepare(for userEntries: OperationPagePA2LDataEntryUserEntry, prepareType: OperationPrepareType)
    func loanOwner(for loanId: UInt64, branch: String)
}

class OperationPagePA2LDataEntryInteractor: OperationPagePA2LDataEntryInteractorProtocol {
    weak var presenter: OperationPagePA2LDataEntryInteractorToPresenterProtocol!
    
    func getAllCards(isRequestedByButtonClick: Bool) {
        NetworkService.Card.cardsList { [weak self] (result) in
            self?.presenter.didGetAllCards(with: result, isRequestedByButtonClick: isRequestedByButtonClick)
        }
    }
    
    func getAllCardBalances() {
        NetworkService.Card.cardBalance { [weak self] (result) in
            self?.presenter.didGetAllCardBalances(with: result)
        }
    }
    
    func accountsList(isRequestedByButtonClick: Bool) {
        NetworkService.Accounts.accountsList { [weak self] (result) in
            self?.presenter.didAccountsList(with: result, isRequestedByButtonClick: isRequestedByButtonClick)
        }
    }
    
    func accountBalance() {
        NetworkService.Accounts.accountBalance { [weak self] (result) in
            self?.presenter.didAccountBalance(with: result)
        }
    }
    
    func operationPrepare(for userEntries: OperationPagePA2LDataEntryUserEntry, prepareType: OperationPrepareType) {
        var amount = userEntries.amount ?? 0
        amount *= 100
        
        if prepareType == .p2l {
            NetworkService.Operation.operationPrepare(prepareType: .p2l,
                                                      sender: nil,
                                                      senderId: userEntries.payer.payerCardId,
                                                      senderBranch: nil,
                                                      receiver: nil,
                                                      receiverId: userEntries.loanId,
                                                      receiverBranch: userEntries.receiversAccountMFO,
                                                      paramNum: nil,
                                                      paramStr: nil,
                                                      paramBool: (userEntries.selectedRepaymentTypeIndex == 0),
                                                      amount: amount) { [weak self] (result) in
                self?.presenter.didOperationPrepare(with: result)
            }
        } else { // .a2l
            NetworkService.Operation.operationPrepare(prepareType: .a2l,
                                                      sender: userEntries.payer.payerAccountNumber, // payerAccount?.number,
                                                      senderId: nil,
                                                      senderBranch: userEntries.payer.payerAccountBranch, // payerAccount?.branch,
                                                      receiver: nil,
                                                      receiverId: userEntries.loanId,
                                                      receiverBranch: userEntries.receiversAccountMFO,
                                                      paramNum: nil,
                                                      paramStr: nil,
                                                      paramBool: (userEntries.selectedRepaymentTypeIndex == 0),
                                                      amount: amount) { [weak self] (result) in
                self?.presenter.didOperationPrepare(with: result)
            }
        }
    }
    
    func loanOwner(for loanId: UInt64, branch: String) {
        NetworkService.Loan.loanOwner(loanId: loanId, branch: branch) { [weak self] (result) in
            self?.presenter.didLoanOwner(with: result)
        }
    }
}
