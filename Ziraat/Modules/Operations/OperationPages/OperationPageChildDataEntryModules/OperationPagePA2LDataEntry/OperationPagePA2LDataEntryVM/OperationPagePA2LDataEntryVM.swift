//
//  OperationPagePA2LDataEntryVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum P2LRepaymentType {
    case repaymentBySchedule
    case earlyRepayment
    
    var title: String {
        switch self {
        case .repaymentBySchedule: return RS.lbl_scheduled_repayment.localized()
        case .earlyRepayment: return RS.lbl_early_repayment.localized()
        }
    }
}

class OperationPagePA2LDataEntryVM: OperationPageParentDataEntryVM {
    let userEntry = OperationPagePA2LDataEntryUserEntry()
    var cards: [CardResponse] = []
    var cardBalances: [CardBalanceResponse] = []
    var accounts: [AccountResponse] = []
    var accountBalances: [AccountBalanceResponse] = []
    var receiverLoanOwnerText: String?
    
    let repaymentTypes: [P2LRepaymentType] = [.repaymentBySchedule, .earlyRepayment]
    var repaymentTypesStrings: [String] {
        return repaymentTypes.map { $0.title }
    }
    
    var selectedRepaymentType: P2LRepaymentType {
        return repaymentTypes[userEntry.selectedRepaymentTypeIndex]
    }
    
    override init() {
        super.init()
        super.tableCells = [.payerCard, .loanId, .receiverAccountMFO, .loanPaymentType, .amount]
    }
    
    func cardVM(at index: Int) -> DashboardCardVM {
        var balance: CardBalanceResponse?
        let card = cards[index]
        for cardBalance in cardBalances {
            if cardBalance.cardId == card.cardId {
                balance = cardBalance
                break
            }
        }
        
        return DashboardCardVM(card, balance)
    }
    
    func accountVM(at index: Int) -> DashboardAccountVM {
        var balance: AccountBalanceResponse?
        let account = accounts[index]
        for accountBalance in accountBalances {
            if accountBalance.number == account.number {
                balance = accountBalance
                break
            }
        }
        
        return DashboardAccountVM(account, balance)
    }
    
    var isAllFieldsValid: Bool {
        var isPayerValid = false
        
        if let prepareType = userEntry.prepareType {
            if prepareType == .p2l {
                let payerCardId = userEntry.payer.payerCardId ?? 0
                isPayerValid = payerCardId > 0
            }
            else if prepareType == .a2l {
                let payerAccountNumber = userEntry.payer.payerAccountNumber ?? ""
                isPayerValid = payerAccountNumber.count > 0
            }
        }
        
        guard let loanId = userEntry.loanId else { return false }
        guard let receiversAccountMFO = userEntry.receiversAccountMFO else { return false }
        guard let amount = userEntry.amount else { return false }
        
        return isPayerValid &&
            loanId > 0 &&
            receiversAccountMFO.count == 5 &&
            amount >= 1000 &&
            userEntry.selectedRepaymentTypeIndex > -1
    }
}
