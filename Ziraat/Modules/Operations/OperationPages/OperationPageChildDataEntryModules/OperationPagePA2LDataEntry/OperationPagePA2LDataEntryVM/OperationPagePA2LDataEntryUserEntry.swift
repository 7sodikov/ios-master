//
//  OperationPagePA2LDataEntryUserEntry.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class OperationPagePA2LDataEntryUserEntry {
    var payer = OperationPagePA2PDataEntryUserEntryPayer() // Card & AccountResponse
    var loanId: UInt64?
    var receiversAccountMFO: String?
    var selectedRepaymentTypeIndex: Int = 0
    var amount: Double?
    var prepareType: OperationPrepareType?
    
    func set(payer: Any) {
        if payer is CardResponse {
            let card = (payer as? CardResponse)
            self.prepareType = .p2l
            self.payer.selectedPayerNumber = card?.pan
            self.payer.payerCardId = card?.cardId
            
        } else if payer is AccountResponse {
            let account = (payer as? AccountResponse)
            self.prepareType = .a2l
            self.payer.selectedPayerNumber = account?.number
            self.payer.payerAccountNumber = account?.number
            self.payer.payerAccountBranch = account?.branch
        }
    }
}
