//
//  OperationPagePA2LDataEntryVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPagePA2LDataEntryVCProtocol: OperationPageParentDataEntryVCProtocol {
    func cardsDownloadPreloader(show: Bool)
    func receiversLoanPreloader(show: Bool)
    func receiverLoanOwnerName(show: Bool)
}

class OperationPagePA2LDataEntryVC: OperationPageParentDataEntryVC, OperationPagePA2LDataEntryViewInstaller {
    
    var presenter: OperationPagePA2LDataEntryPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
    }
    
    @objc func nextButtonClicked(_ sender: UIButton) {
        presenter.nextButtonClicked()
    }
    
    private func receiverLoanCell() -> OperationPageDataEntryLoanNumberTableCell? {
        let receiverLoanCellIndex = presenter.viewModel.index(for: .loanId)
        if receiverLoanCellIndex < 0 {
            return nil
        }
        
        let receiverLoanCell = tableView.cellForRow(at: IndexPath(row: receiverLoanCellIndex, section: 0)) as? OperationPageDataEntryLoanNumberTableCell
        return receiverLoanCell
    }
}

extension OperationPagePA2LDataEntryVC: OperationPagePA2LDataEntryVCProtocol {
    func cardsDownloadPreloader(show: Bool) {
        let myCardCellIndex = presenter.viewModel.index(for: .payerCard)
        if myCardCellIndex < 0 {
            return
        }
        
        let myCardCell = tableView.cellForRow(at: IndexPath(row: myCardCellIndex, section: 0)) as? OperationPageDataEntryChooseMyCardTableCell
        myCardCell?.animateIndicator = show
    }
    
    func receiversLoanPreloader(show: Bool) {
        let cell = receiverLoanCell()
        cell?.animateIndicator = show
    }
    
    func receiverLoanOwnerName(show: Bool) {
        let cell = receiverLoanCell()
        cell?.animateIndicator = false
        cell?.subtitleLabel.isHidden = !show
        cell?.subtitleLabel.text = presenter.viewModel.receiverLoanOwnerText
    }
}

extension OperationPagePA2LDataEntryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.tableCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = presenter.viewModel.tableCells[indexPath.row]
        switch cellType {
        case .payerCard:
            let cellid = String(describing: OperationPageDataEntryChooseMyCardTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryChooseMyCardTableCell
            cell.backgroundColor = .clear
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.payer.selectedPayerNumber)
            
        case .loanId:
            let cellid = String(describing: OperationPageDataEntryLoanNumberTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryLoanNumberTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            
            var loanIdStr: String?
            if let loanId = presenter.viewModel.userEntry.loanId {
                loanIdStr = "\(loanId)"
            }
            return cell.setup(with: cellType, value: loanIdStr)
            
        case .receiverAccountMFO:
            let cellid = String(describing: OperationPageDataEntryReceiversAccountMFOTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryReceiversAccountMFOTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.receiversAccountMFO)
            
        case .loanPaymentType:
            let cellid = String(describing: OperationPageDataEntryLoanPaymentTypeTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryLoanPaymentTypeTableCell
            cell.backgroundColor = .clear
            cell.paymentTypeDropdownList.delegate = presenter as? EZDropdownListDelegate
            
            return cell.setup(with: cellType, selectedIndex: presenter.viewModel.userEntry.selectedRepaymentTypeIndex)
            
        case .amount:
            let cellid = String(describing: OperationPageDataEntryAmountTableCell.self)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataEntryAmountTableCell
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell.setup(with: cellType, value: presenter.viewModel.userEntry.amount)
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(110)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellType = presenter.viewModel.tableCells[indexPath.row]
        if cellType == .payerCard {
            presenter.myCardsClicked()
        }
    }
}

extension OperationPagePA2LDataEntryVC: OperationPageDataEntryLoanNumberTableCellDelegate {
    func didEnter(loanId: String?) {
        presenter.didEnter(loanId: loanId)
    }
}

extension OperationPagePA2LDataEntryVC: OperationPageDataEntryReceiversAccountMFOTableCellDelegate {
    func didEnter(accountMFO: String?) {
        presenter.didEnter(receiversAccountMFO: accountMFO)
    }
}

extension OperationPagePA2LDataEntryVC: OperationPageDataEntryAmountTableCellDelegate {
    func didEnter(amount: Double?) {
        presenter.didEnter(amount: amount)
    }
}
