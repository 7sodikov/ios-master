//
//  OperationPagePA2LDataEntryViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPagePA2LDataEntryViewInstaller: ViewInstaller {
    // declare your UI elements here
}

extension OperationPagePA2LDataEntryViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
    }
}

fileprivate struct Size {
    
}
