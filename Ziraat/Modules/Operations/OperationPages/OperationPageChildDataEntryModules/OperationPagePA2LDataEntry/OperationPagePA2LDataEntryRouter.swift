//
//  OperationPagePA2LDataEntryRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPagePA2LDataEntryRouterProtocol: class {
    static func createModule(with favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?, delegate: OperationPageDataEntryDelegate?) -> UIViewController
    func navigateToNextPage(with prepareResponse: OperationPrepareResponse, operationPrepareType: OperationPrepareType, delegate: OperationPageDataEntryDelegate?)
    func openCardSelectionView(from controller: Any?,
                               delegate: EZSelectControllerDelegate)
}

class OperationPagePA2LDataEntryRouter: OperationPagePA2LDataEntryRouterProtocol {
    static func createModule(with favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?, delegate: OperationPageDataEntryDelegate?) -> UIViewController {
        let vc = OperationPagePA2LDataEntryVC()
        let presenter = OperationPagePA2LDataEntryPresenter()
        let interactor = OperationPagePA2LDataEntryInteractor()
        let router = OperationPagePA2LDataEntryRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        presenter.favoriteItem = favoriteItem
        presenter.historyItem = historyItem
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToNextPage(with prepareResponse: OperationPrepareResponse, operationPrepareType: OperationPrepareType, delegate: OperationPageDataEntryDelegate?) {
        delegate?.dataEntryPageFinished(with: prepareResponse, operationPrepareType: operationPrepareType)
    }
    
    func openCardSelectionView(from controller: Any?,
                               delegate: EZSelectControllerDelegate) {
        let selectionCtrl = EZSelectController()
        selectionCtrl.delegate = delegate
//        let navCtrl = selectionCtrl.wrapIntoNavigation()
        (controller as? UIViewController)?.present(selectionCtrl, animated: true, completion: nil)
    }
}
