//
//  OperationPageDataPreviewPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageDataPreviewDelegate: class {
    func dataPreviewPageBackButtonClicked()
    func dataPreviewPageFinished(with prepareResponse: OperationPrepareResponse)
}

protocol OperationPageDataPreviewPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: OperationPageDataPreviewVM { get }
    var operationPrepareType: OperationPrepareType! { get }
    func previousButtonClicked()
    func nextButtonClicked()
    func cellTypeAndValue(for index: Int) -> (cellType: OperationPageDataPreviewTableCellType, value: String)
}

protocol OperationPageDataPreviewInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didOperationTransfer(with response: ResponseResult<SingleMessageResponse, AppError>)
}

class OperationPageDataPreviewPresenter: OperationPageDataPreviewPresenterProtocol {
    weak var view: OperationPageDataPreviewVCProtocol!
    var interactor: OperationPageDataPreviewInteractorProtocol!
    var router: OperationPageDataPreviewRouterProtocol!
    weak var delegate: OperationPageDataPreviewDelegate?
    var prepareResponse: OperationPrepareResponse!
    var operationPrepareType: OperationPrepareType!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationPageDataPreviewVM()
    
    func previousButtonClicked() {
        router.navigateToPreviousPage(with: delegate)
    }
    
    func nextButtonClicked() {
        view.preloader(show: true)
        interactor.operationTransfer(for: prepareResponse.token)
    }
    
    func cellTypeAndValue(for index: Int) -> (cellType: OperationPageDataPreviewTableCellType, value: String) {
        let cellType = viewModel.tableCells(for: operationPrepareType)[index]
        let value = viewModel.value(for: operationPrepareType, cellType: cellType, prepareResponse: prepareResponse)
        return (cellType, value)
    }
    
    // Private property and methods
    
}

extension OperationPageDataPreviewPresenter: OperationPageDataPreviewInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didOperationTransfer(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            router.navigateToNextPage(with: prepareResponse, delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
}
