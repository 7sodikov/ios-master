//
//  OperationPageDataPreviewVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum OperationPageDataPreviewTableCellType {
    case sender
    case senderBranch
    case senederOwner
    case receiver
    case receiverBranch
    case receiverOwner
    case amount
    case commission
    case totalAmount
    
    func title(for operationPrepareType: OperationPrepareType) -> String {
        switch operationPrepareType {
        case .p2p:
            switch self {
            case .senederOwner: return RS.lbl_sender_name.localized()
            case .sender: return RS.lbl_sender_card.localized()
            case .receiverOwner: return RS.lbl_receiver_name.localized()
            case .receiver: return RS.lbl_receiver_card.localized()
            case .amount: return RS.lbl_amount.localized()
            case .commission: return RS.lbl_commission.localized()
            case .totalAmount: return RS.lbl_total_sum.localized()
            default:
                return ""
            }
            
        case .p2a:
            switch self {
            case .senederOwner: return RS.lbl_sender_name.localized()
            case .sender: return RS.lbl_sender_card.localized()
            case .receiverOwner: return RS.lbl_receiver_name.localized()
            case .receiver: return RS.lbl_receiver_account.localized()
            case .amount: return RS.lbl_amount.localized()
            case .commission: return RS.lbl_commission.localized()
            case .totalAmount: return RS.lbl_total_sum.localized()
            default:
                return ""
            }
            
        case .p2l:
            switch self {
            case .senederOwner: return RS.lbl_sender_name.localized()
            case .sender: return RS.lbl_sender_card.localized()
            case .receiverOwner: return RS.lbl_receiver_name.localized()
            case .receiver: return RS.lbl_loan_id.localized()
            case .amount: return RS.lbl_amount.localized()
            case .commission: return RS.lbl_commission.localized()
            case .totalAmount: return RS.lbl_total_sum.localized()
            default: return ""
            }
            
        case .a2p:
            switch self {
            case .senederOwner: return RS.lbl_sender_name.localized()
            case .sender: return RS.lbl_sender_account.localized()
            case .receiverOwner: return RS.lbl_receiver_name.localized()
            case .receiver: return RS.lbl_receiver_card.localized()
            case .amount: return RS.lbl_amount.localized()
            case .commission: return RS.lbl_commission.localized()
            case .totalAmount: return RS.lbl_total_sum.localized()
            default:
                return ""
            }
            
        case .a2l:
            switch self {
            case .senederOwner: return RS.lbl_sender_name.localized()
            case .sender: return RS.lbl_sender_account.localized()
            case .receiverOwner: return RS.lbl_receiver_name.localized()
            case .receiver: return RS.lbl_loan_id.localized()
            case .amount: return RS.lbl_amount.localized()
            case .commission: return RS.lbl_commission.localized()
            case .totalAmount: return RS.lbl_total_sum.localized()
            default:
                return ""
            }
        default:
            return ""
        }
    }
}

class OperationPageDataPreviewVM {
    
    func tableCells(for operationPrepareType: OperationPrepareType) -> [OperationPageDataPreviewTableCellType] {
        switch operationPrepareType {
        case .p2p, .p2a, .p2l, .a2p, .a2l:
            return [.senederOwner, .sender, .receiverOwner, .receiver, .amount, .commission, .totalAmount]
        default:
            return []
        }
    }
    
    func value(for operationPrepareType: OperationPrepareType,
               cellType: OperationPageDataPreviewTableCellType,
               prepareResponse: OperationPrepareResponse) -> String {
        switch operationPrepareType {
        case .p2p, .p2a, .p2l, .a2p, .a2l:
            switch cellType {
            case .senederOwner:
                return prepareResponse.senderOwner ?? ""
            case .sender:
                return prepareResponse.sender ?? ""
            case .receiverOwner:
                return prepareResponse.receiverOwner ?? ""
            case .receiver:
                return prepareResponse.receiver ?? ""
            case .amount:
                let amount = prepareResponse.amount / 100
                return amount.currencyFormattedStr() ?? ""
            case .commission:
                let commission = prepareResponse.commission / 100
                return commission.currencyFormattedStr() ?? ""
            case .totalAmount:
                let total = prepareResponse.total / 100
                return total.currencyFormattedStr() ?? ""
            default:
                return ""
            }
        default:
            return ""
        }
    }
}
