//
//  OperationPageDataPreviewTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class OperationPageDataPreviewTableCell: UITableViewCell, OperationPageDataPreviewTableCellInstaller {
    var mainView: UIView { contentView }
    var titleLabel: UILabel!
    var valueLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with cellType: OperationPageDataPreviewTableCellType, operationPrepareType: OperationPrepareType, value: String) -> OperationPageDataPreviewTableCell {
        titleLabel.text = cellType.title(for: operationPrepareType)
        valueLabel.text = value
        return self
    }
}
