//
//  OperationPageDataPreviewTableCellInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol OperationPageDataPreviewTableCellInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var valueLabel: UILabel! { get set }
}

extension OperationPageDataPreviewTableCellInstaller {
    func initSubviews() {
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(14))
        titleLabel.textColor = ColorConstants.lightGray
        titleLabel.textAlignment = .center
        
        valueLabel = UILabel()
        valueLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(16.5))
        valueLabel.textColor = .black
        valueLabel.numberOfLines = 2
        valueLabel.textAlignment = .center
    }
    
    func embedSubviews() {
        mainView.addSubview(titleLabel)
        mainView.addSubview(valueLabel)
    }
    
    func addSubviewsConstraints() {
        titleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(10))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
        }
        
        valueLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(5))
            maker.left.right.equalTo(titleLabel)
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(10)
    struct TextField {
        static let height = Adaptive.val(54)
    }
}
