//
//  OperationPageDataPreviewVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageDataPreviewVCProtocol: BaseViewControllerProtocol {
    
}

class OperationPageDataPreviewVC: BaseViewController, OperationPageDataPreviewViewInstaller {
    var mainView: UIView { view }
    var tableView: UITableView!
    var previousButton: NextButton!
    var nextButton: NextButton!
    var buttonsStackView: UIStackView!
    var presenter: OperationPageDataPreviewPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        
        previousButton.addTarget(self, action: #selector(previousButtonClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextButtonClicked(_:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
        previousButton.isEnabled = !show
    }
    
    @objc private func previousButtonClicked(_ sender: UIButton) {
        presenter.previousButtonClicked()
    }
    
    @objc private func nextButtonClicked(_ sender: UIButton) {
        presenter.nextButtonClicked()
    }
}

extension OperationPageDataPreviewVC: OperationPageDataPreviewVCProtocol {
    
}

extension OperationPageDataPreviewVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.tableCells(for: presenter.operationPrepareType).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = String(describing: OperationPageDataPreviewTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! OperationPageDataPreviewTableCell
        cell.backgroundColor = .clear
        let cellData = presenter.cellTypeAndValue(for: indexPath.row)
        return cell.setup(with: cellData.cellType, operationPrepareType: presenter.operationPrepareType, value: cellData.value)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(70)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
