//
//  OperationPageDataPreviewViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageDataPreviewViewInstaller: ViewInstaller {
    var tableView: UITableView! { get set }
    var previousButton: NextButton! { get set }
    var nextButton: NextButton! { get set }
    var buttonsStackView: UIStackView! { get set }
}

extension OperationPageDataPreviewViewInstaller {
    func initSubviews() {
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.register(OperationPageDataPreviewTableCell.self, forCellReuseIdentifier: String(describing: OperationPageDataPreviewTableCell.self))
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        previousButton = NextButton.systemButton(with: RS.btn_back.localized(), cornerRadius: Size.NextButton.height/2)
        previousButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        previousButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        previousButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: Size.NextButton.height/2)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        nextButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        buttonsStackView = UIStackView()
        buttonsStackView.spacing = Adaptive.val(17)
        buttonsStackView.alignment = .fill
        buttonsStackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(tableView)
        mainView.addSubview(buttonsStackView)
        
        buttonsStackView.addArrangedSubview(previousButton)
        buttonsStackView.addArrangedSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        tableView.snp.remakeConstraints { (maker) in
            maker.top.left.right.equalToSuperview()
            maker.bottom.equalTo(nextButton.snp.top).offset(Adaptive.val(50))
        }
        
        previousButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.NextButton.height)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(previousButton)
        }
        
        buttonsStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(ApplicationSize.paddingLeftRight)
            maker.trailing.equalTo(-ApplicationSize.paddingLeftRight)
            maker.bottom.equalTo(-Adaptive.val(50))
        }
    }
}

fileprivate struct Size {
    struct NextButton {
        static let height = Adaptive.val(54)
    }
}
