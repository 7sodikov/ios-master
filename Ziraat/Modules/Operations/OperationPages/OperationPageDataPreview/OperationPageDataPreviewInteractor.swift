//
//  OperationPageDataPreviewInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageDataPreviewInteractorProtocol: class {
    func operationTransfer(for token: String)
}

class OperationPageDataPreviewInteractor: OperationPageDataPreviewInteractorProtocol {
    weak var presenter: OperationPageDataPreviewInteractorToPresenterProtocol!
    
    func operationTransfer(for token: String) {
        NetworkService.Operation.operationTransfer(token: token) { [weak self] (result) in
            self?.presenter.didOperationTransfer(with: result)
        }
    }
}
