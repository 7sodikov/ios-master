//
//  OperationPageDataPreviewRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageDataPreviewRouterProtocol: class {
    static func createModule(with delegate: OperationsPresenter?) -> UIViewController
    func navigateToPreviousPage(with delegate: OperationPageDataPreviewDelegate?)
    func navigateToNextPage(with prepareResponse: OperationPrepareResponse, delegate: OperationPageDataPreviewDelegate?)
}

class OperationPageDataPreviewRouter: OperationPageDataPreviewRouterProtocol {
    static func createModule(with delegate: OperationsPresenter?) -> UIViewController {
        let vc = OperationPageDataPreviewVC()
        let presenter = OperationPageDataPreviewPresenter()
        let interactor = OperationPageDataPreviewInteractor()
        let router = OperationPageDataPreviewRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPreviousPage(with delegate: OperationPageDataPreviewDelegate?) {
        delegate?.dataPreviewPageBackButtonClicked()
    }
    
    func navigateToNextPage(with prepareResponse: OperationPrepareResponse, delegate: OperationPageDataPreviewDelegate?) {
        delegate?.dataPreviewPageFinished(with: prepareResponse)
    }
}
