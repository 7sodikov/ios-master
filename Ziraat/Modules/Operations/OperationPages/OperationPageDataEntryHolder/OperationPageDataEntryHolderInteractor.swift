//
//  OperationPageDataEntryHolderInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageDataEntryHolderInteractorProtocol: class {
    
}

class OperationPageDataEntryHolderInteractor: OperationPageDataEntryHolderInteractorProtocol {
    weak var presenter: OperationPageDataEntryHolderInteractorToPresenterProtocol!
    
}
