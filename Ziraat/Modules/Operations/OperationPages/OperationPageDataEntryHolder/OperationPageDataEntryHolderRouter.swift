//
//  OperationPageDataEntryHolderRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageDataEntryHolderRouterProtocol: class {
    static func createModule(with favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?, delegate: OperationPageDataEntryDelegate?) -> UIViewController
}

class OperationPageDataEntryHolderRouter: OperationPageDataEntryHolderRouterProtocol {
    static func createModule(with favoriteItem: FavoriteListOperationsResponse?, historyItem: HistoryResponse?, delegate: OperationPageDataEntryDelegate?) -> UIViewController {
        let vc = OperationPageDataEntryHolderVC()
        let presenter = OperationPageDataEntryHolderPresenter()
        let interactor = OperationPageDataEntryHolderInteractor()
        let router = OperationPageDataEntryHolderRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        presenter.favoriteItem = favoriteItem
        presenter.historyItem = historyItem
        interactor.presenter = presenter
        
        return vc
    }
    
}
