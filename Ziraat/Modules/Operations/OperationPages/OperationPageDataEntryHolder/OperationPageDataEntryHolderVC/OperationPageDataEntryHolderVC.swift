//
//  OperationPageDataEntryHolderVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageDataEntryHolderVCProtocol: class {
    func setPageControl(index: Int)
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection)
}

class OperationPageDataEntryHolderVC: UIViewController, OperationPageDataEntryHolderViewInstaller {
    var mainView: UIView { view }
    var segmentedControl: MoneyTransferSegmentedControl!
    var pageControllerCoverView: UIView!
    var pageController: PageController!
    
    var presenter: OperationPageDataEntryHolderPresenterProtocol!
    
    var controllers: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        segmentedControl.items = [RS.lbl_universal_to_card.localized(), RS.lbl_universal_to_account.localized(), RS.lbl_universal_loan_repayment.localized()]
        segmentedControl.delegate = presenter as? MoneyTransferSegmentedControlDelegate
        
        controllers = [OperationPagePA2PDataEntryRouter.createModule(with: presenter.getFavoriteItem(for: [.p2p, .a2p]),
                                                                     historyItem: presenter.getHistoryItem(for: [.p2p, .a2p]),
                                                                     delegate: presenter.delegate),
                       
                       OperationPageP2ADataEntryRouter.createModule(with: presenter.getFavoriteItem(for: [.p2a]),
                                                                    historyItem: presenter.getHistoryItem(for: [.p2a]),
                                                                    delegate: presenter.delegate),
                       
                       OperationPagePA2LDataEntryRouter.createModule(with: presenter.getFavoriteItem(for: [.p2l, .a2l]),
                                                                     historyItem: presenter.getHistoryItem(for: [.p2l, .a2l]),
                                                                     delegate: presenter.delegate)]
        
        pageController.open(in: self,
                            parentView: pageControllerCoverView,
                            pages: controllers, isSwipeEnabled: true)
        pageController.pageControllerDelegate = presenter as? PageControllerDelegate
        
        presenter.viewDidLoad()
    }
}

extension OperationPageDataEntryHolderVC: OperationPageDataEntryHolderVCProtocol {
    func setPageControl(index: Int) {
        segmentedControl.selectedIndex = index
    }
    
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection) {
        pageController.scroll(to: pageIndex, direction: direction)
    }
}
