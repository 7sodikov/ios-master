//
//  OperationPageDataEntryHolderViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationPageDataEntryHolderViewInstaller: ViewInstaller {
    var segmentedControl: MoneyTransferSegmentedControl! { get set }
    var pageControllerCoverView: UIView! { get set }
    var pageController: PageController! { get set }
}

extension OperationPageDataEntryHolderViewInstaller {
    func initSubviews() {
        segmentedControl = MoneyTransferSegmentedControl()
        
        pageControllerCoverView = UIView()
        
        pageController = PageController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    func embedSubviews() {
        mainView.addSubview(segmentedControl)
        mainView.addSubview(pageControllerCoverView)
    }
    
    func addSubviewsConstraints() {
        segmentedControl.snp.remakeConstraints { (maker) in
            maker.top.left.right.equalToSuperview()
            maker.height.equalTo(MoneyTransferSegmentedControlSize.Cell.size.height)
        }
        
        pageControllerCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(segmentedControl.snp.bottom)
            maker.leading.bottom.trailing.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
