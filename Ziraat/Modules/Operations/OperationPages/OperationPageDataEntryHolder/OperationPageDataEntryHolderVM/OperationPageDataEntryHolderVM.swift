//
//  OperationPageDataEntryHolderVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

enum OperationPrepareType: String {
    case p2p
    case p2a
    case p2l
    case a2p
    case a2l
    case pca
    case acp
    
//    var title: String {
//        switch self {
//        case .p2p: return RS.nav_ttl_account_transfer.localized()
//        case .p2a: return RS.nav_ttl_transfer_plastic_to_account.localized()
//        case .p2l: return RS.nav_ttl_transfer_plastic_to_loan.localized()
//        case .a2p: return RS.lbl_account_to_card.localized()
//        case .a2l: return RS.lbl_loan_repayment.localized()
//        default: return ""
//        }
//    }
//    
//    var iconName: String {
//        switch self {
//        case .p2p: return "icon_transfer_type_ptop"
//        case .p2a: return "icon_transfer_type_ptop"
//        case .p2l: return "icon_transfer_type_ptop"
//        case .a2p: return "icon_transfer_type_ptop"
//        case .a2l: return "icon_transfer_type_ptop"
//        default: return ""
//        }
//    }
}

class OperationPageDataEntryHolderVM {
    let userEntry = OperationPageDataEntryHolderUserEntry()
}
