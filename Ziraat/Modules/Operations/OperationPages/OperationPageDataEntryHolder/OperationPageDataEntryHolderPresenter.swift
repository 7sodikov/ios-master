//
//  OperationPageDataEntryHolderPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationPageDataEntryHolderPresenterProtocol: class {
    // VIEW -> PRESENTER
    var delegate: OperationPageDataEntryDelegate? { get }
    var favoriteItem: FavoriteListOperationsResponse? { get }
    var historyItem: HistoryResponse? { get }
    var viewModel: OperationPageDataEntryHolderVM { get }
    func viewDidLoad()
    func getFavoriteItem(for prepareTypes: [OperationPrepareType]) -> FavoriteListOperationsResponse?
    func getHistoryItem(for prepareTypes: [OperationPrepareType]) -> HistoryResponse?
}

protocol OperationPageDataEntryHolderInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class OperationPageDataEntryHolderPresenter: OperationPageDataEntryHolderPresenterProtocol {
    weak var view: OperationPageDataEntryHolderVCProtocol!
    var interactor: OperationPageDataEntryHolderInteractorProtocol!
    var router: OperationPageDataEntryHolderRouterProtocol!
    weak var delegate: OperationPageDataEntryDelegate?
    var favoriteItem: FavoriteListOperationsResponse?
    var historyItem: HistoryResponse?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = OperationPageDataEntryHolderVM()
    
    func viewDidLoad() {
        setNeededPage()
    }
    
    func getFavoriteItem(for prepareTypes: [OperationPrepareType]) -> FavoriteListOperationsResponse? {
        guard let favoriteItem = favoriteItem else { return nil }
        
        for prepareType in prepareTypes {
            if favoriteItem.operationMode == prepareType.rawValue {
                return favoriteItem
            }
        }
        
        return nil
    }
    
    func getHistoryItem(for prepareTypes: [OperationPrepareType]) -> HistoryResponse? {
        guard let historyItem = historyItem else { return nil }
        
        for prepareType in prepareTypes {
            if historyItem.operationMode?.rawValue == prepareType.rawValue {
                return historyItem
            }
        }
        
        return nil
    }
    
    // Private property and methods
    private func setNeededPage() {
        if let favoriteItem = favoriteItem {
            setPage(for: favoriteItem.operationMode)
        } else if let historyItem = historyItem {
            setPage(for: historyItem.operationMode?.rawValue ?? "")
        }
    }
    
    private func setPage(for operationMode: String) {
        var pageIndex = 0
        if operationMode == OperationPrepareType.p2p.rawValue ||
            operationMode == OperationPrepareType.a2p.rawValue {
            pageIndex = 0
        }
        else if operationMode == OperationPrepareType.p2a.rawValue {
            pageIndex = 1
        }
        else if operationMode == OperationPrepareType.p2l.rawValue ||
                    operationMode == OperationPrepareType.a2l.rawValue {
            pageIndex = 2
        }
        
        viewModel.userEntry.currentPageIndex = pageIndex
        view.setPageControl(index: pageIndex)
        if pageIndex > 0 {
            view.scroll(to: pageIndex, direction: .forward)
        }
    }
}

extension OperationPageDataEntryHolderPresenter: OperationPageDataEntryHolderInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}

extension OperationPageDataEntryHolderPresenter: PageControllerDelegate {
    func pageController(didScrollToOffsetX offsetX: CGFloat, percent: CGFloat) {
        
    }
    
    func pageController(didMoveToIndex index: Int) {
        viewModel.userEntry.currentPageIndex = index
        view.setPageControl(index: index)
    }
}

extension OperationPageDataEntryHolderPresenter: MoneyTransferSegmentedControlDelegate {
    func moneyTransferSegmentedControl(_ segmentedControl: MoneyTransferSegmentedControl, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == viewModel.userEntry.currentPageIndex {
            return
        }
        
        let direction: UIPageViewController.NavigationDirection = indexPath.item > viewModel.userEntry.currentPageIndex ? .forward : .reverse
        view.scroll(to: indexPath.item, direction: direction)
        viewModel.userEntry.currentPageIndex = indexPath.item
    }
}
