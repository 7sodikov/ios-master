//
//  OperationsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationsVCProtocol: BaseViewControllerProtocol {
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection)
    func getController(for pageIndex: Int) -> Any?
    func closeButtonClicked()
}

class OperationsVC: BaseViewController, OperationsViewInstaller {
    var mainView: UIView { view }
    var bgImageView: UIImageView!
    var pageControl: P2PTransactionPageControl!
    var pageControllerCoverView: UIView!
    var pageController: PageController!
    
    var presenter: OperationsPresenterProtocol!
    
    var controllers: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        title = RS.lbl_money_transfer.localized()
        
        pageControl.addItem(itemModel: (pageNumber: "1", title: RS.lbl_data_input.localized()))
        pageControl.addItem(itemModel: (pageNumber: "2", title: RS.lbl_preview.localized()))
        pageControl.addItem(itemModel: (pageNumber: "3", title: RS.lbl_sms_confirm.localized()))
        pageControl.addItem(itemModel: (pageNumber: "4", title: RS.lbl_conclusion.localized()))
        
        controllers = [OperationPageDataEntryHolderRouter.createModule(with: presenter.favoriteItem, historyItem: presenter.historyItem, delegate: presenter as? OperationPageDataEntryDelegate),
                       OperationPageDataPreviewRouter.createModule(with: presenter as? OperationsPresenter),
                       OperationPageSMSConfirmationRouter.createModule(with: presenter as? OperationsPresenter),
                       OperationPageCompletionRouter.createModule(with: presenter as? OperationsPresenter)]
        
        pageController.open(in: self,
                            parentView: pageControllerCoverView,
                            pages: controllers, isSwipeEnabled: false)
        pageController.pageControllerDelegate = self
    }
}

extension OperationsVC: OperationsVCProtocol {
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection) {
        pageControl.currentItem = pageIndex
        pageController.scroll(to: pageIndex, direction: direction)
    }
    
    func getController(for pageIndex: Int) -> Any? {
        if pageIndex >= controllers.count {
            return nil
        }
        return controllers[pageIndex]
    }
    
    func closeButtonClicked() {
        navigationController?.popViewController(animated: true)
    }
}

extension OperationsVC: PageControllerDelegate {
    func pageController(didScrollToOffsetX offsetX: CGFloat, percent: CGFloat) {
        
    }
    
    func pageController(didMoveToIndex index: Int) {
        pageControl.currentItem = index
    }
}
