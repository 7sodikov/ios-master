//
//  OperationsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol OperationsViewInstaller: ViewInstaller {
    var bgImageView: UIImageView! { get set }
    var pageControl: P2PTransactionPageControl! { get set }
    var pageControllerCoverView: UIView! { get set }
    var pageController: PageController! { get set }
}

extension OperationsViewInstaller {
    func initSubviews() {
        bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "img_dash_light_background")
        
        pageControl = P2PTransactionPageControl()
        
        pageControllerCoverView = UIView()
        
        pageController = PageController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    func embedSubviews() {
        mainView.addSubview(bgImageView)
        mainView.addSubview(pageControl)
        mainView.addSubview(pageControllerCoverView)
    }
    
    func addSubviewsConstraints() {
        bgImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        pageControl.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(80))
            maker.left.equalTo(Size.leftRightPadding)
            maker.right.equalTo(-Size.leftRightPadding)
            maker.height.equalTo(2 * P2PTransactionPageControlSize.Cell.height + P2PTransactionPageControlSize.interCellSpace)
        }
        
        pageControllerCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(pageControl.snp.bottom).offset(Adaptive.val(24))
            maker.leading.trailing.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    static let leftRightPadding = Adaptive.val(20)
}
