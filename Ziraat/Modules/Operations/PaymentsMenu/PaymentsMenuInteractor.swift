//
//  PaymentsMenuInteractor.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/30/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentsMenuBusinessLogic: PaymentsMenuBase {
    var viewModels: [DisclosureViewModel] {get}
}

class PaymentsMenuInteractor: PaymentsMenuBusinessLogic {
    
    var viewModels: [DisclosureViewModel] {
        Row.allCases.map {
            .init(iconName: $0.icon, title: $0.title, showsSeparatorTopLine: $0.showSeparator.top, showsSeparatorBottomLine: $0.showSeparator.bottom)
        }
    }
}
