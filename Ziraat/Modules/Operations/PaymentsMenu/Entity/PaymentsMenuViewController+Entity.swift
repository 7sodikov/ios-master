//
//  PaymentsMenuViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentsMenuBase {
//    typealias Section = PaymentsMenuViewController.Section
    typealias Row = PaymentsMenuViewController.Row
}


extension PaymentsMenuViewController {
    
    enum Row: Int, CaseIterable {
        case payments
        case autoPayments
        
        var icon: String{
            switch self {
            case .payments:
                return "img_payment"
            case .autoPayments:
                return "img_menu_cards"
            }
        }
        
        var title: String {
            switch self {
            case .payments:
                return RS.lbl_payments.localized()
            case .autoPayments:
                return RS.lbl_automatic_payment.localized()
            }
        }
        
        var showSeparator: (top:Bool, bottom: Bool) {
            switch self {
            case .payments:
                return (true, true)
            case .autoPayments:
                return (false, true)
            }
        }
    }
    
}
