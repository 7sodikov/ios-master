//
//  PaymentsMenuViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/30/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentsMenuViewInstaller: ViewInstaller {
    var tableView: UITableView! {get set}
}

extension PaymentsMenuViewInstaller {
    
    func initSubviews() {
        tableView = UITableView()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.tableFooterView = .init()
//        tableView.allowsSelection = false
    }
    
    func embedSubviews() {
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        tableView.constraint { (make) in
            make.top(Adaptive.val(16)).horizontal.bottom.equalToSuperView()
        }
    }
    
}
