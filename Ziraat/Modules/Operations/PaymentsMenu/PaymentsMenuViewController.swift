//
//  PaymentsMenuViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/30/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol PaymentsMenuDisplayLogic: PaymentsMenuViewInstaller {
    
}


class PaymentsMenuViewController: BaseViewController, PaymentsMenuDisplayLogic  {
    
    var tableView: UITableView!
    var mainView: UIView { self.view }
    var presenter: PaymentsMenuPresentationLogic!
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        navigationController?.transparentBackgorund()
        title = RS.lbl_payments.localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        setupTargets()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        presenter.viewWillAppear(animated)
    }
    
    override func setupTargets() {
        super.setupTargets()
    
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(DisclosureCell.self)
        tableView.register(TableViewCell.self)
    }
    
    
}

extension PaymentsMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.tablePresenter(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presenter.tablePresenter(tableView, viewModelAtIndexPath: indexPath)
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        cell.selectionStyle = .none
        cell.set(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        presenter.tablePresenter(tableView, didSelectRowAt: indexPath)
    }
}


