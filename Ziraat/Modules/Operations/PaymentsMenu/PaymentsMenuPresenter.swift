//
//  PaymentsMenuPresenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/30/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol PaymentsMenuPresentationLogic: PresentationProtocol, PaymentsMenuBase, TablePresentationLogic {
    
}


class PaymentsMenuPresenter: PaymentsMenuPresentationLogic {
    
    private let view: PaymentsMenuDisplayLogic
    private let interactor: PaymentsMenuBusinessLogic
    private let router: PaymentsMenuRoutingLogic
    
    private var controller: PaymentsMenuViewController { view as! PaymentsMenuViewController }
    
    
    init(view: PaymentsMenuDisplayLogic, interactor: PaymentsMenuBusinessLogic, router: PaymentsMenuRoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
     
        view.setupSubviews()
    }
    
    func viewWillAppear(_ animated: Bool) {
        
    }
}


// MARK: -TablePresentationLogic

extension PaymentsMenuPresenter {
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        interactor.viewModels.count
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        interactor.viewModels[indexPath.row]
    }
    
    func tablePresenter(_ table: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let row = Row(rawValue: indexPath.row) else { return }
        switch row {
        case .payments:
            router.openPayments()
        case .autoPayments:
            router.openAutoPayments()
        }
    }
    
}
