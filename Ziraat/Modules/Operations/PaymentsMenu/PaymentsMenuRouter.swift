//
//  PaymentsMenuRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/30/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentsMenuRoutingLogic {
    func openPayments()
    func openAutoPayments()
}


class PaymentsMenuRouter: BaseRouter, PaymentsMenuRoutingLogic {
    
    
    init() {
        let controller = PaymentsMenuViewController()
        super.init(viewController: controller)
        
        let interactor = PaymentsMenuInteractor()
        let presenter = PaymentsMenuPresenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    func openPayments() {
        let router = NewPaymentsRouter.createModule(with: nil)
        navigationController?.pushViewController(router, animated: true)
    }
    
    func openAutoPayments() {
        let vc = AutoPaymentsRouter().viewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
