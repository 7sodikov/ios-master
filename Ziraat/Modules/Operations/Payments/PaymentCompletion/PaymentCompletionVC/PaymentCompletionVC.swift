//
//  PaymentCompletionVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentCompletionVCProtocol: BaseViewControllerProtocol {
    func showFavoriteAddedAlert()
}

class PaymentCompletionVC: BaseViewController, PaymentCompletionViewInstaller {
    var mainView: UIView { view }
    var backView: UIImageView!
    var successLabelStackView: UIStackView!
    var successCheckMarkImageView: UIImageView!
    var successLabel: UILabel!
    var buttonsVerticalStackView: UIStackView!
    var buttonsStackView: UIStackView!
    var titleLabel: UILabel!
    var addToFavoriteButton: NextButton!
    var printButton: NextButton!
    var closeButton: NextButton!
    
    var presenter: PaymentCompletionPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        setupNavigation()
//        navigationItem.leftBarButtonItem = nil
//        navigationItem.hidesBackButton = true
        
        titleLabel.text = presenter.viewModel.service.title
        
//        payerCardCell.setup(title: RS.lbl_card_num.localized(), value: presenter.viewModel.payerCard.selectedPayerNumber ?? "")
//        payerCardOwnerCell.setup(title: RS.lbl_sender.localized(), value: presenter.viewModel.payerCard.payerCardOwner ?? "")
        
//        for fieldValue in presenter.viewModel.paymentPrepareResponse.fieldValues {
//            let cell = PaymentDataPreviewFormCell(frame: .zero)
//            let fieldType = ServiceFieldType.getType(for: fieldValue.type)
//            if fieldType == .money {
//                let doubleValue = Double(fieldValue.value) ?? 0
//                let formattedValue = (doubleValue/100).currencyFormattedStr() ?? ""
//                cell.setup(title: fieldValue.title, value: formattedValue)
//            } else {
//                cell.setup(title: fieldValue.title, value: fieldValue.value)
//            }
//            formStackView.addArrangedSubview(cell)
//        }
//
//        let totalAmountStr = (Double(presenter.viewModel.paymentPrepareResponse.total)/100).currencyFormattedStr() ?? ""
//        totalAmountLabel.text = totalAmountStr
        
        addToFavoriteButton.addTarget(self, action: #selector(addToFavoriteBtnClicked(_:)), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeBtnClicked(_:)), for: .touchUpInside)
        printButton.addTarget(self, action: #selector(printButtonClicked(_:)), for: .touchUpInside)
    }
    
    func setupNavigation() {
        self.title = presenter.viewModel.service.title
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func addToFavoriteBtnClicked(_ sender: NextButton) {
        presenter.addToFavoriteButtonClicked()
    }
    
    @objc private func printButtonClicked(_ sender: NextButton) {
        presenter.printButtonClicked()
    }
    
    @objc private func closeBtnClicked(_ sender: NextButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.navigationController?.view.backgroundColor = UIColor.clear
        }) { (isCompleted) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
}

extension PaymentCompletionVC: PaymentCompletionVCProtocol {
    func showFavoriteAddedAlert() {
        let alert = UIAlertController(title: RS.lbl_success.localized(),
                                      message: RS.lbl_fav_added.localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: RS.btn_yes.localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
