//
//  PaymentCompletionViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentCompletionViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
//    var formCoverView: UIView! { get set }
//    var coverStackView: UIStackView! { get set }
//    var titleStackView: UIStackView! { get set }
//    var formStackView: UIStackView! { get set }
//    var separatorLineView: UIView! { get set }
//    var bottomLineStackView: UIStackView! { get set }
//    var payerCardCell: PaymentDataPreviewFormCell! { get set }
//    var payerCardOwnerCell: PaymentDataPreviewFormCell! { get set }
//    var totalAmountLabel: UILabel! { get set }
    var successLabelStackView: UIStackView! { get set }
    var successCheckMarkImageView: UIImageView! { get set }
    var successLabel: UILabel! { get set }
    var buttonsVerticalStackView: UIStackView! { get set }
    var buttonsStackView: UIStackView! { get set }
    
    var titleLabel: UILabel! { get set }
    var addToFavoriteButton: NextButton! { get set }
    var printButton: NextButton! { get set }
    var closeButton: NextButton! { get set }
    
}

extension PaymentCompletionViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
//        formCoverView = UIView()
//        formCoverView.backgroundColor = .white
//        formCoverView.layer.cornerRadius = Adaptive.val(12.5)
//
//        coverStackView = UIStackView()
//        coverStackView.axis = .vertical
//        coverStackView.alignment = .fill
//        coverStackView.spacing = Adaptive.val(20)
//        coverStackView.layoutMargins = Adaptive.edges(top: 20, left:20, bottom: 20, right: 20)
//        coverStackView.isLayoutMarginsRelativeArrangement = true
//
//        titleStackView = UIStackView()
//        titleStackView.axis = .vertical
//        titleStackView.alignment = .center
//
//        formStackView = UIStackView()
//        formStackView.axis = .vertical
//        formStackView.alignment = .fill
//
//        separatorLineView = UIView()
//        separatorLineView.backgroundColor = .black
//
//        bottomLineStackView = UIStackView()
//        bottomLineStackView.axis = .vertical
//        bottomLineStackView.alignment = .center
//        bottomLineStackView.spacing = Adaptive.val(10)
//
//        payerCardCell = PaymentDataPreviewFormCell(frame: .zero)
//        payerCardOwnerCell = PaymentDataPreviewFormCell(frame: .zero)
//
//        totalAmountLabel = UILabel()
//        totalAmountLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(23))
//        totalAmountLabel.textColor = .black
//        totalAmountLabel.textAlignment = .center
        
        successLabelStackView = UIStackView()
        successLabelStackView.axis = .vertical
        successLabelStackView.alignment = .center
        successLabelStackView.spacing = Adaptive.val(15)
        
        successCheckMarkImageView = UIImageView()
        successCheckMarkImageView.image = UIImage(named: "img_success_circle")
        
        successLabel = UILabel()
        successLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(14))
        successLabel.textColor = ColorConstants.gray
        successLabel.textAlignment = .left
        successLabel.text = RS.lbl_payment_success.localized()
        
        buttonsVerticalStackView = UIStackView()
        buttonsVerticalStackView.axis = .vertical
        buttonsVerticalStackView.alignment = .fill
        buttonsVerticalStackView.spacing = Adaptive.val(10)
        
        buttonsStackView = UIStackView()
        buttonsStackView.axis = .horizontal
        buttonsStackView.alignment = .center
        buttonsStackView.distribution = .fillEqually
        buttonsStackView.spacing = Adaptive.val(20)
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(18))
        titleLabel.textColor = .black
        
        addToFavoriteButton = NextButton.systemButton(with: RS.btn_add_to_favourites.localized(), cornerRadius: Size.buttonHeight/2)
        addToFavoriteButton.titleLabel?.lineBreakMode = .byWordWrapping
        addToFavoriteButton.titleLabel?.textAlignment = .center
        addToFavoriteButton.titleLabel?.numberOfLines = 2
        addToFavoriteButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        addToFavoriteButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        addToFavoriteButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        printButton = NextButton.systemButton(with: RS.btn_print.localized().uppercased(), cornerRadius: Size.buttonHeight/2)
        printButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        printButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        printButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        
        closeButton = NextButton.systemButton(with: RS.btn_close.localized().uppercased(), cornerRadius: Size.buttonHeight/2)
        closeButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        closeButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        closeButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
//        mainView.addSubview(formCoverView)
//        formCoverView.addSubview(coverStackView)
//        coverStackView.addArrangedSubview(titleStackView)
//        coverStackView.addArrangedSubview(formStackView)
//        coverStackView.addArrangedSubview(separatorLineView)
//        coverStackView.addArrangedSubview(bottomLineStackView)
//        bottomLineStackView.addArrangedSubview(totalAmountLabel)
        mainView.addSubview(successLabelStackView)
        mainView.addSubview(buttonsVerticalStackView)
        buttonsVerticalStackView.addArrangedSubview(buttonsStackView)
        
//        titleStackView.addArrangedSubview(titleLabel)
//        formStackView.addArrangedSubview(payerCardCell)
//        formStackView.addArrangedSubview(payerCardOwnerCell)
        
        successLabelStackView.addArrangedSubview(successCheckMarkImageView)
        successLabelStackView.addArrangedSubview(successLabel)
        
        buttonsStackView.addArrangedSubview(addToFavoriteButton)
        buttonsStackView.addArrangedSubview(printButton)
        buttonsVerticalStackView.addArrangedSubview(closeButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        successLabelStackView.snp.remakeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.top.equalToSuperview().offset(Adaptive.val(187))
        }
        
        buttonsVerticalStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(successLabelStackView.snp.bottom).offset(Adaptive.val(96))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
        }
        
//        formCoverView.snp.remakeConstraints { (maker) in
//            maker.leading.equalToSuperview().offset(Adaptive.val(16))
//            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
//            maker.centerY.equalToSuperview()
//        }
//
//        coverStackView.snp.remakeConstraints { (maker) in
//            maker.edges.equalToSuperview()
//        }
//
//        separatorLineView.snp.remakeConstraints { (maker) in
//            maker.height.equalTo(Adaptive.val(1))
//        }
        
        successCheckMarkImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(80))
        }
        
        addToFavoriteButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.buttonHeight)
        }
        
        printButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.buttonHeight)
        }
        
        closeButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.buttonHeight)
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(50)
}
