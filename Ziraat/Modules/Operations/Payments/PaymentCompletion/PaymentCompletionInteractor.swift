//
//  PaymentCompletionInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentCompletionInteractorProtocol: class {
    
}

class PaymentCompletionInteractor: PaymentCompletionInteractorProtocol {
    weak var presenter: PaymentCompletionInteractorToPresenterProtocol!
    
}
