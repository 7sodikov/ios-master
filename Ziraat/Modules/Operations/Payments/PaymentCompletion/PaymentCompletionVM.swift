//
//  PaymentCompletionVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class PaymentCompletionVM {
    var service: PaymentDataEntryFormServiceEntity!
    var payerCard: OperationPagePA2PDataEntryUserEntryPayer!
    var paymentPrepareResponse: PaymentPrepareResponse!
    var paymentConfirmationResponse: P2PConfirmResponse!
}
