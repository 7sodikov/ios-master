//
//  PaymentCompletionPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentCompletionPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: PaymentCompletionVM { get }
    func addToFavoriteButtonClicked()
    func printButtonClicked()
}

protocol PaymentCompletionInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class PaymentCompletionPresenter: PaymentCompletionPresenterProtocol {
    weak var view: PaymentCompletionVCProtocol!
    var interactor: PaymentCompletionInteractorProtocol!
    var router: PaymentCompletionRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PaymentCompletionVM()
    
    func addToFavoriteButtonClicked() {
        router.openFavoriteCreationView(from: view, delegate: self, operationId: viewModel.paymentConfirmationResponse.operationId)
    }
    
    func printButtonClicked() {
        router.navigatetoPdfChequeVC(in: view as Any, operation: viewModel.paymentConfirmationResponse.receipt)
    }
    
    // Private property and methods
    
}

extension PaymentCompletionPresenter: PaymentCompletionInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}

extension PaymentCompletionPresenter: FavoriteAddDelegate {
    func favoriteDidAddSuccessfully() {
        view.showFavoriteAddedAlert()
    }
}
