//
//  PaymentCompletionRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentCompletionRouterProtocol: class {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse,
                             paymentConfirmationResponse: P2PConfirmResponse) -> UIViewController
    func openFavoriteCreationView(from controller: Any?, delegate: FavoriteAddDelegate?, operationId: Int)
    func navigatetoPdfChequeVC(in navigateCtrl: Any, operation: String)
}

class PaymentCompletionRouter: PaymentCompletionRouterProtocol {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse,
                             paymentConfirmationResponse: P2PConfirmResponse) -> UIViewController {
        let vc = PaymentCompletionVC()
        let presenter = PaymentCompletionPresenter()
        let interactor = PaymentCompletionInteractor()
        let router = PaymentCompletionRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.service = service
        presenter.viewModel.payerCard = payerCard
        presenter.viewModel.paymentPrepareResponse = paymentPrepareResponse
        presenter.viewModel.paymentConfirmationResponse = paymentConfirmationResponse
        interactor.presenter = presenter
        
        return vc
    }
    
    func openFavoriteCreationView(from controller: Any?, delegate: FavoriteAddDelegate?, operationId: Int) {
        let ctrl = FavoriteAddRouter.createModule(for: operationId, delegate: delegate)
        ctrl.modalPresentationStyle = .overCurrentContext
        (controller as? UIViewController)?.present(ctrl, animated: true, completion: nil)
    }
    
    func navigatetoPdfChequeVC(in navigateCtrl: Any, operation: String) {
        let viewCtrl = navigateCtrl as! UIViewController
//        let statementVC = PdfChequeRouter.createModule(operation: operation, title: RS.lbl_transfer_to_card.localized(), isHidden: false)
        let pdfView = PdfViewController(operation: operation, title: "")
        viewCtrl.navigationController?.pushViewController(pdfView, animated: true)
//        statementVC.modalPresentationStyle = .overFullScreen
//        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
}
