//
//  ServiceSearchRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceSearchRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToPaymentForm(for service: ServiceResponse, from view: Any?)
}

class ServiceSearchRouter: ServiceSearchRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ServiceSearchVC()
        let presenter = ServiceSearchPresenter()
        let interactor = ServiceSearchInteractor()
        let router = ServiceSearchRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPaymentForm(for service: ServiceResponse, from view: Any?) {
        let vc = PaymentDataEntryFormRouter.createModule(for: service, favoriteItem: nil, payment: nil)
        let navCtrl = NavigationController(rootViewController: vc)
        navCtrl.modalPresentationStyle = .overCurrentContext
        
        let currentViewCtrl = view as? UIViewController
        currentViewCtrl?.present(navCtrl, animated: true, completion: nil)
    }
}
