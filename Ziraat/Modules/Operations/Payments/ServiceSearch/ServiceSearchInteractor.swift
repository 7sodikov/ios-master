//
//  ServiceSearchInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ServiceSearchInteractorProtocol: class {
    func servicesFilter(for searchText: String)
}

class ServiceSearchInteractor: ServiceSearchInteractorProtocol {
    weak var presenter: ServiceSearchInteractorToPresenterProtocol!
    
    func servicesFilter(for searchText: String) {
        NetworkService.Payment.servicesFilter(for: searchText) { [weak self] (result) in
            self?.presenter.didServicesFilter(with: result)
        }
    }
}
