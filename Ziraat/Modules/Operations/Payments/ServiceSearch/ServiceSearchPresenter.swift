//
//  ServiceSearchPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ServiceSearchPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: ServiceSearchVM { get }
    func searchBarDidChange(searchText: String)
    func tableView(didSelectRowAt index: Int)
}

protocol ServiceSearchInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didServicesFilter(with response: ResponseResult<[ServicesFilterResponse], AppError>)
}

class ServiceSearchPresenter: ServiceSearchPresenterProtocol {
    weak var view: ServiceSearchVCProtocol!
    var interactor: ServiceSearchInteractorProtocol!
    var router: ServiceSearchRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ServiceSearchVM()
    private var searchDelayerTimer: Timer?
    
    func searchBarDidChange(searchText: String) {
        viewModel.userEntry.searchText = searchText
        searchDelayerTimer?.invalidate()
        searchDelayerTimer = Timer.scheduledTimer(timeInterval: 0.7,
                                                  target: self,
                                                  selector: #selector(performSearch),
                                                  userInfo: nil,
                                                  repeats: false)
    }
    
    func tableView(didSelectRowAt index: Int) {
        let service = viewModel.services[index]
        router.navigateToPaymentForm(for: service, from: view)
    }
    
    // Private property and methods
    @objc private func performSearch() {
        interactor.servicesFilter(for: viewModel.userEntry.searchText)
    }
}

extension ServiceSearchPresenter: ServiceSearchInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didServicesFilter(with response: ResponseResult<[ServicesFilterResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.servicesFilter = result.data
            view.reloadData()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            view.reloadData()
            break
        }
    }
}
