//
//  ServiceSearchTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class ServiceSearchTableCell: UITableViewCell, ServiceSearchTableCellInstaller {
    var mainView: UIView { contentView }
    var iconImageView: UIImageView!
    var titleLabel: UILabel!
    var subtitleLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    var dataModel: FilterModel? {
        didSet {
            self.titleLabel.text = dataModel?.name
        }
    }
    
    func setup(with title: String, logoUrl: String) -> ServiceSearchTableCell {
        iconImageView.kf.setImage(with: URL(string: logoUrl), placeholder: nil)
        titleLabel.text = title
        return self
    }
}
