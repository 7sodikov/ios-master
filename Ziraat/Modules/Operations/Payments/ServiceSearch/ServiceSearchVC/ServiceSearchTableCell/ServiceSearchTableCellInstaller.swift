//
//  ServiceSearchTableCellInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol ServiceSearchTableCellInstaller: ViewInstaller {
    var iconImageView: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
    var subtitleLabel: UILabel! { get set }
}

extension ServiceSearchTableCellInstaller {
    func initSubviews() {
        mainView.backgroundColor = .white
        
        iconImageView = UIImageView()
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: 18)
        titleLabel.textColor = .black
        
        subtitleLabel = UILabel()
        subtitleLabel.font = EZFontType.regular.sfProText(size: 14)
        subtitleLabel.textColor = .black
    }
    
    func embedSubviews() {
        mainView.addSubview(iconImageView)
        mainView.addSubview(titleLabel)
        mainView.addSubview(subtitleLabel)
    }
    
    func addSubviewsConstraints() {
        iconImageView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(10))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.width.height.equalTo(Adaptive.val(30))
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(iconImageView.snp.top)
            maker.left.equalTo(iconImageView.snp.right).offset(Adaptive.val(10))
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
        }
        
        subtitleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(5))
            maker.left.right.equalTo(titleLabel)
        }
    }
}

fileprivate struct Size {
    static let cellCoverPadding = Adaptive.val(15)
}
