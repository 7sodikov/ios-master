//
//  ServiceSearchVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceSearchVCProtocol: BaseViewControllerProtocol {
    func reloadData()
}

class ServiceSearchVC: BaseViewController, ServiceSearchViewInstaller {
    var mainView: UIView { view }
    var cancelButton: UIButton!
    var searchBar: UISearchBar!
    var bgImageView: UIImageView!
    var tableView: UITableView!
    var presenter: ServiceSearchPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: cancelButton)
        
        searchBar.becomeFirstResponder()
        searchBar.delegate = self
        navigationItem.titleView = searchBar
        
        tableView.dataSource = self
        tableView.delegate = self
        
        setCustomGesture()
    }
    
    // https://stackoverflow.com/a/27929223/845345
    // https://stackoverflow.com/a/18123885/845345
    func setCustomGesture() {
        if #available(iOS 13.0, *) {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(makeSearchBarFirstResponder(_:)))
            searchBar.searchTextField.gestureRecognizers = nil
            searchBar.searchTextField.addGestureRecognizer(tapGesture)
        }
    }
    
    @objc private func makeSearchBarFirstResponder(_ sender: UITapGestureRecognizer) {
        sender.view?.becomeFirstResponder()
    }
    
    @objc private func cancelButtonClicked() {
        dismiss(animated: true, completion: nil)
    }
}

extension ServiceSearchVC: ServiceSearchVCProtocol {
    func reloadData() {
        tableView.reloadData()
    }
}

extension ServiceSearchVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = String(describing: ServiceSearchTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! ServiceSearchTableCell
        let service = presenter.viewModel.services[indexPath.row]
        return cell.setup(with: service.title, logoUrl: service.logoURL)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(80)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.tableView(didSelectRowAt: indexPath.row)
    }
}

extension ServiceSearchVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchBarDidChange(searchText: searchText)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        setCustomGesture()
        return true
    }
}
