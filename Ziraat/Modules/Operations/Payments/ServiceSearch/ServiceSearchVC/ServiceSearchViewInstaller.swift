//
//  ServiceSearchViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceSearchViewInstaller: ViewInstaller {
    var cancelButton: UIButton! { get set }
    var searchBar: UISearchBar! { get set }
    var bgImageView: UIImageView! { get set }
    var tableView: UITableView! { get set }
}

extension ServiceSearchViewInstaller {
    func initSubviews() {
        cancelButton = UIButton()
        cancelButton.setImage(UIImage(named: "btn_prev"), for: .normal)
        
        searchBar = UISearchBar()
        
        bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "img_dash_light_background")
        
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.register(ServiceSearchTableCell.self, forCellReuseIdentifier: String(describing: ServiceSearchTableCell.self))
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
    }
    
    func embedSubviews() {
        mainView.addSubview(bgImageView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        bgImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        cancelButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(14.4))
            maker.height.equalTo(Adaptive.val(25.2))
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(64)
            maker.left.bottom.right.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
