//
//  ServiceSearchVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ServiceSearchVM {
    let userEntry = ServiceSearchUserEntry()
    var servicesFilter: [ServicesFilterResponse] = []
    
    var services: [ServiceResponse] {
        servicesFilter.map(\.filteredServices).reduce(.init(), +)
    }
}
