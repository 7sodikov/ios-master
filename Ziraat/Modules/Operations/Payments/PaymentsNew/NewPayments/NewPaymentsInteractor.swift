//
//  NewPaymentsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsInteractorProtocol: class {
    
}

class NewPaymentsInteractor: NewPaymentsInteractorProtocol {
    weak var presenter: NewPaymentsInteractorToPresenterProtocol!
    
}
