//
//  NewPaymentsFirstPageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsFirstPageVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
    func reloadData()
    func openDropDown()
}

class NewPaymentsFirstPageVC: BaseViewController, NewPaymentsFirstPageViewInstaller {
    var mainView: UIView { view }
    var searchLabel: UILabel!
    var searchView: UIView!
    var searchTextField: UISearchBar!
    var searchButton: UIButton!
    var reloadButtonView: ReloadButtonView!
    var tableView: UITableView!
    var presenter: NewPaymentsFirstPagePresenterProtocol!
    
    var cellid = "\(NewPaymentsFirstPageTableViewCell.self)"
    var searchCellId = "\(ServiceSearchTableCell.self)"
    
    // MARK: DropDown
    let filterTableView = UITableView()
    let transparentView = UIView()
    var selectedView = UIView()
    var data = [FilterModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        setupSubviews()
        
        searchTextField.setImage(UIImage(), for: .search, state: .normal)
//        searchTextField.becomeFirstResponder()
        searchTextField.delegate = self
        searchTextField.returnKeyType = UIReturnKeyType.done
//        setCustomGesture()
        
//        searchButton.addTarget(self, action: #selector(searchButtonClicked), for: .touchUpInside)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        filterTableView.register(ServiceSearchTableCell.self, forCellReuseIdentifier: String(describing: ServiceSearchTableCell.self))
        filterTableView.backgroundColor = .clear
        filterTableView.separatorColor = .clear
        filterTableView.tableFooterView = UIView()
        filterTableView.showsVerticalScrollIndicator = false
        filterTableView.delegate = self
        filterTableView.dataSource = self
    }
    
    func setupSearchBar(searchBar : UISearchBar) {
        searchTextField.setPlaceholderTextColorTo(color: UIColor.black)
    }
    
    func setCustomGesture() {
        if #available(iOS 13.0, *) {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(makeSearchBarFirstResponder(_:)))
            searchTextField.searchTextField.gestureRecognizers = nil
            searchTextField.searchTextField.addGestureRecognizer(tapGesture)
            openDropDown()
        }
    }
    
    func addTransparentView(frames: CGRect) {
        let window = UIApplication.shared.keyWindow
        transparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        
        filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width, height: 0)
        self.view.addSubview(filterTableView)
        filterTableView.layer.cornerRadius = 5
        
        transparentView.backgroundColor = .clear //UIColor.black.withAlphaComponent(0.9)
        filterTableView.reloadData()
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(removeTransparentView))
        transparentView.addGestureRecognizer(tapgesture)
        transparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height + 5, width: frames.width, height: Adaptive.val(200))
        }, completion: nil)
    }
    
    @objc func removeTransparentView() {
        searchTextField.endEditing(true)
        let frames = selectedView.frame
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width, height: 0)
        }, completion: nil)
    }
    
    @objc func openDropDown() {
        data = [FilterModel(name: RS.cell_ttl_last_day.localized()),
                FilterModel(name: RS.cell_ttl_last_week.localized()),
                FilterModel(name: RS.cell_ttl_last_month.localized()),
                FilterModel(name: RS.cell_ttl_three_month.localized())
        ]
        selectedView = searchView
        addTransparentView(frames: searchView.frame)
    }
    
    @objc private func makeSearchBarFirstResponder(_ sender: UITapGestureRecognizer) {
        sender.view?.becomeFirstResponder()
    }
    
    @objc private func searchButtonClicked() {
        presenter.searchButtonClicked()
    }
    
    func reloadData() {
        filterTableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        setupSearchBar(searchBar: searchTextField)
    }

}

extension NewPaymentsFirstPageVC: NewPaymentsFirstPageVCProtocol, UITableViewDelegate, UITableViewDataSource {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return presenter.viewModel.categories.count
        } else if tableView == self.filterTableView {
            return presenter.viewModel.services.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! NewPaymentsFirstPageTableViewCell
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            let category = presenter.viewModel.categories[indexPath.row]
            return cell.setup(with: category.title, logoUrl: category.logoUrl)
        } else if tableView == self.filterTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: searchCellId, for: indexPath) as! ServiceSearchTableCell
//            let cellid = String(describing: ServiceSearchTableCell.self)
//            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! ServiceSearchTableCell
            if !presenter.viewModel.services.isEmpty || presenter.viewModel.services.count != 0 {
                let service = presenter.viewModel.services[indexPath.row]
                return cell.setup(with: service.title, logoUrl: service.logoURL)
            } else {
                cell.titleLabel.text = "No Results"
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView {
            presenter.navigateToNextPage(categoryId: indexPath.row)
        } else if tableView == self.filterTableView {
            presenter.navigateToPaymentPage(categoryId: indexPath.row)
        }
    }
}

extension NewPaymentsFirstPageVC: UISearchBarDelegate, UITextFieldDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchBarDidChange(searchText: searchText)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        setCustomGesture()
        return true
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
}
