//
//  NewPaymentsFirstPageTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol NewPaymentsFirstPageTableViewCellInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var serviceImageView: UIImageView! { get set }
    var serviceNameLabel: UILabel! { get set }
    var arrowImageView: UIImageView! { get set }
}

extension NewPaymentsFirstPageTableViewCellInstaller {
    func initSubviews() {
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.borderWidth = Adaptive.val(2)
        backView.layer.borderColor = UIColor.darkGrey.cgColor
        backView.layer.cornerRadius = Adaptive.val(6)
        
        serviceImageView = UIImageView()
        serviceImageView.contentMode = .scaleAspectFit
        
        serviceNameLabel = UILabel()
        serviceNameLabel.font = .gothamNarrow(size: 15, .medium)
        serviceNameLabel.textColor = .black
        serviceNameLabel.numberOfLines = 0
        
        arrowImageView = UIImageView()
        arrowImageView.image = UIImage(named: "img_icon_arr")
                
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(serviceImageView)
        mainView.addSubview(serviceNameLabel)
        mainView.addSubview(arrowImageView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(50))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.top.equalToSuperview().offset(Adaptive.val(7.5))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(7.5))
        }
        
        serviceImageView.snp.remakeConstraints { (maker) in
            maker.top.leading.equalTo(backView).offset(Adaptive.val(8))
            maker.bottom.equalTo(backView).offset(-Adaptive.val(8))
            maker.trailing.equalTo(serviceNameLabel.snp.leading).offset(-Adaptive.val(8))
        }
        
        serviceNameLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(backView)
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(50))
            maker.trailing.equalTo(arrowImageView.snp.leading).offset(-Adaptive.val(10))
        }
        
        arrowImageView.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(backView)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(16))
            maker.height.equalTo(Adaptive.val(13.5))
            maker.width.equalTo(Adaptive.val(8))
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(30)
}
