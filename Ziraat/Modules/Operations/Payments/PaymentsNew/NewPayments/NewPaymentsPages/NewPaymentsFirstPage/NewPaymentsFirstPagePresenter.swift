//
//  NewPaymentsFirstPagePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsFirstPageDelegate: class {
    func newPaymentsFirstPageFinished(with categoryId: Int?)
    func newPaymentFirstPageReturn()
}

protocol NewPaymentsFirstPagePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: NewPaymentsFirstPageVM { get }
    func viewDidLoad()
    func navigateToNextPage(categoryId: Int)
    func navigateToPaymentPage(categoryId: Int)
    func searchButtonClicked()
    func searchBarDidChange(searchText: String)

}

protocol NewPaymentsFirstPageInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didServiceCategories(with response: ResponseResult<[ServiceCategoryResponse], AppError>)
    func didServicesFilter(with response: ResponseResult<[ServicesFilterResponse], AppError>)
}

class NewPaymentsFirstPagePresenter: NewPaymentsFirstPagePresenterProtocol {
    weak var view: NewPaymentsFirstPageVCProtocol!
    var interactor: NewPaymentsFirstPageInteractorProtocol!
    var router: NewPaymentsFirstPageRouterProtocol!
    weak var delegate: NewPaymentsFirstPageDelegate?
    weak var paymentDelegate: NewPaymentsSecondPageDelegate?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = NewPaymentsFirstPageVM()
    
    func viewDidLoad() {
        interactor.services()
    }
    
    func navigateToNextPage(categoryId: Int) {
        let serviceId = viewModel.categories[categoryId]
        router.navigateToNextPage(with: serviceId.id, delegate: delegate)
    }
    
    func navigateToPaymentPage(categoryId: Int) {
        let service = viewModel.services[categoryId]
        router.navigateToPaymentPage(with: service, delegate: paymentDelegate)
    }
    
    func searchButtonClicked() {
        router.navigateToServiceSearch(from: view)
    }
    
    func searchBarDidChange(searchText: String) {
        viewModel.userEntry.searchText = searchText
        searchDelayerTimer?.invalidate()
        searchDelayerTimer = Timer.scheduledTimer(timeInterval: 0.7,
                                                  target: self,
                                                  selector: #selector(performSearch),
                                                  userInfo: nil,
                                                  repeats: false)
    }
    
    // Private property and methods
    private var searchDelayerTimer: Timer?
    
    @objc private func performSearch() {
        interactor.servicesFilter(for: viewModel.userEntry.searchText)
    }
}

extension NewPaymentsFirstPagePresenter: NewPaymentsFirstPageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didServiceCategories(with response: ResponseResult<[ServiceCategoryResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.categories = result.data
            view.showReloadButtonForResponses(false, animateReloadButton: false)
        case let .failure(error):
//            view.showError(message: error.localizedDescription)
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            break
        }
    }
    
    func didServicesFilter(with response: ResponseResult<[ServicesFilterResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.servicesFilter = result.data
            if !viewModel.servicesFilter.isEmpty {
            }
            view.reloadData()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            view.reloadData()
            break
        }
    }
}
