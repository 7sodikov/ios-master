//
//  NewPaymentsFirstPageTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

class NewPaymentsFirstPageTableViewCell: UITableViewCell, NewPaymentsFirstPageTableViewCellInstaller {
    var mainView: UIView { contentView }
    var backView: UIView!
    var serviceImageView: UIImageView!
    var serviceNameLabel: UILabel!
    var arrowImageView: UIImageView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with title: String?, logoUrl: String) -> NewPaymentsFirstPageTableViewCell {
        serviceImageView.kf.setImage(with: URL(string: logoUrl), placeholder: nil)
        serviceNameLabel.text = title
        return self
    }
}
