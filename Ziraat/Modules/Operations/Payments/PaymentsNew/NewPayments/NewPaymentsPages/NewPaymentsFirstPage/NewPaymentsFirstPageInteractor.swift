//
//  NewPaymentsFirstPageInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsFirstPageInteractorProtocol: class {
    func services()
    func servicesFilter(for searchText: String)
}

class NewPaymentsFirstPageInteractor: NewPaymentsFirstPageInteractorProtocol {
    weak var presenter: NewPaymentsFirstPageInteractorToPresenterProtocol!
    
    func services() {
        NetworkService.Payment.serviceCategories { [weak self] (result) in
            self?.presenter.didServiceCategories(with: result)
        }
    }
    
    func servicesFilter(for searchText: String) {
        NetworkService.Payment.servicesFilter(for: searchText) { [weak self] (result) in
            self?.presenter.didServicesFilter(with: result)
        }
    }
}
