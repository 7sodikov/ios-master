//
//  NewPaymentsFirstPageViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsFirstPageViewInstaller: ViewInstaller {
    var searchLabel: UILabel! { get set }
    var searchView: UIView! { get set }
    var searchTextField: UISearchBar! { get set }
    var searchButton: UIButton! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
    var tableView: UITableView! { get set }
//    var filterTableView: UITableView! { get set }
}

extension NewPaymentsFirstPageViewInstaller {
    func initSubviews() {
        searchLabel = UILabel()
        searchLabel.font = .gothamNarrow(size: 16, .medium)
        searchLabel.textColor = .black
        searchLabel.text = RS.txt_f_search.localized()
        
        searchView = UIView()
        searchView.backgroundColor = .white
        searchView.layer.borderWidth = Adaptive.val(2)
        searchView.layer.borderColor = UIColor.darkGrey.cgColor
        searchView.layer.cornerRadius = Adaptive.val(6)
        
        searchTextField = UISearchBar()
        searchTextField.placeholder = RS.txt_f_search.localized()
        searchTextField.backgroundColor = .clear
        searchTextField.keyboardType = .namePhonePad
        if #available(iOS 13.0, *) {
            searchTextField.searchTextField.addDoneButtonOnKeyboard()
        } else {
            // Fallback on earlier versions
        }
//        searchTextField.layer.cornerRadius = Adaptive.val(6)
//        searchTextField.isUserInteractionEnabled = false
        
        searchButton = UIButton()
        searchButton.setImage(UIImage(named: "btn_search"), for: .normal)
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        
        tableView = UITableView()
        tableView.register(NewPaymentsFirstPageTableViewCell.self, forCellReuseIdentifier: String(describing: NewPaymentsFirstPageTableViewCell.self))
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        
//        filterTableView = UITableView()
//        filterTableView.register(ServiceSearchTableCell.self, forCellReuseIdentifier: String(describing: ServiceSearchTableCell.self))
//        filterTableView.backgroundColor = .clear
//        filterTableView.separatorColor = .clear
//        filterTableView.tableFooterView = UIView()
//        filterTableView.showsVerticalScrollIndicator = false
    }
    
    func embedSubviews() {
        mainView.addSubview(searchLabel)
        mainView.addSubview(searchView)
        mainView.addSubview(searchTextField)
        mainView.addSubview(searchButton)
        mainView.addSubview(reloadButtonView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        searchLabel.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(15))
            maker.leading.equalToSuperview().offset(Adaptive.val(21))
        }
        
        searchView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(searchLabel.snp.bottom).offset(Adaptive.val(15))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.height.equalTo(Adaptive.val(50))
        }
        
        searchTextField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(searchView.snp.top).offset(Adaptive.val(2))
            maker.bottom.equalTo(searchView.snp.bottom).offset(-Adaptive.val(2))
            maker.leading.equalTo(searchView).offset(Adaptive.val(5))
            maker.trailing.equalTo(searchButton.snp.leading)
        }
        
        searchButton.snp.remakeConstraints { (maker) in
            maker.height.width.equalTo(Adaptive.val(24))
            maker.centerY.equalTo(searchView)
            maker.trailing.equalTo(searchView.snp.trailing).offset(-Adaptive.val(15))
        }
        
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(searchView.snp.bottom).offset(Adaptive.val(7.5))
            maker.leading.bottom.trailing.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
