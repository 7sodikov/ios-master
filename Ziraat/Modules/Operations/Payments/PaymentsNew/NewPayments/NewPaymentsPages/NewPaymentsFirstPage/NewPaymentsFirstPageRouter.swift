//
//  NewPaymentsFirstPageRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsFirstPageRouterProtocol: AnyObject {
    static func createModule(with delegate: NewPaymentsPresenter?) -> UIViewController
    func navigateToNextPage(with categoryId: Int, delegate: NewPaymentsFirstPageDelegate?)
    func navigateToPaymentPage(with service: ServiceResponse, delegate: NewPaymentsSecondPageDelegate?)
    func navigateToServiceSearch(from view: Any?)
}

class NewPaymentsFirstPageRouter: NewPaymentsFirstPageRouterProtocol {
    static func createModule(with delegate: NewPaymentsPresenter?) -> UIViewController {
        let vc = NewPaymentsFirstPageVC()
        let presenter = NewPaymentsFirstPagePresenter()
        let interactor = NewPaymentsFirstPageInteractor()
        let router = NewPaymentsFirstPageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        presenter.paymentDelegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToNextPage(with categoryId: Int, delegate: NewPaymentsFirstPageDelegate?) {
        delegate?.newPaymentsFirstPageFinished(with: categoryId)
    }
    
    func navigateToPaymentPage(with service: ServiceResponse, delegate: NewPaymentsSecondPageDelegate?) {
        delegate?.newPaymentsSecondPageFinished(for: service)
    }
    
    func navigateToServiceSearch(from view: Any?) {
        let viewCtrl = view as? UIViewController
        let searchCtrl = ServiceSearchRouter.createModule()
        let navCtrl = NavigationController(rootViewController: searchCtrl)
        navCtrl.modalPresentationStyle = .fullScreen
        viewCtrl?.present(navCtrl, animated: true, completion: nil)
    }
}
