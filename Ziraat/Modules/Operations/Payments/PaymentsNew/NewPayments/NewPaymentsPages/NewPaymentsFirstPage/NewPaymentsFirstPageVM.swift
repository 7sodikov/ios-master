//
//  NewPaymentsFirstPageVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class NewPaymentsFirstPageVM {
    let userEntry = ServiceSearchUserEntry()
    var servicesFilter: [ServicesFilterResponse] = []
    var categories: [ServiceCategoryResponse] = []
    var categoryArray: [Int64] = []
    
    func categoryVM(at index: Int) -> NewPaymentFirstPageVM {
        return NewPaymentFirstPageVM(categories[index])
    }
    
    var services: [ServiceResponse] {
        categoryArray = servicesFilter.map(\.categoryId)
        return servicesFilter.map(\.filteredServices).reduce(.init(), +)
    }
}

class NewPaymentFirstPageVM {
    var category: ServiceCategoryResponse
    
    init(_ category: ServiceCategoryResponse) {
        self.category = category
    }
}
