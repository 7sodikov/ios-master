//
//  NewPaymentsSecondPagePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsSecondPageDelegate: AnyObject {
    func newPaymentsSecondPageFinished(for service: ServiceResponse)
}

protocol NewPaymentsSecondPagePresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: NewPaymentsSecondPageVM { get }
    func viewWillAppear()
    func serviceSelected(at index: Int)
}

protocol NewPaymentsSecondPageInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didServices(with response: ResponseResult<[ServiceResponse], AppError>)
}

class NewPaymentsSecondPagePresenter: NewPaymentsSecondPagePresenterProtocol {
    weak var view: NewPaymentsSecondPageVCProtocol!
    var interactor: NewPaymentsSecondPageInteractorProtocol!
    var router: NewPaymentsSecondPageRouterProtocol!
    weak var delegate: NewPaymentsSecondPageDelegate?
    var categoryId: Int?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = NewPaymentsSecondPageVM()
    
    func viewWillAppear() {
        viewModel.services = []
        view.tableView.reloadData()
        interactor.services(for: categoryId ?? 0, searchString: nil)
    }
    
    func serviceSelected(at index: Int) {
        let service = viewModel.services[index]
        router.navigateToPaymentForm(for: service, delegate: delegate)
    }
    
    // Private property and methods
    
}

extension NewPaymentsSecondPagePresenter: NewPaymentsSecondPageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didServices(with response: ResponseResult<[ServiceResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.services = result.data
            if !viewModel.services.isEmpty {
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(true, animateReloadButton: true)
            }
        case let .failure(error):
//            view.showError(message: error.localizedDescription)
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            break
        }
    }
}
