//
//  NewPaymentsSecondPageVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class NewPaymentsSecondPageVM {
    var services: [ServiceResponse] = []
    var allFieldSections: [[CService.CValue]] = []
    var regions: [CService.CValue] = []
    var districts: [String: [CService.CValue]] = .init()
    var fieldSections: [[CService.CValue]] = []
}
