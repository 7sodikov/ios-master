//
//  NewPaymentsSecondPageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsSecondPageVCProtocol: NewPaymentsSecondPageViewInstaller {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class NewPaymentsSecondPageVC: UIViewController, NewPaymentsSecondPageVCProtocol {
    var mainView: UIView { view }
    var searchLabel: UILabel!
    var searchView: UIView!
    var searchTextField: UITextField!
    var searchButton: UIButton!
    var reloadButtonView: ReloadButtonView!
    var tableView: UITableView!
    
    var presenter: NewPaymentsSecondPagePresenterProtocol!
    
    var cellid = "\(NewPaymentsFirstPageTableViewCell.self)"
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        setupSubviews()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
}

extension NewPaymentsSecondPageVC: UITableViewDelegate, UITableViewDataSource {
    
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! NewPaymentsFirstPageTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        let category = presenter.viewModel.services[indexPath.row]
        return cell.setup(with: category.title, logoUrl: category.logoURL)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.serviceSelected(at: indexPath.row)
    }
}
