//
//  NewPaymentsSecondPageRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsSecondPageRouterProtocol: AnyObject {
    static func createModule(with delegate: NewPaymentsPresenter?) -> UIViewController
    func navigateToPaymentForm(for service: ServiceResponse, delegate: NewPaymentsSecondPageDelegate?)
}

class NewPaymentsSecondPageRouter: NewPaymentsSecondPageRouterProtocol {
    static func createModule(with delegate: NewPaymentsPresenter?) -> UIViewController {
        let vc = NewPaymentsSecondPageVC()
        let presenter = NewPaymentsSecondPagePresenter()
        let interactor = NewPaymentsSecondPageInteractor()
        let router = NewPaymentsSecondPageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPaymentForm(for service: ServiceResponse, delegate: NewPaymentsSecondPageDelegate?) {
        delegate?.newPaymentsSecondPageFinished(for: service)
    }
}
