//
//  NewPaymentsSecondPageInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsSecondPageInteractorProtocol: AnyObject {
    func services(for categoryId: Int, searchString: String?)
}

class NewPaymentsSecondPageInteractor: NewPaymentsSecondPageInteractorProtocol {
    weak var presenter: NewPaymentsSecondPageInteractorToPresenterProtocol!
    func services(for categoryId: Int, searchString: String?) {
        NetworkService.Payment.services(for: categoryId, searchString: searchString) { [weak self] (result) in
            self?.presenter.didServices(with: result)
        }
    }
}
