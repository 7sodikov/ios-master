//
//  NewPaymentsThirdPageInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsThirdPageInteractorProtocol: AnyObject {
    func getAllCards(isRequestedByButtonClick: Bool, completion: @escaping()->())
    func getAllCardBalances(completion: @escaping()->())
    func paymentPrepare(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any])
    func paymentGetInfo(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any],infoId: Int)
}

class NewPaymentsThirdPageInteractor: NewPaymentsThirdPageInteractorProtocol {
    weak var presenter: NewPaymentsThirdPageInteractorToPresenterProtocol!
    
    var service: CService?
    var allFieldSections: [[CService.CValue]] = []
    var regions: [CService.CValue] = []
    var districts: [String: [CService.CValue]] = .init()
    var fieldSections: [[CService.CValue]] = []
    
    func getAllCards(isRequestedByButtonClick: Bool, completion: @escaping()->()) {
        NetworkService.Card.cardsList { [weak self] (result) in
            self?.presenter.didGetAllCards(with: result, isRequestedByButtonClick: isRequestedByButtonClick)
            completion()
        }
    }
    
    func getAllCardBalances(completion: @escaping()->()) {
        NetworkService.Card.cardBalance { [weak self] (result) in
            self?.presenter.didGetAllCardBalances(with: result)
            completion()
        }
    }
    
    func paymentPrepare(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any]) {
        NetworkService.Payment.paymentPrepare(senderCardId: senderCardId,
                                              serviceId: serviceId,
                                              serviceType: serviceType,
                                              fields: fields) { [weak self] (result) in
            self?.presenter.didPaymentPrepare(with: result)
        }
    }
    
    func paymentGetInfo(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any], infoId: Int) {
        NetworkService.Payment.paymentGetInfo(senderCardId: senderCardId,
                                              serviceId: serviceId,
                                              serviceType: serviceType,
                                              fields: fields, infoId: infoId) { [weak self] (result) in
            self?.presenter.didPaymentGetInfo(with: result)
        }
    }
    
    func paymentOne(serviceId: String, serviceType: String,completion: @escaping()->()) {
        func configure(service: CService) {
            let fields = service.fields.filter(\.values.isNotEmpty).filter(\.fieldRequired)
            allFieldSections = fields.map(\.values)
            regions = allFieldSections.reduce(.init(), +).filter(\.subitems.isNotEmpty)
            districts = .init(grouping: regions, by: \.value)
            fieldSections = allFieldSections.filter { $0.allSatisfy(\.subitems.isEmpty) }
        }
        NetworkService.Payment.getServiceOne(serviceId: serviceId, serviceType: serviceType) { response in
            switch response {
            case .success(let data):
                configure(service: data.data)
                self.service = data.data
            case .failure(let error):
                print(error.localizedDescription)
                
            }
        }
    }
}

extension Array {
    var isNotEmpty: Bool {
        !isEmpty
    }
}
