//
//  MSAdditionalPaymentInfoView.swift
//  Ziraat
//
//  Created by Muhriddinbek Samidov on 13/07/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class MSAdditionalPaymentInfoView: UIView {
    
    var tableView = UITableView()
    
    var data = [ObjectKeyValueModel]()
    
    var isClosed: Bool {
        return (self.alpha < 1.0) || (self.data.count <= 1)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .red
        self.setUpSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.backgroundColor = .red
        self.setUpSubviews()
    }
    
    private func setUpSubviews() {
        self.addSubview(self.tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.isScrollEnabled = true
        let footerv = UIView(frame: .zero)
        footerv.backgroundColor = .clear
        tableView.tableFooterView = footerv
        
        tableView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().offset(8)
            make.trailing.bottom.equalToSuperview().offset(-8)
        }
    }
    
    func update(with data: [ObjectKeyValueModel]) {
        self.data = data
        self.tableView.reloadData()
    }
    
    func hide() {
        UIView.animate(withDuration: 0.15) {
            self.data.removeAll()
            self.tableView.reloadData()
            self.alpha = 0.0
            self.snp.remakeConstraints { make in
                make.height.equalTo(0)
            }
        }
    }
    
    func show() {
        if self.alpha < 1.0 {
        UIView.animate(withDuration: 0.1) {
            self.alpha = 1.0
            self.snp.remakeConstraints { make in
                make.height.equalTo(200)
            }
        } completion: { done in
            self.tableView.reloadData()
        }
        } else {
            self.tableView.reloadData()
        }
    }
}

extension MSAdditionalPaymentInfoView : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "UITableViewCell")
        cell.accessoryType = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.textLabel?.textColor = .grayTextColor
        cell.textLabel?.font = .systemFont(ofSize: 14)
        cell.detailTextLabel?.textColor = .black
        cell.detailTextLabel?.font = .systemFont(ofSize: 16)
        cell.textLabel?.text = self.data[indexPath.row].label
        cell.detailTextLabel?.text = self.data[indexPath.row].text
        cell.detailTextLabel?.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.getItemHeight(indexPath)
    }
    
    private func getItemHeight(_ indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
}
