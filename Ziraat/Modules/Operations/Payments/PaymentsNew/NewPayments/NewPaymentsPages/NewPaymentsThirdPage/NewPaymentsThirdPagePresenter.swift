//
//  NewPaymentsThirdPagePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsThirdPagePresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: NewPaymentsThirdPageVM { get }
    func viewDidLoad()
    func myCardsClicked()
    func closeBtnClicked()
    func nextBtnClicked()
    func getServiceInfo(id: Int, invoice: String)
    func fetchData()
    func update(with service: ServiceResponse?, repayment: CLastPayment?)
    func openBottomSheet(option: NPOptonEnum)
}

protocol NewPaymentsThirdPageInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool)
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>)
    func didPaymentPrepare(with response: ResponseResult<PaymentPrepareResponse, AppError>)
    func didPaymentGetInfo(with response: ResponseResult<PaymentGetInfoResponse, AppError>)
}

class NewPaymentsThirdPagePresenter: NewPaymentsThirdPagePresenterProtocol, BottomSheetDelegate {
    
    func getServiceInfo(id: Int, invoice: String) {
        guard let service = self.viewModel.service, let infoId = service.infoServiceID else {
            return
        }
        view.preloader(show: true)
        interactor.paymentGetInfo(senderCardId: viewModel.userEntry.payerCard.payerCardId ?? 0,
                                  serviceId: Int64(viewModel.dataEntryFormServiceEntity.serviceId),
                                  serviceType: viewModel.dataEntryFormServiceEntity.serviceType,
                                  fields: viewModel.userEntry.fields,
                                  infoId: infoId)

    }
    
    func didSelect(item viewModel: TableViewModel, at indexPath: IndexPath, with tag: Int) {
        guard let option = NPOptonEnum(rawValue: tag) else { return }
        
        
        switch option {
        
        case .region:
            guard let cityFiled = self.viewModel.dataEntryFormServiceEntity.cityField else {
                return
            }
            let region = cityFiled.regions[indexPath.row]
            self.viewModel.dataEntryFormServiceEntity.selectedRegionId = region.value
            let district = cityFiled.getDistricts(at: region.value).first
            self.viewModel.dataEntryFormServiceEntity.selectedDistrictId = district?.value ?? ""
            self.viewModel.userEntry.set(fieldValue: district?.value, for: cityFiled)
        case .district:
            guard let cityFiled = self.viewModel.dataEntryFormServiceEntity.cityField else {
                return
            }
            let regionID = self.viewModel.dataEntryFormServiceEntity.selectedRegionId
            let district = cityFiled.getDistricts(at: regionID)[indexPath.row]
            self.viewModel.dataEntryFormServiceEntity.selectedDistrictId = district.value
            self.viewModel.userEntry.set(fieldValue: district.value, for: cityFiled)
        case .person:
            break
//            let person = cityFiled?.regions[indexPath.row]
//            self.viewModel.dataEntryFormServiceEntity.selectedRegionId = region?.value ?? ""
        }
        view.clearStackView()
        view.drawView()
        
    }
    
    func bottomSheet(didSelectCardAt indexPath: IndexPath, card: Any, with tag: Int) {
        if let card = card as? Card {
            view.setCardNumber(number: card.pan)
            viewModel.userEntry.setPayer(cardId: card.cardId, pan: card.pan, owner: card.owner)
            view.nextButton(enable: viewModel.isAllFieldsValid)
        }
    }
    
    
    func openBottomSheet(option: NPOptonEnum) {
        var bottomSheet: BottomSheet
        switch option {
        
        case .region:
            let rows = viewModel.dataEntryFormServiceEntity.regionViewModels
            let height = UIScreen.main.bounds.height * 2 / 3
            let section = BottomSheetSection(header: "", rows: rows)
            bottomSheet = BottomSheet(viewHeight: height, dataSource: [section], with: NPOptonEnum.region.rawValue)
            
        case .district:
            let rows = viewModel.dataEntryFormServiceEntity.districtViewModels
            let height = UIScreen.main.bounds.height * 2 / 3
            let section = BottomSheetSection(header: "", rows: rows)
            bottomSheet = BottomSheet(viewHeight: height, dataSource: [section], with: NPOptonEnum.district.rawValue)
        case .person:
            let rows = viewModel.dataEntryFormServiceEntity.personViewModel
            let height = UIScreen.main.bounds.height / 3
            let section = BottomSheetSection(header: "", rows: rows)
            bottomSheet = BottomSheet(viewHeight: height, dataSource: [section], with: NPOptonEnum.person.rawValue)
        }
        
        bottomSheet.delegate = self
        let vc = BottomSheetViewController(bottomSheet: bottomSheet)
        vc.modalPresentationStyle = .custom
        controller.presentPanModal(vc)
    }
    
    unowned var view: NewPaymentsThirdPageVCProtocol!
    var interactor: NewPaymentsThirdPageInteractorProtocol!
    var router: NewPaymentsThirdPageRouterProtocol!
    var serviceData: ServiceResponse?
    var controller: NewPaymentsThirdPageVC { view as! NewPaymentsThirdPageVC }
    // VIEW -> PRESENTER
    private(set) var viewModel = NewPaymentsThirdPageVM()
    
    func viewDidLoad() {
        view.nextButton(enable: true)
        view.cardsDownloadPreloader(show: true)
//        downloadMyCardsWithBalances(isRequestedByButtonClick: false)
    }
    
    func fetchData() {
//        viewModel.dataEntryFormServiceEntity.fields = []
//        viewModel.service = serviceData
        downloadMyCardsWithBalances(isRequestedByButtonClick: false)
    }
    
    func update(with service: ServiceResponse?, repayment: CLastPayment?) {
        viewModel.dataEntryFormServiceEntity.fields = []
        viewModel.service = service
        fetchData()
    }
    
    func myCardsClicked() {
        if viewModel.cards.count > 0 {
            openCardsList()
        } else {
            view.cardsDownloadPreloader(show: true)
            downloadMyCardsWithBalances(isRequestedByButtonClick: true)
        }
    }
    
    func closeBtnClicked() {
        router.dismiss(view: view)
    }
    
    func nextBtnClicked() {
        view.preloader(show: true)
        interactor.paymentPrepare(senderCardId: viewModel.userEntry.payerCard.payerCardId ?? 0,
                                  serviceId: Int64(viewModel.dataEntryFormServiceEntity.serviceId),
                                  serviceType: viewModel.dataEntryFormServiceEntity.serviceType,
                                  fields: viewModel.userEntry.fields)
        
    }
    
    // Private property and methods
    private let dateFormat = "yyyy-MM-dd"
    
    private func downloadMyCardsWithBalances(isRequestedByButtonClick: Bool) {
        let group = DispatchGroup()
        group.enter()
        group.enter()
        
        interactor.getAllCards(isRequestedByButtonClick: isRequestedByButtonClick) {
            group.leave()
        }
        interactor.getAllCardBalances {
            group.leave()
        }
        
        group.notify(queue: .main) {
            self.viewModel.updateCard()
            self.view.cardsDownloadPreloader(show: false)
            self.view.drawView()
            if isRequestedByButtonClick {
                self.openCardsList()
            }
        }
    }
    
    private func openCardsList() {
        let bottomSheet = viewModel.caardAccountBottomSheet
        bottomSheet.delegate = self
        let vc = BottomSheetViewController(bottomSheet: bottomSheet)
        vc.modalPresentationStyle = .custom
        controller.presentPanModal(vc)
        
//        var cardsList: [EZSelectControllerCellModel] = []
//        for i in 0..<viewModel.cards.count {
//            let cardVM = viewModel.cardVM(at: i)
//            let selectionItem = EZSelectControllerCellModel()
//            selectionItem.title = cardVM.card.pan
//            selectionItem.subTitle = cardVM.balanceStr
//            cardsList.append(selectionItem)
//        }
//        router.openCardSelectionView(from: view, for: cardsList, delegate: self)
    }
}

extension NewPaymentsThirdPagePresenter: NewPaymentsThirdPageInteractorToPresenterProtocol {
    
    
    func didPaymentGetInfo(with response: ResponseResult<PaymentGetInfoResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            print(result)
           self.view.showInfoView(result.data.messages)
            break
        case let .failure(error):
            print(error)
            break
        }
    }
    
    
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool) {
        switch response {
        case let .success(result):
            viewModel.cardsTest = result.data.cards.compactMap(\.card)
            viewModel.cards = result.data.cards

        case let .failure(error):
            view.cardsDownloadPreloader(show: false)
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cardBT = result.data.cards?.map(\.cardBalance) ?? []
            viewModel.cardBalances = result.data.cards ?? []
        case let .failure(error):
            print(error)
//            view.showError(message: error.localizedDescription)
            break
        }
    }
    
    func didPaymentPrepare(with response: ResponseResult<PaymentPrepareResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            router.navigateToPaymentDataPreview(with: view as Any,
                                                service: viewModel.dataEntryFormServiceEntity,
                                                payerCard: viewModel.userEntry.payerCard,
                                                paymentPrepareResponse: result.data)
        case let .failure(error):
//            view.showError(message: error.localizedDescription)
            view.validateFields(show: true)
        }
    }
}

extension NewPaymentsThirdPagePresenter: EZSelectControllerDelegate {
    func numberOfSections(in ezSelectController: EZSelectController) -> Int {
        return 1
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cards.count
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleAndSubtitleForRowAt indexPath: IndexPath) -> (title: String, subtitle: String) {
        let cardVM = viewModel.cardVM(at: indexPath.row)
        return (cardVM.card.pan ?? "", cardVM.balanceStr)
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, didSelectRowAt indexPath: IndexPath) {
        let cardVM = viewModel.cardVM(at: indexPath.row)
        viewModel.userEntry.set(payer: cardVM.card)
        view.showSelectedCard()
        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleForHeaderInSection section: Int) -> String? {
        return RS.lbl_cards.localized()
    }
}

extension NewPaymentsThirdPagePresenter: EZDatePickerControllerDelegate {
    func ezDatePickerDidSelect(date: Date, userInfo: Any?) {
        guard let fieldResponse = userInfo as? ServiceFieldResponse else { return }
        let dateStr = date.string(for: dateFormat)
        viewModel.userEntry.set(fieldValue: dateStr, for: fieldResponse)
        view.showSelectedDate(for: fieldResponse, fieldValue: dateStr)
        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
}

extension NewPaymentsThirdPagePresenter: PaymentDataEntryFormFieldDelegate {
    func myCardFieldDidClick() {
        myCardsClicked()
    }
    
    func moneyFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
        view.nextButton(enable: viewModel.isAllFieldsValid)
        view.callToGetInfo(enable: viewModel.isAllFieldsValid, false)
    }
    
    func datePopupFieldDidClick(field: ServiceFieldResponse) {
        let dateStr = viewModel.userEntry.getFieldValue(for: field) as? String
        if let dateStr = dateStr,
           let date = dateStr.date(fromFormat: dateFormat) {
            view.openDatePicker(with: date, userInfo: field)
            
        } else {
            view.openDatePicker(with: Date(), userInfo: field)
        }
    }
    
    func regexBoxFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
        view.nextButton(enable: viewModel.isAllFieldsValid)
//        view.checkInfo(id: field.serviceID, invoice: value)
    }
    
    func phoneFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func stringFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
        view.nextButton(enable: viewModel.isAllFieldsValid)
        view.callToGetInfo(enable: viewModel.isAllFieldsValid, true)
//        view.checkInfo(id: field.infoServiceId, invoice: value)
    }
}

