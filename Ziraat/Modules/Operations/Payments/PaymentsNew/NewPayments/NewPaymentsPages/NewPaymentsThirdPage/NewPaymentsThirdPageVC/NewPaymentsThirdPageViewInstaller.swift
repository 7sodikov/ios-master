//
//  NewPaymentsThirdPageViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol NewPaymentsThirdPageViewInstaller: ViewInstaller {
//    var cardNumberFieldListener: MaskedTextFieldDelegate! { get set }
//    var cardView: DashboardLoanCalculatorComponentView! { get set }
//    var phoneNumberFieldListener: MaskedTextFieldDelegate! { get set }
//    var phoneNumberView: DashboardLoanCalculatorComponentView! { get set }
//    var amountView: DashboardLoanCalculatorComponentView! { get set }
//    var nextButton: NextButton! { get set }
    var scrollView: UIScrollView!  { get set }
    var formCoverView: UIView! { get set }
    var coverStackView: UIStackView! { get set }
    var titleStackView: UIStackView! { get set }
    var formStackView: UIStackView! { get set }
    var buttonsVerticalStackView: UIStackView! { get set }
    var buttonsStackView: UIStackView! { get set }
    
    var titleLabel: UILabel! { get set }
    var cardFieldView: PaymentDataEntryFormMyCardField! { get set }
    var closeButton: NextButton! { get set }
    var nextButton: NextButton! { get set }
    var msInfoView: MSAdditionalPaymentInfoView! { get set }
}

extension NewPaymentsThirdPageViewInstaller {
    func initSubviews() {
//        let font = UIFont.gothamNarrow(size: 15, .medium)
//
//        cardNumberFieldListener = MaskedTextFieldDelegate()
//        cardNumberFieldListener.affinityCalculationStrategy = .prefix
//        cardNumberFieldListener.primaryMaskFormat = "[0000] [0000] [0000] [0000]"
//
//        cardView = DashboardLoanCalculatorComponentView()
//        cardView.nameLabel.text = RS.lbl_cards.localized()
//        cardView.nameLabel.textColor = .dashDarkBlue
//        cardView.backView.layer.borderWidth = Adaptive.val(2)
//        cardView.backView.layer.borderColor = UIColor.darkGrey.cgColor
//        cardView.balanceTextField.attributedPlaceholder = NSAttributedString(string: "xxxx xxxx xxxx xxxx", attributes: [
//            .foregroundColor: UIColor.black,
//            .font: font
//        ])
//        cardView.balanceTextField.delegate = cardNumberFieldListener
//        cardView.typeButton.isHidden = true
//        cardView.typeImageView.isHidden = true
//
//        phoneNumberFieldListener = MaskedTextFieldDelegate()
//        phoneNumberFieldListener.affinityCalculationStrategy = .prefix
//        phoneNumberFieldListener.primaryMaskFormat = "[00000] [000] [00] [00]"
//
//        phoneNumberView = DashboardLoanCalculatorComponentView()
//        phoneNumberView.nameLabel.text = RS.lbl_phone_number.localized().localized()
//        phoneNumberView.nameLabel.textColor = .dashDarkBlue
//        phoneNumberView.backView.layer.borderWidth = Adaptive.val(2)
//        phoneNumberView.backView.layer.borderColor = UIColor.darkGrey.cgColor
//        phoneNumberView.typeButton.isHidden = true
//        phoneNumberView.typeImageView.isHidden = true
//        phoneNumberView.balanceTextField.delegate = phoneNumberFieldListener
//
//        amountView = DashboardLoanCalculatorComponentView()
//        amountView.nameLabel.text = RS.lbl_amount.localized().localized()
//        amountView.nameLabel.textColor = .dashDarkBlue
//        amountView.backView.layer.borderWidth = Adaptive.val(2)
//        amountView.backView.layer.borderColor = UIColor.darkGrey.cgColor
//        amountView.typeButton.isHidden = true
//        amountView.typeImageView.isHidden = true
//
//        nextButton = NextButton.systemButton(with: RS.btn_next.localized(), cornerRadius: Size.buttonHeight/2)
//        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        
        mainView.backgroundColor = .clear
        
        scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        
        formCoverView = UIView()
        formCoverView.backgroundColor = .clear
        formCoverView.layer.cornerRadius = Adaptive.val(12.5)
        
        coverStackView = UIStackView()
        coverStackView.axis = .vertical
        coverStackView.alignment = .fill
        coverStackView.spacing = Adaptive.val(20)
        coverStackView.layoutMargins = Adaptive.edges(top: 20, left:20, bottom: 20, right: 20)
        coverStackView.isLayoutMarginsRelativeArrangement = true
        
        titleStackView = UIStackView()
        titleStackView.axis = .vertical
        titleStackView.alignment = .center
        
        formStackView = UIStackView()
        formStackView.axis = .vertical
        formStackView.alignment = .fill
        formStackView.spacing = Adaptive.val(10)
        
        buttonsVerticalStackView = UIStackView()
        buttonsVerticalStackView.axis = .vertical
        buttonsVerticalStackView.alignment = .center
        
        buttonsStackView = UIStackView()
        buttonsStackView.axis = .horizontal
        buttonsStackView.alignment = .center
        buttonsStackView.spacing = Adaptive.val(20)
        
        msInfoView = MSAdditionalPaymentInfoView(frame: .zero)
        msInfoView.backgroundColor = .white
        msInfoView.layer.cornerRadius = 10
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(18))
        titleLabel.textColor = .black
        
        cardFieldView = PaymentDataEntryFormMyCardField(frame: .zero)
        cardFieldView.setup(title: RS.lbl_cards.localized(), value: RS.lbl_cards.localized(), isRequired: true)
        
        closeButton = NextButton.systemButton(with: RS.btn_close.localized().uppercased(), cornerRadius: 25)
        closeButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        closeButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        closeButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: Size.buttonHeight/2)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        nextButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        nextButton.isEnabled = true
    }
    
    func embedSubviews() {
//        mainView.addSubview(cardView)
//        mainView.addSubview(phoneNumberView)
//        mainView.addSubview(amountView)
//        mainView.addSubview(nextButton)

        mainView.addSubview(scrollView)
        scrollView.addSubview(formCoverView)
        scrollView.addSubview(coverStackView)
//        coverStackView.addArrangedSubview(titleStackView)
        coverStackView.addArrangedSubview(formStackView)
        coverStackView.addArrangedSubview(buttonsVerticalStackView)
        coverStackView.addArrangedSubview(msInfoView)
        buttonsVerticalStackView.addArrangedSubview(buttonsStackView)
        
//        titleStackView.addArrangedSubview(titleLabel)
        formStackView.addArrangedSubview(cardFieldView)
//        buttonsStackView.addArrangedSubview(closeButton)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
//        cardView.snp.remakeConstraints { (maker) in
//            maker.top.equalToSuperview().offset(Adaptive.val(30))
//            maker.leading.trailing.equalToSuperview()
//        }
//
//        phoneNumberView.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(cardView.snp.bottom).offset(Adaptive.val(10))
//            maker.leading.trailing.equalToSuperview()
//        }
//
//        amountView.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(phoneNumberView.snp.bottom).offset(Adaptive.val(10))
//            maker.leading.trailing.equalToSuperview()
//        }
//
//        nextButton.snp.remakeConstraints { (maker) in
//            maker.bottom.equalToSuperview().offset(-Adaptive.val(57))
//            maker.leading.equalToSuperview().offset(Adaptive.val(16))
//            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
//            maker.height.equalTo(Adaptive.val(50))
//        }
        
        scrollView.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalTo(0)
            maker.bottom.equalTo(nextButton.snp.top).offset(-Adaptive.val(15))
        }
        
        formCoverView.snp.remakeConstraints { (maker) in
//            maker.left.equalTo(PaymentDataEntryFormSize.FormCover.leftRightPadding)
//            maker.right.equalTo(-PaymentDataEntryFormSize.FormCover.leftRightPadding)
            maker.left.right.equalTo(self.mainView)
            maker.width.top.bottom.equalTo(self.scrollView)
        }
        
        coverStackView.snp.remakeConstraints { (maker) in
            maker.top.bottom.equalToSuperview()
            maker.leading.trailing.equalTo(formCoverView)
        }
        
        msInfoView.snp.remakeConstraints { make in
            make.height.equalTo(200)
        }
        
//        closeButton.snp.remakeConstraints { (maker) in
//            maker.width.equalTo(PaymentDataEntryFormSize.Button.size.width)
//            maker.height.equalTo(PaymentDataEntryFormSize.Button.size.height)
//        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(formCoverView).offset(Adaptive.val(16))
            maker.trailing.equalTo(formCoverView).offset(-Adaptive.val(16))
            maker.height.equalTo(Size.buttonHeight)
            maker.bottom.equalToSuperview().offset(-Adaptive.val(57))
        }
        msInfoView.hide()
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(50)
}
