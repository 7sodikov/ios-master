//
//  NewPaymentsThirdPageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

protocol NewPaymentsThirdPageVCProtocol: BaseViewControllerProtocol {
    func cardsDownloadPreloader(show: Bool)
    func showSelectedCard()
    func showSelectedDate(for fieldResponse: ServiceFieldResponse, fieldValue: String?)
    func nextButton(enable: Bool)
    func callToGetInfo(enable: Bool, _ force: Bool)
    func openDatePicker(with selectedDate: Date, userInfo: Any)
    func setCardNumber(number: String)
    func drawView()
    func clearStackView()
    func showInfoView(_ data: [ObjectKeyValueModel]?)
    func validateFields(show: Bool)
}

enum NPOptonEnum: Int {
    case region
    case district
    case person
}

class NewPaymentsThirdPageVC: BaseViewController, NewPaymentsThirdPageViewInstaller {
    
    var mainView: UIView { view }
//    var cardNumberFieldListener: MaskedTextFieldDelegate!
//    var cardView: DashboardLoanCalculatorComponentView!
//    var phoneNumberFieldListener: MaskedTextFieldDelegate!
//    var phoneNumberView: DashboardLoanCalculatorComponentView!
//    var amountView: DashboardLoanCalculatorComponentView!
//    var nextButton: NextButton!
    var scrollView: UIScrollView!
    var formCoverView: UIView!
    var coverStackView: UIStackView!
    var titleStackView: UIStackView!
    var formStackView: UIStackView!
    var buttonsVerticalStackView: UIStackView!
    var buttonsStackView: UIStackView!
    var titleLabel: UILabel!
    var msInfoView: MSAdditionalPaymentInfoView!
    var cardFieldView: PaymentDataEntryFormMyCardField!
    var closeButton: NextButton!
    var nextButton: NextButton!
    
    var presenter: NewPaymentsThirdPagePresenterProtocol!
    
    var showErrorLabel: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        presenter.viewDidLoad()

//        cardNumberFieldListener.listener = self
//        phoneNumberFieldListener.listener = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func clearStackView() {
        formStackView.arrangedSubviews.forEach { view in
            if view != cardFieldView {
                formStackView.removeArrangedSubview(view)
                view.removeFromSuperview()
            }
        }
    }
    
    func drawView() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = true
        Utils.delay(seconds: 0.3) {
            UIView.animate(withDuration: 0.3) {
                self.navigationController?.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
        }
        
        self.title = presenter.viewModel.dataEntryFormServiceEntity.title
        
        cardFieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
        // showing initial value
        cardFieldView.textField.text = presenter.viewModel.userEntry.payerCard.selectedPayerNumber
        
        for field in presenter.viewModel.dataEntryFormServiceEntity.fields {
            switch field.enumType {
            case .money:
                let fieldView = PaymentDataEntryFormMoneyField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                if let value = presenter.viewModel.userEntry.fields[field.name] as? String, !value.isEmpty {
                    fieldView.textField.text = value
                }
                formStackView.addArrangedSubview(fieldView)
            case .datePopup:
                let fieldView = PaymentDataEntryFormDatePopupField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                if let value = presenter.viewModel.userEntry.fields[field.name] as? String, !value.isEmpty {
                    fieldView.textField.text = value
                }
                formStackView.addArrangedSubview(fieldView)
            case .regexBox:
                let fieldView = PaymentDataEntryFormRegexBoxField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                if let value = presenter.viewModel.userEntry.fields[field.name] as? String, !value.isEmpty {
                    fieldView.textField.text = value
                }
                formStackView.addArrangedSubview(fieldView)
            case .phone:
                let fieldView = PaymentDataEntryFormPhoneField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                if let value = presenter.viewModel.userEntry.fields[field.name] as? String, !value.isEmpty {
                    fieldView.textField.text = value
                }
                formStackView.addArrangedSubview(fieldView)
            case .string:
                let fieldView = PaymentDataEntryFormStringField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                if let value = presenter.viewModel.userEntry.fields[field.name] as? String, !value.isEmpty {
                    fieldView.textField.text = value
                }
                formStackView.addArrangedSubview(fieldView)
            case .select:
                let entity = presenter.viewModel.dataEntryFormServiceEntity
                if !field.regions.isEmpty {
                    let regionOptionView = OptionView()
                    regionOptionView.headerLabel.text = field.valueTitles?[0]
                    regionOptionView.textLabel.text = entity.selectedRegionName
                    regionOptionView.tag = NPOptonEnum.region.rawValue
                    regionOptionView.didTap = didTap(_:)
                    formStackView.insertArrangedSubview(regionOptionView, at: 1)
                    
                    let districtOptionView = OptionView()
                    districtOptionView.headerLabel.text = field.valueTitles?[1]
                    districtOptionView.textLabel.text = entity.selectedDistrictName
                    districtOptionView.tag = NPOptonEnum.district.rawValue
                    districtOptionView.didTap = didTap(_:)
                    formStackView.insertArrangedSubview(districtOptionView, at: 2)
                }
                if !field.fieldSections.isEmpty {
                    let optionView = OptionView()
                    optionView.tag = NPOptonEnum.person.rawValue
                    optionView.didTap = didTap(_:)
                    formStackView.insertArrangedSubview(optionView, at: 3)
                }
            case .none:
                break
            }
        }
        
        closeButton.addTarget(self, action: #selector(closeBtnClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
    }
    
    @objc func didTap(_ tag: Int) {
        guard let optionType = NPOptonEnum(rawValue: tag) else { return }
        presenter.openBottomSheet(option: optionType)
    }
    
    func setCardNumber(number: String) {
        cardFieldView.textField.text = number
    }
    
    private func drawViewRepayment() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = true
        Utils.delay(seconds: 0.3) {
            UIView.animate(withDuration: 0.3) {
                self.navigationController?.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
        }
        
        titleLabel.text = presenter.viewModel.dataEntryFormServiceEntity.title
        
        cardFieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
        // showing initial value
        cardFieldView.textField.text = presenter.viewModel.userEntry.payerCard.selectedPayerNumber
        
        for field in presenter.viewModel.dataEntryFormServiceEntity.fields {
            switch field.enumType {
            case .money:
                let fieldView = PaymentDataEntryFormMoneyField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                fieldView.errorLabel.isHidden = false //showErrorLabel ?? true
                formStackView.addArrangedSubview(fieldView)
            case .datePopup:
                let fieldView = PaymentDataEntryFormDatePopupField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                fieldView.errorLabel.isHidden = false //showErrorLabel ?? true
                formStackView.addArrangedSubview(fieldView)
//            case .regexBox:
//                let fieldView = PaymentDataEntryFormRegexBoxField(frame: .zero)
//                fieldView.setup(for: field)
//                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
//                fieldView.errorLabel.isHidden = false //showErrorLabel ?? true
//                formStackView.addArrangedSubview(fieldView)
            case .phone:
                let fieldView = PaymentDataEntryFormPhoneField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                fieldView.errorLabel.isHidden = false //showErrorLabel ?? true
                formStackView.addArrangedSubview(fieldView)
            case .string, .regexBox :
                let fieldView = PaymentDataEntryFormStringField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? NewPaymentsThirdPagePresenter)
                fieldView.errorLabel.isHidden = false //showErrorLabel ?? true
                formStackView.addArrangedSubview(fieldView)
                
            case .select:
                let entity = presenter.viewModel.dataEntryFormServiceEntity
                if !field.regions.isEmpty {
                    let regionOptionView = OptionView()
                    regionOptionView.headerLabel.text = field.valueTitles?[0]
                    regionOptionView.textLabel.text = entity.selectedRegionName
                    regionOptionView.tag = NPOptonEnum.region.rawValue
                    regionOptionView.didTap = didTap(_:)
                    formStackView.insertArrangedSubview(regionOptionView, at: 0)
                    
                    let districtOptionView = OptionView()
                    districtOptionView.headerLabel.text = field.valueTitles?[1]
                    districtOptionView.textLabel.text = entity.selectedDistrictName
                    districtOptionView.tag = NPOptonEnum.district.rawValue
                    districtOptionView.didTap = didTap(_:)
                    formStackView.insertArrangedSubview(districtOptionView, at: 1)
                }
                if !field.fieldSections.isEmpty {
                    let optionView = OptionView()
                    optionView.tag = NPOptonEnum.person.rawValue
                    optionView.didTap = didTap(_:)
                    formStackView.insertArrangedSubview(optionView, at: 2)
                }
            case .none:
                break
            }
        }
        
        closeButton.addTarget(self, action: #selector(closeBtnClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
        closeButton.isEnabled = !show
    }
    
    @objc private func closeBtnClicked(_ sender: NextButton) {
        presenter.closeBtnClicked()
    }
    
    @objc private func nextBtnClicked(_ sender: NextButton) {
        presenter.nextBtnClicked()
    }
}

extension NewPaymentsThirdPageVC: NewPaymentsThirdPageVCProtocol, MaskedTextFieldDelegateListener {
    
    func callToGetInfo(enable: Bool, _ force: Bool) {
        if force || (enable && self.msInfoView.isClosed) {
            presenter.getServiceInfo(id: 0, invoice: "")
        } 
    }

    func showInfoView(_ data: [ObjectKeyValueModel]?) {
        if let array = data {
            self.msInfoView.data = array
            self.msInfoView.show()
        } else {
            self.msInfoView.hide()
        }
    }
    
    func checkInfo(id: Int?, invoice: String?) {
        
    }
    
    func cardsDownloadPreloader(show: Bool) {
        cardFieldView.animateIndicator = show
    }
    
    func showSelectedCard() {
        cardFieldView.textField.text = presenter.viewModel.userEntry.payerCard.selectedPayerNumber
    }
    
    func showSelectedDate(for fieldResponse: ServiceFieldResponse, fieldValue: String?) {
        for fieldView in formStackView.arrangedSubviews {
            guard let datePopupField = fieldView as? PaymentDataEntryFormDatePopupField else { continue }
            if datePopupField.fieldResponse.name == fieldResponse.name {
                datePopupField.textField.text = fieldValue
                return
            } else {
                continue
            }
        }
    }
    
    func nextButton(enable: Bool) {
        nextButton.isEnabled = enable
    }
    
    func openDatePicker(with selectedDate: Date, userInfo: Any) {
        let datePicker = EZDatePickerController()
        datePicker.delegate = presenter as? NewPaymentsThirdPagePresenter
        datePicker.userInfo = userInfo
        datePicker.open(in: self, selectedDate: selectedDate)
    }
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
//        if textField == cardView.balanceTextField {
//            presenter.viewModel.userEntry.cardNumber = value
//        } else if textField == phoneNumberView.balanceTextField {
//            presenter.viewModel.userEntry.phoneNumber = value
//        }
    }
    
    func validateFields(show: Bool) {
        showErrorLabel = !show
//        print("hello")
    }
}
