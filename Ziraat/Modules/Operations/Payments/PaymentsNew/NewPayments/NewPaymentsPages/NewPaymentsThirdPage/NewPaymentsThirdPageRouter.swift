//
//  NewPaymentsThirdPageRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsThirdPageRouterProtocol: AnyObject {
    static func createModule(with delegate: NewPaymentsPresenter?, favoriteItem: FavoriteListPaymentsResponse?, payment: CLastPayment?) -> UIViewController
    func navigateToConfirmPage(for service: ServiceResponse, navCtrl: Any)
    func dismiss(view: Any?)
    func openCardSelectionView(from controller: Any?,
                               for cardsList: [EZSelectControllerCellModel],
                               delegate: EZSelectControllerDelegate)
    func navigateToPaymentDataPreview(with view: Any,
                                      service: PaymentDataEntryFormServiceEntity,
                                      payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                      paymentPrepareResponse: PaymentPrepareResponse)
}

class NewPaymentsThirdPageRouter: NewPaymentsThirdPageRouterProtocol {
    static func createModule(with delegate: NewPaymentsPresenter?, favoriteItem: FavoriteListPaymentsResponse?, payment: CLastPayment?) -> UIViewController {
        let vc = NewPaymentsThirdPageVC()
        let presenter = NewPaymentsThirdPagePresenter()
        let interactor = NewPaymentsThirdPageInteractor()
        let router = NewPaymentsThirdPageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.repeatPayment = payment
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToConfirmPage(for service: ServiceResponse, navCtrl: Any) {
        let viewCtrl = navCtrl as! UIViewController
        let legalWarningVC = PaymentDataEntryFormRouter.createModule(for: service, favoriteItem: nil, payment: nil)
        viewCtrl.navigationController?.pushViewController(legalWarningVC, animated: true)
    }
    
    func dismiss(view: Any?) {
        let controller = view as? UIViewController
        
        UIView.animate(withDuration: defaultAnimationDuration, animations: {
            controller?.navigationController?.view.backgroundColor = UIColor.clear
        }) { (isCompleted) in
            controller?.dismiss(animated: true, completion: nil)
        }
    }
    
    func openCardSelectionView(from controller: Any?,
                               for cardsList: [EZSelectControllerCellModel],
                               delegate: EZSelectControllerDelegate) {
//        let selectionCtrl = BottomSheetViewController(bottomSheet: <#T##BottomSheet#>)//EZSelectController()
//        selectionCtrl.delegate = delegate
////        let navCtrl = selectionCtrl.wrapIntoNavigation()
//        (controller as? UIViewController)?.present(selectionCtrl, animated: true, completion: nil)
    }
    
    func navigateToPaymentDataPreview(with view: Any,
                                      service: PaymentDataEntryFormServiceEntity,
                                      payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                      paymentPrepareResponse: PaymentPrepareResponse) {
        let viewCtrl = view as! UIViewController
        let previewCtrl = PaymentDataPreviewFormRouter.createModule(with: service,
                                                                    payerCard: payerCard,
                                                                    paymentPrepareResponse: paymentPrepareResponse)
//        (view as? PaymentDataEntryFormVC)?.navigationController?.pushViewControllerRetro(previewCtrl)
        viewCtrl.navigationController?.pushViewController(previewCtrl, animated: true)

    }
}
