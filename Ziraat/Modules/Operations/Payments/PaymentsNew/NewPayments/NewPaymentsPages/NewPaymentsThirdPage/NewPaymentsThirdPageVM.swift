//
//  NewPaymentsThirdPageVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class NewPaymentsThirdPageVM {
    let userEntry = NewPaymentsThirdPageUserEntry()
    var service: ServiceResponse? {
        didSet {
            fillServiceEntityFromServiceResponse()
        }
    }
    
    var repeatPayment: CLastPayment? {
        didSet {
            fillRepayFromServiceResponse()
        }
    }
    
    var favoriteItem: FavoriteListPaymentsResponse? {
        didSet {
            fillServiceEntityFromFavoriteResponse()
        }
    }
    
    var dataEntryFormServiceEntity = PaymentDataEntryFormServiceEntity()
    
    var dataEntryFromRepayment = PaymentDataEntryFormRepayment()
    
    var cardsTest: [Card] = []
    var cardBT: [CardBalance] = []
    var cards: [CardResponse] = []
    var cardBalances: [CardBalanceResponse] = []
    
    var caardAccountBottomSheet: BottomSheet {
        let vms = cardsTest.map { CardCellViewModel(card: $0) }
        let rows = [CardsRouletteModel(header: RS.lbl_cards.localized(), viewModels: vms)]
        let cardSection = BottomSheetSection(header: "", rows: rows)
        
        return .init(viewHeight: 180, dataSource: [cardSection], with: 0)
    }
    
    func updateCard() {
        
        cardsTest = cardsTest.map { item -> Card in
            var card = item
            if let cardBalance = cardBT.first(where: { $0.cardId.description == card.cardId }) {
                card.update(balance: cardBalance.balance, currency: cardBalance.currency)
            }
            return card
        }
    }
    
    private func fillServiceEntityFromServiceResponse() {
        guard let service = service else { return }
        
        dataEntryFormServiceEntity.maxAmount = service.maxAmount
        dataEntryFormServiceEntity.minAmount = service.minAmount
        dataEntryFormServiceEntity.serviceType = service.serviceType
        dataEntryFormServiceEntity.title = service.title
        dataEntryFormServiceEntity.logoUrl = service.logoURL
        dataEntryFormServiceEntity.serviceId = service.serviceID
        dataEntryFormServiceEntity.fields = service.fields
        dataEntryFormServiceEntity.setdefaults()
    }
    
    private func fillRepayFromServiceResponse() {
        guard let repaymentItem = repeatPayment else { return }
        
//        dataEntryFromRepayment.serviceId = repaymentItem.serviceId
//        dataEntryFromRepayment.operationId = repaymentItem.operationId
//        dataEntryFromRepayment.currency = repaymentItem.currency
//        dataEntryFromRepayment.icon = repaymentItem.icon
//        dataEntryFromRepayment.fieldValueList = repaymentItem.fieldValueList
//        dataEntryFromRepayment.operationType = repaymentItem.operationType
//        dataEntryFromRepayment.amount = repaymentItem.amount
//        dataEntryFromRepayment.operationMode = repaymentItem.operationMode
//        dataEntryFromRepayment.date = repaymentItem.date
//        dataEntryFromRepayment.receiverOwner = repaymentItem.receiverOwner
//        dataEntryFromRepayment.time = repaymentItem.time
//        dataEntryFromRepayment.receiverBranch = repaymentItem.receiverBranch
//        dataEntryFromRepayment.total = repaymentItem.total
//        dataEntryFromRepayment.operationCode = repaymentItem.operationCode
//        dataEntryFromRepayment.senderPan = repaymentItem.senderPan
//        dataEntryFromRepayment.status = repaymentItem.status
//        dataEntryFromRepayment.serviceType = repaymentItem.serviceType
//        dataEntryFromRepayment.commission = repaymentItem.commission
        
        dataEntryFormServiceEntity.serviceType = repaymentItem.serviceType
        dataEntryFormServiceEntity.title = repaymentItem.receiverOwner
        dataEntryFormServiceEntity.logoUrl = repaymentItem.icon
        if let serviceId = Int64(repaymentItem.serviceId) {
            dataEntryFormServiceEntity.serviceId = Int(serviceId)
        }
        dataEntryFormServiceEntity.fields = repaymentItem.fieldValueList
        
        userEntry.payerCard.selectedPayerNumber = repaymentItem.sender
        userEntry.payerCard.payerCardId = repaymentItem.senderId
        userEntry.payerCard.payerCardOwner = repaymentItem.senderOwner
        
//        for fieldValue in repaymentItem.fieldValueList {
//            if fieldValue.type == ServiceFieldType.money.rawValue {
//                guard let strValue = fieldValue,
//                      let numberValue = UInt64(strValue) else {
//                    continue
//                }
//                let money = numberValue / 100
//                userEntry.set(fieldValue: String(money, for: fieldValue)
//            } else {
//                userEntry.set(fieldValue: fieldValue.defaultValue, for: fieldValue)
//            }
//        }
    }
    
    private func fillServiceEntityFromFavoriteResponse() {
        guard let favoriteItem = favoriteItem else { return }
        
//        dataEntryFormServiceEntity.maxAmount =
//        dataEntryFormServiceEntity.minAmount =
        dataEntryFormServiceEntity.serviceType = favoriteItem.serviceType
        dataEntryFormServiceEntity.title = favoriteItem.serviceTitle
        dataEntryFormServiceEntity.logoUrl = favoriteItem.logo
        if let serviceId = Int(favoriteItem.serviceID ?? "") {
            dataEntryFormServiceEntity.serviceId = serviceId
        }
        dataEntryFormServiceEntity.fields = favoriteItem.fieldValueList
        
        userEntry.payerCard.selectedPayerNumber = favoriteItem.sender
        userEntry.payerCard.payerCardId = UInt64(favoriteItem.senderID)
        userEntry.payerCard.payerCardOwner = favoriteItem.senderOwner
        
//        for fieldValue in favoriteItem.fieldValueList {
//            if fieldValue.type == ServiceFieldType.money.rawValue {
//                guard let strValue = fieldValue.defaultValue,
//                      let numberValue = UInt64(strValue) else {
//                    continue
//                }
//                let money = numberValue/100
//                userEntry.set(fieldValue: String(money), for: fieldValue)
//            } else {
//                userEntry.set(fieldValue: fieldValue.defaultValue, for: fieldValue)
//            }
//        }
    }
    
    func cardVM(at index: Int) -> DashboardCardVM {
        var balance: CardBalanceResponse?
        let card = cards[index]
        for cardBalance in cardBalances {
            if cardBalance.cardId == card.cardId {
                balance = cardBalance
                break
            }
        }
        
        return DashboardCardVM(card, balance)
    }
    
    var isAllFieldsValid: Bool {
        var isValid = true
        for i in 0..<dataEntryFormServiceEntity.fields.count {
            let field = dataEntryFormServiceEntity.fields[i]
            let fieldType = ServiceFieldType.getType(for: field.type)
            
            if let fieldValue = userEntry.fields[field.name] {
                if fieldType == .money {
                    guard let amountValue = fieldValue as? Double else {
                        isValid = true //false
                        break
                    }
                    
                    if amountValue < 100000 {
                        isValid = true //false
                        break
                    }
                }
                
                if i == dataEntryFormServiceEntity.fields.count - 1 { // last element
                    isValid = true
                    break
                }
                continue
            }
            isValid = true //false
            break
        }
        
        if userEntry.payerCard.payerCardId == nil ||
            userEntry.payerCard.payerCardId == 0 {
            isValid = false
        }
        
        return isValid
    }
}

class NewPaymentsThirdPageUserEntry {
    var payerCard = OperationPagePA2PDataEntryUserEntryPayer()
    private(set) var fields: [String: Any] = [:]
    
    func clear() {
        fields.removeAll()
    }
    
    func set(payer: CardResponse) {
        payerCard.selectedPayerNumber = payer.pan
        payerCard.payerCardId = payer.cardId
        payerCard.payerCardOwner = payer.owner
    }
    
    func setPayer(cardId: String, pan: String, owner: String) {
        payerCard.selectedPayerNumber = pan
        payerCard.payerCardId = UInt64(cardId)
        payerCard.payerCardOwner = owner
    }
    
    func set(fieldValue: String?, for fieldResponse: ServiceFieldResponse) {
        switch fieldResponse.enumType {
        case .money:
            guard let fieldValue = fieldValue, !fieldValue.isEmpty else {
                fields[fieldResponse.name] = nil
                return
            }
            guard let doubleValue = Double(fieldValue) else {
                fields[fieldResponse.name] = nil
                return
            }
            fields[fieldResponse.name] = doubleValue * 100
            
        case .datePopup:
            fields[fieldResponse.name] = fieldValue
        case .regexBox:
            fields[fieldResponse.name] = fieldValue
        case .phone:
            guard let fieldValue = fieldValue, !fieldValue.isEmpty else {
                fields[fieldResponse.name] = nil
                return
            }
            fields[fieldResponse.name] = fieldValue
        case .string:
            fields[fieldResponse.name] = fieldValue
        case .select:
            fields[fieldResponse.name] = fieldValue
        default:
            break
        }
    }
    
    func getFieldValue(for fieldResponse: ServiceFieldResponse) -> Any? {
        return fields[fieldResponse.name]
    }

    var cardNumber: String?
    var phoneNumber: String?
    var amount: String?
}
