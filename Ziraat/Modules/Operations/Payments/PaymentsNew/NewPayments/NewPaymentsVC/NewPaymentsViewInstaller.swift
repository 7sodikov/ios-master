//
//  NewPaymentsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsViewInstaller: ViewInstaller {
    var backgroundImageView: UIImageView! { get set }
    var backButton: UIButton! { get set }
    var forgotPasswordLabel: UILabel! { get set }
    var pageControl: ForgotPasswordPageControl! { get set }
    var pageControllerCoverView: UIView! { get set }
    var pageController: PageController! { get set }
}

extension NewPaymentsViewInstaller {
    func initSubviews() {
        backgroundImageView = UIImageView()
        backgroundImageView.image = UIImage(named: "img_dash_light_background")
    
        backButton = UIButton()
        backButton.setImage(UIImage(named: "btn_navigation_back"), for: .normal)
        
        pageControl = ForgotPasswordPageControl()
        pageControl.isBlack = false
//        pageControl.layer.borderWidth = 1
//        pageControl.layer.borderColor = UIColor.black.cgColor
        
        pageControllerCoverView = UIView()
        
        pageController = PageController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImageView)
        mainView.addSubview(backButton)
        mainView.addSubview(pageControl)
        mainView.addSubview(pageControllerCoverView)
    }
    
    func addSubviewsConstraints() {
        backgroundImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        backButton.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(51))
            maker.leading.equalToSuperview().offset(Adaptive.val(27))
        }
        
        pageControl.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(89))
            maker.leading.equalTo(ForgotPasswordSize.padding)
            maker.trailing.equalTo(-ForgotPasswordSize.padding)
            maker.height.equalTo(ForgotPasswordSize.PageControl.height)
        }
        
        pageControllerCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(pageControl.snp.bottom).offset(ForgotPasswordSize.interItemSpace)
            maker.leading.trailing.bottom.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
