//
//  NewPaymentsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

enum NewPaymentsCellType: Int {
    case categories
    case list
    case payment
}

protocol NewPaymentsVCProtocol: BaseViewControllerProtocol {
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection)
    func getController(for pageIndex: Int) -> Any?
    func setTitle(title: String)
}

class NewPaymentsVC: BaseViewController, NewPaymentsViewInstaller {
    var mainView: UIView { view }
    var backgroundImageView: UIImageView!
    var backButton: UIButton!
    var forgotPasswordLabel: UILabel!
    var pageControl: ForgotPasswordPageControl!
    var pageControllerCoverView: UIView!
    var pageController: PageController!
    
    var controllers: [UIViewController] = []
        
    var cellTypes: [NewPaymentsCellType] = [.categories, .list,
                                               .payment]
    
    var presenter: NewPaymentsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        
        pageControl.itemsCount = cellTypes.count
        
        if presenter.viewModel.repayment != nil {
            let nextPage = getController(for: 2) as? NewPaymentsThirdPageVC
            let nextPagePresenter = nextPage?.presenter as? NewPaymentsThirdPagePresenter
            nextPagePresenter?.update(with: nil, repayment: presenter.viewModel.repayment)
            scroll(to: 2, direction: .forward)
        }
                
        controllers.append(contentsOf: [NewPaymentsFirstPageRouter.createModule(with: presenter as? NewPaymentsPresenter),
                                        NewPaymentsSecondPageRouter.createModule(with: presenter as? NewPaymentsPresenter),
//                                        NewPaymentsThirdPageRouter.createModule(with: presenter as? NewPaymentsPresenter)],
                                        NewPaymentsThirdPageRouter.createModule(with: presenter as? NewPaymentsPresenter, favoriteItem: nil, payment: nil)])
        
        pageController.open(in: self,
                            parentView: pageControllerCoverView,
                            pages: controllers, isSwipeEnabled: false)
        pageController.pageControllerDelegate = self
                
        backButton.addTarget(self, action: #selector(settingBackButton(_:)), for: .touchUpInside)

    }
    
    @objc func settingBackButton(_ sender: UIButton) {
        if pageControl.currentItem == 0 {
            self.navigationController?.popViewController(animated: true)
        } else if pageControl.currentItem == 1 {
            pageController.setViewControllers([controllers[0]], direction: .reverse, animated: true) { _ in
                self.pageControl.currentItem = 0
                let prevPage = self.getController(for: 0) as? NewPaymentsFirstPageVC
                let prevPagePresenter = prevPage?.presenter as? NewPaymentsFirstPagePresenter                
//                prevPagePresenter?.na
            }
        } else {
            pageController.setViewControllers([controllers[1]], direction: .reverse, animated: true) { _ in
                self.pageControl.currentItem = 1
                let prevPage = self.getController(for: 2) as? NewPaymentsThirdPageVC
                prevPage?.clearStackView()
            }
        }
    }
    
    @objc func scrollBack() {
        scroll(to: 1, direction: .reverse)
    }
    
    @objc func scrollBackSecond() {
        scroll(to: 2, direction: .reverse)
    }
    
    func setupNavigation() {
        self.title = RS.lbl_payments.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
//        self.navigationController?.view.addSubview(backButton)
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NewPaymentsVC: NewPaymentsVCProtocol, PageControllerDelegate {
    
    func setTitle(title: String) {
        self.title = title
    }
    
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection) {
        pageControl.currentItem = pageIndex
        pageController.scroll(to: pageIndex, direction: direction)
    }
    
    func getController(for pageIndex: Int) -> Any? {
        if pageIndex >= controllers.count {
            return nil
        }
        return controllers[pageIndex]
    }
    
    func pageController(didScrollToOffsetX offsetX: CGFloat, percent: CGFloat) {
        
    }
    
    func pageController(didMoveToIndex index: Int) {
        pageControl.currentItem = index
    }
    
    fileprivate func updateBackButtonDisplay(animated: Bool) {

        func setBackButtonAlpha(_ alpha: CGFloat) {
            guard animated else {
                self.backButton.alpha = alpha

                return
            }

            UIView.animate(withDuration: 0.3) {
                self.backButton.alpha = alpha
            }
        }
    }
}
