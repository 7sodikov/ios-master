//
//  NewPaymentsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: NewPaymentsVM { get }
}

protocol NewPaymentsInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    
}

class NewPaymentsPresenter: NewPaymentsPresenterProtocol {
    weak var view: NewPaymentsVCProtocol!
    var interactor: NewPaymentsInteractorProtocol!
    var router: NewPaymentsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = NewPaymentsVM()
    
    // Private property and methods
    
}

extension NewPaymentsPresenter: NewPaymentsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}

extension NewPaymentsPresenter: NewPaymentsFirstPageDelegate {
    func newPaymentFirstPageReturn() {
        
    }
    
    func newPaymentsFirstPageFinished(with categoryId: Int?) {
//        viewModel.loginInitData = loginInitData
        let nextPage = view.getController(for: 1) as? NewPaymentsSecondPageVC
        let nextPagePresenter = nextPage?.presenter as? NewPaymentsSecondPagePresenter
        nextPagePresenter?.categoryId = categoryId
//        nextPagePresenter?.viewDidLoad()
//        nextPagePresenter?.loginInitData = loginInitData
        view.scroll(to: 1, direction: .forward)
    }
}

extension NewPaymentsPresenter: NewPaymentsSecondPageDelegate {
    func newPaymentsSecondPageFinished(for service: ServiceResponse) {
        let nextPage = view.getController(for: 2) as? NewPaymentsThirdPageVC
        let nextPagePresenter = nextPage?.presenter as? NewPaymentsThirdPagePresenter
        nextPagePresenter?.update(with: service, repayment: nil)
        view.scroll(to: 2, direction: .forward)
        view.setTitle(title: service.title)
    }
    
}
