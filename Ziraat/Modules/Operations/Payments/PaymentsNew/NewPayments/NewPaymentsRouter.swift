//
//  NewPaymentsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsRouterProtocol: class {
    static func createModule(with repayment: CLastPayment?) -> UIViewController
}

class NewPaymentsRouter: NewPaymentsRouterProtocol {
    static func createModule(with repayment: CLastPayment?) -> UIViewController {
        let vc = NewPaymentsVC()
        let presenter = NewPaymentsPresenter()
        let interactor = NewPaymentsInteractor()
        let router = NewPaymentsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
