//
//  NewPaymentsSmsConfirmationVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsSmsConfirmationVCProtocol: BaseViewControllerProtocol {
    func nextButton(enabled: Bool)
    func set(nextButtonTitle: String)
    func startTimer()
    func stopTimer()
}

class NewPaymentsSmsConfirmationVC: BaseViewController, NewPaymentsSmsConfirmationViewInstaller {
    var mainView: UIView { view }
    var backView: UIImageView!
    var formCoverView: UIView!
    var coverStackView: UIStackView!
    var titleStackView: UIStackView!
    var formStackView: UIStackView!
    var timerStackView: UIStackView!
    var buttonsVerticalStackView: UIStackView!
    var buttonsStackView: UIStackView!
    var eyeButton: UIButton!
    var titleLabel: UILabel!
    var smsCodeTextField: SMSConfirmationTextField!
    var timerView: TimerView!
    var backButton: NextButton!
    var nextButton: NextButton!
    
    var presenter: NewPaymentsSmsConfirmationPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
//        navigationItem.leftBarButtonItem = nil
//        navigationItem.hidesBackButton = true
        
        titleLabel.text = presenter.viewModel.paymentPayResponse.message
        smsCodeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        timerView.startTimer(with: timerValue)
        timerView.timerReachedItsEnd = {
            self.presenter.timerReachedItsEnd()
        }
        backButton.addTarget(self, action: #selector(backBtnClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
        eyeButton.addTarget(self, action: #selector(textFieldEyeButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc private func textFieldEyeButtonClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.tag == 1 {
            smsCodeTextField.isSecureTextEntry = !sender.isSelected
        }
    }
    
    func setupNavigation() {
        self.title = RS.lbl_payments.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
        backButton.isEnabled = !show
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        presenter.textFieldDidChange(with: sender.text)
    }
    
    @objc private func backBtnClicked(_ sender: NextButton) {
        stopTimer()
//        navigationController?.popViewControllerRetro()
        UIView.animate(withDuration: 0.3, animations: {
            self.navigationController?.view.backgroundColor = UIColor.clear
        }) { (isCompleted) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func nextBtnClicked(_ sender: NextButton) {
        presenter.nextBtnClicked()
    }
}

extension NewPaymentsSmsConfirmationVC: NewPaymentsSmsConfirmationVCProtocol {
    func nextButton(enabled: Bool) {
        nextButton.isEnabled = enabled
    }
    
    func set(nextButtonTitle: String) {
        nextButton.setTitle(nextButtonTitle, for: .normal)
    }
    
    func startTimer() {
        timerView?.startTimer(with: timerValue)
    }
    
    func stopTimer() {
        timerView.stopTimer()
    }
}
