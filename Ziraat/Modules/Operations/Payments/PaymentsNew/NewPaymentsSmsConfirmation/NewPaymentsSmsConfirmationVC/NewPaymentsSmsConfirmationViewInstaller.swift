//
//  NewPaymentsSmsConfirmationViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsSmsConfirmationViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var formCoverView: UIView! { get set }
    var coverStackView: UIStackView! { get set }
    var titleStackView: UIStackView! { get set }
    var formStackView: UIStackView! { get set }
    var timerStackView: UIStackView! { get set }
    var buttonsVerticalStackView: UIStackView! { get set }
    var buttonsStackView: UIStackView! { get set }
    var eyeButton: UIButton! { get set }
    var titleLabel: UILabel! { get set }
    var smsCodeTextField: SMSConfirmationTextField! { get set }
    var timerView: TimerView! { get set }
    var backButton: NextButton! { get set }
    var nextButton: NextButton! { get set }}

extension NewPaymentsSmsConfirmationViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
        formCoverView = UIView()
        formCoverView.backgroundColor = .clear
        formCoverView.layer.cornerRadius = Adaptive.val(12.5)
        
        coverStackView = UIStackView()
        coverStackView.axis = .vertical
        coverStackView.alignment = .fill
        coverStackView.spacing = Adaptive.val(20)
        coverStackView.layoutMargins = Adaptive.edges(top: 20, left:20, bottom: 20, right: 20)
        coverStackView.isLayoutMarginsRelativeArrangement = true
        
        titleStackView = UIStackView()
        titleStackView.axis = .vertical
        titleStackView.alignment = .center
        
        formStackView = UIStackView()
        formStackView.axis = .vertical
        formStackView.alignment = .fill
        formStackView.spacing = Adaptive.val(10)
        
        timerStackView = UIStackView()
        timerStackView.axis = .vertical
        timerStackView.alignment = .center
        
        buttonsVerticalStackView = UIStackView()
        buttonsVerticalStackView.axis = .vertical
        buttonsVerticalStackView.alignment = .center
        
        buttonsStackView = UIStackView()
        buttonsStackView.axis = .horizontal
        buttonsStackView.alignment = .center
        buttonsStackView.spacing = Adaptive.val(20)
        
        eyeButton = createEyeButton(with: 1)
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .medium)
        titleLabel.textColor = .dashDarkBlue
        titleLabel.numberOfLines = 0
        
        smsCodeTextField = SMSConfirmationTextField(cornerRadius: Adaptive.val(6),
                                                    isSecureTextEntry: true)
        smsCodeTextField.layer.borderWidth = Adaptive.val(2)
        smsCodeTextField.layer.borderColor = UIColor.timerGray.cgColor
        smsCodeTextField.textAlignment = .center
//        smsCodeTextField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0)
        smsCodeTextField.rightViewMode = .always
        
        timerView = TimerView(frame: CGRect(x: 0, y: 0, width: Adaptive.val(170), height: Adaptive.val(170)))
        timerView.titleColor = .timerGray
        timerView.shapeColor = ColorConstants.mainRed
        timerView.backShapeColor = .timerGray
        
        backButton = NextButton.systemButton(with: RS.btn_close.localized().uppercased(), cornerRadius: Size.buttonHeight/2)
        backButton.setBackgroundColor(.clear, for: .normal)
        backButton.setBackgroundColor(ColorConstants.highlightedGray, for: .highlighted)
        backButton.setTitleColor(.dashDarkBlue, for: .normal)
        backButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        backButton.layer.borderWidth = Adaptive.val(2)
        backButton.layer.borderColor = UIColor.darkGrey.cgColor
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: Size.buttonHeight/2)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        nextButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        nextButton.isEnabled = false
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(formCoverView)
        formCoverView.addSubview(coverStackView)
        coverStackView.addArrangedSubview(titleStackView)
        coverStackView.addArrangedSubview(formStackView)
        coverStackView.addArrangedSubview(buttonsVerticalStackView)
        buttonsVerticalStackView.addArrangedSubview(buttonsStackView)
        titleStackView.addArrangedSubview(titleLabel)
        formStackView.addArrangedSubview(smsCodeTextField)
        smsCodeTextField.rightView = eyeButton
        mainView.addSubview(timerView)
        mainView.addSubview(backButton)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        formCoverView.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.top.equalToSuperview().offset(Adaptive.val(175))
        }
        
        coverStackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        smsCodeTextField.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Size.buttonHeight)
        }
        
//        timerStackView.snp.remakeConstraints { (maker) in
//            maker.centerX.equalToSuperview()
//            maker.bottom.equalTo(nextButton.snp.top).offset(-Adaptive.val(108))
//        }
        
        timerView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(170))
            maker.centerX.equalToSuperview()
            maker.bottom.equalTo(nextButton.snp.top).offset(-Adaptive.val(108))
        }
        
        backButton.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.height.equalTo(Size.buttonHeight)
            maker.bottom.equalToSuperview().offset(-Adaptive.val(57))
        }
        
        eyeButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Size.EyeButton.size.width)
            maker.height.equalTo(Size.EyeButton.size.height)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.height.equalTo(Size.buttonHeight)
            maker.bottom.equalTo(backButton.snp.top).offset(-Adaptive.val(10))
        }
    }
    
    private func createEyeButton(with tag: Int) -> UIButton {
        let btn = UIButton()
        btn.tag = tag
        btn.tintColor = .black
        btn.setImage(UIImage(named: "img_eye")?.changeColor(.black), for: .normal)
        btn.setImage(UIImage(named: "btn_eye_crossed_white")?.changeColor(.black), for: .selected)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(50)
    
    struct EyeButton {
        static let size = Adaptive.size(50, 20)
    }
}
