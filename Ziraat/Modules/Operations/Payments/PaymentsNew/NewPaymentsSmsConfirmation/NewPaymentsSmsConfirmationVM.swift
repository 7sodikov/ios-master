//
//  NewPaymentsSmsConfirmationVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class NewPaymentsSmsConfirmationVM {
    let userEntry = NewPaymentsSmsConfirmationUserEntry()
    
    var service: PaymentDataEntryFormServiceEntity!
    var payerCard: OperationPagePA2PDataEntryUserEntryPayer!
    var paymentPrepareResponse: PaymentPrepareResponse!
    var paymentPayResponse: SingleMessageResponse!
    
}

class NewPaymentsSmsConfirmationUserEntry {
    var smsCode: String?
}
