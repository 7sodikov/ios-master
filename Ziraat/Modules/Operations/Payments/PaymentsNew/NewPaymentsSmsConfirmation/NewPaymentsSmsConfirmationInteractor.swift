//
//  NewPaymentsSmsConfirmationInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol NewPaymentsSmsConfirmationInteractorProtocol: class {
    func paymentConfirm(with token: String, smsCode: String)
    func operationSMSResend(for token: String)
}

class NewPaymentsSmsConfirmationInteractor: NewPaymentsSmsConfirmationInteractorProtocol {
    weak var presenter: NewPaymentsSmsConfirmationInteractorToPresenterProtocol!
    func paymentConfirm(with token: String, smsCode: String) {
        NetworkService.Payment.paymentConfirm(with: token, smsCode: smsCode) { [weak self] (result) in
            self?.presenter.didPaymentConfirm(with: result)
        }
    }
    
    func operationSMSResend(for token: String) {
        NetworkService.Operation.operationSMSResend(token: token) { [weak self] (result) in
            self?.presenter.didOperationSMSResend(with: result)
        }
    }
}
