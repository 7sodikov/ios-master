//
//  NewPaymentsSmsConfirmationRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol NewPaymentsSmsConfirmationRouterProtocol: class {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse,
                             paymentPayResponse: SingleMessageResponse) -> UIViewController
    func navigateToPaymentCompletion(with view: Any,
                                     service: PaymentDataEntryFormServiceEntity,
                                     payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                     paymentPrepareResponse: PaymentPrepareResponse,
                                     paymentConfirmationResponse: P2PConfirmResponse)
}

class NewPaymentsSmsConfirmationRouter: NewPaymentsSmsConfirmationRouterProtocol {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse,
                             paymentPayResponse: SingleMessageResponse) -> UIViewController {
        let vc = NewPaymentsSmsConfirmationVC()
        let presenter = NewPaymentsSmsConfirmationPresenter()
        let interactor = NewPaymentsSmsConfirmationInteractor()
        let router = NewPaymentsSmsConfirmationRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.service = service
        presenter.viewModel.payerCard = payerCard
        presenter.viewModel.paymentPrepareResponse = paymentPrepareResponse
        presenter.viewModel.paymentPayResponse = paymentPayResponse
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPaymentCompletion(with view: Any,
                                     service: PaymentDataEntryFormServiceEntity,
                                     payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                     paymentPrepareResponse: PaymentPrepareResponse,
                                     paymentConfirmationResponse: P2PConfirmResponse) {
        let viewCtrl = view as! UIViewController
        let completionCtrl = PaymentCompletionRouter.createModule(with: service,
                                                                  payerCard: payerCard,
                                                                  paymentPrepareResponse: paymentPrepareResponse,
                                                                  paymentConfirmationResponse: paymentConfirmationResponse)
//        (view as? PaymentSMSConfirmationVC)?.navigationController?.pushViewControllerRetro(completionCtrl)
        viewCtrl.navigationController?.pushViewController(completionCtrl, animated: true)
    }
}
