//
//  ServiceListInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ServiceListInteractorProtocol: AnyObject {
    func services(for categoryId: Int, searchString: String?)
}

class ServiceListInteractor: ServiceListInteractorProtocol {
    weak var presenter: ServiceListInteractorToPresenterProtocol!
    
    func services(for categoryId: Int, searchString: String?) {
        NetworkService.Payment.services(for: categoryId, searchString: searchString) { [weak self] (result) in
            self?.presenter.didServices(with: result)
        }
    }
}
