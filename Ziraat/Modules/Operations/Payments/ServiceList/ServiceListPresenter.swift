//
//  ServiceListPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ServiceListPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: ServiceListVM { get }
    func viewDidLoad()
    func collectionViewDidSelectItem(at index: Int)
}

protocol ServiceListInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didServices(with response: ResponseResult<[ServiceResponse], AppError>)
}

class ServiceListPresenter: ServiceListPresenterProtocol {
    weak var view: ServiceListVCProtocol!
    var interactor: ServiceListInteractorProtocol!
    var router: ServiceListRouterProtocol!
    var categoryId: Int!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ServiceListVM()
    
    func viewDidLoad() {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.services(for: categoryId, searchString: nil)
    }
    
    func collectionViewDidSelectItem(at index: Int) {
        let service = viewModel.services[index]
        router.navigateToPaymentForm(for: service, from: view)
    }
    
    // Private property and methods
    
}

extension ServiceListPresenter: ServiceListInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didServices(with response: ResponseResult<[ServiceResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.services = result.data
            view.showReloadButtonForResponses(false, animateReloadButton: false)
            view.warningMessageView.isHidden = !result.data.isEmpty
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            break
        }
    }
}
