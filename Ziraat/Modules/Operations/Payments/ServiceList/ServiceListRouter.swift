//
//  ServiceListRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceListRouterProtocol: AnyObject {
    static func createModule(with categoryId: Int) -> UIViewController
    func navigateToPaymentForm(for service: ServiceResponse, from view: Any?)
}

class ServiceListRouter: ServiceListRouterProtocol {
    static func createModule(with categoryId: Int) -> UIViewController {
        let vc = ServiceListVC()
        let presenter = ServiceListPresenter()
        let interactor = ServiceListInteractor()
        let router = ServiceListRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.categoryId = categoryId
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPaymentForm(for service: ServiceResponse, from view: Any?) {
        let vc = PaymentDataEntryFormRouter.createModule(for: service, favoriteItem: nil, payment: nil)
        let navCtrl = NavigationController(rootViewController: vc)
        navCtrl.modalPresentationStyle = .overCurrentContext
        
        let currentViewCtrl = view as? UIViewController
        currentViewCtrl?.present(navCtrl, animated: true, completion: nil)
    }
}
