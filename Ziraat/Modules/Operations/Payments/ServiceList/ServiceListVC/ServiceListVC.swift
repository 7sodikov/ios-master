//
//  ServiceListVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceListVCProtocol: BaseViewControllerProtocol, ServiceListViewInstaller {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class ServiceListVC: BaseViewController, ServiceListVCProtocol {
    var mainView: UIView { view }
    var bgImageView: UIImageView!
    var reloadButtonView: ReloadButtonView!
    var collectionLayout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var warningMessageView: NoItemsAvailableView!
    var presenter: ServiceListPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadButtonClicked(_:)), for: .touchUpInside)
        collectionView.dataSource = self
        collectionView.delegate = self
        presenter.viewDidLoad()
    }
    
    @objc private func reloadButtonClicked(_ sender: UIButton) {
        presenter.viewDidLoad()
    }
    
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        collectionView.isHidden = show
        collectionView.reloadData()
    }
    
}

extension ServiceListVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.viewModel.services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(ServiceCategoryCollectionCell.self)", for: indexPath) as! ServiceCategoryCollectionCell
        let service = presenter.viewModel.services[indexPath.row]
        return cell.setup(with: service.title, logoUrl: service.logoURL)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return ServiceListSize.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let leftRightPadding = (UIScreen.main.bounds.width - (2 * ServiceListSize.itemSize.width) - ServiceListSize.itemLineSpacing)/2
        return UIEdgeInsets(top: 0, left: leftRightPadding, bottom: 0, right: leftRightPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.collectionViewDidSelectItem(at: indexPath.row)
    }
}
