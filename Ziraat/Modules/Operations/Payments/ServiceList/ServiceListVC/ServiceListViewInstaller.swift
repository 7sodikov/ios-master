//
//  ServiceListViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceListViewInstaller: ViewInstaller {
    var bgImageView: UIImageView! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
    var collectionLayout: UICollectionViewFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
    var warningMessageView: NoItemsAvailableView! { get set }
}

extension ServiceListViewInstaller {
    func initSubviews() {
        bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "img_dash_light_background")
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        
        collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.sectionInset = .zero
        collectionLayout.scrollDirection = .vertical
        collectionLayout.minimumInteritemSpacing = 0
        collectionLayout.minimumLineSpacing = ServiceListSize.itemLineSpacing
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = .clear
        collectionView.register(ServiceCategoryCollectionCell.self,
                                forCellWithReuseIdentifier: String(describing: ServiceCategoryCollectionCell.self))
        warningMessageView = NoItemsAvailableView.setupText(title: RS.lbl_no_deposit.localized())
        warningMessageView.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(bgImageView)
        mainView.addSubview(reloadButtonView)
        mainView.addSubview(collectionView)
        mainView.addSubview(warningMessageView)
    }
    
    func addSubviewsConstraints() {
        bgImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
        collectionView.constraint { make in
            make.bottom(self.mainView.bottomAnchor)
                .top.horizontal.equalToSuperView()
        }
        
        warningMessageView.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(214))
            make.centerX.equalToSuperview()
        }
    }
}

struct ServiceListSize {
    static let itemSize = CGSize(width: UIScreen.main.bounds.width * 0.4, height: Adaptive.val(95))
    static let itemLineSpacing = Adaptive.val(13)
}
