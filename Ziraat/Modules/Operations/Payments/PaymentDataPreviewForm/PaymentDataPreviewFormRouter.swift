//
//  PaymentDataPreviewFormRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentDataPreviewFormRouterProtocol: class {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse) -> UIViewController
    func navigateToPaymentConfirmation(with view: Any,
                                       service: PaymentDataEntryFormServiceEntity,
                                       payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                       paymentPrepareResponse: PaymentPrepareResponse,
                                       paymentPayResponse: SingleMessageResponse)
    func closeWindow(in navCtrl: Any, _ delegate:PopubMessageProtocol?)
}

class PaymentDataPreviewFormRouter: PaymentDataPreviewFormRouterProtocol {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse) -> UIViewController {
        let vc = PaymentDataPreviewFormVC()
        let presenter = PaymentDataPreviewFormPresenter()
        let interactor = PaymentDataPreviewFormInteractor()
        let router = PaymentDataPreviewFormRouter()
        var response = paymentPrepareResponse
        if let subItems = response.details {
            response.fieldValues.append(contentsOf: subItems)
        }
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.service = service
        presenter.viewModel.payerCard = payerCard
        presenter.viewModel.paymentPrepareResponse = response
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPaymentConfirmation(with view: Any,
                                       service: PaymentDataEntryFormServiceEntity,
                                       payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                       paymentPrepareResponse: PaymentPrepareResponse,
                                       paymentPayResponse: SingleMessageResponse) {
        let confirmationCtrl = NewPaymentsSmsConfirmationRouter.createModule(with: service,
                                                                         payerCard: payerCard,
                                                                         paymentPrepareResponse: paymentPrepareResponse,
                                                                         paymentPayResponse: paymentPayResponse)
        (view as? PaymentDataPreviewFormVC)?.navigationController?.pushViewControllerRetro(confirmationCtrl)
    }
    
    func closeWindow(in navCtrl: Any, _ delegate: PopubMessageProtocol?) {
        let viewCtrl = navCtrl as! UIViewController
//        let statementVC = PopupMessageRouter.createModule(RS.lbl_sure_cancel_transaction.localized(), delegate)
//        statementVC.modalPresentationStyle = .overFullScreen
//        viewCtrl.present(statementVC, animated: false, completion: nil)
        let controller = NotifyViewController(.confirmationConversion) {
            delegate?.actionResult(true)
        }
        viewCtrl.present(controller, animated: false, completion: nil)
    }
}
