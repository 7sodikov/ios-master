//
//  PaymentDataPreviewFormPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentDataPreviewFormPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: PaymentDataPreviewFormVM { get }
    func nextBtnClicked()
    func closeButtonClicked(in navigationCtrl: Any)
}

protocol PaymentDataPreviewFormInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didPaymentPay(with response: ResponseResult<SingleMessageResponse, AppError>)
}

class PaymentDataPreviewFormPresenter: PaymentDataPreviewFormPresenterProtocol {
    weak var view: PaymentDataPreviewFormVCProtocol!
    var interactor: PaymentDataPreviewFormInteractorProtocol!
    var router: PaymentDataPreviewFormRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PaymentDataPreviewFormVM()
    
    func nextBtnClicked() {
        view.preloader(show: true)
        interactor.paymentPay(for: viewModel.paymentPrepareResponse.token)
    }
    
    func closeButtonClicked(in navigationCtrl: Any) {
        router.closeWindow(in: navigationCtrl, self)
    }
    
    // Private property and methods
    
}

extension PaymentDataPreviewFormPresenter: PaymentDataPreviewFormInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didPaymentPay(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            router.navigateToPaymentConfirmation(with: view as Any,
                                                 service: viewModel.service,
                                                 payerCard: viewModel.payerCard,
                                                 paymentPrepareResponse: viewModel.paymentPrepareResponse,
                                                 paymentPayResponse: result.data)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}

extension PaymentDataPreviewFormPresenter : PopubMessageProtocol{
    func actionResult(_ action: Bool) {
        if action {
            view.dismiss()
        }
    }
}
