//
//  PaymentDataPreviewFormVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentDataPreviewFormVCProtocol: BaseViewControllerProtocol {
    func backBtnClicked()
    func dismiss()
}

class PaymentDataPreviewFormVC: BaseViewController, PaymentDataPreviewFormViewInstaller {    
    var mainView: UIView { view }
    var backView: UIImageView!
    var formCoverView: UIScrollView!
    var descriptionLabel: UILabel!
    var homeButton: UIButton!
    var coverStackView: UIStackView!
    var titleStackView: UIStackView!
    var formStackView: UIStackView!
//    var separatorLineView: UIView!
    var bottomLineStackView: UIStackView!
    var payerCardCell: PaymentDataPreviewFormCell!
    var payerCardOwnerCell: PaymentDataPreviewFormCell!
    var totalAmountCell: PaymentDataPreviewFormCell!
    var buttonsVerticalStackView: UIStackView!
    var buttonsStackView: UIStackView!
    var titleLabel: UILabel!
    var backButton: NextButton!
    var nextButton: NextButton!
    
    var presenter: PaymentDataPreviewFormPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        setupNavigation()
        
//        navigationItem.leftBarButtonItem = nil
//        navigationItem.hidesBackButton = false
        
        payerCardCell.setup(title: RS.lbl_card_num.localized(), value: presenter.viewModel.paymentPrepareResponse.senderPan)
        payerCardOwnerCell.setup(title: RS.lbl_commission.localized(), value: String(presenter.viewModel.paymentPrepareResponse.commission))
        
        for fieldValue in presenter.viewModel.paymentPrepareResponse.fieldValues {
            let cell = PaymentDataPreviewFormCell(frame: .zero)
            let fieldType = ServiceFieldType.getType(for: fieldValue.type)
            if fieldType == .money {
                let doubleValue = Double(fieldValue.value) ?? 0
                let formattedValue = (doubleValue/100).currencyFormattedStr() ?? ""
                cell.setup(title: fieldValue.title, value: formattedValue)
            } else {
                cell.setup(title: fieldValue.title, value: fieldValue.value)
            }
            formStackView.addArrangedSubview(cell)
        }
        
        let totalAmountStr = (Double(presenter.viewModel.paymentPrepareResponse.total)/100).currencyFormattedStr() ?? ""
        totalAmountCell.setup(title: RS.lbl_total_sum.localized(), value: totalAmountStr)
        
        backButton.addTarget(self, action: #selector(backBtnClicked), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
        homeButton.addTarget(self, action: #selector(openDashboard), for: .touchUpInside)
    }
    
    private func setupNavigation() {
//        self.navigationItem.titleView = titleLabel
        self.title = presenter.viewModel.service.title
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: homeButton)
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
        backButton.isEnabled = !show
    }
    
    @objc func backBtnClicked() {
//        navigationController?.popViewControllerRetro()
        presenter.closeButtonClicked(in: self.navigationController as Any)
    }
    
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func nextBtnClicked(_ sender: NextButton) {
        presenter.nextBtnClicked()
    }
}

extension PaymentDataPreviewFormVC: PaymentDataPreviewFormVCProtocol {
    
}
