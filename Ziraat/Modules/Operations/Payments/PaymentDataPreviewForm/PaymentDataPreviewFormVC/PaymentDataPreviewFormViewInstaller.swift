//
//  PaymentDataPreviewFormViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentDataPreviewFormViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var formCoverView: UIScrollView! { get set }
    var homeButton: UIButton! { get set }
    var descriptionLabel: UILabel! { get set }
    var coverStackView: UIStackView! { get set }
    var titleStackView: UIStackView! { get set }
    var formStackView: UIStackView! { get set }
//    var separatorLineView: UIView! { get set }
    var bottomLineStackView: UIStackView! { get set }
    var payerCardCell: PaymentDataPreviewFormCell! { get set }
    var payerCardOwnerCell: PaymentDataPreviewFormCell! { get set }
    var totalAmountCell: PaymentDataPreviewFormCell! { get set }
    var buttonsVerticalStackView: UIStackView! { get set }
    var buttonsStackView: UIStackView! { get set }
    
    var titleLabel: UILabel! { get set }
    var backButton: NextButton! { get set }
    var nextButton: NextButton! { get set }
}

extension PaymentDataPreviewFormViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
        
        formCoverView = UIScrollView()
        formCoverView.backgroundColor = .white
        formCoverView.layer.cornerRadius = Adaptive.val(12.5)
        
        homeButton = UIButton()
        homeButton.setBackgroundImage(UIImage(named: "img_icon_home"), for: .normal)
        
        descriptionLabel = UILabel()
        descriptionLabel.font = .gothamNarrow(size: 15, .book)
        descriptionLabel.textColor = .dashDarkBlue
        descriptionLabel.text = RS.lbl_transaction_preview.localized()
        descriptionLabel.numberOfLines = 0
        
        coverStackView = UIStackView()
        coverStackView.axis = .vertical
        coverStackView.alignment = .fill
        coverStackView.spacing = Adaptive.val(20)
        coverStackView.layoutMargins = Adaptive.edges(top: 20, left:20, bottom: 20, right: 20)
        coverStackView.isLayoutMarginsRelativeArrangement = true
        
        titleStackView = UIStackView()
        titleStackView.axis = .vertical
        titleStackView.alignment = .center
        
        formStackView = UIStackView()
        formStackView.axis = .vertical
        formStackView.spacing = Adaptive.val(15)
        
//        separatorLineView = UIView()
//        separatorLineView.backgroundColor = .black
        
        bottomLineStackView = UIStackView()
        bottomLineStackView.axis = .vertical
        bottomLineStackView.alignment = .fill
        
        payerCardCell = PaymentDataPreviewFormCell(frame: .zero)
        payerCardOwnerCell = PaymentDataPreviewFormCell(frame: .zero)
        
        totalAmountCell = PaymentDataPreviewFormCell(frame: .zero)
        
        buttonsVerticalStackView = UIStackView()
        buttonsVerticalStackView.axis = .vertical
        buttonsVerticalStackView.alignment = .center
        
        buttonsStackView = UIStackView()
        buttonsStackView.axis = .horizontal
        buttonsStackView.alignment = .center
        buttonsStackView.spacing = Adaptive.val(20)
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(18))
        titleLabel.textColor = .black
        titleLabel.text = RS.lbl_confirm_payment.localized()
        
        backButton = NextButton.systemButton(with: RS.lbl_cancel.localized().uppercased(), cornerRadius: PreviewSize.buttonHeight/2)
        backButton.layer.borderWidth = Adaptive.val(2)
        backButton.layer.borderColor = UIColor.darkGrey.cgColor
        backButton.setTitleColor(.dashDarkBlue, for: .normal)
        backButton.setBackgroundColor(.clear, for: .normal)
        backButton.setBackgroundColor(ColorConstants.highlightedGray, for: .highlighted)
        backButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        
        nextButton = NextButton.systemButton(with: RS.btn_confirm.localized().uppercased(), cornerRadius: PreviewSize.buttonHeight/2)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        nextButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(descriptionLabel)
        mainView.addSubview(formCoverView)
        
        formCoverView.addSubview(formStackView)
        
//        coverStackView.addArrangedSubview(titleStackView)
//        coverStackView.addArrangedSubview(formStackView)
//        coverStackView.addArrangedSubview(separatorLineView)
//        coverStackView.addArrangedSubview(bottomLineStackView)
//        coverStackView.addArrangedSubview(buttonsVerticalStackView)
//        buttonsVerticalStackView.addArrangedSubview(buttonsStackView)
        
        formStackView.addArrangedSubview(payerCardCell)
        formStackView.addArrangedSubview(payerCardOwnerCell)
        formStackView.addArrangedSubview(totalAmountCell)
        
        mainView.addSubview(backButton)
        mainView.addSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        descriptionLabel.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(89))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
        }
        
        formCoverView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(descriptionLabel.snp.bottom).offset(Adaptive.val(10))
            maker.bottom.equalTo(nextButton.snp.top).offset(-Adaptive.val(15))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
        }
        
//        coverStackView.snp.makeConstraints { (maker) in
//            maker.leading.trailing.top.bottom.equalToSuperview()
//        }
        
        formStackView.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(Adaptive.val(16))
            make.bottom.equalToSuperview().inset(Adaptive.val(16))
            make.centerX.equalToSuperview()
            make.width.equalTo(UIScreen.main.bounds.width - 4*Adaptive.val(16))
        }
//        separatorLineView.snp.remakeConstraints { (maker) in
//            maker.height.equalTo(Adaptive.val(1))
//        }
        
        backButton.snp.remakeConstraints { (maker) in
            maker.bottom.equalToSuperview().offset(-Adaptive.val(57))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.height.equalTo(PreviewSize.buttonHeight)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(backButton.snp.top).offset(-Adaptive.val(10))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.height.equalTo(PreviewSize.buttonHeight)
        }
    }
}

struct PreviewSize {
    static let buttonHeight = Adaptive.val(50)
}
