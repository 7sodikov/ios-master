//
//  PaymentDataPreviewFormCellInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/22/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol PaymentDataPreviewFormCellInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var valueLabelContainerStackView: UIStackView! { get set }
    var valueLabel: UILabel! { get set }
    var valueView: UIView! { get set }
}

extension PaymentDataPreviewFormCellInstaller {
    func initSubviews() {
        let mainV = (mainView as? UIStackView)
        mainV?.axis = .vertical
        mainV?.alignment = .top
        mainV?.distribution = .fill
        mainV?.spacing = Adaptive.val(5)
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .medium)
        titleLabel.textColor = .red
        
        valueLabelContainerStackView = UIStackView()
        valueLabelContainerStackView.axis = .vertical
        valueLabelContainerStackView.alignment = .leading
        valueLabelContainerStackView.backgroundColor = .lightestGray
        valueLabelContainerStackView.layer.cornerRadius = Adaptive.val(6)
        
        valueLabel = UILabel()
        valueLabel.font = .gothamNarrow(size: 15, .medium)
        valueLabel.textColor = .dashDarkBlue
        valueLabel.numberOfLines = 2
        
        valueView = UIView()
    }
    
    func embedSubviews() {
        (mainView as? UIStackView)?.addArrangedSubview(titleLabel)
        (mainView as? UIStackView)?.addArrangedSubview(valueLabelContainerStackView)
        valueLabelContainerStackView.addArrangedSubview(valueLabel)
    }
    
    func addSubviewsConstraints() {
        valueLabelContainerStackView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(40))
            maker.leading.equalToSuperview()//.offset(Adaptive.val(16))
            maker.trailing.equalToSuperview()//.offset(-Adaptive.val(16))
        }
        
        valueLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(valueLabelContainerStackView)
            maker.leading.equalTo(valueLabelContainerStackView).offset(Adaptive.val(10))
        }
    }
}

fileprivate struct Size {
    
}
