//
//  PaymentDataPreviewFormCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/22/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PaymentDataPreviewFormCell: UIStackView, PaymentDataPreviewFormCellInstaller {
    var mainView: UIView { self }
    var titleLabel: UILabel!
    var valueLabelContainerStackView: UIStackView!
    var valueLabel: UILabel!
    var valueView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func setup(title: String, value: String) {
        titleLabel.text = title
        valueLabel.text = value
    }
}
