//
//  PaymentDataPreviewFormInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentDataPreviewFormInteractorProtocol: class {
    func paymentPay(for token: String)
}

class PaymentDataPreviewFormInteractor: PaymentDataPreviewFormInteractorProtocol {
    weak var presenter: PaymentDataPreviewFormInteractorToPresenterProtocol!
    
    func paymentPay(for token: String) {
        NetworkService.Payment.paymentPay(for: token) { [weak self] (result) in
            self?.presenter.didPaymentPay(with: result)
        }
    }
}
