//
//  PaymentDataPreviewFormVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class PaymentDataPreviewFormVM {
    var service: PaymentDataEntryFormServiceEntity!
    var payerCard: OperationPagePA2PDataEntryUserEntryPayer!
    var paymentPrepareResponse: PaymentPrepareResponse!
}
