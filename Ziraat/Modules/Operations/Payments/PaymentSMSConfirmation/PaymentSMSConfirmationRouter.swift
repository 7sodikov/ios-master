//
//  PaymentSMSConfirmationRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/18/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentSMSConfirmationRouterProtocol: class {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse,
                             paymentPayResponse: SingleMessageResponse) -> UIViewController
    func navigateToPaymentCompletion(with view: Any,
                                     service: PaymentDataEntryFormServiceEntity,
                                     payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                     paymentPrepareResponse: PaymentPrepareResponse,
                                     paymentConfirmationResponse: P2PConfirmResponse)
}

class PaymentSMSConfirmationRouter: PaymentSMSConfirmationRouterProtocol {
    static func createModule(with service: PaymentDataEntryFormServiceEntity,
                             payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                             paymentPrepareResponse: PaymentPrepareResponse,
                             paymentPayResponse: SingleMessageResponse) -> UIViewController {
        let vc = PaymentSMSConfirmationVC()
        let presenter = PaymentSMSConfirmationPresenter()
        let interactor = PaymentSMSConfirmationInteractor()
        let router = PaymentSMSConfirmationRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.service = service
        presenter.viewModel.payerCard = payerCard
        presenter.viewModel.paymentPrepareResponse = paymentPrepareResponse
        presenter.viewModel.paymentPayResponse = paymentPayResponse
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToPaymentCompletion(with view: Any,
                                     service: PaymentDataEntryFormServiceEntity,
                                     payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                     paymentPrepareResponse: PaymentPrepareResponse,
                                     paymentConfirmationResponse: P2PConfirmResponse) {
        let completionCtrl = PaymentCompletionRouter.createModule(with: service,
                                                                  payerCard: payerCard,
                                                                  paymentPrepareResponse: paymentPrepareResponse,
                                                                  paymentConfirmationResponse: paymentConfirmationResponse)
        (view as? PaymentSMSConfirmationVC)?.navigationController?.pushViewControllerRetro(completionCtrl)
    }
}
