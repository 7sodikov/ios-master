//
//  PaymentSMSConfirmationViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/18/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentSMSConfirmationViewInstaller: ViewInstaller {
    var formCoverView: UIView! { get set }
    var coverStackView: UIStackView! { get set }
    var titleStackView: UIStackView! { get set }
    var formStackView: UIStackView! { get set }
    var timerStackView: UIStackView! { get set }
    var buttonsVerticalStackView: UIStackView! { get set }
    var buttonsStackView: UIStackView! { get set }
    
    var titleLabel: UILabel! { get set }
    var smsCodeTextField: SMSConfirmationTextField! { get set }
    var timerView: TimerView! { get set }
    var backButton: NextButton! { get set }
    var nextButton: NextButton! { get set }
}

extension PaymentSMSConfirmationViewInstaller {
    func initSubviews() {
        formCoverView = UIView()
        formCoverView.backgroundColor = .white
        formCoverView.layer.cornerRadius = Adaptive.val(12.5)
        
        coverStackView = UIStackView()
        coverStackView.axis = .vertical
        coverStackView.alignment = .fill
        coverStackView.spacing = Adaptive.val(20)
        coverStackView.layoutMargins = Adaptive.edges(top: 20, left:20, bottom: 20, right: 20)
        coverStackView.isLayoutMarginsRelativeArrangement = true
        
        titleStackView = UIStackView()
        titleStackView.axis = .vertical
        titleStackView.alignment = .center
        
        formStackView = UIStackView()
        formStackView.axis = .vertical
        formStackView.alignment = .fill
        formStackView.spacing = Adaptive.val(10)
        
        timerStackView = UIStackView()
        timerStackView.axis = .vertical
        timerStackView.alignment = .center
        
        buttonsVerticalStackView = UIStackView()
        buttonsVerticalStackView.axis = .vertical
        buttonsVerticalStackView.alignment = .center
        
        buttonsStackView = UIStackView()
        buttonsStackView.axis = .horizontal
        buttonsStackView.alignment = .center
        buttonsStackView.spacing = Adaptive.val(20)
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(18))
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
        
        smsCodeTextField = SMSConfirmationTextField(cornerRadius: PaymentDataEntryFormFieldSize.TextField.height/2,
                                                    isSecureTextEntry: false)
        smsCodeTextField.layer.borderWidth = 1
        smsCodeTextField.layer.borderColor = ColorConstants.borderLine.cgColor
        
        timerView = TimerView(frame: CGRect(x: 0, y: 0, width: ApplicationSize.Timer.width, height: ApplicationSize.Timer.width))
        timerView.titleColor = .black
        timerView.shapeColor = ColorConstants.mainRed
        timerView.backShapeColor = ColorConstants.mainRed.withAlphaComponent(0.40)
        
        backButton = NextButton.systemButton(with: RS.btn_close.localized().uppercased(), cornerRadius: 25)
        backButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        backButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        backButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        
        nextButton = NextButton.systemButton(with: RS.btn_further.localized(), cornerRadius: 25)
        nextButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        nextButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        nextButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(13.5))
        nextButton.isEnabled = false
    }
    
    func embedSubviews() {
        mainView.addSubview(formCoverView)
        formCoverView.addSubview(coverStackView)
        coverStackView.addArrangedSubview(titleStackView)
        coverStackView.addArrangedSubview(formStackView)
        coverStackView.addArrangedSubview(buttonsVerticalStackView)
        buttonsVerticalStackView.addArrangedSubview(buttonsStackView)
        
        titleStackView.addArrangedSubview(titleLabel)
        formStackView.addArrangedSubview(smsCodeTextField)
        formStackView.addArrangedSubview(timerStackView)
        timerStackView.addArrangedSubview(timerView)
        
        buttonsStackView.addArrangedSubview(backButton)
        buttonsStackView.addArrangedSubview(nextButton)
    }
    
    func addSubviewsConstraints() {
        formCoverView.snp.remakeConstraints { (maker) in
            maker.left.equalTo(16)
            maker.right.equalTo(-16)
            maker.centerY.equalToSuperview()
        }
        
        coverStackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        smsCodeTextField.snp.remakeConstraints { (maker) in
            maker.height.equalTo(PaymentDataEntryFormFieldSize.TextField.height)
        }
        
        timerView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ApplicationSize.Timer.width)
        }
        
        backButton.snp.remakeConstraints { (maker) in
            maker.width.equalTo(345)
            maker.height.equalTo(50)
        }
        
        nextButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(backButton)
        }
    }
}

fileprivate struct Size {
    
}
