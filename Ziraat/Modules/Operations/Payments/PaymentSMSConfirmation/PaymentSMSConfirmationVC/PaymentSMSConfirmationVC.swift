//
//  PaymentSMSConfirmationVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/18/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentSMSConfirmationVCProtocol: BaseViewControllerProtocol {
    func nextButton(enabled: Bool)
    func set(nextButtonTitle: String)
    func startTimer()
    func stopTimer()
}

class PaymentSMSConfirmationVC: BaseViewController, PaymentSMSConfirmationViewInstaller {
    var mainView: UIView { view }
    var formCoverView: UIView!
    var coverStackView: UIStackView!
    var titleStackView: UIStackView!
    var formStackView: UIStackView!
    var timerStackView: UIStackView!
    var buttonsVerticalStackView: UIStackView!
    var buttonsStackView: UIStackView!
    var titleLabel: UILabel!
    var smsCodeTextField: SMSConfirmationTextField!
    var timerView: TimerView!
    var backButton: NextButton!
    var nextButton: NextButton!
    
    var presenter: PaymentSMSConfirmationPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = true
        
        titleLabel.text = presenter.viewModel.paymentPayResponse.message
        smsCodeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        timerView.startTimer(with: timerValue)
        timerView.timerReachedItsEnd = {
            self.presenter.timerReachedItsEnd()
        }
        backButton.addTarget(self, action: #selector(backBtnClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
    }
    
    override func preloader(show: Bool) {
        nextButton.animateIndicator = show
        backButton.isEnabled = !show
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        presenter.textFieldDidChange(with: sender.text)
    }
    
    @objc private func backBtnClicked(_ sender: NextButton) {
        stopTimer()
//        navigationController?.popViewControllerRetro()
        UIView.animate(withDuration: 0.3, animations: {
            self.navigationController?.view.backgroundColor = UIColor.clear
        }) { (isCompleted) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func nextBtnClicked(_ sender: NextButton) {
        presenter.nextBtnClicked()
    }
}

extension PaymentSMSConfirmationVC: PaymentSMSConfirmationVCProtocol {
    func nextButton(enabled: Bool) {
        nextButton.isEnabled = enabled
    }
    
    func set(nextButtonTitle: String) {
        nextButton.setTitle(nextButtonTitle, for: .normal)
    }
    
    func startTimer() {
        timerView?.startTimer(with: timerValue)
    }
    
    func stopTimer() {
        timerView.stopTimer()
    }
}
