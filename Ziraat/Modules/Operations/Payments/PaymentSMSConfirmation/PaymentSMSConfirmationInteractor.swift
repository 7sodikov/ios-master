//
//  PaymentSMSConfirmationInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/18/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentSMSConfirmationInteractorProtocol: class {
    func paymentConfirm(with token: String, smsCode: String)
    func operationSMSResend(for token: String)
}

class PaymentSMSConfirmationInteractor: PaymentSMSConfirmationInteractorProtocol {
    weak var presenter: PaymentSMSConfirmationInteractorToPresenterProtocol!
    func paymentConfirm(with token: String, smsCode: String) {
        NetworkService.Payment.paymentConfirm(with: token, smsCode: smsCode) { [weak self] (result) in
            self?.presenter.didPaymentConfirm(with: result)
        }
    }
    
    func operationSMSResend(for token: String) {
        NetworkService.Operation.operationSMSResend(token: token) { [weak self] (result) in
            self?.presenter.didOperationSMSResend(with: result)
        }
    }
}
