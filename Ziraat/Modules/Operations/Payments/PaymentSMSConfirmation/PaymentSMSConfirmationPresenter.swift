//
//  PaymentSMSConfirmationPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/18/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentSMSConfirmationPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: PaymentSMSConfirmationVM { get }
    func nextBtnClicked()
    func textFieldDidChange(with value: String?)
    func timerReachedItsEnd()
}

protocol PaymentSMSConfirmationInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didPaymentConfirm(with response: ResponseResult<P2PConfirmResponse, AppError>)
    func didOperationSMSResend(with response: ResponseResult<SingleMessageResponse, AppError>)
}

class PaymentSMSConfirmationPresenter: PaymentSMSConfirmationPresenterProtocol {
    weak var view: PaymentSMSConfirmationVCProtocol!
    var interactor: PaymentSMSConfirmationInteractorProtocol!
    var router: PaymentSMSConfirmationRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PaymentSMSConfirmationVM()
    
    func nextBtnClicked() {
        if isTimerRunning {
            if let smsCode = viewModel.userEntry.smsCode, smsCode.count == 6 {
                view.preloader(show: true)
                interactor.paymentConfirm(with: viewModel.paymentPrepareResponse.token,
                                          smsCode: smsCode)
            } else {
                view.showError(message: RS.lbl_fill_all_fields.localized())
            }
        } else {
            view.preloader(show: true)
            interactor.operationSMSResend(for: viewModel.paymentPrepareResponse.token)
        }
    }
    
    func textFieldDidChange(with value: String?) {
        viewModel.userEntry.smsCode = value
        if isTimerRunning {
            view.nextButton(enabled: value?.count == 6)
        } else {
            view.nextButton(enabled: true)
        }
    }
    
    func timerReachedItsEnd() {
        isTimerRunning = false
        view.set(nextButtonTitle: RS.btn_resend_sms.localized())
        view.nextButton(enabled: true)
    }
    
    // Private property and methods
    private var isTimerRunning = true
}

extension PaymentSMSConfirmationPresenter: PaymentSMSConfirmationInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didPaymentConfirm(with response: ResponseResult<P2PConfirmResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            router.navigateToPaymentCompletion(with: view as Any,
                                               service: viewModel.service,
                                               payerCard: viewModel.payerCard,
                                               paymentPrepareResponse: viewModel.paymentPrepareResponse,
                                               paymentConfirmationResponse: result.data)
            view.stopTimer()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didOperationSMSResend(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            isTimerRunning = true
            view.startTimer()
            view.set(nextButtonTitle: RS.btn_further.localized())
            view.nextButton(enabled: viewModel.userEntry.smsCode?.count == 6)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
