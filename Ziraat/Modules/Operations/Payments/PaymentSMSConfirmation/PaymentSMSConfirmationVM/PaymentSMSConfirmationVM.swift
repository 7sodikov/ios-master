//
//  PaymentSMSConfirmationVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/18/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class PaymentSMSConfirmationVM {
    let userEntry = PaymentSMSConfirmationUserEntry()
    
    var service: PaymentDataEntryFormServiceEntity!
    var payerCard: OperationPagePA2PDataEntryUserEntryPayer!
    var paymentPrepareResponse: PaymentPrepareResponse!
    var paymentPayResponse: SingleMessageResponse!
}
