//
//  ServiceCategoryCollectionCellInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

protocol ServiceCategoryCollectionCellInstaller: ViewInstaller {
    var iconImageView: UIImageView! { get set }
    var optionButton: UIButton! { get set }
    var titleLabel: UILabel! { get set }
    var indicatorCoverView: UIView! { get set }
    var indicator: MDCActivityIndicator! { get set }
}

extension ServiceCategoryCollectionCellInstaller {
    func initSubviews() {
        mainView.backgroundColor = .white
        mainView.layer.cornerRadius = Adaptive.val(15)
        
        iconImageView = UIImageView()
        iconImageView.layer.cornerRadius = Size.IconImageView.size/2
        iconImageView.clipsToBounds = true
        
        optionButton = UIButton()
        optionButton.setImage(UIImage(named: "img_login_settings")?.changeColor(.black), for: .normal)
        optionButton.layer.cornerRadius = Size.OptionButton.size/2
        optionButton.layer.masksToBounds = true
        optionButton.setBackgroundColor(UIColor.black.withAlphaComponent(0.09), for: .highlighted)
        optionButton.imageEdgeInsets = Adaptive.edges(top: 10, left: 0, bottom:10, right: 0)
        optionButton.imageView?.contentMode = .scaleAspectFit
        optionButton.isHidden = true
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 2
        titleLabel.textAlignment = .center
        titleLabel.textColor = .black
        titleLabel.font = EZFontType.regular.sfProText(size: 9)
        
        indicatorCoverView = UIView()
        indicatorCoverView.layer.cornerRadius = mainView.layer.cornerRadius
        indicatorCoverView.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        indicatorCoverView.isHidden = true
        
        indicator = MDCActivityIndicator()
        indicator.cycleColors = [.black]
        indicator.tintColor = .white
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.radius = ReloadButtonViewSize.Indicator.size/2
    }
    
    func embedSubviews() {
        mainView.addSubview(iconImageView)
        mainView.addSubview(optionButton)
        mainView.addSubview(titleLabel)
        mainView.addSubview(indicatorCoverView)
        indicatorCoverView.addSubview(indicator)
    }
    
    func addSubviewsConstraints() {
        iconImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Size.IconImageView.size)
            maker.centerX.equalToSuperview()
            maker.top.equalTo(Adaptive.val(12))
        }
        
        optionButton.snp.remakeConstraints { (maker) in
            maker.top.right.equalToSuperview()
            maker.width.height.equalTo(Size.OptionButton.size)
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(iconImageView.snp.bottom).offset(5)
            maker.left.equalTo(5)
            maker.bottom.equalTo(-5)
            maker.right.equalTo(-5)
        }
        
        indicatorCoverView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        indicator.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalTo(iconImageView)
        }
    }
}

fileprivate struct Size {
    struct OptionButton {
        static let size = Adaptive.val(40)
    }
    
    struct IconImageView {
        static let size = Adaptive.val(62)
    }
}

