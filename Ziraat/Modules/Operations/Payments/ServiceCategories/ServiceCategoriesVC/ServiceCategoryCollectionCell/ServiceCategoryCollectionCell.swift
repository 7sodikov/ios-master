//
//  ServiceCategoryCollectionCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

protocol ServiceCategoryCollectionCellDelegate: class {
    func optionButtonClicked(in cell: ServiceCategoryCollectionCell)
}

class ServiceCategoryCollectionCell: UICollectionViewCell, ServiceCategoryCollectionCellInstaller {
    var mainView: UIView { contentView }
    var iconImageView: UIImageView!
    var optionButton: UIButton!
    var titleLabel: UILabel!
    var indicatorCoverView: UIView!
    var indicator: MDCActivityIndicator!
    
    weak var delegate: ServiceCategoryCollectionCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        optionButton.addTarget(self, action: #selector(optionButtonClicked(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with title: String?, logoUrl: String) -> ServiceCategoryCollectionCell {
        iconImageView.kf.setImage(with: URL(string: logoUrl), placeholder: nil)
        titleLabel.text = title
        return self
    }
    
    @objc private func optionButtonClicked(_ sender: UIButton) {
        delegate?.optionButtonClicked(in: self)
    }
    
    func indicator(animate: Bool) {
        indicatorCoverView.isHidden = !animate
        if animate {
            indicator.startAnimating()
        } else {
            indicator.stopAnimating()
        }
    }
}
