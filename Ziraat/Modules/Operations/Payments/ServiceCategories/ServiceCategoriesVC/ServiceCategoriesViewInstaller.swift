//
//  ServiceCategoriesViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceCategoriesViewInstaller: ViewInstaller {
    var searchButton: UIButton! { get set }
    var bgImageView: UIImageView! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
    var collectionLayout: UICollectionViewFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
}

extension ServiceCategoriesViewInstaller {
    func initSubviews() {
        searchButton = UIButton()
        searchButton.setImage(UIImage(named: "img_icon_search"), for: .normal)
        
        bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "img_dash_light_background")
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        
        collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.sectionInset = .zero
        collectionLayout.scrollDirection = .vertical
        collectionLayout.minimumInteritemSpacing = 0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = .clear
        collectionView.register(ServiceCategoryCollectionCell.self,
                                forCellWithReuseIdentifier: String(describing: ServiceCategoryCollectionCell.self))
    }
    
    func embedSubviews() {
        mainView.addSubview(bgImageView)
        mainView.addSubview(reloadButtonView)
        mainView.addSubview(collectionView)
    }
    
    func addSubviewsConstraints() {
        bgImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
        
        collectionView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
