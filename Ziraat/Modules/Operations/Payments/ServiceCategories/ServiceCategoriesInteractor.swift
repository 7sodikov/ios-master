//
//  ServiceCategoriesInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ServiceCategoriesInteractorProtocol: class {
    func serviceCategories()
}

class ServiceCategoriesInteractor: ServiceCategoriesInteractorProtocol {
    weak var presenter: ServiceCategoriesInteractorToPresenterProtocol!
    
    func serviceCategories() {
        NetworkService.Payment.serviceCategories { [weak self] (result) in
            self?.presenter.didServiceCategories(with: result)
        }
    }
}
