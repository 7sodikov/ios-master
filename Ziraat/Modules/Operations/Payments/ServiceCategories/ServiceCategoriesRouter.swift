//
//  ServiceCategoriesRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ServiceCategoriesRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToServices(for categoryId: Int, from view: Any?)
    func navigateToServiceSearch(from view: Any?)
}

class ServiceCategoriesRouter: ServiceCategoriesRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ServiceCategoriesVC()
        let presenter = ServiceCategoriesPresenter()
        let interactor = ServiceCategoriesInteractor()
        let router = ServiceCategoriesRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToServices(for categoryId: Int, from view: Any?) {
        let viewCtrl = view as? UIViewController
        let serviceListView = ServiceListRouter.createModule(with: categoryId)
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToServiceSearch(from view: Any?) {
        let viewCtrl = view as? UIViewController
        let searchCtrl = ServiceSearchRouter.createModule()
        let navCtrl = NavigationController(rootViewController: searchCtrl)
        navCtrl.modalPresentationStyle = .fullScreen
        viewCtrl?.present(navCtrl, animated: true, completion: nil)
    }
}
