//
//  ServiceCategoriesVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ServiceCategoryVM {
    var category: ServiceCategoryResponse
    
    init(_ category: ServiceCategoryResponse) {
        self.category = category
    }
}

class ServiceCategoriesVM {
    var categories: [ServiceCategoryResponse] = []
    
    func categoryVM(at index: Int) -> ServiceCategoryVM {
        return ServiceCategoryVM(categories[index])
    }
}
