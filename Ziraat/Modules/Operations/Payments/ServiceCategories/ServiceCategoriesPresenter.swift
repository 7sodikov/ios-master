//
//  ServiceCategoriesPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ServiceCategoriesPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ServiceCategoriesVM { get }
    func viewDidLoad()
    func collectionViewDidSelectItem(at index: Int)
    func searchButtonClicked()
}

protocol ServiceCategoriesInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didServiceCategories(with response: ResponseResult<[ServiceCategoryResponse], AppError>)
}

class ServiceCategoriesPresenter: ServiceCategoriesPresenterProtocol {
    weak var view: ServiceCategoriesVCProtocol!
    var interactor: ServiceCategoriesInteractorProtocol!
    var router: ServiceCategoriesRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ServiceCategoriesVM()
    
    func viewDidLoad() {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.serviceCategories()
    }
    
    func collectionViewDidSelectItem(at index: Int) {
//        guard let id = category.id else { return }
        let category = viewModel.categories[index]
        router.navigateToServices(for: category.id , from: view)
    }
    
    func searchButtonClicked() {
        router.navigateToServiceSearch(from: view)
    }
    
    // Private property and methods
    
}

extension ServiceCategoriesPresenter: ServiceCategoriesInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didServiceCategories(with response: ResponseResult<[ServiceCategoryResponse], AppError>) {
        switch response {
        case let .success(result):
            viewModel.categories = result.data
            view.showReloadButtonForResponses(false, animateReloadButton: false)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            break
        }
    }
}
