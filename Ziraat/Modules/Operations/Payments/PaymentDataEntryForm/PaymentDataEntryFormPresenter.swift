//
//  PaymentDataEntryFormPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentDataEntryFormPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: PaymentDataEntryFormVM { get }
    func viewDidLoad()
    func myCardsClicked()
    func closeBtnClicked()
    func nextBtnClicked()
}

protocol PaymentDataEntryFormInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool)
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>)
    func didPaymentPrepare(with response: ResponseResult<PaymentPrepareResponse, AppError>)
}

class PaymentDataEntryFormPresenter: PaymentDataEntryFormPresenterProtocol, BottomSheetDelegate {
    
    weak var view: PaymentDataEntryFormVCProtocol!
    var interactor: PaymentDataEntryFormInteractorProtocol!
    var router: PaymentDataEntryFormRouterProtocol!
    var controller: PaymentDataEntryFormVC { view as! PaymentDataEntryFormVC }
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PaymentDataEntryFormVM()
    
    init(view: PaymentDataEntryFormVCProtocol!,
         interactor: PaymentDataEntryFormInteractorProtocol!,
         router: PaymentDataEntryFormRouterProtocol!,
         service: ServiceResponse?,
         favoriteItem: FavoriteListPaymentsResponse?,
         payment: CLastPayment?) {
        
        self.view = view
        self.interactor = interactor
        self.router = router
        
        viewModel.service = service
        viewModel.repeatPayment = payment
        viewModel.favoriteItem = favoriteItem
    }
    
    func viewDidLoad() {
     //   view.nextButton(enable: viewModel.isAllFieldsValid)
        view.cardsDownloadPreloader(show: true)
        downloadMyCardsWithBalances(isRequestedByButtonClick: false)
    }
    
    func myCardsClicked() {
        if viewModel.cards.count > 0 {
            openCardsList()
        } else {
            view.cardsDownloadPreloader(show: true)
            downloadMyCardsWithBalances(isRequestedByButtonClick: true)
        }
    }
    
    func closeBtnClicked() {
        router.dismiss(view: view)
    }
    
    func nextBtnClicked() {
        view.preloader(show: true)
        var fileds = viewModel.userEntry.fields
        if fileds.isEmpty,let payment = self.viewModel.repeatPayment {
            payment.fieldValueList.forEach { service in
                fileds[service.name] = service.defaultValue
            }
        }
        
        interactor.paymentPrepare(senderCardId: viewModel.userEntry.payerCard.payerCardId ?? 0,
                                  serviceId: Int64(viewModel.dataEntryFormServiceEntity.serviceId),
                                  serviceType: viewModel.dataEntryFormServiceEntity.serviceType,
                                  fields: fileds)
    }
    
    // Private property and methods
    private let dateFormat = "yyyy-MM-dd"
    
    private func downloadMyCardsWithBalances(isRequestedByButtonClick: Bool) {
        interactor.getAllCards(isRequestedByButtonClick: isRequestedByButtonClick)
        interactor.getAllCardBalances()
    }
    
    private func openCardsList() {
        let bottomSheet = viewModel.cardAccountBottomSheet
        bottomSheet.delegate = self
        let vc = BottomSheetViewController(bottomSheet: bottomSheet)
        vc.modalPresentationStyle = .custom
        controller.presentPanModal(vc)
//        var cardsList: [EZSelectControllerCellModel] = []
//        for i in 0..<viewModel.cards.count {
//            let cardVM = viewModel.cardVM(at: i)
//            let selectionItem = EZSelectControllerCellModel()
//            selectionItem.title = cardVM.card.pan
//            selectionItem.subTitle = cardVM.balanceStr
//            cardsList.append(selectionItem)
//        }
//        router.openCardSelectionView(from: view, for: cardsList, delegate: self)
    }
    
    func bottomSheet(didSelectCardAt indexPath: IndexPath, card: Any, with tag: Int) {
        if let card = card as? Card {
            view.setCardNumber(number: card.pan)
            viewModel.userEntry.setPayer(cardId: card.cardId, pan: card.pan, owner: card.owner)
//            view.nextButton(enable: viewModel.isAllFieldsValid)
        }
    }
}

extension PaymentDataEntryFormPresenter: PaymentDataEntryFormInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>, isRequestedByButtonClick: Bool) {
        switch response {
        case let .success(result):
            viewModel.cardsTest = result.data.cards.compactMap(\.card)
            viewModel.cards = result.data.cards

        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
        view.cardsDownloadPreloader(show: false)
    }
    
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cardBT = result.data.cards?.map(\.cardBalance) ?? []
            viewModel.cardBalances = result.data.cards ?? []
        case let .failure(error):
            print(error)
//            view.showError(message: error.localizedDescription)
            break
        }
    }
    
    
    func didPaymentPrepare(with response: ResponseResult<PaymentPrepareResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            router.navigateToPaymentDataPreview(with: view as Any,
                                                service: viewModel.dataEntryFormServiceEntity,
                                                payerCard: viewModel.userEntry.payerCard,
                                                paymentPrepareResponse: result.data)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}

extension PaymentDataEntryFormPresenter: EZSelectControllerDelegate {
    func numberOfSections(in ezSelectController: EZSelectController) -> Int {
        return 1
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cards.count
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleAndSubtitleForRowAt indexPath: IndexPath) -> (title: String, subtitle: String) {
        let cardVM = viewModel.cardVM(at: indexPath.row)
        return (cardVM.card.pan ?? "", cardVM.balanceStr)
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, didSelectRowAt indexPath: IndexPath) {
        let cardVM = viewModel.cardVM(at: indexPath.row)
        viewModel.userEntry.set(payer: cardVM.card)
        view.showSelectedCard()
//        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func ezSelectController(_ ezSelectController: EZSelectController, titleForHeaderInSection section: Int) -> String? {
        return RS.lbl_cards.localized()
    }
}

extension PaymentDataEntryFormPresenter: EZDatePickerControllerDelegate {
    func ezDatePickerDidSelect(date: Date, userInfo: Any?) {
        guard let fieldResponse = userInfo as? ServiceFieldResponse else { return }
        let dateStr = date.string(for: dateFormat)
        viewModel.userEntry.set(fieldValue: dateStr, for: fieldResponse)
        view.showSelectedDate(for: fieldResponse, fieldValue: dateStr)
//        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
}

extension PaymentDataEntryFormPresenter: PaymentDataEntryFormFieldDelegate {
    func myCardFieldDidClick() {
        myCardsClicked()
    }
    
    func moneyFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
//        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func datePopupFieldDidClick(field: ServiceFieldResponse) {
        let dateStr = viewModel.userEntry.getFieldValue(for: field) as? String
        if let dateStr = dateStr,
           let date = dateStr.date(fromFormat: dateFormat) {
            view.openDatePicker(with: date, userInfo: field)
            
        } else {
            view.openDatePicker(with: Date(), userInfo: field)
        }
    }
    
    func regexBoxFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
//        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func phoneFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
//        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
    
    func stringFieldDidChange(with value: String?, field: ServiceFieldResponse) {
        viewModel.userEntry.set(fieldValue: value, for: field)
//        view.nextButton(enable: viewModel.isAllFieldsValid)
    }
}
