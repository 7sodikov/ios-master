//
//  PaymentDataEntryFormVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentDataEntryFormVCProtocol: BaseViewControllerProtocol {
    func cardsDownloadPreloader(show: Bool)
    func showSelectedCard()
    func showSelectedDate(for fieldResponse: ServiceFieldResponse, fieldValue: String?)
    func nextButton(enable: Bool)
    func openDatePicker(with selectedDate: Date, userInfo: Any)
    func setCardNumber(number: String)
}

class PaymentDataEntryFormVC: BaseViewController, PaymentDataEntryFormViewInstaller {
    var backgroundImage: UIImageView!
    var formStackView: UIStackView!
    var buttonsVerticalStackView: UIStackView!
    var coverStackView: UIStackView!
    var operationStackOrderView: OperationStackOrderView!
    var cardFieldView: PaymentDataEntryFormMyCardField!
    var closeButton: Button!
    var nextButton: Button!
    var mainView: UIView { view }
    var presenter: PaymentDataEntryFormPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        drawView()
        presenter.viewDidLoad()
    }
    
    private func drawView() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = true
        Utils.delay(seconds: 0.3) {
            UIView.animate(withDuration: 0.3) {
                self.navigationController?.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
        }
        
        title = presenter.viewModel.dataEntryFormServiceEntity.title
        cardFieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
        // showing initial value
        cardFieldView.textField.text = presenter.viewModel.userEntry.payerCard.selectedPayerNumber
        
        for field in presenter.viewModel.dataEntryFormServiceEntity.fields {
            switch field.enumType {
            case .money:
                let fieldView = PaymentDataEntryFormMoneyField(frame: .zero)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                fieldView.setup(for: field)
                formStackView.addArrangedSubview(fieldView)
            case .datePopup:
                let fieldView = PaymentDataEntryFormDatePopupField(frame: .zero)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                fieldView.setup(for: field)
                formStackView.addArrangedSubview(fieldView)
            case .regexBox:
                let fieldView = PaymentDataEntryFormRegexBoxField(frame: .zero)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                fieldView.setup(for: field)
                formStackView.addArrangedSubview(fieldView)
            case .phone:
                let fieldView = PaymentDataEntryFormPhoneField(frame: .zero)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                fieldView.setup(for: field)
                formStackView.addArrangedSubview(fieldView)
            case .string:
                let fieldView = PaymentDataEntryFormStringField(frame: .zero)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                fieldView.setup(for: field)
                formStackView.addArrangedSubview(fieldView)
            case .select:
                print("Select ---------")
            case .none:
                break
            }
        }
        
        closeButton.addTarget(self, action: #selector(closeBtnClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
    //    nextButton(enable: presenter.viewModel.isAllFieldsValid)
    }
    
    private func drawViewRepayment() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.hidesBackButton = true
        Utils.delay(seconds: 0.3) {
            UIView.animate(withDuration: 0.3) {
                self.navigationController?.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
        }
        
        title = presenter.viewModel.dataEntryFormServiceEntity.title
        
        cardFieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
        // showing initial value
        cardFieldView.textField.text = presenter.viewModel.userEntry.payerCard.selectedPayerNumber
        
        for field in presenter.viewModel.dataEntryFormServiceEntity.fields {
            switch field.enumType {
            case .money:
                let fieldView = PaymentDataEntryFormMoneyField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                formStackView.addArrangedSubview(fieldView)
            case .datePopup:
                let fieldView = PaymentDataEntryFormDatePopupField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                formStackView.addArrangedSubview(fieldView)
            case .regexBox:
                let fieldView = PaymentDataEntryFormRegexBoxField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                formStackView.addArrangedSubview(fieldView)
            case .phone:
                let fieldView = PaymentDataEntryFormPhoneField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                formStackView.addArrangedSubview(fieldView)
            case .string:
                let fieldView = PaymentDataEntryFormStringField(frame: .zero)
                fieldView.setup(for: field)
                fieldView.delegate = (presenter as? PaymentDataEntryFormPresenter)
                formStackView.addArrangedSubview(fieldView)
            case .select:
                print("Select ---------")
            case .none:
                break
            }
        }
        
        closeButton.addTarget(self, action: #selector(closeBtnClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
        nextButton(enable: presenter.viewModel.isAllFieldsValid)
    }
    
    func setCardNumber(number: String) {
        cardFieldView.textField.text = number
    }
    
    override func preloader(show: Bool) {
//        nextButton.
        closeButton.isEnabled = !show
    }
    
    @objc private func closeBtnClicked(_ sender: NextButton) {
        presenter.closeBtnClicked()
    }
    
    @objc private func nextBtnClicked(_ sender: NextButton) {
        presenter.nextBtnClicked()
    }
}

extension PaymentDataEntryFormVC: PaymentDataEntryFormVCProtocol {
    func cardsDownloadPreloader(show: Bool) {
        cardFieldView.animateIndicator = show
    }
    
    func showSelectedCard() {
        cardFieldView.textField.text = presenter.viewModel.userEntry.payerCard.selectedPayerNumber
    }
    
    func showSelectedDate(for fieldResponse: ServiceFieldResponse, fieldValue: String?) {
        for fieldView in formStackView.arrangedSubviews {
            guard let datePopupField = fieldView as? PaymentDataEntryFormDatePopupField else { continue }
            if datePopupField.fieldResponse.name == fieldResponse.name {
                datePopupField.textField.text = fieldValue
                return
            } else {
                continue
            }
        }
    }
    
    func nextButton(enable: Bool) {
        nextButton.isEnabled = enable
    }
    
    func openDatePicker(with selectedDate: Date, userInfo: Any) {
        let datePicker = EZDatePickerController()
        datePicker.delegate = presenter as? PaymentDataEntryFormPresenter
        datePicker.userInfo = userInfo
        datePicker.open(in: self, selectedDate: selectedDate)
    }
}
