//
//  PaymentDataEntryFormMyCardField.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PaymentDataEntryFormMyCardField: PaymentDataEntryFormField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setPreloader()
        let tap = UITapGestureRecognizer(target: self, action: #selector(textFieldDidClick(_:)))
        textField.addGestureRecognizer(tap)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    @objc private func textFieldDidClick(_ gesture: UITapGestureRecognizer) {
        delegate?.myCardFieldDidClick()
    }
}
