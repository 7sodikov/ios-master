//
//  PaymentDataEntryFormDatePopupField.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PaymentDataEntryFormDatePopupField: PaymentDataEntryFormField {
    var fieldResponse: ServiceFieldResponse!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let tap = UITapGestureRecognizer(target: self, action: #selector(textFieldDidClick(_:)))
        textField.addGestureRecognizer(tap)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    @objc private func textFieldDidClick(_ gesture: UITapGestureRecognizer) {
        delegate?.datePopupFieldDidClick(field: fieldResponse)
    }
    
    func setup(for fieldResponse: ServiceFieldResponse) {
        self.fieldResponse = fieldResponse
        self.set(title: fieldResponse.title, with: fieldResponse.fieldValueListRequired)
        textField.placeholder = fieldResponse.title
        textField.isUserInteractionEnabled = !fieldResponse.readOnly
        
        if let value = fieldResponse.defaultValue {
            textField.text = value
        }
        
    }
}
