//
//  PaymentDataEntryFormRegexBoxField.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PaymentDataEntryFormRegexBoxField: PaymentDataEntryFormField {
    var fieldResponse: ServiceFieldResponse!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    func setup(for fieldResponse: ServiceFieldResponse) {
        self.fieldResponse = fieldResponse
        self.set(title: fieldResponse.title, with: fieldResponse.fieldValueListRequired)
        textField.placeholder = fieldResponse.title
        textField.isUserInteractionEnabled = !fieldResponse.readOnly
        if let value = fieldResponse.defaultValue {
            textField.text = value
        }
    }
}
