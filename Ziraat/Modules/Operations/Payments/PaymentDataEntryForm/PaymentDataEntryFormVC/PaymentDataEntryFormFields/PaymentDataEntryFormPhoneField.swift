//
//  PaymentDataEntryFormPhoneField.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import InputMask

class PaymentDataEntryFormPhoneField: PaymentDataEntryFormField {
    var fieldResponse: ServiceFieldResponse!
    
    private lazy var phoneFieldListener: MaskedTextFieldDelegate = {
        let listener = MaskedTextFieldDelegate()
        listener.affinityCalculationStrategy = .prefix
        listener.primaryMaskFormat = "+998 [00] [000] [00] [00]"
        return listener
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        textField.keyboardType = .phonePad
        textField.delegate = phoneFieldListener
        phoneFieldListener.listener = self
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    func setup(for fieldResponse: ServiceFieldResponse) {
        self.fieldResponse = fieldResponse
        self.set(title: fieldResponse.title, with: fieldResponse.fieldValueListRequired)
        textField.placeholder = fieldResponse.title
        textField.isUserInteractionEnabled = !fieldResponse.readOnly
        
        // setting the initial value (i.e. from favorites)
        let value = "998" + (fieldResponse.defaultValue ?? "")
        phoneFieldListener.put(text: value, into: textField)
        if value.count == 12 {
            delegate?.phoneFieldDidChange(with: fieldResponse.defaultValue, field: fieldResponse)
        }
    }
}

extension PaymentDataEntryFormPhoneField: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        let val = complete ? value : nil
        delegate?.phoneFieldDidChange(with: val, field: fieldResponse)
    }
}
