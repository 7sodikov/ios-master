//
//  PaymentDataEntryFormMoneyField.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PaymentDataEntryFormMoneyField: PaymentDataEntryFormField {
    var fieldResponse: ServiceFieldResponse!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    func setup(for fieldResponse: ServiceFieldResponse) {
        self.fieldResponse = fieldResponse
        self.set(title: fieldResponse.title, with: fieldResponse.fieldValueListRequired)
        textField.placeholder = fieldResponse.title
        textField.isUserInteractionEnabled = !fieldResponse.readOnly
        if let value = fieldResponse.defaultValue, let doubleValue = Double(value) {
            let amount = (doubleValue / 100).description
            textField.text = amount.makeReadableAmount
            delegate?.moneyFieldDidChange(with: amount, field: fieldResponse)
        }
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        guard let amountString = sender.text?.digits else {
            delegate?.moneyFieldDidChange(with: nil, field: fieldResponse)
            return
        }
        
        guard let amount = Int64(amountString) else {
            delegate?.moneyFieldDidChange(with: nil, field: fieldResponse)
            return
        }
        
        textField.text = amount.currencyFormattedStr()
        delegate?.moneyFieldDidChange(with: amountString, field: fieldResponse)
    }
}
