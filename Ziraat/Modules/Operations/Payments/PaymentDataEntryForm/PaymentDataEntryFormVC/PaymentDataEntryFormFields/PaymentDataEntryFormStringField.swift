//
//  PaymentDataEntryFormStringField.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PaymentDataEntryFormStringField: PaymentDataEntryFormField {
    var fieldResponse: ServiceFieldResponse!
    override init(frame: CGRect) {
        super.init(frame: frame)
        textField.keyboardType = .default
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    func setup(for fieldResponse: ServiceFieldResponse) {
        self.fieldResponse = fieldResponse
        self.set(title: fieldResponse.title, with: fieldResponse.fieldValueListRequired)
        textField.placeholder = fieldResponse.title
        textField.isUserInteractionEnabled = !fieldResponse.readOnly
        
        // setting the initial value (i.e. from favorites)
        if let value = fieldResponse.defaultValue {
            textField.text = value
            delegate?.stringFieldDidChange(with: value, field: fieldResponse)
        }
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        guard let text = sender.text else {
            delegate?.stringFieldDidChange(with: nil, field: fieldResponse)
            return
        }
        delegate?.stringFieldDidChange(with: text, field: fieldResponse)
    }
}

