//
//  PaymentDataEntryFormInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentDataEntryFormInteractorProtocol: AnyObject {
    func getAllCards(isRequestedByButtonClick: Bool)
    func getAllCardBalances()
    func paymentPrepare(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any])
}

class PaymentDataEntryFormInteractor: PaymentDataEntryFormInteractorProtocol {
    weak var presenter: PaymentDataEntryFormInteractorToPresenterProtocol!
    
    func getAllCards(isRequestedByButtonClick: Bool) {
        NetworkService.Card.cardsList { [weak self] (result) in
            self?.presenter.didGetAllCards(with: result, isRequestedByButtonClick: isRequestedByButtonClick)
        }
    }
    
    func getAllCardBalances() {
        NetworkService.Card.cardBalance { [weak self] (result) in
            self?.presenter.didGetAllCardBalances(with: result)
        }
    }
    
    func paymentPrepare(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any]) {
        NetworkService.Payment.paymentPrepare(senderCardId: senderCardId,
                                              serviceId: serviceId,
                                              serviceType: serviceType,
                                              fields: fields) { [weak self] (result) in
            self?.presenter.didPaymentPrepare(with: result)
        }
    }
}
