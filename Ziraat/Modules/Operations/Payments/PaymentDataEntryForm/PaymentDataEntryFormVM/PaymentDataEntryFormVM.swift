//
//  PaymentDataEntryFormVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class PaymentDataEntryFormVM {
    let userEntry = PaymentDataEntryFormUserEntry()
    var service: ServiceResponse? {
        didSet {
            fillServiceEntityFromServiceResponse()
        }
    }
    
    var repeatPayment: CLastPayment? {
        didSet {
            fillRepayFromServiceResponse()
        }
    }
    
    var favoriteItem: FavoriteListPaymentsResponse? {
        didSet {
            fillServiceEntityFromFavoriteResponse()
        }
    }
    
    var dataEntryFormServiceEntity = PaymentDataEntryFormServiceEntity()
    
    var dataEntryFromRepayment = PaymentDataEntryFormRepayment()
    
    var cards: [CardResponse] = []
    var cardBalances: [CardBalanceResponse] = []
    var cardsTest: [Card] = []
    var cardBT: [CardBalance] = []
    
    var cardAccountBottomSheet: BottomSheet {
        let vms = cardsTest.map { CardCellViewModel(card: $0) }
        let rows = [CardsRouletteModel(header: RS.lbl_cards.localized(), viewModels: vms)]
        let cardSection = BottomSheetSection(header: "", rows: rows)
        
        return .init(viewHeight: 180, dataSource: [cardSection], with: 0)
    }
    
    func updateCard() {
        
        cardsTest = cardsTest.map { item -> Card in
            var card = item
            if let cardBalance = cardBT.first(where: { $0.cardId.description == card.cardId }) {
                card.update(balance: cardBalance.balance, currency: cardBalance.currency)
            }
            return card
        }
    }
    
    private func fillServiceEntityFromServiceResponse() {
        guard let service = service else { return }
        
        dataEntryFormServiceEntity.maxAmount = service.maxAmount
        dataEntryFormServiceEntity.minAmount = service.minAmount
        dataEntryFormServiceEntity.serviceType = service.serviceType
        dataEntryFormServiceEntity.title = service.title
        dataEntryFormServiceEntity.logoUrl = service.logoURL
        dataEntryFormServiceEntity.serviceId = service.serviceID
        dataEntryFormServiceEntity.fields = service.fields
    }
    
    private func fillRepayFromServiceResponse() {
        guard let repaymentItem = repeatPayment else { return }
        
//        dataEntryFromRepayment.serviceId = repaymentItem.serviceId
//        dataEntryFromRepayment.operationId = repaymentItem.operationId
//        dataEntryFromRepayment.currency = repaymentItem.currency
//        dataEntryFromRepayment.icon = repaymentItem.icon
//        dataEntryFromRepayment.fieldValueList = repaymentItem.fieldValueList
//        dataEntryFromRepayment.operationType = repaymentItem.operationType
//        dataEntryFromRepayment.amount = repaymentItem.amount
//        dataEntryFromRepayment.operationMode = repaymentItem.operationMode
//        dataEntryFromRepayment.date = repaymentItem.date
//        dataEntryFromRepayment.receiverOwner = repaymentItem.receiverOwner
//        dataEntryFromRepayment.time = repaymentItem.time
//        dataEntryFromRepayment.receiverBranch = repaymentItem.receiverBranch
//        dataEntryFromRepayment.total = repaymentItem.total
//        dataEntryFromRepayment.operationCode = repaymentItem.operationCode
//        dataEntryFromRepayment.senderPan = repaymentItem.senderPan
//        dataEntryFromRepayment.status = repaymentItem.status
//        dataEntryFromRepayment.serviceType = repaymentItem.serviceType
//        dataEntryFromRepayment.commission = repaymentItem.commission
        
        dataEntryFormServiceEntity.serviceType = repaymentItem.serviceType
        dataEntryFormServiceEntity.title = repaymentItem.receiverOwner
        dataEntryFormServiceEntity.logoUrl = repaymentItem.icon
        if let serviceId = Int64(repaymentItem.serviceId) {
            dataEntryFormServiceEntity.serviceId = Int(serviceId)
        }
        dataEntryFormServiceEntity.fields = repaymentItem.fieldValueList
        
        userEntry.payerCard.selectedPayerNumber = repaymentItem.sender
        userEntry.payerCard.payerCardId = repaymentItem.senderId
        userEntry.payerCard.payerCardOwner = repaymentItem.senderOwner
        
//        for fieldValue in repaymentItem.fieldValueList {
//            if fieldValue.type == ServiceFieldType.money.rawValue {
//                guard let strValue = fieldValue.defaultValue,
//                      let numberValue = UInt64(strValue) else {
//                    continue
//                }
//                let money = numberValue/100
//                userEntry.set(fieldValue: String(money), for: fieldValue)
//            } else {
//                userEntry.set(fieldValue: fieldValue.defaultValue, for: fieldValue)
//            }
//        }
    }
    
    private func fillServiceEntityFromFavoriteResponse() {
        guard let favoriteItem = favoriteItem else { return }
        
//        dataEntryFormServiceEntity.maxAmount =
//        dataEntryFormServiceEntity.minAmount =
        dataEntryFormServiceEntity.serviceType = favoriteItem.serviceType
        dataEntryFormServiceEntity.title = favoriteItem.serviceTitle
        dataEntryFormServiceEntity.logoUrl = favoriteItem.logo
        if let serviceId = Int(favoriteItem.serviceID ?? "") {
            dataEntryFormServiceEntity.serviceId = serviceId
        }
        dataEntryFormServiceEntity.fields = favoriteItem.fieldValueList
        
        userEntry.payerCard.selectedPayerNumber = favoriteItem.sender
        userEntry.payerCard.payerCardId = UInt64(favoriteItem.senderID)
        userEntry.payerCard.payerCardOwner = favoriteItem.senderOwner
        
//        for fieldValue in favoriteItem.fieldValueList {
//            if fieldValue.type == ServiceFieldType.money.rawValue {
//                guard let strValue = fieldValue.defaultValue,
//                      let numberValue = UInt64(strValue) else {
//                    continue
//                }
//                let money = numberValue/100
//                userEntry.set(fieldValue: String(money), for: fieldValue)
//            } else {
//                userEntry.set(fieldValue: fieldValue.defaultValue, for: fieldValue)
//            }
//        }
    }
    
    func cardVM(at index: Int) -> DashboardCardVM {
        var balance: CardBalanceResponse?
        let card = cards[index]
        for cardBalance in cardBalances {
            if cardBalance.cardId == card.cardId {
                balance = cardBalance
                break
            }
        }
        
        return DashboardCardVM(card, balance)
    }
    
    var isAllFieldsValid: Bool {
        var isValid = false
        for i in 0..<dataEntryFormServiceEntity.fields.count {
            let field = dataEntryFormServiceEntity.fields[i]
            let fieldType = ServiceFieldType.getType(for: field.type)
            
            if let fieldValue = userEntry.fields[field.name] {
                if fieldType == .money {
                    guard let amountValue = fieldValue as? Double else {
                        isValid = false
                        break
                    }
                    
                    if amountValue < 100000 {
                        isValid = false
                        break
                    }
                }
                
                if i == dataEntryFormServiceEntity.fields.count - 1 { // last element
                    isValid = true
                    break
                }
                continue
            }
            isValid = false
            break
        }
        
        if userEntry.payerCard.payerCardId == nil ||
            userEntry.payerCard.payerCardId == 0 {
            isValid = false
        }
        
        return isValid
    }
}
