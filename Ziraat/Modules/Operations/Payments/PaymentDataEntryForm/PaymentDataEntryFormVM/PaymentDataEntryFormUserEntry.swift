//
//  PaymentDataEntryFormUserEntry.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class PaymentDataEntryFormUserEntry {
    var payerCard = OperationPagePA2PDataEntryUserEntryPayer() // Card
    private(set) var fields: [String: Any] = [:]
    
    func set(payer: CardResponse) {
        payerCard.selectedPayerNumber = payer.pan
        payerCard.payerCardId = payer.cardId
        payerCard.payerCardOwner = payer.owner
    }
    
    func setPayer(cardId: String, pan: String, owner: String) {
        payerCard.selectedPayerNumber = pan
        payerCard.payerCardId = UInt64(cardId)
        payerCard.payerCardOwner = owner
    }
    
    func set(fieldValue: String?, for fieldResponse: ServiceFieldResponse) {
        switch fieldResponse.enumType {
        case .money:
            guard let fieldValue = fieldValue, !fieldValue.isEmpty else {
                fields[fieldResponse.name] = nil
                return
            }
            guard let doubleValue = Double(fieldValue) else {
                fields[fieldResponse.name] = nil
                return
            }
            fields[fieldResponse.name] = doubleValue * 100
            
        case .datePopup:
            fields[fieldResponse.name] = fieldValue
        case .regexBox:
            fields[fieldResponse.name] = fieldValue
        case .phone:
            guard let fieldValue = fieldValue, !fieldValue.isEmpty else {
                fields[fieldResponse.name] = nil
                return
            }
            fields[fieldResponse.name] = fieldValue
        case .string:
            fields[fieldResponse.name] = fieldValue
        default:
            break
        }
    }
    
    func getFieldValue(for fieldResponse: ServiceFieldResponse) -> Any? {
        return fields[fieldResponse.name]
    }
}
