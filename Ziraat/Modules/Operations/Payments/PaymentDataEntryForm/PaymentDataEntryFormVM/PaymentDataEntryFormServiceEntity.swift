 //
//  PaymentDataEntryFormServiceEntity.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class PaymentDataEntryFormServiceEntity {
    var maxAmount: Int = 0
    var minAmount: Int = 0
    var serviceType: Int = 0
    var title: String?
    var logoUrl: String?
    var serviceId: Int = 0
    var fields: [ServiceFieldResponse] = []
    
    var selectedRegionId: String = ""
    var selectedDistrictId: String = ""
     
    var cityField: ServiceFieldResponse? {
        fields.first(where: {$0.regions.isNotEmpty})
    }
    
    var regionViewModels: [SingleLabelCellViewModel] {
        cityField?.regionViewModels ?? []
    }
    
    var districtViewModels: [SingleLabelCellViewModel] {
        cityField?.districtViewModels(at: selectedRegionId) ?? []
    }
    
    var personViewModel: [SingleLabelCellViewModel] {
        fields.first(where: { $0.fieldSections.isNotEmpty })?.personViewModel ?? []
    }
    
    var selectedRegionName: String {
        cityField?.regions.first(where: { $0.value == selectedRegionId })?.title ?? ""
    }
    
    var selectedDistrictName: String  {
        cityField?.getDistricts(at: selectedRegionId).first(where: {$0.value == selectedDistrictId})?.title ?? ""
    }
    
    func setdefaults() {
        selectedRegionId = cityField?.regions.first?.value ?? ""
        selectedDistrictId = cityField?.getDistricts(at: selectedRegionId).first?.value ?? ""
    }

}
 

 class PaymentDataEntryFormRepayment {
    var serviceId: String = ""
    var operationId: Int = 0
    var currency: String = ""
    var sender: String = ""
    var icon: String = ""
    var fieldValueList: [ServiceFieldResponse] = []
    var operationType: String = ""
    var amount: Int = 0
    var operationMode: OperationMode = .p2s
    var date: String = ""
    var receiverOwner: String = ""
    var time: String = ""
    var receiverBranch: String = ""
    var total: Int = 0
    var operationCode: String = ""
    var senderOwner: String = ""
    var senderId: Int = 0
    var senderPan: String = ""
    var status: String = ""
    var serviceType: Int = 0
    var commission: Int = 0
 }
