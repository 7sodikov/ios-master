//
//  PaymentDataEntryFormRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentDataEntryFormRouterProtocol: AnyObject {
    static func createModule(for service: ServiceResponse?, favoriteItem: FavoriteListPaymentsResponse?, payment: CLastPayment?) -> UIViewController
    func openCardSelectionView(from controller: Any?,
                               for cardsList: [EZSelectControllerCellModel],
                               delegate: EZSelectControllerDelegate)
    func navigateToPaymentDataPreview(with view: Any,
                                      service: PaymentDataEntryFormServiceEntity,
                                      payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                      paymentPrepareResponse: PaymentPrepareResponse)
    func dismiss(view: Any?)
}

class PaymentDataEntryFormRouter: PaymentDataEntryFormRouterProtocol {
    static func createModule(for service: ServiceResponse?, favoriteItem: FavoriteListPaymentsResponse?, payment: CLastPayment?) -> UIViewController {
        let vc = PaymentDataEntryFormVC()
        let interactor = PaymentDataEntryFormInteractor()
        let router = PaymentDataEntryFormRouter()
        let presenter = PaymentDataEntryFormPresenter(view: vc, interactor: interactor, router: router, service: service, favoriteItem: favoriteItem, payment: payment)
        
        vc.presenter = presenter
        interactor.presenter = presenter
        return vc
    }
    
    func openCardSelectionView(from controller: Any?,
                               for cardsList: [EZSelectControllerCellModel],
                               delegate: EZSelectControllerDelegate) {
        let selectionCtrl = EZSelectController()
        selectionCtrl.delegate = delegate
//        let navCtrl = selectionCtrl.wrapIntoNavigation()
        (controller as? UIViewController)?.present(selectionCtrl, animated: true, completion: nil)
    }
    
    func navigateToPaymentDataPreview(with view: Any,
                                      service: PaymentDataEntryFormServiceEntity,
                                      payerCard: OperationPagePA2PDataEntryUserEntryPayer,
                                      paymentPrepareResponse: PaymentPrepareResponse) {
        let previewCtrl = PaymentDataPreviewFormRouter.createModule(with: service,
                                                                    payerCard: payerCard,
                                                                    paymentPrepareResponse: paymentPrepareResponse)
        (view as? PaymentDataEntryFormVC)?.navigationController?.pushViewControllerRetro(previewCtrl)
    }
    
    func dismiss(view: Any?) {
        let controller = view as? UIViewController
        
        UIView.animate(withDuration: defaultAnimationDuration, animations: {
            controller?.navigationController?.view.backgroundColor = UIColor.clear
        }) { (isCompleted) in
            controller?.dismiss(animated: true, completion: nil)
        }
    }
}
