//
//  PaymentDataEntryFormViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PaymentDataEntryFormViewInstaller: ViewInstaller {
    var backgroundImage: UIImageView! {get set}
    var formStackView: UIStackView! {get set}
    var coverStackView: UIStackView! {get set}
    var buttonsVerticalStackView: UIStackView! { get set }
    var operationStackOrderView: OperationStackOrderView! {get set}
    var cardFieldView: PaymentDataEntryFormMyCardField! { get set }
    var closeButton: Button! { get set }
    var nextButton: Button! { get set }
}

extension PaymentDataEntryFormViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .clear
        backgroundImage = .init(image: UIImage(named: "img_dash_light_background"))
        operationStackOrderView = OperationStackOrderView(numberOfOperation: .three)
        operationStackOrderView.setup(current: .three)
        
        coverStackView = UIStackView()
        coverStackView.axis = .vertical
        coverStackView.spacing = Adaptive.val(24)
        
        
        formStackView = UIStackView()
        formStackView.axis = .vertical
        formStackView.spacing = Adaptive.val(10)
        formStackView.translatesAutoresizingMaskIntoConstraints = false
        
        buttonsVerticalStackView = UIStackView()
        buttonsVerticalStackView.axis = .vertical
        buttonsVerticalStackView.alignment = .center
        buttonsVerticalStackView.spacing = Adaptive.val(10)
        
        cardFieldView = PaymentDataEntryFormMyCardField(frame: .zero)
        cardFieldView.setup(title: RS.lbl_cards.localized(), value: RS.lbl_cards.localized(), isRequired: true)
        
        nextButton = Button(method: .local, style: .next)
        nextButton.setup()
        
        closeButton = Button(method: .network, style: .close)
        closeButton.setup()
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundImage)
        mainView.addSubview(operationStackOrderView)
        mainView.addSubview(coverStackView)
        mainView.addSubview(buttonsVerticalStackView)
        
        coverStackView.addArrangedSubview(formStackView)
        coverStackView.addArrangedSubview(UIStackView())
        
        buttonsVerticalStackView.addArrangedSubview(nextButton)
        buttonsVerticalStackView.addArrangedSubview(closeButton)
    
        formStackView.addArrangedSubview(cardFieldView)
    }
    
    func addSubviewsConstraints() {
        backgroundImage.constraint { [self] (make) in
            make.top(mainView.topAnchor)
                .bottom(mainView.bottomAnchor)
                .horizontal.equalToSuperView()
        }
        operationStackOrderView.constraint { $0.top(16).centerX.equalToSuperView() }
        
        coverStackView.constraint { (make) in
            make.top(self.operationStackOrderView.bottomAnchor, offset: 16)
                .bottom(self.buttonsVerticalStackView.topAnchor, offset: -16)
                .horizontal(16).equalToSuperView()
        }
        buttonsVerticalStackView.constraint { make in
            make.height(110).bottom(-16).horizontal(16).equalToSuperView()
        }
    }
}

