//
//  OperationsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol OperationsInteractorProtocol: class {
    
}

class OperationsInteractor: OperationsInteractorProtocol {
    weak var presenter: OperationsInteractorToPresenterProtocol!
    
}
