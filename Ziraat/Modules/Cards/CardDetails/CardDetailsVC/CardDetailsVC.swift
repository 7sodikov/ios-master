//
//  CardDetailsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol CardDetailsVCProtocol: BaseViewControllerProtocol {
    func present(_ controller : UIViewController)
    func reloadData()
}

class CardDetailsVC: BaseViewController, CardDetailsViewInstaller {
    var cardDetailsView: CardDetailsView!
    var cardHolderLabel: UILabel!
    
    var cardView: CardView!
    var bgView: UIImageView!
    var homeButton: UIButton!
    var titleView: UILabel!
    var historyButton: NextButton!
    var blockButton: NextButton!
    var operationButton: NextButton!
    
    var mainView: UIView { view }
    
    var presenter: CardDetailsPresenterProtocol!
    //var cardInfo : DashboardCardVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupSubviews()
        setupNavigation()
        reloadData()
        
        cardView.eyeIconContainer.addTarget(self, action: #selector(hideBalanceClicked), for: .touchUpInside)
        historyButton.addTarget(self, action: #selector(historyClicked), for: .touchUpInside)
        operationButton.addTarget(self, action: #selector(operationClicked), for: .touchUpInside)
        homeButton.addTarget(self, action: #selector(self.openDashboard), for: .touchUpInside)
        blockButton.addTarget(self, action: #selector(blockUnblockClicked), for: .touchUpInside)
    }
    
    @objc func hideBalanceClicked(){
        UDManager.hideBalance = !UDManager.hideBalance
        reloadData()
    }
    
    private func setupNavigation() {
        self.navigationItem.titleView = titleView
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: homeButton)
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func blockUnblockClicked() {
        presenter.blockOrUnblock()
    }
    
    @objc func historyClicked(){
        presenter.openHistory()
    }
    
    @objc func operationClicked(){
        presenter.showTransacations()
    }
}

extension CardDetailsVC: CardDetailsVCProtocol {
    func present(_ controller: UIViewController) {
        controller.transitioningDelegate = self
        self.present(controller, animated: true, completion: nil)
    }
    func reloadData(){
        let cardInfo = presenter.viewModel.cardInfo
        cardView.dashboardCard = presenter.viewModel.cardInfo
        cardHolderLabel.text = (cardInfo?.card.pan ?? "") + "\n" + (cardInfo?.card.owner ?? "")
        cardDetailsView.cardVM = cardInfo
    }
}

extension CardDetailsVC: UIViewControllerTransitioningDelegate {

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentController = PresentationController(presentedViewController: presented, presenting: presenting)
        if let obj = presented as? ActionsViewController{
            let h : CGFloat = CGFloat(Double(obj.actionList.count) * 50.0 + 60.0 + 70)
            presentController.presentContainerRect = CGRect(x: 0, y: UIScreen.main.bounds.height - h, width: UIScreen.main.bounds.width, height: h)
        }
        
        return presentController
    }
}
