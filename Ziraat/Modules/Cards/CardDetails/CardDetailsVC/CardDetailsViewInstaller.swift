//
//  CardDetailsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit 

protocol CardDetailsViewInstaller: ViewInstaller {
    var titleView: UILabel! { get set }
    var cardView : CardView! {get set}
    var cardDetailsView : CardDetailsView! {get set}
    var bgView: UIImageView! { get set }
    var homeButton: UIButton! { get set }
    var cardHolderLabel: UILabel! { get set }
    var historyButton: NextButton! { get set }
    var blockButton: NextButton! { get set }
    var operationButton: NextButton! { get set }
    
}

extension CardDetailsViewInstaller {
    func initSubviews() {
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        
        homeButton = UIButton()
        homeButton.setBackgroundImage(UIImage(named: "img_icon_home"), for: .normal)
        
        titleView = UILabel()
        titleView.textColor = .timerGray
        titleView.font = .gothamNarrow(size: 16, .bold)
        titleView.text = RS.nav_ttl_card_details.localized()
        
        cardView = CardView(frame: .zero)
        cardView.changeFontSize(10)
        
        cardDetailsView = CardDetailsView(frame: .zero)
        
        cardHolderLabel = UILabel()
        cardHolderLabel.font = .gothamNarrow(size: 16, .medium)//EZFontType.regular.sfProDisplay(size: Adaptive.val(14))
        cardHolderLabel.textColor = .timerGray
        cardHolderLabel.numberOfLines = 4
        
        historyButton = NextButton.systemButton(with: RS.lbl_history.localized(), cornerRadius: Size.Button.big/2)
        
        blockButton = NextButton.systemButton(with: RS.btn_block.localized(), cornerRadius: Size.Button.small/2)
        
        operationButton = NextButton.systemButton(with: RS.lbl_transaction.localized(), cornerRadius: Size.Button.small/2)

        historyButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        historyButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        
        blockButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        blockButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        
        operationButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        operationButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        
        operationButton.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        blockButton.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        historyButton.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
                
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        
        mainView.addSubview(cardHolderLabel)
        mainView.addSubview(cardView)
        mainView.addSubview(cardDetailsView)
        
        mainView.addSubview(historyButton)
        mainView.addSubview(blockButton)
        mainView.addSubview(operationButton)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.remakeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        cardHolderLabel.snp.remakeConstraints { (make) in
            make.left.equalTo(Adaptive.val(20))
            make.top.equalTo(Adaptive.val(119))
            make.right.equalTo(-Adaptive.val(-20))
        }
        
        let left = Adaptive.val(20)
        cardView.snp.remakeConstraints { (make) in
            make.top.equalTo(cardHolderLabel.snp.bottom).offset(10)
            make.width.equalTo(Adaptive.val(216))//.equalToSuperview().multipliedBy(0.75)
            make.centerX.equalToSuperview()
            make.height.equalTo(Adaptive.val(129))//.multipliedBy(53.98/85.6)
        }
        
        cardDetailsView.snp.remakeConstraints { (make) in
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.top.equalTo(cardView.snp.bottom).offset(10)
        }
        let btnLeft = Adaptive.val(30)
        
        historyButton.snp.makeConstraints { (make) in
            make.left.equalTo(btnLeft)
            make.right.equalTo(-btnLeft)
            make.height.equalTo(Size.Button.big)
            make.top.equalTo(cardDetailsView.snp.bottom).offset(Adaptive.val(15))
        }
        
        blockButton.snp.makeConstraints { (make) in
            make.left.equalTo(btnLeft)
            make.top.equalTo(historyButton.snp.bottom).offset(Adaptive.val(15))
            make.height.equalTo(Size.Button.small)
            make.width.equalTo(historyButton.snp.width).multipliedBy(0.5).offset(-5)
        }
        
        operationButton.snp.makeConstraints { (make) in
//            make.left.equalTo(btnLeft)
            make.right.equalTo(-btnLeft)
            make.top.equalTo(historyButton.snp.bottom).offset(Adaptive.val(15))
            make.height.equalTo(Size.Button.small)
            make.width.equalTo(historyButton.snp.width).multipliedBy(0.5).offset(-5)
        }
        
    }
}

fileprivate struct Size {
    struct Button {
        static let big = Adaptive.val(45)
        static let small = Adaptive.val(45)
    }
}
