//
//  CardDetailsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import PanModal

protocol CardDetailsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: CardDetailsVM { get }
    func openHistory()
    func showTransacations()
    func blockOrUnblock()
    func deleteCard(id: Int)
}

protocol CardDetailsInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didCardBlock(with response: ResponseResult<SingleMessageResponse, AppError>)
    func didCardDelete(with response: ResponseResult<CardDeleteResponse, AppError>)
    
}

class CardDetailsPresenter: CardDetailsPresenterProtocol {
    
    
    unowned var view: CardDetailsVCProtocol!
    var interactor: CardDetailsInteractorProtocol!
    var router: CardDetailsRouterProtocol!
    
    var controller: CardDetailsVC { view as! CardDetailsVC }
    // VIEW -> PRESENTER
    private(set) var viewModel = CardDetailsVM()
    
    // Private property and methods
    func openHistory() {
        router.openCardHistory(view as Any, viewModel.cardInfo!)
    }
    
    func blockOrUnblock(){
        let card = viewModel.cardInfo?.card
        //let cardId = card?.cardId ?? 0
        let status = (card?.status ?? "").lowercased()
        if status == "active"{
            viewModel.startBlock = true
            viewModel.startUnBlock = false
        } else{
            viewModel.startBlock = false
            viewModel.startUnBlock = true
        }
        self.interactor.blockCard(viewModel.cardInfo)
    }
    
    func showTransacations(){
        
       let controller = ActionsViewController()
        
        var actionList = [(icon: String, title: String, subtitle: String)]()
        actionList.append(("img_menu_pay", RS.btn_payment.localized(), ""))
        actionList.append(("img_transfer_to_my", RS.btn_money_transfers.localized(), ""))
        actionList.append(("img_menu_cards", RS.lbl_remove_card.localized(), ""))
        
//        let card = viewModel.cardInfo?.card
//        if (card?.canBlock ?? false){
//            let status = (card?.status ?? "").lowercased()
//            var title: String
//            if status == "active" {
//                title = RS.btn_block.localized()
//            } else {
//                title = RS.btn_unblock.localized()
//            }
//
//            actionList.append(("lockIcon", title, ""))
//        }
        
        controller.actionList = actionList
        controller.modalPresentationStyle = .custom
        controller.didActionSelect = {index in
            if index == 0{
                self.router.openModule(self.view as Any, PaymentsMenuRouter().viewController)
                //self.showUserCreditDetails(credit: credit)
            }
            if index == 1{
                
                self.router.openModule(self.view as Any, MoneyTransfersRouter(for: nil, historyItem: nil).viewController)
            }
            if index == 2{
                self.router.cardDeleteConfirm(self.view as Any, self)
                let cardId = self.viewModel.cardInfo?.card.cardId ?? 0
                self.interactor.removeCard(Int(cardId))
            }
//            if index == 3{
//                self.blockOrUnblock()
//            }
        }
        (view as? UIViewController)?.presentPanModal(controller)
    }
    
    func deleteCard(id: Int) {
        interactor.removeCard(id)
    }
    
}

extension CardDetailsPresenter: CardDetailsInteractorToPresenterProtocol {
    func didCardDelete(with response: ResponseResult<CardDeleteResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cardData = result.data
            view.reloadData()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
    // INTERACTOR -> PRESENTER
    func didCardBlock(with response: ResponseResult<SingleMessageResponse, AppError>){
        switch response {
        case let .success(result):
            if viewModel.startBlock{
                viewModel.cardInfo?.card.status = "Block"
                viewModel.startBlock = false
            } else{
                viewModel.cardInfo?.card.status = "Active"
                viewModel.startUnBlock = false
            }
            view.reloadData()
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
    
}

extension CardDetailsPresenter : PopubMessageProtocol{
    func actionResult(_ action: Bool) {
        if action {
            router.navigateToCardDelete(in: view as Any, card: viewModel.cardInfo!, message: viewModel.cardData ?? CardDeleteResponse())
        }
    }
}
