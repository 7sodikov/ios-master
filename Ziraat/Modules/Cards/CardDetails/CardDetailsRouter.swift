//
//  CardDetailsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol CardDetailsRouterProtocol: class {
    static func createModule(_ cardVM: DashboardCardVM) -> UIViewController
    func openCardHistory(_ view : Any, _ card: DashboardCardVM)
    func openModule(_ view: Any, _ module: Any)
    func cardDeleteConfirm(_ view:Any, _ delegate:PopubMessageProtocol?)
    func navigateToCardDelete(in navigationCtrl: Any, card: DashboardCardVM, message: CardDeleteResponse)
}

class CardDetailsRouter: CardDetailsRouterProtocol {
    static func createModule(_ cardVM: DashboardCardVM) -> UIViewController {
        let vc = CardDetailsVC()
        
        let presenter = CardDetailsPresenter()
        let interactor = CardDetailsInteractor()
        let router = CardDetailsRouter()
        presenter.viewModel.cardInfo = cardVM
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    func openCardHistory(_ view: Any, _ card: DashboardCardVM) {
        let module = TransactionHistoryRouter.createModule(card)
        if let controller = view as? UIViewController{
            controller.navigationController?.pushViewController(module, animated: true)
        }
    }
    
    func openModule(_ view: Any, _ module: Any) {
        if let controller = view as? UIViewController {
            if let module = module as? UIViewController{
                controller.navigationController?.pushViewController(module, animated: true)
            }
        }
    }
    
    func cardDeleteConfirm(_ view:Any, _ delegate:PopubMessageProtocol?){
        let module = PopupMessageRouter.createModule(RS.lbl_sure_to_delete.localized(), delegate)
        module.modalPresentationStyle = .overFullScreen
        (view as? UIViewController)?.navigationController?.present(module, animated: true, completion: nil)
    }
    
    func navigateToCardDelete(in navigationCtrl: Any, card: DashboardCardVM, message: CardDeleteResponse) {
        let viewCtrl = navigationCtrl as? UIViewController
        let serviceListView = CardDeleteConfirmRouter.createModule(card: card, message: message)
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
}


