//// Header

import UIKit

class CardDetailsView : BaseView{
    fileprivate var branchView = HorizontalLabelsView(frame: .zero)
    fileprivate var balanceView = HorizontalLabelsView(frame: .zero)
    fileprivate var accountView = HorizontalLabelsView(frame: .zero)
    fileprivate var separatorView = UIView()
    var cardVM : DashboardCardVM?{
        didSet{
            if let vm = cardVM{
                branchView.rightLabel.text = vm.card.branchName
                balanceView.rightLabel.text = vm.balanceStr
                accountView.rightLabel.text = vm.card.account
            }
        }
    }
    var userDeposite : DepositResponse?{
        didSet{
            if let dp = userDeposite{
                branchView.leftLabel.text = RS.lbl_account.localized()
                //balanceView.leftLabel.text = RS.lbl_account_title.localized()
                balanceView.leftLabel.text = RS.lbl_balance.localized()
                accountView.leftLabel.text = RS.lbl_branch_name.localized()
                let balance = ((dp.accounts.first?.balance ?? 0)/100).currencyFormattedStr()
                branchView.rightLabel.text = dp.accounts.first?.number
                balanceView.rightLabel.text = (balance ?? "") + (dp.accounts.first?.currency ?? "")
                accountView.rightLabel.text = dp.accounts.first?.branchName
            }
        }
    }
    override func initSubviews() {
        separatorView.backgroundColor = .white
        branchView.leftLabel.textColor = ColorConstants.borderLine
        balanceView.leftLabel.textColor = ColorConstants.borderLine
        accountView.leftLabel.textColor = ColorConstants.borderLine
        
        branchView.leftLabel.text = RS.lbl_branch_name.localized()
        balanceView.leftLabel.text = RS.lbl_balance.localized()
        accountView.leftLabel.text = RS.lbl_iban.localized()
        
    }
    override func embedSubviews() {
        addSubview(branchView)
        addSubview(balanceView)
        addSubview(accountView)
        addSubview(separatorView)
    }
    override func addSubviewsConstraints() {
        branchView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
        }
        balanceView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(branchView.snp.bottom).offset(3)
        }
        accountView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(balanceView.snp.bottom).offset(3)
            make.bottom.equalToSuperview()
        }
        separatorView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.width.equalTo(Adaptive.val(1))
            make.centerX.equalToSuperview()
        }
    }
    
}
