//
//  CardDetailsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CardDetailsInteractorProtocol: class {
    func removeCard(_ id: Int)
    func blockCard(_ card: DashboardCardVM?)
}

class CardDetailsInteractor: CardDetailsInteractorProtocol {
    weak var presenter: CardDetailsInteractorToPresenterProtocol!
    
    func removeCard(_ id: Int){
        NetworkService.Card.deleteCard(cardId: id, completion: { [weak self] (result) in
            self?.presenter.didCardDelete(with: result)
        })
    }
    
    func blockCard(_ card: DashboardCardVM?) {
        let cardId = card?.card.cardId ?? 0
        let status = (card?.card.status ?? "").lowercased()
        if status == "active"{
            NetworkService.Card.cardBlock(id: Int(cardId), completion: { (result) in
                self.presenter.didCardBlock(with: result)
            })
        } else{
            NetworkService.Card.cardUnBlock(id: Int(cardId), completion: { (result) in
                self.presenter.didCardBlock(with: result)
            })
        }
    }
}
