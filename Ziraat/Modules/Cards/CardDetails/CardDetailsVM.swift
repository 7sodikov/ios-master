//
//  CardDetailsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class CardDetailsVM {
    //represent model from json object
    var cardInfo : DashboardCardVM?
    var cardData: CardDeleteResponse?
    var startBlock : Bool = false
    var startUnBlock : Bool = false
}
