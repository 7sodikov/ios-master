//
//  TransactionHistoryInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol TransactionHistoryInteractorProtocol: class {
    func getHistory(_ start:String, _ end: String, _ id: UInt64)
}

class TransactionHistoryInteractor: TransactionHistoryInteractorProtocol {
    weak var presenter: TransactionHistoryInteractorToPresenterProtocol!
    
    func getHistory(_ start:String, _ end: String, _ id: UInt64){
        NetworkService.Card.addHistory(start: start, end: end, id: id) { (result) in
            self.presenter.didCardHistory(with: result)
        }
    }
}
