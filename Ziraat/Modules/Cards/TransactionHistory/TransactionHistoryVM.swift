//
//  TransactionHistoryVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class TransactionHistoryItemVM {
    var historyItem: CardHistoryItemResponse
    
    init(_ historyItem: CardHistoryItemResponse) {
        self.historyItem = historyItem
    }
    lazy var amountStr: String = {
        let result = ((historyItem.amount/100).currencyFormattedStr() ?? "") + " " + CurrencyType.uzs.data.label
        return result
    }()
    
    lazy var dayStr: String = {
        let date = Date.date(string: historyItem.transDate, format: "yyyy-MM-dd")
        let result = date.string(for: "dd")
        return result
    }()
    
    lazy var dateStr: String = {
        let date = Date.date(string: historyItem.transDate, format: "yyyy-MM-dd")
        let time = Date.date(string: historyItem.transTime, format: "HH:mm:ss")
        let result = date.string(for: "MMM") + "\n" + time.string(for: "HH:mm")
        return result
    }()
    
}

class TransactionHistoryVM {
    //represent model from json object
    var historyItems : CardHistoryResponse? {
        didSet {
            guard let history = historyItems?.history else { return }
            historyItemVMs.removeAll()
            for hItem in history {
                historyItemVMs.append(TransactionHistoryItemVM(hItem))
            }
        }
    }
    
    private var historyItemVMs: [TransactionHistoryItemVM] = []
    var historyList : [TransactionHistoryItemVM]{
        var searchResult = historyItemVMs
        if self.filterText.count > 0 {
            searchResult = searchResult.filter{
                let amountString = String(format: "%.02f", $0.historyItem.amount)
                let merchant = ($0.historyItem.merchantName ?? "").lowercased()
                let type = ($0.historyItem.transType).lowercased()
                return ((merchant.range(of: filterText.lowercased()) != nil) ||
                            (type.range(of: filterText.lowercased()) != nil) ||
                            (amountString.range(of: filterText) != nil) ||
                            ($0.dayStr.range(of: filterText) != nil) ||
                            ($0.dateStr.range(of: filterText) != nil) ||
                            ($0.amountStr.range(of: filterText) != nil))

            }
        }
        if self.filter == 0{
            return searchResult
        }
        if  filter == 1{
            return searchResult.filter{
               return $0.historyItem.isCredit
            }
        }
        if  filter == 2{
            return searchResult.filter{
               return !$0.historyItem.isCredit
            }
        }
        return []
    }
    var filter : Int = 0
    var filterText = ""
    
    subscript(index: Int) -> TransactionHistoryItemVM? {
        get {
            let list = historyList
            return list[index]
        }
    }
}


//class ServiceCategoryVM {
//    var category: ServiceCategoryResponse
//
//    init(_ category: ServiceCategoryResponse) {
//        self.category = category
//    }
//}
//
//class ServiceCategoriesVM {
//    var categories: [ServiceCategoryResponse] = []
//
//    func categoryVM(at index: Int) -> ServiceCategoryVM {
//        return ServiceCategoryVM(categories[index])
//    }
//}
