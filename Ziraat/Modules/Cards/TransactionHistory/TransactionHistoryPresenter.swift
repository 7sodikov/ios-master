//
//  TransactionHistoryPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol TransactionHistoryPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: TransactionHistoryVM { get }
    func getHistory(_ start:String, _ end: String, _ id: UInt64)
}

protocol TransactionHistoryInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didCardHistory(with response: ResponseResult<CardHistoryResponse, AppError>)
    
}

class TransactionHistoryPresenter: TransactionHistoryPresenterProtocol {
    weak var view: TransactionHistoryVCProtocol!
    var interactor: TransactionHistoryInteractorProtocol!
    var router: TransactionHistoryRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = TransactionHistoryVM()
    
    // Private property and methods
    func getHistory(_ start: String, _ end: String, _ id: UInt64) {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.getHistory(start, end, id)
    }
}

extension TransactionHistoryPresenter: TransactionHistoryInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didCardHistory(with response: ResponseResult<CardHistoryResponse, AppError>){
        
        switch response {
        case let .success(result):
            viewModel.historyItems = result.data
            view.showReloadButtonForResponses(false, animateReloadButton: false)
        case let .failure(error):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
        }
        
    }
}
