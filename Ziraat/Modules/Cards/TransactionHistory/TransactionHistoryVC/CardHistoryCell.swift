//// Header

import UIKit

class CardHistoryCell: UITableViewCell {

    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var merchantLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        merchantLabel.superview?.superview?.backgroundColor = .white
        merchantLabel.superview?.superview?.layer.cornerRadius = 7
        
        dayLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(24))
        dayLabel.textAlignment = .center
        dayLabel.textColor = .black
        
        timeLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(15))
        timeLabel.textAlignment = .center
        timeLabel.numberOfLines = 2
        timeLabel.textColor = ColorConstants.gray
        
        typeLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        typeLabel.numberOfLines = 3
        typeLabel.textColor = ColorConstants.gray
        
        merchantLabel.textColor = ColorConstants.gray
        merchantLabel.numberOfLines = 4
        merchantLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        
        amountLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        amountLabel.textColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(_ item:TransactionHistoryItemVM?){
        dayLabel.text = item?.dayStr
        timeLabel.text = item?.dateStr
        amountLabel.text = item?.amountStr
        merchantLabel.text = item?.historyItem.merchantName
        typeLabel.text = item?.historyItem.transType
        if item?.historyItem.isCredit == true{
            amountLabel.textColor = ColorConstants.dashboardGreen
        } else{
            amountLabel.textColor = ColorConstants.red
        }
    }
    
}
