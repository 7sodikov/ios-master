//
//  TransactionHistoryVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import iOSDropDown

protocol TransactionHistoryVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class TransactionHistoryVC: BaseViewController, TransactionHistoryViewInstaller {
    var searchFild: ClipboardControlledTextField!
    var filterDropDown: DropDown!
    var filterView: UIView!
    var cardImage: UIImageView!
    var numberLabel: UILabel!
    var titleLabel: UILabel!
    var searchView: UIView!
    var searchLabel: UILabel!
    var topView: UIView!
    var tableView: UITableView!
    var bgView: UIImageView!
    var reloadButtonView: ReloadButtonView!
    
    var mainView: UIView { view }
    var presenter: TransactionHistoryPresenterProtocol!
    var cardVM: DashboardCardVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = RS.nav_ttl_transactions_history.localized()
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        let nib = UINib(nibName: "CardHistoryCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CardHistoryCell")
       
        numberLabel.text = cardVM?.card.pan
        cardImage.kf.setImage(with: URL(string: cardVM?.cardBGImageURL ?? ""), placeholder: nil)
        titleLabel.text = cardVM?.cardTitle
        
        filterDropDown.optionArray = [RS.lbl_all.localized(),
                                  RS.lbl_entry.localized(),
                                  RS.lbl_write_off.localized()]
        filterDropDown.text = filterDropDown.optionArray[0]
        filterDropDown.didSelect{(selectedText , index ,id) in
            self.presenter.viewModel.filter = index
            self.tableView.reloadData()
        }
        searchFild.addTarget(self, action: #selector(changeText), for: .editingChanged)
        
        reloadButtonView.reloadButton.addTarget(self, action: #selector(getHistory), for: .touchUpInside)
        getHistory()
    }
    
    @objc func changeText(){
        self.presenter.viewModel.filterText = self.searchFild.text ?? ""
        self.tableView.reloadData()
    }
    
    @objc func getHistory(){
        let start = Date().addingTimeInterval(-3600 * 24 * 90).string(for: "yyyy-MM-dd")
        let end = Date().string(for: "yyyy-MM-dd")
        let cardId = cardVM?.card.cardId ??  0
        self.presenter.getHistory(start, end, cardId)
    }
}

extension TransactionHistoryVC: TransactionHistoryVCProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool){
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
}

extension TransactionHistoryVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.historyList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardHistoryCell") as! CardHistoryCell
        let item = presenter.viewModel[indexPath.row]
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.setup(item)
        return cell
    }
}
