//
//  TransactionHistoryViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import iOSDropDown

protocol TransactionHistoryViewInstaller: ViewInstaller {
    var bgView : UIImageView! { get set }
    var tableView : UITableView! { get set }
    var topView : UIView! { get set }
    var cardImage : UIImageView! { get set }
    var numberLabel : UILabel! { get set }
    
    var titleLabel : UILabel! { get set }
    var filterDropDown : DropDown! { get set }
    var filterView : UIView! { get set }
    var searchView : UIView! { get set }
    var searchLabel: UILabel! { get set }
    var searchFild : ClipboardControlledTextField! { get set }
    var reloadButtonView : ReloadButtonView! { get set }
}

extension TransactionHistoryViewInstaller {
    func initSubviews() {
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        
        topView = UIView()
        topView.backgroundColor = .clear
        
        cardImage = UIImageView()
        cardImage.layer.cornerRadius = 6
        cardImage.clipsToBounds = true
        cardImage.contentMode = .scaleAspectFill
        
        numberLabel = UILabel()
        numberLabel.font = EZFontType.medium.sfuiText(size: 14)
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.medium.sfuiDisplay(size: 14)
        
        filterView = UIView()
        filterView.backgroundColor = .white
        filterView.layer.cornerRadius = 4
        filterView.layer.borderWidth = 1
        filterView.layer.borderColor = UIColor.black.cgColor
        
        filterDropDown = DropDown()
        filterDropDown.hideOptionsWhenSelect = true
        filterDropDown.isSearchEnable = false
        filterDropDown.arrowColor = ColorConstants.red
        filterDropDown.selectedRowColor = ColorConstants.red
        
        searchView = UIView()
        searchView.backgroundColor = .white
        searchView.layer.cornerRadius = 4
        searchView.layer.borderWidth = 1
        searchView.layer.borderColor = UIColor.black.cgColor
        
        searchLabel = UILabel()
        searchLabel.text = RS.txt_f_search.localized()
        
        searchFild = ClipboardControlledTextField()
        searchFild.autocorrectionType = .no
        searchFild.placeholder = RS.txt_f_search.localized()
        
        reloadButtonView = ReloadButtonView(frame: .zero)
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)

        mainView.addSubview(topView)
        mainView.addSubview(tableView)
        mainView.addSubview(reloadButtonView)
        
        topView.addSubview(cardImage)
        topView.addSubview(numberLabel)
        topView.addSubview(titleLabel)
        
        topView.addSubview(filterView)
        filterView.addSubview(filterDropDown)
        
        topView.addSubview(searchView)
        searchView.addSubview(searchLabel)
        searchView.addSubview(searchFild)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        topView.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(80))
            make.left.equalTo(Adaptive.val(15))
            make.right.equalTo(-Adaptive.val(15))
        }
        
        cardImage.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(10))
            make.right.top.equalToSuperview()
            make.height.equalTo(Adaptive.val(65))
            make.width.equalTo(cardImage.snp.height).multipliedBy(1.67)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.right.equalTo(cardImage)
            make.bottom.equalTo(cardImage)
            make.left.equalToSuperview()
        }
        
        numberLabel.snp.makeConstraints { (make) in
            make.right.equalTo(cardImage)
            make.bottom.equalTo(titleLabel.snp.top).offset(-1)
            make.left.equalToSuperview()
        }
        
        filterView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(15))
            make.height.equalTo(Adaptive.val(45))
        }
        
        filterDropDown.snp.makeConstraints { (make) in
            make.left.equalTo(Adaptive.val(10))
            make.top.bottom.equalToSuperview()
            make.right.equalTo(-Adaptive.val(20))
        }
        
        searchView.snp.makeConstraints { (make) in
            make.top.equalTo(filterView.snp.bottom).offset(Adaptive.val(15))
            make.height.equalTo(Adaptive.val(45))
            make.left.right.equalToSuperview()
            make.bottom.equalTo(-Adaptive.val(5))
        }
        
//        searchLabel.snp.remakeConstraints { (maker) in
//            maker.centerY.equalToSuperview()
//            maker.top.equalToSuperview().offset(Adaptive.val(15))
//        }
        
        searchFild.snp.makeConstraints { (make) in
            make.left.equalTo(Adaptive.val(10))
            make.top.bottom.equalToSuperview()
            make.right.equalTo(-Adaptive.val(20))
        }
        
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(topView.snp.bottom).offset(Adaptive.val(10))
        }
        
        reloadButtonView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
        }
    }
}

fileprivate struct Size {
    
}
