//
//  TransactionHistoryRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol TransactionHistoryRouterProtocol: class {
    static func createModule(_ card:DashboardCardVM) -> UIViewController
}

class TransactionHistoryRouter: TransactionHistoryRouterProtocol {
    static func createModule(_ card:DashboardCardVM) -> UIViewController {
        let vc = TransactionHistoryVC()
        vc.cardVM = card
        let presenter = TransactionHistoryPresenter()
        let interactor = TransactionHistoryInteractor()
        let router = TransactionHistoryRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
