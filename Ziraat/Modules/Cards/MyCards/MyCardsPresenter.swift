//
//  MyCardsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialTabs_TabBarView

class CardListVM {
    init() {
        cardsVM = DashboardCardsVM()
    }
    var cardsVM : DashboardCardsVM!
    var filterIndex = 0
    var cardList : [CardResponse]{
        if filterIndex == 0 {
            return cardsVM.cards
        }
        if filterIndex == 1 {
            return cardsVM.cards.filter {
                let type = ($0.type ?? "").lowercased()
                return type.contains("uzcard")
            }
        }
        if filterIndex == 2 {
            return cardsVM.cards.filter {
                let type = ($0.type ?? "").lowercased()
                return type.contains("humo")
            }
        }
        if filterIndex == 3 {
            return cardsVM.cards.filter {
                let type = ($0.type ?? "").lowercased()
                return type.contains("visa")
            }
        }
        return []
    }
    
    subscript(index: Int) -> DashboardCardVM! {
        var balance: CardBalanceResponse?
        let card = cardList[index]
        for cardBalance in cardsVM.cardBalances {
            if cardBalance.cardId == card.cardId {
                balance = cardBalance
                break
            }
        }
        return DashboardCardVM(card, balance)
    }
}

protocol MyCardsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: CardListVM { get }
    func viewDidLoad()
    func cardClicked(_ index: Int)
    func addCardClicked()
}

protocol MyCardsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>)
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>)
}

class MyCardsPresenter: MyCardsPresenterProtocol {
    func cardClicked(_ index: Int) {
        router.openDetals(view, viewModel[index]!)
    }
    func addCardClicked(){
        router.addCard(view, self)
    }
    
    weak var view: MyCardsVCProtocol!
    var interactor: MyCardsInteractorProtocol!
    var router: MyCardsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = CardListVM()
    // Private property and methods
    
    func viewDidLoad() {
//        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.getAllCards()
        interactor.getAllCardBalances()
    }
}

extension MyCardsPresenter : AddCardProtocol{
    func didSuccessAddCard() {
        viewDidLoad()
    }
    func didErrorAddCard(_ error: String) {
        view.showError(message: error)
    }
}

extension MyCardsPresenter: MyCardsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didGetAllCards(with response: ResponseResult<CardsResponse, AppError>) {
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                viewModel.cardsVM.cards = result.data.cards

                if viewModel.cardList.count == 0 {
                    view.showReloadButtonForResponses(true, animateReloadButton: false)
                } else {
                    view.showReloadButtonForResponses(false, animateReloadButton: false)
                }
            }
        case let .failure(error):
//            view.showReloadButtonForResponses(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didGetAllCardBalances(with response: ResponseResult<CardBalancesResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.cardsVM.cardBalances = result.data.cards ?? []
            if result.data.count >= 0 {
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(true, animateReloadButton: false)
            }
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            view.showReloadButtonForResponses(true, animateReloadButton: false)
        }
    }
}
