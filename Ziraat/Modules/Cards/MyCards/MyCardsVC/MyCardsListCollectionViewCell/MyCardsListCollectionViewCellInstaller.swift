//
//  MyCardsListCollectionViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol MyCardsListCollectionViewCellInstaller: ViewInstaller {
    var cardView : CardView! {get set}
}

extension MyCardsListCollectionViewCellInstaller {
    func initSubviews() {
        
        mainView.layer.cornerRadius = Adaptive.val(14)
        mainView.layer.masksToBounds = true
        
        cardView = CardView(frame: .zero)
        cardView.changeLabelSize(true)
        cardView.eyeIconContainer.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(cardView)
    }
    
    func addSubviewsConstraints() {
        cardView.snp.makeConstraints { (make) in
            make.left.right.bottom.top.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    static let padding = Adaptive.val(10)
    static let interItemSpace = Adaptive.val(20)
    static let fieldTitleFontSize = Adaptive.val(17)
}
