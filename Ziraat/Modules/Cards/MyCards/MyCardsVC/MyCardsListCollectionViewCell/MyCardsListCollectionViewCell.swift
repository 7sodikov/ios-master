//
//  MyCardsListCollectionViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class MyCardsListCollectionViewCell: UICollectionViewCell, MyCardsListCollectionViewCellInstaller {
    var cardView: CardView!
    
//    var bgImageView: UIImageView!
//    var cardNumberLabel: UILabel!
//    var balanceTitleLabel: UILabel!
//    var balanceLabel: UILabel!
//    var balanceStackView: UIStackView!
//    var balanceHorizontalAlignStackView: UIStackView!
//    var reloadButton: UIButton!
//    var holderNameLabel: UILabel!
//    var expiryDateLabel: UILabel!
//    var nameAndExpiryDateContainerStackView: UIStackView!
//    var expiryDateHorizontalAlignStackView: UIStackView!
    
    var mainView: UIView { contentView }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
//    var cardItem: MyCardItemVM? {
//        didSet {
//            bgImageView.image = cardItem?.backImage
//            cardNumberLabel.text = cardItem?.cardNumber
//            balanceLabel.text = cardItem?.balance
//            holderNameLabel.text = cardItem?.cardHolderName
//            expiryDateLabel.text = cardItem?.expDate
//        }
//    }
    
}
