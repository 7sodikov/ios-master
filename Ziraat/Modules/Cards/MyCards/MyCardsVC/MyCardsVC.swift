//
//  MyCardsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTabs_TabBarView

protocol MyCardsVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
    func reloadAllData()
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class MyCardsVC: BaseViewController, MyCardsViewInstaller {
    var warningMessageView: NoItemsAvailableView!
    var typeContainerView: UIView!
    var cardTypesView: MDCTabBarView!
    var cardListCollectionFlowLayout: UICollectionViewFlowLayout!
    var cardListCollectionView: AutomaticHeightCollectionView!
    var backImageView: UIImageView!
    var addButton: NextButton!
    
    var cardTypesCollectionFlowLayout: UICollectionViewFlowLayout!
    var showDetailsButton: NextButton!
    var mainView: UIView { view }
    var presenter: MyCardsPresenterProtocol!
    var eyeBtn: UIButton!
    var reloadButtonView: ReloadButtonView!
    
    var dataTypes = [String]()
    var dataItems = [MyCardItemVM]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        
        cardListCollectionView.delegate = self
        cardListCollectionView.dataSource = self
        cardListCollectionView.reloadData()
        
        dataTypes = [RS.lbl_all.localized(),
                     RS.lbl_uzcard.localized(),
                     RS.lbl_humo.localized(),
                     RS.lbl_visa.localized()]
        
        addButton.addTarget(self, action: #selector(addCardClicked), for: .touchUpInside)
        
        cardTypesView.items = [
            UITabBarItem(title: RS.lbl_all.localized(), image: nil, tag: 0),
            UITabBarItem(title: RS.lbl_uzcard.localized(), image: nil, tag: 1),
            UITabBarItem(title: RS.lbl_humo.localized(), image: nil, tag: 2),
            UITabBarItem(title: RS.lbl_visa.localized(), image: nil, tag: 3),
        ]
        cardTypesView.setSelectedItem(cardTypesView.items[0], animated: true)
        cardTypesView.preferredLayoutStyle = .nonFixedClusteredCentered
        cardTypesView.tabBarDelegate = self
        
        presenter.viewDidLoad()
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadClicked), for: .touchUpInside)
    }
    
    private func setupNavigation() {
        self.title = RS.nav_ttl_my_cards.localized()        
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addCardClicked(){
        presenter.addCardClicked()
    }
    
    @objc func reloadClicked(){
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: false)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        reloadAllData()
        eyeBtn.addTarget(self, action: #selector(eyeClicked), for: .touchUpInside)
        changeEyeButtonImage()
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        navigationItem.rightBarButtonItems = [homeButton, UIBarButtonItem(customView: eyeBtn)]
    }
    
    @objc func eyeClicked(){
        UDManager.hideBalance = !UDManager.hideBalance
        reloadAllData()
        changeEyeButtonImage()
    }
    
    func changeEyeButtonImage(){
        let eyeImageName = UDManager.hideBalance ? "btn_eye" : "img_eye"
        let icon = UIImage(named: eyeImageName)?.withRenderingMode(.alwaysTemplate)
        eyeBtn.setImage(icon, for: .normal)
    }
}

extension MyCardsVC: MyCardsVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func reloadAllData() {
        cardListCollectionView.reloadData()
    }
    
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool){
        warningMessageView.isHidden = !show
        cardTypesView.isHidden = show
        typeContainerView.isHidden = show
        eyeBtn.isHidden = show
        reloadButtonView.animateIndicator = animateReloadButton
        cardListCollectionView.isHidden = show
        cardListCollectionView.reloadData()
        if show == true {
            addButton.snp.remakeConstraints { (make) in
                make.bottom.equalTo(-Adaptive.val(90))
                make.left.equalTo(Adaptive.val(35))
                make.right.equalTo(-Adaptive.val(35))
                make.height.equalTo(Adaptive.val(45))
            }
        } else {
            addButton.snp.remakeConstraints { (make) in
                make.top.equalTo(90)
                make.left.equalTo(Adaptive.val(35))
                make.right.equalTo(-Adaptive.val(35))
                make.height.equalTo(Adaptive.val(45))
            }
        }
    }
}

extension MyCardsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.viewModel.cardList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == cardListCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCardsListCollectionViewCell", for: indexPath) as! MyCardsListCollectionViewCell
            cell.cardView.dashboardCard =  presenter.viewModel[indexPath.row]
            return cell
        }
        
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.cardClicked(indexPath.row)
    }
}
extension MyCardsVC : MDCTabBarViewDelegate{
    func tabBarView(_ tabBarView: MDCTabBarView, didSelect item: UITabBarItem) {
        presenter.viewModel.filterIndex = item.tag
        reloadAllData()
    }
}
