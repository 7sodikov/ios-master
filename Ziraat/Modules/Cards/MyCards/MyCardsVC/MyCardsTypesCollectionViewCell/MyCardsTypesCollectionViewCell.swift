//
//  MyCardsTypesCollectionViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class MyCardsTypesCollectionViewCell: UICollectionViewCell, MyCardsTypesCollectionViewCellInstaller {
    var backView: UIView!
    
    var cardTypeLabel: UIButton!
    
    var mainView: UIView { contentView }
    
    var typeName: MyCardTypeVM? {
        didSet {
            cardTypeLabel.setTitle(typeName?.typeName, for: .normal)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
