//
//  MyCardsTypesCollectionViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol MyCardsTypesCollectionViewCellInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var cardTypeLabel: UIButton! { get set }
}

extension MyCardsTypesCollectionViewCellInstaller {
    func initSubviews() {
        backView = UIView()
        
        cardTypeLabel = UIButton()
        cardTypeLabel.titleLabel?.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(17))
        cardTypeLabel.setTitleColor(ColorConstants.gray, for: .normal)
        cardTypeLabel.setTitleColor(.black, for: .selected)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(cardTypeLabel)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
            maker.height.equalTo(Adaptive.val(21))
            maker.width.equalTo(Adaptive.val(50))
        }
        
        cardTypeLabel.snp.remakeConstraints { (maker) in
            maker.centerX.centerY.equalTo(backView)
        }
    }
}

fileprivate struct Size {
    
}
