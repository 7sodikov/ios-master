//
//  MyCardsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTabs_TabBarView

protocol MyCardsViewInstaller: ViewInstaller {
    var backImageView: UIImageView! { get set }
    var addButton: NextButton! { get set }
    var cardListCollectionFlowLayout: UICollectionViewFlowLayout! { get set }
    var cardListCollectionView: AutomaticHeightCollectionView! { get set }
    var eyeBtn: UIButton! { get set }
    var cardTypesView : MDCTabBarView! { get set }
    var typeContainerView : UIView! { get set }
    var warningMessageView: NoItemsAvailableView! { get set }
    var reloadButtonView: ReloadButtonView! { get set }
}

extension MyCardsViewInstaller {
    func initSubviews() {
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_dash_light_background")
        
        addButton = NextButton.systemButton(with: RS.btn_add.localized(), cornerRadius: Size.height/2)
        addButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        addButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        
        typeContainerView = UIView()
        typeContainerView.backgroundColor = .clear
        typeContainerView.clipsToBounds = true
        
        cardTypesView = MDCTabBarView()
        cardTypesView.isScrollEnabled = false
        cardTypesView.setTitleFont(EZFontType.medium.sfuiDisplay(size: Adaptive.val(15)), for: .normal)
        cardTypesView.barTintColor = .clear
        cardTypesView.backgroundColor = .clear
        cardTypesView.setTitleColor(ColorConstants.hintGray, for: .normal)
        cardTypesView.setTitleColor(.dashDarkBlue, for: .selected)
        cardTypesView.selectionIndicatorStrokeColor = ColorConstants.red
        cardTypesView.bottomDividerColor = ColorConstants.hintGray
        
        cardListCollectionFlowLayout = UICollectionViewFlowLayout()
        cardListCollectionFlowLayout.sectionInset = .zero
        cardListCollectionFlowLayout.scrollDirection = .vertical
        cardListCollectionFlowLayout.minimumInteritemSpacing = 0
        let padding = Adaptive.val(15)
        let width =  (UIScreen.main.bounds.size.width - 3 * padding)/2.0  //Adaptive.val(171)
        cardListCollectionFlowLayout.itemSize = CGSize(width: width, height: width / 1.67)
        
        cardListCollectionView = AutomaticHeightCollectionView(frame: .zero, collectionViewLayout: cardListCollectionFlowLayout)
        cardListCollectionView.showsHorizontalScrollIndicator = false
        cardListCollectionView.alwaysBounceVertical = true
        cardListCollectionView.backgroundColor = .clear
        cardListCollectionView.register(MyCardsListCollectionViewCell.self,
                                forCellWithReuseIdentifier: String(describing: MyCardsListCollectionViewCell.self))
        eyeBtn = UIButton()
        eyeBtn.tintColor = .black
        
        warningMessageView = NoItemsAvailableView.setupText(title: RS.lbl_no_card.localized())
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        reloadButtonView.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(addButton)
        //mainView.addSubview(cardTypesView)
        mainView.addSubview(cardListCollectionView)
        mainView.addSubview(typeContainerView)
        mainView.addSubview(warningMessageView)
        mainView.addSubview(reloadButtonView)
        typeContainerView.addSubview(cardTypesView)
        
    }
    
    func addSubviewsConstraints() {
        backImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        addButton.snp.remakeConstraints { (make) in
            make.top.equalTo(90)
            make.left.equalTo(Adaptive.val(35))
            make.right.equalTo(-Adaptive.val(35))
            make.height.equalTo(Size.height)
        }
        
        typeContainerView.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(40)
            make.top.equalTo(addButton.snp.bottom).offset(Adaptive.val(15))
        }
        
        cardTypesView.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
            //make.height.equalTo(Adaptive.val(45))
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        
        cardListCollectionView.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.top.equalTo(cardTypesView.snp.bottom).offset(Adaptive.val(10))
            maker.bottom.equalToSuperview()
        }
        
        warningMessageView.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(214))
            make.centerX.equalToSuperview()
        }
        
        reloadButtonView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
        }
    }
}

fileprivate struct Size {
    static let height = Adaptive.val(45)

}
