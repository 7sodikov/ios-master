//
//  MyCardsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MyCardsRouterProtocol: class {
    static func createModule() -> UIViewController
    func openDetals(_ view:Any, _ details:DashboardCardVM)
    func addCard(_ view: Any, _ delegate : AddCardProtocol)
}

class MyCardsRouter: MyCardsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = MyCardsVC()
        let presenter = MyCardsPresenter()
        let interactor = MyCardsInteractor()
        let router = MyCardsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func openDetals(_ view: Any, _ details: DashboardCardVM) {
        if let controller = view as? UIViewController{
            let module = CardDetailsRouter.createModule(details)
            controller.navigationController?.pushViewController(module, animated: true)
        }
    }
    func addCard(_ view: Any, _ delegate : AddCardProtocol){
        if let controller = view as? UIViewController{
            let module = AddCardRouter.createModule(delegate)
            module.modalPresentationStyle = .overCurrentContext
            controller.navigationController?.present(module, animated: true, completion: nil)
        }
    }
    
}
