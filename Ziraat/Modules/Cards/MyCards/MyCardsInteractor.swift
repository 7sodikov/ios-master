//
//  MyCardsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MyCardsInteractorProtocol: class {
    func getAllCards()
    func getAllCardBalances()
}

class MyCardsInteractor: MyCardsInteractorProtocol {
    weak var presenter: MyCardsInteractorToPresenterProtocol!
    
    func getAllCards() {
        NetworkService.Card.cardsList { [weak self] (result) in
            self?.presenter.didGetAllCards(with: result)
        }
    }
    
    func getAllCardBalances() {
        NetworkService.Card.cardBalance { [weak self] (result) in
            self?.presenter.didGetAllCardBalances(with: result)
        }
    }
}
