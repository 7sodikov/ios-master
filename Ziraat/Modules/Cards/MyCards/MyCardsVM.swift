//
//  MyCardsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

enum CardTypes {
    case all
    case uzcard
    case visa
    case humo
}

class MyCardsVM {
    //represent model from json object
}

struct  MyCardTypeVM {
    var typeName: String
    
    init(type: String) {
        typeName = type
    }
}

struct MyCardItemVM {
    var backImage: UIImage
    var cardNumber: String
    var balance: String
    var cardHolderName: String
    var expDate: String
    var cardType: CardTypes
    
    init(image: UIImage, cardNumber: String, balance: String, cardHolder: String, exp: String, cardType: CardTypes) {
        self.backImage = image
        self.cardNumber = cardNumber
        self.balance = balance
        self.cardHolderName = cardHolder
        self.expDate = exp
        self.cardType = cardType
    }
}

