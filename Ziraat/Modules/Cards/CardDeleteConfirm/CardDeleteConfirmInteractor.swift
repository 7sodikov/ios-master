//
//  CardDeleteConfirmInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CardDeleteConfirmInteractorProtocol: class {
    func removeCardConfirm(id: Int, code:String)
}

class CardDeleteConfirmInteractor: CardDeleteConfirmInteractorProtocol {
    weak var presenter: CardDeleteConfirmInteractorToPresenterProtocol!
    
    func removeCardConfirm(id: Int, code:String) {
        NetworkService.Card.deleteCardConfirm(cardId: id, code: code) { [weak self] (result) in
            self?.presenter.didCardDeleteConfirm(with: result)
        }
    }
}
