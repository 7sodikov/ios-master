//
//  CardDeleteConfirmRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol CardDeleteConfirmRouterProtocol: class {
    static func createModule(card: DashboardCardVM, message: CardDeleteResponse) -> UIViewController
}

class CardDeleteConfirmRouter: CardDeleteConfirmRouterProtocol {
    static func createModule(card: DashboardCardVM, message: CardDeleteResponse) -> UIViewController {
        let vc = CardDeleteConfirmVC()
        let presenter = CardDeleteConfirmPresenter()
        let interactor = CardDeleteConfirmInteractor()
        let router = CardDeleteConfirmRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.message = message
        presenter.data = card
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
