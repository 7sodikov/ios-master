//
//  CardDeleteConfirmVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import EasyTipView

protocol CardDeleteConfirmVCProtocol: BaseViewControllerProtocol {
    func tipView(show: Bool)
    func startTimer()
    func stopTimer()
    func set(buttonTitle: String)
    func phoneMessage(title: String)
}

class CardDeleteConfirmVC: UIViewController, CardDeleteConfirmViewInstaller {
    var backImageview: UIImageView!
    var smsSentLabel: UILabel!
    var yourCodeView: UIView!
    var yourCodeTextField: SMSConfirmationTextField!
    var hideButton: UIButton!
    var tipView: EasyTipView!
    var timerView: TimerView!
    var confirmButton: NextButton!
    
    var mainView: UIView { view }
    var presenter: CardDeleteConfirmPresenterProtocol!
    
    var iconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        confirmButton.addTarget(self, action: #selector(confirmButtonClicked(_:)), for: .touchUpInside)
        hideButton.addTarget(self, action: #selector(hideButtonClicked(_:)), for: .touchUpInside)
        
        presenter.viewDidLoad()
        
        timerView.startTimer(with: timerValue)
        timerView.timerReachedItsEnd = {
            self.presenter.timerReachedItsEnd()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    @objc private func hideButtonClicked(_ sender: UIButton) {
        if(iconClick == true) {
            yourCodeTextField.isSecureTextEntry = false
        } else {
            yourCodeTextField.isSecureTextEntry = true
        }
        iconClick = !iconClick
    }
    
    @objc private func confirmButtonClicked(_ sender: NextButton) {
        presenter.confirmButtonClicked(with: yourCodeTextField.text)
        tipView.isHidden = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
}

extension CardDeleteConfirmVC: CardDeleteConfirmVCProtocol {
    func tipView(show: Bool) {
        tipView.show(forView: yourCodeTextField)
    }
    
    func startTimer() {
        timerView.startTimer(with: timerValue)
    }
    
    func stopTimer() {
        timerView.stopTimer()
    }
    
    func set(buttonTitle: String) {
        confirmButton.setTitle(buttonTitle, for: .normal)
    }
    
    func phoneMessage(title: String) {
        smsSentLabel.text = "\(title)"
    }
    
    func preloader(show: Bool) {
        confirmButton.animateIndicator = show
    }
    
    func showError(message: String?) {
        
    }
}
