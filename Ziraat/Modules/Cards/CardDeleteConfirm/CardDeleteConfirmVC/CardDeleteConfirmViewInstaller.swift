//
//  CardDeleteConfirmViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import EasyTipView

protocol CardDeleteConfirmViewInstaller: ViewInstaller {
    var backImageview: UIImageView! { get set }
    var smsSentLabel: UILabel! { get set }
    var yourCodeView: UIView! { get set }
    var yourCodeTextField: SMSConfirmationTextField! { get set }
    var hideButton: UIButton! { get set }
    var tipView: EasyTipView! { get set }
    var timerView: TimerView! { get set }
    var confirmButton: NextButton! { get set }
}

extension CardDeleteConfirmViewInstaller {
    func initSubviews() {
        backImageview = UIImageView()
        backImageview.image = UIImage(named: "img_dash_light_background")
        
        smsSentLabel = UILabel()
        smsSentLabel.numberOfLines = 0
        smsSentLabel.lineBreakMode = .byWordWrapping
        smsSentLabel.font = .gothamNarrow(size: 16, .medium)//EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        smsSentLabel.textAlignment = .center
        smsSentLabel.textColor = .black
        
        yourCodeView = UIView()
        yourCodeView.layer.cornerRadius = Size.TextField.height/2
        yourCodeView.backgroundColor = .white
        
        yourCodeTextField = SMSConfirmationTextField(cornerRadius: Size.TextField.height/2)
        
        var tipPreferences = EasyTipView.Preferences()
        if let font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(13)) {
            tipPreferences.drawing.font = font
        }
        tipPreferences.drawing.foregroundColor = .white
        tipPreferences.drawing.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        tipPreferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        tipPreferences.drawing.arrowHeight = Adaptive.val(10)
        tipPreferences.drawing.arrowWidth = Adaptive.val(20)
        tipPreferences.drawing.borderWidth = Adaptive.val(5)
        tipPreferences.drawing.borderColor = UIColor.white.withAlphaComponent(0.7)
        tipView = EasyTipView(text: RS.lbl_tip.localized(), preferences: tipPreferences)
        
        hideButton = UIButton()
        if #available(iOS 13.0, *) {
            hideButton.setImage(UIImage(named: "btn_eye")?.withTintColor(.black), for: .normal)
        } else {
            hideButton.imageView?.tintColor = .black
        }
        
        timerView = TimerView(frame: CGRect(x: 0, y: 0, width: ApplicationSize.Timer.width, height: ApplicationSize.Timer.width))
        timerView.titleColor = .black
        timerView.shapeColor = .black
        timerView.backShapeColor = .white//ColorConstants.highlightedGray
        
        confirmButton = NextButton.systemButton(with: RS.btn_confirm.localized(), cornerRadius: Size.buttonHeight/2)
        confirmButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        confirmButton.setBackgroundColor(.dashRed, for: .normal)
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageview)
        mainView.addSubview(smsSentLabel)
        mainView.addSubview(yourCodeView)
        yourCodeView.addSubview(yourCodeTextField)
        yourCodeView.addSubview(hideButton)
        mainView.addSubview(timerView)
        mainView.addSubview(confirmButton)
    }
    
    func addSubviewsConstraints() {
        backImageview.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        smsSentLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.leftRightPadding)
            maker.trailing.equalToSuperview().offset(-Size.leftRightPadding)
            maker.top.equalToSuperview().offset(Adaptive.val(63))
        }
        
        yourCodeView.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.paddinfLeadingTrailing)
            maker.trailing.equalToSuperview().offset(-Size.paddinfLeadingTrailing)
            maker.top.equalTo(smsSentLabel.snp.bottom).offset(Adaptive.val(26))
            maker.height.equalTo(Size.TextField.height)
        }
        
        yourCodeTextField.snp.remakeConstraints { (maker) in
            maker.leading.top.bottom.equalTo(yourCodeView)
            maker.trailing.equalTo(hideButton.snp.leading)
        }
        
        hideButton.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(yourCodeView)
            maker.trailing.equalTo(yourCodeView.snp.trailing).offset(-Adaptive.val(21))
            maker.height.equalTo(Adaptive.val(25))
            maker.width.equalTo(Adaptive.val(25))
        }
        
        timerView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(yourCodeTextField.snp.bottom).offset(Adaptive.val(70))
            maker.centerX.equalToSuperview()
            maker.width.height.equalTo(ApplicationSize.Timer.width)
        }
        
        confirmButton.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.leftRightPadding)
            maker.trailing.equalToSuperview().offset(-Size.leftRightPadding)
            maker.height.equalTo(Size.buttonHeight)
            maker.top.equalTo(timerView.snp.bottom).offset(Adaptive.val(31))
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(50)
    static let interItemSpace = Adaptive.val(20)
    static let leftRightPadding = Adaptive.val(16)
    static let paddinfLeadingTrailing = Adaptive.val(83)
    
    struct TextField {
        static let height = Adaptive.val(50)
    }
}
