//
//  CardDeleteConfirmPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CardDeleteConfirmPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: CardDeleteConfirmVM { get }
    func confirmButtonClicked(with smsCode: String?)
    func timerReachedItsEnd()
    func viewDidLoad()
}

protocol CardDeleteConfirmInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didCardDeleteConfirm(with response: ResponseResult<CardDeleteResponse, AppError>)
}

class CardDeleteConfirmPresenter: CardDeleteConfirmPresenterProtocol {
    weak var view: CardDeleteConfirmVCProtocol!
    var interactor: CardDeleteConfirmInteractorProtocol!
    var router: CardDeleteConfirmRouterProtocol!
    var data: DashboardCardVM!
    var message: CardDeleteResponse?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = CardDeleteConfirmVM()
    
    // Private property and methods
    
    func confirmButtonClicked(with smsCode: String?) {
        if isTimerRunning {
            if let smsCode = smsCode, smsCode.count > 0 {
                view.tipView(show: false)
                view.preloader(show: true)
                interactor.removeCardConfirm(id: Int(data.card.cardId), code: smsCode)
            } else {
                view.tipView(show: true)
            }
        } else {
            view.preloader(show: true)
        }
    }
    
    func timerReachedItsEnd() {
        isTimerRunning = false
        view.set(buttonTitle: RS.btn_resend_sms.localized())
    }
    
    // Private property and methods
    
    private var isTimerRunning = true
    
    func viewDidLoad() {
        view.phoneMessage(title: message?.message ?? "")
    }
}

extension CardDeleteConfirmPresenter: CardDeleteConfirmInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didCardDeleteConfirm(with response: ResponseResult<CardDeleteResponse, AppError>) {
        switch response {
        case let .success(result):
            if result.httpStatusCode == 202 {
                appDelegate.router.navigate(to: .mainPage)
            }
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
