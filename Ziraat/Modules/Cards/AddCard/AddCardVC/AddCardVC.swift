//
//  AddCardVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AddCardVCProtocol: BaseViewControllerProtocol {
    func dismissView()
}



class AddCardVC: BaseViewController, AddCardViewInstaller {
    
    var scanCardNumber: String!
    var cardNumber : String!
    var cardName : String!
    var exp : String!
    
    
    var addCardView: AddCardView!
    
    var bgBtn: UIButton!
    
    var mainView: UIView { view }
    var presenter: AddCardPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        self.title = RS.btn_add_card.localized() //"Добавить Карту"
        setupSubviews()
        bgBtn.addTarget(self, action: #selector(self.dismissClicked), for: .touchUpInside)
        addCardView.closeBtn.addTarget(self, action: #selector(dismissClicked), for: .touchUpInside)
        addCardView.addBtn.addTarget(self, action: #selector(addClicked), for: .touchUpInside)
        addCardView.scanerBtn.addTarget(self, action: #selector(openScaner), for: .touchUpInside)
    }
    
    @objc func openScaner(){
        let controller = ScanViewController.createViewController(withDelegate: self)
        controller?.allowSkip = true
        controller?.stringDataSource = self
        self.present(controller!, animated: true, completion: nil)
    }
    func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    @objc func dismissClicked(){
        dismissView()
    }
    
    @objc func addClicked(){
        view.endEditing(true)
        self.presenter.addCard(addCardView.cardNumber, addCardView.exp, addCardView.title)
    }
    
}

extension AddCardVC: AddCardVCProtocol {
    
}

extension AddCardVC : ScanDelegate, ScanStringsDataSource {
    
    func userDidCancel(_ scanViewController: ScanViewController) {
         dismiss(animated: true, completion: nil)
    }
    func userDidScanCard(_ scanViewController: ScanViewController, creditCard: CreditCard) {
        dismiss(animated: true, completion: {
            self.scanCardNumber = creditCard.number
            self.cardNumber = creditCard.number
            self.exp = creditCard.expiryForDisplay()!
            self.cardName = ""
            if creditCard.number.hasPrefix("8600"){
                self.cardName = "Uzcard"
            }
            if creditCard.number.hasPrefix("9860"){
                self.cardName = "Humo"
            }
            self.addCardView.numberField.text = self.scanCardNumber
            self.addCardView.expField.text = self.exp
            self.addCardView.titleField.text = self.cardName
            
            self.addCardView.cardNumber = self.scanCardNumber
            self.addCardView.exp = self.exp
            self.addCardView.title = self.cardName
            
            self.addCardView.validateItems()
        })
    }
    
    func userDidSkip(_ scanViewController: ScanViewController) {
        
    }
    func scanCard() -> String {
        return ""
    }
    func positionCard() -> String {
        return ""
        
    }
    func backButton() -> String {
        return RS.lbl_cancel.localized()
    }
    func skipButton() -> String {
        return ""
    }
}
