//
//  AddCardViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AddCardViewInstaller: ViewInstaller {
    // declare your UI elements here
    var bgBtn: UIButton! { get set }
    var addCardView: AddCardView! { get set }
}

extension AddCardViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        bgBtn = UIButton()
        bgBtn.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        addCardView = AddCardView(frame: .zero)
        addCardView.backgroundColor = .white
        addCardView.layer.cornerRadius = 10
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
        mainView.addSubview(bgBtn)
        mainView.addSubview(addCardView)
    }
    
    func addSubviewsConstraints() {
        bgBtn.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        let left = Adaptive.val(15)
        addCardView.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.centerY.equalToSuperview()
        }
        // Constraints of your UI elements goes here
    }
}

fileprivate struct Size {
    
}
