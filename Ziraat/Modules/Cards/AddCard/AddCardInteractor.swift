//
//  AddCardInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation


protocol AddCardInteractorProtocol: class {
    func addCard(_ number:String,_ exp:String, _ title: String)
}

class AddCardInteractor: AddCardInteractorProtocol {
    weak var presenter: AddCardInteractorToPresenterProtocol!
    
    func addCard(_ number: String, _ exp: String, _ title: String) {
        NetworkService.Card.addCard(pan: number, exp: exp, title: title) { [weak self] (result) in
            self?.presenter.didAddCard(with: result)
        }
    }
}
