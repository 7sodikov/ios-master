//// Header

import UIKit
import InputMask

class AddCardView: BaseView {
    let numberLabel = UILabel()
    let expLabel = UILabel()
    let numberField = LoginTextField(with: "XXXX XXXX XXXX XXXX", keyboard: .numberPad, cornerRadius: 15, isSecureTextEntry: false)
    let expField = LoginTextField(with: "MM/YY", keyboard: .numberPad, cornerRadius:15, isSecureTextEntry: false)
    let titleField = ClipboardControlledTextField()
    let addBtn = UIButton()
    let closeBtn = UIButton()
    let scanerBtn = UIButton()
    
    let redContainerView = UIView()
    fileprivate let helpView = UIView()
    let cardNumberFieldListener = MaskedTextFieldDelegate()
    let expiryDateFieldListener = MaskedTextFieldDelegate()
    var exp = ""
    var cardNumber = ""
    var title = ""
    
    
    override func initSubviews() {
        redContainerView.layer.cornerRadius = 10
        redContainerView.backgroundColor = ColorConstants.red.withAlphaComponent(0.5)
        
        numberLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(14))
        expLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(14))
        expLabel.textAlignment = .right
        
        numberLabel.text = RS.lbl_card_number.localized() //"Номер карты"
        expLabel.text = RS.lbl_expiry_date.localized()
        
        numberField.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        expField.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        
        numberField.layer.cornerRadius = Adaptive.val(40)/2
        expField.layer.cornerRadius = Adaptive.val(40)/2
        titleField.layer.cornerRadius = Adaptive.val(40)/2
        
        let left = Adaptive.val(15)
        numberField.setLeftPaddingPoints(left)
        titleField.setLeftPaddingPoints(left)
        expField.setLeftPaddingPoints(left)
        
        numberField.backgroundColor = .white
        expField.backgroundColor = .white
        titleField.backgroundColor = .white
        
        changeBtn(addBtn, RS.btn_add.localized(), Adaptive.val(45)/2)
        changeBtn(closeBtn, RS.btn_close.localized(), Adaptive.val(45)/2)
        helpView.backgroundColor = .clear
        
        cardNumberFieldListener.listener = self
        cardNumberFieldListener.affinityCalculationStrategy = .prefix
        cardNumberFieldListener.primaryMaskFormat = "[0000] [0000] [0000] [0000]"
        numberField.delegate = cardNumberFieldListener
            
        expiryDateFieldListener.primaryMaskFormat = "[00]/[00]"
        expField.delegate = expiryDateFieldListener
        expiryDateFieldListener.listener = self
        titleField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        scanerBtn.backgroundColor = .clear
        scanerBtn.setImage(UIImage(named: "scan")?.withRenderingMode(.alwaysTemplate), for: .normal)
        scanerBtn.tintColor = ColorConstants.red
        
    }
    
    @objc func textFieldDidChange(){
        title = titleField.string
        validateItems()
    }
    func changeBtn(_ btn : UIButton, _ title: String, _ cornerRadius: CGFloat){
        btn.layer.masksToBounds = true
        btn.setTitleColor(.white, for: .normal)
        btn.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        btn.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        btn.layer.cornerRadius = cornerRadius
        btn.setTitle(title, for: .normal)
        btn.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
    }
    
    override func embedSubviews() {
        addSubview(redContainerView)
        addSubview(numberLabel)
        addSubview(expLabel)
        addSubview(numberField)
        addSubview(expField)
        addSubview(titleField)
        addSubview(addBtn)
        addSubview(closeBtn)
        addSubview(scanerBtn)
        addSubview(helpView)
    }
    override func addSubviewsConstraints() {
        redContainerView.snp.makeConstraints { (make) in
            make.left.top.equalTo(Adaptive.val(10))
            make.right.equalTo(-Adaptive.val(10))
            make.bottom.equalTo(titleField.snp.bottom).offset(20)
        }
        let left = Adaptive.val(20)
        let h = Adaptive.val(40)
        numberLabel.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(20))
            make.left.equalTo(left)
            make.right.equalTo(Adaptive.val(15))
            make.height.equalTo(Adaptive.val(20))
        }
        numberField.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.top.equalTo(numberLabel.snp.bottom).offset(Adaptive.val(15))
            make.right.equalTo(expField.snp.left).offset(-20)
            make.height.equalTo(h)
        }
        expLabel.snp.makeConstraints { (make) in
            make.right.equalTo(-left)
            make.centerY.equalTo(numberLabel)
        }
        expField.snp.makeConstraints { (make) in
            make.right.equalTo(-left)
            make.width.equalTo(Adaptive.val(80))
            make.height.equalTo(h)
            make.centerY.equalTo(numberField.snp.centerY)
        }
        titleField.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.height.equalTo(h)
            make.right.equalTo(-80)
            make.top.equalTo(numberField.snp.bottom).offset(Adaptive.val(15))
        }
        scanerBtn.snp.makeConstraints { (make) in
            make.height.width.equalTo(35)
            make.centerY.equalTo(titleField.snp.centerY)
            make.right.equalTo(-left)
        }
        helpView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalTo(10)
            make.top.equalTo(redContainerView.snp.bottom).offset(10)
        }
        addBtn.snp.makeConstraints { (make) in
            make.right.equalTo(helpView.snp.left).offset(-10)
            make.height.equalTo(Adaptive.val(45))
            make.top.equalTo(redContainerView.snp.bottom).offset(10)
            make.bottom.equalTo(-Adaptive.val(15))
            make.left.equalTo(left + 15)
        }
        closeBtn.snp.makeConstraints { (make) in
            make.right.equalTo(-left - 15)
            make.centerY.equalTo(addBtn)
            make.left.equalTo(helpView.snp.right).offset(10)
            make.height.equalTo(Adaptive.val(45))
        }
        
        validateItems()
    }
    func validateItems(){
        var enabled = false
        if (self.exp.count >= 4 &&
                self.cardNumber.count == 16 && self.title.count > 3){
            enabled = true
        }
        addBtn.isEnabled = enabled
        
    }
    
}

extension AddCardView : MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        if textField == numberField {
            textField.layer.borderWidth = 1
            
            if value.luhnCheck(){
                textField.layer.borderColor = UIColor.green.cgColor
                cardNumber = value
            } else{
                if complete {
                    textField.layer.borderColor = UIColor.red.cgColor
                    textField.shake(nil)
                    cardNumber = ""
                } else{
                    textField.layer.borderColor = UIColor.clear.cgColor
                }
            }
            
        }
        
        if textField == expField {
            if value.isEmpty == true || value.count < 4 {
                exp = value
            } else {
                var splitedValue = value.split(every: 2)
                splitedValue.swapAt(0, 1)
                let stringArray = splitedValue.map{ String($0) }
                let finalExpDate = stringArray.joined(separator: "")
                exp = finalExpDate
            }
        }
        validateItems()
    }
}

