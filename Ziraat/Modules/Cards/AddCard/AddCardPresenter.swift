//
//  AddCardPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AddCardProtocol {
    func didSuccessAddCard()
    func didErrorAddCard(_ error:String)
}
protocol AddCardPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: AddCardVM { get }
    func addCard(_ number:String,_ exp:String, _ title: String)
}

protocol AddCardInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didAddCard(with response: ResponseResult<SingleMessageResponse, AppError>)
    
}

class AddCardPresenter: AddCardPresenterProtocol {
    weak var view: AddCardVCProtocol!
    var interactor: AddCardInteractorProtocol!
    var router: AddCardRouterProtocol!
    var delegate : AddCardProtocol?
    // VIEW -> PRESENTER
    private(set) var viewModel = AddCardVM()
    
    func addCard(_ number: String, _ exp: String, _ title: String) {
        self.interactor.addCard(number, exp, title)
    }
    
}

extension AddCardPresenter: AddCardInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didAddCard(with response: ResponseResult<SingleMessageResponse, AppError>) {
        view.dismissView()
        switch response {
        case let .success(_):
            self.delegate?.didSuccessAddCard()
        break
        case let .failure(error):
            self.delegate?.didErrorAddCard(error.localizedDescription)
            break
        }
        
    }
    
}
