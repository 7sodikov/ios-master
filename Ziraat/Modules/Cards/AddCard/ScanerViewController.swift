//// Header

import UIKit

protocol ScanerCardProtocol: class, ScanDelegate, ScanStringsDataSource{

    var scanCardNumber: String! {get set}
    var cardNumber : String! {get set}
    var cardName : String! {get set}
    var exp : String {get set}
}

extension ScanerCardProtocol {
    func  openScanCard(){
        let controller = ScanViewController.createViewController(withDelegate: self)
        controller?.allowSkip = true
        controller?.stringDataSource = self
        //self.navigationController?.present(controller!, animated: true, completion: nil)
    }
    
    
    func userDidCancel(_ scanViewController: ScanViewController) {
        // self.navigationController?.dismiss(animated: true, completion: nil)
    }
    func userDidScanCard(_ scanViewController: ScanViewController, creditCard: CreditCard) {
        //        self.navigationController?.dismiss(animated: true, completion: {
        self.scanCardNumber = creditCard.number
        self.cardNumber = creditCard.number
        self.exp = creditCard.expiryForDisplay()!
        if creditCard.number.hasPrefix("8600"){
            //                self.cardName = Card.bankName(self.cardNumber ?? "")
            self.cardName = "Uzcard"
        }
        if creditCard.number.hasPrefix("9860"){
            self.cardName = "Humo"
        }
    }
    
    func userDidSkip(_ scanViewController: ScanViewController) {
        
    }
    func scanCard() -> String {
        return RS.lbl_scan.localized()
    }
    func positionCard() -> String {
        return RS.lbl_scan.localized() //"scanCardNumber".localized()
        
    }
    func backButton() -> String {
        return RS.lbl_back.localized()
    }
    func skipButton() -> String {
        return ""
    }
}


class ScanerViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
