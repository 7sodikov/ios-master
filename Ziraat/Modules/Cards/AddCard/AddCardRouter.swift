//
//  AddCardRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AddCardRouterProtocol: class {
    static func createModule(_ delegate : AddCardProtocol) -> UIViewController
}

class AddCardRouter: AddCardRouterProtocol {
    static func createModule(_ delegate : AddCardProtocol) -> UIViewController{
        let vc = AddCardVC()
        let presenter = AddCardPresenter()
        presenter.delegate = delegate
        let interactor = AddCardInteractor()
        let router = AddCardRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
