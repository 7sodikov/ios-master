//
//  FavoriteAddVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class FavoriteAddVM {
    let userEntry = FavoriteAddUserEntry()
    
    var isFieldValid: Bool {
        guard let favoriteTitle = userEntry.favoriteTitle else { return false }
        return favoriteTitle.count > 0
    }
}
