//
//  FavoriteAddViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FavoriteAddViewInstaller: ViewInstaller {
    // declare your UI elements here
}

extension FavoriteAddViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
    }
}

fileprivate struct Size {
    
}
