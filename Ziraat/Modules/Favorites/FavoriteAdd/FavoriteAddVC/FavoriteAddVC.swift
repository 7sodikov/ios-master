//
//  FavoriteAddVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FavoriteAddVCProtocol: BaseViewControllerProtocol {
    func nextButton(enable: Bool)
}

class FavoriteAddVC: BaseViewController, PaymentDataEntryFormViewInstaller {
    var backgroundImage: UIImageView!
    
    var operationStackOrderView: OperationStackOrderView!
    var closeButton: Button!
    var nextButton: Button!
    var mainView: UIView { view }
    var formCoverView: UIView!
    var coverStackView: UIStackView!
    var titleStackView: UIStackView!
    var formStackView: UIStackView!
    var buttonsVerticalStackView: UIStackView!
    var buttonsStackView: UIStackView!
    var titleLabel: UILabel!
    var cardFieldView: PaymentDataEntryFormMyCardField!
    
    let nameField = PaymentDataEntryFormField(frame: .zero)
    
    var presenter: FavoriteAddPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        Utils.delay(seconds: 0.3) {
            UIView.animate(withDuration: 0.3) {
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }
        }
        
        titleLabel.text = RS.btn_add_to_favourites.localized()
        
        cardFieldView.isHidden = true
        
        nameField.set(title: RS.lbl_title.localized(), with: false)
        nameField.textField.placeholder = RS.lbl_title.localized()
        nameField.textField.addTarget(self, action: #selector(nameFieldDidChange(_:)), for: .editingChanged)
        formStackView.addArrangedSubview(nameField)
        
        nextButton.setTitle(RS.btn_add.localized().uppercased(), for: .normal)
        
        closeButton.addTarget(self, action: #selector(closeBtnClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextBtnClicked(_:)), for: .touchUpInside)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    override func preloader(show: Bool) {
//        nextButton.animateIndicator = show
        closeButton.isEnabled = !show
        nameField.isUserInteractionEnabled = !show
    }
    
    @objc private func nameFieldDidChange(_ sender: UITextField) {
        presenter.nameFieldDidChange(text: sender.text)
    }
    
    @objc private func closeBtnClicked(_ sender: NextButton) {
        presenter.closeBtnClicked()
    }
    
    @objc private func nextBtnClicked(_ sender: NextButton) {
        presenter.nextBtnClicked()
    }
}

extension FavoriteAddVC: FavoriteAddVCProtocol {
    func nextButton(enable: Bool) {
        nextButton.isEnabled = enable
    }
}
