//
//  FavoriteAddInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FavoriteAddInteractorProtocol: class {
    func favoriteAdd(for operationId: Int, title: String)
}

class FavoriteAddInteractor: FavoriteAddInteractorProtocol {
    weak var presenter: FavoriteAddInteractorToPresenterProtocol!
    
    func favoriteAdd(for operationId: Int, title: String) {
        NetworkService.Favorite.favoriteAdd(operationId: operationId, title: title) { (result) in
            self.presenter.didFavoriteAdd(with: result)
        }
    }
}
