//
//  FavoriteAddPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FavoriteAddDelegate: class {
    func favoriteDidAddSuccessfully()
}

protocol FavoriteAddPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: FavoriteAddVM { get }
    func nameFieldDidChange(text: String?)
    func closeBtnClicked()
    func nextBtnClicked()
}

protocol FavoriteAddInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func didFavoriteAdd(with response: ResponseResult<FavoriteAddResponse, AppError>)
}

class FavoriteAddPresenter: FavoriteAddPresenterProtocol {
    weak var view: FavoriteAddVCProtocol!
    var interactor: FavoriteAddInteractorProtocol!
    var router: FavoriteAddRouterProtocol!
    weak var delegate: FavoriteAddDelegate?
    var operationId: Int!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = FavoriteAddVM()
    
    func nameFieldDidChange(text: String?) {
        viewModel.userEntry.favoriteTitle = text
        view.nextButton(enable: viewModel.isFieldValid)
    }
    
    func closeBtnClicked() {
        router.dismiss(view: view, delegate: nil)
    }
    
    func nextBtnClicked() {
        view.preloader(show: true)
        interactor.favoriteAdd(for: operationId, title: viewModel.userEntry.favoriteTitle ?? "")
    }
    
    // Private property and methods
    
}

extension FavoriteAddPresenter: FavoriteAddInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didFavoriteAdd(with response: ResponseResult<FavoriteAddResponse, AppError>) {
        view.preloader(show: false)
        switch response {
        case .success:
            router.dismiss(view: view, delegate: delegate)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
}
