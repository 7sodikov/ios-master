//
//  FavoriteAddRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FavoriteAddRouterProtocol: class {
    static func createModule(for operationId: Int, delegate: FavoriteAddDelegate?) -> UIViewController
    func dismiss(view: Any?, delegate: FavoriteAddDelegate?)
}

class FavoriteAddRouter: FavoriteAddRouterProtocol {
    static func createModule(for operationId: Int, delegate: FavoriteAddDelegate?) -> UIViewController {
        let vc = FavoriteAddVC()
        let presenter = FavoriteAddPresenter()
        let interactor = FavoriteAddInteractor()
        let router = FavoriteAddRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.operationId = operationId
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func dismiss(view: Any?, delegate: FavoriteAddDelegate?) {
        let controller = view as? UIViewController
        UIView.animate(withDuration: 0.3, animations: {
            controller?.view.backgroundColor = UIColor.clear
        },
        completion: { isCompleted in
            controller?.dismiss(animated: true, completion: {
                delegate?.favoriteDidAddSuccessfully()
            })
        })
    }
}
