//
//  FavoritesListPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FavoritesListPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: FavoritesListVM { get }
    func viewDidLoad()
    func collectionViewDidSelectItem(at index: Int)
    func collectionViewDidClickOptionButtonForItem(at index: Int)
    func actionSheetEditClicked()
    func actionSheetDeleteClicked()
    func deleteConfirmationAlertYesClicked()
}

protocol FavoritesListInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didFavoriteList(with response: ResponseResult<FavoriteListResponse, AppError>)
    func didFavoriteDelete(with response: ResponseResult<SingleMessageResponse, AppError>, favoriteIndex: Int)
}

class FavoritesListPresenter: FavoritesListPresenterProtocol {
    weak var view: FavoritesListVCProtocol!
    var interactor: FavoritesListInteractorProtocol!
    var router: FavoritesListRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = FavoritesListVM()
    
    func viewDidLoad() {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.favoriteList()
    }
    
    func collectionViewDidSelectItem(at index: Int) {
        let listItem = viewModel.favoritesMergedList[index]
        if let favourite = listItem as? FavoriteListOperationsResponse {
            //            router.navigateToOperationForm(for: listItem, from: view)
            if [OperationPrepareType.pca, .acp].map(\.rawValue).contains(favourite.operationMode) {
                router.navigateToConversion(favorite: favourite, from: view)
            } else {
                router.navigateToMoneyTransfer(favorite: favourite, from: view)
            }
            
        } else {
            router.navigateToPaymentForm(for: listItem as! FavoriteListPaymentsResponse, from: view)
        }
    }

    func collectionViewDidClickOptionButtonForItem(at index: Int) {
        viewModel.userEntry.optionClickedForItemIndex = index
        view.showOptionsActionSheet()
    }
    
    func actionSheetEditClicked() {
        let favorite = viewModel.favoritesMergedList[viewModel.userEntry.optionClickedForItemIndex]
        view.showPreloader(true, forItemAt: viewModel.userEntry.optionClickedForItemIndex)
        router.navigateToFavoriteEdit(for: favorite, view: view, delegate: self)
    }
    
    func actionSheetDeleteClicked() {
        view.showDeletingConfirmationAlert()
    }
    
    func deleteConfirmationAlertYesClicked() {
        let vm = viewModel.favoritesListItemVM(at: viewModel.userEntry.optionClickedForItemIndex)
        guard let favoriteId = vm.favoriteId else { return }
        view.showPreloader(true, forItemAt: viewModel.userEntry.optionClickedForItemIndex)
        view.collectionView(enabled: false)
        interactor.favoriteDelete(for: UInt64(favoriteId), favoriteIndex: viewModel.userEntry.optionClickedForItemIndex)
    }
    
    // Private property and methods
    
}

extension FavoritesListPresenter: FavoritesListInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didFavoriteList(with response: ResponseResult<FavoriteListResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.favoritesList = result.data
            view.showReloadButtonForResponses(false, animateReloadButton: false)
            view.warningMessageView.isHidden = !result.data.isEmpty
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            break
        }
    }
    
    func didFavoriteDelete(with response: ResponseResult<SingleMessageResponse, AppError>, favoriteIndex: Int) {
        view.collectionView(enabled: true)
        view.showPreloader(false, forItemAt: favoriteIndex)
        
        switch response {
        case .success:
            viewModel.removeFromMergedList(at: favoriteIndex)
            view.didRemoveItem(at: favoriteIndex)
            
            if viewModel.favoritesMergedList.count == 0 {
                viewDidLoad()
            }
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            //            view.showReloadButtonForResponses(true, animateReloadButton: false)
            break
        }
    }
}

extension FavoritesListPresenter: FavoriteEditDelegate {
    func favoriteDidEditSuccessfully(with newTitle: String) {
        view.showPreloader(false, forItemAt: viewModel.userEntry.optionClickedForItemIndex)
        let listItem = viewModel.favoritesMergedList[viewModel.userEntry.optionClickedForItemIndex]
        
        if listItem is FavoriteListOperationsResponse {
            (listItem as? FavoriteListOperationsResponse)?.title = newTitle
            
        } else {
            (listItem as? FavoriteListPaymentsResponse)?.title = newTitle
        }
        
        view.reloadItem(at: viewModel.userEntry.optionClickedForItemIndex)
    }
    
    func favoriteEditingCancelled() {
        view.showPreloader(false, forItemAt: viewModel.userEntry.optionClickedForItemIndex)
    }
}

extension FavoriteListOperationsResponse {
    
    func toHistory() -> HistoryResponse {
        .init(
            operationId: Int(favoriteID),
            date: createdAt,
            time: nil,
            sender: sender,
            senderId: senderID,
            senderOwner: senderOwner,
            senderPan: nil,
            senderBranch: senderBranch,
            receiver: receiver,
            receiverId: receiverID,
            receiverOwner: receiverOwner,
            receiverPan: nil,
            receiverBranch: receiverBranch,
            serviceId: nil,
            serviceType: nil,
            amount: amount,
            commission: nil,
            total: Int(amount),
            status: nil,
            operationType: operationType,
            operationCode: nil,
            operationMode: OperationMode.init(rawValue: operationMode),
            icon: nil,
            currency: currency
        )
    }
    
}
