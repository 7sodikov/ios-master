//
//  FavoritesListInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FavoritesListInteractorProtocol: AnyObject {
    func favoriteList()
    func favoriteDelete(for favoriteId: UInt64, favoriteIndex: Int)
}

class FavoritesListInteractor: FavoritesListInteractorProtocol {
    weak var presenter: FavoritesListInteractorToPresenterProtocol!
    
    func favoriteList() {
        NetworkService.Favorite.favoriteList { (result) in
            self.presenter.didFavoriteList(with: result)
        }
    }
    
    func favoriteDelete(for favoriteId: UInt64, favoriteIndex: Int) {
        NetworkService.Favorite.favoriteDelete(favoriteId: favoriteId) { (result) in
            self.presenter.didFavoriteDelete(with: result, favoriteIndex: favoriteIndex)
        }
    }
}
