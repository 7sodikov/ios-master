//
//  FavoritesListVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class FavoritesListItemVM {
    private var favoriteOperation: FavoriteListOperationsResponse?
    private var favoritePayment: FavoriteListPaymentsResponse?
    
    init(favoriteOperation: FavoriteListOperationsResponse?,
         favoritePayment: FavoriteListPaymentsResponse?) {
        
        if favoriteOperation != nil {
            self.favoriteOperation = favoriteOperation
        } else {
            self.favoritePayment = favoritePayment
        }
    }
    
    var logoURL: String {
        if favoriteOperation != nil {
            return favoriteOperation?.logo ?? ""
        } else {
            return favoritePayment?.logo ?? ""
        }
    }
    
    var title: String? {
        if favoriteOperation != nil {
            return favoriteOperation?.title
        } else {
            return favoritePayment?.title
        }
    }
    
    var favoriteId: Int? {
        favoriteOperation != nil ? favoriteOperation?.favoriteID : favoritePayment?.favoriteID
    }
}

class FavoritesListVM {
    let userEntry = FavoritesListUserEntry()
    
    var favoritesList: FavoriteListResponse? {
        didSet {
            guard let favoritesList = favoritesList else {
                favoritesMergedList = []
                return
            }
            
            favoritesMergedList = favoritesList.operations + favoritesList.payments
        }
    }
    
    private(set) var favoritesMergedList: [Any] = []
    
    func favoritesListItemVM(at index: Int) -> FavoritesListItemVM {
        let listItem = favoritesMergedList[index]
        
        if listItem is FavoriteListOperationsResponse {
            return FavoritesListItemVM(favoriteOperation: listItem as? FavoriteListOperationsResponse,
                                       favoritePayment: nil)
        } else {
            return FavoritesListItemVM(favoriteOperation: nil,
                                       favoritePayment: listItem as? FavoriteListPaymentsResponse)
        }
    }
    
    func removeFromMergedList(at index: Int) {
        if index >= 0 && index < favoritesMergedList.count {
            favoritesMergedList.remove(at: index)
        }
    }
}
