//
//  FavoritesListViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FavoritesListViewInstaller: ViewInstaller {
    // declare your UI elements here
}

extension FavoritesListViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
    }
}

fileprivate struct Size {
    
}
