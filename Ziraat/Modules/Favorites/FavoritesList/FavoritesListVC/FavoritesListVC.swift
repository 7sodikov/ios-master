//
//  FavoritesListVC.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FavoritesListVCProtocol: BaseViewControllerProtocol, ServiceListViewInstaller {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
    func showOptionsActionSheet()
    func showDeletingConfirmationAlert()
    func collectionView(enabled: Bool)
    func showPreloader(_ show: Bool, forItemAt index: Int)
    func didRemoveItem(at index: Int)
    func reloadItem(at index: Int)
}

class FavoritesListVC: BaseViewController, FavoritesListVCProtocol {
    
    var warningMessageView: NoItemsAvailableView!
    var mainView: UIView { view }
    var bgImageView: UIImageView!
    var reloadButtonView: ReloadButtonView!
    var collectionLayout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var presenter: FavoritesListPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        title = RS.lbl_favorites.localized()
        
        reloadButtonView.reloadButton.addTarget(self, action: #selector(reloadButtonClicked(_:)), for: .touchUpInside)
        collectionView.dataSource = self
        collectionView.delegate = self
        presenter.viewDidLoad()
        warningMessageView.titleLabel.text = RS.lbl_no_favourites.localized()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    @objc private func reloadButtonClicked(_ sender: UIButton) {
        presenter.viewDidLoad()
    }
    
    private func collectionCell(for index: Int) -> ServiceCategoryCollectionCell? {
        let collectionCell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
        guard let categoryCell = collectionCell as? ServiceCategoryCollectionCell else { return nil }
        return categoryCell
    }
}

extension FavoritesListVC {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        collectionView.isHidden = show
        collectionView.reloadData()
    }
    
    func showOptionsActionSheet() {
        let alert = UIAlertController(title:RS.btn_select_option.localized(), message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: RS.btn_edit.localized(), style: .default , handler:{ (UIAlertAction) in
            self.presenter.actionSheetEditClicked()
        }))
        
        alert.addAction(UIAlertAction(title: RS.btn_delete.localized(), style: .destructive , handler:{ (UIAlertAction) in
            self.presenter.actionSheetDeleteClicked()
        }))
        
        alert.addAction(UIAlertAction(title: RS.lbl_cancel.localized(), style: .cancel, handler:nil))
        
        //uncomment for iPad Support
        //alert.popoverPresentationController?.sourceView = self.view
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showDeletingConfirmationAlert() {
        let alert = UIAlertController(title: RS.al_msg_delete.localized(), message: nil, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: RS.btn_no.localized(), style: .default , handler: nil))
        
        alert.addAction(UIAlertAction(title: RS.btn_yes.localized(), style: .default , handler:{ (UIAlertAction) in
            self.presenter.deleteConfirmationAlertYesClicked()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(enabled: Bool) {
        collectionView.isUserInteractionEnabled = enabled
    }
    
    func showPreloader(_ show: Bool, forItemAt index: Int) {
        let categoryCell = collectionCell(for: index)
        categoryCell?.indicator(animate: show)
    }
    
    func didRemoveItem(at index: Int) {
        collectionView.performBatchUpdates {
            collectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
        } completion: { isFinished in
            
        }
    }
    
    func reloadItem(at index: Int) {
        collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
    }
}

extension FavoritesListVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.viewModel.favoritesMergedList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(ServiceCategoryCollectionCell.self)", for: indexPath) as! ServiceCategoryCollectionCell
        cell.optionButton.isHidden = false
        cell.delegate = self
        let favoriteItem = presenter.viewModel.favoritesListItemVM(at: indexPath.row)
        return cell.setup(with: favoriteItem.title, logoUrl: favoriteItem.logoURL)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return ServiceListSize.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let leftRightPadding = (UIScreen.main.bounds.width - (2 * ServiceListSize.itemSize.width) - ServiceListSize.itemLineSpacing)/2
        return UIEdgeInsets(top: 0, left: leftRightPadding, bottom: 0, right: leftRightPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.collectionViewDidSelectItem(at: indexPath.row)
    }
}

extension FavoritesListVC: ServiceCategoryCollectionCellDelegate {
    func optionButtonClicked(in cell: ServiceCategoryCollectionCell) {
        guard let indexPath = collectionView.indexPath(for: cell) else {
            return
        }
        
        presenter.collectionViewDidClickOptionButtonForItem(at: indexPath.row)
    }
}
