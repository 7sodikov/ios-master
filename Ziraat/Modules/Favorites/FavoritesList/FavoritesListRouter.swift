//
//  FavoritesListRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FavoritesListRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToOperationForm(for favoriteItem: FavoriteListOperationsResponse, from view: Any?)
    func navigateToPaymentForm(for favoriteItem: FavoriteListPaymentsResponse, from view: Any?)
    func navigateToFavoriteEdit(for favorite: Any, view: Any?, delegate: FavoriteEditDelegate?)
    func navigateToMoneyTransfer(favorite: FavoriteListOperationsResponse, from view: Any?)
    func navigateToConversion(favorite: FavoriteListOperationsResponse, from view: Any?)
}

class FavoritesListRouter: FavoritesListRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = FavoritesListVC()
        let presenter = FavoritesListPresenter()
        let interactor = FavoritesListInteractor()
        let router = FavoritesListRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToOperationForm(for favoriteItem: FavoriteListOperationsResponse, from view: Any?) {
        let viewCtrl = view as? UIViewController
        let operationsView = OperationsRouter.createModule(for: favoriteItem, historyItem: nil)
        viewCtrl?.navigationController?.pushViewController(operationsView, animated: true)
    }
    
    func navigateToPaymentForm(for favoriteItem: FavoriteListPaymentsResponse, from view: Any?) {
        let vc = PaymentDataEntryFormRouter.createModule(for: nil, favoriteItem: favoriteItem, payment: nil)
        let navCtrl = NavigationController(rootViewController: vc)
        navCtrl.modalPresentationStyle = .overCurrentContext
        
        let currentViewCtrl = view as? UIViewController
        currentViewCtrl?.present(navCtrl, animated: true, completion: nil)
    }
    
    func navigateToFavoriteEdit(for favorite: Any, view: Any?, delegate: FavoriteEditDelegate?) {
        let ctrl = FavoriteEditRouter.createModule(with: favorite, delegate: delegate)
        ctrl.modalPresentationStyle = .overCurrentContext
        (view as? UIViewController)?.present(ctrl, animated: true, completion: nil)
    }
    
    func navigateToMoneyTransfer(favorite: FavoriteListOperationsResponse, from view: Any?) {
        let viewCtrl = view as? UIViewController
        let router = MoneyTransfersRouter(for: favorite, historyItem: favorite.toHistory())
        viewCtrl?.navigationController?.pushViewController(router.viewController, animated: true)
    }
    
    func navigateToConversion(favorite: FavoriteListOperationsResponse, from view: Any?) {
        let viewCtrl = view as? UIViewController
        
        let router = ConversionNewRouter(history: favorite.toHistory())
        viewCtrl?.navigationController?.pushViewController(router.viewController, animated: true)
    }
    
}
