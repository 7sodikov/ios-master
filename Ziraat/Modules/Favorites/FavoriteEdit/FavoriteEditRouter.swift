//
//  FavoriteEditRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/1/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FavoriteEditRouterProtocol: class {
    static func createModule(with favoriteToEdit: Any, delegate: FavoriteEditDelegate?) -> UIViewController
    func dismiss(view: Any?, newTitle: String, delegate: FavoriteEditDelegate?, isCancellation: Bool)
}

class FavoriteEditRouter: FavoriteEditRouterProtocol {
    static func createModule(with favoriteToEdit: Any, delegate: FavoriteEditDelegate?) -> UIViewController {
        let vc = FavoriteEditVC()
        let presenter = FavoriteEditPresenter()
        let interactor = FavoriteEditInteractor()
        let router = FavoriteEditRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.viewModel.favoriteToEdit = favoriteToEdit
        presenter.delegate = delegate
        interactor.presenter = presenter
        
        return vc
    }
    
    func dismiss(view: Any?, newTitle: String, delegate: FavoriteEditDelegate?, isCancellation: Bool) {
        let controller = view as? UIViewController
        UIView.animate(withDuration: 0.3, animations: {
            controller?.view.backgroundColor = UIColor.clear
        },
        completion: { isCompleted in
            controller?.dismiss(animated: true, completion: {
                if isCancellation {
                    delegate?.favoriteEditingCancelled()
                } else {
                    delegate?.favoriteDidEditSuccessfully(with: newTitle)
                }
            })
        })
    }
}
