//
//  FavoriteEditVM.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/1/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class FavoriteEditVM {
    let userEntry = FavoriteEditUserEntry()
    var favoriteToEdit: Any! {
        didSet {
            if favoriteToEdit is FavoriteListOperationsResponse {
                userEntry.favoriteTitle = (favoriteToEdit as? FavoriteListOperationsResponse)?.title
            } else {
                userEntry.favoriteTitle = (favoriteToEdit as? FavoriteListPaymentsResponse)?.title
            }
        }
    }
    
    var isFieldValid: Bool {
        guard let favoriteTitle = userEntry.favoriteTitle else { return false }
        return favoriteTitle.count > 0
    }
    
    func getFavoriteId() -> Int {
        (
            favoriteToEdit is FavoriteListOperationsResponse
                ? (favoriteToEdit as? FavoriteListOperationsResponse)?.favoriteID
                : (favoriteToEdit as? FavoriteListPaymentsResponse)?.favoriteID
        ) ?? 0
    }
}
