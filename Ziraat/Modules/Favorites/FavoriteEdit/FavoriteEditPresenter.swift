//
//  FavoriteEditPresenter.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/1/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FavoriteEditDelegate: class {
    func favoriteDidEditSuccessfully(with newTitle: String)
    func favoriteEditingCancelled()
}

protocol FavoriteEditPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var viewModel: FavoriteEditVM { get }
    func nameFieldDidChange(text: String?)
    func closeBtnClicked()
    func nextBtnClicked()
}

protocol FavoriteEditInteractorToPresenterProtocol: AnyObject {
    // INTERACTOR -> PRESENTER
    func didFavoriteEdit(with response: ResponseResult<SingleMessageResponse, AppError>, newTitle: String)
}

class FavoriteEditPresenter: FavoriteEditPresenterProtocol {
    weak var view: FavoriteEditVCProtocol!
    var interactor: FavoriteEditInteractorProtocol!
    var router: FavoriteEditRouterProtocol!
    weak var delegate: FavoriteEditDelegate?
    
    // VIEW -> PRESENTER
    private(set) var viewModel = FavoriteEditVM()
    
    func nameFieldDidChange(text: String?) {
        viewModel.userEntry.favoriteTitle = text
        view.nextButton(enable: viewModel.isFieldValid)
    }
    
    func closeBtnClicked() {
        router.dismiss(view: view, newTitle: "", delegate: delegate, isCancellation: true)
    }
    
    func nextBtnClicked() {
        view.preloader(show: true)
        interactor.favoriteEdit(for: UInt64(viewModel.getFavoriteId()), title: viewModel.userEntry.favoriteTitle ?? "")
    }
    
    // Private property and methods
    
}

extension FavoriteEditPresenter: FavoriteEditInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func didFavoriteEdit(with response: ResponseResult<SingleMessageResponse, AppError>, newTitle: String) {
        view.preloader(show: false)
        switch response {
        case .success:
            router.dismiss(view: view, newTitle: newTitle, delegate: delegate, isCancellation: false)
        case let .failure(error):
            view.showError(message: error.localizedDescription)
            break
        }
    }
}
