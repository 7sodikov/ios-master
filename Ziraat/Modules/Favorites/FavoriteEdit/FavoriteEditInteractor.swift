//
//  FavoriteEditInteractor.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/1/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FavoriteEditInteractorProtocol: class {
    func favoriteEdit(for favoriteId: UInt64, title: String)
}

class FavoriteEditInteractor: FavoriteEditInteractorProtocol {
    weak var presenter: FavoriteEditInteractorToPresenterProtocol!
    
    func favoriteEdit(for favoriteId: UInt64, title: String) {
        NetworkService.Favorite.favoriteEdit(favoriteId: favoriteId, title: title) { (result) in
            self.presenter.didFavoriteEdit(with: result, newTitle: title)
        }
    }
}
