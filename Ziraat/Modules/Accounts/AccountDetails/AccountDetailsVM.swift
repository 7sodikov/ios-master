//
//  AccountDetailsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class AccountDetailsVM {
    var accounts: [AccountResponse]?
    
    var account: AccountResponse!
    
    var accountName: String {
        return account.customerName ?? ""
    }
    
    var branchName: String? {
        return account.branchName
    }
    
    var accountNumber: String {
        return account.number
    }
    
    var createDate: String {
        return account.dateOpen
    }
    
    var balance: String? {
        let b = account.balance
        let formattedB = b/100
        return formattedB.currencyFormattedStr()
        //return formattedMoney(account.balance)
    }
    
    var saldo: String? {
//        let b = account.saldo
//        let formattedB = b ?? 0 / 100
//        return formattedB.currencyFormattedStr()
        return formattedMoney(account.saldo)
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    var accountCurrency: String {
        let curType = CurrencyType.type(for: account.currency)
        return curType?.data.label ?? ""
    }
}
