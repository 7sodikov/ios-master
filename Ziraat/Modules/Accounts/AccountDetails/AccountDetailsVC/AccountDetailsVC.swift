//
//  AccountDetailsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountDetailsVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
}

class AccountDetailsVC: BaseViewController, AccountDetailsViewInstaller {
    var mainView: UIView { view }
    var backView: UIImageView!
    var titleLabel: UILabel!
    var notificationButton: UIButton!
    var homeButton: UIButton!
    var accountNameLabel: UILabel!
    var accountNameValueLabel: UILabel!
    var penIconButton: UIButton!
    var ibanLabel: UILabel!
    var ibanValueLabel: UILabel!
    var ibanIconButton: UIButton!
    var balanceLabel: UILabel!
    var balanceValueLabel: UILabel!
    var balanceStackView: UIStackView!
    var availableBalanceLabel: UILabel!
    var availableBalanceValueLabel: UILabel!
    var availableBalanceStackView: UIStackView!
    var dischargeButton: NextButton!
    var openingDateLabel: UILabel!
    var openingDataValueLabel: UILabel!
    var openingDateStackView: UIStackView!
    var openingDateView: UIView!
    
    var presenter: AccountDetailsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        setup()
                        
        dischargeButton.addTarget(self, action: #selector(dischargeButtonClicked(_:)), for: .touchUpInside)
        ibanIconButton.addTarget(self, action: #selector(shareButtonClicked(_:)), for: .touchUpInside)
        homeButton.addTarget(self, action: #selector(openDashboard), for: .touchUpInside)
        notificationButton.addTarget(self, action: #selector(openMenuVC), for: .touchUpInside)
    }
    
    private func setup(){
        let account = presenter.viewModel
        accountNameLabel.text = account.accountName
        ibanLabel.text = account.branchName
        ibanValueLabel.text = account.accountNumber
        balanceValueLabel.text = "\(account.saldo ?? "0") \(account.accountCurrency)"
        availableBalanceValueLabel.text = "\(account.balance ?? "0") \(account.accountCurrency)"
        openingDataValueLabel.text = account.createDate
    }
    
    private func setupNavigation() {
        self.navigationItem.titleView = titleLabel
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: notificationButton), UIBarButtonItem(customView: homeButton)]
        
//        let dashboardImage = UIImage(named: "img_icon_home")!
//        let menuImage = UIImageView(image: UIImage(named: "img_menu"))
//        menuImage.setImageColor(color: .black)
//
//        let menuBtn = UIButton(type: .custom)
//        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
//        menuBtn.setImage(menuImage.image, for: .normal)
//        menuBtn.addTarget(self, action: #selector(self.openMenuVC), for: .touchUpInside)
//
//        let menuBtnItem = UIBarButtonItem(customView: menuBtn)
//        menuBtnItem.tintColor = .black
//        let currMenuWidth = menuBtnItem.customView?.widthAnchor.constraint(equalToConstant: 7)
//        currMenuWidth?.isActive = true
//        let currMenuHeight = menuBtnItem.customView?.heightAnchor.constraint(equalToConstant: 27)
//        currMenuHeight?.isActive = true
//
//        let dashboardButton   = UIBarButtonItem(image: dashboardImage,  style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
//
//        self.navigationItem.rightBarButtonItems = [menuBtnItem, dashboardButton]
    }
    
    @objc func dischargeButtonClicked(_ sender: NextButton) {
        presenter.dischargeButtonClicked(with: navigationCtrl as Any)
    }
    
    @objc func shareButtonClicked(_ sender: UIButton) {
        presenter.openSetting()
    }
    
    @objc private func openMenuVC() {
        presenter.openActions()
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: false)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension AccountDetailsVC: AccountDetailsVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }

}

