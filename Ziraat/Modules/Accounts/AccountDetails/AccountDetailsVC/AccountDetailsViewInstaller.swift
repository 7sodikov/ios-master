//
//  AccountDetailsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountDetailsViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
    var notificationButton: UIButton! { get set }
    var homeButton: UIButton! { get set }
    var accountNameLabel: UILabel! { get set }
    var penIconButton: UIButton! { get set }
    var ibanLabel: UILabel! { get set }
    var ibanValueLabel: UILabel! { get set }
    var ibanIconButton: UIButton! { get set }
    var balanceLabel: UILabel! { get set }
    var balanceValueLabel: UILabel! { get set }
    var balanceStackView: UIStackView! { get set }
    var availableBalanceLabel: UILabel! { get set }
    var availableBalanceValueLabel: UILabel! { get set }
    var availableBalanceStackView: UIStackView! { get set }
    var dischargeButton: NextButton! { get set }
    var openingDateLabel: UILabel! { get set }
    var openingDataValueLabel: UILabel! { get set }
    var openingDateStackView: UIStackView! { get set }
    var openingDateView: UIView! { get set }
}

extension AccountDetailsViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
            
        titleLabel = UILabel()
        titleLabel.text = RS.nav_ttl_accounts_details.localized()
        titleLabel.font = .gothamNarrow(size: 16, .bold)
        titleLabel.textColor = .dashDarkBlue
        
        notificationButton = UIButton()
        if #available(iOS 13.0, *) {
            notificationButton.setBackgroundImage(UIImage(named: "img_login_settings")?.withTintColor(.dashDarkBlue), for: .normal)
        } else {
            notificationButton.setBackgroundImage(UIImage(named: "img_login_settings"), for: .normal)
        }
        
        homeButton = UIButton()
        homeButton.setBackgroundImage(UIImage(named: "img_icon_home"), for: .normal)
        
        accountNameLabel = UILabel()
        accountNameLabel.numberOfLines = 0
        accountNameLabel.font = .gothamNarrow(size: 15, .medium)
        accountNameLabel.textColor = .dashDarkBlue
        
        penIconButton = UIButton()
        penIconButton.setImage(UIImage(named: "btn_icon_pen"), for: .normal)
        penIconButton.contentHorizontalAlignment = .fill
        penIconButton.contentVerticalAlignment = .fill
    
        ibanLabel = UILabel()
        ibanLabel.numberOfLines = 0
        ibanLabel.font = .gothamNarrow(size: 15, .medium)
        ibanLabel.textColor = .dashDarkBlue
        
        ibanValueLabel = UILabel()
        ibanValueLabel.font = .gothamNarrow(size: 15, .medium)
        ibanValueLabel.textColor = .dashDarkBlue
        ibanValueLabel.numberOfLines = 0
        
        ibanIconButton = UIButton()
        ibanIconButton.setImage(UIImage(named: "btn_icon_iban"), for: .normal)
        ibanIconButton.contentHorizontalAlignment = .fill
        ibanIconButton.contentVerticalAlignment = .fill
        
        balanceLabel = UILabel()
        balanceLabel.text = RS.lbl_balance.localized()
        balanceLabel.font = .gothamNarrow(size: 15, .book)
        balanceLabel.textColor = .dashDarkBlue
        balanceLabel.setContentHuggingPriority(.defaultLow, for:.horizontal)
        balanceLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        
        balanceValueLabel = UILabel()
        balanceValueLabel.font = .gothamNarrow(size: 15, .medium)
        balanceValueLabel.textColor = .dashDarkBlue
        balanceValueLabel.setContentHuggingPriority(.defaultHigh, for:.horizontal)
        balanceValueLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        
        balanceStackView = UIStackView()
        balanceStackView.axis = .horizontal
        balanceStackView.distribution = .fill
        balanceStackView.alignment = .fill
        
        availableBalanceLabel = UILabel()
        availableBalanceLabel.text = RS.lbl_available_amount.localized()
        availableBalanceLabel.font = .gothamNarrow(size: 15, .book)
        availableBalanceLabel.textColor = .dashDarkBlue
        availableBalanceLabel.setContentHuggingPriority(.defaultLow, for:.horizontal)
        availableBalanceLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        
        availableBalanceValueLabel = UILabel()
        availableBalanceValueLabel.font = .gothamNarrow(size: 15, .medium)
        availableBalanceValueLabel.textColor = .dashDarkBlue
        availableBalanceValueLabel.setContentHuggingPriority(.defaultHigh, for:.horizontal)
        availableBalanceValueLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        
        availableBalanceStackView = UIStackView()
        availableBalanceStackView.axis = .horizontal
        availableBalanceStackView.distribution = .fill
        availableBalanceStackView.alignment = .fill
        
        dischargeButton = NextButton.systemButton(with: RS.btn_actions_with_account.localized(), cornerRadius: Adaptive.val(20))
        dischargeButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        dischargeButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        
        openingDateLabel = UILabel()
        openingDateLabel.text = RS.lbl_opening_date2.localized()
        openingDateLabel.font = .gothamNarrow(size: 15, .book)
        openingDateLabel.textColor = .dashDarkBlue
        openingDateLabel.setContentHuggingPriority(.defaultLow, for:.horizontal)
        openingDateLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        
        openingDataValueLabel = UILabel()
        openingDataValueLabel.font = .gothamNarrow(size: 15, .medium)
        openingDataValueLabel.textColor = .dashDarkBlue
        openingDataValueLabel.setContentHuggingPriority(.defaultHigh, for:.horizontal)
        openingDataValueLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        
        openingDateStackView = UIStackView()
        openingDateStackView.axis = .horizontal
        openingDateStackView.distribution = .fill
        openingDateStackView.alignment = .fill
        
        openingDateView = UIView()
        openingDateView.backgroundColor = .white
        openingDateView.layer.cornerRadius = Adaptive.val(6)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(accountNameLabel)
        mainView.addSubview(penIconButton)
        mainView.addSubview(ibanLabel)
        mainView.addSubview(ibanValueLabel)
        mainView.addSubview(ibanIconButton)
        mainView.addSubview(dischargeButton)
        
        balanceStackView.addArrangedSubview(balanceLabel)
        balanceStackView.addArrangedSubview(balanceValueLabel)
        mainView.addSubview(balanceStackView)
        
        availableBalanceStackView.addArrangedSubview(availableBalanceLabel)
        availableBalanceStackView.addArrangedSubview(availableBalanceValueLabel)
        mainView.addSubview(availableBalanceStackView)
        
        mainView.addSubview(openingDateView)
        openingDateStackView.addArrangedSubview(openingDateLabel)
        openingDateStackView.addArrangedSubview(openingDataValueLabel)
        mainView.addSubview(openingDateStackView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        accountNameLabel.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
//        penIconButton.snp.remakeConstraints { (maker) in
//            maker.width.height.equalTo(Adaptive.val(18))
//            maker.trailing.equalToSuperview().offset(-Adaptive.val(37))
//            maker.top.equalToSuperview().offset(Adaptive.val(110))
//        }
        
        ibanLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(accountNameLabel.snp.bottom).offset(Adaptive.val(36))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalTo(ibanIconButton.snp.leading).offset(-Adaptive.val(17))
        }
        
        ibanValueLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(ibanLabel.snp.bottom).offset(Adaptive.val(3))
            maker.leading.equalToSuperview().offset(Adaptive.val(37))
        }
        
        ibanIconButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(accountNameLabel.snp.bottom).offset(Adaptive.val(36))
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.width.height.equalTo(Adaptive.val(18))
        }
        
        balanceStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(ibanValueLabel.snp.bottom).offset(Adaptive.val(48))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
        availableBalanceStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(balanceStackView.snp.bottom).offset(Adaptive.val(10))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
        }
        
        dischargeButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(availableBalanceStackView.snp.bottom).offset(Adaptive.val(40))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.height.equalTo(40)
        }
        
        openingDateStackView.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(openingDateView)
            maker.leading.equalTo(openingDateView.snp.leading).offset(Adaptive.val(16))
            maker.trailing.equalTo(openingDateView.snp.trailing).offset(-Adaptive.val(16))
        }
        
        openingDateView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(dischargeButton.snp.bottom).offset(Adaptive.val(10))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(16))
            maker.height.equalTo(Adaptive.val(40))
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(30)
}
