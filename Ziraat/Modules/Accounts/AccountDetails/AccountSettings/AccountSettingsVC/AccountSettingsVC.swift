//
//  AccountSettingsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountSettingsVCProtocol: class {
    
}

class AccountSettingsVC: UIViewController, AccountSettingsViewInstaller {
    var mainView: UIView { view }
    var backView: UIView!
    var messagesTextView: ClipboardControlledTextView!
    var closeBackButton: UIButton!
    var copyLabel: UILabel!
    var copyImageView: UIImageView!
    var copyStackView: UIStackView!
    var copyButton: UIButton!
    var shareLabel: UILabel!
    var shareImageView: UIImageView!
    var shareStackView: UIStackView!
    var shareButton: UIButton!
    var closeLabel: UILabel!
    var closeImageView: UIImageView!
    var closeStackView: UIStackView!
    var closeButton: UIButton!
    var mainStackView: UIStackView!
    var presenter: AccountSettingsPresenterProtocol!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        closeBackButton.addTarget(self, action: #selector(mainViewClicked(_:)), for: .touchUpInside)
        copyButton.addTarget(self, action: #selector(copyButtonClicked), for: .touchUpInside)
        shareButton.addTarget(self, action: #selector(shareButtonClicked), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(mainViewClicked(_:)), for: .touchUpInside)
    }
    
    @objc private func copyButtonClicked() {
        let pasteboard = UIPasteboard.general
        pasteboard.string = presenter.accountNumber
        let alertController = UIAlertController(title: "", message: RS.lbl_account_number_copied.localized(), preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: RS.btn_ok.localized(), style: UIAlertAction.Style.default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc private func shareButtonClicked() {
        let vc = UIActivityViewController(activityItems: [presenter.accountNumber], applicationActivities: nil)
        self.present(vc, animated: true)
    }
    
    @objc private func mainViewClicked(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension AccountSettingsVC: AccountSettingsVCProtocol {
    
}
