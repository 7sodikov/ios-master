//
//  AccountSettingsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountSettingsViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var messagesTextView: ClipboardControlledTextView! { get set }
    var closeBackButton: UIButton! { get set }
    var copyLabel: UILabel! { get set }
    var copyImageView: UIImageView! { get set }
    var copyStackView: UIStackView! { get set }
    var copyButton: UIButton! { get set }
    var shareLabel: UILabel! { get set }
    var shareImageView: UIImageView! { get set }
    var shareStackView: UIStackView! { get set }
    var shareButton: UIButton! { get set }
    var closeLabel: UILabel! { get set }
    var closeImageView: UIImageView! { get set }
    var closeStackView: UIStackView! { get set }
    var closeButton: UIButton! { get set }
    var mainStackView: UIStackView! { get set }
}

extension AccountSettingsViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .moduleBackground
        
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(5)
        
        messagesTextView = ClipboardControlledTextView()
        
        closeBackButton = UIButton()
        closeBackButton.setTitle("", for: .normal)
        
        copyLabel = UILabel()
        copyLabel.font = .gothamNarrow(size: 17, .medium)//EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        copyLabel.textColor = .dashDarkBlue
        copyLabel.text = RS.lbl_copy_iban.localized()
        
        copyImageView = UIImageView()
        copyImageView.image = UIImage(named: "img_copy")
        copyImageView.contentMode = .scaleAspectFit
        
        copyStackView = UIStackView()
        copyStackView.axis = .horizontal
        copyStackView.spacing = Adaptive.val(18)
        
        copyButton = UIButton()
        
        shareLabel = UILabel()
        shareLabel.font = .gothamNarrow(size: 17, .medium)
        shareLabel.textColor = .dashDarkBlue
        shareLabel.text = RS.lbl_share_iban.localized()
        
        shareImageView = UIImageView()
        shareImageView.image = UIImage(named: "img_share_account")
        shareImageView.contentMode = .scaleAspectFit
        
        shareStackView = UIStackView()
        shareStackView.axis = .horizontal
        shareStackView.spacing = Adaptive.val(18)
        
        shareButton = UIButton()
        
        closeLabel = UILabel()
        closeLabel.font = .gothamNarrow(size: 17, .medium) //EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        closeLabel.textColor = .dashDarkBlue
        closeLabel.text = RS.lbl_close.localized()
        
        closeImageView = UIImageView()
        closeImageView.image = UIImage(named: "img_login_close")
        closeImageView.contentMode = .scaleAspectFit
        
        closeStackView = UIStackView()
        closeStackView.axis = .horizontal
        closeStackView.spacing = Adaptive.val(18)
        
        closeButton = UIButton()
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.spacing = Adaptive.val(19)
        mainStackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(mainStackView)
        mainView.addSubview(closeBackButton)
        
        copyStackView.addArrangedSubview(copyImageView)
        copyStackView.addArrangedSubview(copyLabel)
        mainStackView.addArrangedSubview(copyStackView)
        mainView.addSubview(copyButton)
        
        shareStackView.addArrangedSubview(shareImageView)
        shareStackView.addArrangedSubview(shareLabel)
        mainStackView.addArrangedSubview(shareStackView)
        mainView.addSubview(shareButton)
        
        closeStackView.addArrangedSubview(closeImageView)
        closeStackView.addArrangedSubview(closeLabel)
        mainStackView.addArrangedSubview(closeStackView)
        mainView.addSubview(closeButton)
    }
    
    func addSubviewsConstraints() {
        closeBackButton.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalToSuperview()
            maker.bottom.equalTo(backView.snp.top)
        }
        
        backView.snp.remakeConstraints { (maker) in
            maker.leading.trailing.bottom.equalToSuperview()
            maker.height.equalTo(Adaptive.val(147))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(24))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(24))
        }
        
        copyButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(copyStackView)
        }
        
        shareButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(shareStackView)
        }
        
        closeButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(closeStackView)
        }
        
        copyImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
        
        shareImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
        
        closeImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
    }
}

fileprivate struct Size {
    
}
