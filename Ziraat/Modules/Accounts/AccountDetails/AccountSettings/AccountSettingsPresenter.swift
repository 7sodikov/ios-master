//
//  AccountSettingsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AccountSettingsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: AccountSettingsVM { get }
    var accountNumber: String { get }
}

protocol AccountSettingsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class AccountSettingsPresenter: AccountSettingsPresenterProtocol {
    weak var view: AccountSettingsVCProtocol!
    var interactor: AccountSettingsInteractorProtocol!
    var router: AccountSettingsRouterProtocol!
    var account: AccountResponse!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = AccountSettingsVM()
    
    // Private property and methods

    var accountNumber: String {
        return account.number
    }
}

extension AccountSettingsPresenter: AccountSettingsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
