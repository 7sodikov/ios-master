//
//  AccountSettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountSettingsRouterProtocol: class {
    static func createModule(accountNumber: AccountResponse) -> UIViewController
}

class AccountSettingsRouter: AccountSettingsRouterProtocol {
    static func createModule(accountNumber: AccountResponse) -> UIViewController {
        let vc = AccountSettingsVC()
        let presenter = AccountSettingsPresenter()
        let interactor = AccountSettingsInteractor()
        let router = AccountSettingsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.account = accountNumber
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
