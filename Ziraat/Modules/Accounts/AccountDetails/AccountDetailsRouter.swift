//
//  AccountDetailsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountDetailsRouterProtocol: class {
    static func createModule(with account: AccountResponse) -> UIViewController
    func navigateToStatement(in navigationCtrl: Any, data: AccountResponse, currency: String)
    func openSettings(from view: Any?, data: AccountResponse)
    func openActions(from view: Any?, data: AccountResponse)
}

class AccountDetailsRouter: AccountDetailsRouterProtocol {
    static func createModule(with account: AccountResponse) -> UIViewController {
        let vc = AccountDetailsVC()
        let presenter = AccountDetailsPresenter()
        let interactor = AccountDetailsInteractor()
        let router = AccountDetailsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        presenter.viewModel.account = account
        return vc
    }

    func navigateToStatement(in navigationCtrl: Any, data: AccountResponse, currency: String) {
        let confirmationCtrl = AccountStatementRouter.createModule(data: data, currency: currency)
        (navigationCtrl as? UINavigationController)?.pushViewController(confirmationCtrl, animated: true)
    }
    
    func openSettings(from view: Any?, data: AccountResponse) {
        let viewCtrl = view as? UIViewController
        let searchCtrl = AccountSettingsRouter.createModule(accountNumber: data)
        searchCtrl.modalPresentationStyle = .overFullScreen
        viewCtrl?.present(searchCtrl, animated: false, completion: nil)
    }
    
    func openActions(from view: Any?, data: AccountResponse) {
        let viewCtrl = view as? UIViewController
        let searchCtrl = AccountActionsRouter.createModule(accountnumber: data)
        searchCtrl.modalPresentationStyle = .overFullScreen
        viewCtrl?.present(searchCtrl, animated: false, completion: nil)
    }
}
