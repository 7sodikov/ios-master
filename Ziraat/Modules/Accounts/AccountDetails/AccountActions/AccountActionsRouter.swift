//
//  AccountActionsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountActionsRouterProtocol: class {
    static func createModule(accountnumber: AccountResponse) -> UIViewController
    func navigateToErrorPage(navigationCtrl: Any)
}

class AccountActionsRouter: AccountActionsRouterProtocol {
    static func createModule(accountnumber: AccountResponse) -> UIViewController {
        let vc = AccountActionsVC()
        let presenter = AccountActionsPresenter()
        let interactor = AccountActionsInteractor()
        let router = AccountActionsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToErrorPage(navigationCtrl: Any) {
        let viewCtrl = navigationCtrl as! UIViewController
        let statementVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
}
