//
//  AccountActionsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AccountActionsInteractorProtocol: class {
    
}

class AccountActionsInteractor: AccountActionsInteractorProtocol {
    weak var presenter: AccountActionsInteractorToPresenterProtocol!
    
}
