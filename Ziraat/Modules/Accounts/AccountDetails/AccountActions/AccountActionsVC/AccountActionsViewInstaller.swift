//
//  AccountActionsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountActionsViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var closeBackButton: UIButton! { get set }
    var transferBetweenLabel: UILabel! { get set }
    var transferBetweenImageView: UIImageView! { get set }
    var transferBetweenStackView: UIStackView! { get set }
    var transferBetweenButton: UIButton! { get set }
    var transferToOtherLabel: UILabel! { get set }
    var transferToOtherImageView: UIImageView! { get set }
    var transferToOtherStackView: UIStackView! { get set }
    var transferToOtherButton: UIButton! { get set }
    var transferToMyLabel: UILabel! { get set }
    var transferToMyImageView: UIImageView! { get set }
    var transferToMyStackView: UIStackView! { get set }
    var transferToMyButton: UIButton! { get set }
    var closeLabel: UILabel! { get set }
    var closeImageView: UIImageView! { get set }
    var closeStackView: UIStackView! { get set }
    var closeButton: UIButton! { get set }
    var mainStackView: UIStackView! { get set }}

extension AccountActionsViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .moduleBackground
        
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(5)
                
        closeBackButton = UIButton()
        closeBackButton.setTitle("", for: .normal)
        
        transferBetweenLabel = UILabel()
        transferBetweenLabel.font = .gothamNarrow(size: 17, .medium)
        transferBetweenLabel.textColor = .dashDarkBlue
        transferBetweenLabel.text = RS.lbl_transfer.localized()
        
        transferBetweenImageView = UIImageView()
        transferBetweenImageView.image = UIImage(named: "img_transfer_between")
        transferBetweenImageView.contentMode = .scaleAspectFit
        
        transferBetweenStackView = UIStackView()
        transferBetweenStackView.axis = .horizontal
        transferBetweenStackView.spacing = Adaptive.val(18)
        
        transferBetweenButton = UIButton()
        
        transferToOtherLabel = UILabel()
        transferToOtherLabel.font = .gothamNarrow(size: 17, .medium)
        transferToOtherLabel.textColor = .dashDarkBlue
        transferToOtherLabel.text = RS.lbl_transfer_to_other.localized()
        
        transferToOtherImageView = UIImageView()
        transferToOtherImageView.image = UIImage(named: "img_transfer_to_other")
        transferToOtherImageView.tintColor = .black
        transferToOtherImageView.contentMode = .scaleAspectFit
        
        transferToOtherStackView = UIStackView()
        transferToOtherStackView.axis = .horizontal
        transferToOtherStackView.spacing = Adaptive.val(18)
        
        transferToOtherButton = UIButton()
        
        transferToMyLabel = UILabel()
        transferToMyLabel.font = .gothamNarrow(size: 17, .medium)
        transferToMyLabel.textColor = .dashDarkBlue
        transferToMyLabel.text = RS.lbl_transfer_to_card.localized()
        
        transferToMyImageView = UIImageView()
        transferToMyImageView.image = UIImage(named: "img_transfer_to_my")
        transferToMyImageView.tintColor = .black
        transferToMyImageView.contentMode = .scaleAspectFit
        
        transferToMyStackView = UIStackView()
        transferToMyStackView.axis = .horizontal
        transferToMyStackView.spacing = Adaptive.val(18)
        
        transferToMyButton = UIButton()
        
        closeLabel = UILabel()
        closeLabel.font = .gothamNarrow(size: 17, .medium)
        closeLabel.textColor = .dashDarkBlue
        closeLabel.text = RS.lbl_close.localized()
        
        closeImageView = UIImageView()
        closeImageView.image = UIImage(named: "img_login_close")
        closeImageView.contentMode = .scaleAspectFit
        
        closeStackView = UIStackView()
        closeStackView.axis = .horizontal
        closeStackView.spacing = Adaptive.val(18)
        
        closeButton = UIButton()
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.spacing = Adaptive.val(19)
        mainStackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(mainStackView)
        mainView.addSubview(closeBackButton)
        
        transferBetweenStackView.addArrangedSubview(transferBetweenImageView)
        transferBetweenStackView.addArrangedSubview(transferBetweenLabel)
        mainStackView.addArrangedSubview(transferBetweenStackView)
        mainView.addSubview(transferBetweenButton)
        
        transferToOtherStackView.addArrangedSubview(transferToOtherImageView)
        transferToOtherStackView.addArrangedSubview(transferToOtherLabel)
        mainStackView.addArrangedSubview(transferToOtherStackView)
        mainView.addSubview(transferToOtherButton)
        
        transferToMyStackView.addArrangedSubview(transferToMyImageView)
        transferToMyStackView.addArrangedSubview(transferToMyLabel)
        mainStackView.addArrangedSubview(transferToMyStackView)
        mainView.addSubview(transferToMyButton)
        
        closeStackView.addArrangedSubview(closeImageView)
        closeStackView.addArrangedSubview(closeLabel)
        mainStackView.addArrangedSubview(closeStackView)
        mainView.addSubview(closeButton)
    }
    
    func addSubviewsConstraints() {
        closeBackButton.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalToSuperview()
            maker.bottom.equalTo(backView.snp.top)
        }
        
        backView.snp.remakeConstraints { (maker) in
            maker.leading.trailing.bottom.equalToSuperview()
//            maker.height.equalTo(Adaptive.val(147))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(24))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(24))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(24))
        }
        
        transferBetweenButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(transferBetweenStackView)
        }
        
        transferToOtherButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(transferToOtherStackView)
        }
        
        transferToMyButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(transferToMyStackView)
        }
        
        closeButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(closeStackView)
        }
        
        transferBetweenImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
        
        transferToOtherImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
        
        transferToMyImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
        
        closeImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(20))
        }
    }
}

fileprivate struct Size {
    
}
