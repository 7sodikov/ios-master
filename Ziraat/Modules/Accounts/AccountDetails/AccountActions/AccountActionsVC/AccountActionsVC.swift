//
//  AccountActionsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountActionsVCProtocol: class {
    
}

class AccountActionsVC: UIViewController, AccountActionsViewInstaller {
    var mainView: UIView { view }
    var backView: UIView!
    var closeBackButton: UIButton!
    var transferBetweenLabel: UILabel!
    var transferBetweenImageView: UIImageView!
    var transferBetweenStackView: UIStackView!
    var transferBetweenButton: UIButton!
    var transferToOtherLabel: UILabel!
    var transferToOtherImageView: UIImageView!
    var transferToOtherStackView: UIStackView!
    var transferToOtherButton: UIButton!
    var transferToMyLabel: UILabel!
    var transferToMyImageView: UIImageView!
    var transferToMyStackView: UIStackView!
    var transferToMyButton: UIButton!
    var closeLabel: UILabel!
    var closeImageView: UIImageView!
    var closeStackView: UIStackView!
    var closeButton: UIButton!
    var mainStackView: UIStackView!
    
    var presenter: AccountActionsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        closeButton.addTarget(self, action: #selector(mainViewClicked(_:)), for: .touchUpInside)
        transferBetweenButton.addTarget(self, action: #selector(unavailableActionsClicked), for: .touchUpInside)
        transferToOtherButton.addTarget(self, action: #selector(unavailableActionsClicked), for: .touchUpInside)
        transferToMyButton.addTarget(self, action: #selector(unavailableActionsClicked), for: .touchUpInside)
        closeBackButton.addTarget(self, action: #selector(mainViewClicked(_:)), for: .touchUpInside)
    }
    
    @objc private func unavailableActionsClicked() {
        presenter.actionButtonClicked()
    }
    
    @objc private func mainViewClicked(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension AccountActionsVC: AccountActionsVCProtocol {
    
}
