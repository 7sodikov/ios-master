//
//  AccountActionsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AccountActionsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: AccountActionsVM { get }
    func actionButtonClicked()
}

protocol AccountActionsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class AccountActionsPresenter: AccountActionsPresenterProtocol {
    weak var view: AccountActionsVCProtocol!
    var interactor: AccountActionsInteractorProtocol!
    var router: AccountActionsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = AccountActionsVM()
    
    // Private property and methods
    
    func actionButtonClicked() {
        router.navigateToErrorPage(navigationCtrl: view as Any)
    }
}

extension AccountActionsPresenter: AccountActionsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
