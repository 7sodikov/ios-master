//
//  AccountDetailsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AccountDetailsPresenterProtocol: class {
    var viewModel: AccountDetailsVM { get }
    func dischargeButtonClicked(with navigationCtrl: Any)
    func openSetting()
    func openActions()
}

protocol AccountDetailsInteractorToPresenterProtocol: class {
    
}

class AccountDetailsPresenter: AccountDetailsPresenterProtocol {
    weak var view: AccountDetailsVCProtocol!
    var interactor: AccountDetailsInteractorProtocol!
    var router: AccountDetailsRouterProtocol!
    
    private(set) var viewModel = AccountDetailsVM()
    
    func dischargeButtonClicked(with navigationCtrl: Any) {
        let model = viewModel.account
        router.navigateToStatement(in: view.navigationCtrl, data: model!, currency: viewModel.accountCurrency)
    }
    
    // Private property and methods
    
    func openSetting() {
        router.openSettings(from: view, data: viewModel.account)
    }
    
    func openActions() {
        router.openActions(from: view, data: viewModel.account)
    }
}

extension AccountDetailsPresenter: AccountDetailsInteractorToPresenterProtocol {
    
}
