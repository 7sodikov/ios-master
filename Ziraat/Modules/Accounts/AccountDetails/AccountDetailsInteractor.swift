//
//  AccountDetailsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AccountDetailsInteractorProtocol: class {
    
}

class AccountDetailsInteractor: AccountDetailsInteractorProtocol {
    weak var presenter: AccountDetailsInteractorToPresenterProtocol!
    
}
