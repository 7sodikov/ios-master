//
//  AccountStatementVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class AccountStatementVM {
    var statements: [AccountStatementResponse]?
    var items: [AccountStatementItemVM] = []
    
    func add() {
        for statementItem in statements ?? [] {
            items.append(AccountStatementItemVM(statementVM: statementItem))
        }
    }
    
    var fromDay: String {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd"
        var dayComponent        = DateComponents()
        dayComponent.day        = -1
        let theCalendar         = Calendar.current
        let previousDay         = theCalendar.date(byAdding: dayComponent, to: Date())!
        let fromDate            = formatter.string(from: previousDay)
        return fromDate
    }
    
    var fromWeek: String {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd"
        var dayComponent        = DateComponents()
        dayComponent.weekOfYear = -1
        let theCalendar         = Calendar.current
        let previousDay         = theCalendar.date(byAdding: dayComponent, to: Date())!
        let fromDate            = formatter.string(from: previousDay)
        return fromDate
    }
    
    var fromMonth: String {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd"
        var dayComponent        = DateComponents()
        dayComponent.month      = -1
        let theCalendar         = Calendar.current
        let previousDay         = theCalendar.date(byAdding: dayComponent, to: Date())!
        let fromDate            = formatter.string(from: previousDay)
        return fromDate
    }
    
    var fromThreeMonth: String {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd"
        var dayComponent        = DateComponents()
        dayComponent.month      = -3
        let theCalendar         = Calendar.current
        let previousDay         = theCalendar.date(byAdding: dayComponent, to: Date())!
        let fromDate            = formatter.string(from: previousDay)
        return fromDate
    }
    
    var toDay: String {
        let formatter           = DateFormatter()
        formatter.dateFormat    = "yyyy-MM-dd"
        let now                 = Date()
        let toDate              = formatter.string(from:now)
        return toDate
    }
}

class AccountStatementItemVM {
    var statement: AccountStatementResponse
    
    var day: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (statement.vdate) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let day = myCalendar.component(.day, from: date!)
        return String(day)
    }
    
    var month: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (statement.vdate) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let month = myCalendar.component(.month, from: date!)
        if month == 1 {
            return RS.lbl_jan.localized()
        } else if month == 2 {
            return RS.lbl_feb.localized()
        } else if month == 3 {
            return RS.lbl_mar.localized()
        } else if month == 4 {
            return RS.lbl_apr.localized()
        } else if month == 5 {
            return RS.lbl_may.localized()
        } else if month == 6 {
            return RS.lbl_jun.localized()
        } else if month == 7 {
            return RS.lbl_jul.localized()
        } else if month == 8 {
            return RS.lbl_aug.localized()
        } else if month == 9 {
            return RS.lbl_sept.localized()
        } else if month == 10 {
            return RS.lbl_oct.localized()
        } else if month == 11 {
            return RS.lbl_nov.localized()
        } else {
            return RS.lbl_dec.localized()
        }
    }
    
    var year: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: (statement.vdate) ?? "")
        let myCalendar = Calendar(identifier: .gregorian)
        let year = myCalendar.component(.year, from: date!)
        return String(year)
    }
    
    var name: String? {
        return statement.purpose
    }
    
    var amountSdt: String {
        return formattedMoney(statement.sdt) ?? "0" //String((statement.sdt ?? statement.sct) ?? 0)
    }
    
    var amountSct: String {
        return formattedMoney(statement.sct) ?? "0" //String((statement.sdt ?? statement.sct) ?? 0)
    }
    
    var remainingBalance: String? {
        return formattedMoney(statement.sout) //String(statement.sout ?? 0)
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "0" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
    
    init(statementVM: AccountStatementResponse) {
        self.statement = statementVM
    }
}

struct FilterModel {
    var name: String
    
    init(name: String) {
        self.name = name
    }
}
