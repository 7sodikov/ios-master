//
//  AccountStatementPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AccountStatementPresenterProtocol: class {
    var viewModel: AccountStatementVM { get }
    func setupTable(from: String)
    func viewDidLoad()
    func tableViewCellSelected(at index: Int)
    func filterButtonClicked()
    func notificationButtonClicked()
}

protocol AccountStatementInteractorToPresenterProtocol: class {
    func didAccountStatements(with response: ResponseResult<AccountStatementsResponse, AppError>, id: String, from: String, to: String)
}

class AccountStatementPresenter: AccountStatementPresenterProtocol {
    weak var view: AccountStatementVCProtocol!
    var interactor: AccountStatementInteractorProtocol!
    var router: AccountStatementRouterProtocol!
    var account: AccountResponse!

    private(set) var viewModel = AccountStatementVM()
    
    func setupTable(from: String) {
        interactor.accountStatements(id: account.number, from: from, to: viewModel.toDay)
    }
    
    func viewDidLoad() {
        setupTable(from: viewModel.fromMonth)
    }
    
    func tableViewCellSelected(at index: Int) {
        let statement = viewModel.statements?[index]
        router.navigateToStatements(for: statement!, from: view)
    }
    
    func filterButtonClicked() {
//        router.navigateToFilterVC(from: view.navigationCtrl)
        router.openUnderDevelopmentVC(from: view as Any)
    }
    
    func notificationButtonClicked() {
        router.openNotificationList(from: view as Any)
    }
}

extension AccountStatementPresenter: AccountStatementInteractorToPresenterProtocol {
    func didAccountStatements(with response: ResponseResult<AccountStatementsResponse, AppError>, id: String, from: String, to: String) {
        view.preloader(show: false)
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                if result.data.count == 0 {
                    view.showNoStatementsLabel(isOn: true)
                } else {
                    viewModel.statements = result.data.statements
                    view.showNoStatementsLabel(isOn: false)
                    view.reloadTableView()
                }
            }
            break
        case let .failure(_):
            break
        }
    }
}
