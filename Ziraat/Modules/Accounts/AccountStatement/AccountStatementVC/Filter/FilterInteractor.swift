//
//  FilterInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/30/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FilterInteractorProtocol: class {
    
}

class FilterInteractor: FilterInteractorProtocol {
    weak var presenter: FilterInteractorToPresenterProtocol!
    
}
