//
//  FilterPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/30/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol FilterPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: FilterVM { get }
}

protocol FilterInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class FilterPresenter: FilterPresenterProtocol {
    weak var view: FilterVCProtocol!
    var interactor: FilterInteractorProtocol!
    var router: FilterRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = FilterVM()
    
    // Private property and methods
    
}

extension FilterPresenter: FilterInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
