//
//  FilterRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/30/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FilterRouterProtocol: class {
    static func createModule() -> UIViewController
}

class FilterRouter: FilterRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = FilterVC()
        let presenter = FilterPresenter()
        let interactor = FilterInteractor()
        let router = FilterRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
