//
//  FilterViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/30/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FilterViewInstaller: ViewInstaller {
    var backView: UIImageView! { get set }
}

extension FilterViewInstaller {
    func initSubviews() {
        backView = UIImageView()
        backView.image = UIImage(named: "img_dash_light_background")
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
    }
}

fileprivate struct Size {
    
}
