//
//  FilterVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/30/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol FilterVCProtocol: class {
    
}

class FilterVC: BaseViewController, FilterViewInstaller {
    var mainView: UIView { view }
    var backView: UIImageView!
    var presenter: FilterPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        self.title = RS.nav_ttl_filtering.localized()
    }
}

extension FilterVC: FilterVCProtocol {
    
}
