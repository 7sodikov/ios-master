//
//  AccountStatementVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit


protocol AccountStatementVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
    func showNoStatementsLabel(isOn: Bool)
    func reloadTableView()
}

class AccountStatementVC: BaseViewController, AccountStatementViewInstaller {
    var mainView: UIView { view }
    var titleLabel: UILabel!
    var notificationButton: UIButton!
    var homeButton: UIButton!
    var backImageView: UIImageView!
    var filterStackView: UIStackView!
    var filterView: UIView!
    var filterDateLabeL: UILabel!
    var downImage: UIButton!
    var filterButton: UIButton!
    var tableView: UITableView!
    var noStatementLabel: UILabel!
    var noStatementImageView: UIImageView!
    
    var presenter: AccountStatementPresenterProtocol!
    
    fileprivate let cellid = "\(AccountStatementTableViewCell.self)"
    fileprivate let filterCellId = "\(FilterTableViewCell.self)"
    
    // MARK: DropDown
    let transparentView = UIView()
    let filterTableView = UITableView()
    var selectedView = UIView()
    var data = [FilterModel]()
    
    var currency: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
                
        presenter.viewDidLoad()
        
        setupSubviews()
        
        setNavigation()
        
        filterButton.addTarget(self, action: #selector(openfilterView(_:)), for: .touchUpInside)
        
        filterTableView.delegate = self
        filterTableView.dataSource = self
        filterTableView.register(FilterTableViewCell.self, forCellReuseIdentifier: filterCellId)
        filterTableView.tableFooterView = UIView()
        filterTableView.separatorInset = .init(top: 0, left: Adaptive.val(15), bottom: 0, right: Adaptive.val(15))
        
        filterDateLabeL.text = RS.cell_ttl_last_month.localized()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openDropDown(_:)))
        filterView.addGestureRecognizer(tap)
        
        homeButton.addTarget(self, action: #selector(openDashboard), for: .touchUpInside)
        notificationButton.addTarget(self, action: #selector(openNotifications), for: .touchUpInside)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func addTransparentView(frames: CGRect) {
        let window = UIApplication.shared.keyWindow
        transparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        
        filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width, height: 0)
        self.view.addSubview(filterTableView)
        filterTableView.layer.cornerRadius = 5
        
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        filterTableView.reloadData()
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(removeTransparentView))
        transparentView.addGestureRecognizer(tapgesture)
        transparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height + 5, width: frames.width, height: Adaptive.val(200))
        }, completion: nil)
    }
    
    @objc func removeTransparentView() {
        let frames = selectedView.frame
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.filterTableView.frame = CGRect(x: frames.origin.x, y: frames.origin.y + frames.height, width: frames.width, height: 0)
        }, completion: nil)
    }
    
    @objc private func openDropDown(_ gesture: UITapGestureRecognizer) {
        data = [FilterModel(name: RS.cell_ttl_last_day.localized()),
                FilterModel(name: RS.cell_ttl_last_week.localized()),
                FilterModel(name: RS.cell_ttl_last_month.localized()),
                FilterModel(name: RS.cell_ttl_three_month.localized())
        ]
        selectedView = filterView
        addTransparentView(frames: filterView.frame)
    }
    
    @objc private func openfilterView(_ sender: UIButton) {
        presenter.filterButtonClicked()
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func openBell() {
        
    }
    
    func setNavigation() {
        self.navigationItem.titleView = titleLabel
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: homeButton)]
    }
    
    @objc private func openNotifications() {
        presenter.notificationButtonClicked()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isTinted: false)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension AccountStatementVC: AccountStatementVCProtocol, UITableViewDelegate, UITableViewDataSource {
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    func showNoStatementsLabel(isOn: Bool) {
        if isOn == true {
            noStatementLabel.isHidden = false
            noStatementImageView.isHidden = false
            tableView.isHidden = true
        } else {
            noStatementLabel.isHidden = true
            noStatementImageView.isHidden = true
            tableView.isHidden = false
        }
    }
    
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return presenter.viewModel.statements?.count ?? 0
        } else if tableView == self.filterTableView {
            return self.data.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! AccountStatementTableViewCell
            cell.backgroundColor = .clear
            presenter.viewModel.items = []
            presenter.viewModel.add()
            let backgroundView = UIView()
            backgroundView.backgroundColor = .clear
            cell.selectedBackgroundView = backgroundView
            let statementVM = presenter.viewModel.items[indexPath.row]
            return cell.setup(with: statementVM, currency: currency)
        } else if tableView == self.filterTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: filterCellId, for: indexPath) as! FilterTableViewCell
            let backgroundView = UIView()
            backgroundView.backgroundColor = .clear
            cell.selectedBackgroundView = backgroundView
            cell.dataModel = self.data[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
     
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView {
            presenter.tableViewCellSelected(at: indexPath.row)
        } else if tableView == self.filterTableView {
            filterDateLabeL.text = self.data[indexPath.row].name
            if filterDateLabeL.text == data[0].name {
                presenter.setupTable(from: presenter.viewModel.fromDay)
            } else if filterDateLabeL.text == data[1].name {
                presenter.setupTable(from: presenter.viewModel.fromWeek)
            } else if filterDateLabeL.text == data[2].name {
                presenter.setupTable(from: presenter.viewModel.fromMonth)
            } else {
                presenter.setupTable(from: presenter.viewModel.fromThreeMonth)
            }
            self.tableView.reloadData()
            removeTransparentView()
        }
    }
    
}
