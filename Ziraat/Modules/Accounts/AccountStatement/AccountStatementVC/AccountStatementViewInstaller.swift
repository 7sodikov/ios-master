//
//  AccountStatementViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountStatementViewInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var notificationButton: UIButton! { get set }
    var homeButton: UIButton! { get set }
    var backImageView: UIImageView! { get set }
    var filterView: UIView! { get set }
    var filterDateLabeL: UILabel! { get set }
    var downImage: UIButton! { get set }
    var filterButton: UIButton! { get set }
    var tableView: UITableView! { get set }
    var noStatementLabel: UILabel! { get set }
    var noStatementImageView: UIImageView! { get set }
}

extension AccountStatementViewInstaller {
    func initSubviews() {
        titleLabel = UILabel()
        titleLabel.text = RS.nav_ttl_receipt.localized()
        titleLabel.font = .gothamNarrow(size: 16, .bold)
        titleLabel.textColor = .dashDarkBlue
        
        notificationButton = UIButton()
        notificationButton.setBackgroundImage(UIImage(named: "icon_dark_bell"), for: .normal)
        
        homeButton = UIButton()
        homeButton.setBackgroundImage(UIImage(named: "img_icon_home"), for: .normal)
        
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_dash_light_background")
        
        filterView = UIView()
        filterView.backgroundColor = .white
        filterView.layer.borderWidth = Adaptive.val(1)
        filterView.layer.borderColor = UIColor.black.cgColor
        filterView.layer.cornerRadius = Adaptive.val(10)
        
        filterDateLabeL = UILabel()
        filterDateLabeL.textColor = ColorConstants.hintGray
        filterDateLabeL.font = .gothamNarrow(size: 15, .medium)
        
        downImage = UIButton()
        downImage.setImage(UIImage(named: "btn_down"), for: .normal) 
        
        filterButton = UIButton()
        filterButton.setImage(UIImage(named: "btn_filter"), for: .normal)
        
        tableView = UITableView()
        tableView.register(AccountStatementTableViewCell.self, forCellReuseIdentifier: String(describing: AccountStatementTableViewCell.self))
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        
        noStatementLabel = UILabel()
        noStatementLabel.textColor = .black
        noStatementLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(15))
        noStatementLabel.text = RS.lbl_no_statement.localized()
        noStatementLabel.isHidden = true
        
        noStatementImageView = UIImageView()
        noStatementImageView.image = UIImage(named: "img_no_found")
        noStatementImageView.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(filterView)
        mainView.addSubview(filterButton)
        filterView.addSubview(filterDateLabeL)
        filterView.addSubview(downImage)
        mainView.addSubview(tableView)
        mainView.addSubview(noStatementLabel)
        mainView.addSubview(noStatementImageView)
    }
    
    func addSubviewsConstraints() {
        backImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        filterButton.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(119))
            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
            maker.height.equalTo(Adaptive.val(42))
            maker.width.equalTo(Adaptive.val(45))
        }
        
        filterView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(119))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalTo(filterButton.snp.leading).offset(-Size.paddingLeftRight)
            maker.height.equalTo(Adaptive.val(42))
        }
        
        filterDateLabeL.snp.remakeConstraints { (maker) in
            maker.top.equalTo(filterView.snp.top).offset(Adaptive.val(12))
            maker.leading.equalTo(filterView.snp.leading).offset(Adaptive.val(18))
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(filterView.snp.bottom).offset(Adaptive.val(21))
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        
        noStatementImageView.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
        noStatementLabel.snp.remakeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.top.equalTo(noStatementImageView.snp.bottom).offset(Adaptive.val(14))
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(20)
}
