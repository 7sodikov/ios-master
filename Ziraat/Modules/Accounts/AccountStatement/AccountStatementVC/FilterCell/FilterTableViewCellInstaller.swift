//
//  FilterTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/22/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol FilterTableViewCellInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var nameLabel: UILabel! { get set }
}

extension FilterTableViewCellInstaller {
    func initSubviews() {
        backView = UIView()
        backView.backgroundColor = .white
        
        nameLabel = UILabel()
        nameLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        nameLabel.textColor = ColorConstants.gray
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(nameLabel)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
            maker.height.equalTo(Adaptive.val(50))
        }
        
        nameLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView).offset(Adaptive.val(20))
            maker.centerY.equalTo(backView)
        }
    }
}



