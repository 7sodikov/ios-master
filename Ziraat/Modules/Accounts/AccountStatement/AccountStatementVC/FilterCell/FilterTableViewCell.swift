//
//  FilterTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/22/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

class FilterTableViewCell: UITableViewCell, FilterTableViewCellInstaller {
    var backView: UIView!
    var nameLabel: UILabel!
    var mainView: UIView { contentView }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    var dataModel: FilterModel? {
        didSet {
            self.nameLabel.text = dataModel?.name
        }
    }
    
}
