//
//  AccountStatementTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class AccountStatementTableViewCell: UITableViewCell, AccountStatementTableViewCellInstaller {
    var mainView: UIView { contentView }
    var contentBackView: UIView!
    var dayLabel: UILabel!
    var monthLabel: UILabel!
    var yearLabel: UILabel!
    var dateStackView: UIStackView!
    var descriptionLabel: UILabel!
    var amountLabel: UILabel!
    var listIconImage: UIImageView!
    var amountStackView: UIStackView!
    var remainingBalance: UILabel!
    var remainingValueBalance: UILabel!
    var balanceStackView: UIStackView!
    var mainStackView: UIStackView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with statementType: AccountStatementItemVM, currency: String) -> AccountStatementTableViewCell {
        let statementItem = statementType
        dayLabel.text = statementItem.day
        monthLabel.text = statementItem.month
        yearLabel.text = statementItem.year
        descriptionLabel.text = statementItem.name
        if statementType.statement.sdt == 0 {
            amountLabel.text = statementItem.amountSct + " " + currency
            amountLabel.textColor = ColorConstants.dashboardGreen
        } else {
            amountLabel.text = statementItem.amountSdt + " " + currency
            amountLabel.textColor = ColorConstants.red
        }
        remainingValueBalance.text = statementItem.remainingBalance
        return self
    }
}
