//
//  AccountStatementTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol AccountStatementTableViewCellInstaller: ViewInstaller {
    var contentBackView: UIView! { get set }
    var dayLabel: UILabel! { get set }
    var monthLabel: UILabel! { get set }
    var yearLabel: UILabel! { get set }
    var dateStackView: UIStackView! { get set }
    var descriptionLabel: UILabel! { get set }
    var amountLabel: UILabel! { get set }
    var listIconImage: UIImageView! { get set }
    var amountStackView: UIStackView! { get set }
    var remainingBalance: UILabel! { get set }
    var remainingValueBalance: UILabel! { get set }
    var balanceStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
}

extension AccountStatementTableViewCellInstaller {
    func initSubviews() {
        contentBackView = UIView()
        contentBackView.backgroundColor = .white
        contentBackView.layer.cornerRadius = Adaptive.val(6)
        contentBackView.sizeToFit()
        
        dayLabel = UILabel()
        dayLabel.font = .gothamNarrow(size: 25, .book)
        dayLabel.textColor = .dashDarkBlue
        dayLabel.textAlignment = .center
        dayLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        dayLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .vertical)
        dayLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
        dayLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        
        monthLabel = UILabel()
        monthLabel.font = .gothamNarrow(size: 10, .book)
        monthLabel.textColor = .dashDarkBlue
        monthLabel.textAlignment = .center
        monthLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        monthLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .vertical)
        monthLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
        monthLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .vertical)

        yearLabel = UILabel()
        yearLabel.font = .gothamNarrow(size: 10, .book)
        yearLabel.textColor = .dashDarkBlue
        yearLabel.textAlignment = .center
        yearLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        yearLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .vertical)
        yearLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
        yearLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .vertical)

        dateStackView = UIStackView()
        dateStackView.axis = .vertical
        dateStackView.alignment = .center
        dateStackView.distribution = .fillProportionally
        dateStackView.spacing = Adaptive.val(1)
        
        descriptionLabel = UILabel()
        descriptionLabel.font = .gothamNarrow(size: 15, .book)
        descriptionLabel.textColor = .dashDarkBlue
        descriptionLabel.numberOfLines = 0
        
        amountLabel = UILabel()
        amountLabel.font = .gothamNarrow(size: 15, .book)
        amountLabel.textColor = .dashDarkBlue
        amountLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        amountLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
        
        listIconImage = UIImageView()
        listIconImage.image = UIImage(named: "img_list")
        listIconImage.contentMode = .scaleAspectFit
        listIconImage.setContentHuggingPriority(UILayoutPriority(rawValue: 250), for: .horizontal)
        listIconImage.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .horizontal)
        
        amountStackView = UIStackView()
        amountStackView.axis = .vertical
        amountStackView.alignment = .center
        amountStackView.distribution = .fill
        amountStackView.spacing = Adaptive.val(3)
        
        remainingBalance = UILabel()
        remainingBalance.font = .gothamNarrow(size: 10, .medium)
        remainingBalance.textColor = ColorConstants.hintGray
        remainingBalance.text = RS.lbl_remaining.localized()
        
        remainingValueBalance = UILabel()
        remainingValueBalance.font = .gothamNarrow(size: 10, .medium)
        remainingValueBalance.textColor = .dashDarkBlue
        
        balanceStackView = UIStackView()
        balanceStackView.axis = .horizontal
        balanceStackView.spacing = Adaptive.val(5)
        
        mainStackView = UIStackView()
        mainStackView.axis = .horizontal
        mainStackView.alignment = .fill
        mainStackView.distribution = .fillProportionally
        mainStackView.spacing = Adaptive.val(13)
    }
    
    func embedSubviews() {
        mainView.addSubview(contentBackView)
        
        dateStackView.addArrangedSubview(dayLabel)
        dateStackView.addArrangedSubview(monthLabel)
        dateStackView.addArrangedSubview(yearLabel)
        mainStackView.addArrangedSubview(dateStackView)
        
        mainStackView.addArrangedSubview(descriptionLabel)
        
        amountStackView.addArrangedSubview(amountLabel)
        amountStackView.addArrangedSubview(listIconImage)
        mainStackView.addArrangedSubview(amountStackView)
        
        balanceStackView.addArrangedSubview(remainingBalance)
        balanceStackView.addArrangedSubview(remainingValueBalance)
        mainView.addSubview(balanceStackView)
        
        mainView.addSubview(mainStackView)
    }
    
    func addSubviewsConstraints() {
        contentBackView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(2.5))
            maker.leading.equalToSuperview().offset(Adaptive.val(22))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(2.5))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(22))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(contentBackView.snp.top).offset(Adaptive.val(14))
            maker.leading.equalTo(contentBackView.snp.leading).offset(Adaptive.val(12))
            maker.trailing.equalTo(contentBackView.snp.trailing).offset(-Adaptive.val(12))
            maker.bottom.equalTo(balanceStackView.snp.top).offset(-Adaptive.val(5))
        }
        
        listIconImage.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(14))
            maker.height.equalTo(Adaptive.val(15))
        }

        balanceStackView.snp.remakeConstraints { (maker) in
            maker.trailing.equalTo(contentBackView.snp.trailing).offset(-Adaptive.val(15))
            maker.bottom.equalTo(contentBackView.snp.bottom).offset(-Adaptive.val(12))
        }
    }
}



