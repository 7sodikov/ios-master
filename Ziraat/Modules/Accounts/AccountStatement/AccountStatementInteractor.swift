//
//  AccountStatementInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AccountStatementInteractorProtocol: class {
    func accountStatements(id: String, from: String, to: String)
}

class AccountStatementInteractor: AccountStatementInteractorProtocol {
    weak var presenter: AccountStatementInteractorToPresenterProtocol!
    func accountStatements(id: String, from: String, to: String) {
        NetworkService.Accounts.accountStatements(id: id, from: from, to: to) { (result) in
            self.presenter.didAccountStatements(with: result, id: id, from: from, to: to)
        }
    }
}
