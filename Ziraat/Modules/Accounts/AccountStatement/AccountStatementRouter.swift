//
//  AccountStatementRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AccountStatementRouterProtocol: class {
    static func createModule(data: AccountResponse, currency: String) -> UIViewController
    func navigateToStatements(for statement: AccountStatementResponse, from view: Any?)
    func navigateToFilterVC(from view: Any)
    func openUnderDevelopmentVC(from view: Any)
    func openNotificationList(from view: Any)
}

class AccountStatementRouter: AccountStatementRouterProtocol {
    static func createModule(data: AccountResponse, currency: String) -> UIViewController {
        let vc = AccountStatementVC()
        let presenter = AccountStatementPresenter()
        let interactor = AccountStatementInteractor()
        let router = AccountStatementRouter()
        
        vc.presenter = presenter
        vc.currency = currency
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        presenter.account = data
        interactor.presenter = presenter
        
        return vc
    }
    
    func navigateToStatements(for statement: AccountStatementResponse, from view: Any?) {
        let viewCtrl = view as! UIViewController
        let statementVC = TransactionDetailsRouter.createModuleInNavController(statement: statement)
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
    }
    
    func navigateToFilterVC(from view: Any) {
        let filterCtrl = FilterRouter.createModule()
        (view as? UINavigationController)?.pushViewController(filterCtrl, animated: true)
    }
    
    func openUnderDevelopmentVC(from view: Any) {
        let viewCtrl = view as! UIViewController
        let errorVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        errorVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(errorVC, animated: false, completion: nil)
    }
    
    func openNotificationList(from view: Any) {
        let viewCtrl = view as? UIViewController
        let notifListView = NotificationsListRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(notifListView, animated: true)
    }
}
