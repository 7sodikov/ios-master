//
//  SendEmailRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol SendEmailRouterProtocol: class {
    static func createModule() -> UIViewController
}

class SendEmailRouter: SendEmailRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = SendEmailVC()
        let presenter = SendEmailPresenter()
        let interactor = SendEmailInteractor()
        let router = SendEmailRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
