//
//  SendEmailPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol SendEmailPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: SendEmailVM { get }
}

protocol SendEmailInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class SendEmailPresenter: SendEmailPresenterProtocol {
    weak var view: SendEmailVCProtocol!
    var interactor: SendEmailInteractorProtocol!
    var router: SendEmailRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = SendEmailVM()
    
    // Private property and methods
    
}

extension SendEmailPresenter: SendEmailInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
