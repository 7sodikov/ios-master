//
//  SendEmailVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol SendEmailVCProtocol: class {
    
}

class SendEmailVC: UIViewController, SendEmailViewInstaller {
    var mainView: UIView { view }
    var backView: UIView!
    var sendEmailLabel: UILabel!
    var writeEmailLabel: UILabel!
    var emailView: UIView!
    var searchImageView: UIImageView!
    var emailLabel: ClipboardControlledTextField!
    var sendButton: NextButton!
    var cancelButton: NextButton!
    
    var presenter: SendEmailPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
    }
}

extension SendEmailVC: SendEmailVCProtocol {
    
}
