//
//  SendEmailViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol SendEmailViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var sendEmailLabel: UILabel! { get set }
    var writeEmailLabel: UILabel! { get set }
    var emailView: UIView! { get set }
    var searchImageView: UIImageView! { get set }
    var emailLabel: ClipboardControlledTextField! { get set }
    var sendButton: NextButton! { get set }
    var cancelButton: NextButton! { get set }}

extension SendEmailViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        backView = UIView()
        backView.layer.cornerRadius = Adaptive.val(15)
        backView.backgroundColor = .white
        
        sendEmailLabel = UILabel()
        sendEmailLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        sendEmailLabel.textColor = .black
        sendEmailLabel.text = "Send E-Mail"
        
        writeEmailLabel = UILabel()
        writeEmailLabel.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(12))
        writeEmailLabel.textColor = .black
        writeEmailLabel.text = "Write the e-mail to which you want the receipt is sent"
        writeEmailLabel.numberOfLines = 0
        
        emailView = UIView()
        emailView.layer.cornerRadius = Adaptive.val(6)
        emailView.backgroundColor = .white
        emailView.layer.borderWidth = Adaptive.val(1)
        emailView.layer.borderColor = ColorConstants.borderLine.cgColor
        
        searchImageView = UIImageView()
        searchImageView.image = UIImage(named: "img_icon_search")
        
        emailLabel = ClipboardControlledTextField()
        emailLabel.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(14))
        emailLabel.textColor = .black
        emailLabel.placeholder = "oozsahin@gmail.com"
        
        sendButton = NextButton.systemButton(with: "Send email", cornerRadius: Adaptive.val(19))
        sendButton.setBackgroundColor(ColorConstants.red, for: .normal)
        sendButton.titleLabel?.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(12))
        
        cancelButton = NextButton.systemButton(with: "Cancel", cornerRadius: Adaptive.val(19))
        cancelButton.setBackgroundColor(.white, for: .normal)
        cancelButton.titleLabel?.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(12))
        cancelButton.setTitleColor(.black, for: .normal)
        cancelButton.layer.borderWidth = Adaptive.val(1)
        cancelButton.layer.borderColor = ColorConstants.borderLine.cgColor
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(sendEmailLabel)
        mainView.addSubview(writeEmailLabel)
        emailView.addSubview(searchImageView)
        emailView.addSubview(emailLabel)
        mainView.addSubview(emailView)
        mainView.addSubview(sendButton)
        mainView.addSubview(cancelButton)    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
        sendEmailLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.top.equalTo(backView.snp.top).offset(Size.paddingLeftRight/2)
        }
        
        writeEmailLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.paddingLeftRight)
            maker.top.equalTo(sendEmailLabel.snp.bottom).offset(Size.paddingLeftRight/2)
        }
        
        emailView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.paddingLeftRight)
            maker.top.equalTo(writeEmailLabel.snp.bottom).offset(Adaptive.val(19))
            maker.height.equalTo(Adaptive.val(41))
        }
        
        searchImageView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(emailView.snp.leading).offset(Adaptive.val(17))
            maker.top.equalTo(emailView.snp.top).offset(Adaptive.val(12))
            maker.height.equalTo(Adaptive.val(18))
            maker.width.equalTo(Adaptive.val(15))
        }
        
        emailLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(searchImageView.snp.trailing).offset(Adaptive.val(7))
            maker.top.bottom.trailing.equalTo(emailView)//.offset(Adaptive.val(12))
        }
        
        sendButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(emailView.snp.bottom).offset(Adaptive.val(27))
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.paddingLeftRight)
            maker.height.equalTo(Adaptive.val(38))
        }
        
        cancelButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(sendButton.snp.bottom).offset(Adaptive.val(5))
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.paddingLeftRight)
            maker.height.equalTo(Adaptive.val(38))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Size.paddingLeftRight)
        }    }
}

fileprivate struct Size {
    static let marginLeftRight = Adaptive.val(32)
    static let paddingLeftRight = Adaptive.val(24)
}
