//
//  MyAccountsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MyAccountsPresenterProtocol: class {
    var viewModel: MyAccountsVM { get }
    func viewDidLoad()
    func tableViewDidSelectItem(at index: Int, filterIndex: Int)
    func dashboardButtonClicked()
    func notificationButtonClicked()
}

protocol MyAccountsInteractorToPresenterProtocol: class {
    func didGetAllAccounts(with response: ResponseResult<AccountsListResponse, AppError>)
    func didGetAllAccountBalances(with response: ResponseResult<AccountsBalanceResponse, AppError>)
}

class MyAccountsPresenter: MyAccountsPresenterProtocol {
    weak var view: MyAccountsVCProtocol!
    var interactor: MyAccountsInteractorProtocol!
    var router: MyAccountsRouterProtocol!
    
    private(set) var viewModel = MyAccountsVM()
    
    func viewDidLoad() {
        showReloadButtonForAccountsDownload(true, animateReloadButton: true)
        interactor.accountsList()
        interactor.accountsBalance()
    }
    
    // Private property and methods
    private func showReloadButtonForAccountsDownload(_ show: Bool, animateReloadButton: Bool) {
        var show = show
        if viewModel.accounts.count == 0 {
            show = true
        }
        view.showReloadButtonForCardsDownload(show, animateReloadButton: animateReloadButton)
    }
    
    func tableViewDidSelectItem(at index: Int, filterIndex: Int) {
        if filterIndex == 0 {
            let account = viewModel.accounts[index]
            router.navigateToAccounts(for: account, from: view)
        } else if filterIndex ==  1 {
            let account = viewModel.uzsAccounts[index]
            router.navigateToAccounts(for: account, from: view)
        } else if filterIndex ==  2 {
            let account = viewModel.usdAccounts[index]
            router.navigateToAccounts(for: account, from: view)
        } else if filterIndex ==  3 {
            let account = viewModel.eurAccounts[index]
            router.navigateToAccounts(for: account, from: view)
        } else if filterIndex ==  4 {
            let account = viewModel.gbpAccounts[index]
            router.navigateToAccounts(for: account, from: view)
        } else if filterIndex ==  5 {
            let account = viewModel.rubAccounts[index]
            router.navigateToAccounts(for: account, from: view)
        } else {
            let account = viewModel.tryAccounts[index]
            router.navigateToAccounts(for: account, from: view)
        }
    }
    
    func dashboardButtonClicked() {
        router.navigateToDashboard(from: view as Any)
    }
    
    func notificationButtonClicked() {
        router.openNotificationList(from: view as Any)
    }
}

extension MyAccountsPresenter: MyAccountsInteractorToPresenterProtocol {
    func didGetAllAccounts(with response: ResponseResult<AccountsListResponse, AppError>) {
        switch response {
        case let .success(result):
            if result.httpStatusCode == 200 {
                viewModel.accounts = result.data.accounts
                view.reloadSegmentedController(reload: true)
                if viewModel.accounts.count == 0 {
                    view.showReloadButtonForCardsDownload(true, animateReloadButton: false)
                } else {
                    view.showReloadButtonForCardsDownload(false, animateReloadButton: false)
                }
            }
        case let .failure(error):
//            showReloadButtonForAccountsDownload(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
        }
    }
    
    func didGetAllAccountBalances(with response: ResponseResult<AccountsBalanceResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.accountBalance = result.data.accounts
            view.reloadViewForBalanceDownload()
            if viewModel.accountBalance.count == 0 {
                view.showReloadButtonForCardsDownload(true, animateReloadButton: false)
            } else {
                view.showReloadButtonForCardsDownload(false, animateReloadButton: false)
            }
        case let .failure(error):
            view.showError(message: error.localizedDescription)
        }
    }
}
