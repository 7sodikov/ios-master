//
//  MyAccountsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MyAccountsRouterProtocol: class {
    static func createModule() -> UIViewController
    func navigateToAccounts(for account: AccountResponse, from view: Any?)
    func navigateToDashboard(from view: Any)
    func openNotificationList(from view: Any)
}

class MyAccountsRouter: MyAccountsRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = MyAccountsVC()
        let presenter = MyAccountsPresenter()
        let interactor = MyAccountsInteractor()
        let router = MyAccountsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        return vc
    }
    
    func navigateToAccounts(for account: AccountResponse, from view: Any?) {
        let viewCtrl = view as? UIViewController
        let serviceListView = AccountDetailsRouter.createModule(with: account)
        viewCtrl?.navigationController?.pushViewController(serviceListView, animated: true)
    }
    
    func navigateToDashboard(from view: Any) {
        let confirmationCtrl = DashboardRouter.createModule()
        (view as? UINavigationController)?.pushViewController(confirmationCtrl, animated: true)
    }
    
    func openNotificationList(from view: Any) {
        let viewCtrl = view as? UIViewController
        let notifListView = NotificationsListRouter.createModule()
        viewCtrl?.navigationController?.pushViewController(notifListView, animated: true)
    }
}
