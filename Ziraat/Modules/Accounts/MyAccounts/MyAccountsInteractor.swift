//
//  MyAccountsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol MyAccountsInteractorProtocol: class {
    func accountsList()
    func accountsBalance()
}

class MyAccountsInteractor: MyAccountsInteractorProtocol {
    weak var presenter: MyAccountsInteractorToPresenterProtocol!
    func accountsList() {
        NetworkService.Accounts.accountsList() { [weak self] (result) in
            self?.presenter.didGetAllAccounts(with: result)
        }
    }
    
    func accountsBalance() {
        NetworkService.Accounts.accountBalance() { [weak self] (result) in
            self?.presenter.didGetAllAccountBalances(with: result)
        }
    }
}
