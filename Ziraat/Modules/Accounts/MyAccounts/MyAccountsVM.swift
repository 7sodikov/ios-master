//
//  MyAccountsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum FilterType {
    case All
    case UZS
    case USD
    case EUR
    case GBP
    case RUB
    case TRY
    
    var title: String {
        switch self {
        case .All: return RS.lbl_all.localized()
        case .UZS: return "UZS"
        case .USD: return "USD"
        case .EUR: return "EUR"
        case .GBP: return "GBP"
        case .RUB: return "RUB"
        case .TRY: return "TRY"
        }
    }
}

//struct FilterItem {
//    var title: String
//    var type: FilterType
//
//    init(title: String, type: FilterType) {
//        self.title = title
//        self.type = type
//    }
//}

class MyAccountsVM {
    var accounts: [AccountResponse] = []
    var accountBalance: [AccountBalanceResponse] = []
    var filterIndex: Int?
    
    func accountVM(at index: Int) -> MyAccountVM {
        var balance: AccountBalanceResponse?
        if index <= accountBalance.count - 1 {
            balance = accountBalance[index]
        }
        return MyAccountVM(accounts[index], balance)
    }
    
    // MARK: UZS
    var uzsBalance: [AccountBalanceResponse] {
        return accountBalance.filter {$0.currency == CurrencyType.uzs.data.code }
    }
    
    var uzsAccounts: [AccountResponse] {
        return accounts.filter {$0.currency == CurrencyType.uzs.data.code }
    }
    
    func uzsAccountVM(at index: Int) -> MyAccountVM {
        var balance: AccountBalanceResponse?
        if index <= uzsBalance.count - 1 {
            balance = uzsBalance[index]
        }
        return MyAccountVM(uzsAccounts[index], balance)
    }
    
    // MARK: USD
    var usdBalance: [AccountBalanceResponse] {
        return accountBalance.filter {$0.currency == CurrencyType.usd.data.code }
    }
    
    var usdAccounts: [AccountResponse] {
        return accounts.filter {$0.currency == CurrencyType.usd.data.code }
    }
    
    func usdAccountVM(at index: Int) -> MyAccountVM {
        var balance: AccountBalanceResponse?
        if index <= usdBalance.count - 1 {
            balance = usdBalance[index]
        }
        return MyAccountVM(usdAccounts[index], balance)
    }
    
    // MARK: EUR
    var eurBalance: [AccountBalanceResponse] {
        return accountBalance.filter {$0.currency == CurrencyType.eur.data.code }
    }
    
    var eurAccounts: [AccountResponse] {
        return accounts.filter {$0.currency == CurrencyType.eur.data.code }
    }
    
    func eurAccountVM(at index: Int) -> MyAccountVM {
        var balance: AccountBalanceResponse?
        if index <= eurBalance.count - 1 {
            balance = eurBalance[index]
        }
        return MyAccountVM(eurAccounts[index], balance)
    }
    
    // MARK: GBP
    var gbpBalance: [AccountBalanceResponse] {
        return accountBalance.filter {$0.currency == CurrencyType.gbp.data.code }
    }
    
    var gbpAccounts: [AccountResponse] {
        return accounts.filter {$0.currency == CurrencyType.gbp.data.code }
    }
    
    func gbpAccountVM(at index: Int) -> MyAccountVM {
        var balance: AccountBalanceResponse?
        if index <= gbpBalance.count - 1 {
            balance = gbpBalance[index]
        }
        return MyAccountVM(gbpAccounts[index], balance)
    }
    
    // MARK: RUB
    var rubBalance: [AccountBalanceResponse] {
        return accountBalance.filter {$0.currency == CurrencyType.rub.data.code }
    }
    
    var rubAccounts: [AccountResponse] {
        return accounts.filter {$0.currency == CurrencyType.rub.data.code }
    }
    
    func rubAccountVM(at index: Int) -> MyAccountVM {
        var balance: AccountBalanceResponse?
        if index <= rubBalance.count - 1 {
            balance = rubBalance[index]
        }
        return MyAccountVM(rubAccounts[index], balance)
    }
    
    // MARK: TRY
    var tryBalance: [AccountBalanceResponse] {
        return accountBalance.filter {$0.currency == CurrencyType.try.data.code }
    }
    
    var tryAccounts: [AccountResponse] {
        return accounts.filter {$0.currency == CurrencyType.try.data.code }
    }
    
    func tryAccountVM(at index: Int) -> MyAccountVM {
        var balance: AccountBalanceResponse?
        if index <= tryBalance.count - 1 {
            balance = tryBalance[index]
        }
        return MyAccountVM(tryAccounts[index], balance)
    }
    
//    var filterModel: [FilterItem] = [
//                                     FilterItem(title: "All", type: .All),
//                                     FilterItem(title: "UZS", type: .UZS),
//                                     FilterItem(title: "USD", type: .USD),
//                                     FilterItem(title: "EUR", type: .EUR),
//                                     FilterItem(title: "GBP", type: .GBP),
//                                     FilterItem(title: "RUB", type: .RUB),
//                                     FilterItem(title: "TRY", type: .TRY)
//    ]
    
    var filterModel: [FilterType] {
        var items: [FilterType] = []
        items.append(.All)
        
        if uzsAccounts.count > 0 {
            items.append(.UZS)
        }
        
        if usdAccounts.count > 0 {
            items.append(.USD)
        }
        
        if eurAccounts.count > 0 {
            items.append(.EUR)
        }
        
        if gbpAccounts.count > 0 {
            items.append(.GBP)
        }
        
        if rubAccounts.count > 0 {
            items.append(.RUB)
        }
        
        if tryAccounts.count > 0 {
            items.append(.TRY)
        }
        return items
    }
    
//    func makeFilterModel() -> [FilterItem] {
//        let uzsCurrencyType = accounts.filter {$0.currency == CurrencyType.uzs.data.code}
//        let usdCurrencyType = accounts.filter {$0.currency == CurrencyType.usd.data.code}
//        let eurCurrencyType = accounts.filter {$0.currency == CurrencyType.eur.data.code}
//        let gbpCurrencyType = accounts.filter {$0.currency == CurrencyType.gbp.data.code}
//        let rubCurrencyType = accounts.filter {$0.currency == CurrencyType.rub.data.code}
//        let tryCurrencyType = accounts.filter {$0.currency == CurrencyType.try.data.code}
//        
//        if uzsCurrencyType.isEmpty != true {
//            filterModel.append(FilterItem(title: "UZS", type: .UZS))
//            return filterModel
//        } else if usdCurrencyType.isEmpty != true {
//            filterModel.append(FilterItem(title: "USD", type: .USD))
//            return filterModel
//        } else if eurCurrencyType.isEmpty != true {
//            filterModel.append(FilterItem(title: "EUR", type: .EUR))
//            return filterModel
//        } else if gbpCurrencyType.isEmpty != true {
//            filterModel.append(FilterItem(title: "GBP", type: .GBP))
//            return filterModel
//        } else if rubCurrencyType.isEmpty != true {
//            filterModel.append(FilterItem(title: "RUB", type: .RUB))
//            return filterModel
//        } else if tryCurrencyType.isEmpty != true {
//            filterModel.append(FilterItem(title: "TRY", type: .TRY))
//            return filterModel
//        } else {
//            return [FilterItem(title: "", type: .All)]
//        }
//        
//    }
}

class MyAccountVM {
    var account: AccountResponse
    var balance: AccountBalanceResponse?
    
    var currency: String {
        let curType = CurrencyType.type(for: account.currency)
        return curType?.data.label ?? ""
    }
    
    var balanceStr: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(balance?.balance ?? 0)/100
        let balanceNumber = NSNumber(value: b)
        var balanceStr = formatter.string(from: balanceNumber) ?? ""
        if balanceStr.count > 0 {
            balanceStr = "\(balanceStr) \(currency)"
        }
        return balanceStr
    }
    
    var saldo: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(balance?.saldo ?? 0)/100
        let balanceNumber = NSNumber(value: b)
        var balanceStr = formatter.string(from: balanceNumber) ?? ""
        if balanceStr.count > 0 {
            balanceStr = "\(balanceStr) \(currency)"
        }
        return balanceStr
    }
    
    init(_ account: AccountResponse, _ balance: AccountBalanceResponse?) {
        self.account = account
        self.balance = balance
    }
}
