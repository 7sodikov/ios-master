//
//  FilterCollectionViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell, FilterCollectionViewCellInstaller {
    var backView: UIView!
    var titleLabel: UILabel!
    var lineView: UIView!
    var separatorView: UIView!
    var mainView: UIView { contentView }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                lineView.backgroundColor = .red
                titleLabel.textColor = .dashDarkBlue
            } else {
                lineView.backgroundColor = ColorConstants.hintGray
                titleLabel.textColor = ColorConstants.hintGray
            }
        }
    }
    
    func setup(title: String) -> FilterCollectionViewCell {
        titleLabel.text = title
        return self
    }
}
