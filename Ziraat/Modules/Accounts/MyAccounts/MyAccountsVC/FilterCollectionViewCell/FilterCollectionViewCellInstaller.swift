//
//  FilterCollectionViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/21/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol FilterCollectionViewCellInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var titleLabel: UILabel! { get set }
    var lineView: UIView! { get set }
    var separatorView: UIView! { get set }
}

extension FilterCollectionViewCellInstaller {
    func initSubviews() {
        backView = UIView()
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        titleLabel.textColor = ColorConstants.hintGray
        
        lineView = UIView()
        lineView.backgroundColor = ColorConstants.hintGray
        
        separatorView = UIView()
        separatorView.backgroundColor = .clear
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(titleLabel)
        mainView.addSubview(lineView)
        mainView.addSubview(separatorView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.top.leading.trailing.equalToSuperview()
            maker.height.equalTo(Adaptive.val(39))
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.center.equalTo(backView)
        }
        
        lineView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.bottom)
            maker.height.equalTo(Adaptive.val(1))
            maker.leading.trailing.equalTo(backView)
        }
        
        separatorView.snp.remakeConstraints { (maker) in
            maker.top.bottom.equalTo(backView)
            maker.width.equalTo(Adaptive.val(1))
            maker.trailing.equalTo(backView)
        }
    }
}

