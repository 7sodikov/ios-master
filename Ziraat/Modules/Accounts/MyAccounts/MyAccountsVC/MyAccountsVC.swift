//
//  MyAccountsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MyAccountsVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }
    func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool)
    func reloadViewForBalanceDownload()
    func reloadSegmentedController(reload: Bool)
}

class MyAccountsVC: BaseViewController, MyAccountsViewInstaller {
    var mainView: UIView { view }
    var reloadButtonView: ReloadButtonView!
    var backImageView: UIImageView!
    var titleLabel: UILabel!
    var notificationButton: UIButton!
    var homeButton: UIButton!
    var lineView: UIView!
    var collectionLayout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var tableView: AutomaticHeightTableView!
    var warningMessageView: NoItemsAvailableView!
    
    var selectedIndex: Int = 0
    var filtering: FilterType = .All
    
    fileprivate let cellid = "\(MyAccountsTableViewCell.self)"

    var presenter: MyAccountsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        
        presenter.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        homeButton.addTarget(self, action: #selector(self.openDashboard), for: .touchUpInside)
        notificationButton.addTarget(self, action: #selector(self.openNotifications), for: .touchUpInside)
    }
    
    private func setupData() {
        
    }
    
    private func setupNavigation() {
//        self.navigationItem.titleView = titleLabel
        self.title = RS.nav_ttl__my_accounts.localized()
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: homeButton), UIBarButtonItem(customView: notificationButton)]
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func openNotifications() {
        presenter.notificationButtonClicked()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension MyAccountsVC: MyAccountsVCProtocol, UITableViewDelegate, UITableViewDataSource {
    func reloadSegmentedController(reload: Bool) {
        if reload == true {
            collectionView.reloadData()
        }
        
        let indexPath = collectionView.indexPathsForSelectedItems?.last ?? IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
    }
    
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
    func showReloadButtonForCardsDownload(_ show: Bool, animateReloadButton: Bool) {
        reloadButtonView.isHidden = !show
        warningMessageView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        collectionView.isHidden = show
        
        tableView.reloadData()
    }
    
    func reloadViewForBalanceDownload() {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filtering == .All {
            return presenter.viewModel.accounts.count
        } else if filtering == .UZS {
            return presenter.viewModel.uzsAccounts.count
        } else if filtering == .USD {
            return presenter.viewModel.usdAccounts.count
        } else if filtering == .EUR {
            return presenter.viewModel.eurAccounts.count
        } else if filtering == .GBP {
            return presenter.viewModel.gbpAccounts.count
        } else if filtering == .RUB {
            return presenter.viewModel.rubAccounts.count
        } else {
            return presenter.viewModel.tryAccounts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! MyAccountsTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if filtering == .All {
            let accountVM = presenter.viewModel.accountVM(at: indexPath.row)
            return cell.setup(with: accountVM)
        } else if filtering == .UZS {
            let accountVM = presenter.viewModel.uzsAccountVM(at: indexPath.row)
            return cell.setup(with: accountVM)
        } else if filtering == .USD {
            let accountVM = presenter.viewModel.usdAccountVM(at: indexPath.row)
            return cell.setup(with: accountVM)
        } else if filtering == .EUR {
            let accountVM = presenter.viewModel.eurAccountVM(at: indexPath.row)
            return cell.setup(with: accountVM)
        } else if filtering == .GBP {
            let accountVM = presenter.viewModel.gbpAccountVM(at: indexPath.row)
            return cell.setup(with: accountVM)
        } else if filtering == .RUB {
            let accountVM = presenter.viewModel.rubAccountVM(at: indexPath.row)
            return cell.setup(with: accountVM)
        } else {
            let accountVM = presenter.viewModel.tryAccountVM(at: indexPath.row)
            return cell.setup(with: accountVM)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filtering == .All {
            presenter.tableViewDidSelectItem(at: indexPath.row, filterIndex: 0)
        } else if filtering == .UZS {
            presenter.tableViewDidSelectItem(at: indexPath.row, filterIndex: 1)
        } else if filtering == .USD {
            presenter.tableViewDidSelectItem(at: indexPath.row, filterIndex: 2)
        } else if filtering == .EUR {
            presenter.tableViewDidSelectItem(at: indexPath.row, filterIndex: 3)
        } else if filtering == .GBP {
            presenter.tableViewDidSelectItem(at: indexPath.row, filterIndex: 4)
        } else if filtering == .RUB {
            presenter.tableViewDidSelectItem(at: indexPath.row, filterIndex: 5)
        } else {
            presenter.tableViewDidSelectItem(at: indexPath.row, filterIndex: 6)
        }
    }
}

extension MyAccountsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.viewModel.filterModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(FilterCollectionViewCell.self)", for: indexPath) as! FilterCollectionViewCell
        let category = presenter.viewModel.filterModel[indexPath.row]
        return cell.setup(title: category.title)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            filtering = presenter.viewModel.filterModel[0]
        } else if indexPath.item == 1 {
            filtering = presenter.viewModel.filterModel[1]
        } else if indexPath.item == 2 {
            filtering = presenter.viewModel.filterModel[2]
        } else if indexPath.item == 3 {
            filtering = presenter.viewModel.filterModel[3]
        } else if indexPath.item == 4 {
            filtering = presenter.viewModel.filterModel[4]
        } else if indexPath.item == 5 {
            filtering = presenter.viewModel.filterModel[5]
        } else {
            filtering = presenter.viewModel.filterModel[6]
        }
        tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Adaptive.val(90), height: Adaptive.val(41))
    }
}
