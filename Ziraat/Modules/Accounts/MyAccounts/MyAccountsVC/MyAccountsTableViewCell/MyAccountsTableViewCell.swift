//
//  MyAccountsTableViewCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class MyAccountsTableViewCell: UITableViewCell, MyAccountsTableViewCellInstaller {
    var backView: UIView!
    var accountNameLabel: UILabel!
    var accountNumberLabel: UILabel!
//    var availableSourcesLabel: UILabel!
    var accountNumberValueLabel: UILabel!
//    var availableSourcesValueLabel: UILabel!
    var balanceLabel: UILabel!
    var balanceValueLabel: UILabel!
    var arrImageView: UIImageView!
    var numberStackView: UIStackView!
//    var sourceStackView: UIStackView!
    var balanceStackView: UIStackView!
    var mainStackView: UIStackView!
    
    var mainView: UIView { contentView }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(with accountVM: MyAccountVM) -> MyAccountsTableViewCell {
        accountNameLabel.text = accountVM.account.customerName
        accountNumberValueLabel.text = accountVM.account.number
//        availableSourcesValueLabel.text = accountVM.balanceStr
        balanceValueLabel.text = accountVM.balanceStr
        return self
    }
}
