//
//  MyAccountsTableViewCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol MyAccountsTableViewCellInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var accountNameLabel: UILabel! { get set }
    var accountNumberLabel: UILabel! { get set }
    var accountNumberValueLabel: UILabel! { get set }
//    var availableSourcesLabel: UILabel! { get set }
//    var availableSourcesValueLabel: UILabel! { get set }
    var balanceLabel: UILabel! { get set }
    var balanceValueLabel: UILabel! { get set }
    var arrImageView: UIImageView! { get set }
    var numberStackView: UIStackView! { get set }
//    var sourceStackView: UIStackView! { get set }
    var balanceStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
}

extension MyAccountsTableViewCellInstaller {
    func initSubviews() {
        backView = UIView()
        backView.layer.cornerRadius = Adaptive.val(6)
        backView.backgroundColor = .white
        backView.layer.borderWidth = Adaptive.val(2)
        backView.layer.borderColor = UIColor.darkGrey.cgColor
        
        accountNameLabel = UILabel()
        accountNameLabel.font = .gothamNarrow(size: 18, .medium)
        accountNameLabel.textColor = .dashDarkBlue
        accountNameLabel.numberOfLines = 0
        
        accountNumberLabel = UILabel()
        accountNumberLabel.text = RS.lbl_account_number2.localized()
        accountNumberLabel.font = .gothamNarrow(size: 15, .book)
        accountNumberLabel.textColor = .dashDarkBlue
        
        accountNumberValueLabel = UILabel()
        accountNumberValueLabel.font = .gothamNarrow(size: 15, .book)
        accountNumberValueLabel.textColor = .dashDarkBlue
        
//        availableSourcesLabel = UILabel()
//        availableSourcesLabel.text = RS.lbl_available_amount.localized()
//        availableSourcesLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(15))
//        availableSourcesLabel.textColor = .black
//
//        availableSourcesValueLabel = UILabel()
//        availableSourcesValueLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(15))
//        availableSourcesValueLabel.textColor = ColorConstants.borderLine
        
        balanceLabel = UILabel()
        balanceLabel.text = RS.lbl_balance.localized()
        balanceLabel.font = .gothamNarrow(size: 15, .book)
        balanceLabel.textColor = .dashDarkBlue
        
        balanceValueLabel = UILabel()
        balanceValueLabel.font = .gothamNarrow(size: 15, .medium)
        balanceValueLabel.textColor = .dashDarkBlue
        
        arrImageView = UIImageView()
        arrImageView.image = UIImage(named: "img_icon_arr")
        
        numberStackView = UIStackView()
        numberStackView.axis = .horizontal
        numberStackView.alignment = .leading
        numberStackView.spacing = Adaptive.val(2)
        
//        sourceStackView = UIStackView()
//        sourceStackView.axis = .horizontal
//        sourceStackView.alignment = .leading
//        sourceStackView.spacing = Adaptive.val(2)
        
        balanceStackView = UIStackView()
        balanceStackView.axis = .horizontal
        balanceStackView.alignment = .leading
        balanceStackView.spacing = Adaptive.val(2)
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.alignment = .leading
        mainStackView.distribution = .fillEqually
        mainStackView.spacing = Adaptive.val(3)
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(accountNameLabel)
        numberStackView.addArrangedSubview(accountNumberLabel)
        numberStackView.addArrangedSubview(accountNumberValueLabel)
//        sourceStackView.addArrangedSubview(availableSourcesLabel)
//        sourceStackView.addArrangedSubview(availableSourcesValueLabel)
        balanceStackView.addArrangedSubview(balanceLabel)
        balanceStackView.addArrangedSubview(balanceValueLabel)
        mainView.addSubview(arrImageView)
        mainStackView.addArrangedSubview(numberStackView)
//        mainStackView.addArrangedSubview(sourceStackView)
        mainStackView.addArrangedSubview(balanceStackView)
        mainView.addSubview(mainStackView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(7.5)
            maker.bottom.equalToSuperview().offset(-7.5)
            maker.leading.equalToSuperview().offset(16)
            maker.trailing.equalToSuperview().offset(-16)
        }
        
        accountNameLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(13))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(14))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(14))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(accountNameLabel.snp.bottom).offset(Adaptive.val(11))
            maker.leading.equalTo(backView.snp.leading).offset(Size.leadingTrailing)
            maker.trailing.equalTo(arrImageView.snp.leading).offset(-Adaptive.val(5))
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(21))
        }

        arrImageView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(21))
            maker.width.equalTo(Adaptive.val(12))
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(45))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.leadingTrailing)
        }
    }
}

fileprivate struct Size {
    static let paddingTop = Adaptive.val(7.5)
    static let paddingLeft = Adaptive.val(16)
    static let height = Adaptive.val(106)
    static let leadingTrailing = Adaptive.val(14)
    static let topBottom = Adaptive.val(20)
}
