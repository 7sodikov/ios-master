//
//  MyAccountsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTabs_TabBarView

protocol MyAccountsViewInstaller: ViewInstaller {
    var reloadButtonView: ReloadButtonView! { get set }
    var backImageView: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
    var notificationButton: UIButton! { get set }
    var homeButton: UIButton! { get set }
    var lineView: UIView! { get set }
    var collectionLayout: UICollectionViewFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
    var tableView: AutomaticHeightTableView! { get set }
    var warningMessageView: NoItemsAvailableView! { get set }
}

extension MyAccountsViewInstaller {
    func initSubviews() {
        reloadButtonView = ReloadButtonView(frame: .zero)
        reloadButtonView.isHidden = true
        
        warningMessageView = NoItemsAvailableView.setupText(title: RS.lbl_no_account.localized())
        warningMessageView.isHidden = true
        
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_dash_light_background")
        
        titleLabel = UILabel()
        titleLabel.text = RS.nav_ttl__my_accounts.localized()
        titleLabel.font = .gothamNarrow(size: 16, .bold)
        titleLabel.textColor = .dashDarkBlue
        
        notificationButton = UIButton()
        notificationButton.setBackgroundImage(UIImage(named: "icon_dark_bell"), for: .normal)
        
        homeButton = UIButton()
        homeButton.setBackgroundImage(UIImage(named: "img_icon_home"), for: .normal)
        
        lineView = UIView()
        lineView.backgroundColor = ColorConstants.hintGray
        
        collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.sectionInset = .zero
        collectionLayout.scrollDirection = .horizontal
        collectionLayout.minimumInteritemSpacing = 0
        collectionLayout.minimumLineSpacing = 0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = false
        collectionView.backgroundColor = .clear
        collectionView.register(FilterCollectionViewCell.self,
                                forCellWithReuseIdentifier: String(describing: FilterCollectionViewCell.self))
        
        tableView = AutomaticHeightTableView()
        tableView.register(MyAccountsTableViewCell.self, forCellReuseIdentifier: String(describing: MyAccountsTableViewCell.self))
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        tableView.sizeToFit()
    }
    
    func embedSubviews() {
        mainView.addSubview(reloadButtonView)
        mainView.addSubview(warningMessageView)
        mainView.addSubview(backImageView)
        mainView.addSubview(lineView)
        mainView.addSubview(collectionView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        reloadButtonView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
            maker.center.equalToSuperview()
        }
        
        backImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
                
//        accountTypesView.snp.makeConstraints { (maker) in
//            maker.top.equalToSuperview()//.offset(Adaptive.val(110))
//            maker.leading.trailing.equalToSuperview()
//        }
//
//        tableView.snp.remakeConstraints { (maker) in
//            maker.top.equalToSuperview().offset(Adaptive.val(150))
//            maker.trailing.leading.bottom.equalTo(backImageView)
//
        
//        accountTypesView.snp.remakeConstraints { (maker) in
//            maker.top.equalToSuperview().offset(Adaptive.val(110))
//            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
//            maker.width.equalTo(Adaptive.val(800))
////            maker.trailing.equalToSuperview().offset(-Size.paddingLeftRight)
//        }
        
        lineView.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(collectionView.snp.bottom).offset(-Adaptive.val(1))
            maker.leading.equalToSuperview()//.offset(Adaptive.val(15))
            maker.trailing.equalToSuperview()//.offset(-Adaptive.val(15))
            maker.height.equalTo(Adaptive.val(1))
        }
        
        collectionView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(110))
            maker.leading.equalToSuperview()//.offset(Adaptive.val(15))
            maker.trailing.equalToSuperview()//.offset(-Adaptive.val(15))
            maker.height.equalTo(Adaptive.val(41))
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(collectionView.snp.bottom).offset(Adaptive.val(10))
            maker.trailing.leading.bottom.equalTo(backImageView)
        }
        
        warningMessageView.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(214))
            make.centerX.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(22)
}

