//
//  TransactionDetailsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol TransactionDetailsRouterProtocol: class {
    static func createModule(statement: AccountStatementResponse) -> UIViewController
    func navigatetoSendEmailVC(in navCtrl: Any)
    static func createModuleInNavController(statement: AccountStatementResponse) -> UIViewController
}

class TransactionDetailsRouter: TransactionDetailsRouterProtocol {
    static func createModule(statement: AccountStatementResponse) -> UIViewController {
        let vc = TransactionDetailsVC()
        let presenter = TransactionDetailsPresenter()
        let interactor = TransactionDetailsInteractor()
        let router = TransactionDetailsRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        presenter.viewModel.statementDetails = statement
        return vc
    }
    
    static func createModuleInNavController(statement: AccountStatementResponse) -> UIViewController {
        let navCtrl = NavigationController(rootViewController: createModule(statement: statement))
        return navCtrl
    }
    
    func navigatetoSendEmailVC(in navCtrl: Any) {        
        let viewCtrl = navCtrl as! UIViewController
        let statementVC = ErrorMessageRouter.createModule(message: RS.al_msg_in_process.localized())
        statementVC.modalPresentationStyle = .overFullScreen
        viewCtrl.present(statementVC, animated: false, completion: nil)
        
    }
}
