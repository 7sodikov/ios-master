//
//  TransactionDetailsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol TransactionDetailsInteractorProtocol: class {
    
}

class TransactionDetailsInteractor: TransactionDetailsInteractorProtocol {
    weak var presenter: TransactionDetailsInteractorToPresenterProtocol!
    
}
