//
//  TransactionDetailsVM.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class TransactionDetailsVM {
    var statementDetails: AccountStatementResponse!
    
    var remainingBalance: String? {
        return formattedMoney(statementDetails.sout)
    }
    
    var transDate: String? {
        let date = statementDetails.vdate
        let formattedDate = date?.replacingOccurrences(of: "-", with: "/")
        return formattedDate
    }
    
    var explanation: String? {
        return statementDetails.purpose
    }
    
    var amountSdt: String? {
        return formattedMoney(statementDetails.sdt)
    }
    
    var amountSct: String? {
        return formattedMoney(statementDetails.sct)
    }
    
    private func formattedMoney(_ money: Int?) -> String? {
        guard let money = money else { return "-" }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        let b = Double(money)/100
        let balanceNumber = NSNumber(value: b)
        return formatter.string(from: balanceNumber)
    }
}
