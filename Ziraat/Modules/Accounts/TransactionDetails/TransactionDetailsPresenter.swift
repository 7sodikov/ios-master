//
//  TransactionDetailsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol TransactionDetailsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: TransactionDetailsVM { get }
    func receiptButtonClicked(with navigateCtrl: Any)
}

protocol TransactionDetailsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class TransactionDetailsPresenter: TransactionDetailsPresenterProtocol {
    weak var view: TransactionDetailsVCProtocol!
    var interactor: TransactionDetailsInteractorProtocol!
    var router: TransactionDetailsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = TransactionDetailsVM()
    
    
    // Private property and methods
    
    func receiptButtonClicked(with navigateCtrl: Any) {
        router.navigatetoSendEmailVC(in: navigateCtrl)
    }
}

extension TransactionDetailsPresenter: TransactionDetailsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
