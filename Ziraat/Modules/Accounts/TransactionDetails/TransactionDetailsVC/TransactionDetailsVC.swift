//
//  TransactionDetailsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol TransactionDetailsVCProtocol: class {
    var statement: AccountStatementResponse! { get }
    var navigationCtrl: Any { get }
}

class TransactionDetailsVC: UIViewController, TransactionDetailsViewInstaller {
    var receiptButton: NextButton!
    var backButton: NextButton!
    var tranDateStackView: UIStackView!
    var amountStackView: UIStackView!
    var remainingBalanceStackView: UIStackView!
    var explanationStackView: UIStackView!
    var mainStackView: UIStackView!
    var backView: UIView!
    var tranDetailLabel: UILabel!
    var tranDateLabel: UILabel!
    var tranDateValueLabel: UILabel!
    var amountLabel: UILabel!
    var amountValueLabel: UILabel!
    var remainingBalanceLabel: UILabel!
    var remainingBalanceValueLabel: UILabel!
    var explanationLabel: UILabel!
    var explanationValueLabel: UILabel!
    var mainView: UIView { view }
    
    var statementVC: AccountStatementResponse!
        
    var presenter: TransactionDetailsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        setup()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        receiptButton.addTarget(self, action: #selector(self.sendEmailButtonPressed(_:)), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(self.backButtonPressed(_:)), for: .touchUpInside)
    }
    
    private func setup() {
        let transactionDetailsVM = presenter.viewModel
        tranDateValueLabel.text = transactionDetailsVM.transDate
        if transactionDetailsVM.statementDetails.sdt == 0 {
            amountValueLabel.text = transactionDetailsVM.amountSct
            amountValueLabel.textColor = ColorConstants.dashboardGreen
        } else {
            amountValueLabel.textColor = ColorConstants.red
            amountValueLabel.text = transactionDetailsVM.amountSdt
        }
        remainingBalanceValueLabel.text = transactionDetailsVM.remainingBalance
        explanationValueLabel.text = transactionDetailsVM.explanation
    }
    
    @objc func sendEmailButtonPressed(_ sender: UIButton) {
        presenter.receiptButtonClicked(with: navigationCtrl)
    }
    
    @objc func backButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? NavigationController)?.background(isWhite: false)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension TransactionDetailsVC: TransactionDetailsVCProtocol {
    var statement: AccountStatementResponse! {
        return statementVC
    }
    
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
}
