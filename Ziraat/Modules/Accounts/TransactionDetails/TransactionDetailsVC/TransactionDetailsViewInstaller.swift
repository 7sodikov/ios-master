//
//  TransactionDetailsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol TransactionDetailsViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var tranDetailLabel: UILabel! { get set }
    var tranDateLabel: UILabel! { get set }
    var tranDateValueLabel: UILabel! { get set }
    var tranDateStackView: UIStackView! { get set }
    var amountLabel: UILabel! { get set }
    var amountValueLabel: UILabel! { get set }
    var amountStackView: UIStackView! { get set }
    var remainingBalanceLabel: UILabel! { get set }
    var remainingBalanceValueLabel: UILabel! { get set }
    var remainingBalanceStackView: UIStackView! { get set }
    var explanationLabel: UILabel! { get set }
    var explanationValueLabel: UILabel! { get set }
    var explanationStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
    var receiptButton: NextButton! { get set }
    var backButton: NextButton! { get set }
}

extension TransactionDetailsViewInstaller {
    func initSubviews() {
        backView = UIView()
        backView.layer.cornerRadius = Adaptive.val(13)
        backView.backgroundColor = .white
        
        tranDetailLabel = UILabel()
        tranDetailLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        tranDetailLabel.textColor = .black
        tranDetailLabel.text = RS.al_ttl_transaction_detail.localized()
        
        tranDateLabel = UILabel()
        tranDateLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        tranDateLabel.textColor = .black
        tranDateLabel.text = RS.lbl_transaction_date.localized()
        
        tranDateValueLabel = UILabel()
        tranDateValueLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        tranDateValueLabel.textColor = .black
        tranDateValueLabel.textAlignment = .right
        
        tranDateStackView = UIStackView()
        tranDateStackView.axis = .horizontal
        tranDateStackView.alignment = .fill
        tranDateStackView.distribution = .fill
        
        amountLabel = UILabel()
        amountLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        amountLabel.textColor = .black
        amountLabel.text = RS.lbl_amount.localized()
            
        amountValueLabel = UILabel()
        amountValueLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        amountValueLabel.textColor = .black
        amountValueLabel.textAlignment = .right
        
        amountStackView = UIStackView()
        amountStackView.axis = .horizontal
        amountStackView.alignment = .fill
        amountStackView.distribution = .fill
        
        remainingBalanceLabel = UILabel()
        remainingBalanceLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        remainingBalanceLabel.textColor = .black
        remainingBalanceLabel.text = RS.lbl_remaining.localized()
        
        remainingBalanceValueLabel = UILabel()
        remainingBalanceValueLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        remainingBalanceValueLabel.textColor = .black
        remainingBalanceValueLabel.textAlignment = .right
        
        remainingBalanceStackView = UIStackView()
        remainingBalanceStackView.axis = .horizontal
        remainingBalanceStackView.alignment = .fill
        remainingBalanceStackView.distribution = .fill
        
        explanationLabel = UILabel()
        explanationLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        explanationLabel.textColor = .black
        explanationLabel.text = RS.lbl_details.localized()
        
        explanationValueLabel = UILabel()
        explanationValueLabel.font = EZFontType.medium.sfuiText(size: Adaptive.val(14))
        explanationValueLabel.textColor = .black
        explanationValueLabel.numberOfLines = 0
        explanationValueLabel.textAlignment = .left
        
        explanationStackView = UIStackView()
        explanationStackView.axis = .horizontal
        explanationStackView.alignment = .top
        explanationStackView.distribution = .fill
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.alignment = .fill
        mainStackView.distribution = .fill
        mainStackView.spacing = Adaptive.val(8)
        
        receiptButton = NextButton.systemButton(with: RS.btn_receipt.localized(), cornerRadius: Adaptive.val(19))
        receiptButton.setBackgroundColor(ColorConstants.red, for: .normal)
        receiptButton.titleLabel?.font = EZFontType.medium.sfuiText(size: Adaptive.val(12))
        
        backButton = NextButton.systemButton(with: RS.btn_back.localized(), cornerRadius: Adaptive.val(19))
        backButton.setBackgroundColor(.white, for: .normal)
        backButton.titleLabel?.font = EZFontType.medium.sfuiText(size: Adaptive.val(12))
        backButton.setTitleColor(.black, for: .normal)
        backButton.layer.borderWidth = Adaptive.val(1)
        backButton.layer.borderColor = ColorConstants.borderLine.cgColor
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(tranDetailLabel)
        tranDateStackView.addArrangedSubview(tranDateLabel)
        tranDateStackView.addArrangedSubview(tranDateValueLabel)
        mainStackView.addArrangedSubview(tranDateStackView)
        amountStackView.addArrangedSubview(amountLabel)
        amountStackView.addArrangedSubview(amountValueLabel)
        mainStackView.addArrangedSubview(amountStackView)
        remainingBalanceStackView.addArrangedSubview(remainingBalanceLabel)
        remainingBalanceStackView.addArrangedSubview(remainingBalanceValueLabel)
        mainStackView.addArrangedSubview(remainingBalanceStackView)
        explanationStackView.addArrangedSubview(explanationLabel)
        mainView.addSubview(explanationValueLabel)
        mainStackView.addArrangedSubview(explanationStackView)
        mainView.addSubview(mainStackView)
        mainView.addSubview(receiptButton)
        mainView.addSubview(backButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(Adaptive.val(32))
            maker.trailing.equalTo(-Adaptive.val(32))
            maker.top.equalToSuperview().offset(Adaptive.val(195))
            maker.bottom.equalTo(backButton.snp.bottom).offset(Adaptive.val(22))
            maker.centerX.equalToSuperview()
        }
        
        tranDetailLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(12))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(24))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(tranDetailLabel.snp.top).offset(Adaptive.val(31))
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.paddingLeftRight)
        }
        
        explanationValueLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(mainStackView.snp.bottom).offset(Adaptive.val(12))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(24))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(24))
        }
        
        receiptButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(explanationValueLabel.snp.bottom).offset(Adaptive.val(33))
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.paddingLeftRight)
            maker.height.equalTo(Size.buttonHeight)
        }
        
        backButton.snp.remakeConstraints { (maker) in
            maker.top.equalTo(receiptButton.snp.bottom).offset(Adaptive.val(5))
            maker.leading.equalTo(backView.snp.leading).offset(Size.paddingLeftRight)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Size.paddingLeftRight)
            maker.height.equalTo(Size.buttonHeight)
        }
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(24)
    static let buttonHeight = Adaptive.val(38)
}
