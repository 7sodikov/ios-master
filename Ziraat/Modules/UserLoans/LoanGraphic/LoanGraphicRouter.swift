//
//  LoanGraphicRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanGraphicRouterProtocol: class {
    static func createModule(_ loan:LoanResponse?) -> UIViewController
}

class LoanGraphicRouter: LoanGraphicRouterProtocol {
    static func createModule(_ loan:LoanResponse?) -> UIViewController {
        let vc = LoanGraphicVC()
        let presenter = LoanGraphicPresenter()
        
        let interactor = LoanGraphicInteractor()
        let router = LoanGraphicRouter()
        vc.presenter = presenter
        vc.presenter.viewModel.loan = loan
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        return vc
    }
    
}
