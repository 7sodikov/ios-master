//
//  LoanGraphicVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanGraphicVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class LoanGraphicVC: BaseViewController, LoanGraphicViewInstaller {
    var bgView: UIImageView!
    
    var tableView: UITableView!
    
    var reloadButtonView: ReloadButtonView!
    
    var mainView: UIView { view }
    var presenter: LoanGraphicPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = RS.btn_payoff.localized()
        setupSubviews()
        tableView.delegate =  self
        tableView.dataSource = self
        refresh()
    }
    
    func refresh(){
        self.presenter.getGraphic()
    }
    
    @objc func showOrHideCell(_ sender:UIButton){
        let bool = presenter.viewModel.showItems[sender.tag]
        presenter.viewModel.showItems[sender.tag] = !bool
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
    }
}

extension LoanGraphicVC: LoanGraphicVCProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool){
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
        if !show{
            let index = presenter.viewModel.graphicItems?.firstIndex{$0.state == 1}
            if index != nil {
                tableView.scrollToRow(at: IndexPath(row: index!, section: 0), at: .middle, animated: true)
            }

        }
    }
    
}
extension LoanGraphicVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.graphicItems?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "GraphicTableViewCell") as! GraphicTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(showOrHideCell(_:)), for: .touchUpInside)
        cell.showOrHide(presenter.viewModel.showItems[indexPath.row])
        cell.setupGraphic(presenter.viewModel.graphicItems?[indexPath.row])

        return cell
    }
    
}
