//
//  LoanGraphicViewInstaller.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanGraphicViewInstaller: ViewInstaller {
    // declare your UI elements here
    var bgView : UIImageView! {get set}
    var tableView : UITableView! {get set}
    var reloadButtonView : ReloadButtonView! {get set}
}

extension LoanGraphicViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        
        tableView = UITableView(frame: .zero)
        tableView.backgroundColor = .clear
        tableView.backgroundColor = .clear
        tableView.separatorInset = .zero
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.register(GraphicTableViewCell.self, forCellReuseIdentifier: "GraphicTableViewCell")
        
        reloadButtonView = ReloadButtonView(frame: .zero)
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(tableView)
        mainView.addSubview(reloadButtonView)
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        bgView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(85)
        }
        reloadButtonView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
        }
        
        
    }
}

fileprivate struct Size {
    
}
