//
//  LoanGraphicPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanGraphicPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoanGraphicVM { get }
    func getGraphic()
}

protocol LoanGraphicInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func getGraphic(with response: ResponseResult<LoanGraphsResponse, AppError>)
    
}

class LoanGraphicPresenter: LoanGraphicPresenterProtocol {
    weak var view: LoanGraphicVCProtocol!
    var interactor: LoanGraphicInteractorProtocol!
    var router: LoanGraphicRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoanGraphicVM()
    
    // Private property and methods
    func getGraphic() {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        self.interactor.getGraphic(self.viewModel.loan?.branch ?? "", self.viewModel.loan?.id ?? 0)
    }
    
}

extension LoanGraphicPresenter: LoanGraphicInteractorToPresenterProtocol {
    func getGraphic(with response: ResponseResult<LoanGraphsResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.graphicItems = result.data.graph
            view.showReloadButtonForResponses(false, animateReloadButton: false)
        case let .failure(error):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
        }
    }
}
