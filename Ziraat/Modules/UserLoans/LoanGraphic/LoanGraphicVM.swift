//
//  LoanGraphicVM.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class LoanGraphicVM {
    var loan:LoanResponse?
    var showItems = [Bool]()
    var graphicItems : [LoanGraphResponse]?{
        didSet{
            let count = graphicItems?.count ?? 0
            if count > 0{
                for _ in 0...count{
                    showItems.append(false)
                }
            } else{
                showItems.removeAll()
            }
            
        }
    }
}
