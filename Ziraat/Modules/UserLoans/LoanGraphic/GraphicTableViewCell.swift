//// Header

import UIKit

class GraphicTableViewCell: ExpandableTableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        if  let subView = expandableView.arrangedSubviews[4] as? HorizontalLabelsView{
            expandableView.removeArrangedSubview(subView)
        }
        if  let subView = expandableView.arrangedSubviews[3] as? HorizontalLabelsView{
            expandableView.removeArrangedSubview(subView)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupGraphic(_ graphic: LoanGraphResponse?){
        if let item = graphic{
            let amount : Double = (graphic?.payAmount ?? 0)/100
            let currency = CurrencyType.uzs.data.label
            let amountString = amount.currencyFormattedStr() ?? ""
            leftLabel.text = item.payDate
            rightLabel.text = amountString + " " + currency
            rightLabel.textColor = .black
            leftLabel.textColor = .black
            
            if item.state == 1{
                rightLabel.textColor = ColorConstants.dashboardGreen
                leftLabel.textColor = ColorConstants.dashboardGreen
            }
            if item.state == 0{
                rightLabel.textColor = ColorConstants.lightGray
                leftLabel.textColor = ColorConstants.lightGray
            }
            
             
            if  let subView = expandableView.arrangedSubviews[0] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_main_debt.localized()
                let amountString = (item.principalAmount/100).currencyFormattedStr() ?? ""
                subView.rightLabel.text = amountString + " " + currency
            }
            if  let subView = expandableView.arrangedSubviews[1] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_percent.localized()
                let amountString = (item.percentAmount/100).currencyFormattedStr() ?? ""
                subView.rightLabel.text = amountString + " " + currency
            }
            if  let subView = expandableView.arrangedSubviews[2] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_loan_remainder.localized()
                let amountString = (item.balance/100).currencyFormattedStr() ?? ""
                subView.rightLabel.text = amountString + " " + currency
            }
            
        }
        
    }
}
