//
//  LoanGraphicInteractor.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanGraphicInteractorProtocol: class {
    func getGraphic(_ branch:String, _ id:Int)
}

class LoanGraphicInteractor: LoanGraphicInteractorProtocol {
    weak var presenter: LoanGraphicInteractorToPresenterProtocol!
    
    func getGraphic(_ branch: String, _ id: Int) {
        NetworkService.Loan.graphic(loanId: UInt64(id), branch: branch, completion: { (result) in
            self.presenter.getGraphic(with: result)
        })
    }
}
