//
//  LoanDetailsRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/8/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanDetailsRouterProtocol: class {
    static func createModule(_ loan: LoanResponse?) -> UIViewController
    func openStatement(_ view:Any, loan:LoanResponse?)
    func openPay(_ view:Any, loan:LoanResponse?)
    func openGraphic(_ view:Any, loan:LoanResponse?)
}

class LoanDetailsRouter: LoanDetailsRouterProtocol {
    static func createModule(_ loan: LoanResponse?) -> UIViewController{
        let vc = LoanDetailsVC()
        let presenter = LoanDetailsPresenter()
        let interactor = LoanDetailsInteractor()
        let router = LoanDetailsRouter()
        presenter.viewModel.loan = loan
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    func openPay(_ view: Any, loan: LoanResponse?) {
        //fatalError()
        let controller = OperationsRouter.createModule(for: nil, historyItem: nil)
        let viewController = view as? UIViewController
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    func openGraphic(_ view: Any, loan: LoanResponse?) {
        let module = LoanGraphicRouter.createModule(loan)
        (view as? UIViewController)?.navigationController?.pushViewController(module, animated: true)
    }
    func openStatement(_ view: Any, loan: LoanResponse?) {
        let module = LoanStatementRouter.createModule(loan)
        (view as? UIViewController)?.navigationController?.pushViewController(module, animated: true)
    }
    
}
