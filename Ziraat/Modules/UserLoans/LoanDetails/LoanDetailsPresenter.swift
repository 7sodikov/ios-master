//
//  LoanDetailsPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/8/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanDetailsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoanDetailsVM { get }

    func btnClicked(_ tag:Int)
//    func accountActivitiesClicked()
//    func payClicked()
//    func graphicClicked()
    
}

protocol LoanDetailsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class LoanDetailsPresenter: LoanDetailsPresenterProtocol {
    weak var view: LoanDetailsVCProtocol!
    var interactor: LoanDetailsInteractorProtocol!
    var router: LoanDetailsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoanDetailsVM()

    
    func btnClicked(_ tag: Int){
        if tag == 1{
            router.openStatement(view as Any, loan: self.viewModel.loan)
        }
        if tag == 3{
            router.openPay(view as Any, loan: self.viewModel.loan)
        }
        if tag == 4{
            router.openGraphic(view as Any, loan: self.viewModel.loan)
        }
    }
        
    
}

extension LoanDetailsPresenter: LoanDetailsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
