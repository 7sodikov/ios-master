//
//  LoanDetailsVM.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/8/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct KeyValueItem {
    var key: String!
    var value : String!
    var title: String?
    var icon: String?
}

class LoanDetailsVM {
    var loan:LoanResponse?
    
    lazy var loanItems: [KeyValueItem] = {
        var result : [KeyValueItem] = []
        
        let amount = ((loan?.amount ?? 0)/100).currencyFormattedStr()
        result.append(KeyValueItem(key: RS.lbl_loan_amount.localized(), value: amount, title: nil, icon: nil))
        
        let remaining = ((loan?.overall?.maxAmount ?? 0)/100).currencyFormattedStr()
        result.append(KeyValueItem(key: RS.lbl_remaining_debt.localized(), value: remaining, title: nil, icon: nil))
        
        let debt = ((loan?.overall?.termDebtInterest ?? 0)/100).currencyFormattedStr()
        result.append(KeyValueItem(key: RS.lbl_monthly_payment.localized(), value: debt, title: nil, icon: nil))
        return result
    }()
    
    lazy var loanRatesItems: [KeyValueItem] = {
        var result : [KeyValueItem] = []
        
        for rate in loan?.rates ?? []{
            let value = "\(rate.rate) " + "%"
            result.append(KeyValueItem(key: rate.name, value: value, title: nil, icon: nil))
        }
        return result
    }()
    
}
