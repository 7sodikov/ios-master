//
//  LoanDetailsInteractor.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/8/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanDetailsInteractorProtocol: class {
    
}

class LoanDetailsInteractor: LoanDetailsInteractorProtocol {
    weak var presenter: LoanDetailsInteractorToPresenterProtocol!
    
}
