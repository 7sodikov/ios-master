//
//  LoanDetailsViewInstaller.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/8/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanDetailsViewInstaller: ViewInstaller {
    var bgView : UIImageView! {get set}
    var tableView : UITableView! {get set}
    
}

extension LoanDetailsViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.register(LoanItemsCell.self, forCellReuseIdentifier: "LoanItemsCell")
        tableView.register(BtnTableViewCell.self, forCellReuseIdentifier: "BtnTableViewCell")
        
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(tableView)
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        bgView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(Adaptive.val(64))
        }
    }
}

fileprivate struct Size {
    
}
