//
//  LoanDetailsVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/8/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanDetailsVCProtocol: BaseViewControllerProtocol {
    
}

class LoanDetailsVC: BaseViewController, LoanDetailsViewInstaller {
    var bgView: UIImageView!
    
    var tableView: UITableView!
    
    var mainView: UIView { view }
    var presenter: LoanDetailsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        setupNavigation()
        
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        
    }
    
    private func setupNavigation() {
        self.title = RS.nav_ttl_credit_details.localized()

        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func btnClicked(_ sender:UIButton){
        presenter.btnClicked(sender.tag)
    }
}

extension LoanDetailsVC: LoanDetailsVCProtocol {
    
}

extension LoanDetailsVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return presenter.viewModel.loanItems.count
        }
        if section == 1{
            return 1
        }
        if section == 2{
            return presenter.viewModel.loanRatesItems.count
        }
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BtnTableViewCell") as! BtnTableViewCell
            cell.selectionStyle = .none
            cell.btn.tag = indexPath.section
            cell.btn.addTarget(self, action: #selector(self.btnClicked(_:)), for: .touchUpInside)
            cell.btn.setTitle(RS.btn_credit_receipt.localized(), for: .normal)
            cell.btn.setTitleColor(.white, for: .normal)
            cell.backgroundColor = .clear
            cell.btn.backgroundColor  = ColorConstants.red
            return cell
        }
        if indexPath.section == 2 || indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoanItemsCell") as! LoanItemsCell
            cell.backgroundColor = .clear
            if indexPath.section == 0{
                cell.setup(presenter.viewModel.loanItems[indexPath.row])
            } else{
                cell.setup(presenter.viewModel.loanRatesItems[indexPath.row])
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BtnTableViewCell") as! BtnTableViewCell
        cell.selectionStyle = .none
        let string = indexPath.row == 0 ? RS.btn_pay_installment.localized() : RS.btn_payoff.localized()
        cell.btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        cell.btn.tag = indexPath.section + indexPath.row
        cell.btn.setTitle(string, for: .normal)
        cell.btn.setTitleColor(.white, for: .normal)
        cell.backgroundColor = .clear
        cell.btn.backgroundColor  = ColorConstants.red
        return cell
    }
}
