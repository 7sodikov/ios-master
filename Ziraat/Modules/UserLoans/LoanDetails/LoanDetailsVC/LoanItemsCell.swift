//// Header

import UIKit

class LoanItemsCell: UITableViewCell {
    let containerView = HorizontalLabelsView(frame: .zero)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(containerView)
        let padding = Adaptive.val(10)
        containerView.snp.makeConstraints { (make) in
            make.left.top.equalTo(padding)
            make.right.bottom.equalTo(-padding)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(_ item: KeyValueItem?){
        containerView.leftLabel.text = item?.key
        containerView.rightLabel.text = item?.value
    }
    

}
