//// Header

import UIKit

class ExpandableTableViewCell: UITableViewCell {

    let containerView = UIView()
    let expandableArrow = UIImageView()
    let leftLabel = UILabel()
    let rightLabel = UILabel()
    let infoContainerView = UIView()
    let btn = UIButton()
    let lineView = UIView()
    let expandableView = UIStackView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(containerView)
        containerView.addSubview(expandableArrow)
        containerView.addSubview(leftLabel)
        containerView.addSubview(rightLabel)
        containerView.addSubview(lineView)
        containerView.addSubview(btn)
        
        containerView.addSubview(expandableView)
        expandableView.axis = .vertical
        expandableView.distribution = .fillEqually
        expandableView.spacing = 1
        for k in 1...5{
            let view = HorizontalLabelsView()
            view.snp.makeConstraints { (make) in
                make.height.equalTo(30)
            }
            view.leftLabel.text = "LEFT"
            view.rightLabel.text = "RIGHT"
            view.tag = k
            expandableView.addArrangedSubview(view)
        }
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 6
        
        lineView.backgroundColor = .gray
        
        expandableArrow.image = UIImage(named: "btn_down")?.withRenderingMode(.alwaysTemplate)
        expandableArrow.tintColor = ColorConstants.gray
        expandableArrow.contentMode = .scaleAspectFit
        expandableArrow.clipsToBounds = true
        
        leftLabel.font = EZFontType.regular.sfProDisplay(size: 14)
        
        rightLabel.font = EZFontType.regular.sfuiDisplay(size: 14)
        rightLabel.textAlignment = .right
        
        btn.backgroundColor = .clear
        //btn.addTarget(self, action: #selector(btnClicked), for: .touchUpInside)
        
        
        let  left = 15
        containerView.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.top.equalToSuperview()
            make.bottom.equalTo(-left)
        }
        expandableArrow.snp.makeConstraints { (make) in
            make.width.equalTo(15)
            make.height.equalTo(10)
            make.centerY.equalTo(leftLabel)
            make.right.equalTo(-left)
        }
        leftLabel.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.height.equalTo(35)
            make.top.equalTo(0)
            make.width.equalToSuperview().multipliedBy(0.5)
        }
        rightLabel.snp.makeConstraints { (make) in
            make.right.equalTo(expandableArrow.snp.left).offset(-10)
            make.centerY.equalTo(leftLabel)
            make.width.equalToSuperview().multipliedBy(0.5)
        }
        lineView.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.height.equalTo(1)
            make.top.equalTo(leftLabel.snp.bottom)
        }
        expandableView.snp.makeConstraints { (make) in
            make.top.equalTo(lineView.snp.bottom)
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.bottom.equalTo(0)
        }
        btn.snp.makeConstraints { (make) in
            make.top.right.left.equalToSuperview()
            make.bottom.equalTo(lineView.snp.top)
        }
        showOrHide(false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showOrHide(_ show:Bool){
        for view in self.expandableView.arrangedSubviews{
            view.isHidden = !show
        }
        self.lineView.isHidden = !show
        self.expandableArrow.transform = show ? CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2)) :  CGAffineTransform(rotationAngle: 0)
    }
    
    func setupStatement(_ statement: LoanStatementResponse?){
        if let item = statement{
            
            var amount : Double = 0
            var plus = ""
            var color = ColorConstants.red
            
            if item.sdt > 0{
                plus = "-"
                amount = item.sdt
            } else{
                amount = item.sct
                color = ColorConstants.dashboardGreen
            }
            let currency = CurrencyType.uzs.data.label
            let amountString = (amount/100).currencyFormattedStr() ?? ""
            leftLabel.text = statement?.vdate
            rightLabel.text = plus + " " + amountString + " " + currency
            rightLabel.textColor = color
            if  let subView = expandableView.arrangedSubviews[0] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_mfo.localized()
                subView.rightLabel.text = item.bankCo
            }
            if  let subView = expandableView.arrangedSubviews[1] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_doc_type.localized()
                subView.rightLabel.text = statement?.typeDoc
            }
            if  let subView = expandableView.arrangedSubviews[2] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_doc_number.localized()
                subView.rightLabel.text = statement?.docNum
            }
            if  let subView = expandableView.arrangedSubviews[3] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_account.localized()
                subView.rightLabel.text = statement?.accCll
            }
            if  let subView = expandableView.arrangedSubviews[4] as? HorizontalLabelsView{
                subView.leftLabel.text = RS.lbl_remaining.localized()
                subView.rightLabel.text = amountString
            }
                    
        }
        
    }
    
}
