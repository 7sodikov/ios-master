//
//  LoansVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoansVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class LoansVC: BaseViewController, LoansViewInstaller {
    var warningMessageView: NoItemsAvailableView!
    var bgView: UIImageView!
    var tableView: UITableView!
    var reloadButtonView: ReloadButtonView!
    var mainView: UIView { view }
    
    var presenter: LoansPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupNavigation()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        
        getLoanList()
        
        reloadButtonView.reloadButton.addTarget(self, action: #selector(getLoanList), for: .touchUpInside)
    }
    
    private func setupNavigation() {
        self.title = RS.nav_ttl_my_credits.localized()
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "img_icon_home")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openDashboard))
        self.navigationItem.rightBarButtonItem = homeButton
    }
    
    @objc private func openDashboard() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func getLoanList(){
        presenter.getLoanList()
    }
}

extension LoansVC: LoansVCProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool) {
//        reloadButtonView.isHidden = !show
        warningMessageView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
}

extension LoansVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.loanItems?.loans?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoansListCell") as! LoansListCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        let loan = presenter.viewModel.loanItems?.loans?[indexPath.row]
        cell.setup(loan)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.tableViewCellSelected(indexPath)
    }
}
