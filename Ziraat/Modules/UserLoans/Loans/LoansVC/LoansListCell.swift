//// Header

import UIKit

class LoansListCell: UITableViewCell {

    let containerView = UIView()
    let nameLabel = UILabel()
    let openLabel = UILabel()
    let openValueLabel = UILabel()
    let amountLabel = UILabel()
    let amountValueLabel = UILabel()
    
    let stackView1 = UIStackView()
    let stackView2 = UIStackView()
    let progressView = UIProgressView()
    let percentLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    func setupView() {
        super.awakeFromNib()
        contentView.addSubview(containerView)
        
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 7
        containerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalTo(Adaptive.val(15))
            make.right.equalTo(-Adaptive.val(15))
            make.bottom.equalTo(-Adaptive.val(10))
        }
        
        containerView.addSubview(nameLabel)
        nameLabel.font = EZFontType.bold.sfuiText(size: 14)
        nameLabel.textColor = .black
        nameLabel.numberOfLines = 3
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(10))
            make.left.equalTo(Adaptive.val(10))
            make.right.equalTo(-Adaptive.val(10))
        }
        
        stackView1.distribution = .fill
        stackView1.axis = .horizontal
        stackView1.spacing = Adaptive.val(5)
        containerView.addSubview(stackView1)
        stackView1.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(5)
            make.left.equalTo(nameLabel)
            make.right.equalTo(nameLabel)
        }
        
        let fontSize = Adaptive.val(15)
        openLabel.font = EZFontType.medium.sfuiText(size: fontSize)
        openLabel.textColor = .black
        openValueLabel.font = EZFontType.medium.sfuiText(size: fontSize)
        openValueLabel.textColor = ColorConstants.gray
        openLabel.setContentHuggingPriority(UILayoutPriority.init(1000), for: .horizontal)
        stackView1.addArrangedSubview(openLabel)
        stackView1.addArrangedSubview(openValueLabel)
        
        stackView2.distribution = .fill
        stackView2.axis = .horizontal
        stackView2.spacing = Adaptive.val(5)
        containerView.addSubview(stackView2)
        stackView2.snp.makeConstraints { (make) in
            make.top.equalTo(stackView1.snp.bottom).offset(5)
            make.left.equalTo(nameLabel)
            make.right.equalTo(nameLabel)
        }
        
        amountLabel.setContentHuggingPriority(UILayoutPriority.init(1000), for: .horizontal)
        amountLabel.font = EZFontType.medium.sfuiText(size: fontSize)
        amountLabel.textColor = .black
        amountValueLabel.font = EZFontType.medium.sfuiText(size: fontSize)
        amountValueLabel.textColor = ColorConstants.gray
        stackView2.addArrangedSubview(amountLabel)
        stackView2.addArrangedSubview(amountValueLabel)
        
        openLabel.text = RS.lbl_opening_date.localized()
        amountLabel.text = RS.lbl_loan_amount.localized()
        
        containerView.addSubview(progressView)
        //progressView.backgroundColor = ColorConstants.dashboardGreen
        progressView.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.right.equalTo(nameLabel)
            make.height.equalTo(Adaptive.val(20))
            make.top.equalTo(stackView2.snp.bottom).offset(Adaptive.val(15))
            make.bottom.equalTo(-Adaptive.val(15))
        }
        progressView.addSubview(percentLabel)
        progressView.progressTintColor = ColorConstants.dashboardGreen
        progressView.tintColor = ColorConstants.red
        progressView.layer.cornerRadius = Adaptive.val(20)/2
        progressView.clipsToBounds = true
        percentLabel.font = EZFontType.medium.sfuiText(size: fontSize)
        percentLabel.textAlignment = .center
        percentLabel.textColor = .white
        percentLabel.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

    func setup(_ loan:LoanResponse?){
        nameLabel.text = loan?.target
        openValueLabel.text = loan?.ldDate
        let amount =  (loan?.amount ?? 1)/100
        //let currency = loan?.currency ?? ""
        amountValueLabel.text = (amount.currencyFormattedStr() ?? "") + " " + CurrencyType.uzs.data.label
        let  paid = (loan?.overall?.principalBalance ?? 0)/100
        let percent = (amount - paid)/amount * 100
        percentLabel.text = String.init(format: "%.2f %@", percent, "%")
        progressView.progress = Float(paid/amount)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
