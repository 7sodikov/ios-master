//
//  LoansViewInstaller.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoansViewInstaller: ViewInstaller {
    var bgView : UIImageView! { get set }
    var tableView : UITableView! { get set }
    var warningMessageView: NoItemsAvailableView! { get set }
    var reloadButtonView : ReloadButtonView! { get set }
}

extension LoansViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
        bgView = UIImageView()
        bgView.image = UIImage(named: "img_dash_light_background")
        tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.register(LoansListCell.self, forCellReuseIdentifier: "LoansListCell")
        
        warningMessageView = NoItemsAvailableView.setupText(title: RS.lbl_no_loan.localized())
        warningMessageView.isHidden = true
        
        reloadButtonView = ReloadButtonView(frame: .zero)
        reloadButtonView.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(tableView)
        mainView.addSubview(warningMessageView)
        mainView.addSubview(reloadButtonView)
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
        bgView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(Adaptive.val(64))
        }
        
        warningMessageView.snp.makeConstraints { (make) in
            make.top.equalTo(Adaptive.val(214))
            make.centerX.equalToSuperview()
        }
        
        reloadButtonView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.width.height.equalTo(ReloadButtonViewSize.Indicator.size)
        }
    }
}

fileprivate struct Size {
    
}
