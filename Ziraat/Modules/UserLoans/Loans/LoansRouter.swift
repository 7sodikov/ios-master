//
//  LoansRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoansRouterProtocol: class {
    static func createModule() -> UIViewController
    func openDetails(_ view:Any, _ loan:LoanResponse?)
}

class LoansRouter: LoansRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = LoansVC()
        let presenter = LoansPresenter()
        let interactor = LoansInteractor()
        let router = LoansRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    func openDetails(_ view: Any, _ loan: LoanResponse?) {
        if let controller = view as? UIViewController{
            let module = LoanDetailsRouter.createModule(loan)
            controller.navigationController?.pushViewController(module, animated: true)
        }
    }
    
}
