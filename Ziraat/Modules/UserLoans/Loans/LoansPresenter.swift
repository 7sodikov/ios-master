//
//  LoansPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoansPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoansVM { get }
    func getLoanList()
    
    func tableViewCellSelected(_ indexPath: IndexPath)
    
}

protocol LoansInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func resultLoanList(_ response: ResponseResult<LoansResponse, AppError>)
    
}

class LoansPresenter: LoansPresenterProtocol {
    unowned var view: LoansVCProtocol!
    var interactor: LoansInteractorProtocol!
    var router: LoansRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoansVM()
    
    // Private property and methods
    func getLoanList() {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        self.interactor.getLoanList()
    }
    
    func tableViewCellSelected(_ indexPath: IndexPath){
        router.openDetails(view as Any, viewModel.loanItems?.loans?[indexPath.row])
    }
    
    
}

extension LoansPresenter: LoansInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    func resultLoanList(_ response: ResponseResult<LoansResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.loanItems = result.data
            if viewModel.loanItems?.loans?.count == 0 {
                view.showReloadButtonForResponses(true, animateReloadButton: false)
            } else {
                view.showReloadButtonForResponses(false, animateReloadButton: false)
            }
        case let .failure(error):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
        }
        
        
    }
    
}
