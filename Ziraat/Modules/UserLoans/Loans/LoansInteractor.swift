//
//  LoansInteractor.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoansInteractorProtocol: class {
    func getLoanList()
}

class LoansInteractor: LoansInteractorProtocol {
    weak var presenter: LoansInteractorToPresenterProtocol!
    func getLoanList() {
        NetworkService.Loan.loanList { (result) in
            self.presenter.resultLoanList(result)
        }
        
    }
}
