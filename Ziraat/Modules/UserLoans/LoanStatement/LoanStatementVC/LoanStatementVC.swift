//
//  LoanStatementVC.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanStatementVCProtocol: BaseViewControllerProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool)
}

class LoanStatementVC: BaseViewController, LoanStatementViewInstaller {
    var bgView: UIImageView!
    
    var tableView: UITableView!
    
    var reloadButtonView: ReloadButtonView!
    
    var mainView: UIView { view }
    var presenter: LoanStatementPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = RS.btn_credit_receipt.localized()
        setupSubviews()
        tableView.delegate =  self
        tableView.dataSource = self
        refresh()
    }
    
    func refresh(){
        let start = Date().addingTimeInterval(-3600 * 24 * 365).string(for: "yyyy-MM-dd")
        let end = Date().string(for: "yyyy-MM-dd")
        let loanId = presenter.viewModel.loan?.id ?? 0
        self.presenter.getStatements(start, end, loanId)
    }
    
    @objc func showOrHideCell(_ sender:UIButton){
        let bool = presenter.viewModel.showItems[sender.tag]
        presenter.viewModel.showItems[sender.tag] = !bool
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
    }
}

extension LoanStatementVC: LoanStatementVCProtocol {
    func showReloadButtonForResponses(_ show: Bool, animateReloadButton: Bool){
        reloadButtonView.isHidden = !show
        reloadButtonView.animateIndicator = animateReloadButton
        tableView.isHidden = show
        tableView.reloadData()
    }
    
}
extension LoanStatementVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.statementList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "ExpandableTableViewCell") as! ExpandableTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(showOrHideCell(_:)), for: .touchUpInside)
        cell.showOrHide(presenter.viewModel.showItems[indexPath.row])
        cell.setupStatement(presenter.viewModel.statementList?[indexPath.row])
        return cell
    }
    
}
