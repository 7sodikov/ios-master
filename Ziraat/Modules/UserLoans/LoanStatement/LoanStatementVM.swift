//
//  LoanStatementVM.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class LoanStatementVM {
    var loan : LoanResponse?
    var showItems = [Bool]()

    var statementList : [LoanStatementResponse]?{
        didSet{
            let count = statementList?.count ?? 0
            if count > 0{
                for _ in 0...count{
                    showItems.append(false)
                }
            } else{
                showItems.removeAll()
            }
            
        }
    }
}
