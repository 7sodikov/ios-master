//
//  LoanStatementInteractor.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanStatementInteractorProtocol: class {
    func getStatements(_ start:String, _ end: String, _ id:Int)
}

class LoanStatementInteractor: LoanStatementInteractorProtocol {
    weak var presenter: LoanStatementInteractorToPresenterProtocol!
    
    func getStatements(_ start:String, _ end: String, _ id:Int){
        
        NetworkService.Loan.statements(loanId: UInt64(id), from: start, to: end, completion: { (result) in
            self.presenter.getStatements(with: result)
        })
    }
}
