//
//  LoanStatementRouter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoanStatementRouterProtocol: class {
    static func createModule(_ loan: LoanResponse?) -> UIViewController
}

class LoanStatementRouter: LoanStatementRouterProtocol {
    static func createModule(_ loan: LoanResponse?) -> UIViewController {
        let vc = LoanStatementVC()
        let presenter = LoanStatementPresenter()
        let interactor = LoanStatementInteractor()
        let router = LoanStatementRouter()
        
        vc.presenter = presenter
        vc.presenter.viewModel.loan = loan
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
