//
//  LoanStatementPresenter.swift
//  Ziraat
//
//  Created by Jurayev Nodir on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol LoanStatementPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: LoanStatementVM { get }
    func getStatements(_ start: String, _ end: String, _ id: Int)
}

protocol LoanStatementInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    func getStatements(with response: ResponseResult<LoanStatementsResponse, AppError>)
    
    
}

class LoanStatementPresenter: LoanStatementPresenterProtocol {
    weak var view: LoanStatementVCProtocol!
    var interactor: LoanStatementInteractorProtocol!
    var router: LoanStatementRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = LoanStatementVM()
    
    // Private property and methods
    func getStatements(_ start: String, _ end: String, _ id: Int) {
        view.showReloadButtonForResponses(true, animateReloadButton: true)
        interactor.getStatements(start, end, id)
    }
    
}

extension LoanStatementPresenter: LoanStatementInteractorToPresenterProtocol {
    
    // INTERACTOR -> PRESENTER
    func getStatements(with response: ResponseResult<LoanStatementsResponse, AppError>) {
        switch response {
        case let .success(result):
            viewModel.statementList = result.data.statements
            view.showReloadButtonForResponses(false, animateReloadButton: false)
        case let .failure(error):
            view.showReloadButtonForResponses(true, animateReloadButton: false)
            view.showError(message: error.localizedDescription)
        }
    }
    
}
