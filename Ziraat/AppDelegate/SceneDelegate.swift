//
//  SceneDelegate.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var blurViewTag = 6719935
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        appDelegate.window = window
//        appDelegate.openAppRoute()
        if KeychainManager.sessionToken == nil {
            appDelegate.router.navigate(to: .loginInit)
        } else {
            appDelegate.router.navigate(to: .mainPage)
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        self.prepareForEnteringIntoActiveMode()
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        self.prepareForEnteringIntoBackgroundMode()
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        (UIApplication.shared.delegate as? AppDelegate)?.applicationWillEnterForeground(UIApplication.shared)
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        
        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }
    
    func applyBlurEffect(toImage image: inout UIImage) {
        let ciImage = CIImage(image: image)
        let filter = CIFilter(name: "CIGaussianBlur")
        filter?.setValue(ciImage, forKey: "inputImage")
        filter?.setValue(18, forKey: "inputRadius")
        guard let blurredImage = filter?.outputImage, let window = window else {
            image = UIImage()
            return
        }
        let croppedFrame = CGRect(
                x: window.frame.origin.x,
                y: window.frame.origin.y,
                width: window.frame.width * UIScreen.main.scale,
                height: window.frame.height * UIScreen.main.scale
        )
        let cover = blurredImage.cropped(to: croppedFrame)
        image = UIImage.init(ciImage: cover)
    }
    
    func screenShot() -> UIImage {
        guard let layer = UIApplication.shared.keyWindow?.layer, let window = self.window else {
            return UIImage()
        }
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(window.frame.size, false, scale);
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshot ?? UIImage()
    }
    
    func prepareForEnteringIntoBackgroundMode() {
        let imageView = UIImageView()
        window = appDelegate.window
        imageView.frame = window!.frame
        imageView.tag = self.blurViewTag
        imageView.contentMode = .scaleToFill
        //change by desire the color and transparency here
        imageView.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        window?.addSubview(imageView)
        var cover = screenShot()
        applyBlurEffect(toImage: &cover)
        imageView.image = cover
    }

    private func prepareForEnteringIntoActiveMode() {
        self.window?.viewWithTag(self.blurViewTag)?.removeFromSuperview()
    }
}
