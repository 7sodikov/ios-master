//
//  AppDelegate.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//


import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import UserNotifications

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    var interactor: AppDelegateInteractorProtocol = AppDelegateInteractor()
    var router: AppDelegateRouterProtocol = AppDelegateRouter()
    
    func openAppRoute() {
        if KeychainManager.sessionToken == nil {
            router.navigate(to: .loginInit)
        } else {
            router.navigate(to: .pinCodeValidate)
        }
    }
        
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        KeychainManager.actionForFirstAppRun()
        if #available(iOS 13.0, *) {
            
        } else {
            let window = UIWindow(frame: UIScreen.main.bounds)
            self.window = window
            
            openAppRoute()
        }

        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        application.registerForRemoteNotifications()
      
        FirebaseApp.configure()
        //Crashlytics.crashlytics()
        Messaging.messaging().delegate = self
        
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        
        if let _ = LocalizationManager.language {
            
        } else {
            if let langStr = Locale.current.languageCode,
                let deviceLanguage = Language(rawValue: langStr) {
                LocalizationManager.language = deviceLanguage
            } else {
                LocalizationManager.language = Language.english
            }
        }
        
        IQKeyboardManager.shared.enable = true
        
//        if let option = launchOptions {
//            let info = option[UIApplication.LaunchOptionsKey.remoteNotification]
//          if (info != nil) {
//            goAnotherVC()
//          }
//        }
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        let notificationId = userInfo["gcm.message_id"] as! String
//        let notifVC = NotificationsListRouter.createModule()
//        let navCtrl = NavigationController(rootViewController: notifVC)
//        navCtrl.pushViewController(notifVC, animated: true)
        
        if application.applicationState == UIApplication.State.active {

            } else if application.applicationState == UIApplication.State.background {

            } else if application.applicationState == UIApplication.State.inactive {
                
        }
    }
    
    func goAnotherVC() {
        let notifVC = NotificationsListRouter.createModule()
        let navCtrl = NavigationController(rootViewController: notifVC)
        navCtrl.pushViewController(notifVC, animated: true)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if self.window?.rootViewController?.isMember(of: PinSettingsVC.self) == false {
            UDManager.lastUpdatedTime = Date().timeIntervalSince1970
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if KeychainManager.didLogin {
            if let lastTime = UDManager.lastUpdatedTime {
                let currentTime = Date().timeIntervalSince1970
                if currentTime - lastTime > 30 {
                    router.navigate(to: .pinCodeValidate)
                }
            }
        }
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        KeychainManager.logout()
        PushNotificationsManager.unsubscribeFromTopic()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Ziraat")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        
        if self.window?.rootViewController?.isMember(of: PinSettingsVC.self) == false {
            UDManager.lastUpdatedTime = Date().timeIntervalSince1970
        }
    }
    
//    //get device token here
//        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
//            deviceToken: Data)
//        {
//            var token = ""
//            for i in 0..<deviceToken.count {
//                token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
//            }
//            print("Registration succeeded!")
//            print("Token: ", token)
//
//            //send tokens to backend server
//    //        storeTokens(token)
//        }
//
//        //get error here
//        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error:
//            Error) {
//            print("Registration failed!")
//        }
//
//        //get Notification Here below ios 10
//        func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
//            // Print notification payload data
//            print("Push notification received: \(data)")
//        }
//
//        //This is the two delegate method to get the notification in iOS 10..
//        //First for foreground
//        @available(iOS 10.0, *)
//        func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options:UNNotificationPresentationOptions) -> Void)
//        {
//            print("Handle push from foreground")
//            // custom code to handle push while app is in the foreground
//            print("\(notification.request.content.userInfo)")
//        }
//        //Second for background and close
//        @available(iOS 10.0, *)
//        func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response:UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
//        {
//            print("Handle push from background or closed")
//            // if you set a member variable in didReceiveRemoteNotification, you will know if this is from closed or background
//            print("\(response.notification.request.content.userInfo)")
//        }
//
//        func applicationWillResignActive(_ application: UIApplication) {
//            // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//            // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
//        }
//
//        func applicationDidBecomeActive(_ application: UIApplication) {
//            // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        }
//
//        func applicationWillTerminate(_ application: UIApplication) {
//            // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//        }

    
//    func getNotificationSettings() {
//      UNUserNotificationCenter.current().getNotificationSettings { settings in
//        print("Notification settings: \(settings)")
//
//        guard settings.authorizationStatus == .authorized else { return }
//        DispatchQueue.main.async {
//          UIApplication.shared.registerForRemoteNotifications()
//        }
//      }
//    }
//
//    func registerForPushNotifications() {
//        UNUserNotificationCenter.current()
//          .requestAuthorization(
//            options: [.alert, .sound, .badge]) { [weak self] granted, _ in
//            print("Permission granted: \(granted)")
//            guard granted else { return }
//            self?.getNotificationSettings()
//          }
//    }
//
//    func application(
//      _ application: UIApplication,
//      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
//    ) {
//      let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
//      let token = tokenParts.joined()
//      print("Device Token: \(token)")
//    }
//
//    func application(
//      _ application: UIApplication,
//      didFailToRegisterForRemoteNotificationsWithError error: Error
//    ) {
//      print("Failed to register: \(error)")
//    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //      if let messageID = userInfo[gcmMessageIDKey] {
        //        debugPrint("Message ID: \(messageID)")
        //      }
        //
        // Print full message.
        debugPrint(userInfo)
        
        // Change this to your preferred presentation option
            completionHandler([UNNotificationPresentationOptions.alert, UNNotificationPresentationOptions.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
//              if let messageID = userInfo[gcmMessageIDKey] {
//                debugPrint("Message ID: \(messageID)")
//              }
        
        // Print full message.
        debugPrint(userInfo)
        
//        appDelegate.window?.rootViewController = NotificationsListRouter.createModule()
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        
    }
}

extension AppDelegate: MessagingDelegate {
    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        debugPrint("Firebase registration token: \(fcmToken)")
//
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: NSNotification.Name("FCMToken"), object: nil, userInfo: dataDict)
//    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let dataDict:[String: String?] = ["token": fcmToken]
        NotificationCenter.default.post(name: NSNotification.Name("FCMToken"), object: nil, userInfo: dataDict)
        debugPrint("Firebase registration token: \(fcmToken)")
//        UDManager.fcmtoken = fcmToken
        PushNotificationsManager.fcmToken = fcmToken
    }
    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        debugPrint("\(remoteMessage.appData)")
//    }
//
    
}
