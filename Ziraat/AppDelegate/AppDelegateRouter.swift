//
//  AppDelegateRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/1/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

enum AppScreenType: Int {
    case mainPage = 0
    case loginInit
    case login
    case pinCodeValidate
    case pinCodeCreate
    case myAccount
    case accountDetails
    case resetPassword
    case logout
    case loginConfirm
}

protocol AppDelegateRouterProtocol: AnyObject {
    func navigate(to screen: AppScreenType)
}

class AppDelegateRouter: AppDelegateRouterProtocol {
    func navigate(to screen: AppScreenType) {
        
        if JailbreakDetector.isDeviceJailBroken() {
            appDelegate.window?.rootViewController = JailbreakBlockerRouter.createModule()
            appDelegate.window?.makeKeyAndVisible()
            return
        }
        
        var controller = UIViewController()
        switch screen {
        case .loginInit:
            controller = LoginInitRouter.createModule()
        case .login:
            controller = LoginRouter.createModuleInNavController()
        case .mainPage:
            controller = DashboardRouter.createModule()
            break
        case .pinCodeValidate:
            let pin = KeychainManager.pin
            let graphic = KeychainManager.graphicKey
            if graphic != nil{
                controller = GraphicKeySettingsRouter.createModule(createMode:false)
            }
            if pin != nil{
                controller = PinSettingsRouter.createModule(createPin: false)
            }
            if !(pin != nil || graphic != nil){
                UDManager.logout()
                controller = LoginInitRouter.createModule()
            }
            break
        case .pinCodeCreate:
            break
        case .myAccount:
            break
        case .accountDetails:
            break
        case .resetPassword:
            controller = ResetPasswordRouter.createModuleWithNavigationController(isRedStyle: true)
        case .logout:
            controller = LogoutRouter.createModule()
        case .loginConfirm:
            break
        }

        appDelegate.window?.rootViewController = controller
        
//        appDelegate.window?.rootViewController = ResetPasswordRouter.createModuleWithNavigationController(isRedStyle: true)
        
//        let navCtrl = NavigationController(rootViewController: FavoritesListRouter.createModule())
//        appDelegate.window?.rootViewController = navCtrl// PinSettingsRouter.createModule()

        appDelegate.window?.makeKeyAndVisible()
    }
}
