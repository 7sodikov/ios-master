//
//  Array+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

extension Array {
   func filterDuplicate<T: Hashable>(_ keyValue: (Element) -> T) -> [Element] {
      var uniqueKeys = Set<T>()
      return filter {uniqueKeys.insert(keyValue($0)).inserted }
   }

   func filterDuplicate<T>(_ keyValue: (Element) -> T) -> [Element] {
      return filterDuplicate {"\(keyValue($0))"}
   }
}
