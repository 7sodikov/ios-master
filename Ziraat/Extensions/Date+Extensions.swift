//
//  Date+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension Date {
    func string(for format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
//        formatter.timeZone = TimeZone(identifier: "GMT")!
        return formatter.string(from: self)
    }
    
    static public func date(string: String?, format: String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en")
        return dateFormatter.date(from: string ?? "") ?? Date()
    }
    
    func secondsTillNow() -> TimeInterval {
        return Date().timeIntervalSince(self)
    }


//Wednesday, Sep 12, 2018           --> EEEE, MMM d, yyyy
//09/12/2018                        --> MM/dd/yyyy
//09-12-2018 14:11                  --> MM-dd-yyyy HH:mm
//Sep 12, 2:11 PM                   --> MMM d, h:mm a
//September 2018                    --> MMMM yyyy
//Sep 12, 2018                      --> MMM d, yyyy
//Wed, 12 Sep 2018 14:11:54 +0000   --> E, d MMM yyyy HH:mm:ss Z
//2018-09-12T14:11:54+0000          --> yyyy-MM-dd'T'HH:mm:ssZ
//12.09.18                          --> dd.MM.yy
//10:41:02.112                      --> HH:mm:ss.SSS
    
    func currentDateTime() -> CLong {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = formatter.string(from: now)
        if let date = DateFormatter().getDateFromFormat(dateStr: dateString, newDateFormat: formatter.dateFormat) {
            return CLong(date.timeIntervalSince1970)
        }
        return 0
    }
    
    func calendar() -> Calendar {
        var cal = Calendar.current
        cal.timeZone = TimeZone(identifier: "GMT")!
        return cal
    }
    
    func monday() -> Date {
        return Date().calendar().date(from: Date().calendar().dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    
    func daysCountOfMonth() -> Int {
        let c = calendar()
        let dateComponents = DateComponents(year: c.component(.year, from: self), month: c.component(.month, from: self))
        let date = c.date(from: dateComponents)!
        let range = c.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        return numDays
    }
    
//    func startOfMonth() -> Date {
//        let dateStr = DateFormatter().getStrFromDate(Date(), "01.MM.yyyy")
//        return DateFormatter().getDateFromFormat(dateStr: dateStr, newDateFormat: defaultDateFormat)!
//    }
//    
//    func endOfMonth() -> Date {
//        let date = Date().calendar().date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
//        return Date().calendar().date(bySettingHour: 23, minute: 59, second: 59, of: date)!
//    }
    
    var evening: Date {
        return Date().calendar().date(bySettingHour: 23, minute: 59, second: 59, of: self)!
    }
    
    var morning: Date {
        return Date().calendar().date(bySettingHour: 0, minute: 0, second: 0, of: self)!
    }
    
    var yeasterday: Date {
        return Date().calendar().date(byAdding: .day, value: -1, to: morning)!
    }
    
    var startOfCurrentWeek: Date {
        let day = Date().calendar().date(bySettingHour: 0, minute: 0, second: 0, of: monday())!
        return Date().calendar().date(byAdding: .day, value: 0, to: day)!
    }
    
    var endOfCurrentWeek: Date {
        let day = Date().calendar().date(bySettingHour: 23, minute: 59, second: 59, of: monday())!
        return Date().calendar().date(byAdding: .day, value: 6, to: day)!
    }
    
    var startOfBeforeWeek: Date {
        let day = Date().calendar().date(bySettingHour: 0, minute: 0, second: 0, of: monday())!
        return Date().calendar().date(byAdding: .day, value: -7, to: day)!
    }
    
    var endOfBeforeWeek: Date {
        let day = Date().calendar().date(bySettingHour: 23, minute: 59, second: 59, of: monday())!
        return Date().calendar().date(byAdding: .day, value: -1, to: day)!
    }
    
//    func startPreviousMonth() -> Date {
//        let prevDate = Date().calendar().date(byAdding: .month, value: -1, to: Date().startOfMonth())!
//        return prevDate
//    }
//
//    func endPreviousMonth() -> Date {
//        let prevDate = Date().calendar().date(bySettingHour: 23, minute: 59, second: 59, of: Date().startPreviousMonth())!
//        return Date().calendar().date(byAdding: DateComponents(month: 1, day: -1), to: prevDate)!
//    }
    
//    func currentDateNow() -> CLong {
//        let date = Date()
//        let format = defaultDateFormat
//        let str = DateFormatter().getStrFromDate(date, format)
//        if let nowDate = DateFormatter().getDateFromFormat(dateStr: str, newDateFormat: format) {
//            return CLong(nowDate.timeIntervalSince1970)
//        }
//        return 0
//    }
}
