//
//  CALayer+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    func currentMediaTime() -> Double {
        return convertTime(CACurrentMediaTime(), from: nil)
    }
    
    func clear() {
        sublayers?.forEach { $0.removeFromSuperlayer() }
    }
    
    func setShadow(opacity: Float = 1, shadowRadius: CGFloat, shadowOpacity: Float, shadowOffset: CGSize = .zero) {
        self.opacity = opacity
        self.shadowRadius = Adaptive.val(shadowRadius)
        self.shadowOpacity = shadowOpacity
        self.shadowOffset = shadowOffset
        self.shadowColor = UIColor.black.cgColor
    }
}
