//
//  Codable+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct JSONCodingKeys: CodingKey {
    var stringValue: String
    
    init(stringValue: String) {
        self.stringValue = stringValue
    }
    
    var intValue: Int?
    
    init?(intValue: Int) {
        self.init(stringValue: "\(intValue)")
        self.intValue = intValue
    }
}

//extension JSONDecoder {
//    func decode<T: BaseModel>(_ type: T.Type, from object: Any?) -> T? {
//        if let obj = object, let data = try? JSONSerialization.data(withJSONObject: obj, options: .init(rawValue: 0)) {
//            do {
//                let result = try JSONDecoder().decode(T.self, from: data) as T
//                return result
//            } catch {
//                debugPrint(error)
//                return nil
//            }
//        }
//        return nil
//    }
//}
