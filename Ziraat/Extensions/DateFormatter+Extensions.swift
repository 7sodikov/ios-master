//
//  DateFormatter+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

extension DateFormatter {
    func getDateFromFormat(dateStr: String, newDateFormat: String, timeZone: TimeZone? = TimeZone(abbreviation: "GMT")) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = newDateFormat
        dateFormatter.locale = Locale(identifier: "en")
        dateFormatter.timeZone = timeZone
        return dateFormatter.date(from: dateStr)
    }
    
    func getStrFromDate(_ date: Date?, _ dateFormat: String, timeZone: TimeZone? = TimeZone(abbreviation: "GMT")) -> String {
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = dateFormat
        newDateFormatter.locale = Locale(identifier: "en")
        newDateFormatter.timeZone = timeZone
        if let dt = date {
            return newDateFormatter.string(from: dt)
        } else {
            return String.emptyString
        }
        
    }
}
