//
//  NSLayoutConstraint+Extension.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import Foundation

extension NSLayoutConstraint {
    func topConstraintFromSaveArea() {
        if #available(iOS 11.0, *) {
            self.constant -= 64
        }
    }
}
