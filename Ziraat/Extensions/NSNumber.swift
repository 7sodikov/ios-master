//
//  NSNumber.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension NSNumber {
    func prettyFormattedStr() -> String {
//        let formatter = NumberFormatter()
//        formatter.groupingSeparator = " "
//        formatter.groupingSize = 3
//        formatter.numberStyle = NumberFormatter.Style.decimal
//        return formatter.string(from: self) ?? ""
        let str = String(format: "%.2f", self.floatValue)
        return Utils.divideAmountString(text: str, char: " ")
    }
}
