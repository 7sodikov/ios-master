//// Header

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Add)
//- (BOOL)numberValidation:(UITextField *)textField range:(NSRange)range replacementString:(NSString *)string length:(NSInteger)length;
- (BOOL)integerValidation:(UITextField *)textField range:(NSRange)range replacementString:(NSString *)string length:(NSInteger)length;
- (BOOL)amountFormat:(UITextField *)textField range:(NSRange)range replacementString:(NSString *)string length:(NSInteger)length;

@end

NS_ASSUME_NONNULL_END
