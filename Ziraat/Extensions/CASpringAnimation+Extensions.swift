//
//  CASpringAnimation+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension CASpringAnimation {
    static func newsHeartAnimation(for layer: CALayer, showing: Bool, key: String, delegate: CAAnimationDelegate?) {
        if showing {
            layer.transform = CATransform3DMakeScale(0.6, 0.6, 1)
            let scaling = CASpringAnimation(keyPath: "transform")
            scaling.damping = 15
            scaling.mass = 0.9
            scaling.stiffness = 700
            scaling.beginTime = 0 //layer.currentMediaTime()
            scaling.duration = scaling.settlingDuration
            scaling.fromValue = NSValue(caTransform3D: CATransform3DMakeScale(0.6, 0.6, 1))
            scaling.toValue = NSValue(caTransform3D: CATransform3DMakeScale(1, 1, 1))
            layer.add(scaling, forKey: nil)
            layer.transform = CATransform3DMakeScale(1, 1, 1)
        } else {
            let collapsing = CABasicAnimation(keyPath: "transform")
            collapsing.fromValue = NSValue(caTransform3D: CATransform3DMakeScale(1, 1, 1))
            collapsing.toValue = NSValue(caTransform3D: CATransform3DMakeScale(0.6, 0.6, 1))
            
            /// Fade Animation
            let fadeAnimation = CABasicAnimation(keyPath: "opacity")
            fadeAnimation.fromValue = NSNumber(value: 1)
            fadeAnimation.toValue = NSNumber(value: 0)
            
            /// Grouping All Animations
            let groupAnimation = CAAnimationGroup()
            groupAnimation.beginTime = 0 //layer.currentMediaTime()
            groupAnimation.duration = 0.1
            groupAnimation.delegate = delegate
            groupAnimation.animations = [collapsing, fadeAnimation]
            groupAnimation.setValue(layer, forKey: key)
            layer.add(groupAnimation, forKey: nil)
            
            layer.transform = CATransform3DMakeScale(0.6, 0.6, 1)
            layer.opacity = 0
        }
    }
}
