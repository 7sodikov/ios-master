//// Header

#import "UIViewController+Add.h"

@implementation UIViewController (Add)

- (BOOL)numberValidation:(UITextField *)textField range:(NSRange)range replacementString:(NSString *)string length:(NSInteger)length {
    __block NSString *text = [textField text];
    if ([string isEqualToString:@"."]) {
        if ([text rangeOfString:@"."].location != NSNotFound) {
            return NO;
        }
        if (text.length == 0){
            return NO;
        }
        if (text.length == length) {
            length += 3;
        }
    }
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.\b"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
        return NO;
    }
    if ([text rangeOfString:@"."].location != NSNotFound) {
        length = length + 3;
    }
    
    if (text.length > range.location) {
        text = [text stringByReplacingCharactersInRange:range withString:string];
    }
    
    if (text.length >= length) {
        return NO;
    }
    return YES;
}

- (BOOL)integerValidation:(UITextField *)textField range:(NSRange)range replacementString:(NSString *)string length:(NSInteger)length {
    __block NSString *text = [textField text];
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
        return NO;
    }
    if ([text rangeOfString:@"."].location != NSNotFound) {
        length = length + 3;
    }
    
    if (text.length > range.location) {
        text = [text stringByReplacingCharactersInRange:range withString:string];
    }
    
    if (text.length >= length) {
        return NO;
    }
    return YES;
}
- (BOOL)amountFormat:(UITextField *)textField range:(NSRange)range replacementString:(NSString *)string length:(NSInteger)length {
    
    //return [self doubleFormat:textField range:range replacementString:string length:length + 1];
    
    string = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    __block NSString *text = [textField text];
    
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@".0123456789\b"];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
        return NO;
    }
    
    text = [text stringByReplacingCharactersInRange:range withString:string];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSRange dotRange = [text rangeOfString:@"."];
    if (dotRange.location != NSNotFound && [text isEqualToString:@"."]) {
        return NO;
    }
    
    NSString *tiyin = nil;
    if (dotRange.length) {
        NSArray *items = [text componentsSeparatedByString:@"."];
        text = items[0];
        if (items.count == 2) {
            tiyin = items[1];
        }
        
    }
    
    NSMutableArray *array = @[].mutableCopy;
    NSString *greeting;
    NSString *sum = text;
    if (sum.length <= 3) {
        
    } else{
        while (sum.length > 3) {
            
            NSString *result = [sum substringFromIndex:[sum length] - 3];
            
            sum = [sum substringWithRange:NSMakeRange(0,[sum length]-[result length])];
            [array insertObject:result atIndex:0];
            greeting = [array componentsJoinedByString:@" "];
        }
        sum = [NSString stringWithFormat:@"%@ %@",sum,greeting];
    }
    
    NSString *newString = sum;
    newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
    
    if (newString.length >= length + 3) {
        return NO;
    }
    
    if (tiyin.length > 2) {
        return NO;
    }
    if (tiyin) {
        [textField setText:[NSString stringWithFormat:@"%@.%@",newString,tiyin]];
    } else{
        [textField setText:newString];
    }
    [textField sendActionsForControlEvents:UIControlEventEditingChanged];
    return NO;
    
}

@end
