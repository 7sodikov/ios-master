//
//  UIViewController+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UIViewController {
    func showNotice(_ message: String, title: String? = nil, okBtnTitle: String? = nil, cancelBtnTitle: String? = nil,
                    okClick: ((UIAlertAction) -> Swift.Void)? = nil, cancelClick: ((UIAlertAction) -> Swift.Void)? = nil) {
        
        let alert = UIAlertController(title: title ?? "Внимание!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okBtnTitle ?? "OK", style: .cancel, handler: okClick))
        if let cancelTitle = cancelBtnTitle {
            alert.addAction(UIAlertAction(title: cancelTitle, style: .default, handler: cancelClick))
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func insert(in viewController: UIViewController, view: UIView? = nil, frame: CGRect? = nil) {
        viewController.addChild(self)
        self.didMove(toParent: viewController)
        if let container = view {
            container.addSubview(self.view)
            self.view.frame = container.bounds
        } else {
            viewController.view.addSubview(self.view)
            self.view.frame = viewController.view.bounds
        }
        if let newFrame = frame {
            self.view.frame = newFrame
        }
    }
    
    func add(childController: UIViewController) {
        // Add Child View Controller
        addChild(childController)
        
        // Add Child View as Subview
        view.addSubview(childController.view)
        
        // Configure Child View
        childController.view.frame = view.bounds
        childController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        childController.didMove(toParent: self)
    }
    
    func remove(childController: UIViewController) {
        // Notify Child View Controller
        childController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        childController.view.removeFromSuperview()
        
        // Notify Child View Controller
        childController.removeFromParent()
    }
    
    func showToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
