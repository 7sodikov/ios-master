//
//  UIAlertController+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func showAlert(_ onCtrl: UIViewController, title: String? = nil, message: String, okBtnTitle: String? = nil, cancelBtnTitle: String? = nil,
                          okClick: ((UIAlertAction) -> Void)? = nil, cancelClick: ((UIAlertAction) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title ?? "Внимание!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: okBtnTitle ?? "OK", style: .cancel, handler: okClick))
        if let cancelTitle = cancelBtnTitle {
            alert.addAction(UIAlertAction(title: cancelTitle, style: .default, handler: cancelClick))
        }
        
        onCtrl.present(alert, animated: true, completion: nil)
    }
    
//    static func showAlertOnForgotPINTap(_ onCtrl: UIViewController,
//                                        okClick: @escaping ((UIAlertAction) -> Void)) {
//        UIAlertController.showAlert(onCtrl,
//                                    title: nil,
//                                    message: R.string.localizable.alert_ask_to_reset_pin(),
//                                    okBtnTitle: R.string.localizable.btn_yes().uppercased(),
//                                    cancelBtnTitle: R.string.localizable.btn_no().uppercased(),
//                                    okClick: okClick,
//                                    cancelClick: nil)
//    }
}
