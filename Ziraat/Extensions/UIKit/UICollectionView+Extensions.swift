//
//  UICollectionView+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UICollectionView {
    /** Finds collectionview cell IndexPath nearest to the center of collectionView */
    func mostCenteredCellIndexPath() -> IndexPath? {
        // Arbitrarily start with the last cell (as a default)
        var closestCell = visibleCells[0]
        for cell in visibleCells as [UICollectionViewCell] {
            let closestCellDelta = abs(closestCell.center.x - bounds.size.width/2.0 - contentOffset.x)
            let cellDelta = abs(cell.center.x - bounds.size.width/2.0 - contentOffset.x)
            if cellDelta < closestCellDelta {
                closestCell = cell
            }
        }
        let indexPath = self.indexPath(for: closestCell)
        return indexPath
    }
}

public extension UICollectionView {
    
    func register(_ cellType: CollectionViewCell.Type) {
        register(cellType, forCellWithReuseIdentifier: cellType.uid)
    }
    
    func reusable<CellType: CollectionViewCell>(_ cellType: CellType.Type, for indexPath: IndexPath) -> CellType {
        dequeueReusableCell(withReuseIdentifier: cellType.uid, for: indexPath) as! CellType
    }
 
    func scrollToFirstItem(_ isEmpty: Bool) {
        guard !isEmpty else { return }
        let indexPath = IndexPath(row: 0, section: 0)
        scrollToItem(at: indexPath, at: .init(), animated: false)
    }
    
}

public extension UICollectionViewCell {
    
    static var uid: String {
        "\(String(describing: self))UniqueCellIdentifier"
    }
    
}
