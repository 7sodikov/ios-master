//
//  UIDevice+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

enum ModelType {
    // iPod Touch
    case iPod_Touch
    case iPod_Touch_2
    case iPod_Touch_3
    case iPod_Touch_4
    case iPod_Touch_5
    case iPod_Touch_6
    
    // iPhone
    case iPhone
    case iPhone_3G
    case iPhone_3GS
    case iPhone_4
    case iPhone_4s
    case iPhone_5
    case iPhone_5c
    case iPhone_5s
    case iPhone_6
    case iPhone_6_Plus
    case iPhone_6s
    case iPhone_6s_Plus
    case iPhone_7
    case iPhone_7_Plus
    case iPhone_SE
    case iPhone_8
    case iPhone_8_Plus
    case iPhone_X
    case iPhone_XS
    case iPhone_XS_Max
    case iPhone_XR
    
    // iPad
    case iPad_1
    case iPad_2
    case iPad_3
    case iPad_4
    case iPad_Air
    case iPad_Air_2
    case iPad_5
    case iPad_6
    case iPad_Mini
    case iPad_Mini_2
    case iPad_Mini_3
    case iPad_Mini_4
    case iPad_Pro_9_7_Inch
    case iPad_Pro_12_9_Inch
    case iPad_Pro_12_9_Inch_2_Generation
    case iPad_Pro_10_5_Inch
    
    // Apple TV
    case apple_TV
    case apple_TV_4K
    case homePod
    
    // Simulator
    case simulator
    case undefined
}

extension UIDevice {
    var info: String {
        return "\(systemName) \(systemVersion)"
    }
    
    var uuid: String {
        return identifierForVendor?.uuidString.alphanumeric ?? ""
    }
    
    // please refer to: https://stackoverflow.com/a/11197770/845345
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        // iPod Touch
        case "iPod1,1":                                 return "iPod Touch"
        case "iPod2,1":                                 return "iPod Touch 2"
        case "iPod3,1":                                 return "iPod Touch 3"
        case "iPod4,1":                                 return "iPod Touch 4"
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
            
        // iPhone
        case "iPhone1,1":                               return "iPhone"
        case "iPhone1,2":                               return "iPhone 3G"
        case "iPhone2,1":                               return "iPhone 3GS"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        
        // iPad
        case "iPad1,1":                                 return "iPad 1"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad7,5", "iPad7,6":                      return "iPad 6"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        
        // Apple TV
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        
        // Simulator
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    var modelType: ModelType {
        switch UIDevice.current.modelName {
        case "iPod Touch 5":                        return .iPod_Touch_5
        case "iPod Touch 6":                        return .iPod_Touch_6
        case "iPhone 4":                            return .iPhone_4
        case "iPhone 4s":                           return .iPhone_4s
        case "iPhone 5":                            return .iPhone_5
        case "iPhone 5c":                           return .iPhone_5c
        case "iPhone 5s":                           return .iPhone_5s
        case "iPhone 6":                            return .iPhone_6
        case "iPhone 6 Plus":                       return .iPhone_6_Plus
        case "iPhone 6s":                           return .iPhone_6s
        case "iPhone 6s Plus":                      return .iPhone_6s_Plus
        case "iPhone 7":                            return .iPhone_7
        case "iPhone 7 Plus":                       return .iPhone_7_Plus
        case "iPhone SE":                           return .iPhone_SE
        case "iPhone 8":                            return .iPhone_8
        case "iPhone 8 Plus":                       return .iPhone_8_Plus
        case "iPhone X":                            return .iPhone_X
        case "iPad 2":                              return .iPad_2
        case "iPad 3":                              return .iPad_3
        case "iPad 4":                              return .iPad_4
        case "iPad Air":                            return .iPad_Air
        case "iPad Air 2":                          return .iPad_Air_2
        case "iPad 5":                              return .iPad_5
        case "iPad 6":                              return .iPad_6
        case "iPad Mini":                           return .iPad_Mini
        case "iPad Mini 2":                         return .iPad_Mini_2
        case "iPad Mini 3":                         return .iPad_Mini_3
        case "iPad Mini 4":                         return .iPad_Mini_4
        case "iPad Pro 9.7 Inch":                   return .iPad_Pro_9_7_Inch
        case "iPad Pro 12.9 Inch":                  return .iPad_Pro_12_9_Inch
        case "iPad Pro 12.9 Inch 2. Generation":    return .iPad_Pro_12_9_Inch_2_Generation
        case "iPad Pro 10.5 Inch":                  return .iPad_Pro_10_5_Inch
        case "Apple TV":                            return .apple_TV
        case "Apple TV 4K":                         return .apple_TV_4K
        case "HomePod":                             return .homePod
        case "Simulator":                           return .simulator
        default:                                    return .undefined
        }
    }
}
