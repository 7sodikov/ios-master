//
//  UIImageView.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UIImageView {
    // You have to set this value to true to use vector (scalable) images
    @IBInspectable var adjustsImageSize: Bool {
        get {
            if #available(iOS 11.0, *) {
                return adjustsImageSizeForAccessibilityContentSizeCategory
            } else {
                return false
            }
        }
        set {
            if #available(iOS 11.0, *) {
                adjustsImageSizeForAccessibilityContentSizeCategory = newValue
            }
        }
    }
    
    static func addViewButton (_ fromY: CGFloat, _ scale: CGFloat, _ bg_color: UIColor, _ border: Bool = false ) -> UIImageView {
        let width = screenSize.width * 560 / 748
        let loginCardView = UIImageView(frame: CGRect(x: 0,
                                                      y: 0,
                                                      width: width,
                                                      height: width*0.57))
        loginCardView.center = CGPoint(x: screenSize.width/2, y: loginCardView.center.y + fromY)
        loginCardView.backgroundColor = bg_color
        loginCardView.clipsToBounds = true
        loginCardView.layer.cornerRadius = 10
        loginCardView.isUserInteractionEnabled = true
        loginCardView.transform = CGAffineTransform(scaleX: scale, y: scale)
        
        if border {
            let borderLayer = CAShapeLayer()
            borderLayer.frame = loginCardView.bounds
            borderLayer.path = UIBezierPath(roundedRect: loginCardView.bounds, cornerRadius: loginCardView.layer.cornerRadius).cgPath
            borderLayer.fillColor = UIColor.clear.cgColor
            borderLayer.strokeColor = UIColor(127, 131, 168).cgColor
            borderLayer.lineWidth = 2
            borderLayer.lineJoin = CAShapeLayerLineJoin.round
            borderLayer.lineDashPattern = [15, 5]
            loginCardView.layer.addSublayer(borderLayer)
        }
        
        return loginCardView
    }
    
//    func setGifImage(fileName: String, repeatCount: Int = 0 ) {
//        DispatchQueue.global().async {
//            if let gif = UIImage.makeGIFFromCollection(name: fileName, repeatCount: repeatCount) {
//                DispatchQueue.main.async {
//                    self.animationImages = gif.images
//                    self.animationDuration = gif.durationInSec
//                    self.animationRepeatCount = gif.repeatCount
//                    self.startAnimating()
//                }
//            }
//        }
//    }
    
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
