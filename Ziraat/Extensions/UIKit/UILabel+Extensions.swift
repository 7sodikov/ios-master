//
//  UILabel+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UILabel {
    var sizeOfText: CGSize {
        if let text = text {
            let fontAttributes: [NSAttributedString.Key: UIFont] = [NSAttributedString.Key.font: font]
            let size = text.size(withAttributes: fontAttributes)
            return size
        }
        return frame.size
    }
}

class VerticalTopAlignLabel: UILabel {

    override func drawText(in rect:CGRect) {
        guard let labelText = text else {  return super.drawText(in: rect) }

        let attributedText = NSAttributedString(string: labelText, attributes: [NSAttributedString.Key.font: font as Any])
        var newRect = rect
        newRect.size.height = attributedText.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size.height

        if numberOfLines != 0 {
            newRect.size.height = min(newRect.size.height, CGFloat(numberOfLines) * font.lineHeight)
        }

        super.drawText(in: newRect)
    }
}
