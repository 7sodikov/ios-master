//
//  UIView+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UIView {
    
    func constraint(_ action: @escaping (LayoutGuide) -> Void) {
        translatesAutoresizingMaskIntoConstraints = false
        action(LayoutGuide(self))
    }
    
    func takeScreenShot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        return image
    }
}

// MARK: - Appearance
extension UIView {
    func makeOval() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.masksToBounds = true
    }
}

// MARK: - Animations
extension UIView {
    func shake(_ completion: ((Bool) -> Void)?) {
        self.transform = CGAffineTransform(translationX: 20, y: 0)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: completion)
    }
    
    static func shake(view: UIView) {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0]
        view.layer.add(animation, forKey: nil)
    }
    
    func clearSubviews() {
        for v in subviews {
            v.removeFromSuperview()
        }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    public func fillSuperView(padding: UIEdgeInsets = .zero){
        translatesAutoresizingMaskIntoConstraints = false
        if let superviewTopAnchor = superview?.topAnchor{
            topAnchor.constraint(equalTo: superviewTopAnchor, constant: padding.top).isActive = true
        }
        if let superviewLeftAnchor = superview?.leftAnchor{
            leftAnchor.constraint(equalTo: superviewLeftAnchor, constant: padding.left).isActive = true
        }
        if let superviewBottomAnchor = superview?.bottomAnchor{
            bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -padding.bottom).isActive = true
        }
        if let superviewRightAnchor = superview?.rightAnchor{
            rightAnchor.constraint(equalTo: superviewRightAnchor, constant: -padding.right).isActive = true
        }
        
    }
}

extension UIView {
    @discardableResult
    func setWidthConstraint(for value: CGFloat) -> NSLayoutConstraint {
        let constr = NSLayoutConstraint(item: self as Any, attribute: .width, relatedBy: .equal,
                                        toItem: nil, attribute: .notAnAttribute,
                                        multiplier: 1.0, constant: value)
        NSLayoutConstraint.activate([constr])
        return constr
    }
    
    @discardableResult
    func setHeightConstraint(for value: CGFloat) -> NSLayoutConstraint {
        let constr = NSLayoutConstraint(item: self as Any, attribute: .height, relatedBy: .equal,
                                        toItem: nil, attribute: .notAnAttribute,
                                        multiplier: 1.0, constant: value)
        NSLayoutConstraint.activate([constr])
        return constr
    }
    
    func centerVertically(in view: UIView) {
        let yConstraint = NSLayoutConstraint(item: self as Any, attribute: .centerY, relatedBy: .equal,
                                             toItem: view, attribute: .centerY,
                                             multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([yConstraint])
    }
    
    func centerHorizontally(in view: UIView) {
        let xConstraint = NSLayoutConstraint(item: self as Any, attribute: .centerX, relatedBy: .equal,
                                             toItem: view, attribute: .centerX,
                                             multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([xConstraint])
    }
    
    func pin(to view: UIView) {
        let top = NSLayoutConstraint(item: self as Any, attribute: .top, relatedBy: .equal,
                                     toItem: view, attribute: .top, multiplier: 1, constant: 0)
        
        let leading = NSLayoutConstraint(item: self as Any, attribute: .leading, relatedBy: .equal,
                                         toItem: view, attribute: .leading, multiplier: 1, constant: 0)
        
        let bottom = NSLayoutConstraint(item: self as Any, attribute: .bottom, relatedBy: .equal,
                                        toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        
        let trailing = NSLayoutConstraint(item: self as Any, attribute: .trailing, relatedBy: .equal,
                                          toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([top, leading, bottom, trailing])
    }
}

extension UIView {
    
    func addShadow() {
        layer.borderWidth = 0.3
        layer.borderColor = UIColor.lightGray.cgColor
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.3
        layer.shadowOffset = .zero
        layer.shadowRadius = 6
    }
}

extension UIView {
    func addTopBorder(_ color: UIColor, height: CGFloat) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(border)
            border.addConstraint(NSLayoutConstraint(item: border,
                                                    attribute: NSLayoutConstraint.Attribute.height,
                                                    relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: nil,
                attribute: NSLayoutConstraint.Attribute.height,
                multiplier: 1, constant: height))
            self.addConstraint(NSLayoutConstraint(item: border,
                                                  attribute: NSLayoutConstraint.Attribute.top,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.top,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.leading,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.leading,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.trailing,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.trailing,
                multiplier: 1, constant: 0))
        }

        func addBottomBorder(_ color: UIColor, height: CGFloat) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(border)
            border.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.height,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: nil,
                attribute: NSLayoutConstraint.Attribute.height,
                multiplier: 1, constant: height))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.bottom,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.bottom,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.leading,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.leading,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.trailing,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.trailing,
                multiplier: 1, constant: 0))
        }

        func addLeftBorder(_ color: UIColor, width: CGFloat) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(border)
            border.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.width,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: nil,
                attribute: NSLayoutConstraint.Attribute.width,
                multiplier: 1, constant: width))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.leading,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.leading,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.bottom,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.bottom,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.top,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.top,
                multiplier: 1, constant: 0))
        }

        func addRightBorder(_ color: UIColor, width: CGFloat) {
            let border = UIView()
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(border)
            border.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.width,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: nil,
                attribute: NSLayoutConstraint.Attribute.width,
                multiplier: 1, constant: width))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.trailing,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.trailing,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.bottom,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.bottom,
                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: border,
                attribute: NSLayoutConstraint.Attribute.top,
                relatedBy: NSLayoutConstraint.Relation.equal,
                toItem: self,
                attribute: NSLayoutConstraint.Attribute.top,
                multiplier: 1, constant: 0))
        }
}

extension UISearchBar {
    func setPlaceholderTextColorTo(color: UIColor)
    {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = color
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = color
    }
}

extension UIAlertAction {

    /// Image to display left of the action title
    var actionImage: UIImage? {
        get {
            if self.responds(to: Selector(Constants.imageKey)) {
                return self.value(forKey: Constants.imageKey) as? UIImage
            }
            return nil
        }
        set {
            if self.responds(to: Selector(Constants.imageKey)) {
                self.setValue(newValue, forKey: Constants.imageKey)
            }
        }
    }

    private struct Constants {
        static var imageKey = "image"
    }
}
