//
//  UIColor+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(rgb: UInt, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgb & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
    
    convenience init(_ red: UInt, _ green: UInt, _ blue: UInt, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat(red)/255.0,
                  green: CGFloat(green)/255.0,
                  blue: CGFloat(blue)/255.0,
                  alpha: alpha)
    }
    
    convenience init(string: String) {
        let nums = string.components(separatedBy: ",")
        if nums.count == 3 {
            self.init(red: CGFloat(NSString(string: nums[0]).floatValue),
                      green: CGFloat(NSString(string: nums[1]).floatValue),
                      blue: CGFloat(NSString(string: nums[2]).floatValue), alpha: 1.0)
        } else {
            self.init()
        }
    }
    
    /** This function calculates a new color by blending the two colors.
     A percent of 0.0 gives the "from" color,
     a percent of 1.0 gives the "to" color.
     Any other percent gives an appropriate color in between the two */
    func blend(from: UIColor, to: UIColor, percent: Double) -> UIColor {
        var fR: CGFloat = 0.0
        var fG: CGFloat = 0.0
        var fB: CGFloat = 0.0
        var tR: CGFloat = 0.0
        var tG: CGFloat = 0.0
        var tB: CGFloat = 0.0
        
        from.getRed(&fR, green: &fG, blue: &fB, alpha: nil)
        to.getRed(&tR, green: &tG, blue: &tB, alpha: nil)
        
        let dR = tR - fR
        let dG = tG - fG
        let dB = tB - fB
        
        let rR = fR + dR * CGFloat(percent)
        let rG = fG + dG * CGFloat(percent)
        let rB = fB + dB * CGFloat(percent)
        
        return UIColor(red: rR, green: rG, blue: rB, alpha: 1.0)
    }
    
    func asImage() -> UIImage? {
        UIGraphicsBeginImageContext(CGSize(width: 50, height: 50))
        let context = UIGraphicsGetCurrentContext()
        self.setFill()
        context?.fill(CGRect(x: 0, y: 0, width: 50, height: 50))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    // Adapted from Stack Overflow answer by David Crow http://stackoverflow.com/a/43235
    // Question: Algorithm to randomly generate an aesthetically-pleasing color palette by Brian Gianforcaro
    // Method randomly generates a pastel color, and optionally mixes it with another color
    func generateRandomPastelColor(withMixedColor mixColor: UIColor?) -> UIColor {
        // Randomly generate number in closure
        let randomColorGenerator = { ()-> CGFloat in
            CGFloat(arc4random() % 256 ) / 256
        }
        
        var red: CGFloat = randomColorGenerator()
        var green: CGFloat = randomColorGenerator()
        var blue: CGFloat = randomColorGenerator()
        
        // Mix the color
        if let mixColor = mixColor {
            var mixRed: CGFloat = 0, mixGreen: CGFloat = 0, mixBlue: CGFloat = 0;
            mixColor.getRed(&mixRed, green: &mixGreen, blue: &mixBlue, alpha: nil)
            
            red = (red + mixRed) / 2;
            green = (green + mixGreen) / 2;
            blue = (blue + mixBlue) / 2;
        }
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}

extension UIColor {
    static var appLightBlue: UIColor { return UIColor(rgb: 0x33a2dc) }
    static var appGray: UIColor { return UIColor(rgb: 0xa19f9f) }
    static var appGreen: UIColor { return UIColor(rgb: 0x92bf3a)}
    static var dashBlack: UIColor { return UIColor(rgb: 0x4D212121)}
    static var dashDarkBlue: UIColor { return UIColor(rgb: 0x445056)}
    static var dashLightGray: UIColor { return UIColor(rgb: 0xB1B5B7)}
    static var dashRed: UIColor { return UIColor(rgb: 0xE10514)}
    static var lightGrey: UIColor { return UIColor(rgb: 0x424B50)}
    static var darkGrey: UIColor { return UIColor(rgb: 0x91969B)}
    static var moduleBackground: UIColor { return UIColor(rgb: 0x1A202D, alpha: 0.89)}
    static var lightestGray: UIColor { return UIColor(rgb: 0xE7E9EA)}
    /// A89B70
    static var swampyGreen: UIColor { return UIColor(rgb: 0xA89B70)}
    /// 545F65
    static var timerGray: UIColor { return UIColor(rgb: 0x545F65) }
    
    static let mainBgColor = UIColor(56, 57, 71)
    static let reportCellBg = UIColor(56, 59, 74)
    static let mainBgColorDark = UIColor(32, 35, 44)
    static let tableViewBgColor = UIColor(32, 35, 43)
    static let publickOfferColor = UIColor(40, 40, 46)
    
    static let bgColor2 = UIColor(49, 49, 56, alpha: 0.1)
    static let bgColor = UIColor(49, 49, 56)
    
    static let plusLineGradientStartColor = UIColor(red: 160.0/255.0, green: 222.0/255.0, blue: 211.0/255.0, alpha: 1.0)
    static let plusLineGradientEndColor = UIColor(red: 52.0/255.0, green: 187.0/255.0, blue: 164.0/255.0, alpha: 1.0)
    static let minusLineGradientStartColor = UIColor(red: 255.0/255.0, green: 111.0/255.0, blue: 109.0/255.0, alpha: 1.0)
    static let minusLineGradientEndColor = UIColor(red: 219.0/255.0, green: 131.0/255.0, blue: 177.0/255.0, alpha: 1.0)
    
    static func tableViewBgColorGradient(_ alpha: CGFloat) -> UIColor {
        return UIColor(32, 35, 43, alpha: alpha)
    }
    
    static func pieChartsColors() -> [[UIColor]] {
        return [
            [UIColor(213, 39, 152), UIColor(105, 77, 226)],
            [UIColor(245, 102, 111), UIColor(183, 60, 50)],
            [UIColor(69, 207, 181), UIColor(35, 133, 144)],
            [UIColor(230, 208, 60), UIColor(191, 99, 76)],
            [UIColor(117, 51, 239), UIColor(121, 24, 139)]]
    }
    
    static func pieChartHeartGradientColors() -> [CGColor] {
        return [UIColor(red: 70.0/255.0, green: 75.0/255.0, blue: 107.0/255.0, alpha: 1.0).cgColor,
                UIColor(red: 55.0/255.0, green: 58.0/255.0, blue: 73.0/255.0, alpha: 1.0).cgColor]
    }
    
    static func monthFilterColor() -> UIColor {
        return blueColor
    }
    
    static let blueColor = UIColor(0, 251, 254)
    static let greenColor = UIColor(53, 171, 154)
    static let orangeColor = UIColor(238, 154, 119)
    static let redColor = UIColor(255, 124, 118)
    static let redColorError = UIColor(255, 23, 23)
    static let grayTextColor = UIColor(225, 225, 225)
    static let blueGrey = UIColor(132, 142, 159)
}


// Ziraat Colors

extension UIColor {
     /// rgba: 68,80,86,1;  hex: #445056
    static let buttonBlack = UIColor(68,80,86)
    
    /// rgba: 225,5,20,1; 
    static let buttonRed = UIColor(225,5,20)
    
     /// rgba: 196, 196, 196, 1;
    static let buttonGray = UIColor(196, 196, 196)

}
