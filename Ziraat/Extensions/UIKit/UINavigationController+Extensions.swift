//
//  UINavigationController+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    /** returns false if class is not found in navigation stack */
    func popToViewController(ofClass: AnyClass, animated: Bool = true, checkFromTop: Bool = true) -> Bool {
        var viewController: UIViewController
        if checkFromTop {
            guard let vc = viewControllers.filter({$0.isKind(of: ofClass)}).first else { return false }
            viewController = vc
            
        } else {
            guard let vc = viewControllers.filter({$0.isKind(of: ofClass)}).last else { return false }
            viewController = vc
        }
        
        popToViewController(viewController, animated: animated)
        return true
    }
    
    func popViewControllers(viewsToPop: Int, animated: Bool = true) {
        if viewControllers.count > viewsToPop {
            let vc = viewControllers[viewControllers.count - viewsToPop - 1]
            popToViewController(vc, animated: animated)
        }
    }
    
    func pushViewControllerRetro(_ viewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .push
        transition.subtype = .fromRight
        view.layer.add(transition, forKey: nil)
        pushViewController(viewController, animated: false)
    }
    
    func popViewControllerRetro() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .push
        transition.subtype = .fromLeft
        view.layer.add(transition, forKey: nil)
        popViewController(animated: false)
    }
    
    override open var childForStatusBarStyle: UIViewController? {
            return self.topViewController
    }
    
    func transparentBackgorund() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = .clear
            navBarAppearance.shadowColor = nil
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.standardAppearance = navBarAppearance
        } else {
            navigationController?.navigationBar.isTranslucent = true
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
        }
    }
    
}
