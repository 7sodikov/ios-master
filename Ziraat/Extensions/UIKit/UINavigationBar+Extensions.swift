//
//  UINavigationBar+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension UINavigationBar {
    func setGradientBackground(colors: [UIColor]) {
        var updatedFrame = bounds
        updatedFrame.size.height += 20 // 20 is status bar height
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        setBackgroundImage(gradientLayer.convertToImage(), for: UIBarMetrics.default)
    }
    
    func hideNavigationBar(_ hide: Bool) {
//        isTranslucent = true
        
        if hide {
            setBackgroundImage(UIImage(), for: .default)
            shadowImage = UIImage()
        } else {
            setGradientBackground(colors: [ColorConstants.navBarStartColor, ColorConstants.navBarEndColor])
        }
    }
}
