//
//  UIFont+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

//for family in UIFont.familyNames {
//    let sName: String = family as String
//    print("family: \(sName)")
//    for name in UIFont.fontNames(forFamilyName: sName) {
//        print("name: \(name as String)")
//    }
//}

extension UIFont {
    
    class func regular(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "CirceRounded-Regular5", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func bold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "CirceRounded-Regular4", size: size) ?? UIFont.systemFont(ofSize: size, weight: .bold)
    }
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        if let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits)) {
            return UIFont(descriptor: descriptor, size: 0)
        }
        return UIFont.regular(ofSize: UIFont.systemFontSize)
    }
}

extension UIFont {
    static func sfFontRegular(_ ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Regular", size: ofSize) ?? UIFont.systemFont(ofSize: ofSize, weight: UIFont.Weight.regular)
    }
    
    static func sfFontRegularItalic(_ ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-RegularItalic", size: ofSize) ?? UIFont.systemFont(ofSize: ofSize, weight: UIFont.Weight.regular)
    }
    
    static func sfFontMedium(_ ofSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Medium", size: ofSize) ?? UIFont.systemFont(ofSize: ofSize, weight: UIFont.Weight.medium)
    }
}

public extension UIFont {
    
    enum GothamFontWeight: String {
//        case black = "Black"
//        case book = "Book"
//        case bold = "Bold"
//        case heavy = "Heavy"
//        case light = "Light"
//        case medium = "Medium"
//        case regular = "Regular"
//        case semibold = "Semibold"
//        case thin = "Thin"
//        case ultralight = "Ultralight"
        
        case book = "Book"
        case bold = "Bold"
        case medium = "Medium"
    }
    
    static func gothamNarrow(size: CGFloat, _ weight: GothamFontWeight) -> UIFont {
        let suitableSize = Adaptive.val(size)
        guard let font = UIFont(name: "GothamNarrow" + "-" + weight.rawValue, size: suitableSize) else {
            return UIFont(size: size, weight: .bold) ?? UIFont.boldSystemFont(ofSize: suitableSize)
        }
        return font
    }
    
    static func gothamBoldVersion(size: CGFloat, _ weight: GothamFontWeight) -> UIFont {
        let suitableSize = Adaptive.val(size)
        guard let font = UIFont(name: "SFUIDisplay-" + weight.rawValue, size: suitableSize) else {
            fatalError("Could not find font >> \(weight.rawValue) from Gotham Narrow family")
        }
        return font
    }
    
    convenience init?(size: CGFloat, weight: GothamFontWeight = .medium) {
        self.init(name: weight.rawValue, size: size)
    }
}

