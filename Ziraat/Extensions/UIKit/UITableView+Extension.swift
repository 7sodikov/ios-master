//
//  UITableView+Extension.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

public extension UITableView {
 
    func register(_ cellType: TableViewCell.Type) {
        register(cellType, forCellReuseIdentifier: cellType.uid)
    }
    
    func reusable<CellType: TableViewCell>(_ cellType: CellType.Type, for indexPath: IndexPath) -> CellType {
        dequeueReusableCell(withIdentifier: cellType.uid, for: indexPath) as! CellType
    }
    
}

public extension UITableViewCell {
    
    static var uid: String {
        "\(String(describing: self))UniqueCellIdentifier"
    }
    
    func removeSectionSeparators() {
        for subview in subviews {
            if subview != contentView && subview.frame.width == frame.width {
                subview.removeFromSuperview()
            }
        }
    }
    
}

public extension UITableViewHeaderFooterView {
    
    static var uid: String {
        "\(String(describing: self))UniqueCellIdentifier"
    }
}
