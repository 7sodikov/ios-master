//
//  String+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension String {
//    func sha512Hex() -> String {
//        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
//        if let data = self.data(using: String.Encoding.utf8) {
//            let value =  data as NSData
//            CC_SHA512(value.bytes, CC_LONG(data.count), &digest)
//        }
//        var digestHex = String.emptyString
//        for index in 0..<Int(CC_SHA512_DIGEST_LENGTH) {
//            digestHex += String(format: "%02x", digest[index])
//        }
//        return digestHex
//    }
//    
//    func sha256() -> String {
//        var hash = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
//        if let data = self.data(using: String.Encoding.utf8) {
//            data.withUnsafeBytes {
//                _ = CC_SHA256($0.baseAddress, CC_LONG(data.count), &hash)
//            }
//        }
//        var digestHex = String.emptyString
//        for index in 0..<Int(CC_SHA256_DIGEST_LENGTH) {
//            digestHex += String(format: "%02x", hash[index])
//        }
//        return digestHex
//    }
//    
//    func sha512() -> String {
//        var hash = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
//        if let data = self.data(using: String.Encoding.utf8) {
//            data.withUnsafeBytes {
//                _ = CC_SHA512($0.baseAddress, CC_LONG(data.count), &hash)
//            }
//        }
//        var digestHex = String.emptyString
//        for index in 0..<Int(CC_SHA512_DIGEST_LENGTH) {
//            digestHex += String(format: "%02x", hash[index])
//        }
//        return digestHex
//    }
    
//    func sha256() -> String {
//        let string = self.data(using: String.Encoding.utf8)?.castToCPointer() ?? nil
//        let context = UnsafeMutablePointer<CC_SHA256_CTX>.allocate(capacity: 1)
//        var digest = Array<UInt8>(repeating:0, count:Int(CC_SHA256_DIGEST_LENGTH))
//        CC_SHA256_Init(context)
//        CC_SHA256_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
//        CC_SHA256_Final(&digest, context)
//        context.deallocate()
//        var hexString = ""
//        for byte in digest {
//            hexString += String(format:"%02x", byte)
//        }
//        return hexString
//    }
    
//    func sha1() -> String {
//        let data = self.data(using: String.Encoding.utf8)!
//        var digest = [UInt8](repeating: 0, count: Int(CC_SHA1_DIGEST_LENGTH))
//        data.withUnsafeBytes {
//            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
//        }
//        let hexBytes = digest.map { String(format: "%02hhx", $0) }
//        return hexBytes.joined()
//    }
//
//    func md5() -> String {
//        let str = self.cString(using: String.Encoding.utf8)
//        let strLen = CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8))
//        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
//        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
//
//        CC_MD5(str!, strLen, result)
//
//        let hash = NSMutableString()
//        for i in 0..<digestLen {
//            hash.appendFormat("%02x", result[i])
//        }
//
//        result.deallocate()//(capacity: digestLen)
//
//        return String(format: hash as String)
//    }
    
    
    func luhnCheck() -> Bool {
        var sum = 0
        let digitStrings = String(self.reversed()) //String(number.reversed())
        
        for (index, char) in digitStrings.enumerated() {
            guard let digit = Int(String(char)) else { return false }
            let odd = index % 2 == 1
            
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
    
    func date(fromFormat format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
    
    func split(every: Int, backwards: Bool = false) -> [String] {
        var result = [String]()

        for i in stride(from: 0, to: self.count, by: every) {
            switch backwards {
            case true:
                let endIndex = self.index(self.endIndex, offsetBy: -i)
                let startIndex = self.index(endIndex, offsetBy: -every, limitedBy: self.startIndex) ?? self.startIndex
                result.insert(String(self[startIndex..<endIndex]), at: 0)
            case false:
                let startIndex = self.index(self.startIndex, offsetBy: i)
                let endIndex = self.index(startIndex, offsetBy: every, limitedBy: self.endIndex) ?? self.endIndex
                result.append(String(self[startIndex..<endIndex]))
            }
        }

        return result
    }
    
    var alphanumeric: String {
        return components(separatedBy: CharacterSet.alphanumerics.inverted).joined()
    }
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    var digitsAndDots: String {
        return components(separatedBy: CharacterSet(charactersIn: "0123456789.").inverted).joined()
    }
    
    var withOnlyOneDot: String {
        var foundFirstDot = false
        var result = ""
        for char in self {
            if char == "." {
                if foundFirstDot {
                    continue
                }
                foundFirstDot = true
            }
            result.append(char)
        }
        return result
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func replace(of: String, with: String) -> String {
        return self.replacingOccurrences(of: of, with: with)
    }
    
    subscript(start: Int, length: Int) -> String {
        let startInd = index(startIndex, offsetBy: start, limitedBy: endIndex) ?? endIndex
        var endInd = index(startInd, offsetBy: length, limitedBy: endIndex) ?? endIndex
        if length == 0 {
            endInd = endIndex
        }
        let range = startInd..<endInd
        return String(self[range])
    }
    
//    func toAttributed(_ size: CGFloat) -> NSAttributedString {
//        return NSAttributedString(string: self, attributes: [NSAttributedString.Key.font: UIFont.appFont(size)])
//    }
    
    func separate(every: Int, with separator: String) -> String {
        return String(stride(from: 0, to: Array(self).count, by: every).map { Array(Array(self)[$0..<min($0 + every, Array(self).count)])}
            .joined(separator: separator))
    }
    
    subscript(i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }
    
    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
    
    func size(withConstrainedWidth width: CGFloat, font: UIFont) -> CGSize {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return CGSize(width: ceil(boundingBox.width), height: ceil(boundingBox.height))
    }
    
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func matches(regEx: String?) -> Bool {
        if let regEx = regEx {
            return self.range(of: regEx, options: .regularExpression, range: nil, locale: nil) != nil
        }
        return true
    }
    
    var isEmail: Bool {
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, count))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    var urlEncoded : String? {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    
    var urlDecoded : String? {
        return self.removingPercentEncoding
    }
    
    var dataFromBase64: Data? {
        return Data(base64Encoded: self, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
    }
    
    var imageFromBase64: UIImage? {
        guard let dataDecoded = dataFromBase64 else {
            return nil
        }
        return UIImage(data: dataDecoded)
    }
    
//    func matchesInString(_ string: String, pattern: String) -> [NSTextCheckingResult] {
//        return try!
//            NSRegularExpression(pattern: pattern, options: .caseInsensitive)
//                .matches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: string.count))
//    }
//    
//    func matchesInString(pattern: String) -> Bool {
//        let regex = try! NSRegularExpression(pattern: pattern, options: .caseInsensitive)
//        let matchResults = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSRange(location: 0, length: self.count))
//        return matchResults.isEmpty
//    }
//    
//    func verifyWhileTyping(pattern: String) -> Bool {
//        let predicate = NSPredicate(format: "SELF MATCHES %@", pattern)
//        return predicate.evaluate(with: self)
//    }
    
    var makeReadableAmount: String {
        let price: String
        let storedReminder: String
        
        var imutableSelft = self
        
        if imutableSelft.hasPrefix("."), let index = imutableSelft.firstIndex(of: ".") {
            imutableSelft.insert("0", at: index)
        }
        
        if self.contains(".") {
            let separated = imutableSelft.split(separator: ".")
            price = String(separated[0])
            storedReminder = separated.count > 1 ? String(separated[1]) : "00"
        } else {
            price = imutableSelft
            storedReminder = String.init()
        }
        
        guard price.count > 3 else { return self }
        
        var count = 0
        var amount = String.init()
        let reversedPrice = String(price.reversed())
        (0..<reversedPrice.count).forEach {
            count += 1
            amount += String(reversedPrice[price.index(reversedPrice.startIndex, offsetBy: $0)])
            
            if count == 3 && $0 != reversedPrice.count-1 {
                count = 0
                amount += " "
            }
        }
        
        let readableAmount = String(amount.reversed())
        return (storedReminder.isEmpty) ? readableAmount : "\(readableAmount).\(storedReminder)"
    }
    
    var makeReadableCardNumber: String {
        var formattedCardNumber = String()
        for (index, character) in self.enumerated() {
            formattedCardNumber += String(character)
            formattedCardNumber += ([3, 7, 11].contains(index)) ? " " : ""
        }
        return formattedCardNumber
    }
    
    func insertSeparator(_ separatorString: String, atEvery n: Int) -> String {
        guard 0 < n else { return self }
        return self.enumerated().map({String($0.element) + (($0.offset != self.count - 1 && $0.offset % n ==  n - 1) ? "\(separatorString)" : "")}).joined()
    }
    
    mutating func insertedSeparator(_ separatorString: String, atEvery n: Int) {
        self = insertSeparator(separatorString, atEvery: n)
    }
    
}

extension String {
    static let emptyString = ""
    static let spaceString = " "
    static let slashString = "/"
    
    static func checkSize(textWidth: CGFloat, width: CGFloat, size: CGFloat, value: String, bold: Bool) -> CGFloat {
        var valueFont = (bold ? UIFont.bold(ofSize: Adaptive.val(size)) : UIFont.regular(ofSize: Adaptive.val(size)))
        var txtSize = size
        if textWidth > width {
            txtSize -= 1
            valueFont = (bold ? UIFont.bold(ofSize: Adaptive.val(size)) : UIFont.regular(ofSize: Adaptive.val(size)))
            let valueSize = value.size(withConstrainedWidth: .infinity, font: valueFont)
            return checkSize(textWidth: valueSize.width, width: width, size: txtSize, value: value, bold: bold)
        } else {
            return txtSize
        }
    }
    
    static func clearString(str: String) -> String {
        var res = str
        res = res.trimmingCharacters(in: .whitespacesAndNewlines)
        res = res.replacingOccurrences(of: " ", with: "")
        res = res.replacingOccurrences(of: ")", with: "")
        res = res.replacingOccurrences(of: "(", with: "")
        res = res.replacingOccurrences(of: "-", with: "")
        res = res.replacingOccurrences(of: "+", with: "")
        res = res.replacingOccurrences(of: "/", with: "")
        return res
    }
    
    static func divideAmountString(text: String, char: String = " ") -> String {
        if text == "0" || text.isEmpty {
            return "\(text)"
        }
        
        var result = text
        if text.count > 1 && text[0..<1] == "0" {
            result = text[1..<text.count]
        }
        var count = 0
        var str = String.emptyString
        for i in (0..<result.count).reversed() {
            if count == 3 {
                str.append(char)
                count = 0
            }
            str.append(result[i])
            count += 1
        }
        return String(str.reversed())
    }
    
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    var withoutWhiteSpace: String {
        return self.replacingOccurrences(of: " ", with: "")
    }
    
    var addPlusPrefix: String {
        if self.hasPrefix("+") {
            return self
        } else {
            return ("+" + self)
        }
    }
    
    var addCountryCode: String {
        let hasPlusPrefix = self.hasPrefix("+")
        if hasPlusPrefix, self.hasPrefix("+998") {
            return self
        } else if self.hasPrefix("998"), self.count == 12 {
            return self.addPlusPrefix
        }
        else {
            return ("+998" + self)
        }
    }
    
    var makeUZPhoneFormat: String {
        var formattedPhone = String()
        for (index, element) in self.addCountryCode.enumerated() {
            formattedPhone += String(element)
            formattedPhone += ([3, 5, 8, 10].contains(index)) ? " " : ""
        }
        return formattedPhone
    }
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension Data {
    func castToCPointer<T>() -> T {
        return self.withUnsafeBytes(\.pointee)
    }
}

extension NumberFormatter {
    convenience init(numberStyle: NumberFormatter.Style, groupingSeparator: String?, decimalSeparator: String?) {
        self.init()
        self.set(numberStyle: numberStyle, groupingSeparator: groupingSeparator, decimalSeparator: decimalSeparator)
    }

    convenience init(numberStyle: NumberFormatter.Style, locale: Locale) {
        self.init()
        self.set(numberStyle: numberStyle, locale: locale)
    }

    func set(numberStyle: NumberFormatter.Style, groupingSeparator: String?, decimalSeparator: String?) {
        self.locale = nil
        self.numberStyle = numberStyle
        self.groupingSeparator = groupingSeparator
        self.decimalSeparator = decimalSeparator
        self.maximumFractionDigits = 2
        self.minimumFractionDigits = 2
    }

    func set(numberStyle: NumberFormatter.Style, locale: Locale?) {
        self.numberStyle = numberStyle
        self.locale = locale
    }
}

extension Numeric {
    func format(formatter: NumberFormatter) -> String? {
        if let num = self as? NSNumber { return formatter.string(from: num) }
        return nil
    }
}

