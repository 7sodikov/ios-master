//
//  Dictionary+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

func += <K, V> (left: inout [K: V], right: [K: V]) {
    for (k, v) in right {
        left[k] = v
    }
}

extension Dictionary where Key == String, Value == Any? {
    func object(for key: String) -> [String: Any]? {
        return self[key] as? [String: Any]
    }
    
    func any(for key: String) throws -> Any {
        guard let val = self[key] else { throw AppError.dictObjectNotFound(key: key) }
        return val as Any
    }
    
    func obj(for key: String) throws -> [String: Any?] {
        if let val = self[key] {
            guard let v = val as? [String: Any?] else { throw AppError.unexpectedStructure(key: key) }
            return v
        } else {
            throw AppError.dictObjectNotFound(key: key)
        }
    }
    
    func arrayOfObjs(for key: String) throws -> [[String: Any?]] {
        guard let val = self[key] as? [[String: Any?]] else { throw AppError.dictObjectNotFound(key: key) }
        return val
    }
    
    func arrayOfStrings(for key: String) throws -> [String] {
        guard let val = self[key] as? [String] else { throw AppError.dictObjectNotFound(key: key) }
        return val
    }
    
    func arrayOfAnies(for key: String) throws -> [Any] {
        guard let val = self[key] as? [Any] else { throw AppError.dictObjectNotFound(key: key) }
        return val
    }
    
    func string(for key: String) throws -> String {
        guard let val = self[key] as? String else { throw AppError.dictObjectNotFound(key: key) }
        return val
    }
    
    func number(for key: String) throws -> NSNumber {
        guard let val = self[key] as? NSNumber else { throw AppError.dictObjectNotFound(key: key) }
        return val
    }
    
    func long(for key: String) throws -> CLong {
        guard let val = self[key] as? CLong else { throw AppError.dictObjectNotFound(key: key) }
        return val
    }
    
    func float(for key: String) throws -> Float {
        guard let val = self[key] as? Float else { throw AppError.dictObjectNotFound(key: key) }
        return val
    }
    
    var toString: String? {
        do {
            let data1 = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            return convertedString
        } catch {
            return nil
        }
    }
    
    /** Returns Strings' array of values for searching key.
     Method iterates through each nested dictionary objects and gathers values for searching key.*/
    func getAllValues(for searchingKey: String) -> [String] {
        func getAllValues(from array: [Any]) -> [String] {
            var result: [String] = []
            for value in array {
                if let val = value as? [String: Any?] {
                    result.append(contentsOf: val.getAllValues(for: searchingKey))
                    
                } else if let val = value as? [Any] {
                    result.append(contentsOf: getAllValues(from: val))
                }
            }
            return result
        }
        
        var result: [String] = []
        for (key, value) in self {
            if let val = value as? [String: Any?] {
                result.append(contentsOf: val.getAllValues(for: searchingKey))
                
            } else if let val = value as? [Any] {
                result.append(contentsOf: getAllValues(from: val))
                
            } else if let val = value as? String, key == searchingKey {
                result.append(val)
            }
        }
        return result
    }
}

// for Optional Fields
extension Dictionary where Key == String, Value == Any {
    // for dates
    mutating func update(_ value: CLong?, forKey: String) {
        if let val = value {
            updateValue(val, forKey: forKey)
        }
    }
    
    // for strings
    mutating func update(_ value: String, forKey: String) {
        if !value.isEmpty {
            updateValue(value, forKey: forKey)
        }
    }
    
    // for All
    mutating func updateAny(_ value: Any?, forKey: String) {
        if let val = value {
            updateValue(val, forKey: forKey)
        }
    }
}
