//
//  Int64+Extensions.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

extension Int64 {
    func currencyFormattedStr() -> String? {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.groupingSize = 3
        formatter.groupingSeparator = " "
        return formatter.string(for: self)
    }
}

extension Double {
    func currencyFormattedStr() -> String? {
        let formatter = NumberFormatter()
//        formatter.usesGroupingSeparator = true
//        formatter.groupingSize = 3
//        formatter.groupingSeparator = " "
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        
        let number = NSNumber(value: self)
        return formatter.string(from: number)
    }
}
