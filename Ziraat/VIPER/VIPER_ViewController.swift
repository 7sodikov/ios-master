//
//  VIPER_ViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol VIPER_DisplayLogic: VIPER_ViewInstaller {
    
}


class VIPER_ViewController: UIViewController, VIPER_DisplayLogic  {
    
    
    var mainView: UIView { self.view }
    var presenter: VIPER_PresentationLogic!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        presenter.viewWillAppear(animated)
    }
    
    
}



