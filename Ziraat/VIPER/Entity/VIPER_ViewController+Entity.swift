//
//  VIPER_ViewController+Entity.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol VIPER_Base {
    typealias Section = VIPER_ViewController.Section
    typealias Row = VIPER_ViewController.Row
}


extension VIPER_ViewController {
    
    enum Section: Int, CaseIterable {
        case row1
        case row2
    }
    
    enum Row {
        
    }
    
    // all entity need to be here which they were userd in View controller
    
    struct VIPER {
        
    }
}
