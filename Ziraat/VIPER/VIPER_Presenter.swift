//
//  VIPER_Presenter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol VIPER_PresentationLogic: PresentationProtocol, VIPER_Base, TablePresentationLogic {
    
}


class VIPER_Presenter: VIPER_PresentationLogic {
    
    private unowned let view: VIPER_DisplayLogic
    private let interactor: VIPER_BusinessLogic
    private let router: VIPER_RoutingLogic
    
    private var controller: VIPER_ViewController { view as! VIPER_ViewController }
    
    
    init(view: VIPER_DisplayLogic, interactor: VIPER_BusinessLogic, router: VIPER_RoutingLogic) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        
    }
    
    func viewWillAppear(_ animated: Bool) {
        
    }
}


// MARK: -TablePresentationLogic

extension VIPER_Presenter {
    
    func numberOfSections(in table: UITableView) -> Int {
        1
    }
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel {
        .init()
    }
    
}
