//
//  VIPER_Router.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol VIPER_RoutingLogic {
    
}


class VIPER_Router: BaseRouter, VIPER_RoutingLogic {
    
    
    init() {
        let controller = VIPER_ViewController()
        super.init(viewController: controller)
        
        let interactor = VIPER_Interactor()
        let presenter = VIPER_Presenter(view: controller, interactor: interactor, router: self)
        controller.presenter = presenter
    }
    
    
    
}
