//
//  VIPER_ViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol VIPER_ViewInstaller: ViewInstaller {
    
    
    
}

extension VIPER_ViewInstaller {
    
    func initSubviews() {
        
    }
    
    func embedSubviews() {
        
    }
    
    func addSubviewsConstraints() {
        
    }
    
}
