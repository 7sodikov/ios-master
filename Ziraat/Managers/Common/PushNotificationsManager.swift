//
//  PushNotificationsManager.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import FirebaseMessaging

class PushNotificationsManager {
    static func subscribeToTopic() {
        if LocalizationManager.language == Language.english {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_EN")
        } else if LocalizationManager.language == Language.russian {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_RU")
        } else if LocalizationManager.language == Language.uzbek {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_UZ")
        } else {
            Messaging.messaging().subscribe(toTopic: "IOS_TOPIC_TR")
        }
    }
    
    static func unsubscribeFromTopic() {
        Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_EN")
        Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_RU")
        Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_UZ")
        Messaging.messaging().unsubscribe(fromTopic: "IOS_TOPIC_TR")
    }
    
    static var fcmToken: String? {
        get {
            return UDManager.fcmtoken
        }
        set {
            UDManager.fcmtoken = newValue
        }
    }
}
