//
//  Adaptive.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

let screenSize = UIScreen.main.bounds.size
fileprivate let originalDesignSize = CGSize(width: 375, height: 812) // size of designed screen iPhoneX
fileprivate let newAvailableSize = Adaptive.size(for: originalDesignSize.width/originalDesignSize.height, constrainedToSize: screenSize)
fileprivate let newSizeVariation = newAvailableSize.width/originalDesignSize.width // you can use height/height instead of width/width. Result will be same

class Adaptive {
    
    /**
     It creates rectangular size, sides of which satisfies with the given "aspectRatio".
     Calculated (returned) size fits given "constrainedToSize" value and DOESN'T exceed it.
     Returned value behaves same as a UIImageView with "scaleAspectFit" contentMode value.
     
     @param aspectRatio Equal to width/height.
     */
    fileprivate static func size(for aspectRatio: CGFloat, constrainedToSize: CGSize) -> CGSize {
        var w = constrainedToSize.height * aspectRatio
        if w > constrainedToSize.width {
            w = constrainedToSize.width
        }
        
        let h = w/aspectRatio
        return CGSize(width: w, height: h)
    }
    
    static func val(_ value: CGFloat) -> CGFloat {
        return value * newSizeVariation
    }
    
    static func size(_ width: CGFloat, _ height: CGFloat) -> CGSize {
        return CGSize(width: val(width), height: val(height))
    }
    
    static func frame(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> CGRect {
        return CGRect(x: val(x), y: val(y), width: val(width), height: val(height))
    }
    
    static func edges(top: CGFloat, left: CGFloat, bottom: CGFloat, right: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: val(top), left: val(left), bottom: val(bottom), right: val(right))
    }
}
