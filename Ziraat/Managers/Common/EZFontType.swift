//
//  EZFontType.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

enum EZFontType: String {
    case black = "Black"
    case book = "Book"
    case bold = "Bold"
    case heavy = "Heavy"
    case light = "Light"
    case medium = "Medium"
    case regular = "Regular"
    case semibold = "Semibold"
    case thin = "Thin"
    case ultralight = "Ultralight"
    
    func sfuiDisplay(size: CGFloat) -> UIFont? {
        return UIFont(name: "SFUIDisplay-" + self.rawValue, size: size)
    }
    
    func sfProDisplay(size: CGFloat) -> UIFont? {
        return UIFont(name: "SFProDisplay-" + self.rawValue, size: size)
    }
    
    func sfuiText(size: CGFloat) -> UIFont? {
        return UIFont(name: "SFUIText-" + self.rawValue, size: size)
    }
    
    func sfProText(size: CGFloat) -> UIFont? {
        return UIFont(name: "SFProText-" + self.rawValue, size: size)
    }
    
    func gothamText(size: CGFloat) -> UIFont? {
        return UIFont(name: "Gotham-Narrow-" + self.rawValue, size: size)
    }
    
    func gothamBoldText(size: CGFloat) -> UIFont? {
        return UIFont(name: "Gotham-" + self.rawValue, size: size)
    }
}
