//
//  KeychainManager.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import KeychainAccess

fileprivate struct KeychainKey {
    // "sessionToken"
    static let sessionToken = [40, 43, 32, 32, 6, 29, 26, 16, 10, 24, 6, 28].reveal
    
    // "pin"
    static let pin = [43, 39, 61].reveal
    
    // "graphicKey"
    static let graphicKey = [60, 60, 50, 35, 7, 27, 23, 15, 0, 10].reveal
    
    // "rsaPublicKey"
    static let rsaPublicKey = [41, 61, 50, 3, 26, 16, 24, 45, 6, 56, 6, 11].reveal
    
    // "userName"
    static let userName = [46, 61, 54, 33, 33, 19, 25, 33].reveal
}

class KeychainManager {
    
    // "uz.ziraat.mobile-credentials"
    private static let appKeychainService = [46, 52, 125, 41, 6, 0, 21, 37, 17, 93, 14, 29, 11, 25, 24, 10, 95, 79, 82, 48, 45, 51, 7, 17, 30, 77, 76, 61].reveal
    
    private static let appKeychain = Keychain(service: appKeychainService)
    private static let serverKeychain = Keychain(server: URLConstant.domain, protocolType: .https)
    
    static func clear() {
        logout()
    }
    
    static func logout() {

        try? serverKeychain.removeAll()
        try? appKeychain.remove(pin ?? "")
        try? appKeychain.remove(graphicKey ?? "")
        try? appKeychain.remove(rsaPublicKey ?? "")
    }
    
    static var didLogin: Bool {
        return sessionToken != nil && (pin != nil || graphicKey != nil);
    }
    
    static func actionForFirstAppRun() {
        if !UDManager.isFirstRun {
            clear()
            UDManager.isFirstRun = true
        }
    }
    
    static var sessionToken: String? {
        set(newValue) {
            if let val = newValue {
                serverKeychain[string: KeychainKey.sessionToken] = "Bearer \(val)"
                PushNotificationsManager.subscribeToTopic()
                UDManager.notificationOnOff = true
            } else {
                serverKeychain[KeychainKey.sessionToken] = nil
            }
        } get {
            return serverKeychain[string: KeychainKey.sessionToken]
        }
    }
    
    static var pin: String? {
        set(newValue) {
            if let val = newValue {
                appKeychain[string: KeychainKey.pin] = val
            } else {
                appKeychain[KeychainKey.pin] = nil
            }
        } get {
            return appKeychain[string: KeychainKey.pin]
        }
    }
    
    static var graphicKey: String? {
        set(newValue) {
            if let val = newValue {
                appKeychain[string: KeychainKey.graphicKey] = val
            } else {
                appKeychain[KeychainKey.graphicKey] = nil
            }
        } get {
            return appKeychain[string: KeychainKey.graphicKey]
        }
    }
    
    static var rsaPublicKey: String? {
        set(newValue) {
            if let val = newValue {
                serverKeychain[string: KeychainKey.rsaPublicKey] = val
            } else {
                serverKeychain[KeychainKey.rsaPublicKey] = nil
            }
        } get {
            return serverKeychain[string: KeychainKey.rsaPublicKey]
        }
    }
    
    static var userName: String? {
        set(newValue) {
            if let val = newValue {
                appKeychain[string: KeychainKey.userName] = val
            } else {
                appKeychain[KeychainKey.userName] = nil
            }
        } get {
            return appKeychain[string: KeychainKey.userName]
        }
    }
}
