//
//  UDManager.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import Localize_Swift

fileprivate struct UserDefaultsKey {
    static let phoneNumber = "phoneNumber"
    static let regTimestamp = "regTimestamp"
    static let regImei = "regImei"
    static let hideBalance = "hideBalance"
    static let data = "Data"
    static let deviceID = "deviceID"
    static let language = "language"
    static let theme = "theme"
    static let bioAuthentication = "bioAuthentication"
    static let lastTimeUpdate = "lastTimeUpdate"
    static let rememberMeFlag = "rememberMe" //"REMEMBER_USER"
    static let userName = "userName" //"USER_NAME"
    static let notification = "notification"
    static let fcmToken = "fcmToken"
    static let isFirstRun = "isFirstRun"
}

// Prefix UD is an abbreviation for UserDefaults
class UDManager {
    private static let UD = UserDefaults.standard
    
    static func logout() {        
    }
    
    static var phoneNumber: String? {
        get { return UD.string(forKey: UserDefaultsKey.phoneNumber) }
        set { UD.set(newValue, forKey: UserDefaultsKey.phoneNumber) }
    }
    
    static var datetime: Int {
        get { return UD.integer(forKey: UserDefaultsKey.regTimestamp) }
        set { UD.set(newValue, forKey: UserDefaultsKey.regTimestamp) }
    }
    
    static var notificationOnOff:Bool {
        set {
            UD.set(newValue, forKey: UserDefaultsKey.notification)
            UD.synchronize()
        } get {
            return UD.bool(forKey: UserDefaultsKey.notification)
        }
    }
    
    static var lastUpdatedTime:TimeInterval? {
        get {
            return UserDefaults.standard.double(forKey: UserDefaultsKey.lastTimeUpdate)
        } set(newValue) {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKey.lastTimeUpdate)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var uuid: String {
        get { return UD.string(forKey: UserDefaultsKey.regImei).notNullString }
        set { UD.set(newValue, forKey: UserDefaultsKey.regImei) }
    }
    
    static var deviceID: String {
        get { return UD.string(forKey: UserDefaultsKey.deviceID).notNullString }
        set { UD.set(newValue, forKey: UserDefaultsKey.deviceID) }
    }
    
    static var theme: Int {
        get { return UD.integer(forKey: UserDefaultsKey.theme) }
        set { UD.set(newValue, forKey: UserDefaultsKey.theme) }
    }
    
    // don't remove that action !
    static func clear() {
        UD.removeObject(forKey: UserDefaultsKey.phoneNumber)
        UD.removeObject(forKey: UserDefaultsKey.regTimestamp)
        UD.removeObject(forKey: UserDefaultsKey.regImei)
        UD.removeObject(forKey: UserDefaultsKey.hideBalance)
        UD.removeObject(forKey: UserDefaultsKey.language)
//        UD.removeObject(forKey: UserDefaultsKey.fcmToken)
        UD.removeObject(forKey: UserDefaultsKey.notification)
        UD.removeObject(forKey: UserDefaultsKey.userName)
        UD.removeObject(forKey: UserDefaultsKey.rememberMeFlag)
        resetDefaults()
    }
    
    static func resetDefaults() {
        let dictionary = UD.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key != UserDefaultsKey.fcmToken {
                UD.removeObject(forKey: key)
            }
        }
        UD.synchronize()
    }
    
    static var rememberUser: Bool {
        get {
            return UD.bool(forKey: UserDefaultsKey.rememberMeFlag)
        }
        set(newValue) {
            UD.set(newValue, forKey: UserDefaultsKey.rememberMeFlag)
            UD.synchronize()
        }
    }
    
    static var userName: String? {
        get {
            return UD.string(forKey: UserDefaultsKey.userName)
        }
        set {
            UD.set(newValue, forKey: UserDefaultsKey.userName)
            UD.synchronize()
        }
    }
    
    static var bioAuthentication: Bool {
        set {
            UD.set(!newValue, forKey: UserDefaultsKey.bioAuthentication)
            UD.synchronize()
        } get {
            return !UD.bool(forKey: UserDefaultsKey.bioAuthentication)
        }
    }
    
    static var hideBalance: Bool {
        set {
            UD.set(newValue, forKey: UserDefaultsKey.hideBalance)
            UD.synchronize()
        } get {
            return UD.bool(forKey: UserDefaultsKey.hideBalance)
        }
    }
    
    static var fcmtoken: String? {
        set {
            UD.set(newValue, forKey: UserDefaultsKey.fcmToken)
            UD.synchronize()
        } get {
            return UD.string(forKey: UserDefaultsKey.fcmToken)
        }
    }
    
    static var isFirstRun: Bool {
        get { return UD.bool(forKey: UserDefaultsKey.isFirstRun) }
        set { UD.set(newValue, forKey: UserDefaultsKey.isFirstRun) }
    }
}
