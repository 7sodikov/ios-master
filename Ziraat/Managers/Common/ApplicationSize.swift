//
//  ApplicationSize.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

// Used to store global sizes
struct ApplicationSize {
    static let paddingLeftRight = Adaptive.val(16)
    struct TextField {
        static let height = Adaptive.val(54)
    }
    
    struct Timer {
        static let width = Adaptive.val(190)
    }
}
