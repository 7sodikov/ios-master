//
//  Types.swift
//  AppNetwork
//
//  Created by Botir Nasridinov on 12/12/2019.
//

import Foundation
import Alamofire

/// A value that represents either a success or a failure, including an
/// associated value in each case.
enum OlamResult<Success> {
    /// A success, storing a `Success` value.
    case success(Success)
    
    /// A success, storing a `Error` value.
    case failure(Error)
}

/// A closure that handles a `OlamResult`.
typealias ResultHandler<Success> = (OlamResult<Success>) -> Void

/// A value that represents a http header field to use in `OlamNetwork`'s http requests
enum OlamHTTPHeader {
    case contentType(String)
    case appVersion(String)
    case language(String)
    case deviceModel(String)
    case deviceOSVersion(String)
    case pushToken(String)
    case deviceType(String)
    case deviceUniqueId(String)
    case authToken(String)
    
    var fieldName: String {
        switch self {
        case .contentType(_):
            return "Content-Type"
        case .appVersion(_):
            return "X-Mobile-AppVersion"
        case .language(_):
            return "X-Mobile-Lang"
        case .deviceModel(_):
            return "X-Mobile-Model"
        case .deviceOSVersion(_):
            return "X-Mobile-OSVersion"
        case .pushToken(_):
            return "X-Mobile-PushToken"
        case .deviceType(_):
            return "X-Mobile-Type"
        case .deviceUniqueId(_):
            return "X-Mobile-UID"
        case .authToken(_):
            return "Authorization"
        }
    }
    
    var value: String {
        switch self {
        case .contentType(let value),
             .appVersion(let value),
             .language(let value),
             .deviceModel(let value),
             .deviceOSVersion(let value),
             .pushToken(let value),
             .deviceType(let value),
             .deviceUniqueId(let value),
             .authToken(let value):
            return value
        }
    }
    
    var httpHeader: HTTPHeader {
        switch self {
        case .contentType(let value):
            return .init(name: fieldName, value: value)
        case .appVersion(let value):
            return .init(name: fieldName, value: value)
        case .language(let value):
            return .init(name: fieldName, value: value)
        case .deviceModel(let value):
            return .init(name: fieldName, value: value)
        case .deviceOSVersion(let value):
            return .init(name: fieldName, value: value)
        case .pushToken(let value):
            return .init(name: fieldName, value: value)
        case .deviceType(let value):
            return .init(name: fieldName, value: value)
        case .deviceUniqueId(let value):
            return .init(name: fieldName, value: value)
        case .authToken(let value):
            return .init(name: fieldName, value: value)
        }
    }
}


struct OlamHTTPHeaders {
    private var _headers: [OlamHTTPHeader] = []
    
    var headers: [OlamHTTPHeader] {
        return self._headers
    }
    
    init() {
        self._headers = []
    }
    
    init(headers: [OlamHTTPHeader]) {
        self._headers = headers
    }
    
    mutating func add(_ header: OlamHTTPHeader) {
        self._headers.append(header)
    }
    
    func asHTTPHeaders() -> HTTPHeaders {
        return HTTPHeaders.init(_headers.map({ $0.httpHeader }))
    }
}



