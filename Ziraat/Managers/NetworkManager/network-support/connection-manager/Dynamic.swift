//
//  Dynamic.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 06/01/2020.
//

import Foundation

class Dynamic<T> {
    
    typealias Listener = (T) -> Void
    private var _listener: Listener?
    
    var value: T {
        didSet {
            DispatchQueue.main.async {
                self._listener?(self.value)
            }
        }
    }
    
    init(value: T) {
        self.value = value
    }
    
    /// Added listener will be called, whenever the value changes
    func bind(_ listener: @escaping Listener) {
        self._listener = listener
    }
    
    /// Added listener will be called, whenever the value changes and immediately after closure set
    func bindAndFire(_ listener: Listener?) {
        self._listener = listener
        listener?(value)
    }
}
