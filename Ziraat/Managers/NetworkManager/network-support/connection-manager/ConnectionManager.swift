//
//  ConnectionManager.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 06/01/2020.
//


import Network
import UIKit

/// Class to monitor wi-fi/cellur connection
final class ConnectionManager {
    
    /// The only instance of class
    static let shared: ConnectionDelegate = {
        if #available(iOS 12.0, *) {
            return ModernConnection()
        } else {
            return ClassicConnection()
        }
    }()
    
}
