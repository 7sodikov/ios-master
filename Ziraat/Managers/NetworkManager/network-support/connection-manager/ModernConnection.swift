//
//  ModernConnection.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 06/01/2020.
//

import Network
import UIKit


// MARK: - ModernConnection

@available(iOS 12.0, *)
final class ModernConnection: Connection {
    
    private var _connectionStatusHasChanged: ((ConnectionStatus) -> Void)?
    
    // Connection observers
    private let _pathMonitor: NWPathMonitor
    
    /// Connection status
    private var _status: NWPath.Status {
        didSet {
            isAvailable.value = isReachable
        }
    }
    
    // Connection queue
    private let _dispatchQueue = DispatchQueue(label: "olam_connection_queue")
    
    /// Block class initialization
    override init() {
        _status = .requiresConnection
        _pathMonitor = NWPathMonitor()
        super.init()
        
        /// Initialize connection path
        _pathMonitor.pathUpdateHandler = _statusHasChanged
    }
    
    override func network(isReachable: Bool) {
        super.network(isReachable: isReachable)
        DispatchQueue.main.async { [weak self] in
            self?._connectionStatusHasChanged?((isReachable) ? .success : .failure)
        }
    }
    
    private func _statusHasChanged(_ sender: NWPath) {
        _status = sender.status
    }
}


@available(iOS 12.0, *)
extension ModernConnection: ConnectionDelegate {
    
    /// Runs responsible observers to network connection
    func start() {
        _pathMonitor.start(queue: _dispatchQueue)
    }
    
    func stop() {
        _pathMonitor.cancel()
    }
    
    var isReachable: Bool {
        (_status == .satisfied)
    }
    
    func connectionStatusHasChanged(completion: @escaping (_ status: ConnectionStatus) -> Void) {
        _connectionStatusHasChanged = completion
    }
}
