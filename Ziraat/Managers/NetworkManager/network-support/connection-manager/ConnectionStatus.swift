//
//  ConnectionStatus.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 06/01/2020.
//

enum ConnectionStatus {
    /// Successful status for reachable network connection
    case success
    /// Failure status for unreachable network connection
    case failure
}
