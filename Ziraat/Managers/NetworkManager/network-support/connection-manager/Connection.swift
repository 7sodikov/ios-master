//
//  Connection.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 06/01/2020.
//

import UIKit

// MARK: - Super class

class Connection: NSObject {
    
    /// Network connection status
    let isAvailable: Dynamic<Bool>
    
    override init() {
        isAvailable = Dynamic<Bool>(value: false)
        super.init()
        
        /// Add observer in order to update network connection
        isAvailable.bind { (isReachable) in
            self.network(isReachable: isReachable)
        }
    }

    func network(isReachable: Bool) {
        isReachable ? print("Connection is on") : print("Connection is lost")
    }
}
