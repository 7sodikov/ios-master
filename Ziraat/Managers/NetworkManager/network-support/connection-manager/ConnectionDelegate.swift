//
//  ConnectionDelegate.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 06/01/2020.
//

import Foundation

protocol ConnectionDelegate: class {
    /**
     Starts on background
     
     Creates a new objects in order to check network status. If network available,
     tells delegate method networkReachable that connection is satisfied.
     */
    func start()
    
    /// Stops background objects that checks network connection status
    func stop()
    
    /// Returns a boolean about network status
    var isReachable: Bool { get }
    
    /// Notifies about connection status (network available or not)
    func connectionStatusHasChanged(completion: @escaping (_ status: ConnectionStatus) -> Void)
}
