//
//  ClassicConnection.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 06/01/2020.
//

import Network
import UIKit


// MARK: - ClassicConnection

/// Available frm iOS 9.0 up to iOS 12
final class ClassicConnection: Connection {
    
    private var _connectionStatusHasChanged: ((ConnectionStatus) -> Void)?
    
    let reachability: Reachability
    
    override init() {
        guard let reachability = Reachability() else { fatalError("Could not load Reachability")}
        self.reachability = reachability
        super.init()
        
        NotificationCenter.default
            .addObserver(self, selector: #selector(_networkStatusHasChanged), name: .reachabilityChanged, object: reachability)
    }
    
    override func network(isReachable: Bool) {
        super.network(isReachable: isReachable)
        DispatchQueue.main.async { [weak self] in
            self?._connectionStatusHasChanged?((isReachable) ? .success : .failure)
        }
    }
    
    @objc private func _networkStatusHasChanged(_ sender: NSNotification) {
        isAvailable.value = isReachable
    }
}


extension ClassicConnection: ConnectionDelegate {
    
    func start() {
        do {
            try reachability.startNotifier()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func stop() {
        reachability.stopNotifier()
    }
    
    var isReachable: Bool {
        (reachability.connection == .wifi) || (reachability.connection == .cellular)
    }
    
    func connectionStatusHasChanged(completion: @escaping (_ status: ConnectionStatus) -> Void) {
        _connectionStatusHasChanged = completion
    }
    
}
