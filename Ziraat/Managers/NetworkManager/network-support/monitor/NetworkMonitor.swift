//
//  NetworkMonitor().swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

import Alamofire

enum ResponseResult<Success, Failure> where Failure: Error {
    case success(ResponseContainer<Success>)
    case failure(Failure)
    
    @inlinable public func get() throws -> Success {
        switch self {
        case .success(let data):
            return data.data
        case .failure(let error):
            throw error
        }
    }

}

final class NetworkMonitor {
    typealias AFResponse = AFDataResponse<Any>
    
    static var errorRecorder: ((_ error: AppError) -> Void)?
    static var unauthorizedUserHandler: (() -> Void)?
    static var updateAppHandler: (()->Void)?
    
    /**
     Analyzes response object and decodes decodable object concluding on success or error
     
     - Parameters:
     - response: The response returned by server
     - kind: The decodable object type
     - completion: The callback called after monitor finish
     */
    func monitor<Model: Codable>(_ response: AFResponse, kind: Model.Type, completion: @escaping (ResponseResult<Model, AppError>) -> Void) {
        do {
            guard let statusCode = response.response?.statusCode else {
                throw AppError.unexpected(description: "Response without status code")
            }
            if statusCode == 401 { // send to login
                completion(.failure(.standard(description: "Unauthorized", code: 401)))
                UDManager.clear()
//                KeychainManager.clear()
                KeychainManager.logout()
                appDelegate.router.navigate(to: .loginInit)
                return
            } else if statusCode == 428 {
                completion(.failure(.standard(description: "Password expired", code: 428)))
                appDelegate.router.navigate(to: .resetPassword)
                return
            }
//            else if statusCode == 426 {
//                NetworkMonitor.updateAppHandler?()
//                return completion(.failure(.unableToConnect))
//            } else if statusCode == 413 {
//                let error = AppError.standard(description: NSLocalizedString("Error.PayloadTooLarge", comment: ""), code: 413)
//                fire(error, kind: kind, completion: completion)
//                return
//            }
            
            do {
                let datum: JSONResponse<Model> = try decode(response.data, with: response.result)
                let result = datum.result(statusCode: statusCode)
                switch result {
                case let .success(model):
                    DispatchQueue.main.async {
                        completion(.success(model))
                    }
                    
                case let .failure(error):
                    fire(error, kind: kind, completion: completion)
                }
            } catch {
                dump(error)
                fire(error, kind: kind, completion: completion)
            }
            
        } catch {
            fire(error, kind: kind, completion: completion)
        }
    }
    
    private func decode<Model: Codable>(_ data: Data?, with result: Result<Any, AFError>) throws -> Model {
        guard let data = data else {
            throw AppError.unexpected(description: "Response data is nil")
        } // { throw NetworkHelper.NetworkHelperError.encodedDataNotFound }
        
        do {
            // Check result
            switch result {
            case .success:
                return try JSONDecoder().decode(Model.self, from: data)
                
            case let .failure(error):
                throw AppError.unexpected(description: error.localizedDescription)
            }
            
        } catch let DecodingError.keyNotFound(key, context) {
            let codingPath = getCodingPath(from: context)
            let errorStr = "Key \(key.stringValue) not found: \(context.debugDescription) for codingPath: \(codingPath)"
            throw AppError.unexpected(description: errorStr)
        } catch let DecodingError.valueNotFound(value, context) {
            let codingPath = getCodingPath(from: context)
            let errorStr = "Value \(value) not found: \(context.debugDescription) for codingPath: \(codingPath)"
            throw AppError.unexpected(description: errorStr)
        } catch let DecodingError.typeMismatch(type, context)  {
            let codingPath = getCodingPath(from: context)
            let errorStr = "Type \(type) mismatch: \(context.debugDescription) for codingPath: \(codingPath)"
            throw AppError.unexpected(description: errorStr)
        } catch {
            throw AppError.unexpected(description: error.localizedDescription)
        }
    }
    
    private func getCodingPath(from context: DecodingError.Context) -> String {
        var codingP = ""
        for cp in context.codingPath {
            codingP.append(cp.stringValue)
            codingP.append(".")
        }
        return codingP
    }
    
    private func fire<Model>(_ error: Error, kind: Model.Type, completion: @escaping (ResponseResult<Model, AppError>) -> Void) {
        let reason = error as! AppError
        NetworkMonitor.errorRecorder?(reason)
        DispatchQueue.main.async {
            completion(.failure(reason))
        }
        print("» Failure of «Decoding»\n")
        #if DEBUG
        dump(reason)
        #endif
        print(reason)
        print("\n")
    }
}
