//
//  NetworkPreference.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

import Foundation


final class NetworkPreference {
    
    static let shared = NetworkPreference()
    
    private let standard = UserDefaults(suiteName: "group.uz.ziraat.mobile") ?? UserDefaults.standard
    
    private init() {}
    
    /// Returns language code
    func languageCode() throws -> String {
        if let code = standard.value(forKeyPath: Key.kLanguage.rawValue) as? String {
            return code
        } else {
            guard let code = UserDefaults.standard.value(forKeyPath: Key.kLanguage.rawValue) as? String else {
                throw Error.languageNotFound(key: Key.kLanguage.rawValue)
            }
            return code
        }
    }
    
    /// Returns user token
    func authToken() throws -> String {
        if let token = standard.value(forKeyPath: Key.kUserToken.rawValue) as? String {
            return token
        } else {
            guard let token = UserDefaults.standard.value(forKeyPath: Key.kUserToken.rawValue) as? String else {
                throw Error.languageNotFound(key: Key.kUserToken.rawValue)
            }
            return token
        }
    }
    
    func firebaseToken() -> String {
        if let token = standard.value(forKeyPath: Key.kUserFirebaseToken.rawValue) as? String {
            return token
        } else {
            guard let token = UserDefaults.standard.value(forKeyPath: Key.kUserFirebaseToken.rawValue) as? String else {
                return ""
            }
            return token
        }
    }
    
//    func region() -> CRegion? {
//        guard let savedRegion = standard.object(forKey: Key.kRegion.rawValue) as? Data else {
//            return nil
//        }
//        let decoder = JSONDecoder()
//        return try? decoder.decode(CRegion.self, from: savedRegion)
//    }
}

extension NetworkPreference {
    
    /// Sets app language code
    func setLanguage(code: String) {
        standard.set(code, forKey: Key.kLanguage.rawValue)
    }
    
    /// Sets user token
    func setUser(token: String) {
        standard.set(token, forKey: Key.kUserToken.rawValue)
    }
    
    /// Sets user firebase token
    func setUser(firebaseToken token: String) {
        standard.set(token, forKey: Key.kUserFirebaseToken.rawValue)
    }
    
//    func setRegion(_ region: CRegion?) {
//        let encoder = JSONEncoder()
//        if let encoded = try? encoder.encode(region) {
//            standard.set(encoded, forKey: Key.kRegion.rawValue)
//        }
//    }
}

fileprivate extension NetworkPreference {
    
    enum Key: String {
        case kLanguage = "NetworkPreferenceKey:Language"
        case kUserToken = "NetworkPreferenceKey:UserToken"
        case kUserFirebaseToken = "NetworkPereferenceKey:FirebaseToken"
        case kRegion = "NetworkPereferenceKey:Region"
    }
    
    enum Error: Swift.Error {
        case languageNotFound(key: String)
        case userTokenNotFound(key: String)
        case firebaseTokenNotFound(key: String)
    }
    
}
