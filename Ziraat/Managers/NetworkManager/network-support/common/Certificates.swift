//
//  Certificates.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct Certificates {
    static let stackExchange = Certificates.certificate(filename: "ziraatbank_uz")
    
    private static func certificate(filename: String) -> SecCertificate? {
        guard let filePath = Bundle.main.path(forResource: filename, ofType: "der") else {
            return nil
        }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) else {
            return nil
        }
        let certificate = SecCertificateCreateWithData(nil, data as CFData)
        return certificate
    }
}
