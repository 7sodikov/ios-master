//
//  RequestManager.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import Alamofire

/// Reference to `RequestManager.shared` for quick bootstrapping and examples.
let RM = RequestManager.shared

class RequestManager {
    static let shared = RequestManager()
    
    // Look at:
    // https://www.raywenderlich.com/1484288-preventing-man-in-the-middle-attacks-in-ios-with-ssl-pinning
    // for more information about SSL Pinning
    //
    private let evaluators = [
        URLConstant.hostName: PinnedCertificatesTrustEvaluator(certificates: [
            Certificates.stackExchange!
        ])
    ]
    
    let session: Session
    
    init() {
        session = Session(serverTrustManager: ServerTrustManager(evaluators: evaluators))
    }
    
    func request<Model: Codable>(_ convertible: URLRequestConvertible,
                                 interceptor: RequestInterceptor? = nil,
                                 kind: Model.Type,
                                 completion: @escaping (ResponseResult<Model, AppError>) -> Void) {
//        let request = AF.request(convertible, interceptor: interceptor)
        let request = session.request(convertible)
        let requestId = LogManager.makeIdentifier()
        LogManager.logRequest(request: convertible.urlRequest, with: requestId)
        request.responseJSON { (response) in
            let t = response.response?.statusCode
            LogManager.logResponse(with: requestId, response: response)
            NetworkMonitor().monitor(response, kind: kind, completion: completion)
        }
    }
}
