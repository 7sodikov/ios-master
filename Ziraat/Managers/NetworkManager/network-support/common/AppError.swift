//
//  AppError.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

enum AppError: Swift.Error {
    /// The error send by backend
    case standard(description: String?, code: Int?)
    /// Error handled in iOS app
    case technical(title: String, description: String)
    /// Unhandled error
    case unexpected(description: String)
    /// No network connection
    case unableToConnect
    
    case dictObjectNotFound(key: String)
    
    case unexpectedStructure(key: String)
    
    var localizedDescription: String {
        switch self {
        case .standard(let description, _):
            return description ?? String.init()
            
        case .unexpected(let description):
            return description
            
        case let .technical(title, description):
            return "\(title)\n\(description)"
            
        case .unableToConnect:
            return "Unable to connect to network\nCheck if you have Wi-Fi or mobile Internet enabled, and try again"
            
        case let .dictObjectNotFound(key):
            return "Object for \"\(key)\" not found"
            
        case let .unexpectedStructure(key):
            return "Structure of object for \"\(key)\" is unexpected (invalid)."
        }
    }
    
    var code: Int? {
        switch self {
        case .standard(_, let code):
            return code
        default:
            return nil
        }
    }
}
