//
//  Parameter.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

enum Parameter: String {
    case code
    case phone
    case token
    case username
    case captcha
    case oldPassword
    case pinfl
    case password
    case expiry
    case expire
    case card
    case cardId
    case newPass = "newPassword"
    case repeatPass = "repeatPassword"
    case loanPeriod = "period"
    case gracePeriod
    case loanRate = "rate"
    case loanType = "type"
    case loan
    
    case id
    case from
    case to
    
    case sender
    case senderPan
    case senderId
    case senderBranch
    case receiverPan
    case receiver
    case receiverBranch
    case receiverId
    case branch
    case pan
    case amount
    case paramBool
    
    case categoryId
    case search
    case serviceId
    case serviceType
    case fields
    case infoServiceId
    
    case startDate
    case endDate
    case page
    case size
    
    case account
    case operationId
    
    case favoriteId
    case title
    
    case status
    case limit
    
    case productLoanId
    case productDepositId
    case active
    case keyword
    case autoPayId

}
