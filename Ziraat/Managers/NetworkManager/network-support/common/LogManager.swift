//
//  LogManager.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire
import SwiftyJSON

final class LogManager {
    
    static func logRequest(request: URLRequest?,
                    with identifier: String) {
//        if !showLogs {
//            return
//        }
        
        #if !DEBUG
            return
        #endif
        
        print("\n\n")
        print("SEND: >>>>")
        print(identifier)
        print(request?.url?.absoluteString ?? "")
        print(request?.httpMethod ?? "")
        print(JSON(request?.allHTTPHeaderFields ?? [:]))
        print(JSON(request?.httpBody as Any))
    }
    
    static func logResponse(with identifier: String,
                            response: AFDataResponse<Any>) {
//        if !showLogs {
//            return
//        }
        
        #if !DEBUG
            return
        #endif
        
        var responseResult: Any = ""
        if let error = response.error {
            responseResult = error.localizedDescription
        } else {
            responseResult = response.data ?? ""
        }
        
        print("\n\n")
        print("RECV: <<<<")
        print(identifier)
        print(response.response?.statusCode ?? 0)
        print(JSON(responseResult))
    }
    
    static func makeIdentifier() -> String {
        let currentDate = Date()
        return "\(currentDate.timeIntervalSince1970)"
    }
}
