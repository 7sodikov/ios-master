//
//  NetworkHelper.swift
//  AppNetwork
//
//  Created by Botir Nasridinov on 12/12/2019.
//

import Foundation
import Alamofire

class NetworkHelper {
    
    enum NetworkHelperError: Error {
        case encodedDataNotFound
        case decodeFailure(description: Swift.Error)
    }
    
    /// Standard url request
    @nonobjc class func urlRequest(url: URL, method: HTTPMethod, requiresToken: Bool = false) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.method = method
        
        // Setup headers
        do {
            try urlRequest.set(key: .contentType, value: .applicationJSON)
            try urlRequest.set(key: .authToken, value: .authToken)
            try urlRequest.set(key: .language, value: .language)
//            try urlRequest.set(key: .timestamp, value: .timestamp)
            try urlRequest.set(key: .deviceModel, value: .deviceModel)
            try urlRequest.set(key: .deviceUID, value: .deviceUID)
            try urlRequest.set(key: .deviceOS, value: .deviceOS)
            try urlRequest.set(key: .appVersion, value: .appVersion)
            try urlRequest.set(key: .build, value: .build)
            try urlRequest.set(key: .firebaseToken, value: .firebaseToken)
        } catch {
            #if DEBUG
            print(error.localizedDescription)
            #endif
        }
        
        return urlRequest
    }
}

func choose<T>(_ data: T?, _ certain: T) -> T {
    guard let data = data else { return certain }
    return data
}
