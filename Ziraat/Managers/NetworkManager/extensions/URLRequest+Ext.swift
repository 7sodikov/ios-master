//
//  URLRequest+Ext.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

import UIKit

extension URLRequest {
    
    enum HeaderField: String {
        case contentType = "Content-Type"
        case authToken = "Authorization"
        case language = "X-Lang"
        case timestamp = "X-Timestamp"
        case deviceModel = "X-Device-Model"
        case deviceUID = "X-Device-Uid"
        case deviceOS = "X-Device-Type"
        case appVersion = "X-App-Version"
        case build = "X-App-Build"
        case firebaseToken = "X-FCM-Token"

//        case accept = "Accept"
//        case deviceOSVersion = "X-Mobile-OSVersion"
    }
    
    enum HeaderValue {
        case applicationJSON
        case applicationXWWWFromUrlEncoded        
        case multipartFormData
        case firebaseToken
        case language
        case deviceUID
        case deviceModel
        case deviceOS
        case deviceOSVersion
        case appVersion
        case authToken
        case build
        case timestamp
        
        func string() throws -> String {
            switch self {
            case .applicationJSON:
                return "application/json"
                
            case .applicationXWWWFromUrlEncoded:
                return "application/x-www-form-urlencoded; charset=utf-8"
                
            case .multipartFormData:
                return "multipart/form-data"
                
            case .authToken:
                return KeychainManager.sessionToken ?? ""
                
            case .firebaseToken:
                return UDManager.fcmtoken ?? "" //NetworkPreference.shared.firebaseToken()
                
            case .language:
                if LocalizationManager.language == Language.turkish {
                    return "TR"
                } else if LocalizationManager.language == Language.english {
                    return "EN"
                } else if LocalizationManager.language == Language.russian {
                    return "RU"
                } else if LocalizationManager.language == Language.uzbek {
                    return "UZ"
                } else {
                    return "EN"
                }
            case .deviceUID:
                guard let uid = UIDevice.current.identifierForVendor else {
                    throw Error.deviceIdentifierNotFound
                }
                return uid.uuidString
             
            case .deviceModel:
                return DeviceService.getDeviceModel()
                
            case .deviceOS:
                return "ios"
                
            case .deviceOSVersion:
                return UIDevice.current.systemVersion
                
            case .appVersion:
                return Bundle.main.appVersion
                
            case .build:
                return Bundle.main.appBuild
                
            case .timestamp:
                return ""
            }
        }
    }
    
    enum Error: Swift.Error {
        case deviceIdentifierNotFound
    }
    
    /**
     Sets value to header field
     
     - Parameter key: Header key
     - Parameter value: Header value
     
     */
    mutating func set(key: HeaderField, value: HeaderValue) throws {
        do {
            setValue(try value.string(), forHTTPHeaderField: key.rawValue)
        } catch {
            throw error
        }
    }
        
}


extension Bundle {
    
    var appVersion: String {
        let dictionary = Bundle.main.infoDictionary
        let kVersion = "CFBundleShortVersionString"
        guard let version = dictionary?[kVersion] as? String else { return "" }
        return version
    }
    
    var appBuild: String {
        let dictionary = Bundle.main.infoDictionary
        let kBuild = "CFBundleVersion"
        guard let build = dictionary?[kBuild] as? String else { return  ""}
        return build
    }
    
}
