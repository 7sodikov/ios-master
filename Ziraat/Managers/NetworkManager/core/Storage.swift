//
//  Storage.swift
//  JOYDA
//
//  Created by botirjon nasridinov on 19.11.2019.
//

import Foundation

protocol StorageKey {
    var value: String { get }
}

class Storage {
    static let shared: Storage = Storage.init()
    private init() {}
    
    private let defaults = UserDefaults.standard
    
//    func save<T: CustomDataConvertible>(value: T, forKey key: StorageKey, isSecurityItem: Bool = false) {
//        if isSecurityItem {
//            KeychainService.shared.saveItem(value, forKey: key.value)
//        }
//        else {
//            defaults.setValue(value.data, forKey: key.value)
//        }
//    }
//
//    func getValue<T: CustomDataConvertible>(forKey key: StorageKey, isSecurityItem: Bool = false) -> T? {
//        if isSecurityItem {
//            return KeychainService.shared.getItem(forKey: key.value)
//        }
//        else {
//            guard let data = defaults.value(forKey: key.value) as? Data else {
//                return nil
//            }
//            return T.from(data: data)
//        }
//    }
//
//    func deleteValue(forKey key: StorageKey, isSecurityItem: Bool = false) {
//        if isSecurityItem {
//            KeychainService.shared.deleteData(forKey: key.value)
//        }
//        else {
//            defaults.setValue(nil, forKey: key.value)
//        }
//    }
}

extension Storage {
//    func getToken() -> String? {
//        return getValue(forKey: .token, isSecurityItem: true)
//    }
//
//    func saveToken(_ token: String) {
//        save(value: token, forKey: .token, isSecurityItem: true)
//    }
//
//    func deleteToken() {
//        deleteValue(forKey: .token, isSecurityItem: true)
//    }
//
//    func getPushToken() -> String? {
//        return getValue(forKey: .pushToken)
//    }
//
//    func savePushToken(_ token: String) {
//        save(value: token, forKey: .pushToken)
//    }
}

extension String: CustomDataConvertible {
    typealias T = String
    
    var data: Data? {
        return data(using: .utf8)
    }
    
    static func from<T>(data: Data) -> T? {
        return String.init(bytes: data, encoding: .utf8) as? T
    }
}
