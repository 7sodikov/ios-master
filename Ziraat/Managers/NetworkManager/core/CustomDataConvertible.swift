//
//  CustomDataConvertible.swift
//  JOYDA
//
//  Created by botirjon nasridinov on 19.11.2019.
//

import Foundation

protocol CustomDataConvertible {
    associatedtype T
    var data: Data? { get }
    static func from<T>(data: Data) -> T?
}
