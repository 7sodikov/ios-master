//
//  BankProductsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/13/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

enum BankProductsRouter: URLRequestConvertible {
    
    case loanList
    case depositList
    case getLoanDetails(loanId: Int)
    case getDepositDetails(depositId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .loanList, .depositList:
            return .get
        case .getLoanDetails, .getDepositDetails:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case .loanList:
            return AppURL.v1.bankProducts.loanList
        case .depositList:
            return AppURL.v1.bankProducts.depositList
        case .getLoanDetails:
            return AppURL.v1.bankProducts.getLoanDetails
        case .getDepositDetails:
            return AppURL.v1.bankProducts.getDepositDetails
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .loanList:
            return nil
        case .depositList:
            return nil
        case let .getLoanDetails(loanId):
            return [Parameter.productLoanId.rawValue: loanId]
        case let .getDepositDetails(depositId):
            return [Parameter.productDepositId.rawValue: depositId]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
