//
//  NotificationsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

enum NotificationsRouter: URLRequestConvertible {
    
    case notificationsList(page: Int, size: Int)
    case notificationTopic
    case notificationId(id: Int)
    
    var method: HTTPMethod {
        switch self {
        case .notificationsList, .notificationTopic, .notificationId:
            return .get
        }
    }
    
    var endPoint: String {
        switch self {
        case let .notificationsList(page, size):
            return String(format: AppURL.v1.notifications.notificationsList, page, size)
        case .notificationTopic:
            return AppURL.v1.notifications.notificationTopic
        case let .notificationId(id):
            return String(format: AppURL.v1.notifications.notificationId, id)
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .notificationsList(_, _):
            return nil
        case .notificationTopic:
            return nil
        case .notificationId(_):
            return nil
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
