//
//  InfoRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum InfoRouter: URLRequestConvertible {
    
    case getMainCurrencyList
    case getAllCurrencyList
    case getMainExchangeRates
    case getAllExchangeRates
    
    var method: HTTPMethod {
        switch self {
        case .getMainCurrencyList,
             .getAllCurrencyList,
             .getMainExchangeRates,
             .getAllExchangeRates:
            return .get
        }
    }
    
    var endPoint: String {
        switch self {
        case .getMainCurrencyList:
            return AppURL.v1.info.getMainCurrencyList
        case .getAllCurrencyList:
            return AppURL.v1.info.getAllCurrencyList
        case .getMainExchangeRates:
                return AppURL.v1.info.getMainExchangeRates
        case .getAllExchangeRates:
                return AppURL.v1.info.getAllExchangeRates
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getMainCurrencyList:
            return nil
        case .getAllCurrencyList:
            return nil
        case .getMainExchangeRates:
                return nil
        case .getAllExchangeRates:
                return nil
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
