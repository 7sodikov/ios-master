//
//  CardRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum CardRouter: URLRequestConvertible {
    
    case cardsList
    case cardBalance
    case cardBalanceByID
    case cardOwner(cardId: Int64?, pan: String)
    case cardAdd(pan:String, exp:String, title:String)
    case cardHistory(start:String, end:String, id:UInt64)
    case cardBlock(id:Int)
    case cardUnblock(id:Int)
    case cardDelete(id:Int)
    case cardDeleteConfirm(id:Int, code:String)
    
    var method: HTTPMethod {
        switch self {
        case .cardsList,
             .cardBalance:
            return .get
        case .cardBalanceByID,
             .cardOwner:
            return .post
        case .cardAdd:
            return .post
        case .cardHistory, .cardDelete:
            return .post
        default: return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case .cardsList:
            return AppURL.v1.card.cardsList
        case .cardBalance:
            return AppURL.v1.card.cardBalance
        case .cardBalanceByID:
            return AppURL.v1.card.cardBalanceByID
        case .cardOwner:
            return AppURL.v1.card.cardOwner
        case .cardAdd:
            return AppURL.v1.card.cardAdd
            
        case .cardHistory:
            return AppURL.v1.card.cadHistory
            
        case .cardBlock(id: _):
            return AppURL.v1.card.cardBlock
            
        case .cardUnblock(id: _):
            return AppURL.v1.card.cardUnblock
            
        case .cardDelete:
            return AppURL.v1.card.cardDelete
            
        case .cardDeleteConfirm:
            return AppURL.v1.card.cardDeleteConfirm
        
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .cardsList:
            return nil
        case .cardBalance:
            return nil
        case .cardBalanceByID:
            return nil
        case let .cardOwner(cardId, pan):
            var params: [String: Any] = [Parameter.pan.rawValue: pan]
            if let cardId = cardId {
                params[Parameter.cardId.rawValue] = cardId
            }
            return params
        case let .cardAdd(pan, exp, title):
            var params: [String: Any] = [Parameter.pan.rawValue: pan]
            params[Parameter.expire.rawValue] = exp
            params[Parameter.title.rawValue] = title
            return params
        
        case let .cardHistory(start, end, id):
            var params: [String: Any] = [Parameter.startDate.rawValue: start]
            params[Parameter.endDate.rawValue] = end
            params[Parameter.cardId.rawValue] = id
            return params
            
        case let .cardUnblock(id: id):
            return [Parameter.cardId.rawValue: id]
            
        case let .cardBlock(id: id):
            return [Parameter.cardId.rawValue: id]
        
        case let .cardDelete(id: id):
            return [Parameter.cardId.rawValue : id]
            
        case let .cardDeleteConfirm(id: id, code: code):
            return [Parameter.cardId.rawValue : id,
                    Parameter.code.rawValue : code]
        
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
    
}
