//
//  FavoriteRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

enum FavoriteRouter: URLRequestConvertible {
    case favoriteList
    case favoritePaymentList
    case favoriteOperationList
    case favoriteDelete(favoriteId: UInt64)
    case favoriteEdit(favoriteId: UInt64, title: String)
    case favoriteAdd(operationId: Int, title: String)
    case favoritePayment(favoriteId: Int)
    case favoriteOperation(favoriteId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .favoriteList,
             .favoritePaymentList,
             .favoriteOperationList:
            return .get
            
        case .favoriteDelete,
             .favoriteEdit,
             .favoriteAdd,
             .favoritePayment,
             .favoriteOperation:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case .favoriteList:
            return AppURL.v1.favorite.favoriteList
        case .favoritePaymentList:
            return AppURL.v1.favorite.favoritePaymentList
        case .favoriteOperationList:
            return AppURL.v1.favorite.favoriteOperationList
        case .favoriteDelete:
            return AppURL.v1.favorite.favoriteDelete
        case .favoriteEdit:
            return AppURL.v1.favorite.favoriteEdit
        case .favoriteAdd:
            return AppURL.v1.favorite.favoriteAdd
        case .favoritePayment:
            return AppURL.v1.favorite.favoritePayment
        case .favoriteOperation:
            return AppURL.v1.favorite.favoriteOperation
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .favoriteList:
            return nil
            
        case .favoritePaymentList:
            return nil
            
        case .favoriteOperationList:
            return nil
            
        case let .favoriteDelete(favoriteId):
            return [Parameter.favoriteId.rawValue: favoriteId]
            
        case let .favoriteEdit(favoriteId, title):
            return [Parameter.favoriteId.rawValue: favoriteId,
                    Parameter.title.rawValue: title]
            
        case let .favoriteAdd(operationId, title):
            return [Parameter.operationId.rawValue: operationId,
                    Parameter.title.rawValue: title]
            
        case let .favoritePayment(favoriteId):
            return [Parameter.favoriteId.rawValue: favoriteId]
            
        case let .favoriteOperation(favoriteId):
            return [Parameter.favoriteId.rawValue: favoriteId]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
}
