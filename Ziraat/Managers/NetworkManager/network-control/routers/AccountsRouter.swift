//
//  AccountsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum AccountsRouter: URLRequestConvertible {
    
    case accountsList
    case accountBalance
    case accountsStatements(Id: String, from: String, to: String)
    case accountOwner(accountNumber: String, branch: String)
    
    var method: HTTPMethod {
        switch self {
        case .accountsList,
             .accountBalance:
            return .get
        case .accountsStatements,
             .accountOwner:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case .accountsList:
            return AppURL.v1.accounts.accountsList
        case .accountBalance:
            return AppURL.v1.accounts.accountBalance
        case .accountsStatements:
            return AppURL.v1.accounts.accountStatements
        case .accountOwner:
            return AppURL.v1.accounts.accountOwner
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .accountsList:
            return nil
        case .accountBalance:
            return nil
        case let .accountsStatements(id, from, to):
            return [Parameter.id.rawValue: id,
                    Parameter.from.rawValue: from,
                    Parameter.to.rawValue: to
            ]
        case let .accountOwner(accountNumber, branch):
            return [Parameter.account.rawValue: accountNumber,
                    Parameter.branch.rawValue: branch]
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
