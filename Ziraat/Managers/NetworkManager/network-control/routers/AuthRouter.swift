//
//  AuthRouter.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

import Alamofire

enum AuthRouter: URLRequestConvertible {
    
    case loginInit
    case login(token: String, username: String, password: String)
    case loginConfirm(token: String, code: String, username: String, password: String)
    case loginResendCaptcha(token: String)
    case loginResendSMS(token: String)
    case logout
    case changePassword(oldPassword: String, newPassword: String, repeatPassword: String)
    case passwordRecoveryInit
    case passwordRecoveryResendSMS(token: String)
    case passwordRecoveryResendCaptcha(token: String)
    case passwordRecoveryStepOne(token: String, phone: String, pinfl: String, captcha: String)
    case passwordRecoveryStepTwo(token: String, card: String, expiry: String)
    case passwordRecoveryStepThree(token: String, code: String)
    case passwordRecoveryStepFour(token: String, newPassword: String, repeatPassword: String)
    
    /// Type representing HTTP methods.
    var method: HTTPMethod {
        switch self {
        case .login,
             .loginConfirm,
             .loginResendCaptcha,
             .loginResendSMS,
             .changePassword,
             .passwordRecoveryResendSMS,
             .passwordRecoveryResendCaptcha,
             .passwordRecoveryStepOne,
             .passwordRecoveryStepTwo,
             .passwordRecoveryStepThree,
             .passwordRecoveryStepFour:
            
            return .post
            
        case .loginInit,
             .passwordRecoveryInit,
             .logout:
            
            return .get
        }
    }
    
    /// End-point
    var endPoint: String {
        switch self {

        case .loginInit:
            return AppURL.v1.auth.loginInit
            
        case .login:
            return AppURL.v1.auth.login
            
        case .loginConfirm:
            return AppURL.v1.auth.loginConfirm
            
        case .loginResendCaptcha:
            return AppURL.v1.auth.loginResendCaptcha
            
        case .loginResendSMS:
            return AppURL.v1.auth.loginResendSMS
            
        case .logout:
            return AppURL.v1.auth.logout
            
        case .changePassword:
            return AppURL.v1.auth.changePassword
            
        case .passwordRecoveryInit:
            return AppURL.v1.auth.passwordRecoveryInit
            
        case .passwordRecoveryResendSMS:
            return AppURL.v1.auth.passwordRecoveryResendSMS
        
        case .passwordRecoveryResendCaptcha:
            return AppURL.v1.auth.passwordRecoveryResendCaptcha
            
        case .passwordRecoveryStepOne:
            return AppURL.v1.auth.passwordRecoveryStepOne
            
        case .passwordRecoveryStepTwo:
            return AppURL.v1.auth.passwordRecoveryStepTwo
            
        case .passwordRecoveryStepThree:
            return AppURL.v1.auth.passwordRecoveryStepThree
            
        case .passwordRecoveryStepFour:
            return AppURL.v1.auth.passwordRecoveryStepFour
        }
    }
    
    /// A dictionary of parameters to apply to a URLRequest.
    var parameters: Parameters? {
        switch self {
        
        case let .login(token, username, password):
            return [Parameter.token.rawValue: token,
                    Parameter.username.rawValue: username,
                    Parameter.password.rawValue: password]
            
        case .loginInit:
            return nil
            
        case let .loginConfirm(token, code, username, password):
            return [Parameter.token.rawValue: token,
                    Parameter.code.rawValue: code,
                    Parameter.username.rawValue: username,
                    Parameter.password.rawValue: password]
            
        case let .loginResendCaptcha(token):
            return [Parameter.token.rawValue: token]
            
        case let .loginResendSMS(token):
            return [Parameter.token.rawValue: token]
            
        case .logout:
            return nil
            
        case let .changePassword(oldPassword, newPassword, repeatPassword):
            return [Parameter.oldPassword.rawValue: oldPassword,
                    Parameter.newPass.rawValue: newPassword,
                    Parameter.repeatPass.rawValue: repeatPassword]
            
        case .passwordRecoveryInit:
            return nil
            
        case let .passwordRecoveryResendSMS(token):
            return [Parameter.token.rawValue: token]
            
        case let .passwordRecoveryResendCaptcha(token):
            return [Parameter.token.rawValue: token]
            
        case let .passwordRecoveryStepOne(token, phone, pinfl, captcha):
            return [Parameter.token.rawValue: token,
                    Parameter.phone.rawValue: phone,
                    Parameter.pinfl.rawValue: pinfl,
                    Parameter.captcha.rawValue: captcha]
            
        case let .passwordRecoveryStepTwo(token, card, expiry):
            return [Parameter.token.rawValue: token,
                    Parameter.card.rawValue: card,
                    Parameter.expiry.rawValue: expiry]
            
        case let .passwordRecoveryStepThree(token, code):
            return [Parameter.token.rawValue: token,
                    Parameter.code.rawValue: code]
            
        case let .passwordRecoveryStepFour(token, newPassword, repeatPassword):
            return [Parameter.token.rawValue: token,
                    Parameter.newPass.rawValue: newPassword,
                    Parameter.repeatPass.rawValue: repeatPassword]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
