//
//  MapRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

enum MapRouter: URLRequestConvertible {
    
    case map
    case mapAtm
    case mapBranch
    case mapExchange
    
    var method: HTTPMethod {
        switch self {
        case .map, .mapAtm, .mapBranch, .mapExchange:
            return .get
        }
    }
    
    var endPoint: String {
        switch self {
        case .map:
            return AppURL.v1.map.map
        case .mapAtm:
            return AppURL.v1.map.mapAtm
        case .mapBranch:
            return AppURL.v1.map.mapBranch
        case .mapExchange:
            return AppURL.v1.map.mapExchange
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .map, .mapAtm, .mapBranch, .mapExchange:
            return nil
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        return urlRequest
    }
    
}
