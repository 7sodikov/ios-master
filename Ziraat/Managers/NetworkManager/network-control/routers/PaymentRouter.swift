//
//  PaymentRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum PaymentRouter: URLRequestConvertible {
    
    case serviceCategories
    case services(categoryId: Int, searchString: String?)
    case servicesFilter(searchText: String)
    case paymentPrepare(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any])
    case serviceGetInfo(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any], infoId: Int)
    case paymentPay(token: String)
    case paymentConfirm(token: String, smsCode: String)
    case service(serviceId: String, serviceType: String)
    
    var method: HTTPMethod {
        switch self {
        case .services,
             .servicesFilter,
             .paymentPrepare,
             .paymentPay,
             .serviceGetInfo,
             .paymentConfirm,
             .service:
            return .post
        case .serviceCategories:
            return .get
        }
    }
    
    var endPoint: String {
        switch self {
        case .serviceCategories:
            return AppURL.v1.payment.serviceCategories
        case .services:
            return AppURL.v1.payment.services
        case .servicesFilter:
            return AppURL.v1.payment.servicesFilter
        case .paymentPrepare:
            return AppURL.v1.payment.paymentPrepare
        case .serviceGetInfo:
            return AppURL.v1.payment.serviceGetInfo
        case .paymentPay:
            return AppURL.v1.payment.paymentPay
        case .paymentConfirm:
            return AppURL.v1.payment.paymentConfirm
        case .service:
            return AppURL.v1.payment.service
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .serviceCategories:
            return nil
        case let .services(categoryId, searchString):
            var params: [String: Any] = [Parameter.categoryId.rawValue: categoryId]
            if let searchString = searchString {
                params[Parameter.search.rawValue] = searchString
            }
            return params
        case let .servicesFilter(searchText):
            return [Parameter.search.rawValue: searchText]
        case let .paymentPrepare(senderCardId, serviceId, serviceType, fields):
            return [Parameter.senderId.rawValue: senderCardId,
                    Parameter.serviceId.rawValue: serviceId,
                    Parameter.serviceType.rawValue: serviceType,
                    Parameter.fields.rawValue: fields]
        case let .serviceGetInfo(senderCardId, serviceId, serviceType, fields,infoId):
            return [Parameter.senderId.rawValue: senderCardId,
                    Parameter.serviceId.rawValue: serviceId,
                    Parameter.serviceType.rawValue: serviceType,
                    Parameter.infoServiceId.rawValue: infoId,
                    Parameter.fields.rawValue: fields]
        case let .paymentPay(token):
            return [Parameter.token.rawValue: token]
        case let .paymentConfirm(token, smsCode):
            return [Parameter.token.rawValue: token,
                    Parameter.code.rawValue: smsCode]
        case let .service(serviceId, serviceType):
            return [Parameter.serviceId.rawValue: serviceId,
                    Parameter.serviceType.rawValue: serviceType]
            
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
}
