//
//  OperationRouter.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum OperationRouter: URLRequestConvertible {
    
    case operationPrepare(prepareType: OperationPrepareType,
                          sender: String?, senderId: UInt64?,
                          senderBranch: String?,
                          receiver: String?, receiverId: UInt64?,
                          receiverBranch: String?,
                          paramNum: Int64?,
                          paramStr: String?,
                          paramBool: Bool?,
                          amount: Double)
    case operationTransfer(token: String)
    case operationConfirm(token: String, code: String)
    case operationSMSResend(token: String)
    case operationHistory(operationType: OperationType, size: Int)
    case operationReceipt(operationId: Int)
    case receiverList(keyword:String?, type: String?, page: Int, size: Int)
    
    var method: HTTPMethod {
        switch self {
        case .operationPrepare,
             .operationTransfer,
             .operationConfirm,
             .operationSMSResend,
             .operationHistory,
             .operationReceipt,
             .receiverList:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case let .operationPrepare(prepareType, _, _, _, _, _, _, _, _, _, _):
            return String(format: AppURL.v1.operation.operationPrepare, prepareType.rawValue)
            
        case .operationTransfer:
            return AppURL.v1.operation.operationTransfer
            
        case .operationConfirm:
            return AppURL.v1.operation.operationConfirm
            
        case .operationSMSResend:
            return AppURL.v1.operation.operationSMSResend
            
        case let .operationHistory(operationType, _):
            return String(format: AppURL.v1.operation.operationHistory, operationType.rawValue)
            
        case .operationReceipt:
            return AppURL.v1.operation.operationReceipt
        case .receiverList:
            return AppURL.v1.operation.receiverList
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case let .operationPrepare(prepareType,
                                   sender, senderId,
                                   senderBranch,
                                   receiver, receiverId,
                                   receiverBranch,
                                   _,
                                   _,
                                   paramBool,
                                   amount):
            
            var params: Parameters = [:]
            switch prepareType {
            case .p2p:
                params[Parameter.senderId.rawValue] = senderId
                params[Parameter.receiver.rawValue] = receiver
                params[Parameter.receiverId.rawValue] = receiverId
                params[Parameter.amount.rawValue] = amount
                
            case .p2a:
                params[Parameter.senderId.rawValue] = senderId
                params[Parameter.receiver.rawValue] = receiver
                params[Parameter.receiverBranch.rawValue] = receiverBranch
                params[Parameter.amount.rawValue] = amount
                
            case .p2l:
                params[Parameter.senderId.rawValue] = senderId
                params[Parameter.receiverId.rawValue] = receiverId
                params[Parameter.receiverBranch.rawValue] = receiverBranch
                params[Parameter.paramBool.rawValue] = paramBool
                params[Parameter.amount.rawValue] = amount
                
            case .a2p:
                params[Parameter.sender.rawValue] = sender
                params[Parameter.senderBranch.rawValue] = senderBranch
                params[Parameter.receiver.rawValue] = receiver
                params[Parameter.receiverId.rawValue] = receiverId
                params[Parameter.amount.rawValue] = amount
                
            case .a2l:
                params[Parameter.sender.rawValue] = sender
                params[Parameter.senderBranch.rawValue] = senderBranch
                params[Parameter.receiverId.rawValue] = receiverId
                params[Parameter.receiverBranch.rawValue] = receiverBranch
                params[Parameter.paramBool.rawValue] = paramBool
                params[Parameter.amount.rawValue] = amount
            default:
                break
            }
            return params
            
        case let .operationTransfer(token):
            return [Parameter.token.rawValue: token]
            
        case let .operationConfirm(token, code):
            return [Parameter.token.rawValue: token,
                    Parameter.code.rawValue: code]
            
        case let .operationSMSResend(token):
            return [Parameter.token.rawValue: token]
            
        case let .operationHistory(operationType, size):
            var params: Parameters = [:]
            switch operationType {
            case .payment:
                params[Parameter.size.rawValue] = size
            case .operation:
                params[Parameter.size.rawValue] = size
        }
            return params
        
        case let .operationReceipt(operationId):
            return [Parameter.operationId.rawValue: operationId]
            
        case let .receiverList(keyword, cardType, page, size):
            var param: Parameters = [Parameter.page.rawValue: page,
                                     Parameter.size.rawValue: size]
            if let type = cardType {
                param[Parameter.loanType.rawValue] = type
            }
            if let searchParam = keyword {
                param[Parameter.keyword.rawValue] = searchParam
            }
            return param
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
}
