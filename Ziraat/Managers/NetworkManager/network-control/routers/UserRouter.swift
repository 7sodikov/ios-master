//
//  UserRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

enum UserRouter: URLRequestConvertible {
    
    case getUserMeInfo
    case authHistory(status: String, limit: Int)
    
    var method: HTTPMethod {
        switch self {
        case .getUserMeInfo:
            return .get
        case .authHistory:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case .getUserMeInfo:
            return AppURL.v1.user.getUserMeInfo
        case .authHistory:
            return AppURL.v1.user.authHistory
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getUserMeInfo:
            return nil
        case let .authHistory(status, limit):
            return [Parameter.status.rawValue: status,
                    Parameter.limit.rawValue: limit]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
