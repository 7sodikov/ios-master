//
//  SettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//


import Alamofire

enum SettingsRouter: URLRequestConvertible {
    
    case getLangList
    case getReferencesNameList
    
    var method: HTTPMethod {
        switch self {
        case .getLangList,
             .getReferencesNameList:
            return .get
        }
    }
    
    var endPoint: String {
        switch self {
        case .getLangList:
            return AppURL.v1.settings.getLangList
        case .getReferencesNameList:
            return AppURL.v1.settings.getReferencesNameList
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getLangList:
            return nil
        case .getReferencesNameList:
            return nil
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
//            let encoder = URLEncoding.init(destination: .httpBody, arrayEncoding: .brackets, boolEncoding: .literal)
//            urlRequest = try encoder.encode(urlRequest, with: parameters)
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
