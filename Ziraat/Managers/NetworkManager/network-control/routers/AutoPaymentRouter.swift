//
//  AutoPaymentRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import Alamofire

enum AutoPaymentRouter: URLRequestConvertible {
    typealias AutoPayment = AppURL.AutoPayment
     
    
    case serviceCategory
    case autopayServices(_ categoryId: String)
    case autopayList(_ status: String?)
    case autopayHistoryList(_ autoPayId: String?, _ status: String?)
    case create(params: CAutoPayment.Params)
    case changeStatus(autoPayId: Int, status: String)
    case delete(autoPayId: Int)
    
    var method: HTTPMethod {
        switch self {
        case .serviceCategory:
            return .get
        case .autopayServices,
             .autopayList,
             .autopayHistoryList,
             .create,
             .changeStatus,
             .delete:
            return .post
        
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .serviceCategory:
            return nil
        case .autopayServices(let categoryId):
            return [Parameter.categoryId.rawValue: categoryId]
        case .autopayList(let status):
            guard let st = status else { return nil }
            return [Parameter.status.rawValue: st]
        case let .autopayHistoryList(autoPayId, status):
            return [Parameter.status.rawValue: status as Any,
                    Parameter.autoPayId.rawValue: autoPayId as Any]
        case .create(let params):
            return try? params.getParameters()
        case let .changeStatus(autoPayId, status):
            return  [Parameter.status.rawValue: status,
                     Parameter.autoPayId.rawValue: autoPayId]
        case .delete(let autoPayId):
            return [Parameter.autoPayId.rawValue: autoPayId]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let router = AutoPayment(router: self)
        var urlRequest = NetworkHelper.urlRequest(url: try router.domain.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        return urlRequest
    }
    
}
