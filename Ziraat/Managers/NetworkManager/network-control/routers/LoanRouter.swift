//
//  LoanRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum LoanRouter: URLRequestConvertible {
    
    case getLoans
    case loanBalance
    case loanCalculate(amount: Int, period: Int, gracePeriod: Int, rate: Int, typeLoan: Int)
    case loanOwner(loanId: UInt64, branch: String)
    case statement(loanId: UInt64, from: String, to:String)
    case graphic(loanId: UInt64, branch: String)
    
    var method: HTTPMethod {
        switch self {
        case .getLoans,
             .loanBalance:
            return .get
            
        case .loanCalculate,
             .statement,
             .graphic,
             .loanOwner:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case .getLoans:
            return AppURL.v1.loans.loanList
        case .loanBalance:
            return AppURL.v1.loans.loanBalance
        case .loanCalculate:
            return AppURL.v1.loans.loanCalculate
        case .loanOwner:
            return AppURL.v1.loans.loanOwner
        case .statement:
            return AppURL.v1.loans.statement
        case .graphic:
            return AppURL.v1.loans.graghic
        
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getLoans:
            return nil
        case .loanBalance:
            return nil
            
        case let .loanCalculate(amount, period, gracePeriod, rate, typeLoan):
            return [Parameter.amount.rawValue: amount,
                    Parameter.loanPeriod.rawValue: period,
                    Parameter.gracePeriod.rawValue: gracePeriod,
                    Parameter.loanRate.rawValue: rate,
                    Parameter.loanType.rawValue: typeLoan]
            
        case let .loanOwner(loanId, branch):
            return [Parameter.loan.rawValue: loanId,
                    Parameter.branch.rawValue: branch]
            
        case let .statement(loanId, from, to):
            return [Parameter.id.rawValue: loanId,
                    Parameter.from.rawValue: from,
                    Parameter.to.rawValue: to]
            
        case let .graphic(loanId, branch):
            return [Parameter.id.rawValue: loanId,
                    Parameter.branch.rawValue: branch]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
