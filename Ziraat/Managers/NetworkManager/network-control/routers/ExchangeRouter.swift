//
//  ExchangeRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

enum ExchangeRouter: URLRequestConvertible {
    
    case conversionPrepare(prepareType: OperationPrepareType,
                          sender: String?,
                          senderId: String?,
                          senderBranch: String?,
                          receiver: String?,
                          receiverId: String?,
                          receiverBranch: String?,
                          paramNum: Int64?,
                          paramStr: String?,
                          paramBool: Bool?,
                          amount: Double)
    
    var method: HTTPMethod {
        switch self {
        case .conversionPrepare:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case let .conversionPrepare(prepareType, _, _, _, _, _, _, _, _, _, _):
            return String(format: AppURL.v1.exchange.exchangePrepare, prepareType.rawValue)
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case let .conversionPrepare(prepareType,
                                   sender,
                                   senderId,
                                   senderBranch,
                                   receiver,
                                   receiverId,
                                   receiverBranch,
                                   _,
                                   _,
                                   _,
                                   amount):
            
            var params: Parameters = [:]
            switch prepareType {
            case .p2a, .a2p, .pca, .acp:
                params[Parameter.sender.rawValue] = sender
                params[Parameter.senderId.rawValue] = senderId
                params[Parameter.senderBranch.rawValue] = senderBranch
                params[Parameter.receiver.rawValue] = receiver
                params[Parameter.receiverId.rawValue] = receiverId
                params[Parameter.receiverBranch.rawValue] = receiverBranch
                params[Parameter.amount.rawValue] = amount
                
            case .p2l, .p2p, .a2l:
                break
            default:
                break
            }
        
            return params
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        return urlRequest
    }
}
