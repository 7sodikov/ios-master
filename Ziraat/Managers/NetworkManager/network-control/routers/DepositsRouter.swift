//
//  DepositsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum DepositsRouter: URLRequestConvertible {
    
    case getDeposits
    case depositBalance
    
    var method: HTTPMethod {
        switch self {
        case .getDeposits,
             .depositBalance:
            return .get
        }
    }
    
    var endPoint: String {
        switch self {
        case .getDeposits:
            return AppURL.v1.deposits.depositList
        case .depositBalance:
            return AppURL.v1.deposits.depositBalance
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getDeposits:
            return nil
        case .depositBalance:
            return nil
        }
    }
    
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}
