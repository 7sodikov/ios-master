//
//  CardsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

enum CardsRouter: URLRequestConvertible {
    
    case getCardList
    case getCardBalanceConversion
    case getCardBalanceIdentificator
    
    var method: HTTPMethod {
        switch self {
        case .getCardList,
             .getCardBalanceConversion:
            return .get
        case .getCardBalanceIdentificator:
            return .post
        }
    }
    
    var endPoint: String {
        switch self {
        case .getCardList:
            return AppURL.v1.settings.getLangList
        case .getCardBalanceConversion:
            return AppURL.v1.settings.getReferencesNameList
        case .getCardBalanceIdentificator:
                return AppURL.v1.settings.getLangList
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .getCardList:
            return nil
        case .getCardBalanceConversion:
            return nil
        case .getCardBalanceIdentificator:
                return nil
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = NetworkHelper.urlRequest(url: try endPoint.asURL(), method: method)
        
        if let parameters = self.parameters {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters)
        }
        
        return urlRequest
    }
    
}

