//
//  NetworkService+Loans.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Loan {
        static func loanList(completion: @escaping (ResponseResult<LoansResponse, AppError>) -> Void) {
            let request = LoanRouter.getLoans
            RM.request(request, kind: LoansResponse.self, completion: completion)
        }
        
        static func loanBalance(completion: @escaping (ResponseResult<LoanBalancesResponse, AppError>) -> Void) {
            let request = LoanRouter.loanBalance
            RM.request(request, kind: LoanBalancesResponse.self, completion: completion)
        }
        
        static func loanCalculate(amount: Int, period: Int, gracePeriod: Int, rate: Int, typeLoan: Int, completion: @escaping (ResponseResult<[LoanCalculateResponse], AppError>) -> Void) {
            let request = LoanRouter.loanCalculate(amount: amount, period: period, gracePeriod: gracePeriod, rate: rate, typeLoan: typeLoan)
            RM.request(request, kind: [LoanCalculateResponse].self, completion: completion)
        }
        
        static func loanOwner(loanId: UInt64, branch: String, completion: @escaping (ResponseResult<CardOwnerResponse, AppError>) -> Void) {
            let request = LoanRouter.loanOwner(loanId: loanId, branch: branch)
            RM.request(request, kind: CardOwnerResponse.self, completion: completion)
        }
        
        static func statements(loanId: UInt64, from: String,to:String, completion: @escaping (ResponseResult<LoanStatementsResponse, AppError>) -> Void) {
            let request = LoanRouter.statement(loanId: loanId, from: from, to: to)
            RM.request(request, kind: LoanStatementsResponse.self, completion: completion)
        }
        static func graphic(loanId: UInt64, branch: String, completion: @escaping (ResponseResult<LoanGraphsResponse, AppError>) -> Void) {
            let request = LoanRouter.graphic(loanId: loanId, branch: branch)
            RM.request(request, kind: LoanGraphsResponse.self, completion: completion)
        }
    }
}
