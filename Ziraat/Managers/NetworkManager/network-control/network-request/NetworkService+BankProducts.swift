//
//  NetworkService+BankProducts.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/13/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class BankProducts {
        static func loanList(completion: @escaping (ResponseResult<[LoanListResponse], AppError>) -> Void) {
            let request = BankProductsRouter.loanList
            RM.request(request, kind: [LoanListResponse].self, completion: completion)
        }
        
        static func depositList(completion: @escaping (ResponseResult<[DepositsListResponse], AppError>) -> Void) {
            let request = BankProductsRouter.depositList
            RM.request(request, kind: [DepositsListResponse].self, completion: completion)
        }
        
//        static func authInfo(status: String, limit: Int, completion: @escaping (ResponseResult<AuthHistoryResponse, AppError>) -> Void) {
//            let request = UserRouter.authHistory(status: status, limit: limit)
//            RM.request(request, kind: AuthHistoryResponse.self, completion: completion)
//        }
    }
}
