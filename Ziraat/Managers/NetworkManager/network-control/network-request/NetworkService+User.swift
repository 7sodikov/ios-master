//
//  NetworkService+User.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class User {
        static func getUserMeInfo(completion: @escaping (ResponseResult<UserResponse, AppError>) -> Void) {
            let request = UserRouter.getUserMeInfo
            RM.request(request, kind: UserResponse.self, completion: completion)
        }
        
        static func authInfo(status: String, limit: Int, completion: @escaping (ResponseResult<AuthHistoriesResponse, AppError>) -> Void) {
            let request = UserRouter.authHistory(status: status, limit: limit)
            RM.request(request, kind: AuthHistoriesResponse.self, completion: completion)
        }
    }
}
