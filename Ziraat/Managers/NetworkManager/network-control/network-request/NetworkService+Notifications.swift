//
//  NetworkService+Notifications.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Notifications {
        static func notificationList(page: Int, size: Int, completion: @escaping (ResponseResult<NotificationsListResponse, AppError>) -> Void) {
            let request = NotificationsRouter.notificationsList(page: page, size: size)
            RM.request(request, kind: NotificationsListResponse.self, completion: completion)
        }
        
        static func notificationTopic(completion: @escaping (ResponseResult<[String], AppError>) -> Void) {
            let request = NotificationsRouter.notificationTopic
            RM.request(request, kind: [String].self, completion: completion)
        }
    }
}
