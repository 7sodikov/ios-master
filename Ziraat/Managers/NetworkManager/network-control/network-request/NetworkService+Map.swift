//
//  NetworkService+Map.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Map {
        static func map(completion: @escaping (ResponseResult<[MapPoint], AppError>) -> Void) {
            let request = MapRouter.map
        RM.request(request, kind: [MapPoint].self, completion: completion)
        }
        
        static func mapAtm(completion: @escaping (ResponseResult<MapPoint, AppError>) -> Void) {
            let request = MapRouter.mapAtm
            RM.request(request, kind: MapPoint.self, completion: completion)
        }
        
        static func mapBranch(completion: @escaping (ResponseResult<MapPoint, AppError>) -> Void) {
            let request = MapRouter.mapBranch
            RM.request(request, kind: MapPoint.self, completion: completion)
        }
        
        static func mapExchange(completion: @escaping (ResponseResult<MapPoint, AppError>) -> Void) {
            let request = MapRouter.mapExchange
            RM.request(request, kind: MapPoint.self, completion: completion)
        }
    }
}
