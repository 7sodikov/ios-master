//
//  NetworkService+Card.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Card {
        static func cardsList(completion: @escaping (ResponseResult<CardsResponse, AppError>) -> Void) {
            let request = CardRouter.cardsList
            RM.request(request, kind: CardsResponse.self, completion: completion)
        }
        
        static func cardBalance(completion: @escaping (ResponseResult<CardBalancesResponse, AppError>) -> Void) {
            let request = CardRouter.cardBalance
            RM.request(request, kind: CardBalancesResponse.self, completion: completion)
        }
        
        static func addCard(pan: String, exp: String, title:String, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = CardRouter.cardAdd(pan: pan, exp: exp, title: title)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        static func addHistory(start: String, end: String, id: UInt64, completion: @escaping (ResponseResult<CardHistoryResponse, AppError>) -> Void) {
            let request = CardRouter.cardHistory(start: start, end: end, id: id)
            RM.request(request, kind: CardHistoryResponse.self, completion: completion)
        }
        
        static func cardBlock(id: Int, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = CardRouter.cardBlock(id: id)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        static func cardUnBlock(id: Int, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = CardRouter.cardUnblock(id: id)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        static func cardOwner(cardId: Int64?, pan: String, completion: @escaping (ResponseResult<CardOwnerResponse, AppError>) -> Void) {
            let request = CardRouter.cardOwner(cardId: cardId, pan: pan)
            RM.request(request, kind: CardOwnerResponse.self, completion: completion)
        }
        
        static func deleteCard(cardId: Int, completion: @escaping (ResponseResult<CardDeleteResponse, AppError>) -> Void) {
            let request = CardRouter.cardDelete(id: cardId)
            RM.request(request, kind: CardDeleteResponse.self, completion: completion)
        }
        static func deleteCardConfirm(cardId: Int, code: String, completion: @escaping (ResponseResult<CardDeleteResponse, AppError>) -> Void) {
            let request = CardRouter.cardDeleteConfirm(id: cardId, code: code)
            RM.request(request, kind: CardDeleteResponse.self, completion: completion)
        }
    }
}
