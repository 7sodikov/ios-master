//
//  NetworkService+Payment.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Payment {
        static func serviceCategories(completion: @escaping (ResponseResult<[ServiceCategoryResponse], AppError>) -> Void) {
            let request = PaymentRouter.serviceCategories
            RM.request(request, kind: [ServiceCategoryResponse].self, completion: completion)
        }
        
        static func services(for categoryId: Int, searchString: String?, completion: @escaping (ResponseResult<[ServiceResponse], AppError>) -> Void) {
            let request = PaymentRouter.services(categoryId: categoryId, searchString: searchString)
            RM.request(request, kind: [ServiceResponse].self, completion: completion)
        }
        
        static func servicesFilter(for searchText: String, completion: @escaping (ResponseResult<[ServicesFilterResponse], AppError>) -> Void) {
            let request = PaymentRouter.servicesFilter(searchText: searchText)
            RM.request(request, kind: [ServicesFilterResponse].self, completion: completion)
        }
        
        static func paymentPrepare(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any], completion: @escaping (ResponseResult<PaymentPrepareResponse, AppError>) -> Void) {
            let request = PaymentRouter.paymentPrepare(senderCardId: senderCardId,
                                                       serviceId: serviceId,
                                                       serviceType: serviceType,
                                                       fields: fields)
            RM.request(request, kind: PaymentPrepareResponse.self, completion: completion)
        }
        
        static func paymentGetInfo(senderCardId: UInt64, serviceId: Int64, serviceType: Int, fields: [String: Any],infoId: Int, completion: @escaping (ResponseResult<PaymentGetInfoResponse, AppError>) -> Void) {
            let request = PaymentRouter.serviceGetInfo(senderCardId: senderCardId,
                                                       serviceId: serviceId,
                                                       serviceType: serviceType,
                                                       fields: fields, infoId: infoId)
            RM.request(request, kind: PaymentGetInfoResponse.self, completion: completion)
        }
        
        static func getServiceOne(serviceId: String, serviceType: String, completion: @escaping(ResponseResult<CService, AppError>)->Void) {
            let request = PaymentRouter.service(serviceId: serviceId, serviceType: serviceType)
            RM.request(request, kind: CService.self, completion: completion)
        }
        
        static func paymentPay(for token: String, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = PaymentRouter.paymentPay(token: token)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        static func paymentConfirm(with token: String, smsCode: String, completion: @escaping (ResponseResult<P2PConfirmResponse, AppError>) -> Void) {
            let request = PaymentRouter.paymentConfirm(token: token, smsCode: smsCode)
            RM.request(request, kind: P2PConfirmResponse.self, completion: completion)
        }
    }
}
