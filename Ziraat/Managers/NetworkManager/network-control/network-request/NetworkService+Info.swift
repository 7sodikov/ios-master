//
//  NetworkService+Info.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Info {
        static func getMainCurrencyList(completion: @escaping (ResponseResult<CurrencyListResponse, AppError>) -> Void) {
            let request = InfoRouter.getAllCurrencyList
            RM.request(request, kind: CurrencyListResponse.self, completion: completion)
        }
        
        static func getAllCurrencyList(completion: @escaping (ResponseResult<CurrencyListResponse, AppError>) -> Void) {
            let request = InfoRouter.getAllCurrencyList
            RM.request(request, kind: CurrencyListResponse.self, completion: completion)
        }
        
        static func getMainExchangeRates(completion: @escaping (ResponseResult<ExchangeRatesResponse, AppError>) -> Void) {
            let request = InfoRouter.getMainExchangeRates
            RM.request(request, kind: ExchangeRatesResponse.self, completion: completion)
        }
        
        static func getAllExchangeRatesList(completion: @escaping (ResponseResult<ExchangeRatesResponse, AppError>) -> Void) {
            let request = InfoRouter.getAllExchangeRates
            RM.request(request, kind: ExchangeRatesResponse.self, completion: completion)
        }
        
    }
}
