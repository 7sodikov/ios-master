//
//  NetworkService+AutoPayment.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire


extension NetworkService {
    final class AutoPayment {
        
        @discardableResult init() {}
        
        func fetchServiceCategories(completion: @escaping (ResponseResult<[CServiceCategory], AppError>) -> Void) {
            let request = AutoPaymentRouter.serviceCategory
            RM.request(request, kind: [CServiceCategory].self, completion: completion)
        }
        
        func fetchServices(categoryId: String, completion: @escaping (ResponseResult<[CAutoPayService], AppError>) -> Void) {
            let request = AutoPaymentRouter.autopayServices(categoryId)
            RM.request(request, kind: [CAutoPayService].self, completion: completion)
        }
        
        func fetchAutopays(status: String?, completion: @escaping (ResponseResult<[CAutoPayment], AppError>) -> Void) {
            let request = AutoPaymentRouter.autopayList(status)
            RM.request(request, kind: [CAutoPayment].self, completion: completion)
        }
        
        func fetchAutopayHistories(autopayId: String?, status: String?, completion: @escaping (ResponseResult<[CAutoPaymentHistory], AppError>) -> Void) {
            let request = AutoPaymentRouter.autopayHistoryList(autopayId, status)
            RM.request(request, kind: [CAutoPaymentHistory].self, completion: completion)
        }
        
        func createAutoPayment(params: CAutoPayment.Params, completion: @escaping (ResponseResult<CAutoPayment, AppError>) -> Void) {
            let request = AutoPaymentRouter.create(params: params)
            RM.request(request, kind: CAutoPayment.self, completion: completion)
        }
        
        func changeStatusAutoPayment(autoPayId: Int, status: String, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = AutoPaymentRouter.changeStatus(autoPayId: autoPayId, status: status)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        func deleteAutoPayment(autoPayId: Int, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = AutoPaymentRouter.delete(autoPayId: autoPayId)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
    }
}
