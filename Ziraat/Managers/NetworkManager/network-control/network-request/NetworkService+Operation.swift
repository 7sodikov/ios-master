//
//  NetworkService+Operation.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Operation {
        static func operationPrepare(prepareType: OperationPrepareType,
                                     sender: String?, senderId: UInt64?,
                                     senderBranch: String?,
                                     receiver: String?, receiverId: UInt64?,
                                     receiverBranch: String?,
                                     paramNum: Int64?,
                                     paramStr: String?,
                                     paramBool: Bool?,
                                     amount: Double, completion: @escaping (ResponseResult<OperationPrepareResponse, AppError>) -> Void) {
            let request = OperationRouter.operationPrepare(prepareType: prepareType,
                                                           sender: sender, senderId: senderId,
                                                           senderBranch: senderBranch,
                                                           receiver: receiver, receiverId: receiverId,
                                                           receiverBranch: receiverBranch,
                                                           paramNum: paramNum,
                                                           paramStr: paramStr,
                                                           paramBool: paramBool,
                                                           amount: amount)
            RM.request(request, kind: OperationPrepareResponse.self, completion: completion)
        }
        
        static func operationTransfer(token: String, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = OperationRouter.operationTransfer(token: token)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        static func operationConfirm(token: String, code: String, completion: @escaping (ResponseResult<P2PConfirmResponse, AppError>) -> Void) {
            let request = OperationRouter.operationConfirm(token: token, code: code)
            RM.request(request, kind: P2PConfirmResponse.self, completion: completion)
        }
        
        static func operationSMSResend(token: String, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = OperationRouter.operationSMSResend(token: token)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        static func operationHistory(operationType: OperationType, size: Int, completion: @escaping (ResponseResult<[HistoryResponse], AppError>) -> Void) {
            let request = OperationRouter.operationHistory(operationType: operationType, size: size)
            RM.request(request, kind: [HistoryResponse].self, completion: completion)
        }
        
        static func paymentHistory(operationType: OperationType, size: Int, completion: @escaping (ResponseResult<[CLastPayment], AppError>) -> Void) {
            let request = OperationRouter.operationHistory(operationType: operationType, size: size)
            RM.request(request, kind: [CLastPayment].self, completion: completion)
        }
        
        static func operationReceipt(operationId: Int, completion: @escaping (ResponseResult<OperationReceiptResponse, AppError>) -> Void) {
            let request = OperationRouter.operationReceipt(operationId: operationId)
            RM.request(request, kind: OperationReceiptResponse.self, completion: completion)
        }
        
        /// type : "UZCARD"/"HUMO" OR null
        static func receiverList(keyword: String?, type: String?, page: Int, size: Int, completion: @escaping (ResponseResult<ReceiverList, AppError>) -> Void) {
            let request = OperationRouter.receiverList(keyword: keyword, type: type, page: page, size: size)
            RM.request(request, kind: ReceiverList.self, completion: completion)
        }
    }
}
