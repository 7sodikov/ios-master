//
//  NetworkService+Accounts.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Accounts {
        static func accountsList(completion: @escaping (ResponseResult<AccountsListResponse, AppError>) -> Void) {
            let request = AccountsRouter.accountsList
            RM.request(request, kind: AccountsListResponse.self, completion: completion)
        }
        
        static func accountBalance(completion: @escaping (ResponseResult<AccountsBalanceResponse, AppError>) -> Void) {
            let request = AccountsRouter.accountBalance
            RM.request(request, kind: AccountsBalanceResponse.self, completion: completion)
        }
        
        static func accountStatements(id: String, from:String, to: String, completion: @escaping (ResponseResult<AccountStatementsResponse, AppError>) -> Void) {
            let request = AccountsRouter.accountsStatements(Id: id, from: from, to: to)
            RM.request(request, kind: AccountStatementsResponse.self, completion: completion)
        }
        
        static func accountOwner(accountNumber: String, branch: String, completion: @escaping (ResponseResult<CardOwnerResponse, AppError>) -> Void) {
            let request = AccountsRouter.accountOwner(accountNumber: accountNumber, branch: branch)
            RM.request(request, kind: CardOwnerResponse.self, completion: completion)
        }
    }
}
