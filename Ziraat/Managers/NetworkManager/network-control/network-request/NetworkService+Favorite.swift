//
//  NetworkService+Favorite.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Favorite {
        static func favoriteList(completion: @escaping (ResponseResult<FavoriteListResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoriteList
            RM.request(request, kind: FavoriteListResponse.self, completion: completion)
        }
        
        static func favoritePaymentList(completion: @escaping (ResponseResult<PaymentPrepareResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoritePaymentList
            RM.request(request, kind: PaymentPrepareResponse.self, completion: completion)
        }
        
        static func favoriteOperationList(completion: @escaping (ResponseResult<PaymentPrepareResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoriteOperationList
            RM.request(request, kind: PaymentPrepareResponse.self, completion: completion)
        }
        
        static func favoriteDelete(favoriteId: UInt64, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoriteDelete(favoriteId: favoriteId)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        static func favoriteEdit(favoriteId: UInt64, title: String, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoriteEdit(favoriteId: favoriteId, title: title)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        static func favoriteAdd(operationId: Int, title: String, completion: @escaping (ResponseResult<FavoriteAddResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoriteAdd(operationId: operationId, title: title)
            RM.request(request, kind: FavoriteAddResponse.self, completion: completion)
        }
        
        static func favoritePayment(favoriteId: Int, completion: @escaping (ResponseResult<PaymentPrepareResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoritePayment(favoriteId: favoriteId)
            RM.request(request, kind: PaymentPrepareResponse.self, completion: completion)
        }
        
        static func favoriteOperation(favoriteId: Int, completion: @escaping (ResponseResult<PaymentPrepareResponse, AppError>) -> Void) {
            let request = FavoriteRouter.favoriteOperation(favoriteId: favoriteId)
            RM.request(request, kind: PaymentPrepareResponse.self, completion: completion)
        }
    }
}

