//
//  NetworkService+Exchange.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Exchange {
        static func exchangePrepare(prepareType: OperationPrepareType,
                                    sender: String?,
                                    senderId: String?,
                                    senderBranch: String?,
                                    receiver: String?,
                                    receiverId: String?,
                                    receiverBranch: String?,
                                    paramNum: Int64?,
                                    paramStr: String?,
                                    paramBool: Bool?,
                                    amount: Double, completion: @escaping (ResponseResult<ExchangePrepareResponse, AppError>) -> Void) {
            let request = ExchangeRouter.conversionPrepare(prepareType: prepareType,
                                                           sender: sender,
                                                           senderId: senderId,
                                                           senderBranch: senderBranch,
                                                           receiver: receiver,
                                                           receiverId: receiverId,
                                                           receiverBranch: receiverBranch,
                                                           paramNum: paramNum,
                                                           paramStr: paramStr,
                                                           paramBool: paramBool,
                                                           amount: amount)
            RM.request(request, kind: ExchangePrepareResponse.self, completion: completion)
        }
    }
}
