//
//  NetworkService+Deposit.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Deposit {
        static func depositList(completion: @escaping (ResponseResult<DepositsResponse, AppError>) -> Void) {
            let request = DepositsRouter.getDeposits
            RM.request(request, kind: DepositsResponse.self, completion: completion)
        }
        
        static func depositBalance(completion: @escaping (ResponseResult<DepositsBalanceResponse, AppError>) -> Void) {
            let request = DepositsRouter.depositBalance
            RM.request(request, kind: DepositsBalanceResponse.self, completion: completion)
        }
        
    }
}
