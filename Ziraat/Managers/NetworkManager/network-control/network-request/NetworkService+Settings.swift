//
//  NetworkService+Settings.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

extension NetworkService {
    final class Settings {
        static func getLangList(completion: @escaping (ResponseResult<LanguageListResponse, AppError>) -> Void) {
            let request = SettingsRouter.getLangList
            RM.request(request, kind: LanguageListResponse.self, completion: completion)
        }
        
        static func getReferencesNameList(completion: @escaping (ResponseResult<ReferenceListResponse, AppError>) -> Void) {
            let request = SettingsRouter.getReferencesNameList
            RM.request(request, kind: ReferenceListResponse.self, completion: completion)
        }
    }
}
