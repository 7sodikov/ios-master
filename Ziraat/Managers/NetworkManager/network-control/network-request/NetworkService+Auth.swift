//
//  NetworkService+Auth.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Alamofire

final class NetworkService {
    
    private init() { }
    
    final class Auth {
        
      
//        static func restorePassword(phone: String, password: String, confirmPassword: String, secretWord: String, code: String = "", completion: @escaping (Result<SingleMessage, AppError>) -> Void) {
//            let request = AF.request(AuthRouter.restorePassword(phone: phone, password: password, confirmPassword: confirmPassword, code: code, secretWord: secretWord))
//            request.responseJSON { response in
//                NetworkMonitor().monitor(response, kind: SingleMessage.self, completion: completion)
//            }
//        }
        
        static func loginInit(completion: @escaping (ResponseResult<LoginInitResponse, AppError>) -> Void) {
            let request = AuthRouter.loginInit
            RM.request(request, kind: LoginInitResponse.self, completion: completion)
        }
        
        static func login(token: String, username: String, password: String, completion: @escaping (ResponseResult<AuthLoginResponse, AppError>) -> Void) {
            let request = AuthRouter.login(token: token, username: username, password: password)
            RM.request(request, kind: AuthLoginResponse.self, completion: completion)
        }
        
        static func loginConfirm(token: String, code: String, username: String, password: String, completion: @escaping (ResponseResult<LoginConfirmResponse, AppError>) -> Void) {
            let request = AuthRouter.loginConfirm(token: token, code: code, username: username, password: password)
            RM.request(request, kind: LoginConfirmResponse.self, completion: completion)
        }
        
        static func loginResendCaptcha(token: String, completion: @escaping (ResponseResult<LoginInitResponse, AppError>) -> Void) {
            let request = AuthRouter.loginResendCaptcha(token: token)
            RM.request(request, kind: LoginInitResponse.self, completion: completion)
        }
        
        static func loginResendSMS(token: String, completion: @escaping (ResponseResult<LoginResendSMSResponse, AppError>) -> Void) {
            let request = AuthRouter.loginResendSMS(token: token)
            RM.request(request, kind: LoginResendSMSResponse.self, completion: completion)
        }
        
        static func logout(completion: @escaping (ResponseResult<LogoutResponse, AppError>) -> Void) {
            let request = AuthRouter.logout
            RM.request(request, kind: LogoutResponse.self, completion: completion)
        }
        
        static func changePassword(oldPassword: String, newPassword: String, repeatPassword: String, completion: @escaping (ResponseResult<SingleMessageResponse, AppError>) -> Void) {
            let request = AuthRouter.changePassword(oldPassword: oldPassword, newPassword: newPassword, repeatPassword: repeatPassword)
            RM.request(request, kind: SingleMessageResponse.self, completion: completion)
        }
        
        static func passwordRecoveryInit(completion: @escaping (ResponseResult<LoginInitResponse, AppError>) -> Void) {
            let request = AuthRouter.passwordRecoveryInit
            RM.request(request, kind: LoginInitResponse.self, completion: completion)
        }
        
        static func passwordRecoveryResendSMS(token: String, completion: @escaping (ResponseResult<LoginResendSMSResponse, AppError>) -> Void) {
            let request = AuthRouter.passwordRecoveryResendSMS(token: token)
            RM.request(request, kind: LoginResendSMSResponse.self, completion: completion)
        }
        
        static func passwordRecoveryResendCaptcha(token: String, completion: @escaping (ResponseResult<LoginInitResponse, AppError>) -> Void) {
            let request = AuthRouter.passwordRecoveryResendCaptcha(token: token)
            RM.request(request, kind: LoginInitResponse.self, completion: completion)
        }
        
        static func passwordRecoveryStepOne(token: String, phone: String, pinfl: String, captcha: String, completion: @escaping (ResponseResult<LogoutResponse, AppError>) -> Void) {
            let request = AuthRouter.passwordRecoveryStepOne(token: token, phone: phone, pinfl: pinfl, captcha: captcha)
            RM.request(request, kind: LogoutResponse.self, completion: completion)
        }
        
        static func passwordRecoveryStepTwo(token: String, card: String, expiry: String, completion: @escaping (ResponseResult<LoginResendSMSResponse, AppError>) -> Void) {
            let request = AuthRouter.passwordRecoveryStepTwo(token: token, card: card, expiry: expiry)
            RM.request(request, kind: LoginResendSMSResponse.self, completion: completion)
        }
        
        static func passwordRecoveryStepThree(token: String, code: String, completion: @escaping (ResponseResult<LogoutResponse, AppError>) -> Void) {
            let request = AuthRouter.passwordRecoveryStepThree(token: token, code: code)
            RM.request(request, kind: LogoutResponse.self, completion: completion)
        }
        
        static func passwordRecoveryStepFour(token: String, newPassword: String, repeatPassword: String, completion: @escaping (ResponseResult<LogoutResponse, AppError>) -> Void) {
            let request = AuthRouter.passwordRecoveryStepFour(token: token, newPassword: newPassword, repeatPassword: repeatPassword)
            RM.request(request, kind: LogoutResponse.self, completion: completion)
        }
    }
}
