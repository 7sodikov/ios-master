//
//  AppURL.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

fileprivate let IP_HOME = "192.168.0.100"

enum AppURL: String {
    case v1 = "v1"
    case v2 = "v2"

    fileprivate var mockDomain: String { mockingDomain() }
    
    var version: String { self.rawValue }
    
    var prefix: String {
        switch self {
        case .v1, .v2:
            return "\(URLConstant.domain)/api/\(version)"
        }
    }
    
    var mockPrefix: String {
        switch self {
        case .v1, .v2:
            return "\(mockDomain)/api/\(version)"
        }
    }
    
    func mockingDomain() -> String {
        var port = ""
        switch self {
        case .v1:
            port = "6000"
        case .v2:
            port = "7000"
        }
        #if targetEnvironment(simulator)
        return "http://localhost:\(port)"
        #else
        return "http://\(IP_HOME):\(port)"
        #endif
    }
}


// MARK: - Routes

extension AppURL {
    
    /// Authentication URL collection
    var auth: AuthV1 {
        switch self {
        case .v1, .v2:
            return AuthV1(version: self)
        }
    }
    
    var settings: SettingsV1 {
        switch self {
        case .v1, .v2:
            return SettingsV1(version: self)
        }
    }
    
    var info: InfoV1 {
        switch self {
        case .v1, .v2:
            return InfoV1(version: self)
        }
    }
    
    var card: CardV1 {
        switch self {
        case .v1, .v2:
            return CardV1(version: self)
        }
    }
    
    var accounts: AccountsV1 {
        switch self {
        case .v1, .v2:
            return AccountsV1(version: self)
        }
    }
    
    var deposits: DepositsV1 {
        switch self {
        case .v1, .v2:
            return DepositsV1(version: self)
        }
    }
    
    var loans: LoansV1 {
        switch self {
        case .v1, .v2:
            return LoansV1(version: self)
        }
    }
    
    var payment: PaymentV1 {
        switch self {
        case .v1, .v2:
            return PaymentV1(version: self)
        }
    }
    
    var operation: OperationV1 {
        switch self {
        case .v1, .v2:
            return OperationV1(version: self)
        }
    }
    
    var user: UserV1 {
        switch self {
        case .v1, .v2:
            return UserV1(version: self)
        }
    }
    
    var favorite: FavoriteV1 {
        switch self {
        case .v1, .v2:
            return FavoriteV1(version: self)
        }
    }
    
    var bankProducts: BankProductsV1 {
        switch self {
        case .v1, .v2:
            return BankProductsV1(version: self)
        }
    }
    
    var notifications: NotificationsV1 {
        switch self {
        case .v1, .v2:
            return NotificationsV1(version: self)
        }
    }
    
    var exchange: ExchangeV1 {
        switch self {
        case .v1, .v2:
            return ExchangeV1(version: self)
        }
    }
    
    var map: MapV1 {
        switch self {
        case .v1, .v2:
            return MapV1(version: self)
        }
    }
}
