//
//  AppURL+AutoPayment.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

// MAKR: - need to see this extension
extension AppURL {
    
    struct AutoPayment: DomainProtocol {
        let router: AutoPaymentRouter
        var version: AppURL = .v1
        
        var domain: String {
            switch router {
            case .serviceCategory:
                return "\(version.prefix)/autopay/services/category"
            case .autopayServices:
                return "\(version.prefix)/autopay/services"
            case .autopayList:
                return "\(version.prefix)/autopay/list"
            case .autopayHistoryList:
                return "\(version.prefix)/autopay/history/list"
            case .create:
                return "\(version.prefix)/autopay/create"
            case .changeStatus:
                return "\(version.prefix)/autopay/status"
            case .delete:
                return "\(version.prefix)/autopay/delete"
            }
        }
        
    }
    
}

protocol DomainProtocol {
    var domain: String { get }
}

