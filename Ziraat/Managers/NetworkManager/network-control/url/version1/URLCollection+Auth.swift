//
//  URLCollection+Auth.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

class AuthV1 {
    var version: AppURL
    
    var loginInit: String
    let login: String
    var loginConfirm: String
    var loginResendCaptcha: String
    var loginResendSMS: String
    let logout: String
    var changePassword: String
    var passwordRecoveryInit: String
    var passwordRecoveryResendSMS: String
    var passwordRecoveryResendCaptcha: String
    var passwordRecoveryStepOne: String
    var passwordRecoveryStepTwo: String
    var passwordRecoveryStepThree: String
    var passwordRecoveryStepFour: String
    
    init(version: AppURL) {
        self.version = version
        
        loginInit = "\(version.prefix)/auth/login"
        login = "\(version.prefix)/auth/login"
        loginConfirm = "\(version.prefix)/auth/login/confirm"
        loginResendCaptcha = "\(version.prefix)/auth/login/captcha"
        loginResendSMS = "\(version.prefix)/auth/login/sms"
        logout = "\(version.prefix)/auth/logout"
        changePassword = "\(version.prefix)/auth/password/change"
        passwordRecoveryInit = "\(version.prefix)/auth/password/recovery/init"
        passwordRecoveryResendSMS = "\(version.prefix)/auth/password/recovery/sms"
        passwordRecoveryResendCaptcha = "\(version.prefix)/auth/password/recovery/captcha"
        passwordRecoveryStepOne = "\(version.prefix)/auth/password/recovery/step/1"
        passwordRecoveryStepTwo = "\(version.prefix)/auth/password/recovery/step/2"
        passwordRecoveryStepThree = "\(version.prefix)/auth/password/recovery/step/3"
        passwordRecoveryStepFour = "\(version.prefix)/auth/password/recovery/step/4"
    }
}
