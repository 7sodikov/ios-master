//
//  URLCollection+Cards.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

class CardsV1 {
    
    var version: AppURL

    var getCardList: String
    var getCardBalanceConversion: String
    var getCardBalanceIdentificator: String
    
    init(version: AppURL) {
        self.version = version
        
        self.getCardList = "\(version.prefix)/card/list"
        self.getCardBalanceConversion = "\(version.prefix)/card/balance"
        self.getCardBalanceIdentificator = "\(version.prefix)/card/balance"
    }
}
