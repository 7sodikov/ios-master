//
//  URLCollection+Settings.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//


class SettingsV1 {
    
    var version: AppURL

    var getLangList: String
    var getReferencesNameList: String
    
    init(version: AppURL) {
        self.version = version
        
        getLangList = "\(version.prefix)/lang"
        getReferencesNameList = "\(version.prefix)/references/list"
    }
}
