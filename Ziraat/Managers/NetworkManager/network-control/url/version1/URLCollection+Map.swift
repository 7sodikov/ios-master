//
//  URLCollection+Map.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class MapV1 {
    
    var version: AppURL

    var map: String
    var mapAtm: String
    var mapBranch: String
    var mapExchange: String
    
    init(version: AppURL) {
        self.version = version

        map = "\(version.prefix)/map/"
        mapAtm = "\(version.prefix)/map/atm"
        mapBranch = "\(version.prefix)/map/branch"
        mapExchange = "\(version.prefix)/map/exchange"
    }
}
