//
//  URLCollection+Info.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

class InfoV1 {
    
    var version: AppURL

    var getMainCurrencyList: String
    var getAllCurrencyList: String
    var getMainExchangeRates: String
    var getAllExchangeRates: String
    
    init(version: AppURL) {
        self.version = version
        
        getMainCurrencyList = "\(version.prefix)/info/currency"
        getAllCurrencyList = "\(version.prefix)/info/currency/all"
        getMainExchangeRates = "\(version.prefix)/info/exchange_rates"
        getAllExchangeRates = "\(version.prefix)/info/exchange_rates/all"
    }
}
