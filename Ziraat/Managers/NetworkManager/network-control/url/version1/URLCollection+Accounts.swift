//
//  URLCollection+Accounts.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

class AccountsV1 {
    
    var version: AppURL

    var accountsList: String
    var accountBalance: String
    var accountStatements: String
    var accountOwner: String
    
    init(version: AppURL) {
        self.version = version
        
        self.accountsList = "\(version.prefix)/account/list"
        self.accountBalance = "\(version.prefix)/account/balance"
        self.accountStatements = "\(version.prefix)/account/statements"
        self.accountOwner = "\(version.prefix)/account/owner"
    }
}
