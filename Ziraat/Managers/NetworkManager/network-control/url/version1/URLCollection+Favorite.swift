//
//  URLCollection+Favorite.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class FavoriteV1 {
    var version: AppURL
    
    var favoriteList: String
    var favoritePaymentList: String
    var favoriteOperationList: String
    var favoriteDelete: String
    var favoriteEdit: String
    var favoriteAdd: String
    var favoritePayment: String
    var favoriteOperation: String
    
    init(version: AppURL) {
        self.version = version
        favoriteList = "\(version.prefix)/favorite/list"
        favoritePaymentList = "\(version.prefix)/favorite/payment/list"
        favoriteOperationList = "\(version.prefix)/favorite/operation/list"
        favoriteDelete = "\(version.prefix)/favorite/delete"
        favoriteEdit = "\(version.prefix)/favorite/edit"
        favoriteAdd = "\(version.prefix)/favorite/add"
        favoritePayment = "\(version.prefix)/favorite/payment"
        favoriteOperation = "\(version.prefix)/favorite/operation"
    }
}
