//
//  URLCollection+BankProducts.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/13/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class BankProductsV1 {
    
    var version: AppURL

    var loanList: String
    var depositList: String
    var getLoanDetails: String
    var getDepositDetails: String
    
    init(version: AppURL) {
        self.version = version
        
        loanList = "\(version.prefix)/product/loan/list"
        depositList = "\(version.prefix)/product/deposit/list"
        getLoanDetails = "\(version.prefix)/product/loan/one"
        getDepositDetails = "\(version.prefix)/product/deposit/one"
    }
}
