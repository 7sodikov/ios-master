//
//  URLCollection+Exchange.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class ExchangeV1 {
    var version: AppURL
    
    var exchangePrepare: String
    
    init(version: AppURL) {
        self.version = version
        
        exchangePrepare = "\(version.prefix)/operation/exchange/%@"
    }
}
