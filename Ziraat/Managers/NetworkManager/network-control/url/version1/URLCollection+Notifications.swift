//
//  URLCollection+Notifications.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class NotificationsV1 {
    
    var version: AppURL

    var notificationsList: String
    var notificationTopic: String
    var notificationId: String
    
    init(version: AppURL) {
        self.version = version

        notificationsList = "\(version.prefix)/notification/page?page=%d&size=%d"
        notificationTopic = "\(version.prefix)/notification/topic"
        notificationId = "\(version.prefix)/notification/%d"
    }
}
