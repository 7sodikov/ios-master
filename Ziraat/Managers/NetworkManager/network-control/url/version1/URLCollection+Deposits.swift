//
//  URLCollection+Deposits.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

class DepositsV1 {
    var version: AppURL
    
    var depositList: String
    var depositBalance: String
    
    init(version: AppURL) {
        self.version = version
        depositList = "\(version.prefix)/deposit/list"
        depositBalance = "\(version.prefix)/deposit/balance"
    }
}
