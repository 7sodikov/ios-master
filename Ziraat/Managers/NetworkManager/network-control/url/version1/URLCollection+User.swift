//
//  URLCollection+User.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class UserV1 {
    
    var version: AppURL

    var getUserMeInfo: String
    var authHistory: String
    
    init(version: AppURL) {
        self.version = version
        
        getUserMeInfo = "\(version.prefix)/users/me/info"
        authHistory = "\(version.prefix)/auth/history"
    }
}
