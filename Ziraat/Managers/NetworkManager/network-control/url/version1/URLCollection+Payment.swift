//
//  URLCollection+Payment.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class PaymentV1 {
    var version: AppURL
    
    var serviceCategories: String
    var services: String
    var servicesFilter: String
    var paymentPrepare: String
    var paymentPay: String
    var paymentConfirm: String
    var serviceGetInfo: String
    var service: String
    
    init(version: AppURL) {
        self.version = version
        
        serviceCategories = "\(version.prefix)/payment/services/category"
        services = "\(version.prefix)/payment/services"
        servicesFilter = "\(version.prefix)/payment/services/filter"
        paymentPrepare = "\(version.prefix)/payment/prepare"
        paymentPay = "\(version.prefix)/payment/pay"
        paymentConfirm = "\(version.prefix)/payment/confirm"
        serviceGetInfo = "\(version.prefix)/payment/services/info"
        service = "\(version.prefix)/payment/services/one"
    }
}
