//
//  URLCollection+Loans.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation


class LoansV1 {
    var version: AppURL
    
    var loanList: String
    var loanBalance: String
    var loanCalculate: String
    var loanOwner: String
    var statement: String
    var graghic: String
    
    init(version: AppURL) {
        self.version = version
        loanList = "\(version.prefix)/loan/list"
        loanBalance = "\(version.prefix)/loan/balance"
        loanCalculate = "\(version.prefix)/info/loan/calc"
        loanOwner = "\(version.prefix)/loan/owner"
        statement = "\(version.prefix)/loan/statements"
        graghic = "\(version.prefix)/loan/graph"
    }
}
