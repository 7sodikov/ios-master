//
//  URLCollection+Card.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

class CardV1 {
    var version: AppURL
    
    var cardsList: String
    var cardBalance: String
    var cardBalanceByID: String
    var cardOwner: String
    var cardAdd: String
    var cadHistory: String
    var cardBlock : String
    var cardUnblock : String
    var cardDelete : String
    var cardDeleteConfirm : String
    
    init(version: AppURL) {
        self.version = version
        cardsList = "\(version.prefix)/card/list"
        cardBalance = "\(version.prefix)/card/balance"
        cardBalanceByID = "\(version.prefix)/card/balance"
        cardOwner = "\(version.prefix)/card/owner"
        cardAdd = "\(version.prefix)/card/add"
        cadHistory = "\(version.prefix)/card/history"
        cardBlock = "\(version.prefix)/card/block"
        cardUnblock = "\(version.prefix)/card/unblock"
        cardDelete = "\(version.prefix)/card/delete"
        cardDeleteConfirm = "\(version.prefix)/card/delete/confirm"
    }
}
