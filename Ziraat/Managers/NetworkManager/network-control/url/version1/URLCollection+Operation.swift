//
//  URLCollection+Operation.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class OperationV1 {
    var version: AppURL
    
    var operationPrepare: String
    var operationTransfer: String
    var operationConfirm: String
    var operationSMSResend: String
    var operationHistory: String
    var operationReceipt: String
    var receiverList: String
    
    init(version: AppURL) {
        self.version = version
        
        operationPrepare = "\(version.prefix)/operation/prepare/%@"
        operationTransfer = "\(version.prefix)/operation/transfer"
        operationConfirm = "\(version.prefix)/operation/confirm"
        operationSMSResend = "\(version.prefix)/operation/sms/resend"
        operationHistory = "\(version.prefix)/operation/history/%@"
        operationReceipt = "\(version.prefix)//operation/receipt"
        receiverList = "\(version.prefix)/receiver/history/list"
    }
}
