//
//  P2PConfirmResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct P2PConfirmResponse: Codable {
    var operationId: Int
    var receipt: String
}
