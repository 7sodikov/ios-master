//
//  MetaType.swift
//  AppNetwork
//
//  Created by botirjon nasridinov on 30.04.2020.
//

import Foundation


enum MetaType: Codable {
    case int(Int)
    case string(String)
    case double(Double)
    case bool(Bool)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let val = try? container.decode(Int.self) {
            self = .int(val)
        } else if let val = try? container.decode(Double.self) {
            self = .double(val)
        } else if let val = try? container.decode(String.self) {
            self = .string(val)
        } else if let val = try? container.decode(Bool.self) {
            self = .bool(val)
        } else {
            throw DecodingError.typeMismatch(MetaType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "An unsupported meta type for decoding"))
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .int(let int):
            try container.encode(int)
        case .string(let string):
            try container.encode(string)
        case .double(let double):
            try container.encode(double)
        case .bool(let bool):
            try container.encode(bool)
        }
    }
    
    func asString() -> String {
        switch self {
        case .int(let val):
            return "\(val)"
        case .string(let val):
            return val
        case .double(let val):
            return "\(val)"
        case .bool(let val):
            return "\(val)"
        }
    }
    
    func asInt() -> Int? {
        guard case let .int(val) = self else {
            return nil
        }
        return val
    }
    
    func asDouble() -> Double? {
        guard case let .double(val) = self else {
            return nil
        }
        return val
    }
    
    func asBool() -> Bool? {
        guard case let .bool(val) = self else {
            return nil
        }
        return val
    }
}

public protocol DictionaryConvertible: Encodable {
    func getParameters() throws -> [String: Any]?
}

public extension DictionaryConvertible {
    func getParameters() throws -> [String: Any]? {
        let encoder = JSONEncoder()
        let data = try encoder.encode(self)
        let json = try JSONSerialization.jsonObject(with: data, options: [])
        return json as? [String: Any]
    }
}
