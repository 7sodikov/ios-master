//
//  FavoriteListResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class FavoriteListResponse: Codable {
    var operations: [FavoriteListOperationsResponse]
    var payments: [FavoriteListPaymentsResponse]
    
    var isEmpty: Bool {
        payments.isEmpty && operations.isEmpty
    }
}

class FavoriteListOperationsResponse: Codable {
    let favoriteID: Int
    var title: String
    let logo: String
    let createdAt: String
    let favoriteType: String
    let operationType, operationMode: String
    let senderID: Int?
    let senderTitle, senderOwner: String
    let sender: String
    let senderExpire: String?
    let senderBranch: String
    let receiverID: Int?
    let receiverTitle, receiverOwner: String
    let receiver: String
    let receiverExpire: String?
    let receiverBranch: String
    let currency: String
    let amount: Int
    
    enum CodingKeys: String, CodingKey {
        case favoriteID = "favoriteId"
        case title, logo, createdAt, favoriteType, operationType, operationMode
        case senderID = "senderId"
        case senderTitle, senderOwner, sender, senderExpire, senderBranch
        case receiverID = "receiverId"
        case receiverTitle, receiverOwner, receiver, receiverExpire, receiverBranch, currency, amount
    }
}

class FavoriteListPaymentsResponse: Codable {
    let favoriteID: Int
    var title: String
    let logo: String
    let createdAt, favoriteType, operationType, operationMode: String
    let senderID: Int
    let senderTitle, senderOwner, sender, senderExpire: String
    let serviceID: String?
    let serviceType, serviceCategory: Int
    let serviceTitle, fieldData: String
    let fieldValueList: [ServiceFieldResponse]
    let currency: String
    let amount: Int
    
    enum CodingKeys: String, CodingKey {
        case favoriteID = "favoriteId"
        case title, logo, createdAt, favoriteType, operationType, operationMode
        case senderID = "senderId"
        case senderTitle, senderOwner, sender, senderExpire
        case serviceID = "serviceId"
        case serviceType, serviceCategory, serviceTitle, fieldData, fieldValueList, currency, amount
    }
}
