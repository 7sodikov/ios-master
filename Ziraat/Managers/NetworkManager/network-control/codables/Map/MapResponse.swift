//
//  MapResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct MapResponse: Codable {
    var mapPoints: [MapPoint]?
}

struct MapPoint: Codable {
    var id: Int?
    var latitude: String
    var longitude: String
    var email: String?
    var fax: String?
    var phone: String?
    var open_time: String?
    var close_time: String?
    var break_start_time: String?
    var break_end_time: String?
    var logo_url: String?
    var img_url: String?
    var type: MapPointType?
//    var card_types: String? // should be an enum
//    var operations: String // should be an enum
    var name: String?
    var address: String?
    var position: String?
    var weekend: String?
}

enum MapPointType: String, Codable {
    case atm = "ATM"
    case branch = "BRANCH"
    case exchange = "EXCHANGE"
}
