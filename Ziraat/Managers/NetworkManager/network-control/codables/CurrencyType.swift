//
//  CurrencyType.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

enum CurrencyType: String {
    case uzs
    case rub
    case gbp
    case usd
    case `try`
    case eur
    
    var data: (code: String, label: String) {
        switch self {
        case .uzs: return ("000", "UZS")
        case .rub: return ("643", "RUB")
        case .gbp: return ("826", "GBP")
        case .usd: return ("840", "USD")
        case .try: return ("949", "TRY")
        case .eur: return ("978", "EUR")
        }
    }
    
    static func type(for code: String?) -> CurrencyType? {
        if code == "000" { return .uzs }
        if code == "643" { return .rub }
        if code == "826" { return .gbp }
        if code == "840" { return .usd }
        if code == "949" { return .try }
        if code == "978" { return .eur }
        
        return nil
    }
}
