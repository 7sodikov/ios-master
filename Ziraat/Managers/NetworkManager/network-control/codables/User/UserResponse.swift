//
//  UserResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct UserResponse: Codable {
    var username: String
    var firstName: String
    var lastName: String
    var middleName: String
    var fullName: String
    var phone: String
    var email: String
    var lastLoginDay: String
    var lastLoginTime: String
    var lastLoginFailedDay: String
    var lastLoginFailedTime: String
    var failCount: Int
}

struct AuthHistoriesResponse: Codable {
    var history: [AuthHistoryResponse]
}

struct AuthHistoryResponse: Codable {
    var status: String
    var date: String
    var time: String
    var ip: String
}
