//
//  DepositsBalanceResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct DepositsBalanceResponse: Codable {
    var deposits: [DepositBalanceResponse]?
    var count: Int
}

struct DepositBalanceResponse: Codable {
    var id: Int
    var branch: String
    var currency: String
    var saldo: Int
    var convert: [ConvertResponse]?
    
    var coreObject: DepositBalance {
        DepositBalance(
            id: id,
            branch: branch,
            currency: (CurrencyType.type(for: currency) ?? .uzs),

            saldo: saldo,
            convert: (convert ?? []).map(\.coreObject)
        )
        
    }
}

struct DepositBalance {
    let id: Int
    let branch: String
    let currency: CurrencyType
    let saldo: Int
    let convert: [CardBalance.Convert]
}
