//
//  DepositsResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct DepositsResponse: Codable {
    var deposits: [DepositResponse]?
    var count: Int
}

struct DepositResponse: Codable {
    var id: Int!
    var branch: String!
    var name: String
    var currency: String!
    var saldo: Int
    var dateOpen: String
    var dateClose: String
    var interest: Int
    var accounts: [AccountResponse]
    var condition: DepositConditionResponse?
}

struct DepositConditionResponse: Codable {
    var unit: String?
    var unitTerm: String?
}
