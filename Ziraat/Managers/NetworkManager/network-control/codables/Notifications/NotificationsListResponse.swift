//
//  NotificationsListResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/22/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct NotificationsListResponse: Codable {
    var content: [NotificationItemResponse]
    var first: Bool?
    var totalElements: Int?
    var totalPages: Int?
    var size: Int?
    var numberOfElements: Int?
}

struct NotificationItemResponse: Codable {
    let shortText, title: String?
    let id: Int?
    let fullText, createDate, createTime, type: String?
    let headingImage: String?
    let sendingStatus: String?
}
