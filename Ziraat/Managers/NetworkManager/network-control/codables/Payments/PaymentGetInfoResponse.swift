//
//  PaymentGetInfoResponse.swift
//  Ziraat
//
//  Created by Muhriddinbek Samidov on 14/06/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct PaymentGetInfoResponse : Codable {
    
    let finished : Bool?
    let messages : [ObjectKeyValueModel]?
        
    enum CodingKeys: String, CodingKey {
        
        case finished = "finished"
        case messages = "messages"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        finished = try values.decodeIfPresent(Bool.self, forKey: .finished)
        messages = try values.decodeIfPresent([ObjectKeyValueModel].self, forKey: .messages)
    }
    
}

struct ObjectKeyValueModel : Codable {
    let key : String?
    let label : String?
    let text : String?
    let type : String?
    
    enum CodingKeys: String, CodingKey {
        
        case key = "name"
        case label = "title"
        case text = "value"
        case type = "type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        label = try values.decodeIfPresent(String.self, forKey: .label)
        text = try values.decodeIfPresent(String.self, forKey: .text)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
    
}
