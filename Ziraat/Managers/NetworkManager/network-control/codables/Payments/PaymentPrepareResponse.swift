//
//  PaymentPrepareResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/21/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct PaymentPrepareResponse: Codable {
    var senderPan: String
    var amount: Int64
    var commission: Int64
    var total: Int64
    var token: String
//    var fields: [String: Any]
    var fieldValues: [PaymentPrepareFieldValueResponse]
    var details: [PaymentPrepareFieldValueResponse]?
}

struct PaymentPrepareFieldValueResponse: Codable {
    var name: String
    var title: String
    var value: String
    var type: String
}
