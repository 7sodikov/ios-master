//
//  CLastPayment.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/19/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct CLastPayment: Codable {
    var serviceId: String
    var operationId: Int
    var currency: String
    var sender: String
    var icon: String
    var fieldValueList: [ServiceFieldResponse]
    var operationType: String
    var amount: Int64
    var operationMode: OperationMode
    var date: String
    var receiverOwner: String
    var time: String
    var receiverBranch: String
    var total: Int64
    var operationCode: String
    var senderOwner: String
    var senderId: UInt64
    var senderPan: String
    var status: String
    var serviceType: Int
    var commission: Int
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.serviceId = (try? container.decodeIfPresent(String.self, forKey: .serviceId)) ?? ""
        self.operationId = (try? container.decodeIfPresent(Int.self, forKey: .operationId)) ?? .zero
        self.currency = (try? container.decodeIfPresent(String.self, forKey: .currency)) ?? ""
        self.sender = (try? container.decodeIfPresent(String.self, forKey: .sender)) ?? ""
        self.icon = (try? container.decodeIfPresent(String.self, forKey: .icon)) ?? ""
        self.fieldValueList = (try? container.decodeIfPresent([ServiceFieldResponse].self, forKey: .fieldValueList)) ?? [ServiceFieldResponse]()
        self.operationType = (try? container.decodeIfPresent(String.self, forKey: .operationType)) ?? ""
        self.amount = (try? container.decodeIfPresent(Int64.self, forKey: .amount)) ?? .zero
        self.operationMode = (try? container.decodeIfPresent(OperationMode.self, forKey: .operationMode)) ?? .p2s
        self.date = (try? container.decodeIfPresent(String.self, forKey: .date)) ?? ""
        self.receiverOwner = (try? container.decodeIfPresent(String.self, forKey: .receiverOwner)) ?? ""
        self.time = (try? container.decodeIfPresent(String.self, forKey: .time)) ?? ""
        self.receiverBranch = (try? container.decodeIfPresent(String.self, forKey: .receiverBranch)) ?? ""
        self.total = (try? container.decodeIfPresent(Int64.self, forKey: .total)) ?? .zero
        self.operationCode = (try? container.decodeIfPresent(String.self, forKey: .operationCode)) ?? ""
        self.senderOwner = (try? container.decodeIfPresent(String.self, forKey: .senderOwner)) ?? ""
        self.senderId = (try? container.decodeIfPresent(UInt64.self, forKey: .senderId)) ?? .zero
        self.senderPan = (try? container.decodeIfPresent(String.self, forKey: .senderPan)) ?? ""
        self.status = (try? container.decodeIfPresent(String.self, forKey: .status)) ?? ""
        self.serviceType = (try? container.decodeIfPresent(Int.self, forKey: .serviceType)) ?? .zero
        self.commission = (try? container.decodeIfPresent(Int.self, forKey: .commission)) ?? .zero
    }
}
