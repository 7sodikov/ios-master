//
//  ServicesFilterResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct ServicesFilterResponse: Codable {
    var categoryTitle: String
    var categoryId: Int64
    var categoryLogoUrl: String
    var filteredServices: [ServiceResponse]
}
