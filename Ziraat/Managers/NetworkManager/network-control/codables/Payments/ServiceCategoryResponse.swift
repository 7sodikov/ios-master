//
//  ServiceCategoryResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct ServiceCategoryResponse: Codable {
    var logoUrl: String
    var title: String
    var id: Int
}
