//
//  CService.swift
//  Ziraat
//
//  Created by Jasur Amirov on 7/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

// MARK: - Service
struct CService: Codable {
    let serviceID, minAmount, maxAmount: Int
    let title: String
    let logoURL: String
    let serviceType: Int
    let autoPay: Bool
    let infoServiceID: Int?
    let infoServiceFields: [String]
    let fields: [CField]

    enum CodingKeys: String, CodingKey {
        case serviceID = "serviceId"
        case minAmount, maxAmount, title
        case logoURL = "logoUrl"
        case serviceType, autoPay
        case infoServiceID = "infoServiceId"
        case infoServiceFields, fields
    }
}

// MARK: - Field
extension CService {
    
    struct CField: Codable {
        let name, title: String
        let fieldRequired, readOnly: Bool
        let type: FieldType?
        let mask: String?
        let values: [CValue]
        let valueTitles: [String]

        enum CodingKeys: String, CodingKey {
            case name, title
            case fieldRequired = "required"
            case readOnly, type, mask, values, valueTitles
        }
    }
    
    enum FieldType: String, Codable {
        case money = "MONEY"
        case datePopup = "DATEPOPUP"
        case regexBox = "REGEXBOX"
        case phone = "PHONE"
        case string = "STRING"
        case select = "SELECT"
    }
    
}


// MARK: - Value
extension CService {
    
    struct CValue: Codable {
        let title, value: String
        let subitems: [CValue]
    }
    
}
