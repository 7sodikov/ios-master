//
//  ServiceResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ServiceResponse: Codable {
    let serviceID, minAmount, maxAmount: Int
    let title: String
    let logoURL: String
    let serviceType: Int
    let autoPay: Bool
    let infoServiceID: Int?
    let infoServiceFields: [String]
    let fields: [ServiceFieldResponse]
    
    enum CodingKeys: String, CodingKey {
           case serviceID = "serviceId"
           case minAmount, maxAmount, title
           case logoURL = "logoUrl"
           case serviceType, autoPay
           case infoServiceID = "infoServiceId"
           case infoServiceFields, fields
       }
    
}

enum ServiceFieldType: String, Codable {
    case money = "MONEY"
    case datePopup = "DATEPOPUP"
    case regexBox = "REGEXBOX"
    case phone = "PHONE"
    case string = "STRING"
    case select = "SELECT"
    
    
    static func getType(for str: String) -> ServiceFieldType? {
        ServiceFieldType(rawValue: str)
    }
}

class ServiceFieldResponse: Codable {
    let name, title, type: String
    let fieldValueListRequired, readOnly: Bool
    let mask: String?
    let defaultValue: String?
    let values: [ServiceFieldValue]?
    let valueTitles: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name, title, type
        case fieldValueListRequired = "required"
        case readOnly, mask, values, valueTitles
        case defaultValue
    }
    
    var enumType: ServiceFieldType? {
        ServiceFieldType.getType(for: type)
    }
    
    private var _regions: [ServiceFieldValue] = []
    //    key: regionId, value: dictsricts
    private var _districts: [String: [ServiceFieldValue]] = .init()
    private var _fieldSections: [ServiceFieldValue] = []
    
    public var regions: [ServiceFieldValue] {
        if _regions.isEmpty {
            _regions = values?.filter(\.subitems.isNotEmpty) ?? []
        }
        return _regions
    }
    
    func getDistricts(at regionID: String) -> [ServiceFieldValue] {
        if _districts.isEmpty {
            _districts = .init(grouping: _regions, by: \.value).mapValues { $0.map(\.subitems).reduce(.init(), +) }
        }
        return _districts[regionID] ?? []
    }
    
    
    var fieldSections: [ServiceFieldValue] {
        if _fieldSections.isEmpty {
            _fieldSections = values?.filter(\.subitems.isEmpty) ?? []
        }
        return _fieldSections
    }
    
    var regionViewModels: [SingleLabelCellViewModel] {
        _regions.map { .init(text: $0.title) }
    }
    
    func districtViewModels(at regionID: String) -> [SingleLabelCellViewModel] {
        (_districts[regionID] ?? []).map { .init(text: $0.title) }
    }
    
    var personViewModel: [SingleLabelCellViewModel] {
        _fieldSections.map { .init(text: $0.title) }
    }
}


struct ServiceFieldValue: Codable {
    let title, value: String
    let subitems: [ServiceFieldValue]
}
