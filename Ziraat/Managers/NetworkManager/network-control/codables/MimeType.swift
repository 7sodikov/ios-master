//
//  MimeType.swift
//  AppNetwork
//
//  Created by botirjon nasridinov on 07.05.2020.
//

import Foundation


enum MimeType: String {
    case imageJPEG = "image/jpeg"
    case imagePNG = "image/png"
    case textPlain = "text/plain"
    case applicationJSON = "application/json"
    case multipartFormData = "multipart/form-data"
}
