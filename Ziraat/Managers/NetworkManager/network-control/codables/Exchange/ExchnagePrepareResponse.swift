//
//  ExchnagePrepareResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/26/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Rswift
import Foundation

struct ExchangePrepareResponse: Codable {
    var sender: String?
    var senderBranch: String?
    var senderOwner: String?
    
    var receiver: String?
    var receiverBranch: String?
    var receiverOwner: String?
    
    var debit: UInt64?
    var debitCurrency: String?
    var credit: UInt64?
    var creditCurrency: String?
    var rate: UInt64?
    var commission: UInt64?
    var total: UInt64?
    var token: String?
    
//    var detailedViewModels: [DetailCenteredCellViewModel] {
//
//        let sender = (senderOwner ?? "") + "\n" + (self.sender?.makeReadableCardNumber ?? "")
//        let receiver = (receiverOwner ?? "") + "\n" + (self.receiver?.makeReadableCardNumber ?? "")
//        let creditSum = readableAmount(credit) + " " + currencyLabel(creditCurrency)
//        let debitSum = readableAmount(rate) + " " + currencyLabel(debitCurrency)
//        let totalSum = readableAmount(total) + " " + currencyLabel(debitCurrency)
//
//        let models:[(title: StringResource, value: String)] = [
//            (RS.lbl_payer, sender),
//            (RS.lbl_receiver, receiver),
//            (RS.lbl_currency, creditSum),
//            (RS.lbl_exchange, debitSum),
//            (RS.lbl_total_amount, totalSum),
//        ]
//        return models.map { .init(title: $0.title.localized(), value: $0.value) }
//    }
//
//    func detailedViewModels(foreignCyrrencyAmount: String, exchangeRate: String, totalAmount: String) -> [DetailCenteredCellViewModel] {
//        let sender = (senderOwner ?? "") + "\n" + (self.sender?.makeReadableCardNumber ?? "")
//        let receiver = (receiverOwner ?? "") + "\n" + (self.receiver?.makeReadableCardNumber ?? "")
//
//        let models:[(title: StringResource, value: String)] = [
//            (RS.lbl_payer, sender),
//            (RS.lbl_receiver, receiver),
//            (RS.lbl_currency, foreignCyrrencyAmount),
//            (RS.lbl_exchange, exchangeRate),
//            (RS.lbl_total_amount, totalAmount),
//        ]
//        return models.map { .init(title: $0.title.localized(), value: $0.value) }
//    }
    
    func detaisViewModels(foreignCyrrencyAmount: String, exchangeRate: String, totalAmount: String) -> [DetailsCellViewModel] {
        typealias Tupe2 = (title: String, value: String)
        
        let sender: Tupe2 = ((senderOwner ?? ""),(self.sender?.makeReadableCardNumber ?? ""))
        let receiver: Tupe2 = ((receiverOwner ?? ""),(self.receiver ?? ""))
        
        let models:[(header: StringResource, title: String, value: String, canEdit: Bool)] = [
            (RS.lbl_payer, sender.title,sender.value, true),
            (RS.lbl_receiver, receiver.title,receiver.value, true),
            (RS.lbl_currency, "",foreignCyrrencyAmount, true),
            (RS.lbl_exchange, "",exchangeRate, false),
            (RS.lbl_total_amount, "",totalAmount, false),
        ]
        
        return models.map {
            .init(headerTitle: $0.header.localized(), valueTitle: $0.title, value: $0.value, canEdit: $0.canEdit)
        }
    }
    
    func readableAmount(_ amount: UInt64?) -> String {
        ((Double(amount ?? 0)) / 100).description.makeReadableAmount
    }
    
    func currencyLabel(_ currency: String?) -> String {
        CurrencyType.type(for: currency)?.data.label ?? ""
    }
}
