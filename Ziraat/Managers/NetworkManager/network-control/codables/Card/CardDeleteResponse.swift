//
//  CardDeleteResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct CardDeleteResponse: Codable {
    var message: String?
}
