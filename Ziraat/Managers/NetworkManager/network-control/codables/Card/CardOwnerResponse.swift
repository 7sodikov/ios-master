//
//  CardOwnerResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/30/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class CardOwnerResponse: Codable {
    var ownerName: String
    var additions: Additional
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.ownerName = (try? container.decode(String.self, forKey: .ownerName)) ?? ""
        self.additions = (try? container.decode(Additional.self, forKey: .additions)) ?? .empty()
    }
    
    enum CodingKeys: String, CodingKey {
        case ownerName, additions
    }
}

extension CardOwnerResponse {
    
    struct Additional: Codable {
        
        var type, typeX2, typeX3: String
        var img, imgX2, imgX3: String
        
        internal init(type: String, typeX2: String, typeX3: String, img: String, imgX2: String, imgX3: String) {
            self.type = type
            self.typeX2 = typeX2
            self.typeX3 = typeX3
            self.img = img
            self.imgX2 = imgX2
            self.imgX3 = imgX3
        }
        
        internal init?(_ additional: CardAdditionsResponse?) {
            guard let info = additional else { return nil }
            self.img = info.img ?? ""
            self.imgX2 = info.imgX2 ?? ""
            self.imgX3 = info.imgX3 ?? ""
            self.type = info.type ?? ""
            self.typeX2 = info.typeX2 ?? ""
            self.typeX3 = info.typeX3 ?? ""
        }
        
        static func empty() -> Additional {
            .init(type: "", typeX2: "", typeX3: "", img: "", imgX2: "", imgX3: "")
        }
    }
    
}

