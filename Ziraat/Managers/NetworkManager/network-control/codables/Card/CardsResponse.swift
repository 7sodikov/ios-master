//
//  CardsResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class CardsResponse: Codable {
    var cards: [CardResponse]
    var count: Int
}

class CardResponse: Codable {
    var cardId: UInt64
    var status: String?
    var pan: String?
    var expire: String?
    var type: String?
    var owner: String?
    var account: String?
    var branch: String?
    var branchName: String?
    var title: String?
    var description: String?
    var canBlock:Bool?
    var additions: CardAdditionsResponse?
    
    var card: Card? {
        Card(card: self)
    }
}

class CardAdditionsResponse: Codable {
    var type: String?
    var typeX2: String?
    var typeX3: String?
    
    var img: String?
    var imgX2: String?
    var imgX3: String?
    
    var bank: String?
    var bankX2: String?
    var bankX3: String?
}

struct Card {
    
    let cardId: String
    let status: String
    let pan: String
    let expire: String
    let type: String
    let owner: String
    let account: String
    let branch: String
    let branchName: String
    let title: String
    let description: String
    let canBlock: Bool
    let additions: Card.Additional
    
    var balance: Double = .zero
    var currency: CurrencyType = .uzs
    
    internal init?(card: CardResponse) {
        self.cardId = card.cardId.description
        self.status = card.status ?? ""
        self.pan = card.pan ?? ""
        self.expire = card.expire ?? ""
        self.type = card.type ?? ""
        self.owner = card.owner ?? ""
        self.account = card.account ?? ""
        self.branch = card.branch ?? ""
        self.branchName = card.branchName ?? ""
        self.title = card.title ?? ""
        self.description = card.description ?? ""
        self.canBlock = card.canBlock ?? false
        self.additions = Additional(card.additions) ?? .empty()
    }
    
    mutating func update(balance: Double, currency: CurrencyType) {
        self.balance = balance / 100
        self.currency = currency
    }
    
    func isEqual(with cardId: String) -> Bool {
        self.cardId == cardId
    }
    
    var isUzb: Bool { currency == .uzs }
}

extension Card {
    
    struct Additional {
        
        var type, typeX2, typeX3: String
        var img, imgX2, imgX3: String
        var bank, bankX2, bankX3: String
        
        internal init(type: String, typeX2: String, typeX3: String, img: String, imgX2: String, imgX3: String, bank: String, bankX2: String, bankX3: String) {
            self.type = type
            self.typeX2 = typeX2
            self.typeX3 = typeX3
            self.img = img
            self.imgX2 = imgX2
            self.imgX3 = imgX3
            self.bank = bank
            self.bankX2 = bankX2
            self.bankX3 = bankX3
        }
        
        internal init?(_ additional: CardAdditionsResponse?) {
            guard let info = additional else { return nil }
            self.img = info.img ?? ""
            self.imgX2 = info.imgX2 ?? ""
            self.imgX3 = info.imgX3 ?? ""
            self.type = info.type ?? ""
            self.typeX2 = info.typeX2 ?? ""
            self.typeX3 = info.typeX3 ?? ""
            self.bank = info.bank ?? ""
            self.bankX2 = info.bankX2 ?? ""
            self.bankX3 = info.bankX3 ?? ""
        }
        
        static func empty() -> Additional {
            .init(type: "", typeX2: "", typeX3: "", img: "", imgX2: "", imgX3: "", bank: "", bankX2: "", bankX3: "")
        }
    }
    
}
