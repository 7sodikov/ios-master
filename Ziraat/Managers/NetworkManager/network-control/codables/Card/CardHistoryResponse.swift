//// Header

import UIKit

class CardHistoryResponse: Codable {
    var history: [CardHistoryItemResponse]
    
}

class CardHistoryItemResponse: Codable {
    var transType:String
    var transDate:String
    var transTime:String
    var amount:Double = 0
    var merchantName:String?
    var fee:Double = 0
    var currency : String
    var isCredit:Bool
    
//    var amountStr : String{
//        return ""
////        return self.amount.currencyFormattedStr() + " " + self.currency
//    }
}
