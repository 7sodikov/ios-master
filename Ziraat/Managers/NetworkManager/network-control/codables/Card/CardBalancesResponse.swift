//
//  CardBalancesResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class CardBalancesResponse: Codable {
    var cards: [CardBalanceResponse]?
    var count: Int
}

class CardBalanceResponse: Codable {
    var cardId: Int
    var currency: String?
    var balance: Int
    var trueBalance: Bool
    var convert: [ConvertResponse]?
    
    var cardBalance: CardBalance {
        .init(cardBalance: self)
    }
}

class CardBalanceConvert: Codable {
    var currency: String?
    var amount: Int
    var rate: Int
}

struct CardBalance {
    var cardId: String
    var currency: CurrencyType
    var balance: Double
    var trueBalance: Bool
    var convert: [Convert]
    
    init(cardBalance: CardBalanceResponse) {
        self.cardId = cardBalance.cardId.description
        self.currency = CurrencyType.type(for: cardBalance.currency) ?? .uzs
        self.balance = Double(cardBalance.balance)
        self.trueBalance = cardBalance.trueBalance
        self.convert = (cardBalance.convert ?? []).map(\.coreObject)
    }
    
}

extension CardBalance {
    
    struct Convert {
        var currency: String?
        var amount: Double
        var rate: Int
        
        init(info: ConvertResponse) {
            self.currency = info.currency ?? ""
            self.amount = Double(info.amount ?? .zero)
            self.rate = info.rate ?? .zero
        }
    }
    
}
