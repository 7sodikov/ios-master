//
//  LoanListResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/13/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct LoanListResponse: Codable {
    var minAmount: Double?
    var maxAmount: Double?
    var id: Int?
    var consumerProperties: String?
    var name: String?
    var goal: String?
    var currency: String?
    var term: Int?
    var termUnit: String?
    var rates: [LoanRateResponse]?
    var backgroundImgUrl: String?
    var documents: String?
    var initialFee: Double?
    var requirements: String?
    var additionalInfo: String?
}

struct LoanRateResponse: Codable {
    var minTerm: Int?
    var minTermUnit: String?
    var maxTerm: Int?
    var maxTermUnit: String?
    var rate: Float?
    var id: Int?
//    var minAmount: Int
//    var maxAmount: Int
//    var initialFee: Double
//    var backgroundImgUrl: String
//    var requirements: String
//    var documents: String
//    var additionalInfo: String
}
