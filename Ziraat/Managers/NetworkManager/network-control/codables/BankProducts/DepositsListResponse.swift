//
//  DepositsListResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/13/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct DepositsListResponse: Codable {
    var id: Int?
    var name: String?
    var currency: String?
    var term: Int?
    var termUnit: String?
    var rate: String?
    var minAmount: Int?
    var maxAmount: Int?
    var requirements: String?
    var backgroundImgUrl: String?
    var documents: String?
    var additionalInfo: String?
}

enum CurrencyTypes: String, Codable {
    case uzs = "UZS"
    case usd = "USD"
    case eur = "EUR"
}

enum TermUnit: String, Codable {
    case month = "MONTH"
}
