//
//  ResponseContainer.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ResponseContainer<Model> {
    var data: Model
    var httpStatusCode: Int
    var userInfo: [String: Any?]?
    
    init(data: Model, statusCode: Int) {
        self.data = data
        self.httpStatusCode = statusCode
    }
}
