//
//  CAutoPayment.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct CAutoPaymentHistory: Codable {
    var historyId: Int?
    var prepareDate: String?
    var payDate: String?
    var status: AutoPaymentStatus?
    var result: String?
    var autoPay: CAutoPayment?
    
    var isValid: Bool {
        historyId != nil && status != nil && autoPay != nil && autoPay!.isValid
    }
}

struct CAutoPayment: Codable {
    let autoPayID: Int?
    let title: String?
    let payerID: Int?
    let payerNumber, payerBranch, payerOwner, serviceID: String?
    let serviceType: Int?
    let frequency: Frequency?
    let dayOf, amount: Int?
    let status: AutoPaymentStatus?
    let fields: [CBalanceField]?

    enum CodingKeys: String, CodingKey {
        case autoPayID = "autoPayId"
        case title
        case payerID = "payerId"
        case payerNumber, payerBranch, payerOwner
        case serviceID = "serviceId"
        case serviceType, frequency, dayOf, amount, status, fields
    }
    
    var isValid: Bool {
        autoPayID != nil && status != nil
    }
    
    var numberCardText: String {
        RS.lbl_card_number.localized() + " " + (payerNumber ?? "")
    }
    
    var frequencyText: String {
        RS.lbl_Periodicity.localized() + " " + (frequency?.localized ?? "")
    }
    
    var amountText: String {
        RS.lbl_amount.localized() + " " + ((amount ?? 0) / 100).description.makeReadableAmount
    }
    
}

// MARK: - BalanceField

extension CAutoPayment {
    struct CBalanceField: Codable {
        let name, title, value, type: String
    }
}


extension CAutoPayment {
    // MARK: - BalanceWelcome
    struct Params: DictionaryConvertible {
        let title: String
        let payerID: Int
        let payerNumber, payerBranch, serviceID: String
        let serviceType: Int
        let frequency: String
        let dayOf, amount: Int
        let fields: Dictionary<String, String>

        enum CodingKeys: String, CodingKey {
            case title
            case payerID = "payerId"
            case payerNumber = "payerNumber"
            case payerBranch = "payerBranch"
            case serviceID = "serviceId"
            case frequency, amount, fields
            case dayOf = "dayOf"
            case serviceType = "serviceType"
        }
    }
}
