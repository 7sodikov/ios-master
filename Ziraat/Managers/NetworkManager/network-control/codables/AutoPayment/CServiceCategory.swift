//
//  CServiceCategory.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct CServiceCategory: Codable {
    var id: Int?
    var title, logoUrl: String?
    var autoPay: Bool?
    
    var coreObject: ServiceCategory? {
        guard let id = self.id else { return nil }
        return .init(id: id, title: self.title ?? "", logo: logoUrl ?? "", autoPay: autoPay ?? false)
    }
}

