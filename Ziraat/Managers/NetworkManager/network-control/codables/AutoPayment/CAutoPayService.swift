//
//  CAutoPayService.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/31/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

struct CAutoPayService: Codable {
    let serviceID, minAmount, maxAmount: Int
    let title: String
    let logoURL: String
    let serviceType: Int
    let autoPay: Bool
    let fields: [CField]
    
    var coreObject: AutoPayService {
        .init(
            serviceID: serviceID,
            minAmount: minAmount,
            maxAmount: maxAmount,
            title: title,
            logoURL: logoURL,
            serviceType: serviceType,
            autoPay: autoPay,
            fields: fields.map(\.coreObject)
        )
    }

    enum CodingKeys: String, CodingKey {
        case serviceID = "serviceId"
        case minAmount, maxAmount, title
        case logoURL = "logoUrl"
        case serviceType, autoPay, fields
    }
}


// MARK: - CAutoPayService Field
extension CAutoPayService {
    struct CField: Codable {
        let name, title: String
        let fieldRequired, readOnly: Bool
        let type: String
        let mask: String?
        
        var coreObject: AutoPayService.Field {
            .init(
                name: name,
                title: title,
                fieldRequired: fieldRequired,
                readOnly: readOnly,
                type: FieldType(rawValue: type) ?? .STRING,
                mask: mask
            )
        }

        enum CodingKeys: String, CodingKey {
            case name, title
            case fieldRequired = "required"
            case readOnly, type, mask
        }
    }
}



