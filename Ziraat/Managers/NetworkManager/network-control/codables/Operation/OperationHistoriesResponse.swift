//
//  OperationHistoriesResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct OperationHistoriesResponse: Codable {
    var history: [HistoryResponse]?
    var totalItems: Int?
    var totalPages: Int
    var currentPage: Int?
}

struct HistoryResponse: Codable {
    var operationId: Int?
    var date: String?
    var time: String?
    var sender: String?
    var senderId: Int?
    var senderOwner: String?
    var senderPan: String?
    var senderBranch: String?
    var receiver: String?
    var receiverId: Int?
    var receiverOwner: String?
    var receiverPan: String?
    var receiverBranch: String?
    var serviceId: String?
    var serviceType: Int?
    var amount: Int
    var commission: Int?
    var total: Int?
    var status: String?
    var operationType: String?
    var operationCode: String?
    var operationMode: OperationMode?
    var icon: String?
    var currency: String
}

enum OperationMode: String, Codable {
    case p2p
    case p2a
    case a2p
    case p2d
    case d2p
    case p2l
    case p2s
    case a2l
    case acp
    case pca
    
//    case p2p = "p2p"
//    case p2a = "p2a"
//    case a2p = "a2p"
//    case p2d = "p2d"
//    case d2p = "d2p"
//    case p2l = "p2l"
//    case p2s = "p2s"
//    case a2l = "a2l"
}
