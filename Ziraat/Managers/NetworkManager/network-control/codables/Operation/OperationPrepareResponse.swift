//
//  OperationPrepareResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/9/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import Rswift

struct OperationPrepareResponse: Codable {
    var sender: String?
    var senderBranch: String?
    var senderOwner: String?
    var receiver: String?
    var receiverBranch: String?
    var receiverOwner: String?
    var amount: Double
    var commission: Double
    var total: Double
    var token: String
}

extension OperationPrepareResponse {
    var prepareDetails: [DetailsCellViewModel] {
        
        let senderName = senderOwner ?? ""
        let sender = self.sender ?? ""
        let receiverName = receiverOwner ?? ""
        let receiver = self.receiver ?? ""
        let amount = (self.amount / 100).description.makeReadableAmount
        let commission = (self.commission / 100).description
        let totalAmount = (total / 100).description.makeReadableAmount
        
        let models:[(header: StringResource, title: String, value: String, canEdit: Bool)] = [
            (RS.lbl_payer, senderName, sender, true),
            (RS.lbl_receiver, receiverName, receiver, true),
            (RS.lbl_amount, "", amount, true),
            (RS.lbl_commission, "", commission, false),
            (RS.lbl_total_amount, "", totalAmount, false),
        ]
        
        return models.map {
            .init(headerTitle: $0.header.localized(), valueTitle: $0.title, value: $0.value, canEdit: $0.canEdit)
        }
    }
}
