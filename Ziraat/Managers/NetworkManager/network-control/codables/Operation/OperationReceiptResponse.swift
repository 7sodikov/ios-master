//
//  OperationReceiptResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct OperationReceiptResponse: Codable {
    var receipt: String
}
