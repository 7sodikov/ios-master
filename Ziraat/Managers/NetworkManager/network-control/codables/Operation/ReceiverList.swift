//
//  ReceiverList.swift
//  Ziraat
//
//  Created by Jasur Amirov on 5/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

// MARK: - ReceiverList
struct ReceiverList: Codable {
    let totalItems, totalPages, currentPage: Int
    let receivers: [BalanceReceiver]
}

// MARK: - BalanceReceiver
struct BalanceReceiver: Codable {
    let id, pan, owner, cardType: String
    let additions: BalanceAdditions
    
    enum CodingKeys: String, CodingKey {
        case id = "receiverUnique"
        case pan = "receiverMask"
        case owner = "receiverOwner"
        case cardType = "receiverType"
        case additions
    }
}

// MARK: - BalanceAdditions
struct BalanceAdditions: Codable {
    let type, bank: String
}
