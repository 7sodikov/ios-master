//
//  SingleMessageResponse.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

struct SingleMessageResponse: Codable {
    var message: String?
    var operationId: Int?
    var receipt: String?
}
