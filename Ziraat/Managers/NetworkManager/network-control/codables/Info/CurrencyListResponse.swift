//
//  CurrencyListResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation


struct CurrencyListResponse: Codable {
    var currencies: [CurrencyResponse]?
    var count: Int?
}
