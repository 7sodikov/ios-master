//
//  CurrencyResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct CurrencyResponse: Codable {
    var code: String?
    var codeLabel: String?
    var name: String?
    
    enum CodingKeys: String, CodingKey { // Our Saviour
        case code = "code"
        case codeLabel = "codeLabel"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decodeIfPresent(String.self, forKey: .code)
        codeLabel = try container.decodeIfPresent(String.self, forKey: .codeLabel)
        name = try container.decodeIfPresent(String.self, forKey: .name)
    }
}
