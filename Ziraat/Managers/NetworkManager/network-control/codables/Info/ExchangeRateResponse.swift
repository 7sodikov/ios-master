//
//  ExchangeRateResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct ExchangeRateResponse: Codable {
    var courseType: String?
    var courseTypeName: String?
    var currency: String?
    var scale: Int?
    var course: Int?
    var flag: String?
    
    var exchangeRate: ExchangeRate {
        .init(info: self)
    }
}

struct ExchangeRate {
    
    var courseType: String
    var courseTypeName: String
    var currency: String
    var scale: Int
    var course: Int
    var flag: String
    
    internal init(info: ExchangeRateResponse) {
        self.courseType = info.courseType ?? ""
        self.courseTypeName = info.courseTypeName ?? ""
        self.currency = info.currency ?? ""
        self.scale = info.scale ?? .zero
        self.course = info.course ?? .zero
        self.flag = info.flag ?? ""
    }
    
}

extension ExchangeRate {
    
    enum CourceType: String {
        case buy = "5"
        case cell = "4"
        case cb = "1"
        case other
    }
    
}
