//
//  LoginResendSMSResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 7/30/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation


class LoginResendSMSResponse: Codable {
    var message: String?
}
