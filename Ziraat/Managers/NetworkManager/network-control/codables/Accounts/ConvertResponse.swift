//
//  ConvertResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ConvertResponse: Codable {
    var currency: String?
    var amount: Int?
    var rate: Int?
    
    var coreObject: CardBalance.Convert {
        .init(info: self)
    }
}
