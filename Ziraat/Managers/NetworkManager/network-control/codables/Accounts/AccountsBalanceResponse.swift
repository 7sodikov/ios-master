//
//  AccountsBalanceResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class AccountsBalanceResponse: Codable {
    var accounts: [AccountBalanceResponse]
    var count: Int
}

class AccountBalanceResponse: Codable {
    var saldo: UInt?
    var number: String?
    var branch: String?
    var currency: String?
    var balance: UInt?
    var branchName: String?
    var convert: [ConvertResponse]?
    
    var coreObject: AccountBalance {
        AccountBalance(
            saldo: Int(saldo ?? 0),
            number: number,
            branch: branch,
            currency: .type(for: currency),
            balance: Double(balance ?? 0),
            branchName: branchName,
            convert: (convert ?? []).map(\.coreObject)
        )
    }
}

struct AccountBalance {
    internal init(saldo: Int?, number: String?, branch: String?, currency: CurrencyType?, balance: Double?, branchName: String?, convert: [CardBalance.Convert]?) {
        self.saldo = saldo
        self.number = number
        self.branch = branch
        self.currency = currency
        self.balance = balance
        self.branchName = branchName
        self.convert = convert
    }

    
    let saldo: Int?
    let number: String?
    let branch: String?
    let currency: CurrencyType?
    let balance: Double?
    let branchName: String?
    let convert: [CardBalance.Convert]?
}
