//
//  AccountsListResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class AccountsListResponse: Codable {
    var accounts: [AccountResponse]
    var count: Int
}

class AccountResponse: Codable {
    var number: String
    var branch: String?
    var name: String
    var customerName: String?
    var currency: String
    var balance: Double
    var dateOpen: String
    var dateClose: String?
    var accGroupId: Int
    var branchName: String?
    var stateName: String?
    var sout: Int?
    var state: Int?
    var saldo: Int?
    
    var account: Account {
        .init(account: self)
    }
}

struct Account {
    var number: String
    var branch: String?
    var name: String
    var customerName: String?
    var currency: CurrencyType
    var balance: Double
    var dateOpen: String
    var dateClose: String?
    var accGroupId: Int
    var branchName: String?
    var stateName: String?
    var sout: Int?
    var state: Int?
    var saldo: Int?
    
    internal init(account: AccountResponse) {
        self.number = account.number
        self.branch = account.branch ?? ""
        self.name = account.name
        self.customerName = account.customerName
        self.currency = CurrencyType.type(for: account.currency) ?? .uzs
        self.balance = account.balance / 100
        self.dateOpen = account.dateOpen
        self.dateClose = account.dateClose ?? ""
        self.accGroupId = account.accGroupId
        self.branchName = account.branchName ?? ""
        self.stateName = account.stateName ?? ""
        self.sout = account.sout
        self.state = account.state
        self.saldo = account.saldo
    }
    
    var isUZB: Bool {
        currency == .uzs
    }
    
}
