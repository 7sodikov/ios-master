//// Header

import UIKit

class LoanStatementsResponse: Codable {
    var statements: [LoanStatementResponse]?
}

class LoanStatementResponse: Codable {
    var accCll : String
    var bankCll : String
    var docNum : String
    var typeDoc : String
    var bankCo : String
    var accCo : String
    var purpose : String
    var sout : Double
    var vdate : String
    var sin : Double
    var sdt : Double
    var sct : Double
    var ldate : String
}
