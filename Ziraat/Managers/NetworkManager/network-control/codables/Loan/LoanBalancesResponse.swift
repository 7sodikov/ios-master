//
//  LoanBalancesResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct LoanBalancesResponse: Codable {
    var loans: [LoanBalanceResponse]?
    var count: Int?
}

struct LoanBalanceResponse: Codable {
    var id: Int
    var branch: String
    var currency: String
    var loanAmount: Int
    var principalAmount: Int
    var branchName: String?
    var isError: Bool
    var convert: [ConvertResponse]?
    
    var coreObject: LoanBalance {
        LoanBalance(
            id: id,
            branch: branch,
            currency: CurrencyType.type(for: currency) ?? .uzs,
            loanAmount: loanAmount,
            principalAmount: principalAmount,
            branchName: branchName,
            isError: isError,
            convert: (convert ?? []).map(\.coreObject)
        )

    }
}

struct LoanBalance {
    var id: Int
    var branch: String
    var currency: CurrencyType
    var loanAmount: Int
    var principalAmount: Int
    var branchName: String?
    var isError: Bool
    var convert: [CardBalance.Convert]?
}
    

