//
//  LoanCalculateResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct LoanCalculateResponse: Codable {
    var date: String
    var principalAmount: Int
    var interestAmount: Int
    var totalAmount: Int
    var saldo: Int
}
