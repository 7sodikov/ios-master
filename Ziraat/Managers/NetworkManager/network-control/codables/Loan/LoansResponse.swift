//
//  LoansResponse.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

struct LoansResponse: Codable {
    var loans: [LoanResponse]?
    var count: Int?
}

struct LoanResponse: Codable {
    var id: Int
    var branch: String
    var currency: String
    var amount: Double
    //var idDate: String
    var dateClose: String
    var target: String
    var ldDate: String
    var rates: [Rate]
    var overall : LoanOverallResponse?
}

struct LoanOverallResponse : Codable {
    var requestId: Int
    var branch: String
    var mainAccBalance: Double
    var principalBalance: Double
    var arrearsOnPrincipal: Double
    var overdueDebt: Double
    var termDebtInterest: Double
    var overdueInterest: Double
    var arrearsInterest: Double
    var recommendedAmount: Double
    var maxAmount: Double
}
struct Rate: Codable {
    var name: String
    var rate: Int
}

