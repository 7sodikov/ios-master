//// Header

import UIKit

class LoanGraphsResponse: Codable {
    var graph:[LoanGraphResponse]?
}

class LoanGraphResponse: Codable {
    var payDate : String
    var principalAmount: Double
    var percentAmount: Double
    var payAmount: Double
    var balance : Double
    var status : String
    var state : Int
    
}
