//
//  JSONResponse.swift
//  AppNetwork
//
//  Created by Bilal Bakhrom on 23/12/2019.
//

/// Common json body
struct JSONResponse<Model: Codable>: Codable {
    /// Response success described data
    var data: Model?
    var timestamp: Int = 0
    var errorMessage: String = ""
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        timestamp = try container.decode(Int.self, forKey: .timestamp)
        errorMessage = try container.decode(String.self, forKey: .errorMessage)
        if errorMessage.isEmpty {
            data = try container.decode(Model.self, forKey: .data)
        }
    }
    
    /// Returns success described data or failure described data inside Error model
    
    func result(statusCode: Int) -> ResponseResult<Model, AppError> {
        if errorMessage.isEmpty, let data = data {
            let response = ResponseContainer(data: data, statusCode: statusCode)
            return .success(response)
        }
        else {
            return .failure(.standard(description: errorMessage, code: statusCode))
        }
    }
}

/// Failure model
struct CCommonError: Codable {
    /// Decription of error
    var message: String?
    /// Server code
    var code: Int?
    /// Status code
    var status: Int?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        message = (try? container.decode(String.self, forKey: .message))
        code = (try? container.decode(Int.self, forKey: .code))
        status = (try? container.decode(Int.self, forKey: .status))
    }
}
