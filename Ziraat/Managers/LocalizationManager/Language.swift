//
//  Language.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/26/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

enum Language: String {
    case english = "en"
    case russian = "ru"
    case uzbek = "uz"
    case turkish = "tr"
    case lanDesc = "Language"
    
    static func language(for str: String) -> Language {
        if str == "en" {
            return .english
        } else if str == "ru" {
            return .russian
        } else if str == "uz" {
            return .uzbek
        } else if str == "tr" {
            return .turkish
        } else {
            return .lanDesc
        }
    }
}
