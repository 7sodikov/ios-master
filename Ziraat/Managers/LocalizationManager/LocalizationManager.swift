//
//  LocalizationManager.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/26/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Localize_Swift
import Rswift

// Prefix RS is an abbreviation for R.string
let RS = R.string.localizable.self

class LocalizationManager {
    static func setApplLang(_ lang: Language) {
        Localize.setCurrentLanguage(lang.rawValue)
        // UD.language = lang.rawValue
    }
    
    static func setAppLang(langStr: String) {
        let language = Language.language(for: langStr.lowercased())
        LocalizationManager.setApplLang(language)
    }
    
    static func getAppLang() -> Language {
       return Language(rawValue: Localize.currentLanguage()) ?? .russian
    }
    
    static var language: Language? {
         set {
            if let value = newValue {
                let lanString = value.rawValue
                Localize.setCurrentLanguage(lanString)
                UserDefaults.standard.set(lanString, forKey: "language")
                UserDefaults.standard.synchronize()
            }
        } get {
            if let lanString = UserDefaults.standard.string(forKey: "language") {
                return Language(rawValue: lanString)
            } else {
                return nil
            }
        }
    }
    
    static func reloadWindow() {
        let sharedApplication = UIApplication.shared
        let window = sharedApplication.delegate?.window
        window??.subviews.forEach({ (view: UIView) in
            view.removeFromSuperview()
            window??.addSubview(view)
        })
        window??.rootViewController?.setNeedsStatusBarAppearanceUpdate()
    }

}

extension StringResourceType {
    func localized() -> String {
        return self.key.localized()
    }
}
