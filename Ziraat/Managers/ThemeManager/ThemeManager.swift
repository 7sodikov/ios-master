//
//  ThemeManager.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

var pallete: ThemePalette {
    return ThemeManager.shared.theme.pallete
}

final class ThemeManager {
    static let shared = ThemeManager()
    private init() {}
    
    private(set) var theme: Theme {
        get {
            guard let theme = Theme(rawValue: UDManager.theme) else {
                return .light
            }
            return theme
        }
        set {
            UDManager.theme = newValue.rawValue
        }
    }
    
    func applyTheme(_ theme: Theme) {
        guard theme != self.theme  else {
            return
        }
        self.theme = theme
        setNeedsThemeUpdate()
    }
    
    func setNeedsThemeUpdate() {
        reloadWindow()
    }
    
    func reloadWindow() {
        let sharedApplication = UIApplication.shared
        let window = sharedApplication.delegate?.window
        window??.subviews.forEach({ (view: UIView) in
            view.removeFromSuperview()
            window??.addSubview(view)
        })
        window??.rootViewController?.setNeedsStatusBarAppearanceUpdate()
    }
}
