//
//  ThemePalette.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ThemePalette {
    var textColor: UIColor { get }
    var textFieldBorderColor: UIColor { get }
    
    var tableBGColor: UIColor { get }
    var tableCellBGColor: UIColor { get }
    
    var forgotPassSegmentedCtrlSelectedColor: UIColor { get }
}

extension ThemePalette {
    var textColor: UIColor { ColorConstants.red }
    var textFieldBorderColor: UIColor { .orange}
    
    var tableBGColor: UIColor { .lightGray }
    var tableCellBGColor: UIColor { .gray }
    var forgotPassSegmentedCtrlSelectedColor: UIColor { ColorConstants.red }
}
