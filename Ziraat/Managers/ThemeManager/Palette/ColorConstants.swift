//
//  ColorConstants.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class ColorConstants {
    static let red = UIColor(214, 0, 18)
    static let gray = UIColor.darkGray
    static let highlightedGray = UIColor(45, 45, 45)
    static let highlightedWhite = UIColor.white.withAlphaComponent(0.3)
    static let nativeTableSectionHeaderColor = UIColor(223, 223, 223)
    
    /// 333536
    static let backgroundBlack = UIColor(rgb: 0x333536)
    /// 6B6F78
    static let borderLine = UIColor(rgb: 0x6B6F78)
    
    static let borderLineHighlighted = UIColor(rgb: 0x393B40)
    
    /// FFFFFE
    static let grayishWhite = UIColor(rgb: 0xFFFFFE)
    /// 2490FD
    static let dashboardBlue = UIColor(rgb: 0x2490FD)
    /// 56B164
    static let dashboardGreen = UIColor(rgb: 0x56B164)
    /// FFB80E
    static let dashboardYellow = UIColor(rgb: 0xFFB80E)
    /// 8456D2
    static let dashboardPurple = UIColor(rgb: 0x8456D2)
    /// 939598
    static let lightGray = UIColor(rgb: 0x939598)
    /// BBBBBB
    static let lightestGray = UIColor(rgb: 0xBBBBBB)
    /// D50215
    static let mainRed = UIColor(rgb: 0xE10514)
    /// 72010
    static let highlightedMainRed = UIColor(rgb: 0x72010b)
    /// DBDEDF
    static let separatorGrayLine = UIColor(rgb: 0xDBDEDF)
    /// 939598
    static let hintGray = UIColor(rgb: 0x939598)
    /// 445056
    static let mainTextColor = UIColor(rgb: 0x445056)
    
    static let navBarStartColor = UIColor(rgb: 0x3a68ca)
    static let navBarEndColor = UIColor(rgb: 0x33a0db)
}
