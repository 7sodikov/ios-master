//
//  Theme.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

public enum Theme: Int {
    case light
    case dark
    
    var pallete: ThemePalette {
        switch self {
        case .light:
            return LightThemePalette()
        case .dark:
            return DarkThemePalette()
        }
    }
}
