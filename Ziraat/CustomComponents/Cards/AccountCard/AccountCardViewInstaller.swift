//
//  AccountCardViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol AccountCardViewInstaller: ViewInstaller {
    var backImageView: UIImageView! { get set}
    var container: UIView! { get set}
    var logoView: UIImageView! { get set}
    var descriptiveStackView: UIStackView! { get set}
    var currencyView: UIImageView! { get set}
    var userNameLabel:UILabel! { get set}
    var accuntView: DescriptiveLabeledView! { get set}
    var fundsView: DescriptiveLabeledView! { get set}
    var balanceView: DescriptiveLabeledView! { get set}
    
}

extension AccountCardViewInstaller {
    
    func initSubviews() {
        
        mainView.backgroundColor = .clear
        mainView.layer.cornerRadius = 9
        mainView.layer.borderWidth = 0.3
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOpacity = 1
        mainView.layer.shadowOffset = .zero
        mainView.layer.shadowRadius = 5
        
        backImageView = UIImageView()
        backImageView.image = UIImage(named: "img_accountCardBackgound")
        backImageView.layer.cornerRadius = 9
        backImageView.contentMode = .scaleToFill
        backImageView.layer.masksToBounds = true
        
        container = UIView()
        container.backgroundColor = .clear
        
        currencyView = UIImageView()
        currencyView.layer.masksToBounds = true
        currencyView.layer.cornerRadius = 25
        currencyView.image = UIImage(named: "CurrencySymbols_yen")
        
        logoView = UIImageView()
        logoView.image = UIImage(named: "img_Icones_ZiraatBank2")
        
        accuntView = DescriptiveLabeledView()
        fundsView = DescriptiveLabeledView()
        balanceView = DescriptiveLabeledView()
        
        userNameLabel = UILabel()
        userNameLabel.font = .systemFont(ofSize: 13, weight: .semibold)
        userNameLabel.numberOfLines = 2
        userNameLabel.lineBreakMode = .byWordWrapping
        userNameLabel.text = "KOMILJONOV SHUKHRAT KOMILJON OGLI Operu"
        
        descriptiveStackView = .init()
        descriptiveStackView.axis = .vertical
        descriptiveStackView.spacing = 2
        descriptiveStackView.distribution = .fill
        
    }
    
    func embedSubviews() {
        mainView.addSubview(backImageView)
        mainView.addSubview(container)
        
        container.addSubview(currencyView)
        container.addSubview(logoView)
        container.addSubview(userNameLabel)
        container.addSubview(descriptiveStackView)
        
        descriptiveStackView.addArrangedSubview(accuntView)
        descriptiveStackView.addArrangedSubview(fundsView)
        descriptiveStackView.addArrangedSubview(balanceView)
    }
    
    func addSubviewsConstraints() {
        
        backImageView.constraint { (make) in
            make.height(UI.CardSize.height).width(UI.CardSize.width).edges.equalToSuperView()
        }
        container.constraint { (make) in
            make.edges.equalToSuperView()
        }
        currencyView.constraint { (make) in
            make.square(50).top(15).leading(15).equalToSuperView()
        }
        logoView.constraint { (make) in
            make.width(53).height(49).trailing(-49).top(11).equalToSuperView()
        }
        userNameLabel.constraint { (make) in
            make.top(self.currencyView.bottomAnchor, offset: 16)
                .horizontal(10).equalToSuperView()
        }
        descriptiveStackView.constraint { (make) in
            make.top(self.userNameLabel.bottomAnchor, offset: 4).horizontal(10).equalToSuperView()
        }
    }
    
    func setupSubviews() {
        initSubviews()
        embedSubviews()
        addSubviewsConstraints()
    }
    
}

fileprivate enum UI {
    struct CardSize {
        static let width = CGFloat(296)
        static let height = CGFloat(180)
    }
}
