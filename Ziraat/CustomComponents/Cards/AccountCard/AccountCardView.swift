//
//  AccountCardView.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class AccountCardView: UIView, AccountCardViewInstaller {

    var backImageView: UIImageView!
    var container: UIView!
    var currencyView: UIImageView!
    var logoView: UIImageView!
    var userNameLabel:UILabel!
    var accuntView: DescriptiveLabeledView!
    var fundsView: DescriptiveLabeledView!
    var balanceView: DescriptiveLabeledView!
    var descriptiveStackView: UIStackView!
    var mainView: UIView { self }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(currencyIcon: String, userName: String, account: String, fund: String, balance: String) {
        currencyView.image = UIImage(named: currencyIcon)
        userNameLabel.text = userName
        accuntView.set(title: "Счет:", value: account)
        fundsView.set(title: "Доступные средства:", value: fund)
        balanceView.set(title: "Баланс:", value: balance)
    }
}
