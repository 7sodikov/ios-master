//
//  ProductTransferViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol ProductTransferViewInstaller: ViewInstaller {
    var containerView: UIView! { get set }
    var upAndDownIcon: UIImageView! { get set }
    var imageView: UIImageView! { get set }
    var defaultLabel: UILabel! { get set }
    var nameLabel: UILabel! { get set }
    var amountLabel: UILabel! { get set }
    var detailedAmountLabel: UILabel! { get set }
}

extension ProductTransferViewInstaller {
    func initSubviews() {
        containerView = UIView()
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = Size.viewHeight/2
        
        upAndDownIcon = UIImageView()
        upAndDownIcon.image = UIImage(named: "btn_up_down")
        
        imageView = UIImageView()
        imageView.image = UIImage(named: "img_conver_detail")
        
        defaultLabel = UILabel()
        defaultLabel.text = RS.txt_f_choose.localized()
        defaultLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(18))
        defaultLabel.textColor = .black
        
        nameLabel = UILabel()
        
        amountLabel = UILabel()
        
        detailedAmountLabel = UILabel()
    }
    
    func embedSubviews() {
        mainView.addSubview(containerView)
        containerView.addSubview(upAndDownIcon)
        containerView.addSubview(imageView)
        containerView.addSubview(defaultLabel)
        mainView.addSubview(nameLabel)
        mainView.addSubview(amountLabel)
        mainView.addSubview(detailedAmountLabel)
    }
    
    func addSubviewsConstraints() {
        containerView.snp.remakeConstraints() { (maker) in
            maker.edges.equalTo(0)
        }
        
        upAndDownIcon.snp.remakeConstraints() { (maker) in
            maker.width.equalTo(Adaptive.val(15))
            maker.height.equalTo(Adaptive.val(24))
            maker.centerY.equalTo(containerView)
            maker.trailing.equalTo(containerView.snp.trailing).offset(-Adaptive.val(20))
        }
        
        imageView.snp.remakeConstraints() { (maker) in
            maker.width.height.equalTo(Adaptive.val(36))
            maker.centerY.equalTo(containerView)
            maker.leading.equalTo(containerView.snp.leading).offset(Adaptive.val(20))
        }
        
        defaultLabel.snp.remakeConstraints() { (maker) in
            maker.centerY.equalTo(containerView)
            maker.leading.equalTo(imageView.snp.trailing).offset(Adaptive.val(20))
        }
    }
}

fileprivate struct Size {
    static let viewHeight = Adaptive.val(54)
}
