//
//  ProductTransferView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import Foundation

protocol ProductTransferViewDelegate : AnyObject {
    func productTransferDidPressed(view: ProductTransferView)
}

class ProductTransferView: UIView, ProductTransferViewInstaller {
    var mainView: UIView { self }
    var containerView: UIView!
    var upAndDownIcon: UIImageView!
    var imageView: UIImageView!
    var defaultLabel: UILabel!
    var nameLabel: UILabel!
    var amountLabel: UILabel!
    var detailedAmountLabel: UILabel!

    var tapGestureRecogniser: UITapGestureRecognizer!
    
    weak var delegate: ProductTransferViewDelegate?
    
    var needPercent: Bool = false
    var credit:Bool = false

//    var product: Product? {
//        didSet {
//            if let product = self.product {
//                self.imageView.subviews.forEach { (subviews) in
//                    subviews.removeFromSuperview()
//                }
//
//                let view = product.productImage
//                self.imageView.addSubview(view)
//                view.fillSuperView()
//
//                self.nameLabel.text = product.productName
//
//                self.amountLabel.text = product.productFormattedAmount(needOfPercent: self.needPercent)
//
//                self.defaultLabel.isHidden = true
//                self.amountLabel.superview?.isHidden = false
//            } else {
//                let emptyImageView = UIImageView(image: UIImage(named: "empty_product"))
//                self.imageView.addSubview(emptyImageView)
//                emptyImageView.fillSuperView()
//
//                self.defaultLabel.isHidden = false
//                self.amountLabel.superview?.isHidden = true
//            }
//        }
//    }
    
    var desiredAmount:String? {
        didSet {
            self.detailedAmountLabel.isHidden = self.desiredAmount == nil
            self.detailedAmountLabel.text = self.desiredAmount
            if self.credit {
                self.detailedAmountLabel.textColor = ColorConstants.mainRed
            } else {
                self.detailedAmountLabel.textColor = ColorConstants.dashboardGreen
            }
        }
    }
        
    var isEnabled:Bool = true {
        didSet {
            self.tapGestureRecogniser.isEnabled = self.isEnabled
            self.upAndDownIcon.isHidden = !self.isEnabled
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        
        self.detailedAmountLabel.isHidden = true
                
        self.imageView.layer.cornerRadius = 16
        
        
     
        self.nameLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(16))
        self.amountLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(20))
        
//        tapGestureRecogniser.addTarget(self, action: #selector(handlesTapGestureRecognier(_:)))
    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//        self.detailedAmountLabel.isHidden = true
//
//        self.containerView.layer.cornerRadius = 16
//
//        self.imageView.layer.cornerRadius = 16
//
//        self.defaultLabel.text = "Choose".localized()
//        self.defaultLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(20))
//
//        self.nameLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(16))
//        self.amountLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(20))
//
//        tapGestureRecogniser.addTarget(self, action: #selector(handlesTapGestureRecognier(_:)))
//
////        self.product = nil
//
//        NotificationCenter.default.addObserver(self, selector: #selector(updateColor), name: NSNotification.Name("color_change"), object: nil)
//        self.updateColor()
//    }
//
//    @objc private func updateColor() {
//        self.containerView.backgroundColor = ColorConstants.gray
//        self.defaultLabel.textColor = .black
//
//        self.nameLabel.textColor = .black
//        self.amountLabel.textColor = .black
//    }
    
    @objc func handlesTapGestureRecognier(_ sender: Any) {
        self.delegate?.productTransferDidPressed(view: self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
