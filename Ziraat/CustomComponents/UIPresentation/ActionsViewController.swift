//// Header

import UIKit
import PanModal

class ActionsViewController: BasePresentController {

    var actionList = [(icon: String, title: String, subtitle: String)]()
    var didActionSelect :((Int) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topLabel.text = nil
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorInset = .zero
        self.tableView.tableFooterView = UIView()
        self.tableView.register(ActionsViewCell.self, forCellReuseIdentifier: "ActionsViewCell")
    }
    
    @objc func cancelClicked(){
        self.dismiss(animated: true) {
            
        }
    }
    

}
extension ActionsViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return actionList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActionsViewCell", for: indexPath) as! ActionsViewCell
        cell.backgroundColor = .white
        let backgroundView = UIView()
        backgroundView.backgroundColor = ColorConstants.lightGray.withAlphaComponent(0.3)
        cell.selectedBackgroundView = backgroundView
        cell.menuActionLabel.text = actionList[indexPath.row].title
        cell.subtitleLabel.text = actionList[indexPath.row].subtitle
        cell.menuActionImage.image = nil
        
        cell.menuActionImage.image =  UIImage(named: actionList[indexPath.row].icon)?.withRenderingMode(.alwaysTemplate)
        cell.menuActionImage.tintColor = ColorConstants.gray
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            
        }
        if self.didActionSelect != nil{
            self.didActionSelect!(indexPath.row)
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        let btn = NextButton.systemButton(with: RS.lbl_cancel.localized(), cornerRadius: 25)
        btn.setBackgroundColor(ColorConstants.red, for: .normal)
        btn.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        btn.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
        view.addSubview(btn)
        btn.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
}
extension ActionsViewController: PanModalPresentable {
    var panScrollable: UIScrollView? {
        return tableView
    }
    
    var shortFormHeight: PanModalHeight {
        return  .contentHeight(UIScreen.main.bounds.size.height/3)
//        if self.thumbnailImageView.image == nil {
//            return
//        } else {
//            return .contentHeight(UIScreen.main.bounds.size.height/1.5)
//        }
    }
    
    var longFormHeight: PanModalHeight {
        return .contentHeight(UIScreen.main.bounds.size.height/2)
    }
    
    var cornerRadius: CGFloat {
        return 16
    }
           
    var allowsExtendedPanScrolling: Bool {
        return false
    }

}

