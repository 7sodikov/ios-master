//// Header

import UIKit

class PresentationController: UIPresentationController {
    // MARK: Properties
    
    let blurEffectView: UIVisualEffectView!
    var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()
    var presentContainerRect : CGRect?
    // 1.
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        let blurEffect = UIBlurEffect(style: .dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissController))
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEffectView.isUserInteractionEnabled = true
        self.blurEffectView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // 2.
    override var frameOfPresentedViewInContainerView: CGRect {
        
//        let safeBounds = self.containerView!.bounds.inset(by: containerView?.safeAreaInsets ?? .zero)
//        let inset: CGFloat = 16
//
//        let targetWidth = safeBounds.width - 2*16
//        let targetSize = CGSize(
//            width: targetWidth,
//            height: UIView.layoutFittingCompressedSize.height
//        )
//        let targetHeight = self.presentedView?.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .defaultLow).height
//
//        return CGRect(x: inset, y: UIScreen.main.bounds.size.height - (targetHeight ?? 0), width: targetWidth, height: targetHeight ?? 0)
        let h = self.containerView!.frame.height * 0.70
        let top = self.containerView!.frame.height * 0.30
        if self.presentContainerRect != nil{
            return self.presentContainerRect!
        }
        return CGRect(origin: CGPoint(x: 0, y: top),
                      size: CGSize(width: self.containerView!.frame.width, height: h))
    }
    
    // 3.
    override func presentationTransitionWillBegin() {
        self.blurEffectView.alpha = 0
        self.containerView?.addSubview(blurEffectView)
        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) in
            self.blurEffectView.alpha = 0.4
        }, completion: { (UIViewControllerTransitionCoordinatorContext) in })
    }
    
    // 4.
    override func dismissalTransitionWillBegin() {
        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) in
            self.blurEffectView.alpha = 0
        }, completion: { (UIViewControllerTransitionCoordinatorContext) in
            self.blurEffectView.removeFromSuperview()
        })
    }
    
    // 5.
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        presentedView!.roundCorners(corners: [.topLeft, .topRight], radius: 22)
    }
    
    // 6.
    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()
        presentedView?.frame = frameOfPresentedViewInContainerView
        blurEffectView.frame = containerView!.bounds
    }
    
    // 7.
    @objc func dismissController(){
        self.presentedViewController.dismiss(animated: true, completion: nil)
    }
}


class BasePresentController: BaseViewController {
    let topView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .white
        return view
    }()
    
    let topLabel : UILabel = {
        let label = UILabel()
        label.textColor = ColorConstants.red
        label.font = EZFontType.bold.sfuiText(size: 18)
        label.textAlignment = .center
        label.text = "actions"
        return label
    }()
    
    let topDarkLine: UIView = {
        let view = UIView()
        view.backgroundColor = ColorConstants.red
        view.layer.cornerRadius = 3
        return view
    }()
    
    
    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        topView.addGestureRecognizer(panGesture)
        setupViews()
        setupTableView()
    }
    
    func setupViews() {
        view.addSubview(topView)
        topView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        topView.addSubview(topDarkLine)
        
        topDarkLine.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(12)
            make.height.equalTo(6)
            make.width.equalToSuperview().multipliedBy(0.15)
            make.centerX.equalToSuperview()
        }
        topView.addSubview(topLabel)
        topLabel.snp.makeConstraints { (make) in
            make.left.equalTo(25)
            make.right.equalTo(-25)
            make.height.equalTo(20)
            make.top.equalTo(30)
        }
    
    }
    
    func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(topView.snp.bottom)
        }
    }
    
//    func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize{
//        var size = tableView.contentSize
//        size.height += 60
//        return size
//    }

    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
        if let preset = self.presentationController as? PresentationController{
            let h = tableView.contentSize.height
            if  h < preset.containerView?.frame.height ?? 0 {
                preset.presentContainerRect = CGRect(origin: CGPoint(x: 0, y: preset.containerView?.frame.size.height ?? 0 - h),
                size: CGSize(width: preset.containerView!.frame.width, height: h))
            }
        }
        
    }
    // 3.
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }
        
        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
        
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                // Velocity fast enough to dismiss the uiview
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
                }
            }
        }
    }
}
