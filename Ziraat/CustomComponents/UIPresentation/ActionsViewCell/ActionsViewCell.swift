//
//  ActionsViewCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class ActionsViewCell: UITableViewCell, ActionsViewCellInstaller {
    var menuActionImage: UIImageView!
    var menuActionLabel: UILabel!
    var subtitleLabel: UILabel!
    var mainView: UIView { contentView }
    
    var data: MenuCellVM? {
        didSet {
            if let data = self.data {
                self.menuActionImage.image = data.image
                self.menuActionLabel.text = data.name
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
