//
//  ActionsViewCellInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol ActionsViewCellInstaller: ViewInstaller {
    var menuActionImage: UIImageView! { get set }
    var menuActionLabel: UILabel! { get set }
    var subtitleLabel: UILabel! { get set }
}

extension ActionsViewCellInstaller {
    func initSubviews() {
        menuActionImage = UIImageView()
        menuActionImage.contentMode = .scaleAspectFit
        
        menuActionLabel = UILabel()
        menuActionLabel.textColor = .black
        menuActionLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        
        subtitleLabel = UILabel()
        subtitleLabel.textColor = .black
        subtitleLabel.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(13))
    }
    
    func embedSubviews() {
        mainView.addSubview(menuActionImage)
        mainView.addSubview(menuActionLabel)
        mainView.addSubview(subtitleLabel)
    }
    
    func addSubviewsConstraints() {
        menuActionImage.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(30))
            maker.top.equalToSuperview().offset(Adaptive.val(10))
            maker.bottom.equalToSuperview().offset(-Adaptive.val(19))
            maker.leading.equalToSuperview().offset(Adaptive.val(16))
        }
        
        menuActionLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(menuActionImage.snp.trailing).offset(Adaptive.val(18))
            maker.top.equalTo(Adaptive.val(13))
        }
        
        subtitleLabel.snp.remakeConstraints { (maker) in
            maker.leading.trailing.equalTo(menuActionLabel)
            maker.top.equalTo(menuActionLabel.snp.bottom).offset(Adaptive.val(2))
        }
    }
}


