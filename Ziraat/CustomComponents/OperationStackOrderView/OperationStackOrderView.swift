//
//  OperationStackOrderView.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class OperationStackOrderView: UIView {
    
    private var current: StackOrder = .one
    private let numberOfOperation: NumberOfOperation
    var circleViews: [UIView] = []
    
    init(numberOfOperation: NumberOfOperation = .four) {
        self.numberOfOperation = numberOfOperation
        super.init(frame: .zero)
        // setup(current: .one)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(current: StackOrder) {
        circleViews.forEach { $0.removeFromSuperview() }
        self.current = current
        circleViews = numberOfOperation.viewTypes(of: current).map { circle($0) }
        let stackView = UIStackView(arrangedSubviews: circleViews)
        stackView.spacing = 16
        
        addSubview(stackView)
        circleViews.forEach { circle in circle.constraint { $0.square(40)} }
        stackView.constraint { (make) in
            make.edges.equalToSuperView()
        }
    }
    
    func next(to stackOrder: StackOrder? = nil) {
        let currentOperation = stackOrder ?? self.current
        let next = numberOfOperation.next(from: currentOperation)
        setup(current: next)
    }
    
    func back(to stackOrder: StackOrder? = nil) {
        let currentOperation = stackOrder ?? self.current
        let next = numberOfOperation.back(from: currentOperation)
        setup(current: next)
    }
    
    private func circle(_ viewType: CircleViewType) -> UIView {
        let view = UIView()
        view.layer.cornerRadius = 20
        view.layer.borderWidth = viewType.borderWidth
        view.backgroundColor = viewType.backColor
        let subView = viewType.view
        view.addSubview(subView)
        subView.constraint { (make) in
            make.center.equalToSuperView()
        }
        return view
    }
    
}

extension OperationStackOrderView {
    
    enum CircleViewType {
        case completed
        case current(order: Int)
        case notYet(order: Int)
        
        var borderWidth: CGFloat {
            switch self {
            case .current, .completed:
                return 0
            case .notYet:
                return 1
            }
        }
        
        var backColor: UIColor {
            switch self {
            
            case .current:
                return  UIColor(red: 225/255, green: 5/255, blue: 20/255, alpha: 1)
            case .completed:
                return UIColor(red: 168/255, green: 155/255, blue: 112/255, alpha: 1)
            case .notYet:
                return .clear
            }
        }
        
        var view: UIView {
            let label = UILabel()
            label.font = .systemFont(ofSize: 16)
            switch self {
            case .completed:
                return UIImageView(image: UIImage(named: "img_checked"))
                
            case .current(let order):
                label.textColor = .white
                label.text = order.description
                
            case .notYet(let order):
                label.textColor = .black
                label.text = order.description
            }
            return label
        }
        
    }
    
    
    enum NumberOfOperation: Int {
        case two = 2
        case three
        case four
        
        //        #warning("number of operation must not be greater than maximum rawValue of StackOrder")
        var stackOrders: [StackOrder]  {
            if rawValue > StackOrder.allCases.count {
                return StackOrder.allCases
            }
            return Array(StackOrder.allCases[...(rawValue - 1)])
        }
        
        func viewTypes(of stackOrder: StackOrder) -> [CircleViewType] {
            stackOrders.map { item -> CircleViewType in
                stackOrder.rawValue > item.rawValue
                    ? .completed
                    : stackOrder == item ? .current(order: item.rawValue) : .notYet(order: item.rawValue)
            }
        }
        
        func next(from stackOrder: StackOrder) -> StackOrder {
            if stackOrder.rawValue < stackOrders.count,
               let nextItem = StackOrder(rawValue: stackOrder.rawValue + 1) {
                return nextItem
            }
            return stackOrder
        }
        
        func back(from stackOrder: StackOrder) -> StackOrder {
            if let backItem = StackOrder(rawValue: stackOrder.rawValue - 1) {
                return backItem
            }
            return stackOrder
        }
    }
    
    enum StackOrder: Int, CaseIterable {
        case one = 1
        case two
        case three
        case four
        
        var viewTypes: [CircleViewType] {
            StackOrder.allCases.map { item -> CircleViewType in
                self.rawValue > item.rawValue
                    ? .completed
                    : self == item
                    ? .current(order: item.rawValue)
                    : .notYet(order: item.rawValue)
            }
        }
        
        var next: StackOrder {
            rawValue < 4 ? StackOrder(rawValue: self.rawValue + 1)! : .four
        }
        var back: StackOrder {
            rawValue > 1 ? StackOrder(rawValue: rawValue - 1)! : .one
        }
    }
    
}
