//
//  SingleLabelCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/19/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class SingleLabelCellViewModel: TableViewModel {

    override var cellType: TableViewCell.Type {
        SingleLabelCell.self
    }
    
    let text: String
    let textAlignment: NSTextAlignment
    
    internal init(text: String, textAlignment: NSTextAlignment = .left) {
        self.text = text
        self.textAlignment = textAlignment
    }
    
}
