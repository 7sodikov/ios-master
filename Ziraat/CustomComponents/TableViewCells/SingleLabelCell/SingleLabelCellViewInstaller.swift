//
//  SingleLabelCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/19/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol SingleLabelCellViewInstaller: ViewInstaller {
    var label: UILabel! {get set}
}

extension SingleLabelCellViewInstaller {
    
    func initSubviews() {
        
        label = UILabel()
        label.font = .gothamNarrow(size: 16, .book)
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
    }
    
    func embedSubviews() {
        mainView.addSubview(label)
    }
    
    func addSubviewsConstraints() {
        label.constraint { (make) in
            make.horizontal(16).vertical(16).equalToSuperView()
        }
    }
    
}

