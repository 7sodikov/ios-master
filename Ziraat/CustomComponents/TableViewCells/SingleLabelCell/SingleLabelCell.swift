//
//  SingleLabelCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/19/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class SingleLabelCell: TableViewCell, SingleLabelCellViewInstaller {
    
    var label: UILabel!
    var mainView: UIView { self.contentView }
    
    weak var buttonDelegate: ButtonCellDelegate? = nil

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        backgroundColor = .clear
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? SingleLabelCellViewModel else { return }
        label.text = viewModel.text
        label.textAlignment = viewModel.textAlignment
    }
    
}
