//
//  ConfirmationCellViewModel.swift
//  Ziraat
//
//  Created by Macbook on 28/03/21.
//  Copyright © 2020 Jasur Amirov. All rights reserved.
//

import UIKit

public class ConfirmationCellViewModel: TableViewModel {
    
    public override var cellType: TableViewCell.Type {
        ConfirmationCell.self
    }
    
    let message: String
    
    init(message: String) {
        self.message = message
        super.init(uid: nil)
    }
}
