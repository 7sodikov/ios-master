//
//  ConfirmationCell.swift
//  Ziraat
//
//  Created by Macbook on 28/03/21.
//  Copyright © 2020 Jasur Amirov. All rights reserved.
//

import UIKit

protocol ConfirmationCellDelegate: class {
    func textFiled(text: String)
}

public class ConfirmationCell: TableViewCell, ConfirmationCellViewInstaller {
    
    var titleLabel: UILabel!
    var smsCodeTextField: SMSConfirmationTextField!
    var timerView: TimerView!
    var mainView: UIView { self.contentView }
    
    weak var confirmationDelegate: ConfirmationCellDelegate? = nil
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        setupSubviews()
        setupTargets()   
    }
    
    func setupTargets() {
        smsCodeTextField.addTarget(self, action: #selector(changeSmsFieldText(_:)), for: .editingChanged)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ConfirmationCellViewModel else { return }
    
        timerView.startTimer(with: 60)
        titleLabel.text = viewModel.message
    }
    
    @objc func changeSmsFieldText(_ sender: UITextField) {
        confirmationDelegate?.textFiled(text: sender.text ?? "")
    }
    
}
