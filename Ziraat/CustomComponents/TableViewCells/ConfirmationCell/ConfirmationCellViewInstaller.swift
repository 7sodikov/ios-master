//
//  ConfirmationCellViewInstaller.swift
//  Ziraat
//
//  Created by Macbook on 28/03/21.
//  Copyright © 2020 Jasur Amirov. All rights reserved.
//

import UIKit

protocol ConfirmationCellViewInstaller: ViewInstaller {
    var timerView: TimerView! {get set}
    var smsCodeTextField: SMSConfirmationTextField! {get set}
    var titleLabel: UILabel! {get set}
}

extension ConfirmationCellViewInstaller {
    
    func initSubviews() {
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .book)
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        
        smsCodeTextField = SMSConfirmationTextField(cornerRadius: 6)
        smsCodeTextField.textAlignment = .center
        smsCodeTextField.layer.borderWidth = 2
        smsCodeTextField.layer.borderColor = UIColor(84, 95, 101).cgColor
        smsCodeTextField.addShadow()
        
        timerView = TimerView(frame: .init(origin: .zero, size: CGSize(width: Adaptive.val(170), height: Adaptive.val(170))))
        timerView.titleColor = .black
        timerView.shapeColor = ColorConstants.mainRed
        timerView.backShapeColor = ColorConstants.mainRed.withAlphaComponent(0.40)
        
    }
    
    func embedSubviews() {
        mainView.addSubview(titleLabel)
        mainView.addSubview(smsCodeTextField)
        mainView.addSubview(timerView)
    }
    
    func addSubviewsConstraints() {
        
        titleLabel.constraint { (make) in
            make.top.horizontal(20).equalToSuperView()
        }
        smsCodeTextField.constraint { (make) in
            make.horizontal(20).height(Adaptive.val(50))
                .top(self.titleLabel.bottomAnchor, offset: 16)
                .centerX.equalToSuperView()
        }
        timerView.constraint { (make) in
            make.top(self.smsCodeTextField.bottomAnchor, offset: Adaptive.val(64))
                .square(Adaptive.val(170))
                .centerX.bottom.equalToSuperView()
        }
    }
    
    
}
