//
//  BorderedInputCellViewModel.swift
//  MilliyCore
//
//  Created by Macbook on 3/9/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public class BorderedInputCellViewModel: TableViewModel {
    
    public var id: Int
    public var holder: String
    public var cardNumber: String
    public var editable: Bool
    
    public override var cellType: TableViewCell.Type {
        BorderedInputCell.self
    }
    
    public override var estimatedRowHeight: CGFloat {
        56
    }
    
    public init(id: Int, holder: String, cardNumber: String, editable: Bool = true) {
        self.id = id
        self.holder = holder
        self.cardNumber = cardNumber
        self.editable = editable
        super.init(uid: nil)
    }
    
}
