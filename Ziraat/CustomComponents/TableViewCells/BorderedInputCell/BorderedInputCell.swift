//
//  BorderedInputCell.swift
//  MilliyCore
//
//  Created by Macbook on 3/9/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public class BorderedInputCell: TableViewCell, BorderedInputCellViewInstaller {
    
    var titleLabel: UILabel!
    var stackView: UIStackView!
   
    var mainView: UIView { self }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)        
        setupSubviews()
        setupTargets()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var cellHeight: CGFloat {
        56
    }
    
    func setupTargets() {
       
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let _ = viewModel as? BorderedInputCellViewModel else { return }
        
    }
    
    @objc private func didTouchUpInsideSettings() {
        swipeOffset.isZero ? showSwipe(orientation: .right) : hideSwipe(animated: true)
    }
    
    fileprivate func update(editable: Bool) {
        
    }
}
