//
//  BorderedInputCellViewInstaller.swift
//  MilliyCore
//
//  Created by Macbook on 3/9/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

protocol BorderedInputCellViewInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var stackView: UIStackView! {get set}
}

extension BorderedInputCellViewInstaller {
    
    func initSubviews() {
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 18, .book)
        titleLabel.textColor = .black
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 2.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func embedSubviews() {
       
    }
    
    func addSubviewsConstraints() {
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: mainView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: -16/2),
        ])
    }
    
}
