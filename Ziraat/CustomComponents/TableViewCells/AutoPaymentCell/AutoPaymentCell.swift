//
//  AutoPaymentCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

class AutoPaymentCell: TableViewCell, AutoPaymentCellViewInstaller {
    
    var container: UIView!
    var headerStackView: UIStackView!
    var titleLabel: UILabel!
    var statusLabel: PaddingLabel!
    var detailsStackView: UIStackView!
    var cardNumberLabel: UILabel!
    var frequnecyLabel: UILabel!
    var amountLabel: UILabel!
    var iconView: UIImageView!
    var mainView: UIView { self.contentView }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        setupSubviews()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var cellHeight: CGFloat {
        140
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? AutoPaymentCellViewModel else { return }
        
        titleLabel.text = viewModel.name
        statusLabel.text = viewModel.status.title
        statusLabel.backgroundColor = viewModel.status.color
        cardNumberLabel.text = viewModel.cardNumber
        frequnecyLabel.text = viewModel.frequency
        amountLabel.text = viewModel.amount
    }
    
}
