//
//  AutoPaymentCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AutoPaymentCellViewInstaller: ViewInstaller {
    var container: UIView! {get set}
    var headerStackView: UIStackView! {get set}
    var titleLabel: UILabel! {get set}
    var statusLabel: PaddingLabel! {get set}
    var detailsStackView: UIStackView! {get set}
    var cardNumberLabel: UILabel! {get set}
    var frequnecyLabel: UILabel! {get set}
    var amountLabel: UILabel! {get set}
    var iconView: UIImageView! {get set}

}

extension AutoPaymentCellViewInstaller {
    
    func initSubviews() {
        container = UIView()
        container.layer.borderWidth = 2
        container.layer.cornerRadius = 6
        container.backgroundColor = .white
        container.layer.borderColor = UIColor.gray.cgColor
        
        headerStackView = UIStackView()
        headerStackView.axis = .horizontal
        headerStackView.alignment = .fill
        headerStackView.distribution = .equalCentering
        
        titleLabel = UILabel()
        titleLabel.font = .systemFont(ofSize: 16, weight: .regular)
        titleLabel.textAlignment = .right
        
        statusLabel = PaddingLabel(withInsets: .init(top: 5, left: 12, bottom: 5, right: 12))
        statusLabel.font = .systemFont(ofSize: 10, weight: .regular)
        statusLabel.textAlignment = .left
        statusLabel.layer.cornerRadius = 11
        statusLabel.layer.masksToBounds = true
        statusLabel.textColor = .white
        
        detailsStackView = UIStackView()
        detailsStackView.spacing = 4
        detailsStackView.axis = .vertical
        
        cardNumberLabel = UILabel()
        cardNumberLabel.font = .systemFont(ofSize: 15, weight: .regular)
        
        frequnecyLabel = UILabel()
        frequnecyLabel.font = .systemFont(ofSize: 15, weight: .regular)
        frequnecyLabel.text = "Order Date: 04/02/2021"
        
        amountLabel = UILabel()
        amountLabel.font = .systemFont(ofSize: 15, weight: .regular)
        amountLabel.text = "Account No: 2487-35582770-5006"
        
        iconView = UIImageView()
        iconView.image = UIImage(named: "img_disclosure")
    }
    
    func embedSubviews() {
        mainView.addSubview(container)
        
        container.addSubview(headerStackView)
        container.addSubview(detailsStackView)
        container.addSubview(iconView)
        
        headerStackView.addArrangedSubview(titleLabel)
        headerStackView.addArrangedSubview(statusLabel)
        
        detailsStackView.addArrangedSubview(cardNumberLabel)
        detailsStackView.addArrangedSubview(frequnecyLabel)
        detailsStackView.addArrangedSubview(amountLabel)
    }
    
    func addSubviewsConstraints() {
        container.constraint { make in
            make.vertical(8).horizontal(15).equalToSuperView()
        }
        headerStackView.constraint { maker in
            maker.top(15).horizontal(15).equalToSuperView()
        }
        iconView.constraint { maker in
            maker.square(24).centerY.trailing(-15).equalToSuperView()
        }
        detailsStackView.constraint { maker in
            maker.top(self.headerStackView.bottomAnchor, offset: 15)
                .trailing(self.iconView.leadingAnchor, offset: -8)
                .bottom(-15).leading(15).equalToSuperView()
        }
    }
    
}
