//
//  AutoPaymentCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//


class AutoPaymentCellViewModel: TableViewModel {
    
    
    override var cellType: TableViewCell.Type {
        AutoPaymentCell.self
    }
    
    override var estimatedRowHeight: CGFloat {
        16 + 22 + 15 + 12 + 57 + 15
    }
    
    let name: String
    let status: AutoPaymentStatus
    let cardNumber: String
    let frequency: String
    let amount: String
    
    internal init(name: String, status: AutoPaymentStatus, cardNumber: String, frequency: String, amount: String) {
        self.name = name
        self.status = status
        self.cardNumber = cardNumber
        self.frequency = frequency
        self.amount = amount
    }
    
}

