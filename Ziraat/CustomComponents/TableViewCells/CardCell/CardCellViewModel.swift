//
//  CardCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class CardCellViewModel: CollectionViewModel {
    
    override var cellType: CollectionViewCell.Type {
        CardCell.self
    }
    
    override var itemSize: CGSize {
        .init(width: Adaptive.val(164), height: Adaptive.val(98))
    }
    
    let card: Card
    
     init(card: Card, isSelected: Bool = false) {
        self.card =  card
        super.init(uid: card.cardId, isSelected: isSelected)
    }
    
    var backImage: String { card.additions.img }
    var cardNumber: String { card.pan.makeReadableCardNumber }
    var balance: String { card.balance.description.makeReadableAmount + " " + card.currency.data.label }
    var cardHolderName: String { card.owner }
    var expDate: String { card.expire }
    
}
