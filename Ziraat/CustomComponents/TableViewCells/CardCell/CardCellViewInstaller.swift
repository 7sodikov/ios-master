//
//  CardCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol CardCellViewInstaller: ViewInstaller {
    var cardView : CardView! {get set}
}

extension CardCellViewInstaller {
    func initSubviews() {
        
        mainView.layer.cornerRadius = Adaptive.val(14)
        mainView.layer.masksToBounds = true
        
        cardView = CardView(frame: .zero)
        cardView.changeLabelSize(true)
        cardView.eyeIconContainer.isHidden = true
    }
    
    func embedSubviews() {
        mainView.addSubview(cardView)
    }
    
    func addSubviewsConstraints() {
        cardView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}
