//
//  CardCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class CardCell: CollectionViewCell, CardCellViewInstaller {
    var cardView: CardView!
    
    var mainView: UIView { contentView }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func didUpdate(viewModel: CollectionViewModel) {
        guard let viewModel = viewModel as? CardCellViewModel else { return }
        
        cardView.update(
            backgroundImage: viewModel.backImage,
            balance: viewModel.balance,
            pan: viewModel.cardNumber,
            expire: viewModel.expDate,
            holder: viewModel.cardHolderName
        )
        cardView.layer.borderWidth = viewModel.isSelected ? 2 : 0
        cardView.layer.borderColor = UIColor(51, 198, 0).cgColor
        cardView.layer.cornerRadius = 12
    }
    
}
