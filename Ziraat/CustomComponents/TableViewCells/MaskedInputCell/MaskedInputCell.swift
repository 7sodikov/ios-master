//
//  MaskedInputCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/10/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import InputMask

protocol MaskedInputCellDelegate: AnyObject {
    func maskedInput(text: String, maskType: MaskType, tag: Int, uid: String?)
}

class MaskedInputCell: TableViewCell, MaskedInputCellViewInstaller {
    
    var titleLabel: UILabel!
    var container: UIView!
    var textField: TextField!
    var textFieldListener: MaskedTextFieldDelegate!
    var stackView: UIStackView!
    var messageLabel: UILabel!
    var mainView: UIView { self.contentView }
    
    var ukey: Int? = nil
    var amount: String? = nil
    var viewModel: MaskedInputCellViewModel? = nil
    weak var maskedInputCellDelegate: MaskedInputCellDelegate? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        
    }
    
    private func setupTarget() {
        guard let viewModel = self.viewModel else { return }
    
        textFieldListener.listener = self
        textFieldListener.primaryMaskFormat = viewModel.maskType.maskFormat
        
        textField.placeholder = viewModel.maskType.placeholder + viewModel.title
        textField.keyboardType = viewModel.maskType.keyboardType
        
        titleLabel.text = viewModel.title
        
        tag = viewModel.tag
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCellTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTouchUpInsideView))
        tapGesture.cancelsTouchesInView = false
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func didTouchUpInsideView() {
        maskedInputCellDelegate?.presentPicker(at: self.ukey)
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? MaskedInputCellViewModel else { return }
        self.viewModel = viewModel
        
    
        setupTarget()
    }
    
    func updateMessageLabel() {
        
    }
    
    
    
}

extension MaskedInputCell: MaskedTextFieldDelegateListener {
    func textField(
        _ textField: UITextField,
        didFillMandatoryCharacters complete: Bool,
        didExtractValue value: String
    ) {
        
        guard let vm = self.viewModel else {return}
        maskedInputCellDelegate?.maskedInput(text: value, maskType: vm.maskType, tag: tag, uid: vm.uid)
    }
}

extension MaskedInputCellDelegate {
    
    func input(phone: String, ukey: Int) {}
    func presentPicker(at ukey: Int?) {}
    
}
