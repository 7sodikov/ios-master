//
//  MaskedInputCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/10/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit
import InputMask

protocol MaskedInputCellViewInstaller: ViewInstaller {
    var titleLabel: UILabel! {get set}
    var container: UIView! {get set}
    var textFieldListener: MaskedTextFieldDelegate! {get set}
    var textField: TextField! {get set}
    var stackView: UIStackView! {get set}
    var messageLabel: UILabel! {get set}
}

extension MaskedInputCellViewInstaller {
    
    func initSubviews() {
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .book)
        titleLabel.textColor = UIColor.buttonBlack
        
        container = .init()
        container.backgroundColor = .clear
        
        textFieldListener = MaskedTextFieldDelegate()

        textField = TextField()
        textField.delegate = textFieldListener
        textField.layer.cornerRadius = 6
        textField.backgroundColor = .white
        textField.placeholder = RS.lbl_amount.localized()
        textField.font = .gothamNarrow(size: 16, .medium)
        textField.addLeftSide(view: UIView(frame: .init(x: 0, y: 0, width: 16, height: 5)))
        textField.addShadow()
        
        
        messageLabel = UILabel()
        messageLabel.font = .gothamNarrow(size: 16, .book)
        messageLabel.textColor = UIColor(225,5,20)
        
        stackView = UIStackView()
        stackView.spacing = 12
        stackView.axis = .vertical
        stackView.backgroundColor = .clear
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(container)
        
        container.addSubview(textField)
        container.addSubview(messageLabel)
    }
    
    func addSubviewsConstraints() {
        stackView.constraint { $0.horizontal(16).vertical.equalToSuperView() }
        container.constraint({ $0.height(70) })
        textField.constraint { $0.height(50).top.horizontal.equalToSuperView() }
        
        messageLabel.constraint { (make) in
            make.top(self.textField.bottomAnchor).leading.trailing.bottom.equalToSuperView()
        }
    }
    
    func localizeText() {
        titleLabel.text = RS.lbl_amount.localized()
    }
    
}
