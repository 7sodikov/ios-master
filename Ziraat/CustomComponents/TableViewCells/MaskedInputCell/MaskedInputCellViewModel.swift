//
//  MaskedInputCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/10/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class MaskedInputCellViewModel: TableViewModel {
    
    override var cellType: TableViewCell.Type {
        MaskedInputCell.self
    }
    
    let title, value: String
    let maskType: MaskType
    let tag: Int
    
    init(uid: String? = nil, title: String, value: String, maskType: MaskType, tag: Int = 0) {
        self.title = title
        self.value = value
        self.maskType = maskType
        self.tag = tag
        super.init(uid: uid)
        
    }
    
}

enum MaskType {
    case phone
    case none
    
    var keyboardType: UIKeyboardType {
        switch self {
        case .phone:
            return .phonePad
        
        case .none:
            return .default
        }
    }
    
    var maskFormat: String {
        switch self {
        case .phone:
            return  "+998 [00] [000] [00] [00]"
        case .none:
            return .init()
        }
    }
    
    var placeholder: String {
        switch self {
        case .phone:
            return "+998 "
        case .none:
            return ""
        }
    }
    
}
