//
//  ButtonsCellViewModel.swift
//  MilliyCore
//
//  Created by Macbook on 4/30/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public class ButtonsCellViewModel: TableViewModel {
    
    public override var cellType: TableViewCell.Type {
        ButtonsCell.self
    }
    
    public let leftButtonStyle: ButtonStyle
    public let rightButtonStyle: ButtonStyle
    public let tag: Int
    
    public init(leftButtonStyle: ButtonStyle, rightButtonStyle: ButtonStyle, tag: Int) {
        self.leftButtonStyle = leftButtonStyle
        self.rightButtonStyle = rightButtonStyle
        self.tag = tag
        super.init(uid: tag.description)
    }
    
}

public enum ButtonStyle {
    case red(title: String)
    case bordered(title: String)
    
    var title: String {
        switch self {
        case .bordered(let title),
             .red(let title):
            return title
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
            
        case .red:
            return UIColor(225,5,20)
        case .bordered:
            return .clear
        }
    }
    
    var borderWidth: CGFloat {
        switch self {
            
        case .red:
            return .zero
        case .bordered:
            return 2.0
        }
    }
    
    var borderColor: CGColor {
        switch self {
            
        case .red:
            return backgroundColor.cgColor
        case .bordered:
            return titleColor.cgColor
        }
    }
    
    var titleColor: UIColor {
        switch self {
        case .red:
            return .white
        case .bordered:
            return UIColor(84,95,101)
        }
    }
}
