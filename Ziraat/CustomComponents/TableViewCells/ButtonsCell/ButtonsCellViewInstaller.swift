//
//  ButtonsCellViewInstaller.swift
//  MilliyCore
//
//  Created by Macbook on 4/30/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

protocol ButtonsCellViewInstaller: ViewInstaller {
    var leftButton: UIButton! { get set }
    var rightButton: UIButton! { get set }
    var stackView: UIStackView! { get set }
}

extension ButtonsCellViewInstaller {
    
    func initSubviews() {
        leftButton = UIButton()
        leftButton.layer.cornerRadius = 25
        leftButton.backgroundColor = .clear
        leftButton.layer.borderWidth = 2
        leftButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        leftButton.setTitleColor(.black, for: .normal)
        leftButton.titleLabel?.numberOfLines = 2
        
        rightButton = UIButton()
        rightButton.layer.cornerRadius = 25
        rightButton.backgroundColor = UIColor(225,5,20)
        rightButton.setTitleColor(.white, for: .normal)
        rightButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = Adaptive.val(16)
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
        
        stackView.addArrangedSubview(leftButton)
        stackView.addArrangedSubview(rightButton)
    }
    
    func addSubviewsConstraints() {
        stackView.constraint { $0.height(50).horizontal(16).vertical.equalToSuperView() }
    }

}
