//
//  ButtonsCell.swift
//  MilliyCore
//
//  Created by Macbook on 4/30/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public protocol ButtonsCellDelegate: AnyObject {
    func buttonCellsButtonClicked(at tag: Int, isLeft: Bool)
}

public class ButtonsCell: TableViewCell, ButtonsCellViewInstaller {
    
    var leftButton: UIButton!
    var rightButton: UIButton!
    var stackView: UIStackView!
    
    var mainView: UIView { self.contentView }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTargets()
    }
    
    public weak var buttonDelegate: ButtonsCellDelegate?
    
    func setupTargets() {
        for (index, button) in [leftButton, rightButton].enumerated() {
            button?.addTarget(self, action: #selector(self.buttonActionHandler(_:)), for: .touchUpInside)
            button?.tag = index
        }
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ButtonsCellViewModel else { return }
        
        leftButton.configure(with: viewModel.leftButtonStyle)
        rightButton.configure(with: viewModel.rightButtonStyle)
        tag = viewModel.tag
    }
    
    @objc func buttonActionHandler(_ sender: UIButton) {
        buttonDelegate?.buttonCellsButtonClicked(at: tag, isLeft: sender.tag == leftButton.tag)
    }
    
}

extension UIButton {
    
    func configure(with style: ButtonStyle) {
        backgroundColor = style.backgroundColor
        layer.borderWidth = style.borderWidth
        layer.borderColor = style.borderColor
        setTitleColor(style.titleColor, for: .normal)
        setTitle(style.title, for: .normal)
    }
    
}
