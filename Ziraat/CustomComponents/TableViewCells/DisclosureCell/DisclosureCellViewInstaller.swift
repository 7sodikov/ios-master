//
//  DisclosureCellViewInstaller.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 23/01/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

internal protocol DisclosureCellViewInstaller: ViewInstaller {
    var iconView: UIImageView! {get set}
    var titleLabel: UILabel! {get set}
    var topLine: UIView! {get set}
    var bottomLine: UIView! {get set}
}

internal extension DisclosureCellViewInstaller {
    
    func initSubviews() {
        iconView = UIImageView()
        iconView.contentMode = .scaleAspectFill
        iconView.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel = UILabel()
        titleLabel.font = UIFont(size: 16, weight: .book)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        topLine = UIView()
        topLine.backgroundColor = UIColor.blueGrey.withAlphaComponent(0.15)
        topLine.translatesAutoresizingMaskIntoConstraints = false
        
        bottomLine = UIView()
        bottomLine.backgroundColor = UIColor.blueGrey.withAlphaComponent(0.15)
        bottomLine.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func embedSubviews() {
        mainView.addSubview(topLine)
        mainView.addSubview(bottomLine)
        mainView.addSubview(iconView)
        mainView.addSubview(titleLabel)
    }
    
    func addSubviewsConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 12),
            titleLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -44),
            titleLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -12),
            titleLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 56),
            
            iconView.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor, constant: -16),
            iconView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            iconView.heightAnchor.constraint(equalToConstant: 20),
            iconView.widthAnchor.constraint(equalToConstant: 20),
            
            topLine.topAnchor.constraint(equalTo: mainView.topAnchor),
            topLine.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            topLine.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            topLine.heightAnchor.constraint(equalToConstant: 1.0),
                        
            bottomLine.leadingAnchor.constraint(equalTo: mainView.leadingAnchor),
            bottomLine.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            bottomLine.bottomAnchor.constraint(equalTo: mainView.bottomAnchor),
            bottomLine.heightAnchor.constraint(equalToConstant: 1.0),
        ])
    }
    
    func cellHeightFor(_ text: String) -> CGFloat {
        let width = UIScreen.main.bounds.width - 100
        let frame = CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude)
        let label = UILabel(frame: frame)
        label.font = UIFont(size: 16, weight: .book)
        label.text = text
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        return label.bounds.height + 24
    }
}
