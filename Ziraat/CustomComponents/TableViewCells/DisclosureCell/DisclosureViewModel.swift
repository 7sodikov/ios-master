//
//  DisclosureViewModel.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 23/01/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public final class DisclosureViewModel: TableViewModel {
    
    public override var cellType: TableViewCell.Type {
        DisclosureCell.self
    }
    
    public let iconName: String
    public let title: String
    public let showsSeparatorTopLine: Bool
    public let showsSeparatorBottomLine: Bool
    public var accessoryType: UITableViewCell.AccessoryType = .disclosureIndicator
    
    public init(uid: String? = nil, iconName: String, title: String, showsSeparatorTopLine: Bool = false, showsSeparatorBottomLine: Bool = true) {
        self.iconName = iconName
        self.title = title
        self.showsSeparatorTopLine = showsSeparatorTopLine
        self.showsSeparatorBottomLine = showsSeparatorBottomLine
        super.init(uid: uid)
    }
}
