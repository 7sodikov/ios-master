//
//  DisclosureCell.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 23/01/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

open class DisclosureCell: TableViewCell, DisclosureCellViewInstaller {
    
    internal var iconView: UIImageView!
    internal var titleLabel: UILabel!
    internal var topLine: UIView!
    internal var bottomLine: UIView!
    internal var mainView: UIView { contentView }

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var cellHeight: CGFloat {
        48
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? DisclosureViewModel else { return }
        if !viewModel.iconName.isEmpty {
            iconView.image =  UIImage(named: viewModel.iconName) ?? UIImage(named: viewModel.iconName)
        }
        titleLabel.text = viewModel.title
        accessoryType = viewModel.accessoryType
        
        topLine.isHidden = !viewModel.showsSeparatorTopLine
        bottomLine.isHidden = !viewModel.showsSeparatorBottomLine
    }
    
}

