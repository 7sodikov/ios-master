//
//  DetailCenteredCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class DetailCenteredCell: TableViewCell, DetailCenteredCellViewInstaller {
    
    var titleLabel: UILabel!
    var valueLabel: UILabel!
    var stackView: UIStackView!
    var mainView: UIView { self.contentView }
    
    weak var buttonDelegate: ButtonCellDelegate? = nil

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
        backgroundColor = .clear
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? DetailCenteredCellViewModel else { return }
        titleLabel.text = viewModel.title
        valueLabel.text = viewModel.value
    }

    
}

