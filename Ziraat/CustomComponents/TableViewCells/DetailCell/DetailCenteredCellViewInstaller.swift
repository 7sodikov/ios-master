//
//  DetailCenteredCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DetailCenteredCellViewInstaller: ViewInstaller {
    var titleLabel: UILabel! {get set}
    var valueLabel: UILabel! {get set}
    var stackView: UIStackView! {get set}
}

extension DetailCenteredCellViewInstaller {
    
    func initSubviews() {
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 14, .medium)
        titleLabel.textColor = UIColor(145,150,155)
        titleLabel.textAlignment = .center
        
        valueLabel = .init()
        valueLabel.font = .gothamNarrow(size: 16, .book)
        valueLabel.textAlignment = .center
        valueLabel.numberOfLines = 0
        valueLabel.lineBreakMode = .byWordWrapping
        valueLabel.textColor = UIColor(68,80,86)
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(valueLabel)
    }
    
    func addSubviewsConstraints() {
        stackView.constraint { (make) in
            make.vertical(8).horizontal(16).equalToSuperView()
        }
    }
    
}

