//
//  DetailCenteredCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation


class DetailCenteredCellViewModel: TableViewModel {
    

    override var cellType: TableViewCell.Type {
        DetailCenteredCell.self
    }
    
    var title, value: String

    internal init(title: String, value: String) {
        self.title = title
        self.value = value
    }
}
