//
//  HorizontalDetailCellViewModel.swift
//  MilliyCore
//
//  Created by Macbook on 21/06/21.
//  Copyright © 2020 Amirov Jasur. All rights reserved.
//

import UIKit

public class HorizontalDetailCellViewModel: TableViewModel {
    
    public var title: String
    public var value: String
    
    public override var cellType: TableViewCell.Type {
        HorizontalDetailCell.self
    }
    
    public override var estimatedRowHeight: CGFloat {
        56
    }
    
    public init(title: String, value: String) {
        self.title = title
        self.value = value
        super.init(uid: nil)
    }
    
}
