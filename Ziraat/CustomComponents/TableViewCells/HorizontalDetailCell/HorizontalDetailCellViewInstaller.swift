//
//  HorizontalDetailCellViewInstaller.swift
//  MilliyCore
//
//  Created by Macbook on 21/06/21.
//  Copyright © 2020 Amirov Jasur. All rights reserved.
//

import UIKit

protocol HorizontalDetailCellViewInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var valueLabel: UILabel! { get set }
    var stackView: UIStackView! {get set}
}

extension HorizontalDetailCellViewInstaller {
    
    func initSubviews() {
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .book)
        titleLabel.textColor = UIColor(89, 96, 103)
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        valueLabel = UILabel()
        valueLabel.font = .gothamNarrow(size: 16, .book)
        valueLabel.textColor = UIColor(89, 96, 103)
//        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .equalCentering
        stackView.translatesAutoresizingMaskIntoConstraints = false
                
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
    
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(valueLabel)
    }
    
    func addSubviewsConstraints() {
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: mainView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -16),
        ])
    }
    
}
