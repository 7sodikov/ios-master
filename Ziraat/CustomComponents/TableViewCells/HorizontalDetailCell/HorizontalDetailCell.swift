//
//  HorizontalDetailCell.swift
//  MilliyCore
//
//  Created by Macbook on 21/06/21.
//  Copyright © 2020 Amirov Jasur. All rights reserved.
//

import UIKit

public class HorizontalDetailCell: TableViewCell, HorizontalDetailCellViewInstaller {
    
    var titleLabel: UILabel!
    var valueLabel: UILabel!
    var stackView: UIStackView!
    var mainView: UIView { self.contentView }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        setupSubviews()
        setupTargets()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var cellHeight: CGFloat {
       56
    }
    
    func setupTargets() {
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? HorizontalDetailCellViewModel else { return }
        titleLabel.text = viewModel.title
        valueLabel.text = viewModel.value
    }
    
}
