//
//  DetailsCell.swift
//  MilliyCore
//
//  Created by Amirov Jasur on 6/04/2021.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public protocol DetailsCellDelegate: AnyObject {
    func editButtonClicked(tag: Int)
}

open class DetailsCell: TableViewCell, DetailsCellViewInstaller {
    
    var mainContainer: UIView!
    var headerTitle: UILabel!
    var container: UIView!
    var valueStack: UIStackView!
    var valueTitleLabel: UILabel!
    var valueLabel: UILabel!
    var iconButton: UIButton!
    
    internal var mainView: UIView { contentView }
    public weak var editDelegate: DetailsCellDelegate? = nil

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        backgroundColor = .clear
        setupSubviews()
        setupTargets()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTargets() {
        iconButton.addTarget(self, action: #selector(iconButtonClicked), for: .touchUpInside)
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? DetailsCellViewModel else { return }
       
        headerTitle.text = viewModel.headerTitle
        
        valueTitleLabel.text = viewModel.valueTitle
        valueTitleLabel.isHidden = viewModel.isTitleHidden
        
        valueLabel.text = viewModel.value
        valueLabel.isHidden = viewModel.isValueHidden
        
        iconButton.setImage(UIImage(named: viewModel.iconName), for: .normal)
        iconButton.isHidden = !viewModel.canEdit
        
        tag = viewModel.tag
    }
    
    @objc func iconButtonClicked() {
        editDelegate?.editButtonClicked(tag: tag)
    }
    
}
