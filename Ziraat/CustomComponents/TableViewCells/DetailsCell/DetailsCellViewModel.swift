//
//  DetailsCellViewModel.swift
//  MilliyCore
//
//  Created by Amirov Jasur on 6/04/2021.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//
import UIKit

public final class DetailsCellViewModel: TableViewModel {
    
    public override var cellType: TableViewCell.Type {
        DetailsCell.self
    }
    
    public override var estimatedRowHeight: CGFloat {
        return 116 - (isTitleHidden || isValueHidden ? 38 : 0)
    }
    
    public let headerTitle, valueTitle, value: String
    public let iconName: String
    public let canEdit: Bool
    public var tag: Int = 0
    
    public init(uid: String? = nil, headerTitle: String, valueTitle: String, value: String, iconName: String = "btn_icon_pen", canEdit: Bool = true) {
        self.headerTitle = headerTitle
        self.valueTitle = valueTitle
        self.value = value
        self.iconName = iconName
        self.canEdit = canEdit
        super.init(uid: uid)
    }
    
    
    var isTitleHidden: Bool { valueTitle.isEmpty }
    var isValueHidden: Bool { value.isEmpty }
}
