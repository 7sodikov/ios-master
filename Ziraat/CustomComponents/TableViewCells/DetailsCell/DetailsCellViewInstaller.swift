//
//  DisclosureCellViewInstaller.swift
//  MilliyCore
//
//  Created by Amirov Jasur on 6/04/2021.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

internal protocol DetailsCellViewInstaller: ViewInstaller {
    var mainContainer: UIView! {get set}
    var headerTitle: UILabel! {get set}
    var container: UIView! {get set}
    var valueStack: UIStackView! {get set}
    var valueTitleLabel: UILabel! {get set}
    var valueLabel: UILabel! {get set}
    var iconButton: UIButton! {get set}
}

internal extension DetailsCellViewInstaller {
    
    func initSubviews() {
        
        mainContainer = UIView()
        mainContainer.backgroundColor = .white
        
        headerTitle = UILabel()
        headerTitle.textColor = UIColor(225,5,20)
        headerTitle.font = .gothamNarrow(size: 16, .book)
        
        container = UIView()
        container.backgroundColor = UIColor(231,233,234)
        container.layer.cornerRadius = 6
        
        valueTitleLabel = UILabel()
        valueTitleLabel.textColor = UIColor(84,95,101)
        valueTitleLabel.font = .gothamNarrow(size: 15, .book)
        
        valueLabel = UILabel()
        valueLabel.textColor = UIColor(84,95,101)
        valueLabel.font = .gothamNarrow(size: 15, .book)
        
        valueStack = UIStackView()
        valueStack.axis = .vertical
        valueStack.spacing = 12
        
        iconButton = UIButton()
        iconButton.imageView?.contentMode = .scaleAspectFill
        iconButton.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    func embedSubviews() {
        mainView.addSubview(mainContainer)
        
        mainContainer.addSubview(headerTitle)
        mainContainer.addSubview(container)
        
        container.addSubview(valueStack)
        container.addSubview(iconButton)
        
        valueStack.addArrangedSubview(valueTitleLabel)
        valueStack.addArrangedSubview(valueLabel)
        
    }
    
    func addSubviewsConstraints() {
        mainContainer.constraint { (make) in
            make.vertical.horizontal(16).equalToSuperView()
        }
        headerTitle.constraint { (make) in
            make.top(8).leading(24).trailing(-16).equalToSuperView()
        }
        container.constraint { (make) in
            make.top(self.headerTitle.bottomAnchor, offset: 8)
                .horizontal(16).bottom(-8).equalToSuperView()
        }
        iconButton.constraint { (make) in
            make.square(20).top(10).trailing(-10).equalToSuperView()
        }
        valueStack.constraint { make in
            make.trailing(self.iconButton.leadingAnchor, offset: -8)
                .leading(8).centerY.equalToSuperView()
        }
    }
    
    
}
