//
//  InputCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol InputCellDelegate: AnyObject {
    func input(text: String, tag: Int, uid: String?)
    func presentPicker(at ukey: Int?)
}

class InputCell: TableViewCell, InputCellViewInstaller {
    
    var titleLabel: UILabel!
    var container: UIView!
    var textField: TextField!
    var stackView: UIStackView!
    var messageLabel: UILabel!
    var mainView: UIView { self.contentView }
    
    var ukey: Int? = nil
    var amount: String? = nil
    var viewModel: InputCellViewModel? = nil
    weak var inputCellDelegate: InputCellDelegate? = nil
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        clear()
    }
    
    private func setupTarget() {
        guard let viewModel = self.viewModel else { return }
        switch viewModel.inputType {
        case .amount:
            textField.addTarget(self, action: #selector(didEnterAmount(_:)), for: .editingChanged)
            textField.keyboardType = .decimalPad
        case .text:
            textField.addTarget(self, action: #selector(didEntedText), for: .editingChanged)
            textField.keyboardType = .default
        case .mfo:
            textField.addTarget(self, action: #selector(didEntedMfo(_:)), for: .editingChanged)
            textField.keyboardType = .numberPad
        case .search:
            textField.addTarget(self, action: #selector(didBeginSearch), for: .touchUpInside)
            textField.keyboardType = .default
        case  let .picker(_,_, ukey):
            self.ukey = ukey
            setupCellTapGesture()
            textField.isEnabled = false
            let rightView = UIImageView(image: UIImage(named: "img_jam_chevron-left"))
            textField.addRightSide(view: rightView, by: .init(top: 0, left: 0, bottom: 0, right: 8))
        }
        textField.placeholder = viewModel.inputType.placeholder
        textField.text = viewModel.inputType.textValue
        
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCellTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTouchUpInsideView))
        tapGesture.cancelsTouchesInView = false
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func didTouchUpInsideView() {
        inputCellDelegate?.presentPicker(at: self.ukey)
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? InputCellViewModel else { return }
        self.viewModel = viewModel
        
        titleLabel.text = viewModel.headerTitle
        tag = viewModel.tag
        
        updateMessageLabel(viewModel.notify)
        clear()
        setupTarget()
    }
    
    func updateMessageLabel(_ notify: InputCellViewModel.InputNotify) {
        messageLabel.isHidden = notify.messageAttributedString == nil
        messageLabel.attributedText = notify.messageAttributedString
    }
    
    func clear() {
        textField.removeTarget(self, action: #selector(didEnterAmount), for: .editingChanged)
        textField.removeTarget(self, action: #selector(didEntedText), for: .editingChanged)
        textField.removeTarget(self, action: #selector(didEntedMfo(_:)), for: .editingChanged)
        textField.removeTarget(self, action: #selector(didBeginSearch(_:)), for: .editingChanged)
        textField.keyboardType = .default
    }
    
    @objc private func didEnterAmount(_ sender: UITextField) {
        guard var amount = sender.text?.withoutWhiteSpace.replacingOccurrences(of: ",", with: ".") else { return }
        textField.text = amount.makeReadableAmount
        
        if let index = amount.lastIndex(of: "."), amount[index...].count > 3 {
            amount.removeLast()
            textField.text = amount.makeReadableAmount
        }
        inputCellDelegate?.input(text: amount, tag: tag, uid: viewModel?.uid)
    }
    
    @objc private func didEntedText(_ sender: UITextField) {
        guard let text = sender.text else { return }
        inputCellDelegate?.input(text: text, tag: tag, uid: viewModel?.uid)
    }
    
    @objc func didEntedMfo(_ sender: UITextField) {
        guard let mfo = sender.text else { return }
        if mfo.count > 5 {
            textField.deleteBackward()
            return
        }
        inputCellDelegate?.input(text: mfo, tag: tag, uid: viewModel?.uid)
    }
    
    @objc func didBeginSearch(_ sender: UITextField) {
        
    }
    
}

extension InputCellDelegate {
    
    func input(phone: String, ukey: Int) {}
    func presentPicker(at ukey: Int?) {}
    
}
