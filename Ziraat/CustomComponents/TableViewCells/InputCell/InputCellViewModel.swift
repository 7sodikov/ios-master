//
//  InputCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class InputCellViewModel: TableViewModel {
    
    override var cellType: TableViewCell.Type {
        InputCell.self
    }
    
    var headerTitle: String
    var inputType: Input
    var notify: InputNotify
    var tag: Int = -1
    
    init(uid: String? = nil, headerTitle: String, inputType: Input, notify: InputNotify = .none) {
        self.headerTitle = headerTitle
        self.inputType = inputType
        self.notify = notify
        super.init(uid: uid)
        
    }
    
    var messageAttributedString: NSAttributedString {
        let text = RS.lbl_tip.localized()
        var attributes: [NSAttributedString.Key : Any] = [:]
        attributes[.font] = UIFont.gothamNarrow(size: 16, .book)
//        attributes[.foregroundColor] = message == nil ? UIColor(225,5,20) : UIColor(145, 150, 155)
        
        return .init(string: text, attributes: attributes)
    }
    
}

extension InputCellViewModel {
    
    enum Input {
        case amount(_ amount: String, placeholder: String)
        case text(_ text: String, placeholder: String)
        case mfo(_ mfo: String, placeholder: String)
        case search(_ text: String, placeHoder: String)
        case picker(title: String, value: String?, ukey: Int)
        
        var textValue: String {
            switch self {
                
            case let .amount(amount, _):
                return amount
            case let .text(text, _):
                return text
            case let .mfo(mfo, _):
                return mfo
            case let .search(text, _):
                return text
            case let .picker(_, value, _):
                return value ?? ""
            }
        }
        
        var placeholder: String {
            switch self {
            case let .amount(_, placeHolder):
                return placeHolder
            case let .mfo(_, placeHolder):
                return placeHolder
            case let .text(_, placeHolder):
                return placeHolder
            case let .search(_, placeHoder):
                return placeHoder
            case let .picker(title, _,_):
                return title
            }
        }
    }
    
    enum InputNotify {
        case error(_ text: String)
        case notify(_ text: String)
        case none
        
        var messageAttributedString: NSAttributedString? {
            
            switch self {
                
            case .error(let text):
                return attributedString(with: text, color: .buttonRed)
            case .notify(let text):
                return  attributedString(with: text, color: .buttonBlack)
            case .none:
                return nil
            }
            
        }
        
        func attributedString(with text: String, color: UIColor) -> NSAttributedString {
            var attributes: [NSAttributedString.Key : Any] = [:]
            attributes[.font] = UIFont.gothamNarrow(size: 16, .book)
            attributes[.foregroundColor] = color
            return .init(string: text, attributes: attributes)
        }
        
    }
    
}
