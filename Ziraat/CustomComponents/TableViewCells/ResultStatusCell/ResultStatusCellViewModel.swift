//
//  ResultStatusCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class ResultStatusCellViewModel: TableViewModel {
    
    public override var cellType: TableViewCell.Type {
        ResultStatusCell.self
    }
    
    
    let status: Status
    
    init(_ status: Status) {
        self.status = status
        super.init(uid: nil)
    }
 
    enum Status {
        case success(message: String)
        case failure(message: String)
        case aPDeleteWarning
        
        var message: String {
            switch self {
            case .success(let message):
                return message
            case .failure(let message):
                return message
            case .aPDeleteWarning:
                return RS.lbl_cancel_auto_pay_instruction.localized()
            }
        }
        
        var icon: String {
            switch self {
            case .success:
                return "img_success"
            case .failure:
                return "btn_close"
            case .aPDeleteWarning:
                return "icon_warning"
            }
        }
        
        var backColor: UIColor {
            switch self {
            case .success:
                return .white
            case .failure:
                return UIColor(225,5,20)
            case .aPDeleteWarning:
                return .white
            }
        }
        
//        var buttonTile: String {
//            switch self {
//            case .success:
//                return RS.btn_close.localized()
//            case .failure:
//                return RS.btn_return_to_homepage.localized()
//            }
//        }
    }
    
}
