//
//  ResultStatusCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

open class ResultStatusCell: TableViewCell, ResultStatusCellViewInstaller {
    var circleView: UIView!
    var iconView: UIImageView!
    var titleLabel: UILabel!
    var stackView: UIStackView!
    
    internal var mainView: UIView { contentView }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        setupSubviews()
    }
    
    public required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ResultStatusCellViewModel else { return }
        
        circleView.backgroundColor = viewModel.status.backColor
        iconView.image = UIImage(named: viewModel.status.icon)
        titleLabel.text = viewModel.status.message
        if case .aPDeleteWarning = viewModel.status {
            iconView.snp.remakeConstraints { $0.edges.equalToSuperview() }
        }
    }
}

