//
//  ResultStatusCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/27/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

 protocol ResultStatusCellViewInstaller: ViewInstaller {
//    var contain
    var circleView: UIView! {get set}
    var iconView: UIImageView! {get set}
    var titleLabel: UILabel! {get set}
    var stackView: UIStackView! {get set}
}


 extension ResultStatusCellViewInstaller {
    
    func initSubviews() {
        
        mainView.backgroundColor = .clear
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .book)
        titleLabel.textColor = UIColor.black
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        circleView = UIView()
        circleView.layer.cornerRadius = 40
        circleView.addShadow()
        
        iconView = UIImageView()
//        iconView.tintColor = .white
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
      
    }
    
    func embedSubviews() {
//        mainView.addSubview(stackView)
        circleView.addSubview(iconView)
        
        mainView.addSubview(circleView)
        mainView.addSubview(titleLabel)
    }
    
    func addSubviewsConstraints() {
        circleView.constraint { $0.square(80).top.centerX.equalToSuperView() }
        iconView.constraint { (make) in
            make.square(36).center.equalToSuperView()
        }
//        stackView.constraint { (make) in
//            make.center.equalToSuperView()
//        }
        
        titleLabel.constraint { (make) in
            make.top(self.circleView.bottomAnchor, offset: 16).horizontal(32).bottom.equalToSuperView()
        }
//        titleLabel.widthAnchor.constraint(lessThanOrEqualToConstant: (UIScreen.main.bounds.width - 64)).isActive = true
    }
    
}
