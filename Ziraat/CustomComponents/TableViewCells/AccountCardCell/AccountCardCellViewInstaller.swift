//
//  AccountCardCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol AccountCardCellViewInstaller: ViewInstaller {
    var accountCard: AccountCardView! {get set}

}

extension AccountCardCellViewInstaller {
    
    func initSubviews() {
        mainView.backgroundColor = .clear
        
        accountCard = AccountCardView()
    }
    
    func embedSubviews() {
        mainView.addSubview(accountCard)
    }
    
    func addSubviewsConstraints() {
        accountCard.constraint { (make) in
            make.edges.equalToSuperView()
        }
    }
    
}
