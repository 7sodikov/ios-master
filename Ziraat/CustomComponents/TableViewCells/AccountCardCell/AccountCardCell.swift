//
//  AccountCardCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


class AccountCardCell: CollectionViewCell, AccountCardCellViewInstaller {
    
    var accountCard: AccountCardView!
    var mainView: UIView { self.contentView }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func didUpdate(viewModel: CollectionViewModel) {
        guard let viewModel = viewModel as? AccountCardCellViewModel else { return }
        let account = viewModel.account
        let balance = account.balance.description.makeReadableAmount + " " + account.currency.data.label
        accountCard.update(currencyIcon: "",
                           userName: account.name,
                           account: account.number,
                           fund: balance,
                           balance: balance)
    }
    
}
