//
//  AccountCardCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

final class AccountCardCellViewModel: CollectionViewModel {
    
    override var cellType: CollectionViewCell.Type {
        AccountCardCell.self
    }
    
    override var itemSize: CGSize {
        return .init(width: 296, height: 180)
    }
    
    private(set) var account: Account
    private(set) var isBalanceHidden: Bool
    var allowsSelection = false
    
    init(account: Account, isBalanceHidden: Bool) {
        self.account = account
        self.isBalanceHidden = isBalanceHidden
        super.init(uid: account.number)
    }
    
    public func updateBalance(_ account: Account?) {
       
    }
    
}
