//
//  DisclosureBorderViewModel.swift
//  MilliyCore
//
//  Created by Amirov Jasur on 27/05/2021.
//

public final class DisclosureBorderViewModel: TableViewModel {
    
    public override var cellType: TableViewCell.Type {
        DisclosureBorderCell.self
    }
    
    public let iconName: String
    public let title: String
    
    public init(uid: String? = nil, iconName: String, title: String) {
        self.iconName = iconName
        self.title = title
        super.init(uid: uid)
    }
}
