//
//  DisclosureBorderCell.swift
//  MilliyCore
//
//  Created by Amirov Jasur on 27/05/2021.
//

import UIKit
import Kingfisher

open class DisclosureBorderCell: TableViewCell, DisclosureBorderCellViewInsatller {
    
    internal var container: UIView!
    internal var stackView: UIStackView!
    internal var iconView: UIImageView!
    internal var titleLabel: UILabel!
    internal var rightIconView: UIImageView!
    
    internal var mainView: UIView { contentView }

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        setupSubviews()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var cellHeight: CGFloat {
        66
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? DisclosureBorderViewModel else { return }
        iconView.kf.setImage(with: URL(string: viewModel.iconName), placeholder: nil)
        
        titleLabel.text = viewModel.title
    }
    
}
