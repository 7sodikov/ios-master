//
//  DisclosureBorderCellViewInsatller.swift
//  MilliyCore
//
//  Created by Amirov Jasur on 27/05/2021.
//

import UIKit

protocol DisclosureBorderCellViewInsatller: ViewInstaller {
    var container: UIView! {get set}
    var stackView: UIStackView! {get set}
    var iconView: UIImageView! {get set}
    var titleLabel: UILabel! {get set}
    var rightIconView: UIImageView! {get set}
}

extension DisclosureBorderCellViewInsatller {
    
    
    func initSubviews() {
        container = UIView()
        container.addShadow()
        container.backgroundColor = .white
        container.layer.cornerRadius = 6
        container.layer.borderWidth = 2
        
        stackView = UIStackView()
        stackView.spacing = 8
        
        iconView = UIImageView()
        iconView.contentMode = .scaleAspectFill
        iconView.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .book)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        rightIconView = UIImageView()
        rightIconView.image = UIImage(named: "img_disclosure")
        rightIconView.contentMode = .scaleAspectFill
        rightIconView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func embedSubviews() {
        mainView.addSubview(container)
        container.addSubview(stackView)
        container.addSubview(rightIconView)
        
        stackView.addArrangedSubview(iconView)
        stackView.addArrangedSubview(titleLabel)
    }
    
    func addSubviewsConstraints() {
        container.constraint { make in
            make.height(50).horizontal(16).vertical(8).equalToSuperView()
        }
        rightIconView.constraint { maker in
            maker.square(24).trailing(-8).centerY.equalToSuperView()
        }
        stackView.constraint { maker in
            maker.trailing(self.rightIconView.leadingAnchor, offset: -8)
                .leading(8).vertical(8).equalToSuperView()
        }
        iconView.constraint { $0.square(34) }
    }
    
}
