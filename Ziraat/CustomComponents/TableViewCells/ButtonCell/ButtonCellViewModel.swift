//
//  ButtonCellViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/19/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation


class ButtonCellViewModel: TableViewModel {

    override var cellType: TableViewCell.Type {
        ButtonCell.self
    }
    
    let style: ButtonStyle
    let isEnabled: Bool
    let tag: Int
    
    internal init(style: ButtonStyle, isEnabled: Bool, tag: Int) {
        self.style = style
        self.isEnabled = isEnabled
        self.tag = tag
    }
    
}
