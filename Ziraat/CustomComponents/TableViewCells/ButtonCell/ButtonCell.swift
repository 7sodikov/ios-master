//
//  ButtonCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/19/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ButtonCellDelegate: AnyObject {
    func buttonClicked(tag: Int)
}

class ButtonCell: TableViewCell, ButtonCellViewInstaller {
    
    var button: UIButton!
    var mainView: UIView { self.contentView }
    
    weak var buttonDelegate: ButtonCellDelegate? = nil

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        setupSubviews()
        setupTarget()
    }
    
    private func setupTarget() {
        button.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    @objc func buttonClicked() {
        buttonDelegate?.buttonClicked(tag: tag)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func set(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ButtonCellViewModel else { return }
        
        button.configure(with: viewModel.style)
        button.isEnabled = viewModel.isEnabled
        tag = viewModel.tag
    }

    
}
