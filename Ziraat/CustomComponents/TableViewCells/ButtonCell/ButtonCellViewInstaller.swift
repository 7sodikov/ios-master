//
//  ButtonCellViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/19/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ButtonCellViewInstaller: ViewInstaller {
    var button: UIButton! {get set}
}

extension ButtonCellViewInstaller {
    
    func initSubviews() {
        button = UIButton()
        button.backgroundColor = UIColor(255, 5, 20)
        button.layer.cornerRadius = Adaptive.val(25)
        button.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        button.layer.masksToBounds = true
//        button.roundCorners(corners: .allCorners, radius: Adaptive.val(25))
        button.setTitleColor(.white, for: .normal)
    }
    
    func embedSubviews() {
        mainView.addSubview(button)
    }
    
    func addSubviewsConstraints() {
        button.constraint { (make) in
            make.height(Adaptive.val(50)).horizontal(16).vertical(8).equalToSuperView()
        }
    }
    
}

