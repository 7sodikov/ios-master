//
//  ReceiverCellViewInstaller.swift
//  MilliyCore
//
//  Created by Macbook on 3/9/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

protocol ReceiverCellViewInstaller: ViewInstaller {
    var container: UIView! {get set}
    var logoView: UIImageView! {get set}
    var holderLabel: UILabel! { get set }
    var cardNumberLabel: UILabel! { get set }
    var stackView: UIStackView! {get set}
    var settingsButton: UIButton! {get set}
}

extension ReceiverCellViewInstaller {
    
    func initSubviews() {
        
        container = UIView()
        container.backgroundColor = .white
        container.layer.cornerRadius = 6
        container.addShadow()
        
        logoView = UIImageView()
        logoView.contentMode = .scaleAspectFit
        
        cardNumberLabel = UILabel()
        cardNumberLabel.font = .gothamNarrow(size: 18, .book)
        cardNumberLabel.textColor = UIColor(84, 95, 101)
        cardNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        
        holderLabel = UILabel()
        holderLabel.font = .gothamNarrow(size: 16, .book)
        holderLabel.textColor = UIColor(145, 150, 155)
        holderLabel.translatesAutoresizingMaskIntoConstraints = false
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 2.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
                
        settingsButton = UIButton()
        settingsButton.backgroundColor = .clear
        settingsButton.setImage(UIImage(named: "img_login_settings")?.withRenderingMode(.alwaysTemplate), for: .normal)
        settingsButton.tintColor = .black
        settingsButton.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func embedSubviews() {
        mainView.addSubview(container)
        
        container.addSubview(logoView)
        container.addSubview(stackView)
        container.addSubview(settingsButton)
        
        stackView.addArrangedSubview(cardNumberLabel)
        stackView.addArrangedSubview(holderLabel)
    }
    
    func addSubviewsConstraints() {
        container.constraint { make in
            make.vertical(8).horizontal(16).equalToSuperView()
        }
        logoView.constraint { make in
            make.square(54).vertical(10).leading(10).equalToSuperView()
        }
        settingsButton.constraint { make in
            make.square(28).trailing(-16).centerY.equalToSuperView()
        }
        stackView.constraint { make in
            make.leading(self.logoView.trailingAnchor, offset: 10)
                .trailing(self.settingsButton.leadingAnchor, offset: 10)
                .centerY.equalToSuperView()
        }
        
    }
    
}
