//
//  ReceiverCellViewModel.swift
//  MilliyCore
//
//  Created by Macbook on 3/9/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public class ReceiverCellViewModel: TableViewModel {
    
    public var id: String
    public var cardLogo: String
    public var holder: String
    public var cardNumber: String
    public var editable: Bool
    
    public override var cellType: TableViewCell.Type {
        ReceiverCell.self
    }
    
    public override var estimatedRowHeight: CGFloat { 90 }
    
    public init(id: String, cardLogo: String, holder: String, cardNumber: String, editable: Bool = true) {
        self.id = id
        self.cardLogo = cardLogo
        self.holder = holder
        self.cardNumber = cardNumber
        self.editable = editable
        super.init(uid: nil)
    }
    
}
