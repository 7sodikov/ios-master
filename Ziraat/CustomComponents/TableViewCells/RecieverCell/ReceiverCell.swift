//
//  ReceiverCell.swift
//  MilliyCore
//
//  Created by Macbook on 3/9/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit
import Kingfisher

public class ReceiverCell: TableViewCell, ReceiverCellViewInstaller {
    
    var container: UIView!
    var logoView: UIImageView!
    var holderLabel: UILabel!
    var cardNumberLabel: UILabel!
    var stackView: UIStackView!
    var settingsButton: UIButton!
    var mainView: UIView { self.contentView }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        setupSubviews()
        setupTargets()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var cellHeight: CGFloat { 90 }
    
    func setupTargets() {
        settingsButton.addTarget(self, action: #selector(didTouchUpInsideSettings), for: .touchUpInside)
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        guard let viewModel = viewModel as? ReceiverCellViewModel else { return }
        holderLabel.text = viewModel.holder
        cardNumberLabel.text = viewModel.cardNumber
        logoView.kf.setImage(with: URL(string: viewModel.cardLogo), placeholder: UIImage(named: "img_conver_detail"))
        update(editable: viewModel.editable)
    }
    
    @objc private func didTouchUpInsideSettings() {
        swipeOffset.isZero ? showSwipe(orientation: .right) : hideSwipe(animated: true)
    }
    
    fileprivate func update(editable: Bool) {
//        delegate = editable ? delegate : nil
//        settingsButton.isHidden = !editable
    }
}
