//
//  CardsRouletteViewInstaller.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 25/05/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

protocol CardsRouletteViewInstaller: ViewInstaller {
    var headerLabel: UILabel! {get set}
    var indicator: UIActivityIndicatorView! {get set}
    var centeredFlowLayout: CenteredCollectionViewFlowLayout! {get set}
    var collectionView: UICollectionView! {get set}
    var collectionViewHeightAnchor: NSLayoutConstraint! {get set}
}


extension CardsRouletteViewInstaller {
    
    func initSubviews() {
        headerLabel = UILabel()
        headerLabel.font = UIFont(size: 18, weight: .medium)
        headerLabel.textColor = UIColor.buttonBlack
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.minimumScaleFactor = 0.5
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        
        centeredFlowLayout = CenteredCollectionViewFlowLayout()
        centeredFlowLayout.itemSize = .init(width: 255, height: 125)
        centeredFlowLayout.minimumLineSpacing = 16
        centeredFlowLayout.minimumInteritemSpacing = 16
        centeredFlowLayout.scrollDirection = .horizontal
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: centeredFlowLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.allowsSelection = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        indicator = UIActivityIndicatorView(style: .gray)
        indicator.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func embedSubviews() {
        mainView.addSubview(headerLabel)
        mainView.addSubview(collectionView)
        mainView.addSubview(indicator)
    }
    
    func addSubviewsConstraints() {
        let leading = (UIScreen.main.bounds.width - (centeredFlowLayout.itemSize.width + 32))
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 24),
            headerLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 16),
            headerLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -16),
            
            collectionView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 12),
            collectionView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: -leading),
            collectionView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -16),
            
            indicator.centerYAnchor.constraint(equalTo: mainView.centerYAnchor),
            indicator.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),
        ])
        
        collectionViewHeightAnchor = collectionView.heightAnchor.constraint(equalToConstant: 98)
        collectionViewHeightAnchor.isActive = true
    }
    
}
