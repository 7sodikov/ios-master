//
//  CardsRouletteModel.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 25/05/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

open class CardsRouletteModel: TableViewModel {
    
    public var header: String
//    public let status: CardCellStatus
    public let viewModels: [CollectionViewModel]
    
    public init(header: String, viewModels: [CollectionViewModel]) {
        self.header = header
        self.viewModels = viewModels
    }
    
    public var selectedCard: Int? {
        viewModels.firstIndex(where: \.isSelected)
    }
    
    public override var cellType: TableViewCell.Type {
        CardsRouletteCell.self
    }
    
    
}
