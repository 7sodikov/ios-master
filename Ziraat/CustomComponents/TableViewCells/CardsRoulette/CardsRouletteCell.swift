//
//  CardsRouletteCell.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 25/05/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public protocol CardsRouletteDelegate: AnyObject {
    func cardsRoulette(didSelectCardAt indexPath: IndexPath, card: Any)
}

open class CardsRouletteCell: TableViewCell, CardsRouletteViewInstaller {
    
    public weak var cardsDelegate: CardsRouletteDelegate?
    
    internal var headerLabel: UILabel!
    internal var indicator: UIActivityIndicatorView!
    internal var centeredFlowLayout: CenteredCollectionViewFlowLayout!
    internal var collectionView: UICollectionView!
    internal var mainView: UIView { contentView }
    internal var viewModels: [CollectionViewModel] = []
    internal var collectionViewHeightAnchor: NSLayoutConstraint!
    internal var viewModel: CardsRouletteModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            configure(with: viewModel)
        }
    }
    
    private var centeredRow: Int = 0
    private var centeredCell: UICollectionViewCell?
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupSubviews()
        setupTargets()
    }
    
    public required init?(coder: NSCoder) {
        fatalError()
    }
    
    internal func setupTargets() {
        collectionView.register(CollectionViewCell.self)
        collectionView.register(CardCell.self)
        collectionView.register(AccountCardCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func didUpdate(viewModel: TableViewModel) {
        self.viewModel = viewModel as? CardsRouletteModel
        

    }
    
    fileprivate func configure(with viewModel: CardsRouletteModel) {
        func selectCard() {
            guard !viewModels.isEmpty else { return }
            let indexPath = IndexPath(row: viewModel.selectedCard ?? 0, section: 0)
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
//            didSelect(viewModel: viewModels[indexPath.row], at: indexPath)
        }
        
        if let vm = viewModel.viewModels.first, vm is AccountCardCellViewModel {
            collectionViewHeightAnchor.constant = 180
        }
        
        headerLabel.text = viewModel.header
        viewModels = viewModel.viewModels
        collectionView.reloadData()
        selectCard()
    }
    
    fileprivate func didSelect(viewModel: CollectionViewModel, at indexPath: IndexPath) {
        if let account = (viewModel as? AccountCardCellViewModel)?.account {
            cardsDelegate?.cardsRoulette(didSelectCardAt: indexPath, card: account)
        } else if  let card = (viewModel as? CardCellViewModel)?.card {
            cardsDelegate?.cardsRoulette(didSelectCardAt: indexPath, card: card)
        }
    }
    
}

extension CardsRouletteCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModels.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = viewModels[indexPath.row]
        let cell = collectionView.reusable(viewModel.cellType, for: indexPath)
        cell.configure(viewModel: viewModel)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelect(viewModel: viewModels[indexPath.row], at: indexPath)
        if let currentCenteredPage = centeredFlowLayout.currentCenteredPage,
            currentCenteredPage != indexPath.row {
            centeredFlowLayout.scrollToPage(index: indexPath.row, animated: true)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        viewModels[indexPath.row].itemSize
    }
}
