//
//  EZSegmentedControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class EZSegmentedControl: UIControl, EZSegmentedControlViewInstaller {
    var mainView: UIView { self }
    var stackView: UIStackView!
    var text: String!
    
    var selectedIndex: Int = 0 {
        didSet {
            guard selectedIndex < stackView.arrangedSubviews.count else {
                return
            }
            
            for i in 0..<stackView.arrangedSubviews.count {
                let btn = stackView.arrangedSubviews[i] as? UIButton
                btn?.isSelected = (i == selectedIndex)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func addSegment(with title: String) {
        let btn = createSegmentButton(with: title)
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        btn.tag = stackView.arrangedSubviews.count
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButton(with title: String) -> UIButton {
        let comp = UIButton(frame: .zero)
        text = title
        comp.setTitle(text, for: .normal)
        comp.layer.cornerRadius = Adaptive.val(5)
        comp.clipsToBounds = true
        comp.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        comp.titleLabel?.font = .gothamNarrow(size: 16, .bold) //EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        comp.setTitleColor(.white, for: .normal)
        comp.setTitleColor(.black, for: .selected)
        comp.setBackgroundColor(ColorConstants.grayishWhite.withAlphaComponent(0.33), for: .normal)
        comp.setBackgroundColor(ColorConstants.grayishWhite.withAlphaComponent(0.33), for: .highlighted)
        comp.setBackgroundColor(.white, for: .selected)
        return comp
    }
    
    func addSegmentDarkVersion(with title: String) {
        let btn = createSegmentButtonDarkVersion(with: title)
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        btn.tag = stackView.arrangedSubviews.count
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButtonDarkVersion(with title: String) -> UIButton {
        let comp = UIButton(frame: .zero)
        comp.setTitle(title, for: .normal)
        comp.layer.cornerRadius = Adaptive.val(5)
        comp.clipsToBounds = true
        comp.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        comp.titleLabel?.font = .gothamNarrow(size: 16, .bold) //EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        comp.titleLabel?.numberOfLines = 0
        comp.setTitleColor(.white, for: .normal)
        comp.setTitleColor(.dashDarkBlue, for: .selected)
        comp.setBackgroundColor(ColorConstants.highlightedGray.withAlphaComponent(0.9), for: .normal)
        comp.setBackgroundColor(ColorConstants.highlightedGray.withAlphaComponent(0.9), for: .highlighted)
        comp.setBackgroundColor(.white, for: .selected)
        return comp
    }
    
    @objc private func btnClicked(_ sender: UIButton) {
        selectedIndex = sender.tag
        self.sendActions(for: .valueChanged)
    }
    
    
}
