//
//  EZSegmentedControlViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZSegmentedControlViewInstaller: ViewInstaller {
    var stackView: UIStackView! { get set }
}

extension EZSegmentedControlViewInstaller {
    func initSubviews() {
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Adaptive.val(9)
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
            maker.height.equalTo(Adaptive.val(41))
        }
    }
}

fileprivate struct Size {
    
}
