//
//  EZSegmentedControlLoginRecordsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZSegmentedControlLoginRecordsViewInstaller: ViewInstaller {
    var stackView: UIStackView! { get set }
}

extension EZSegmentedControlLoginRecordsViewInstaller {
    func initSubviews() {
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
            maker.height.equalTo(Adaptive.val(41))
        }
    }
}

fileprivate struct Size {
    
}
