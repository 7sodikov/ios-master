//
//  EZSegmentedControlLoginRecordsModel.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class EZSegmentedControlLoginRecordsModel {
    var nameLabel: String!
    
    init(name: String) {
        self.nameLabel = name
    }
}

