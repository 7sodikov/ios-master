//
//  EZSegmentedControlLoginRecords.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class EZSegmentedControlLoginRecords: UIControl, EZSegmentedControlLoginRecordsViewInstaller {
    var mainView: UIView { self }
    var stackView: UIStackView!
    var text: String!
    
    var selectedIndex: Int = 0 {
        didSet {
            guard selectedIndex < stackView.arrangedSubviews.count else {
                return
            }
            
            for i in 0..<stackView.arrangedSubviews.count {
                let btn = stackView.arrangedSubviews[i] as? UIButton
                btn?.isSelected = (i == selectedIndex)
                if btn?.isSelected == true {
                    btn?.layer.borderWidth = 0
                    btn?.layer.borderColor = UIColor.clear.cgColor
                } else {
                    btn?.layer.borderWidth = Adaptive.val(1)
                    btn?.layer.borderColor = UIColor.black.cgColor
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func addSegment(with title: String) {
        let btn = createSegmentButton(with: title)
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        btn.tag = stackView.arrangedSubviews.count
        if btn.tag == 0 {
            btn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        } else {
            btn.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        }
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButton(with title: String) -> UIButton {
        let comp = UIButton(frame: .zero)
        text = title
        comp.setTitle(text, for: .normal)
        comp.layer.cornerRadius = Adaptive.val(10)
        comp.clipsToBounds = true
        comp.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(10))
        comp.setTitleColor(.black, for: .normal)
        comp.setTitleColor(.white, for: .selected)
        comp.setBackgroundColor(.white, for: .normal)
        comp.setBackgroundColor(ColorConstants.grayishWhite.withAlphaComponent(0.33), for: .highlighted)
        comp.setBackgroundColor(ColorConstants.mainRed, for: .selected)
        return comp
    }
    
    func addSegmentDarkVersion(with title: String) {
        let btn = createSegmentButtonDarkVersion(with: title)
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        btn.tag = stackView.arrangedSubviews.count
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButtonDarkVersion(with title: String) -> UIButton {
        let comp = UIButton(frame: .zero)
        comp.setTitle(title, for: .normal)
        comp.layer.cornerRadius = Adaptive.val(10)
        comp.clipsToBounds = true
        comp.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        comp.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        comp.titleLabel?.numberOfLines = 0
        comp.setTitleColor(.white, for: .normal)
        comp.setTitleColor(.black, for: .selected)
        comp.setBackgroundColor(ColorConstants.highlightedGray.withAlphaComponent(0.9), for: .normal)
        comp.setBackgroundColor(ColorConstants.highlightedGray.withAlphaComponent(0.9), for: .highlighted)
        comp.setBackgroundColor(.white, for: .selected)
        return comp
    }
    
    @objc private func btnClicked(_ sender: UIButton) {
        selectedIndex = sender.tag
        self.sendActions(for: .valueChanged)
    }
    
    
}
