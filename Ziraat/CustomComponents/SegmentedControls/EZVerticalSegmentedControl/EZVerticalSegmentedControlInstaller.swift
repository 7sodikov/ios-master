//
//  EZVerticalSegmentedControlInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZVerticalSegmentedControlInstaller: ViewInstaller {
    var stackView: UIStackView! { get set }
}

extension EZVerticalSegmentedControlInstaller {
    func initSubviews() {
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Adaptive.val(16)
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
        
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct EZVerticalSegmentedControlSize {
    static let width = Adaptive.val(52)
}
