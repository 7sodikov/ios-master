//
//  EZVerticalSegmentedControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

class EZVerticalSegmentedControl: UIControl, EZVerticalSegmentedControlInstaller {
    var mainView: UIView { self }
    var stackView: UIStackView!
    
    var selectedIndex: Int = 0 {
        didSet {
            guard selectedIndex < stackView.arrangedSubviews.count else {
                return
            }
            
            for i in 0..<stackView.arrangedSubviews.count {
                let btn = stackView.arrangedSubviews[i] as? UIButton
                btn?.isSelected = (i == selectedIndex)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func addSegment(with title: String?, image: UIImage?) {
        let btn = createSegmentButton(with: title, image: image)
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        btn.tag = stackView.arrangedSubviews.count
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButton(with title: String?, image: UIImage?) -> UIButton {
        let comp = UIButton(frame: .zero)
        comp.setTitle(title, for: .normal)
        comp.layer.cornerRadius = Adaptive.val(17)
        comp.clipsToBounds = true
        comp.titleLabel?.font = .gothamNarrow(size: 12, .bold)//EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))
        comp.setImage(image, for: .normal)
        if #available(iOS 13.0, *) {
            comp.setImage(image?.withTintColor(.black), for: .selected)
        } else {
            comp.imageView?.tintColor = .black
        }
        comp.setTitleColor(.white, for: .normal)
        comp.setTitleColor(.black, for: .selected)
        comp.setBackgroundColor(UIColor.black.withAlphaComponent(0.4), for: .normal)
        comp.setBackgroundColor(ColorConstants.grayishWhite.withAlphaComponent(0.33), for: .highlighted)
        comp.setBackgroundColor(.white, for: .selected)
        return comp
    }
    
    @objc private func btnClicked(_ sender: UIButton) {
        selectedIndex = sender.tag
        self.sendActions(for: .valueChanged)
    }
}
