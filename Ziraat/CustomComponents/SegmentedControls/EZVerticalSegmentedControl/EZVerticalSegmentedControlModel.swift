//
//  EZVerticalSegmentedControlModel.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class EZVerticalSegmentedControlModel {
    var nameLabel: String!
    var imageImage: UIImage!
    
    init(name: String, image: UIImage) {
        self.nameLabel = name
        self.imageImage = image
    }
}
