//
//  LoginSegmentedControlViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol LoginSegmentedControlViewInstaller: ViewInstaller {
    var personalLabel: UILabel! { get set }
    var segmentedControlSeparator: UIView! { get set }
    var corporateLabel: UILabel! { get set }
    var containerStackView: UIStackView! { get set }
    var horizontalStackView: UIStackView! { get set }
}

extension LoginSegmentedControlViewInstaller {
    func initSubviews() {
        
        personalLabel = UILabel()
        personalLabel.font = EZFontType.medium.gothamText(size: Adaptive.val(16))
        personalLabel.textColor = .white
        personalLabel.text = RS.lbl_personal.localized()
        
        segmentedControlSeparator = UIView()
        segmentedControlSeparator.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        
        corporateLabel = UILabel()
        corporateLabel.font = EZFontType.medium.gothamText(size: Adaptive.val(16))
        corporateLabel.textColor = UIColor.white.withAlphaComponent(0.5)
        corporateLabel.text = RS.lbl_corporate.localized()
        
        containerStackView = UIStackView()
        containerStackView.alignment = .center
        containerStackView.axis = .vertical
        
        horizontalStackView = UIStackView()
        horizontalStackView.alignment = .fill
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = Adaptive.val(11)
        
    }
    
    func embedSubviews() {
        mainView.addSubview(containerStackView)
        containerStackView.addArrangedSubview(horizontalStackView)
        horizontalStackView.addArrangedSubview(personalLabel)
        horizontalStackView.addArrangedSubview(segmentedControlSeparator)
        horizontalStackView.addArrangedSubview(corporateLabel)
    }
    
    func addSubviewsConstraints() {
        containerStackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        segmentedControlSeparator.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(25))
            maker.width.equalTo(Adaptive.val(2))
        }
    }
}

