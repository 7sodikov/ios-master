//
//  LoginSegmentedControl.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class LoginSegmentedControl: UIControl, LoginSegmentedControlViewInstaller {
    var personalLabel: UILabel!
    var segmentedControlSeparator: UIView!
    var corporateLabel: UILabel!
    var containerStackView: UIStackView!
    var horizontalStackView: UIStackView!
    
    var mainView: UIView { self }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
}
