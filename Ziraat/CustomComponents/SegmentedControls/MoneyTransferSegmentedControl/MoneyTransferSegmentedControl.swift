//
//  MoneyTransferSegmentedControl.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol MoneyTransferSegmentedControlDelegate: class {
    func moneyTransferSegmentedControl(_ segmentedControl: MoneyTransferSegmentedControl, didSelectItemAt indexPath: IndexPath)
}

class MoneyTransferSegmentedControl: UIControl, MoneyTransferSegmentedControlViewInstaller {
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var mainView: UIView { self }
    
    weak var delegate: MoneyTransferSegmentedControlDelegate?
    
    var items: [String] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var selectedIndex: Int = 0 {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension MoneyTransferSegmentedControl: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MoneyTransferSegmentedControlCell.self), for: indexPath) as! MoneyTransferSegmentedControlCell
        let item = items[indexPath.row]
        cell.setup(for: item, isSelected: selectedIndex == indexPath.item)
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        (cell as? MoneyTransferSegmentedControlCell)?.becomeResponder()
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        delegate?.moneyTransferSegmentedControl(self, didSelectItemAt: indexPath)
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return MoneyTransferSegmentedControlSize.Cell.size
    }
    
//    https://stackoverflow.com/a/37065909/845345
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = MoneyTransferSegmentedControlSize.Cell.size.width * CGFloat(items.count)
        let totalSpacingWidth = 0 * CGFloat(items.count - 1)
        
        let leftInset = (self.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
}
