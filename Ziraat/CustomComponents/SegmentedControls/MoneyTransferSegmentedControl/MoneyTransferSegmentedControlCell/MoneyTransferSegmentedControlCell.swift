//
//  MoneyTransferSegmentedControlCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class MoneyTransferSegmentedControlCell: UICollectionViewCell, MoneyTransferSegmentedControlCellViewInstaller {
    var mainView: UIView { contentView }
    var segmentTitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(for title: String, isSelected: Bool) {
        mainView.backgroundColor = isSelected ? .white : UIColor(214, 214, 215)
        mainView.layer.borderWidth = 1
        let color = isSelected ? ColorConstants.mainRed : ColorConstants.lightestGray
        mainView.layer.borderColor = color.cgColor
        segmentTitleLabel.text = title
    }
}
