//
//  MoneyTransferSegmentedControlCellViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol MoneyTransferSegmentedControlCellViewInstaller: ViewInstaller {
    var segmentTitleLabel: UILabel! { get set }
}

extension MoneyTransferSegmentedControlCellViewInstaller {
    func initSubviews() {
        mainView.layer.cornerRadius = MoneyTransferSegmentedControlSize.Cell.size.height/2
        mainView.layer.masksToBounds = true
        
        segmentTitleLabel = UILabel()
        segmentTitleLabel.backgroundColor = .clear
        segmentTitleLabel.textColor = .black
        segmentTitleLabel.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(12))
        segmentTitleLabel.textAlignment = .center
    }
    
    func embedSubviews() {
        mainView.addSubview(segmentTitleLabel)
    }
    
    func addSubviewsConstraints() {
        segmentTitleLabel.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
//            maker.center.equalToSuperview()
//            maker.width.height.equalTo(MoneyTransferSegmentedControlSize.Cell.size.height * 0.8)
        }
    }
}
