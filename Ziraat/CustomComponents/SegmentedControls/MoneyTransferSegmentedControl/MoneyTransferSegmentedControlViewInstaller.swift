//
//  MoneyTransferSegmentedControlViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol MoneyTransferSegmentedControlViewInstaller: ViewInstaller {
//    var lineStackView: UIStackView! { get set }
    var layout: UICollectionViewFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
}

extension MoneyTransferSegmentedControlViewInstaller {
    func initSubviews() {
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = .zero
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.bounces = true;
        collectionView.alwaysBounceHorizontal = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(MoneyTransferSegmentedControlCell.self, forCellWithReuseIdentifier: String(describing: MoneyTransferSegmentedControlCell.self))
    }
    
    func embedSubviews() {
        mainView.addSubview(collectionView)
    }
    
    func addSubviewsConstraints() {
        collectionView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct MoneyTransferSegmentedControlSize {
    struct Cell {
        static let size = CGSize(width: UIScreen.main.bounds.width * 0.307, height: Adaptive.val(30))
    }
}
