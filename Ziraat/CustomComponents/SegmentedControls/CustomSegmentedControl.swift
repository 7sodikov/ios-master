//
//  CustomSegmentedControl.swift
//  MilliyCore
//
//  Created by Macbook on 4/19/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public protocol CustomSegmentedControlDelegate: AnyObject {
    func changeToIndex(index: Int)
}

public class CustomSegmentedControl: UIView {
    
    private var buttons: [UIButton]!
    private var selectorView: UIView!
    private var diminsions: [CGFloat]!
    
    public private(set) var selectedIndex: Int = 0
    
    public var items: [String] = ["segment one", "segment two"] {
        didSet {
            updateViews()
        }
    }
    
    public weak var delegate: CustomSegmentedControlDelegate?
    
    public var textColor: UIColor = UIColor(137, 142, 148)
    public var selectedViewColor: UIColor =  UIColor(225, 5, 20)
    public var selectedTextColor: UIColor = UIColor(68, 80, 86)
    public var buttonTitleFont: UIFont = .gothamNarrow(size: 13, .bold)
    public var buttonsSpacing: CGFloat = 32
    
    public init(selectedIndex index: Int = 0) {
        self.selectedIndex = index
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        
        updateViews()
    }
    
    private func updateViews() {
        
        let itemsWidth = items.map { widthString(text: $0) }.reduce(0, +)
        buttonsSpacing = (UIScreen.main.bounds.width - 32 - itemsWidth) / CGFloat(items.count)
        createButton()
        setupStackView()
        setupSelectorView()
    }
    
    private func setupStackView() {
        let stackView = UIStackView()
        buttons.forEach { stackView.addArrangedSubview($0) }
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = buttonsSpacing
        stackView.alignment = .center
        
        addSubview(stackView)
        stackView.constraint { $0.centerY.centerX.equalToSuperView() }
    }
    
    private func setupSelectorView() {
        diminsions = items.map { widthString(text: $0) + buttonsSpacing }
        
        
        let backSelectorView = UIView(frame: CGRect(x: 0, y: frame.height - 1, width: frame.width, height: 1))
        backSelectorView.backgroundColor = UIColor.blueGrey.withAlphaComponent(0.15)
        addSubview(backSelectorView)
        
        selectorView = UIView(frame: CGRect(x: 0, y: frame.height - 2, width: diminsions[0] + 8, height: 2))
        selectorView.backgroundColor = selectedViewColor
        addSubview(selectorView)
    }
    
    private func createButton() {
        subviews.forEach { $0.removeFromSuperview() }
        buttons = items.map { (title) -> UIButton in
            let button = UIButton(type: .system)
            button.titleLabel?.font = buttonTitleFont
            button.setTitle(title, for: .normal)
            button.titleLabel?.numberOfLines = 2
            button.setTitleColor(textColor, for: .normal)
            button.addTarget(self, action: #selector(buttonSelected(_:)), for: .touchUpInside)
            return button
        }
        buttons[selectedIndex].setTitleColor(selectedTextColor, for: .normal)
    }
    
    @objc private func buttonSelected(_ sender: UIButton) {
        for (index, button) in buttons.enumerated() {
            button.setTitleColor(textColor, for: .normal)
            if button == sender {
                DispatchQueue.main.async {
                    self.selectedIndex = index
                    self.delegate?.changeToIndex(index: index)
                    self.animate(index: index)
                    button.setTitleColor(self.selectedTextColor, for: .normal)
                }
            }
        }
    }
    
    public func setIndex(index: Int) {
        buttonSelected(buttons[index])
    }
    
    private func animate(index: Int) {
        let selectedPosition = self.diminsions[0..<index].reduce(16, +)
        UIView.animate(withDuration: 0.3) {
            self.selectorView.frame.size.width = self.diminsions[index]
            self.selectorView.frame.origin.x = selectedPosition
            
        }
        
    }
    
    private func widthString(text: String) -> CGFloat {
        let boundingBox = text.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: self.frame.height),
                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                            attributes: [NSAttributedString.Key.font: buttonTitleFont],
                                            context: nil)
        return boundingBox.width
    }
}
