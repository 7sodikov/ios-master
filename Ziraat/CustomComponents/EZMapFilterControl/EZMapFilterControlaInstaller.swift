//
//  EZMapFilterControlaInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZMapFilterControlaInstaller: ViewInstaller {
    var stackView: UIStackView! { get set }
}

extension EZMapFilterControlaInstaller {
    func initSubviews() {
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
            maker.height.equalTo(Adaptive.val(50))
        }
    }
}

fileprivate struct Size {
    
}
