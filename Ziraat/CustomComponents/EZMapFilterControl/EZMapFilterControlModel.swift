//
//  EZMapFilterControlModel.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class EZMapFilterControlModel {
    var nameLabel: String!
    
    init(name: String) {
        self.nameLabel = name
    }
}
