//
//  EZMapFilterControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 4/14/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol EZMapFilterControlDelegate: class {
    func filterSelected(at language: Language)
}

class EZMapFilterControl: UIControl, EZMapFilterControlaInstaller {
    var mainView: UIView { self }
    var stackView: UIStackView!
    var text: String!
    
    weak var delegate: EZMapFilterControlDelegate?
    
    var selectedIndex: Int = 0 {
        didSet {
            guard selectedIndex < stackView.arrangedSubviews.count else {
                return
            }
            
            for i in 0..<stackView.arrangedSubviews.count {
                let btn = stackView.arrangedSubviews[i] as? UIButton
                btn?.isSelected = (i == selectedIndex)
                if btn?.isSelected == true {
//                    btn?.layer.borderWidth = 0
//                    btn?.layer.borderColor = UIColor.clear.cgColor
                } else {
//                    btn?.layer.borderWidth = Adaptive.val(1)
//                    btn?.layer.borderColor = UIColor.dashDarkBlue.cgColor
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func addSegment(with title: String) {
        let btn = createSegmentButton(with: title)
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        btn.tag = stackView.arrangedSubviews.count
        if btn.tag == 0 {
            btn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            btn.addRightBorder(ColorConstants.hintGray, width: Adaptive.val(1))
        } else  if btn.tag == 1 {
            btn.layer.maskedCorners = [.init(), .init()]
            btn.addRightBorder(ColorConstants.hintGray, width: Adaptive.val(1))
        } else {
            btn.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            
        }
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButton(with title: String) -> UIButton {
        let comp = UIButton(frame: .zero)
        text = title
        comp.setTitle(text, for: .normal)
        comp.layer.cornerRadius = Adaptive.val(10)
        comp.clipsToBounds = true
        comp.titleLabel?.font = .gothamNarrow(size: 16, .medium)
        comp.setTitleColor(.dashDarkBlue, for: .normal)
        comp.setTitleColor(.white, for: .selected)
        comp.setBackgroundColor(.white, for: .normal)
        comp.setBackgroundColor(ColorConstants.grayishWhite.withAlphaComponent(0.33), for: .highlighted)
        comp.setBackgroundColor(ColorConstants.mainRed, for: .selected)
        return comp
    }
       
    @objc private func btnClicked(_ sender: UIButton) {
        selectedIndex = sender.tag
        self.sendActions(for: .valueChanged)
    }
}
