//
//  PaymentDataEntryFormFieldViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

protocol PaymentDataEntryFormFieldViewInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var textField: LoginTextField! { get set }
    var indicator: MDCActivityIndicator! { get set }
    var errorLabel: UILabel! { get set }
}

extension PaymentDataEntryFormFieldViewInstaller {
    func initSubviews() {
        titleLabel = UILabel()
        
        textField = LoginTextField(with: "", cornerRadius: Adaptive.val(6), isSecureTextEntry: false)
        textField.layer.borderWidth = Adaptive.val(2)
        textField.layer.borderColor = UIColor.darkGrey.cgColor
        textField.textColor = .dashDarkBlue
        
        indicator = MDCActivityIndicator(frame: CGRect(x: 0, y: 0, width: LoanCalculatorTableCellSize.TextField.height, height: LoanCalculatorTableCellSize.TextField.height/2))
        indicator.cycleColors = [.lightGray]
        indicator.tintColor = .lightGray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.radius = (LoanCalculatorTableCellSize.TextField.height/2) * 0.45
        
        errorLabel = UILabel()
        errorLabel.text = RS.lbl_fill_blank_field.localized()
        errorLabel.isHidden = true
        errorLabel.textColor = .red
        errorLabel.font = .gothamNarrow(size: 12, .book)
    }
    
    func embedSubviews() {
        (mainView as? UIStackView)?.addArrangedSubview(titleLabel)
        (mainView as? UIStackView)?.addArrangedSubview(textField)
        (mainView as? UIStackView)?.addArrangedSubview(errorLabel)
    }
    
    func addSubviewsConstraints() {
        textField.snp.remakeConstraints { (maker) in
            maker.height.equalTo(PaymentDataEntryFormFieldSize.TextField.height)
//            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(14))
        }
        mainView.constraint { make in
            make.height.lessThanOrEqualToConstant(84)
        }
    }
}

//PaymentDataEntryFormMoneyFieldSize
struct PaymentDataEntryFormFieldSize {
    struct TextField {
        static let height = Adaptive.val(50)
    }
}
