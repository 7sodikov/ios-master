//
//  PaymentDataEntryFormFieldDelegate.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/19/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PaymentDataEntryFormFieldDelegate: class {
    func myCardFieldDidClick()
    func moneyFieldDidChange(with value: String?, field: ServiceFieldResponse)
    func datePopupFieldDidClick(field: ServiceFieldResponse)
    func regexBoxFieldDidChange(with value: String?, field: ServiceFieldResponse)
    func phoneFieldDidChange(with value: String?, field: ServiceFieldResponse)
    func stringFieldDidChange(with value: String?, field: ServiceFieldResponse)
}
