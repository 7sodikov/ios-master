//
//  PaymentDataEntryFormField.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

class PaymentDataEntryFormField: UIStackView, PaymentDataEntryFormFieldViewInstaller {
    var mainView: UIView { self }
    var titleLabel: UILabel!
    var textField: LoginTextField!
    var indicator: MDCActivityIndicator!
    var errorLabel: UILabel!
    
    weak var delegate: PaymentDataEntryFormFieldDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        axis = .vertical
        alignment = .fill
        distribution = .fill
        spacing = Adaptive.val(14)
    }
    
    func setPreloader() {
        textField.rightView = indicator
        textField.rightViewMode = .unlessEditing
    }
    
    var animateIndicator: Bool {
        get {
            indicator.isAnimating
        }
        set {
            if newValue {
                indicator.startAnimating()
            } else {
                indicator.stopAnimating()
            }
        }
    }
    
    func setup(title: String, value: String, isRequired: Bool = false) {
        set(title: title, with: isRequired)
        textField.placeholder = value
    }
    
    func set(title: String, with requiredAsterisk: Bool = false) {
        let titleAttrString = NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor: UIColor.dashDarkBlue,
                                                                             NSAttributedString.Key.font: UIFont.gothamNarrow(size: 18, .book)])
        let asteriskAttrString = NSAttributedString(string: "*", attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.mainRed,
                                                                              NSAttributedString.Key.font: EZFontType.regular.sfProText(size: 14)!])
        let finalAttrString = NSMutableAttributedString()
        finalAttrString.append(titleAttrString)
        if requiredAsterisk {
            finalAttrString.append(asteriskAttrString)
        }
        
        titleLabel.attributedText = finalAttrString
    }
    
}
