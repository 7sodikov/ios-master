//
//  BaseRouter.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class BaseRouter {
    
    private unowned var controller: UIViewController
    
    private var temporaryViewController: UIViewController?
    
    init(viewController: UIViewController) {
        controller = viewController
        temporaryViewController = viewController
    }
    
    var viewController: UIViewController {
        defer {
            temporaryViewController = nil
        }
        return controller
    }
    
    var navigationController: UINavigationController? {
        viewController.navigationController
    }
    
    func push(_ router: BaseRouter, animated: Bool) {
        push(router.viewController, animated: animated)
    }
    
    func push(_ viewController: UIViewController, animated: Bool) {
        navigationController?.pushViewController(viewController, animated: animated)
    }
    
    func backTo(nScreeen: Int) {
        let controllers = navigationController?.viewControllers ?? []
        if controllers.count > nScreeen  {
            navigationController?.popToViewController(controllers[controllers.count - nScreeen], animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
}
