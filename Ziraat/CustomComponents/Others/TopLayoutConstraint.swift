//
//  SafeAreaLayoutConstraint.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

open class TopLayoutConstraint: NSLayoutConstraint {
    
    public var offset: CGFloat = 0
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        if #available(iOS 11.0, *) {
             self.constant -= 64
        }
    }
}
