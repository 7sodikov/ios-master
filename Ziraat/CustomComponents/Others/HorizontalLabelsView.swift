//// Header

import UIKit

class HorizontalLabelsView: BaseView {
    let leftLabel = UILabel()
    let rightLabel = UILabel()
    private let stackView = UIStackView()
    
    override func initSubviews() {
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = Adaptive.val(4)
        
        leftLabel.textColor = .black
        leftLabel.textAlignment = .left
        leftLabel.numberOfLines = 5
        leftLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(14))
        
        rightLabel.textColor = .timerGray
        rightLabel.textAlignment = .right
        rightLabel.numberOfLines = 5
        rightLabel.font = .gothamNarrow(size: 14, .medium)//EZFontType.regular.sfProDisplay(size: Adaptive.val(14))
        
    }
    
    override func embedSubviews() {
        stackView.addArrangedSubview(leftLabel)
        stackView.addArrangedSubview(rightLabel)
        self.addSubview(stackView)
    }
    override func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (make) in
            make.left.right.bottom.top.bottom.equalToSuperview()
        }
    }
    func changeAligment(_ left:Bool){
        stackView.spacing = 5
        if left{
            self.stackView.distribution = .fill
            self.leftLabel.setContentHuggingPriority(UILayoutPriority.init(1000), for: .horizontal)
            self.rightLabel.textAlignment = .left
        } else{
            self.stackView.distribution = .fill
            self.rightLabel.setContentHuggingPriority(UILayoutPriority.init(1000), for: .horizontal)
        }
    }
    
}
