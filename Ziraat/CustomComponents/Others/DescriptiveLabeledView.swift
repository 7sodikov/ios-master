//
//  DescriptiveLabeledView.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/24/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class DescriptiveLabeledView: UIView {
    var titleLabel = UILabel()
    var valueLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel.font = .systemFont(ofSize: 12, weight: .light)
        valueLabel.font = .systemFont(ofSize: 13, weight: .semibold)
        
        addSubview(titleLabel)
        addSubview(valueLabel)
    
        titleLabel.constraint { (make) in
            make.vertical.leading.equalToSuperView()
        }
        valueLabel.constraint { (make) in
            make.leading(self.titleLabel.trailingAnchor, offset: 4)
                .vertical.equalToSuperView()
        }
        valueLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(title: String, value: String) {
        titleLabel.text = title
        valueLabel.text = value
    }
}
