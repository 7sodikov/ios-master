//
//  PaddingLabel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 6/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class PaddingLabel: UILabel {

    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat

    required init(withInsets padding: UIEdgeInsets) {
        self.topInset = padding.top
        self.bottomInset = padding.bottom
        self.leftInset = padding.left
        self.rightInset = padding.right
        super.init(frame: CGRect.zero)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }

    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
