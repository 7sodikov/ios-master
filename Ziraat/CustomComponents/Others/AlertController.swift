//
//  AlertController.swift
//  Ziraat
//
//  Created by Shamsiddin on 1/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class AlertController: UIAlertController {
    private var countDownTimer: Timer?
    private var okClickAction: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @objc private func countdown(dt: Timer) {
        stopTimer()
        dismiss(animated: true, completion: nil)
        okClickAction?()
    }
    
    private func stopTimer() {
        countDownTimer?.invalidate()
        countDownTimer = nil
    }
    
    func showAutoDismissAlert(in controller: UIViewController, okClick: (() -> Void)? = nil) {
        okClickAction = okClick
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            self.stopTimer()
            okClick?()
        })
        addAction(okAction)
        
        countDownTimer = Timer.scheduledTimer(timeInterval: 3.0,
                                              target: self,
                                              selector: #selector(countdown(dt:)),
                                              userInfo: nil,
                                              repeats: false)
        
        controller.present(self, animated: true, completion: nil)
    }
}
