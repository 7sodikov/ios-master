//
//  Obfuscator.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/11/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//
// README:
// http://www.splinter.com.au/2014/09/16/storing-secret-keys/
// https://medium.com/swift2go/increase-the-security-of-your-ios-app-by-obfuscating-sensitive-strings-swift-c915896711e6
// https://gist.github.com/DejanEnspyra/80e259e3c9adf5e46632631b49cd1007
//

import Foundation

//class Obfuscator {
//
//    // MARK: - Variables
//
//    /// The salt used to obfuscate and reveal the string.
//    private var salt: String
//
//    // MARK: - Initialization
//
//    init() {
//        self.salt = "\(String(describing: AppDelegate.self))\(String(describing: NSObject.self))"
//    }
//
//    init(with salt: String) {
//        self.salt = salt
//    }
//
//
//    // MARK: - Instance Methods
//
//    /**
//     This method obfuscates the string passed in using the salt
//     that was used when the Obfuscator was initialized.
//
//     - parameter string: the string to obfuscate
//
//     - returns: the obfuscated string in a byte array
//     */
//    func bytesByObfuscatingString(string: String) -> [UInt8] {
//        let text = [UInt8](string.utf8)
//        let cipher = [UInt8](self.salt.utf8)
//        let length = cipher.count
//
//        var encrypted = [UInt8]()
//
//        for t in text.enumerated() {
//            encrypted.append(t.element ^ cipher[t.offset % length])
//        }
//
//        #if DEVELOPMENT
//        print("Salt used: \(self.salt)\n")
//        print("Swift Code:\n************")
//        print("// Original \"\(string)\"")
//        print("let key: [UInt8] = \(encrypted)\n")
//        #endif
//
//        return encrypted
//    }
//
//    /**
//     This method reveals the original string from the obfuscated
//     byte array passed in. The salt must be the same as the one
//     used to encrypt it in the first place.
//
//     - parameter key: the byte array to reveal
//
//     - returns: the original string
//     */
//    func reveal(key: [UInt8]) -> String {
//        let cipher = [UInt8](self.salt.utf8)
//        let length = cipher.count
//
//        var decrypted = [UInt8]()
//
//        for k in key.enumerated() {
//            decrypted.append(k.element ^ cipher[k.offset % length])
//        }
//
//        return String(bytes: decrypted, encoding: .utf8)!
//    }
//}

class Obfuscator {
    
    // MARK: - Variables
    
    /// The salt used to obfuscate and reveal the string.
    private var salt: String = ""
    
    
    // MARK: - Initialization
    
    init(withSalt salt: [AnyObject]) {
        self.salt = salt.description
    }
    
    
    // MARK: - Instance Methods
    
    /**
     This method obfuscates the string passed in using the salt
     that was used when the Obfuscator was initialized.
     
     - parameter string: the string to obfuscate
     
     - returns: the obfuscated string in a byte array
     */
    func bytesByObfuscatingString(string: String) -> [UInt8] {
        let text = [UInt8](string.utf8)
        let cipher = [UInt8](self.salt.utf8)
        let length = cipher.count
        
        var encrypted = [UInt8]()
        
        for t in text.enumerated() {
            encrypted.append(t.element ^ cipher[t.offset % length])
        }
        
        #if DEVELOPMENT
        print("Salt used: \(self.salt)\n")
        print("Swift Code:\n************")
        print("// Original \"\(string)\"")
        print("let key: [UInt8] = \(encrypted)\n")
        #endif
    
        return encrypted
    }
    
    /**
     This method reveals the original string from the obfuscated
     byte array passed in. The salt must be the same as the one
     used to encrypt it in the first place.
     
     - parameter key: the byte array to reveal
     
     - returns: the original string
     */
    func reveal(key: [UInt8]) -> String {
        let cipher = [UInt8](self.salt.utf8)
        let length = cipher.count
        
        var decrypted = [UInt8]()
        
        for k in key.enumerated() {
            decrypted.append(k.element ^ cipher[k.offset % length])
        }
        
        return String(bytes: decrypted, encoding: .utf8)!
    }
}

// Additional Project specific helper extension
extension Obfuscator {
    
    // CAUTION! Don't change the salt array items!
    static let o = Obfuscator(withSalt: [NSSortDescriptor.self, UIView.self, NSObject.self, UIDatePicker.self, NumberFormatter.self, NSPredicate.self])
    
//    static func obfuscate() {
//        let obfuscatedStr = o.bytesByObfuscatingString(string: "userName")
//        print(obfuscatedStr)
//    }
}

extension Sequence where Iterator.Element == UInt8 {
    var reveal: String {
        return Obfuscator.o.reveal(key: self as! [UInt8])
    }
}
