//
//  AutomaticHeightTableView.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/29/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class AutomaticHeightTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height + 20)
    }
}
