//
//  TimerView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class TimerView: UIView {
    
    var timerReachedItsEnd: (() -> Void)?
    
    var titleColor: UIColor = .white {
        didSet {
            timerLabel.textColor = titleColor
            timerExpiredLabel.textColor = titleColor
            timerExpiredIcon.image = timerExpiredImage?.changeColor(titleColor)
        }
    }
    
    var shapeColor: UIColor = .white {
        didSet {
            shapeLayer.strokeColor = shapeColor.cgColor
        }
    }
    
    var backShapeColor: UIColor = .white {
        didSet {
            backShapeLayer.strokeColor = backShapeColor.cgColor
        }
    }
    
    private lazy var backShapeLayer: CAShapeLayer = {
        let backLayer = CAShapeLayer()
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.width/2, y: frame.height/2),
                                      radius: (frame.width - Size.circleWidth)/2,
                                      startAngle: CGFloat(-Double.pi/2),
                                      endAngle:CGFloat(2*Double.pi-Double.pi/2),
                                      clockwise: true)
        
        backLayer.path = circlePath.cgPath
        backLayer.fillColor = UIColor.clear.cgColor
        backLayer.strokeColor = UIColor.white.cgColor
        backLayer.lineWidth = Size.circleWidth
        
        return backLayer
    }()
    
    private lazy var shapeLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.width/2, y: frame.height/2),
                                      radius: (frame.width - Size.circleWidth)/2,
                                      startAngle: CGFloat(-Double.pi/2),
                                      endAngle:CGFloat(2*Double.pi-Double.pi/2),
                                      clockwise: true)
        
        layer.path = circlePath.cgPath
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = ColorConstants.gray.cgColor
        layer.lineWidth = Size.circleWidth
        layer.lineCap = CAShapeLayerLineCap.round
        
        return layer
    }()
    
    private lazy var timerLabel: UILabel = {
        let lbl = UILabel(frame: CGRect(x: Size.labelPadding,
                                        y: Size.labelPadding,
                                        width: frame.width - (2 * Size.labelPadding),
                                        height: frame.width - (2 * Size.labelPadding)))
        lbl.textAlignment = .center
        lbl.font = EZFontType.regular.sfuiDisplay(size: Adaptive.val(48))
        lbl.textColor = .white
        return lbl
    }()
    
    private lazy var timerExpiredLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = .gothamNarrow(size: 16, .medium)//EZFontType.bold.sfuiDisplay(size: Adaptive.val(16))
        lbl.textColor = titleColor
        lbl.text = RS.lbl_time_expired.localized()
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private lazy var timerExpiredIcon: UIImageView = {
        let imgView = UIImageView()
        imgView.isHidden = true
        imgView.image = timerExpiredImage?.changeColor(titleColor)
        return imgView
    }()
    
    private let timerExpiredImage = UIImage(named: "icon_hourglass")
    
    private var countDownTimer = Timer()
    private var timerValue = 900
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.addSublayer(backShapeLayer)
        layer.addSublayer(shapeLayer)
        addSubview(timerLabel)
        addSubview(timerExpiredLabel)
        addSubview(timerExpiredIcon)
        
        timerExpiredIcon.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(40))
            maker.centerX.equalTo(timerExpiredLabel)
            maker.width.equalTo(Adaptive.val(41))
            maker.height.equalTo(Adaptive.val(40))
        }
        
        timerExpiredLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(timerExpiredIcon.snp.bottom).offset(Adaptive.val(16))
            maker.left.equalToSuperview().offset(Size.expLabelPadding)
            maker.right.equalToSuperview().offset(-Size.expLabelPadding)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func startAnimation() {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = Double(timerValue)
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        shapeLayer.add(animation, forKey: "ani")
    }
    
    private func timeFormatted(totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        // let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    private func timerReachedItsEndFunc() {
        countDownTimer.invalidate()
        timerLabel.isHidden = true
        timerExpiredLabel.isHidden = false
        timerExpiredIcon.isHidden = false
        timerReachedItsEnd?()
    }
    
    // Needs @objc to be able to call private function in NSTimer.
    @objc private func countdown(dt: Timer) {
        timerValue -= 1
        if timerValue == 0 {
            timerReachedItsEndFunc()
        } else if timerValue <= 10 && timerValue > 0 {
            timerLabel.textColor = titleColor
            timerLabel.text = timeFormatted(totalSeconds: timerValue)
        } else {
            timerLabel.textColor = titleColor
            timerLabel.text = timeFormatted(totalSeconds: timerValue)
        }
    }
    
    func startTimer(with value: Int) {
        timerValue = value
        timerLabel.text = timeFormatted(totalSeconds: value)
        timerLabel.isHidden = false
        timerExpiredLabel.isHidden = true
        timerExpiredIcon.isHidden = true
        
        countDownTimer.invalidate()
        countDownTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                              target: self,
                                              selector: #selector(countdown(dt:)),
                                              userInfo: nil,
                                              repeats: true)
        startAnimation()
    }
    
    func stopTimer() {
        countDownTimer.invalidate()
        shapeLayer.removeAllAnimations()
    }
}

fileprivate struct Size {
    static let circleWidth = Adaptive.val(8)
    static let labelPadding = Adaptive.val(8)
    static let expLabelPadding = Adaptive.val(20)
}

//let view = ClockView()
//view.backgroundColor = UIColor.whiteColor()
//view.frame = CGRect(x: 0, y: 0, width: 320, height: 480)
//
//view.startTimer(with: 1)
//
//XCPlaygroundPage.currentPage.liveView = view
