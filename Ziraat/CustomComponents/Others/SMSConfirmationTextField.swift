//
//  SMSConfirmationTextField.swift
//  Ziraat
//
//  Created by Shamsiddin on 12/26/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class SMSConfirmationTextField: LoginTextField {
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    required init(with placeholder: String = "XXXXXX",
                  keyboard: UIKeyboardType = .default,
                  cornerRadius: CGFloat,
                  isSecureTextEntry: Bool = true) {
        super.init(with: placeholder, keyboard: keyboard, cornerRadius: cornerRadius, isSecureTextEntry: isSecureTextEntry)
        self.delegate = self
        self.textContentType = .oneTimeCode
    }
}

extension SMSConfirmationTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)
        return newString.count <= maxLength
    }
}
