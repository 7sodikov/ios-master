//
//  AutomaticHeightCollectionView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/20/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class AutomaticHeightCollectionView: UICollectionView {
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height + 20)
    }
}
