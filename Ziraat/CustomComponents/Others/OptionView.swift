//
//  OptionView.swift
//  Ziraat
//
//  Created by Jasur Amirov on 7/6/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class OptionView: UIView {
    var stackView: UIStackView = {
       let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 12
        return stackView
    }()
    
    var headerLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.gothamNarrow(size: 16, .book)
        label.textColor = UIColor(68, 80, 86)
        return label
    }()
    
    var contaner: UIView = {
       let view = UIView()
        view.layer.cornerRadius = 6
        view.backgroundColor = .white
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor(145, 150, 155).cgColor
        return view
    }()
    
    var textLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.gothamNarrow(size: 15, .book)
        label.textColor = .black
        return label
    }()
    
    var iconView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "img_disclosure")
        imageView.transform = CGAffineTransform(rotationAngle: .pi/2)
        return imageView
    }()
    
    var didTap: ((Int) -> Void)? = nil
    
    init() {
        super.init(frame: .zero)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onViewTapped)))
        
        addSubview(stackView)
        stackView.addArrangedSubview(headerLabel)
        stackView.addArrangedSubview(contaner)
        contaner.addSubview(textLabel)
        contaner.addSubview(iconView)
        
        stackView.constraint { maker in
            maker.vertical.horizontal.equalToSuperView()
        }
        iconView.constraint { make in
            make.square(24).trailing(-16).centerY.equalToSuperView()
        }
        contaner.constraint { make in
            make.height(50)
        }
        textLabel.constraint { make in
            make.trailing(self.iconView.leadingAnchor, offset: -8)
                .leading(16).centerY.equalToSuperView()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func onViewTapped() {
        didTap?(self.tag)
    }
}
