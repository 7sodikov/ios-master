//
//  NavigationController.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, UIGestureRecognizerDelegate {
    
    fileprivate let navTitleFont = EZFontType.medium.sfuiDisplay(size: Adaptive.val(21))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // navigationBar.barTintColor = .clear
        navigationBar.tintColor = .black
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.backgroundColor = .clear
        navigationBar.isTranslucent = true
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navTitleFont,
                                             NSAttributedString.Key.foregroundColor: UIColor.black]
        
        /// Custom back buttons disable the interactive pop animation
        /// To enable it back we set the recognizer to `self`
        interactivePopGestureRecognizer?.delegate = self
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {

        if let topVC = viewControllers.last {
            //return the status property of each VC, look at step 2
            return topVC.preferredStatusBarStyle
        }

        return .default
    }
    
    open override var childForStatusBarStyle: UIViewController? {
        return topViewController?.childForStatusBarStyle ?? topViewController
    }
    
    func background(isWhite: Bool) {
        if isWhite {
            navigationBar.tintColor = .white
            navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navTitleFont,
                                                 NSAttributedString.Key.foregroundColor: UIColor.white]
        } else {
            navigationBar.tintColor = .black
            navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navTitleFont,
                                                 NSAttributedString.Key.foregroundColor: UIColor.timerGray]
        }
    }
    
    func background(isTinted: Bool) {
        if isTinted {
            navigationBar.tintColor = nil
            navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navTitleFont,
                                                 NSAttributedString.Key.foregroundColor: UIColor.white]
        } else {
            navigationBar.tintColor = .black
            navigationBar.barTintColor = .green
            navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navTitleFont,
                                                 NSAttributedString.Key.foregroundColor: UIColor.black]
        }
    }
    
//    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        if let vc = viewControllers.last {
//            if vc is OfflineMainViewController {
//                return false
//            }
//        }
//        return viewControllers.count > 1
//    }
}
