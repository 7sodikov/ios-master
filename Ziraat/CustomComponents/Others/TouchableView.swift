//
//  TouchableView.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

enum TouchableViewState {
    case began
    case cancelled
    case ended
}
