//
//  TextField.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/15/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class TextField: UITextField {
    
    func addLeftSide(view: UIView, by insert: UIEdgeInsets = .zero) {
        leftView = insert == .zero ? view : newParentView(to: view, by: insert)
        leftViewMode = .always
    }
    
    func addRightSide(view: UIView, by insert: UIEdgeInsets = .zero) {
        rightView = insert == .zero ? view : newParentView(to: view, by: insert)
        rightViewMode = .always
    }
    
    private func newParentView(to childView: UIView, by insert: UIEdgeInsets) -> UIView {
        let view = UIView()
        view.addSubview(childView)
        childView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            childView.topAnchor.constraint(equalTo: view.topAnchor, constant: insert.top),
            childView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -insert.bottom),
            childView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: insert.left),
            childView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -insert.right)
        ])
        return view
    }
}
