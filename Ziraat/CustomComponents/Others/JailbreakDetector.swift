//
//  JailbreakDetector.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//
// README:
// https://medium.com/better-programming/how-to-make-ios-app-secure-from-jailbroken-device-1efccc736d9b
//

import Foundation

class JailbreakDetector {
    static func isDeviceJailBroken() -> Bool {
//        return false
        return containsJailbreakSpecificApps() ||
            containsSydiaScheme() ||
            canAppAccessOutsideOfItsSandbox()
    }
    
    fileprivate static func containsJailbreakSpecificApps() -> Bool {
        if access("/Applications/Cydia.app", F_OK) != -1 ||
//            access("/Applications/blackra1n.app", F_OK) != -1 ||
//            access("/Applications/FakeCarrier.app", F_OK) != -1 ||
//            access("/Applications/Icy.app", F_OK) != -1 ||
//            access("/Applications/IntelliScreen.app", F_OK) != -1 ||
//            access("/Applications/MxTube.app", F_OK) != -1 ||
//            access("/Applications/RockApp.app", F_OK) != -1 ||
//            access("/Applications/SBSettings.app", F_OK) != -1 ||
//            access("/Applications/WinterBoard.app", F_OK) != -1 ||
//            access("/Library/MobileSubstrate/MobileSubstrate.dylib", F_OK) != -1 ||
//            access("/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist", F_OK) != -1 ||
//            access("/Library/MobileSubstrate/DynamicLibraries/Veency.plist", F_OK) != -1 ||
//            access("/private/var/lib/apt", F_OK) != -1 ||
            access("/private/var/lib/cydia", F_OK) != -1 ||
//            access("/private/var/mobile/Library/SBSettings/Themes", F_OK) != -1 ||
//            access("/private/var/stash", F_OK) != -1 ||
            access("/private/var/tmp/cydia.log", F_OK) != -1
//            access("/System/Library/LaunchDaemons/com.ikey.bbot.plist", F_OK) != -1 ||
//            access("/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist", F_OK) != -1 ||
//            access("/usr/bin/sshd", F_OK) != -1 ||
//            access("/usr/libexec/sftp-server", F_OK) != -1 ||
//            access("/usr/sbin/sshd", F_OK) != -1 ||
//            access("/bin/bash", F_OK) != -1 ||
//            access("/etc/apt", F_OK) != -1
        {
            return true
        }
        return false
    }
    
    fileprivate static func containsSydiaScheme() -> Bool {
        if let url = URL(string: "cydia://package/com.example.package") {
            if UIApplication.shared.canOpenURL(url) {
                return true
            }
        }
        return false
    }
    
    fileprivate static func canAppAccessOutsideOfItsSandbox() -> Bool {
        var canWrite = true
        do {
            let stringToBeWritten = "This is a JB test."
            try stringToBeWritten.write(toFile: "/private/jailbreak.txt", atomically: true, encoding: .utf8)
        } catch {
            canWrite = false
        }
        
        var canRemove = true
        do {
            let fileManager = FileManager.default
            try fileManager.removeItem(atPath: "/private/jailbreak.txt")
        } catch {
            canRemove = false
        }
        
        return canWrite || canRemove
    }
}
