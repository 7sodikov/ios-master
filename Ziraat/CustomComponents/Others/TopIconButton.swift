//
//  TopIconButton.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/6/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


public class TopIconButton: UIButton {
    
    public var width: CGFloat = (UIScreen.main.bounds.width - 96)/3
    public var height: CGFloat = 84
    
    public override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        super.titleRect(forContentRect: contentRect)
        return CGRect(x: 0, y: 50, width: width, height: 34)
    }
    
    public override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        super.imageRect(forContentRect: contentRect)
        let x: CGFloat = (width - 40)/2
        return CGRect(origin: .init(x: x, y: 0), size: .init(width: 40, height: 40))
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel?.textColor = .black
        titleLabel?.font = .gothamNarrow(size: 14, .medium) //.regular(ofSize: 14)
        titleLabel?.numberOfLines = 2
        titleLabel?.lineBreakMode = .byWordWrapping
        titleLabel?.textAlignment = .center
        imageView?.contentMode = .scaleAspectFit
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var intrinsicContentSize: CGSize {
        .init(width:width, height: height)
    }

}
