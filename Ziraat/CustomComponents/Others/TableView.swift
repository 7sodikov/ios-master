//
//  TableView.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/28/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class TableView: UITableView {
    var showHeader = false {
        didSet {
            if showHeader {
                footerHeaderView.frame = CGRect(x: 0, y: 0,
                                                width: UIScreen.main.bounds.width,
                                                height: Adaptive.val(30))
                tableHeaderView = footerHeaderView
            }
        }
    }
    
    var showFooter = false {
        didSet {
            if showFooter {
                tableFooterView = footerHeaderView
            }
        }
    }
    
    var animatePreloader = false {
        didSet {
            if animatePreloader {
                indicator.startAnimating()
            } else {
                indicator.stopAnimating()
            }
        }
    }
    
    var showReloadBtn = false {
        didSet {
            reloadButton.isHidden = !showReloadBtn
        }
    }
    
    lazy var reloadButton: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0,
                                         width: Adaptive.val(23),
                                         height: Adaptive.val(23)))
        btn.setImage(UIImage(named: ""), for: .normal)
        btn.isHidden = true
        return btn
    }()
    
    
    private lazy var indicator: UIActivityIndicatorView = {
        var indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0,
                                                              width: Adaptive.val(40),
                                                              height: Adaptive.val(40)))
        indicator.color = .black
        return indicator
    }()
    
    lazy var footerHeaderView: UIView = {
        let v = UIView(frame: CGRect(x: 0, y: 0,
                                     width: UIScreen.main.bounds.width,
                                     height: self.indicator.frame.width))
        v.backgroundColor = .clear
        return v
    }()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        addSubview(indicator)
        addSubview(reloadButton)
    }
    
    override var contentSize: CGSize {
        didSet {
            
            var rowsCount = 0
            
            for i in 0..<numberOfSections {
                rowsCount = numberOfRows(inSection: i)
                if rowsCount > 0 {
                    break
                }
            }
            
            isScrollEnabled = true
            
            if rowsCount > 0 {
                if showHeader {
                    indicator.frame = CGRect(x: 0, y: 0,
                                             width: UIScreen.main.bounds.width,
                                             height: Adaptive.val(40))
                    
                } else if showFooter {
                    indicator.frame = CGRect(x: 0,
                                             y: contentSize.height - indicator.frame.height,
                                             width: UIScreen.main.bounds.width,
                                             height: Adaptive.val(40))
                }
            } else {
                isScrollEnabled = false
                indicator.frame = CGRect(x: (frame.width - indicator.frame.width)/2,
                                         y: (frame.height - indicator.frame.height)/2,
                                         width: indicator.frame.width,
                                         height: indicator.frame.height)
                
                reloadButton.frame = CGRect(x: (frame.width - reloadButton.frame.width)/2,
                                            y: (frame.height - reloadButton.frame.height)/2,
                                            width: reloadButton.frame.width,
                                            height: reloadButton.frame.height)
            }
        }
    }
}
