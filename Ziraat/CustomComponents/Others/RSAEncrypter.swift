//
//  RSAEncrypter.swift
//  Ziraat
//
//  Created by Shamsiddin on 3/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import SwiftyRSA

class RSAEncrypter {
    static func encrypt(text: String?) -> String? {
        guard let text = text else { return nil }
        
        guard let rsaPublicKey = KeychainManager.rsaPublicKey,
              let publicKey = try? PublicKey(base64Encoded: rsaPublicKey) else {
            return nil
        }
        
        let textMessage = try? ClearMessage(string: text, using: .utf8)
        
        guard let encryptedText = try? textMessage?.encrypted(with: publicKey, padding: .PKCS1).base64String else {
            return nil
        }
        
        return encryptedText
    }
}
