//// Header

import UIKit

class CardView: BaseView {
    
    fileprivate var numberLabel = UILabel()
    fileprivate var balanceTitleLabel = UILabel()
    fileprivate var balanceLabel = UILabel()
    fileprivate var expTitleLabel = UILabel()
    fileprivate var expLabel = UILabel()
    fileprivate var nameLabel = UILabel()
    fileprivate var bankLabel = UILabel()
    fileprivate let gradient = UIView()
    fileprivate let eyeImageView = UIImageView()
    
    fileprivate let blockView = UIView()
    fileprivate let blockImage = UIImageView()
    fileprivate let typeIcon = UIImageView()
    var eyeIconContainer = UIButton()
    
    fileprivate var bgImageView = UIImageView()
    var dashboardCard: DashboardCardVM?{
        didSet{
            changeCard()
        }
    }
    func changeLabelSize(_ small : Bool){
        if small{
            changeFontSize(Adaptive.val(8))
        }
    }
    private func changeCard(){
        if let obj = self.dashboardCard{
            bgImageView.kf.setImage(with: URL(string: obj.cardBGImageURL), placeholder: nil)
            balanceTitleLabel.text = RS.lbl_balance.localized() + ":"
            balanceLabel.text = obj.balanceStr
            numberLabel.text = (obj.card.pan ?? "")
            expTitleLabel.text = ""
            expLabel.text = obj.expiry
            nameLabel.text = obj.cardTitle
            
            let eyeImageName = UDManager.hideBalance ? "btn_eye" : "img_eye"
            let icon = UIImage(named: eyeImageName)?.withRenderingMode(.alwaysTemplate)
            eyeImageView.image = icon //setImage(icon, for: .normal)
            
            if obj.card.status?.lowercased() != "active"{
                gradient.isHidden = true
                blockView.isHidden = false
            } else{
                gradient.isHidden = false
                blockView.isHidden = true
            }
            _ = dashboardCard?.card.type ?? ""
            typeIcon.isHidden = true
            //            if type.lowercased() == "uzcard" {
            //                typeIcon.isHidden = false
            //            } else{
            //                typeIcon.isHidden = true
            //            }
        }
    }
    
    func update(backgroundImage: String, balance: String, pan: String, expire: String, holder: String) {
        bgImageView.kf.setImage(with: URL(string: backgroundImage), placeholder: nil)
        balanceTitleLabel.text = RS.lbl_balance.localized() + ":"
        balanceLabel.text = balance
        numberLabel.text = pan
        expTitleLabel.text = ""
        expLabel.text = expire
        nameLabel.text = holder
        
        let eyeImageName = UDManager.hideBalance ? "btn_eye" : "img_eye"
        let icon = UIImage(named: eyeImageName)?.withRenderingMode(.alwaysTemplate)
        eyeImageView.image = icon //setImage(icon, for: .normal)
        
        gradient.isHidden = false
        blockView.isHidden = true
        typeIcon.isHidden = true
    }
    
    override func embedSubviews() {
        self.clipsToBounds = true
        self.addSubview(bgImageView)
        addSubview(gradient)
        gradient.addSubview(numberLabel)
        gradient.addSubview(expTitleLabel)
        gradient.addSubview(expLabel)
        gradient.addSubview(balanceTitleLabel)
        gradient.addSubview(balanceLabel)
        gradient.addSubview(nameLabel)
        gradient.addSubview(eyeIconContainer)
        eyeIconContainer.addSubview(eyeImageView)
        addSubview(blockView)
        addSubview(typeIcon)
        blockView.addSubview(blockImage)
    }
    
    func changeFontSize(_ size: CGFloat){
        numberLabel.font = .gothamNarrow(size: size, .medium)//EZFontType.regular.sfProDisplay(size: size)
        expTitleLabel.font = .gothamNarrow(size: size, .medium)
        expLabel.font = .gothamNarrow(size: size, .medium)
        balanceTitleLabel.font = .gothamNarrow(size: size, .medium)
        balanceLabel.font = .gothamNarrow(size: size, .medium)
        nameLabel.font = .gothamNarrow(size: size, .medium)
    }
    
    override func initSubviews() {
        bgImageView.contentMode = .scaleAspectFill
        bgImageView.clipsToBounds = true
        bgImageView.layer.cornerRadius = 12
        
        let font = UIFont.gothamNarrow(size: 14, .bold)
        
        numberLabel.textColor = .white
        numberLabel.textAlignment = .right
        numberLabel.font = font
        
        balanceLabel.font = font
        balanceLabel.textColor = .white
        balanceLabel.adjustsFontSizeToFitWidth = true
        
        balanceTitleLabel.font = font
        balanceTitleLabel.textColor = .white
        balanceTitleLabel.adjustsFontSizeToFitWidth = true
        
        eyeImageView.tintColor = .white
        eyeIconContainer.backgroundColor = .clear
        
        expLabel.textColor = .white
        expLabel.textAlignment = .right
        expLabel.font = font
        
        expTitleLabel.textColor = .white
        expTitleLabel.textAlignment = .left
        expTitleLabel.font = font
        
        nameLabel.textColor = .white
        nameLabel.numberOfLines = 3
        nameLabel.font = font
        
        gradient.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        gradient.layer.cornerRadius = 4
        changeFontSize(Adaptive.val(16))
        
        blockImage.image = UIImage(named: "lockIcon")
        blockImage.contentMode = .scaleAspectFit
        
        blockView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        blockView.layer.cornerRadius = 4
        
        typeIcon.contentMode = .scaleAspectFit
        typeIcon.image = UIImage(named: "uzcardIcon")
        
    }
    
    override func addSubviewsConstraints() {
        bgImageView.snp.updateConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        let left = Adaptive.val(5)
        let padding = Adaptive.val(10)
        
        gradient.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.bottom.equalTo(-Adaptive.val(10))
        }
        print(self.frame.height, self.bounds.height,self.frame)
        
        balanceTitleLabel.snp.updateConstraints { (make) in
            make.left.equalTo(left)
            //            make.right.equalTo(-left)
            make.top.equalTo(numberLabel.snp.bottom).offset(padding)
        }
        
        balanceLabel.snp.updateConstraints { (make) in
            make.left.equalTo(balanceTitleLabel.snp.right).offset(left)
            make.top.equalTo(numberLabel.snp.bottom).offset(padding)
        }
        
        numberLabel.snp.updateConstraints { (make) in
            make.left.equalTo(left)
            make.top.equalTo(left)
        }
        
        expTitleLabel.snp.updateConstraints { (make) in
            make.bottom.equalToSuperview().offset(-left)
            make.right.equalTo(-Adaptive.val(48))
        }
        
        nameLabel.snp.updateConstraints { (make) in
            make.left.equalTo(left)
            make.top.equalTo(balanceLabel.snp.bottom).offset(padding)
            make.bottom.equalToSuperview().offset(-left)
            make.right.equalTo(-left)
        }
        
        expLabel.snp.updateConstraints { (make) in
            make.left.equalTo(left)
            make.top.equalTo(left)
            make.right.equalTo(Adaptive.val(-54))
        }
        
        eyeIconContainer.snp.updateConstraints { (make) in
            make.right.equalTo(Adaptive.val(0 - left))
            make.width.height.equalTo(Adaptive.val(25))
            make.top.equalTo(numberLabel.snp.top)
        }
        
        eyeImageView.snp.updateConstraints { (make) in
            make.centerY.centerX.equalToSuperview()
            make.width.height.equalTo(Adaptive.val(25))
        }
        
        blockView.snp.makeConstraints { (make) in
            make.left.equalTo(left)
            make.right.equalTo(-left)
            make.height.equalToSuperview().multipliedBy(0.34)
            make.bottom.equalTo(gradient.snp.bottom)
        }
        
        blockImage.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.top.equalTo(Adaptive.val(5))
            make.bottom.equalTo(-Adaptive.val(5))
            make.width.equalTo(blockImage.snp.height)
        }
        
        typeIcon.snp.makeConstraints { (make) in
            make.width.height.equalTo(35)
            make.right.equalTo(-10)
            make.top.equalTo(10)
        }
    }
}
