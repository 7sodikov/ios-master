//
//  KTCenterFlowLayout.swift
//  wtp
//
//  Created by Shamsiddin on 9/1/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class KTCenterFlowLayout: UICollectionViewFlowLayout {
    var attrCache: [IndexPath: UICollectionViewLayoutAttributes] = [:]
    
    override func prepare() {
        attrCache = [:]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var updatedAttributes: [UICollectionViewLayoutAttributes] = []
        
        let sections = collectionView?.numberOfSections ?? 1
        var s: Int = 0
        while s < sections {
            let rows = collectionView?.numberOfItems(inSection: s) ?? 0
            var r: Int = 0
            while r < rows {
                let indexPath = IndexPath(row: r, section: s)
                let attrs = self.layoutAttributesForItem(at: indexPath)
                if let attrs = attrs, attrs.frame.intersects(rect) {
                    updatedAttributes.append(attrs)
                }
                
                let headerAttrs = super.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                             at: indexPath)
                if let headerAttrs = headerAttrs, headerAttrs.frame.intersects(rect) {
                    updatedAttributes.append(headerAttrs)
                }
                
                let footerAttrs = super.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
                                                                             at: indexPath)
                if let footerAttrs = footerAttrs, footerAttrs.frame.intersects(rect) {
                    updatedAttributes.append(footerAttrs)
                }
                r += 1
            }
            s += 1
        }
        return updatedAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if let attr = self.attrCache[indexPath] {
            return attr
        }
        
        // Find the other items in the same "row"
        var rowBuddies: [UICollectionViewLayoutAttributes] = []
        
        // Calculate the available width to center stuff within
        // sectionInset is NOT applicable here because a) we're centering stuff
        // and b) Flow layout has arranged the cells to respect the inset. We're
        // just hijacking the X position.
        let collectionViewWidth = collectionView!.bounds.width -
            collectionView!.contentInset.left -
            collectionView!.contentInset.right
        
        // To find other items in the "row", we need a rect to check intersects against.
        // Take the item attributes frame (from vanilla flow layout), and stretch it out
        var rowTestFrame: CGRect = super.layoutAttributesForItem(at: indexPath)?.frame ?? .zero
        rowTestFrame.origin.x = 0
        rowTestFrame.size.width = collectionViewWidth
        
        let totalRows = collectionView?.numberOfItems(inSection: indexPath.section) ?? 0
        
        // From this item, work backwards to find the first item in the row
        // Decrement the row index until a) we get to 0, b) we reach a previous row
        var rowStartIDX: Int = indexPath.row
        while true {
            let prevIDX: Int = rowStartIDX - 1
            if prevIDX < 0 {
                break
            }
            
            let prevPath = IndexPath(row: prevIDX, section: indexPath.section)
            let prevFrame: CGRect = super.layoutAttributesForItem(at: prevPath)?.frame ?? .zero
            
            // If the item intersects the test frame, it's in the same row
            if prevFrame.intersects(rowTestFrame) {
                rowStartIDX = prevIDX
            } else {
                // Found previous row, escape!
                break
            }
        }
        
        // Now, work back UP to find the last item in the row
        // For each item in the row, add it's attributes to rowBuddies
        var buddyIDX = rowStartIDX
        while true {
            if buddyIDX > (totalRows - 1) {
                break
            }
            
            let buddyPath = IndexPath(row: buddyIDX, section: indexPath.section)
            let buddyAttributes = super.layoutAttributesForItem(at: buddyPath)
            if let buddyAttributes = buddyAttributes, buddyAttributes.frame.intersects(rowTestFrame) {
                // If the item intersects the test frame, it's in the same row
                rowBuddies.append(buddyAttributes)
                buddyIDX += 1
            } else {
                // Encountered next row
                break
            }
        }
        
        let flowDelegate: UICollectionViewDelegateFlowLayout? = collectionView?.delegate as? UICollectionViewDelegateFlowLayout
        let delegateSupportsInteritemSpacing = flowDelegate?.collectionView?(collectionView!, layout: self, minimumInteritemSpacingForSectionAt: indexPath.section) != nil
        
        // x-x-x-x ... sum up the interim space
        var interitemSpacing = self.minimumInteritemSpacing
        
        // Check for minimumInteritemSpacingForSectionAtIndex support
        if delegateSupportsInteritemSpacing && rowBuddies.count > 0 {
            interitemSpacing = flowDelegate?.collectionView?(self.collectionView!, layout: self, minimumInteritemSpacingForSectionAt: indexPath.section) ?? 0
        }
        
        let aggregateInteritemSpacing = interitemSpacing * CGFloat(rowBuddies.count - 1)
        
        // Sum the width of all elements in the row
        var aggregateItemWidths: CGFloat = 0
        for itemAttributes in rowBuddies {
            aggregateItemWidths += itemAttributes.frame.width
        }
        
        // Build an alignment rect
        // |  |x-x-x-x|  |
        let alignmentWidth = aggregateItemWidths + aggregateInteritemSpacing
        let alignmentXOffset = (collectionViewWidth - alignmentWidth)/2
        
        // Adjust each item's position to be centered
        var previousFrame = CGRect.zero
        for itemAttributes in rowBuddies {
            var itemFrame = itemAttributes.frame
            if previousFrame.equalTo(.zero) {
                itemFrame.origin.x = alignmentXOffset
            } else {
                itemFrame.origin.x = previousFrame.maxX + interitemSpacing
            }
            
            itemAttributes.frame = itemFrame
            previousFrame = itemFrame
            
            // Finally, add it to the cache
            self.attrCache[itemAttributes.indexPath] = itemAttributes
        }
        return self.attrCache[indexPath]
    }
}
