//
//  BaseViewController.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/2/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BaseViewControllerProtocol: class {
    func preloader(show: Bool)
    func showError(message: String?)
    
}

class BaseViewController: BaseMainSwipableViewController, BaseViewControllerProtocol {
    
    var vSpinner : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton(hide: false)
        self.activateLeftSwipeGesture()
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        setNeedsStatusBarAppearanceUpdate()
    //    }
    
    //it was private before
    func setBackButton(hide: Bool) {
        let backBtn = UIButton()
        backBtn.setImage(UIImage(named: "btn_prev"), for: .normal)
        
        //        let backBarBtn = UIBarButtonItem(customView: backBtn)
        let backBarBtn = UIBarButtonItem(image: UIImage(named: "btn_navigation_back")?.changeColor(.dashDarkBlue),
                                         style: .plain,
                                         target: navigationController,
                                         action: #selector(UINavigationController.popViewController(animated:)))
        
        navigationItem.leftBarButtonItem = backBarBtn
        backBtn.isHidden = hide
    }
    
    
    func showError(message: String?) {
        //        let alert = UIAlertController(title: RS.lbl_error.localized(),
        //                                      message: message,
        //                                      preferredStyle: .alert)
        //        alert.setValue(UIImage(named: "img_excl_mark")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        //        alert.addAction(UIAlertAction(title: RS.btn_yes.localized(), style: .cancel, handler: nil))
        //        self.present(alert, animated: true, completion: nil)
        
        let statementVC = ErrorMessageRouter.createModule(message: message)
        statementVC.modalPresentationStyle = .overFullScreen
        self.present(statementVC, animated: false, completion: nil)
    }
    
    deinit {
        print("dealloc:" + Self.description())
    }
    
    func preloader(show: Bool) {
        
    }
    
    func setupTargets() {
        navigationItem.setRightBarButton(UIBarButtonItem(image: UIImage(named: "img_icon_home"), style: .plain, target: self, action: #selector(goHome)), animated: true)
    }
    
    @objc func goHome() {
        appDelegate.router.navigate(to: .mainPage)
//        self.dismiss(animated: true, completion: nil)
    }
    
    private func activateLeftSwipeGesture(){
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    func deAactivateLeftSwipeGesture(){
        if let gestures = self.view.gestureRecognizers {
            for gesture in gestures {
                if let leftswipe = (gesture as? UIScreenEdgePanGestureRecognizer), leftswipe.edges == .left {
                    self.view.removeGestureRecognizer(leftswipe)
                }
            }
        }
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            print("Screen edge swiped!")
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension BaseViewController {
    
    enum SpinnerAction: Int {
        case start
        case stop
    }
    
    func spinner(_ spinnerAction: SpinnerAction) {
        switch spinnerAction {
        case .start:
            showSpinner()
        case .stop:
            removeSpinner()
        }
    }
    
    func showSpinner() {
        let spinnerView = UIView.init(frame: view.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            self.view.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
    
}


class BaseMainSwipableViewController : UIViewController {
    
    var baseNavigationTitleLabel: UILabel?
    
    func initBaseNavLabel(_ index: Int) {
        baseNavigationTitleLabel = UILabel()
        baseNavigationTitleLabel?.textAlignment = .center
        baseNavigationTitleLabel?.textColor = .white
        baseNavigationTitleLabel?.font = .gothamNarrow(size: 16, .bold)
        
        guard let label = baseNavigationTitleLabel else {
            return
        }
        self.view.addSubview(label)
        
        baseNavigationTitleLabel?.snp.remakeConstraints { (maker) in
            maker.width.equalTo(UIScreen.main.bounds.width)
            maker.centerX.equalToSuperview()
            maker.top.equalToSuperview().offset(Adaptive.val(0))
        }
        baseNavigationTitleLabel?.setTitleFor(pageIndex: index)
//        baseNavigationTitleLabel?.layer.masksToBounds = true
        self.view.clipsToBounds = true
        
    }
    
}
