//
//  LabelWithInsets.swift
//  Ziraat
//
//  Created by Shamsiddin on 7/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class LabelWithInsets: UILabel {
    var edgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    override func drawText(in rect: CGRect) {
//        super.drawText(in: UIEdgeInsetsInsetRect(rect, edgeInsets))
        super.drawText(in: rect.inset(by: edgeInsets))
    }
}
