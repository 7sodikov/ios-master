//
//  EZHorizontalControlInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/13/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZHorizontalControlInstaller: ViewInstaller {
    var stackView: UIStackView! { get set }
}

extension EZHorizontalControlInstaller {
    func initSubviews() {
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Adaptive.val(2)
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
//            maker.height.equalTo(Adaptive.val(41))
        }
    }
}

fileprivate struct Size {
    
}
