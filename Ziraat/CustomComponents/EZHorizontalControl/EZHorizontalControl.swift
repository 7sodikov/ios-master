//
//  EZHorizontalControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/13/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class EZHorizontalControl: UIControl, EZHorizontalControlInstaller {
    var mainView: UIView { self }
    var stackView: UIStackView!
    var text: String!
    
    func setControl() -> EZHorizontalControl {
        for i in 0..<stackView.arrangedSubviews.count {
            let btn = stackView.arrangedSubviews[i] as? UIStackView
            btn?.backgroundColor = .systemPink
        }
        return self
    }
    
//    var selectedIndex: Int = 0 {
//        didSet {
//            guard selectedIndex < stackView.arrangedSubviews.count else {
//                return
//            }
//            
//            for i in 0..<stackView.arrangedSubviews.count {
//                let btn = stackView.arrangedSubviews[i] as? UIStackView
//                btn?.backgroundColor = .systemPink
//            }
//        }
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func addSegment(with rate: String, interest: String) {
        let btn = createSegmentButton(with: rate, interest: interest)
        btn.tag = stackView.arrangedSubviews.count
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButton(with rate: String, interest: String) -> UIStackView {
        let comp = UIStackView(frame: .zero)
        let interestLabel = UILabel()
        let rateLabel = UILabel()
                
        interestLabel.textColor = .black
        interestLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        interestLabel.textAlignment = .right
        interestLabel.text = interest
        
        rateLabel.textColor = .black
        rateLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        rateLabel.textAlignment = .right
        rateLabel.text = rate

        comp.addArrangedSubview(rateLabel)
        comp.addArrangedSubview(interestLabel)
        comp.axis = .horizontal
        comp.distribution = .fillEqually
//        comp.snp.remakeConstraints { (maker) in
//            maker.height.equalTo(Adaptive.val(30))
//        }
        return comp
    }
    
//    func addSegmentDarkVersion(with title: String) {
//        let btn = createSegmentButtonDarkVersion(with: title)
//        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
//        btn.tag = stackView.arrangedSubviews.count
//        stackView.addArrangedSubview(btn)
//    }
//    
//    private func createSegmentButtonDarkVersion(with title: String) -> UIButton {
//        let comp = UIButton(frame: .zero)
//        comp.setTitle(title, for: .normal)
//        comp.layer.cornerRadius = Adaptive.val(10)
//        comp.clipsToBounds = true
//        comp.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//        comp.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
//        comp.titleLabel?.numberOfLines = 0
//        comp.setTitleColor(.white, for: .normal)
//        comp.setTitleColor(.black, for: .selected)
//        comp.setBackgroundColor(ColorConstants.highlightedGray.withAlphaComponent(0.9), for: .normal)
//        comp.setBackgroundColor(ColorConstants.highlightedGray.withAlphaComponent(0.9), for: .highlighted)
//        comp.setBackgroundColor(.white, for: .selected)
//        return comp
//    }
//    
//    @objc private func btnClicked(_ sender: UIButton) {
//        selectedIndex = sender.tag
//        self.sendActions(for: .valueChanged)
//    }
    
    
}
