//
//  CaptchaView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/4/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class CaptchaView: UIView, CaptchaViewInstaller {
    var captchaView: UIView!
    var captchaImageView: UIImageView!
    var reloadCaptchaButton: UIButton!
    var mainView: UIView { self }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
}
