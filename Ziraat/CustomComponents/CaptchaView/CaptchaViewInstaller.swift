//
//  CaptchaViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/4/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol CaptchaViewInstaller: ViewInstaller {
    var captchaView: UIView! { get set }
    var captchaImageView: UIImageView! { get set }
    var reloadCaptchaButton: UIButton! { get set }
}

extension CaptchaViewInstaller {
    func initSubviews() {
        captchaView = UIView()
        captchaView.backgroundColor = .white
        captchaView.layer.cornerRadius = Adaptive.val(27)
        
        captchaImageView = UIImageView()
        captchaView.contentMode = .scaleAspectFill
        
        reloadCaptchaButton = UIButton()
        reloadCaptchaButton.setImage(UIImage(named: "btn_reload_captcha"), for: .normal)
        reloadCaptchaButton.tintColor = .black
    }
    
    func embedSubviews() {
        mainView.addSubview(captchaView)
        captchaView.addSubview(captchaImageView)
        captchaView.addSubview(reloadCaptchaButton)
    }
    
    func addSubviewsConstraints() {
        captchaView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
        }
        
        captchaImageView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(60))
            maker.top.bottom.equalTo(captchaView)
            maker.leading.equalTo(captchaView.snp.leading).offset(Adaptive.val(10))
            maker.trailing.equalTo(reloadCaptchaButton.snp.leading).offset(Adaptive.val(5))
        }
        
        reloadCaptchaButton.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(60))
            maker.width.equalTo(Adaptive.val(45))
            maker.top.trailing.bottom.equalTo(captchaView)
        }
    }
}

