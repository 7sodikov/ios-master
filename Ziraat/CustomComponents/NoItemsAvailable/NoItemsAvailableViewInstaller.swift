//
//  NoItemsAvailableViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol NoItemsAvailableViewInstaller: ViewInstaller {
    var warrantyImageView: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
}

extension NoItemsAvailableViewInstaller {
    func initSubviews() {
        warrantyImageView = UIImageView()
        warrantyImageView.image = UIImage(named: "img_red_exclam")
        
        titleLabel = UILabel()
        titleLabel.textColor = .black
        titleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(14))
    }
    
    func embedSubviews() {
        mainView.addSubview(warrantyImageView)
        mainView.addSubview(titleLabel)
    }
    
    func addSubviewsConstraints() {
        warrantyImageView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.centerX.equalTo(mainView)
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(warrantyImageView.snp.bottom).offset(Adaptive.val(24))
            maker.centerX.equalTo(mainView)
        }
    }
}

fileprivate struct Size {
    
}
