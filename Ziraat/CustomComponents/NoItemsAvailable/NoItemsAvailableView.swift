//
//  NoItemsAvailableView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/3/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class NoItemsAvailableView: UIView, NoItemsAvailableViewInstaller {
    var warrantyImageView: UIImageView!
    var titleLabel: UILabel!
    var mainView: UIView { self }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
    
    static func setupText(title: String) -> NoItemsAvailableView {
        let message = NoItemsAvailableView(frame: .zero)
        message.titleLabel.text = title
        return message
    }
}
