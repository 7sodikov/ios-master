//
//  EZSelectControllerViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZSelectControllerViewInstaller: ViewInstaller {
    var bgImageView: UIImageView! { get set }
    var tableView: UITableView! { get set }
    var cancelButton: NextButton! { get set }
}

extension EZSelectControllerViewInstaller {
    func initSubviews() {
        bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "img_dash_light_background")
        
        tableView = UITableView(frame: .zero, style: .plain)
//        tableView.register(UITableViewCell.self, forCellReuseIdentifier: String(describing: UITableViewCell.self))
        tableView.backgroundColor = .clear
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: Adaptive.val(110), right: 0)
        
        cancelButton = NextButton.systemButton(with: RS.lbl_cancel.localized(), cornerRadius: Size.NextButton.height/2)
        cancelButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        cancelButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        cancelButton.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
    }
    
    func embedSubviews() {
        mainView.addSubview(bgImageView)
        mainView.addSubview(tableView)
        mainView.addSubview(cancelButton)
    }
    
    func addSubviewsConstraints() {
        bgImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        tableView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(20) // was 64 when used navigation bar
            maker.left.bottom.right.equalToSuperview()
        }
        
        cancelButton.snp.remakeConstraints { (maker) in
            maker.bottom.equalTo(-Adaptive.val(50))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(Size.NextButton.height)
        }
    }
}

fileprivate struct Size {
    struct NextButton {
        static let height = Adaptive.val(54)
    }
}

