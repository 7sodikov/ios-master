//
//  EZSelectController.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class EZSelectControllerCellModel {
    var title: String?
    var subTitle: String?
}

protocol EZSelectControllerDelegate: class {
    func numberOfSections(in ezSelectController: EZSelectController) -> Int
    func ezSelectController(_ ezSelectController: EZSelectController, numberOfRowsInSection section: Int) -> Int
    func ezSelectController(_ ezSelectController: EZSelectController, titleAndSubtitleForRowAt indexPath: IndexPath) -> (title: String, subtitle: String)
    func ezSelectController(_ ezSelectController: EZSelectController, didSelectRowAt indexPath: IndexPath)
    func ezSelectController(_ ezSelectController: EZSelectController, titleForHeaderInSection section: Int) -> String?
}

class EZSelectController: UIViewController, EZSelectControllerViewInstaller {
    var mainView: UIView { view }
    var bgImageView: UIImageView!
    var tableView: UITableView!
    var cancelButton: NextButton!
    weak var delegate: EZSelectControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        tableView.dataSource = self
        tableView.delegate = self
        
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked), for: .touchUpInside)
//        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: cancelButton)
    }
    
    func wrapIntoNavigation() -> UINavigationController {
        let navCtrl = NavigationController(rootViewController: self)
        return navCtrl
    }
    
    @objc private func cancelButtonClicked() {
        dismiss(animated: true, completion: nil)
    }
}

extension EZSelectController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return delegate?.numberOfSections(in: self) ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.ezSelectController(self, numberOfRowsInSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = String(describing: UITableViewCell.self)
        var cell = tableView.dequeueReusableCell(withIdentifier: cellid)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellid)
        }
        
        cell?.backgroundColor = .clear
        let cellItem = delegate?.ezSelectController(self, titleAndSubtitleForRowAt: indexPath)
        cell?.textLabel?.textColor = .black
        cell?.detailTextLabel?.textColor = .black
        cell?.textLabel?.text = cellItem?.title
        cell?.detailTextLabel?.text = cellItem?.subtitle
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Adaptive.val(80)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title = delegate?.ezSelectController(self, titleForHeaderInSection: section)
        let lbl = LabelWithInsets()
        lbl.backgroundColor = ColorConstants.nativeTableSectionHeaderColor
        lbl.textColor = .black
        lbl.edgeInsets = UIEdgeInsets(top: 3, left: 16, bottom: 3, right: 16)
        lbl.text = title
        lbl.font = EZFontType.regular.sfuiDisplay(size: 18)
        return lbl
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.ezSelectController(self, didSelectRowAt: indexPath)
        dismiss(animated: true, completion: nil)
    }
}
