//
//  EZDatePickerController.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol EZDatePickerControllerDelegate: class {
    func ezDatePickerDidSelect(date: Date, userInfo: Any?)
}

class EZDatePickerController: UIViewController, EZDatePickerControllerViewInstaller {
    var mainView: UIView { view }
    var cancelButton: UIButton!
    var doneButton: UIButton!
    var cancelBarButton: UIBarButtonItem!
    var doneBarButton: UIBarButtonItem!
    var toolBar: UIToolbar!
    var datePicker: UIDatePicker!
    
    var userInfo: Any?
    
    weak var delegate: EZDatePickerControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked(_:)), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc private func cancelButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func doneButtonClicked(_ sender: UIButton) {
        delegate?.ezDatePickerDidSelect(date: datePicker.date, userInfo: userInfo)
        dismiss(animated: true, completion: nil)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismiss(animated: true, completion: nil)
    }
    
    func open(in controller: UIViewController, selectedDate: Date = Date()) {
        modalPresentationStyle = .overCurrentContext
        controller.present(self, animated: true, completion: nil)
        datePicker.setDate(selectedDate, animated: true)
    }
}
