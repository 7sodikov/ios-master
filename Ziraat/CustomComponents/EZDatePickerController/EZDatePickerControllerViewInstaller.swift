//
//  EZDatePickerControllerViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZDatePickerControllerViewInstaller: ViewInstaller {
    var cancelButton: UIButton! { get set }
    var doneButton: UIButton! { get set }
    var cancelBarButton: UIBarButtonItem! { get set }
    var doneBarButton: UIBarButtonItem! { get set }
    
    var toolBar: UIToolbar! { get set }
    var datePicker: UIDatePicker! { get set }
}

extension EZDatePickerControllerViewInstaller {
    func initSubviews() {
        cancelButton = UIButton()
        cancelButton.setTitle(RS.lbl_cancel.localized(), for: .normal)
        cancelButton.setTitleColor(.black, for: .normal)
        cancelButton.setTitleColor(ColorConstants.lightGray, for: .highlighted)
        
        doneButton = UIButton()
        doneButton.setTitle(RS.btn_done.localized(), for: .normal)
        doneButton.setTitleColor(.black, for: .normal)
        doneButton.setTitleColor(ColorConstants.lightGray, for: .highlighted)
        
        cancelBarButton = UIBarButtonItem(customView: cancelButton)
        
        doneBarButton = UIBarButtonItem(customView: doneButton)
        
        toolBar = UIToolbar()
        toolBar.items = [cancelBarButton, doneBarButton]
        
        datePicker = UIDatePicker(frame: .zero)
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .white
    }
    
    func embedSubviews() {
        mainView.addSubview(toolBar)
        mainView.addSubview(datePicker)
    }
    
    func addSubviewsConstraints() {
        toolBar.snp.remakeConstraints { (maker) in
            maker.left.right.equalToSuperview()
            maker.bottom.equalTo(datePicker.snp.top)
        }
        
        datePicker.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(220))
            maker.left.bottom.right.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}

