//
//  PopupMessageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PopupMessageVCProtocol: class {
    
}

class PopupMessageVC: BaseViewController, PopupMessageViewInstaller {
    var backView: UIView!
    var exclamationMarkImageView: UIImageView!
    var mainLabel: UILabel!
    var buttonStackView: UIStackView!
    var yesButton: NextButton!
    var noButton: NextButton!
    var mainView: UIView { view }
    var presenter: PopupMessagePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        mainLabel.text = presenter.message
        yesButton.addTarget(self, action: #selector(yesClicked), for: .touchUpInside)
        noButton.addTarget(self, action: #selector(closeButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc func closeButtonClicked(_ sender: UIGestureRecognizer) {
        self.dismiss(animated: false, completion: nil)
        presenter.delegate?.actionResult(false)
    }
    
    @objc private func yesClicked() {
        self.dismiss(animated: false, completion: nil)
        presenter.delegate?.actionResult(true)
    }
}

extension PopupMessageVC: PopupMessageVCProtocol {
    
}
