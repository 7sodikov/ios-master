//
//  PopupMessageViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PopupMessageViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var exclamationMarkImageView: UIImageView! { get set }
    var mainLabel: UILabel! { get set }
    var buttonStackView: UIStackView! { get set }
    var yesButton: NextButton! { get set }
    var noButton: NextButton! { get set }
}

extension PopupMessageViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = UIColor(red: 26/255, green: 32/255, blue: 45/255, alpha: 0.4)
        
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(13)
        
        exclamationMarkImageView = UIImageView()
        exclamationMarkImageView.image = UIImage(named: "img_excl_mark")
//        exclamationMarkImageView.contentMode = .scaleAspectFit
        
        mainLabel = UILabel()
        mainLabel.textColor = ColorConstants.gray
        mainLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(16))
        mainLabel.text = RS.al_msg_exit.localized()
        mainLabel.textAlignment = .center
        mainLabel.contentMode = .scaleToFill
        mainLabel.numberOfLines = 0
        
        buttonStackView = UIStackView()
        buttonStackView.axis = .horizontal
        buttonStackView.alignment = .fill
        buttonStackView.distribution = .fillEqually
        buttonStackView.spacing = Adaptive.val(13)
        
        yesButton = NextButton.systemButton(with: RS.btn_yes.localized(), cornerRadius: Size.buttonHeight/2)
        yesButton.setBackgroundColor(ColorConstants.mainRed, for: .normal)
        
        noButton = NextButton.systemButton(with: RS.btn_no.localized(), cornerRadius: Size.buttonHeight/2)
        noButton.setBackgroundColor(.white, for: .normal)
        noButton.setTitleColor(ColorConstants.gray, for: .normal) 
        noButton.layer.borderWidth = Adaptive.val(1)
        noButton.layer.borderColor = ColorConstants.gray.cgColor
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(exclamationMarkImageView)
        mainView.addSubview(mainLabel)
        mainView.addSubview(buttonStackView)
        buttonStackView.addArrangedSubview(noButton)
        buttonStackView.addArrangedSubview(yesButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Adaptive.val(32))
            maker.trailing.equalToSuperview().offset(-Adaptive.val(32))
            maker.centerY.equalToSuperview()
//            maker.height.equalTo(Adaptive.val(260))
        }
        
        exclamationMarkImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(80))
            maker.height.equalTo(Adaptive.val(80))
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(16))
            maker.centerX.equalTo(backView)
        }
        
        mainLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(12))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(12))
            maker.top.equalTo(exclamationMarkImageView.snp.bottom).offset(Adaptive.val(16))
        }
        
        buttonStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(20))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(20))
            maker.top.equalTo(mainLabel.snp.bottom).offset(Adaptive.val(17))
            maker.height.equalTo(Size.buttonHeight)
            maker.bottom.equalTo(backView.snp.bottom).offset(-Adaptive.val(20))
        }
    }
}

fileprivate struct Size {
    static let buttonHeight = Adaptive.val(40)
}
