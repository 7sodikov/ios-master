//
//  PopupMessageInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PopupMessageInteractorProtocol: class {
    
}

class PopupMessageInteractor: PopupMessageInteractorProtocol {
    weak var presenter: PopupMessageInteractorToPresenterProtocol!
    
}
