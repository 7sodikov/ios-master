//
//  PopupMessagePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol PopubMessageProtocol {
    func actionResult(_ action:Bool)
}
protocol PopupMessagePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: PopupMessageVM { get }
    var message: String? {get set}
    var delegate : PopubMessageProtocol? {get set}
}

protocol PopupMessageInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class PopupMessagePresenter: PopupMessagePresenterProtocol {
    var delegate: PopubMessageProtocol?
    var message: String?
    weak var view: PopupMessageVCProtocol!
    var interactor: PopupMessageInteractorProtocol!
    var router: PopupMessageRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = PopupMessageVM()
    
    // Private property and methods
    
}

extension PopupMessagePresenter: PopupMessageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
