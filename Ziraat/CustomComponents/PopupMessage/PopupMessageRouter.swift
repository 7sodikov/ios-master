//
//  PopupMessageRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PopupMessageRouterProtocol: class {
    static func createModule(_ message:String?, _ delegate:PopubMessageProtocol?) -> UIViewController
}

class PopupMessageRouter: PopupMessageRouterProtocol {
    static func createModule(_ message: String?, _ delegate: PopubMessageProtocol?) -> UIViewController {
        let vc = PopupMessageVC()
        let presenter = PopupMessagePresenter()
        presenter.message = message
        presenter.delegate = delegate
        let interactor = PopupMessageInteractor()
        let router = PopupMessageRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
