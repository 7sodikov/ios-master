//
//  InputAlertViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol InputAlertDelegate: class {
    func send()
}

class InputAlertViewController: UIViewController, InputAlertViewInstaller {
    
    var inputContainer: UIView!
    var container: UIView!
    var titleLabel: UILabel!
    var textLabel: UILabel!
    var inputFeild: UITextField!
    var topButton: Button!
    var bottomButton: Button!
    var stackButtonView: UIStackView!
    var mainView: UIView { self.view }
    
    let inpurt: Input
    var parameter: Any? { self.inpurt }
    weak var delegate: InputAlertDelegate? = nil
    
    init(inpurt: Input) {
        self.inpurt = inpurt
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        setupSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setupTargets()
    }
    
    func setupTargets() {
        bottomButton.didClick {
            self.dismiss(animated: true)
        }
        
        topButton.didClick {
            self.dismiss(animated: true) {
                self.delegate?.send()
            }
        }
    }
    
}


extension InputAlertViewController {
    
    enum Input {
        case sendMessage(mail: String, data: Data?)
    
        
        var title: String {
            switch self {
            case .sendMessage:
                #warning("need to localize")
                return "Send a message to email"
            }
        }
        
        var messageText: String {
            switch self {
            case .sendMessage:
                #warning("need to localize")
                return "Send a cheque to email which desplayed below"
            }
        }
        
        var inputDefaultText: String {
            switch self {
            case .sendMessage(let mail,_):
                return mail
            }
        }
        
        var data: Data? {
            switch self {
            case let .sendMessage(mail, data):
                return data
            }
        }
        
    }
    
}
