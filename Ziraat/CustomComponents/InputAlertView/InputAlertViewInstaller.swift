//
//  InputAlertViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol InputAlertViewInstaller: ViewInstaller {
    var container: UIView! {get set}
    var titleLabel: UILabel! {get set}
    var textLabel: UILabel! {get set}
    var inputContainer: UIView! {get set}
    var inputFeild: UITextField! {get set}
    var topButton: Button! {get set}
    var bottomButton: Button! {get set}
    var stackButtonView: UIStackView! {get set}
    
}

extension InputAlertViewInstaller {
    
    func initSubviews() {
        container = UIView()
        container.backgroundColor = .white
        container.layer.cornerRadius = 12
        container.addShadow()
        
        titleLabel = UILabel()
        titleLabel.font = .gothamNarrow(size: 16, .medium)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textAlignment = .center
        
        textLabel = UILabel()
        textLabel.font = .gothamNarrow(size: 14, .book)
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.textAlignment = .center
        
        inputContainer = UIView()
        inputContainer.layer.cornerRadius = 6
        inputContainer.layer.borderWidth = 1
        inputContainer.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        
        inputFeild = UITextField()
        
        topButton = Button(method: .network, style: .red)
        topButton.setup(title: RS.btn_send.localized().uppercased())

        bottomButton = Button(method: .local, style: .back)
        bottomButton.setup()
        
        stackButtonView = UIStackView()
        stackButtonView.axis = .vertical
        stackButtonView.spacing = 4
        
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        if let input = parameter as? InputAlertViewController.Input, case .sendMessage = input {
            titleLabel.text = input.title
            textLabel.text = input.messageText
            inputFeild.placeholder = input.inputDefaultText
            inputFeild.isUserInteractionEnabled = false
        }
    }
    
    func embedSubviews() {
        mainView.addSubview(container)
        
        container.addSubview(titleLabel)
        container.addSubview(textLabel)
        container.addSubview(inputContainer)
        container.addSubview(stackButtonView)
        
        inputContainer.addSubview(inputFeild)
        
        stackButtonView.addArrangedSubview(topButton)
        stackButtonView.addArrangedSubview(bottomButton)
    }
    
    func addSubviewsConstraints() {
        let height: CGFloat = 268
        container.constraint { (make) in
            make.width(312).center.equalToSuperView()
        }
        titleLabel.constraint { (make) in
            make.top(16).horizontal(16).equalToSuperView()
        }
       
        textLabel.constraint { (make) in
            make.top(self.titleLabel.bottomAnchor, offset: 32)
                .horizontal(16).equalToSuperView()
        }
        inputContainer.constraint { (make) in
            make.height(50).top(self.textLabel.bottomAnchor, offset: 16).horizontal(16).equalToSuperView()
        }
        
        inputFeild.constraint { (make) in
            make.centerY.horizontal(16).equalToSuperView()
        }
        
        stackButtonView.constraint { (make) in
            make.top(self.inputFeild.bottomAnchor, offset: 24)
            .bottom(-16).horizontal(16).equalToSuperView()
        }
        
        topButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bottomButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    
}
