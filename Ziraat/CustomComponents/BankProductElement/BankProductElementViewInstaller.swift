//
//  BankProductElementViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol BankProductElementViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var productImageView: UIImageView! { get set }
    var productLabel: UILabel! { get set }
    var mainButton: UIButton! { get set }
}

extension BankProductElementViewInstaller {
    func initSubviews() {
        backView = UIView()
        backView.backgroundColor = .white
        backView.layer.cornerRadius = Adaptive.val(20)
        
        productImageView = UIImageView()
        productImageView.contentMode = .scaleAspectFit
        
        productLabel = UILabel()
        productLabel.textColor = .black
        productLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(14))
        
        mainButton = UIButton()
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        backView.addSubview(productImageView)
        backView.addSubview(productLabel)
        backView.addSubview(mainButton)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
            maker.width.equalTo(Adaptive.val(152))
            maker.height.equalTo(Adaptive.val(94))
        }
        
        productImageView.snp.remakeConstraints { (maker) in
            maker.centerX.equalTo(backView)
            maker.top.equalTo(backView).offset(Adaptive.val(10))
            maker.width.height.equalTo(Adaptive.val(50))
        }
        
        productLabel.snp.remakeConstraints { (maker) in
            maker.centerX.equalTo(backView)
            maker.top.equalTo(productImageView.snp.bottom).offset(Adaptive.val(5))
        }
        
        mainButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(backView)
        }
    }
}

