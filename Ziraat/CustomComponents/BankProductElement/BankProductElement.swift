//
//  BankProductElement.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class BankProductElement: UIView, BankProductElementViewInstaller {
    var backView: UIView!
    var productImageView: UIImageView!
    var productLabel: UILabel!
    var mainButton: UIButton!    
    var mainView: UIView { self }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
    
    static func systemView(title: String, image: UIImage) -> BankProductElement {
        let view = BankProductElement(frame: .zero)
        view.productLabel.text = title
        view.productImageView.image = image
        return view
    }
}
