//
//  PinKeyboardInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit


protocol PinKeyboardInstaller: ViewInstaller {
    var buttonStackView: UIStackView! { get set }
    var firstRowStackView: UIStackView! { get set }
    var oneButton: UIButton! { get set }
    var twoButton: UIButton! { get set }
    var threeButton: UIButton! { get set }
    var secondRowStackView: UIStackView! { get set }
    var fourButton: UIButton! { get set }
    var fiveButton: UIButton! { get set }
    var sixButton: UIButton! { get set }
    var thirdRowStackView: UIStackView! { get set }
    var sevenButton: UIButton! { get set }
    var eightButton: UIButton! { get set }
    var nineButton: UIButton! { get set }
    var forthStackView: UIStackView! { get set }
    var noneButton: UIButton! { get set }
    var zeroButton: UIButton! { get set }
    var deleteButton: UIButton! { get set }
}

extension PinKeyboardInstaller {
    func initSubviews() {
        buttonStackView = UIStackView()
        buttonStackView.axis = .vertical
        buttonStackView.distribution = .equalSpacing
        buttonStackView.spacing = Adaptive.val(23.5)
        
        firstRowStackView = UIStackView()
        firstRowStackView.axis = .horizontal
        firstRowStackView.distribution = .equalSpacing
        firstRowStackView.spacing = Adaptive.val(22.5)

        oneButton = UIButton()
        oneButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        oneButton.setTitle("1", for: .normal)
        
        twoButton = UIButton()
        twoButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        twoButton.setTitle("2", for: .normal)

        threeButton = UIButton()
        threeButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        threeButton.setTitle("3", for: .normal)

        secondRowStackView = UIStackView()
        secondRowStackView.axis = .horizontal
        secondRowStackView.distribution = .equalSpacing
        secondRowStackView.spacing = Adaptive.val(22.5)
        
        fourButton = UIButton()
        fourButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        fourButton.setTitle("4", for: .normal)

        fiveButton = UIButton()
        fiveButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        fiveButton.setTitle("5", for: .normal)

        sixButton = UIButton()
        sixButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        sixButton.setTitle("6", for: .normal)

        thirdRowStackView = UIStackView()
        thirdRowStackView.axis = .horizontal
        thirdRowStackView.distribution = .equalSpacing
        thirdRowStackView.spacing = Adaptive.val(22.5)
        
        sevenButton = UIButton()
        sevenButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        sevenButton.setTitle("7", for: .normal)
        
        eightButton = UIButton()
        eightButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        eightButton.setTitle("8", for: .normal)
        
        nineButton = UIButton()
        nineButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        nineButton.setTitle("9", for: .normal)

        forthStackView = UIStackView()
        forthStackView.axis = .horizontal
        forthStackView.distribution = .fill
        forthStackView.alignment = .trailing
        forthStackView.spacing = Adaptive.val(22.5)
        
        noneButton = UIButton()
        noneButton.tintColor = .clear
        
        zeroButton = UIButton()
        zeroButton.setBackgroundImage(UIImage(named: "btn_pin"), for: .normal)
        zeroButton.setTitle("0", for: .normal)
        
        deleteButton = UIButton()
        deleteButton.setImage(UIImage(named: "btn_arrow_delete"), for: .normal)
    }
    
    func embedSubviews() {
        mainView.addSubview(buttonStackView)
        
        buttonStackView.addArrangedSubview(firstRowStackView)
        firstRowStackView.addArrangedSubview(oneButton)
        firstRowStackView.addArrangedSubview(twoButton)
        firstRowStackView.addArrangedSubview(threeButton)
        
        buttonStackView.addArrangedSubview(secondRowStackView)
        secondRowStackView.addArrangedSubview(fourButton)
        secondRowStackView.addArrangedSubview(fiveButton)
        secondRowStackView.addArrangedSubview(sixButton)
        
        buttonStackView.addArrangedSubview(thirdRowStackView)
        thirdRowStackView.addArrangedSubview(sevenButton)
        thirdRowStackView.addArrangedSubview(eightButton)
        thirdRowStackView.addArrangedSubview(nineButton)
        
        buttonStackView.addArrangedSubview(forthStackView)
        forthStackView.addArrangedSubview(zeroButton)
        forthStackView.addArrangedSubview(deleteButton)
    }
    
    func addSubviewsConstraints() {
        buttonStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(25))
            maker.centerX.equalToSuperview()
            maker.height.equalTo(Adaptive.val(430.5))
        }
        
        oneButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        twoButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        threeButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        fourButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        fiveButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        sixButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        sevenButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        eightButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        nineButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
        
        zeroButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
            maker.leading.equalTo(buttonStackView.snp.leading).offset(Adaptive.val(112.5))
        }
        
        deleteButton.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(90))
        }
    }
}

struct PinKeyboardViewSize {
    struct Indicator {
        static let size = Adaptive.val(40)
    }
}
