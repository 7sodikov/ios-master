//
//  PinSettingsNumberCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol PinSettingsNumberCellInstaller: ViewInstaller {
    var numberImageView: UIButton! { get set }
}

extension PinSettingsNumberCellInstaller {
    func initSubviews() {
        mainView.backgroundColor = .clear
        
        numberImageView = UIButton()
//        numberImageView.setImage(UIImage(named: "btn_pin"), for: .normal)
        numberImageView.setTitleColor(.white, for: .normal)
        numberImageView.setTitleColor(ColorConstants.highlightedGray, for: .selected)
        numberImageView.titleLabel?.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(45))
        numberImageView.setBackgroundColor(.red, for: .selected)
        
//        numberLabel = UILabel()
//        numberLabel.font = EZFontType.regular.sfProDisplay(size: Adaptive.val(45))
//        numberLabel.textColor = .white
//        numberLabel.textAlignment = .center
//        numberLabel.isUserInteractionEnabled = false
        
    }
    
    func embedSubviews() {
        mainView.addSubview(numberImageView)
    }
    
    func addSubviewsConstraints() {
        numberImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
            maker.height.width.equalTo(Adaptive.val(90))
        }
        
//        numberLabel.snp.remakeConstraints { (maker) in
//            maker.top.equalTo(numberImageView.snp.top).offset(Size.topBottom)
//            maker.bottom.equalTo(numberImageView.snp.bottom).offset(-Size.topBottom)
//            maker.leading.equalTo(numberImageView.snp.leading).offset(Size.leftRight)
//            maker.trailing.equalTo(numberImageView.snp.trailing).offset(-Size.leftRight)
//        }
    }
}

fileprivate struct Size {
    static let topBottom = Adaptive.val(16)
    static let leftRight = Adaptive.val(29)
}

