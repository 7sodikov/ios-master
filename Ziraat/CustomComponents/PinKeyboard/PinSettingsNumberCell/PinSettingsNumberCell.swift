//
//  PinSettingsNumberCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PinSettingsNumberCellDelegate: class {
    func numberButtonClicked()
}

class PinSettingsNumberCell: UIView, PinSettingsNumberCellInstaller {
    var numberImageView: UIButton!
    var mainView: UIView { self }
    
    weak var delegate: PinSettingsNumberCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        
    }

    @objc private func numberButtonClicked(_ sender: UIButton) {
        delegate?.numberButtonClicked()
    }
}
