//
//  PinKeyboard.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PinKeyboard: UIView, PinKeyboardInstaller {
    var buttonStackView: UIStackView!
    
    var firstRowStackView: UIStackView!
    
    var secondRowStackView: UIStackView!
    
    var thirdRowStackView: UIStackView!
    
    var forthStackView: UIStackView!
    
    var oneButton: UIButton!
    
    var twoButton: UIButton!
    
    var threeButton: UIButton!
    
    var fourButton: UIButton!
    
    var fiveButton: UIButton!
    
    var sixButton: UIButton!
    
    var sevenButton: UIButton!
    
    var eightButton: UIButton!
    
    var nineButton: UIButton!
    
    var noneButton: UIButton!
    
    var zeroButton: UIButton!
    
    var deleteButton: UIButton!
    
    var mainView: UIView { self }
    var reloadButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        
    }
}
