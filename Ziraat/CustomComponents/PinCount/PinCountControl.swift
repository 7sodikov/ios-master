//
//  PinCountControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

class PinCountControl: UIView, PinCountControlInstaller {
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var mainView: UIView { self }
    
    var model = PinCountControlModel()
    
    var itemsCount: Int {
        get {
            model.itemsCount
        }
        set {
            model.itemsCount = newValue
            collectionView.reloadData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension PinCountControl: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PinCountCell.self), for: indexPath) as! PinCountCell
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return PinControlSize.Cell.size
    }
    
//    https://stackoverflow.com/a/37065909/845345
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = PinControlSize.Cell.size.width * CGFloat(model.itemsCount)
        let totalSpacingWidth = PinControlSize.interCellSpace * CGFloat(model.itemsCount - 1)
        
        let leftInset = (self.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
}
