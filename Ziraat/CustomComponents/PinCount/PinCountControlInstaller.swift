//
//  PinCountControlInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol PinCountControlInstaller: ViewInstaller {
//    var lineStackView: UIStackView! { get set }
    var layout: UICollectionViewFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
}

extension PinCountControlInstaller {
    func initSubviews() {
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = .zero
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = PinControlSize.interCellSpace
        layout.minimumLineSpacing = PinControlSize.interCellSpace
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.bounces = true;
        collectionView.alwaysBounceHorizontal = true
        collectionView.register(PinCountCell.self, forCellWithReuseIdentifier: String(describing: PinCountCell.self))
    }
    
    func embedSubviews() {
        mainView.addSubview(collectionView)
    }
    
    func addSubviewsConstraints() {
        collectionView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct PinControlSize {
    static let interCellSpace = Adaptive.val(3)
    struct Cell {
        static let size = Adaptive.size(14, 14)
    }
}
