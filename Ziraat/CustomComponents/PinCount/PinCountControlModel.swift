//
//  PinCountControlModel.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class PinCountControlModel {
    private(set) var items: [Bool] = []
    
    var itemsCount: Int {
        get {
            return items.count
        }
        set {
            for _ in 0..<newValue {
                items.append(false)
            }
        }
    }
    
}
