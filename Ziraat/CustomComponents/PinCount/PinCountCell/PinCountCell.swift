//
//  PinCountCell.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class PinCountCell: UICollectionViewCell, PinCountCellInstaller {
    var backImageview: UIImageView!
    var mainView: UIView { self }
    var bgView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
