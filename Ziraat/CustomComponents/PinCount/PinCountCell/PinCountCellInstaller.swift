//
//  PinCountCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol PinCountCellInstaller: ViewInstaller {
    var bgView: UIView! { get set }
    var backImageview: UIImageView! { get set }
}

extension PinCountCellInstaller {
    func initSubviews() {
        bgView = UIView()
        
        backImageview = UIImageView()
        backImageview.image = UIImage(named: "img_pin")
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(backImageview)
    }
    
    func addSubviewsConstraints() {
        backImageview.snp.remakeConstraints { (maker) in
            maker.leading.trailing.bottom.equalToSuperview()
            maker.top.equalTo(Adaptive.val(1))
        }
    }
}

fileprivate struct Size {
    static let widthHeight = Adaptive.val(13.5)
}
