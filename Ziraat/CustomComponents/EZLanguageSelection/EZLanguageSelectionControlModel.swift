//
//  EZLanguageSelectionControlModel.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/4/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class EZLanguageSelectionControlModel {
    var nameLabel: String!
    
    init(name: String) {
        self.nameLabel = name
    }
}
