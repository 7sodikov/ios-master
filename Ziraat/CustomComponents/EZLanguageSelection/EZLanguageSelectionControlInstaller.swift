//
//  EZLanguageSelectionControlInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/4/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZLanguageSelectionControlInstaller: ViewInstaller {
    var stackView: UIStackView! { get set }
}

extension EZLanguageSelectionControlInstaller {
    func initSubviews() {
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Adaptive.val(9)
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
        
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
            maker.height.equalTo(Adaptive.val(26))
        }
    }
}

