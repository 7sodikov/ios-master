//
//  EZLanguageSelectionControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/4/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZLanguageSelectionControlDelegate: class {
    func languageSelected(at language: Language)
}

class EZLanguageSelectionControl: UIControl, EZLanguageSelectionControlInstaller {
    var mainView: UIView { self }
    var stackView: UIStackView!
    weak var delegate: EZLanguageSelectionControlDelegate?
    
    var selectedIndex: Int = 0 {
        didSet {
            guard selectedIndex < stackView.arrangedSubviews.count else {
                return
            }
            
            for i in 0..<stackView.arrangedSubviews.count {
                let btn = stackView.arrangedSubviews[i] as? UIButton
                btn?.isSelected = (i == selectedIndex)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func addSegment(with title: String, language: Language) {
        let btn = createSegmentButton(with: title, language: language)
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        btn.tag = stackView.arrangedSubviews.count
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButton(with title: String, language: Language) -> UIButton {
        let comp = CustomButton(frame: .zero)
        comp.href = language
        comp.setBackgroundImage(UIImage(named: "btn_lang_unselected"), for: .normal)
        comp.setBackgroundImage(UIImage(named: "btn_lang_selected"), for: .selected)
        comp.setTitle(title, for: .normal)
        comp.layer.cornerRadius = Adaptive.val(6)
        comp.clipsToBounds = true
        comp.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))
        comp.setTitleColor(.black, for: .normal)
        comp.setTitleColor(.white, for: .selected)
        comp.addTarget(self, action: #selector(handleClick(_:)), for: .touchUpInside)
        
        return comp
    }
    
    @objc func handleClick(_ sender: CustomButton) {
      if let href = sender.href{
        delegate?.languageSelected(at: href)
      }
    }

    override var isSelected: Bool {
        didSet {
            layer.borderColor = isSelected ? UIColor.clear.cgColor : UIColor.black.cgColor
        }
    }
    
    @objc private func btnClicked(_ sender: UIButton) {
        selectedIndex = sender.tag
        self.sendActions(for: .valueChanged)
    }
}

class CustomButton: UIButton {
  var href: Language?
}


