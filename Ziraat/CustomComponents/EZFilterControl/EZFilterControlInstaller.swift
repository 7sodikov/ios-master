//
//  EZFilterControlInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZFilterControlInstaller: ViewInstaller {
    var stackView: UIStackView! { get set }
}

extension EZFilterControlInstaller {
    func initSubviews() {
        stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = Adaptive.val(2)
        stackView.distribution = .fillEqually
    }
    
    func embedSubviews() {
        mainView.addSubview(stackView)
    }
    
    func addSubviewsConstraints() {
        stackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
            maker.height.equalTo(Adaptive.val(41))
        }
    }
}

fileprivate struct Size {
    
}
