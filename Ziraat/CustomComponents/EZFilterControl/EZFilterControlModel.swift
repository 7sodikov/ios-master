//
//  EZFilterControlModel.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class EZFilterControlModel {
    var nameLabel: String!
    
    init(name: String) {
        self.nameLabel = name
    }
}
