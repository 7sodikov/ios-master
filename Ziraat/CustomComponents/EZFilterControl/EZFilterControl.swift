//
//  EZFilterControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/20/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class EZFilterControl: UIControl, EZFilterControlInstaller {
    var mainView: UIView { self }
    var stackView: UIStackView!
    
    var selectedIndex: Int = 0 {
        didSet {
            guard selectedIndex < stackView.arrangedSubviews.count else {
                return
            }
            
            for i in 0..<stackView.arrangedSubviews.count {
                let btn = stackView.arrangedSubviews[i] as? UIButton
//                btn?.frame = CGRect(x: 0, y: 0, width: 90, height: 41)
                btn?.isSelected = (i == selectedIndex)
//                let lineView = UIView(frame: CGRect(x: 0, y: 41, width: btn!.frame.size.width, height: 2))
                if btn?.isSelected == true {
                    btn?.layer.borderWidth = 1
                    btn?.layer.borderColor = UIColor.red.cgColor
                } else {
                    btn?.layer.borderWidth = 0
                    btn?.layer.borderColor = UIColor.black.cgColor
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    func addSegment(with name: String) {
        let btn = createSegmentButton(with: name)
        btn.tag = stackView.arrangedSubviews.count
        btn.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
        stackView.addArrangedSubview(btn)
    }
    
    private func createSegmentButton(with name: String) -> UIButton {
        let comp = UIButton(frame: CGRect(x: 0, y: 0, width: Adaptive.val(90), height: Adaptive.val(41)))
        comp.clipsToBounds = true
        comp.setTitle(name, for: .normal)
        comp.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        comp.setTitleColor(.black, for: .normal)
        comp.setBackgroundColor(.clear, for: .normal)
        comp.setBackgroundColor(UIColor.black.withAlphaComponent(0.5), for: .highlighted)
        comp.setBackgroundColor(.clear, for: .selected)
        return comp
    }
    
    @objc private func btnClicked(_ sender: UIButton) {
        selectedIndex = sender.tag
        self.sendActions(for: .valueChanged)
    }
}


class FilterButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    static func systemButton(with title: String, cornerRadius: CGFloat) -> FilterButton {
        let btn = FilterButton(frame: CGRect(x: 0, y: 0, width: Adaptive.val(90), height: Adaptive.val(41)))
        return btn
    }
}
