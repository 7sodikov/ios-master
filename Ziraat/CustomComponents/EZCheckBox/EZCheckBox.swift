//
//  EZCheckBox.swift
//  Ziraat
//
//  Created by Shamsiddin on 3/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

class EZCheckBox: UIControl, EZCheckBoxViewInstaller {
    var mainView: UIView { self }
    var checkMarkImageCoverView: UIView!
    var checkMarkImageView: UIImageView!
    var titleLabel: UILabel!
    var widthHeightConstraint: Constraint!
    
    var sizeMultiplier: CGFloat = 1 {
        didSet {
            titleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17) * sizeMultiplier)
            widthHeightConstraint.update(offset: EZCheckBoxSize.checkMarkSize * sizeMultiplier)
            checkMarkImageCoverView.layer.cornerRadius = EZCheckBoxSize.checkMarkSize * sizeMultiplier * 0.2
            checkMarkImageCoverView.layer.borderWidth = sizeMultiplier * Adaptive.val(2)
        }
    }
    
    var isOn: Bool = false {
        didSet {
            if isOn {
                checkMarkImageView.image = checkMarkImage
            } else {
                checkMarkImageView.image = nil
            }
        }
    }
    
    var color: UIColor? {
        didSet {
            titleLabel.textColor = color
            if let color = color {
                checkMarkImage = UIImage(named: "img_check_mark")?.changeColor(color)
            }
        }
    }
    
    private var checkMarkImage = UIImage(named: "img_check_mark")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isOn = !isOn
        self.sendActions(for: .valueChanged)
    }
}
