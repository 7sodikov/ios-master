//
//  EZCheckBoxViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 3/5/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZCheckBoxViewInstaller: ViewInstaller {
    var checkMarkImageCoverView: UIView! { get set }
    var checkMarkImageView: UIImageView! { get set }
    var titleLabel: UILabel! { get set }
    var widthHeightConstraint: Constraint! { get set }
}

extension EZCheckBoxViewInstaller {
    func initSubviews() {
        checkMarkImageCoverView = UIView()
        checkMarkImageCoverView.layer.cornerRadius = EZCheckBoxSize.checkMarkSize * 0.2
        checkMarkImageCoverView.layer.masksToBounds = true
        checkMarkImageCoverView.layer.borderWidth = Adaptive.val(2)
        checkMarkImageCoverView.layer.borderColor = ColorConstants.separatorGrayLine.cgColor
        
        checkMarkImageView = UIImageView()
        checkMarkImageView.image = UIImage(named: "img_check_mark")
        checkMarkImageView.contentMode = .scaleAspectFit
        
        titleLabel = UILabel()
        titleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(17))
        titleLabel.textColor = .white
    }
    
    func embedSubviews() {
        mainView.addSubview(checkMarkImageCoverView)
        checkMarkImageCoverView.addSubview(checkMarkImageView)
        mainView.addSubview(titleLabel)
    }
    
    func addSubviewsConstraints() {
        checkMarkImageCoverView.snp.remakeConstraints { (maker) in
            maker.left.equalToSuperview()
            widthHeightConstraint = maker.width.height.equalTo(EZCheckBoxSize.checkMarkSize).constraint
            maker.centerY.equalToSuperview()
        }
        
        checkMarkImageView.snp.remakeConstraints { (maker) in
            maker.top.left.equalToSuperview().offset(Adaptive.val(4))
            maker.bottom.right.equalToSuperview().offset(-Adaptive.val(4))
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.left.equalTo(checkMarkImageCoverView.snp.right).offset(Adaptive.val(6))
            maker.right.equalToSuperview()
            maker.centerY.equalToSuperview()
        }
    }
}

struct EZCheckBoxSize {
    static let checkMarkSize = Adaptive.val(25)
}

