//
//  TextFieldView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

class TextFieldView: UIView {
    
    @IBOutlet private weak var textField: ClipboardControlledTextField!
    
    
    var shouldAllowWhiteSpaceFirst: Bool = true
    var previousValue = ""
    var maxCount:Int?
    var maxLength: Int = 9
    var userData:AnyObject?
        
    var amountFormat: Bool = false
        
    var text: String? {
            get {
                return self.textField.text
            } set (newValue) {
                self.textField.text = newValue
            }
        }
            
    var keyboardType: UIKeyboardType! {
            set(newValue) {
                self.textField.keyboardType = newValue
            } get {
                return self.textField.keyboardType
            }
        }
        
    var isSecure: Bool! {
            set(newValue) {
                self.textField.isSecureTextEntry = newValue
            } get {
                return self.textField.isSecureTextEntry
            }
        }
        
    var capitalize: UITextAutocapitalizationType! {
            set(newValue) {
                self.textField.autocapitalizationType = newValue
            } get {
                return self.textField.autocapitalizationType
            }
        }
        
    var autoCorrection: UITextAutocorrectionType! {
            set(newValue) {
                self.textField.autocorrectionType = newValue
            } get {
                return self.textField.autocorrectionType
            }
        }
        

        override func awakeFromNib() {
            super.awakeFromNib()
            
            
        }
        
        deinit {
            
        }

        
        @objc
        func setupViews() {
            textField.layer.cornerRadius = 30
            textField.setLeftPaddingPoints(15)
            textField.attributedPlaceholder = NSAttributedString(string: "Enter captcha", attributes: [
                .foregroundColor: UIColor.lightGray,
                .font: UIFont.boldSystemFont(ofSize: 17.0)
            ])
            textField.layer.masksToBounds = true
        }
        
    }
