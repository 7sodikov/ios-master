//
//  PageControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/16/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import Foundation

class PageControl: UIPageControl {

    override var numberOfPages: Int {
            didSet {
                self._updateIndicators()
            }
        }
        
        override var currentPage: Int {
            didSet {
                self._updateIndicators()
            }
        }

        private func _updateIndicators() {
            var indicators: [UIView] = []
            
            if #available(iOS 14.0, *) {
                indicators = self.subviews.first?.subviews.first?.subviews ?? []
            } else {
//                indicators = self.subviews
            }
            
            for (index, indicator) in indicators.enumerated() {
                let image = self.currentPage == index ? UIImage.init(named: "pc_selected") : UIImage.init(named: "pc_unselected")
                if let dot = indicator as? UIImageView {
                    dot.image = image
                } else {
                    let imageView = UIImageView.init(image: image)
                    indicator.addSubview(imageView)
                    // here you can add some constraints to fix the imageview to his superview

                }
            }
        }
}


