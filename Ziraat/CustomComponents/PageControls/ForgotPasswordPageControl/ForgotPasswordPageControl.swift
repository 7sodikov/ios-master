//
//  ForgotPasswordPageControl.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class ForgotPasswordPageControl: UIControl, ForgotPasswordPageControlViewInstaller {
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var mainView: UIView { self }
    var isBlack: Bool!
    
    var model = ForgotPasswordPageControlModel()
    
    var itemsCount: Int {
        get {
            model.itemsCount
        }
        set {
            model.itemsCount = newValue
            collectionView.reloadData()
        }
    }
    
    var currentItem: Int {
        get {
            model.currentItem
        }
        set {
            model.currentItem = newValue
            collectionView.reloadData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension ForgotPasswordPageControl: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ForgotPasswordPageControlCell.self), for: indexPath) as! ForgotPasswordPageControlCell
        let item = model.items[indexPath.row]
        cell.setup(for: indexPath.row,
                   isBlack: isBlack,
                   isChecked: item,
                   isCurrent: indexPath.row == model.currentItem)
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        (cell as? ForgotPasswordPageControlCell)?.becomeResponder()
//    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return ForgotPasswordPageControlSize.Cell.size
    }
    
//    https://stackoverflow.com/a/37065909/845345
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = ForgotPasswordPageControlSize.Cell.size.width * CGFloat(model.itemsCount)
        let totalSpacingWidth = ForgotPasswordPageControlSize.interCellSpace * CGFloat(model.itemsCount - 1)
        
        let leftInset = (self.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
}
