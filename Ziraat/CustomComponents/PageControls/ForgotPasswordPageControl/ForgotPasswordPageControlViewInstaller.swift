//
//  ForgotPasswordPageControlViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol ForgotPasswordPageControlViewInstaller: ViewInstaller {
//    var lineStackView: UIStackView! { get set }
    var layout: UICollectionViewFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
}

extension ForgotPasswordPageControlViewInstaller {
    func initSubviews() {
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = .zero
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = ForgotPasswordPageControlSize.interCellSpace
        layout.minimumLineSpacing = ForgotPasswordPageControlSize.interCellSpace
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.bounces = true;
        collectionView.alwaysBounceHorizontal = true
        collectionView.register(ForgotPasswordPageControlCell.self, forCellWithReuseIdentifier: String(describing: ForgotPasswordPageControlCell.self))
    }
    
    func embedSubviews() {
        mainView.addSubview(collectionView)
    }
    
    func addSubviewsConstraints() {
        collectionView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct ForgotPasswordPageControlSize {
    static let interCellSpace = Adaptive.val(20)
    struct Cell {
        static let size = Adaptive.size(45, 45)
    }
}
