//
//  ForgotPasswordPageControlModel.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class ForgotPasswordPageControlModel {
    private(set) var items: [Bool] = []
    
    var itemsCount: Int {
        get {
            return items.count
        }
        set {
            for _ in 0..<newValue {
                items.append(false)
            }
        }
    }
    
    var currentItem: Int {
        get {
            for i in 0..<items.count {
                let item = items[i]
                if !item {
                    return i
                }
            }
            return 0
        }
        set {
            for i in 0..<items.count {
                items[i] = !(i >= newValue)
            }
        }
    }
    
}
