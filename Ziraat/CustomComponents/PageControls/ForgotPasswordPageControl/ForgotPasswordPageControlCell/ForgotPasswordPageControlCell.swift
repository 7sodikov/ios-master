//
//  ForgotPasswordPageControlCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class ForgotPasswordPageControlCell: UICollectionViewCell, ForgotPasswordPageControlCellViewInstaller {
    var mainView: UIView { contentView }
    var bgView: UIView!
    var checkImageView: UIImageView!
    var pageNumberLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(for index: Int, isBlack: Bool, isChecked: Bool, isCurrent: Bool) {
        pageNumberLabel.text = "\(index + 1)"
        
        if isChecked {
            if isBlack {
                bgView.backgroundColor = ColorConstants.gray
            } else {
                bgView.backgroundColor = .swampyGreen
            }
            bgView.layer.borderColor = UIColor.clear.cgColor
            checkImageView.isHidden = false
            pageNumberLabel.isHidden = true
        } else {
            checkImageView.isHidden = true
            pageNumberLabel.isHidden = false
            
            if isCurrent {
                if isBlack {
                    bgView.backgroundColor = ColorConstants.highlightedGray
                    bgView.layer.borderColor = UIColor.clear.cgColor
                    pageNumberLabel.textColor = .white
                } else {
                    bgView.backgroundColor = ColorConstants.mainRed
                    bgView.layer.borderColor = UIColor.clear.cgColor
                    pageNumberLabel.textColor = .white
                }
                
            } else {
                if isBlack {
                    bgView.backgroundColor = .white
                    bgView.layer.borderColor = ColorConstants.gray.cgColor
                    pageNumberLabel.textColor = ColorConstants.gray
                } else {
                    bgView.backgroundColor = .clear
                    bgView.layer.borderColor = UIColor.dashDarkBlue.cgColor
                    pageNumberLabel.textColor = .dashDarkBlue
                }
            }
        }
    }
}
