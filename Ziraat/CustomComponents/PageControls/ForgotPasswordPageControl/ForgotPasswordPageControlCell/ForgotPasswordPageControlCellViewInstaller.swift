//
//  ForgotPasswordPageControlCellViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/31/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol ForgotPasswordPageControlCellViewInstaller: ViewInstaller {
    var bgView: UIView! { get set }
    var checkImageView: UIImageView! { get set }
    var pageNumberLabel: UILabel! { get set }
}

extension ForgotPasswordPageControlCellViewInstaller {
    func initSubviews() {
        bgView = UIView()
        bgView.layer.cornerRadius = ForgotPasswordPageControlSize.Cell.size.height/2
        bgView.layer.borderWidth = Adaptive.val(1)
        bgView.layer.borderColor = UIColor.clear.cgColor
        
        checkImageView = UIImageView()
        checkImageView.image = UIImage(named: "img_check_mark")
        
        pageNumberLabel = UILabel()
        pageNumberLabel.textAlignment = .center
    }
    
    func embedSubviews() {
        mainView.addSubview(bgView)
        mainView.addSubview(checkImageView)
        mainView.addSubview(pageNumberLabel)
    }
    
    func addSubviewsConstraints() {
        bgView.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.height.equalTo(ForgotPasswordPageControlSize.Cell.size.height)
        }
        
        checkImageView.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.height.equalTo(ForgotPasswordPageControlSize.Cell.size.height * 0.4)
        }
        
        pageNumberLabel.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.height.equalTo(ForgotPasswordPageControlSize.Cell.size.height * 0.8)
        }
    }
}
