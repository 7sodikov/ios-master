//
//  P2PTransactionPageControlViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol P2PTransactionPageControlViewInstaller: ViewInstaller {
    var layout: UICollectionViewFlowLayout! { get set }
    var collectionView: UICollectionView! { get set }
}

extension P2PTransactionPageControlViewInstaller {
    func initSubviews() {
        layout = UICollectionViewFlowLayout()
        layout.sectionInset = .zero
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = P2PTransactionPageControlSize.interCellSpace
        layout.minimumLineSpacing = P2PTransactionPageControlSize.interCellSpace
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.bounces = true;
        collectionView.alwaysBounceHorizontal = true
        collectionView.register(P2PTransactionPageControlCell.self, forCellWithReuseIdentifier: String(describing: P2PTransactionPageControlCell.self))
    }
    
    func embedSubviews() {
        mainView.addSubview(collectionView)
    }
    
    func addSubviewsConstraints() {
        collectionView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct P2PTransactionPageControlSize {
    static let interCellSpace = Adaptive.val(2)
    struct Cell {
        static let height = Adaptive.val(44)
    }
}
