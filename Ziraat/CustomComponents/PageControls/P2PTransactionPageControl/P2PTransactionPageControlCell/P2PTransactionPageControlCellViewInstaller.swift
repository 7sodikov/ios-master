//
//  P2PTransactionPageControlCellViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import SnapKit

protocol P2PTransactionPageControlCellViewInstaller: ViewInstaller {
    var circleView: UIView! { get set }
    var pageNumberLabel: UILabel! { get set }
    var checkmarkImageView: UIImageView! { get set }
    var pageTitleLabel: UILabel! { get set }
}

extension P2PTransactionPageControlCellViewInstaller {
    func initSubviews() {
        mainView.layer.cornerRadius = Adaptive.val(9)
        mainView.backgroundColor = .white
        
        circleView = UIView()
        circleView.layer.cornerRadius = Size.circleViewRadius
        circleView.layer.borderWidth = 1
        
        pageNumberLabel = UILabel()
        pageNumberLabel.textAlignment = .center
        pageNumberLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(16.5))
        
        checkmarkImageView = UIImageView()
        checkmarkImageView.image = UIImage(named: "img_check_mark")
        checkmarkImageView.isHidden = true
        
        pageTitleLabel = UILabel()
        pageTitleLabel.textAlignment = .left
        pageTitleLabel.numberOfLines = 2
        pageTitleLabel.font = EZFontType.regular.sfProText(size: Adaptive.val(15))
    }
    
    func embedSubviews() {
        mainView.addSubview(circleView)
        circleView.addSubview(pageNumberLabel)
        circleView.addSubview(checkmarkImageView)
        mainView.addSubview(pageTitleLabel)
    }
    
    func addSubviewsConstraints() {
        circleView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(2 * Size.circleViewRadius)
            maker.centerY.equalToSuperview()
            maker.left.equalTo(Adaptive.val(10))
        }
        
        pageNumberLabel.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
        checkmarkImageView.snp.remakeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.size.equalTo(circleView.snp.size).multipliedBy(0.5)
        }
        
        pageTitleLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalToSuperview()
            maker.left.equalTo(circleView.snp.right).offset(Adaptive.val(8))
            maker.right.equalToSuperview().offset(Adaptive.val(10))
        }
    }
}

fileprivate struct Size {
    static let circleViewRadius = Adaptive.val(12.5)
}
