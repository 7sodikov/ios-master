//
//  P2PTransactionPageControlCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class P2PTransactionPageControlCell: UICollectionViewCell, P2PTransactionPageControlCellViewInstaller {
    var mainView: UIView { contentView }
    var circleView: UIView!
    var pageNumberLabel: UILabel!
    var checkmarkImageView: UIImageView!
    var pageTitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setup(for itemModel: P2PTransactionPageControl.ItemModel,
               isChecked: Bool,
               isCurrent: Bool) {
        pageNumberLabel.text = itemModel.pageNumber
        pageTitleLabel.text = itemModel.title
        pageNumberLabel.isHidden = isChecked
        checkmarkImageView.isHidden = !isChecked
        
        if isChecked {
            mainView.backgroundColor = .white
            circleView.backgroundColor = ColorConstants.mainRed
            circleView.layer.borderColor = ColorConstants.mainRed.cgColor
            pageTitleLabel.textColor = ColorConstants.gray
            
        } else {
            if isCurrent {
                mainView.backgroundColor = ColorConstants.mainRed
                circleView.backgroundColor = ColorConstants.mainRed
                circleView.layer.borderColor = UIColor.white.cgColor
                pageNumberLabel.textColor = .white
                pageTitleLabel.textColor = .white
            } else {
                mainView.backgroundColor = .white
                circleView.backgroundColor = .white
                circleView.layer.borderColor = ColorConstants.mainRed.cgColor
                pageNumberLabel.textColor = ColorConstants.mainRed
                pageTitleLabel.textColor = ColorConstants.gray
            }
        }
    }
}
