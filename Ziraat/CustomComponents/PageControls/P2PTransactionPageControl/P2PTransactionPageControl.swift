//
//  P2PTransactionPageControl.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class P2PTransactionPageControl: UIControl, P2PTransactionPageControlViewInstaller {
    typealias ItemModel = (pageNumber: String, title: String)
    
    var mainView: UIView { self }
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    
    private(set) var items: [ItemModel] = []
    
    var currentItem: Int = 0 {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var cellSize: CGSize {
        return CGSize(width: (self.frame.width - P2PTransactionPageControlSize.interCellSpace)/2,
                      height: P2PTransactionPageControlSize.Cell.height)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func addItem(itemModel: ItemModel) {
        items.append(itemModel)
        collectionView.reloadData()
    }
}

extension P2PTransactionPageControl: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: P2PTransactionPageControlCell.self), for: indexPath) as! P2PTransactionPageControlCell
        let item = items[indexPath.row]
        
        cell.setup(for: item,
                   isChecked: indexPath.row < currentItem,
                   isCurrent: indexPath.row == currentItem)
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
}
