//
//  AdditionalTimeVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AdditionalTimeVCProtocol: class {
    
}

class AdditionalTimeVC: UIViewController, AdditionalTimeViewInstaller {
    var mainView: UIView { view }
    var presenter: AdditionalTimePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
    }
}

extension AdditionalTimeVC: AdditionalTimeVCProtocol {
    
}
