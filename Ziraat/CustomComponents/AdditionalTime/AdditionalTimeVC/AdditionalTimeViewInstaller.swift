//
//  AdditionalTimeViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AdditionalTimeViewInstaller: ViewInstaller {
    // declare your UI elements here
}

extension AdditionalTimeViewInstaller {
    func initSubviews() {
        // init your UI elements and set their initial setups here
    }
    
    func embedSubviews() {
        // add your UI elements as a subview to the view
    }
    
    func addSubviewsConstraints() {
        // Constraints of your UI elements goes here
    }
}

fileprivate struct Size {
    
}
