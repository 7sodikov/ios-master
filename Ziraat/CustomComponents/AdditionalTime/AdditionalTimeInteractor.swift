//
//  AdditionalTimeInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AdditionalTimeInteractorProtocol: class {
    
}

class AdditionalTimeInteractor: AdditionalTimeInteractorProtocol {
    weak var presenter: AdditionalTimeInteractorToPresenterProtocol!
    
}
