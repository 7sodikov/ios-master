//
//  AdditionalTimePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol AdditionalTimePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: AdditionalTimeVM { get }
}

protocol AdditionalTimeInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class AdditionalTimePresenter: AdditionalTimePresenterProtocol {
    weak var view: AdditionalTimeVCProtocol!
    var interactor: AdditionalTimeInteractorProtocol!
    var router: AdditionalTimeRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = AdditionalTimeVM()
    
    // Private property and methods
    
}

extension AdditionalTimePresenter: AdditionalTimeInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
