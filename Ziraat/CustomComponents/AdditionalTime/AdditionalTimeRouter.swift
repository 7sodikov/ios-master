//
//  AdditionalTimeRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol AdditionalTimeRouterProtocol: class {
    static func createModule() -> UIViewController
}

class AdditionalTimeRouter: AdditionalTimeRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = AdditionalTimeVC()
        let presenter = AdditionalTimePresenter()
        let interactor = AdditionalTimeInteractor()
        let router = AdditionalTimeRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
