//
//  PageController.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PageControllerDelegate: AnyObject {
    func pageController(didScrollToOffsetX offsetX: CGFloat, percent: CGFloat)
    func pageController(didMoveToIndex index: Int)
    func pageController(deriction: Int, didScrollToOffsetX offsetX: CGFloat, percent: CGFloat)
}

extension PageControllerDelegate {
    func pageController(deriction: Int, didScrollToOffsetX offsetX: CGFloat, percent: CGFloat) {}
}

class PageController: UIPageViewController {
    private var pages: [UIViewController] = []
    weak var pageControllerDelegate: PageControllerDelegate?
    private var startOffset: CGFloat = 0
    
    private var currentPageIndex: Int {
        guard let viewController = viewControllers?.last else {
            return 0
        }
        return pages.firstIndex(of: viewController) ?? 0
    }
    private var currentPageAtStart: Int = 0
    
    func open(in parentController: UIViewController, parentView: UIView, pages: [UIViewController], isSwipeEnabled: Bool = true) {
        self.pages = pages
        
        // 1. Set its datasource and delegate methods
        if isSwipeEnabled {
            dataSource = self
        }
        
        delegate = self
        
        // 2. Show view controller with initial page - page zero
        setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
        parentController.addChild(self)
        
        // 3. Add to holder view
        parentView.addSubview(self.view)
        self.didMove(toParent: parentController)
        
        // 4. Pin to super view - (holder view)
        view.snp.remakeConstraints { (maker) in
            //MARK: - do not leave
            maker.top.equalToSuperview()
            maker.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func scroll(to pageIndex: Int, direction: UIPageViewController.NavigationDirection) {
        if pageIndex >= pages.count {
            return
        }
        
        let controller = pages[pageIndex]
        setViewControllers([controller], direction: direction, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let scrollView = view.subviews.filter { $0 is UIScrollView }.first as! UIScrollView
        scrollView.delegate = self
    }
    
    private func getNextPage(after page: UIViewController) -> UIViewController? {
        let currentPageIndex = pages.firstIndex(of: page) ?? 0
        
        if currentPageIndex < pages.count - 1 { // is not last page
            return pages[currentPageIndex + 1]
        } else {
            return nil
        }
    }
    
    private func getPreviousPage(before page: UIViewController) -> UIViewController? {
        let currentPageIndex = pages.firstIndex(of: page) ?? 0
        
        if currentPageIndex > 0 {
            return pages[currentPageIndex - 1]
        } else {
            return nil
        }
    }
}

extension PageController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return getPreviousPage(before: viewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return getNextPage(after: viewController)
    }
    
    // can be edited
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
//        pageViewController.view.isUserInteractionEnabled = false
        
//        guard let ctr = viewControllers?.last else {
//            return
//        }
//
//        let currentIndex = pages.firstIndex(of: ctr) ?? 0
//        pageControllerDelegate?.pageController(didMoveToIndex: currentIndex)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        
        //MARK:- do not left
        guard let ctr = viewControllers?.last else {
            return
        }
        // can be edited
//        pageViewController.view.isUserInteractionEnabled = true

        let currentIndex = pages.firstIndex(of: ctr) ?? 0
        pageControllerDelegate?.pageController(didMoveToIndex: currentIndex)
    }
}

extension PageController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        currentPageAtStart = currentPageIndex
        startOffset = scrollView.contentOffset.x
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        print("\(currentPageIndex) - scrollViewDidEndDecelerating")
//    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var direction = 0 //scroll stopped
        
        if startOffset < scrollView.contentOffset.x {
            direction = 1 //going right
        }else if startOffset > scrollView.contentOffset.x {
            direction = -1 //going left
        }
        
        let positionFromStartOfCurrentPage = abs(startOffset - scrollView.contentOffset.x)
        let percent = positionFromStartOfCurrentPage /  self.view.frame.width
        
        // you can decide what to do with scroll
        
        let contentWidth = CGFloat(pages.count) * view.frame.width
        let offset = CGFloat(currentPageAtStart) * view.frame.width + CGFloat(direction) * percent * view.frame.width
        if direction == 0 {
            return
        }
        let cws = String(format: "%0.3f", contentWidth)
        let ofs = String(format: "%0.3f", offset)
        let ps = String(format: "%0.3f", percent)
         print("\(direction) - \(cws) - \(currentPageAtStart) - \(ofs) - \(ps)")
        pageControllerDelegate?.pageController(didScrollToOffsetX: offset, percent: offset/contentWidth)
        
        pageControllerDelegate?
            .pageController(deriction: direction, didScrollToOffsetX: offset, percent: offset/contentWidth)
    }
}

extension UIPageViewController {

    func goToNextPage() {
       guard let currentViewController = self.viewControllers?.first else { return }
       guard let nextViewController = dataSource?.pageViewController( self, viewControllerAfter: currentViewController ) else { return }
       setViewControllers([nextViewController], direction: .forward, animated: false, completion: nil)
    }

    @objc func goToPreviousPage() {
       guard let currentViewController = self.viewControllers?.first else { return }
       guard let previousViewController = dataSource?.pageViewController( self, viewControllerBefore: currentViewController ) else { return }
       setViewControllers([previousViewController], direction: .reverse, animated: false, completion: nil)
    }

}

class LabelBanner: UIView {
    
    enum Direction: Int {
        case leftToRight
        case rightToLeft
    }
    
    var label: UILabel!
    
    init() {
        super.init(frame: .zero)
        setupSubveiws()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubveiws() {
        label = UILabel()
        label.font = .gothamNarrow(size: 16, .bold)
    }
    
    func didmove(direction: Direction, percent: CGFloat) {
        
    }
}
