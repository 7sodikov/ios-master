//
//  ClipboardControlledTextView.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class ClipboardControlledTextView: UITextView {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
