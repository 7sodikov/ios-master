//
//  ClipboardControlledTextField.swift
//  Ziraat
//
//  Created by Shamsiddin on 2/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class ClipboardControlledTextField: UITextField {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        return action == #selector(UIResponderStandardEditActions.cut) || action == #selector(UIResponderStandardEditActions.copy)
        return false
    }
}
