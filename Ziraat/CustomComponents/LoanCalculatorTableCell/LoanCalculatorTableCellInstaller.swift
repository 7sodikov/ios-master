//
//  LoanCalculatorTableCellInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

protocol LoanCalculatorTableCellInstaller: ViewInstaller {
    var titleLabel: UILabel! { get set }
    var textField: LoginTextField! { get set }
    var subtitleLabel: UILabel! { get set }
    var indicator: MDCActivityIndicator! { get set }
}

extension LoanCalculatorTableCellInstaller {
    func initSubviews() {
        titleLabel = UILabel()
        titleLabel.font = EZFontType.regular.sfProText(size: 16.5)
        titleLabel.textColor = .black
        
        textField = LoginTextField(with: "", cornerRadius: LoanCalculatorTableCellSize.TextField.height/2, isSecureTextEntry: false)
        
        subtitleLabel = UILabel()
        subtitleLabel.font = EZFontType.regular.sfProText(size: 12)
        subtitleLabel.textColor = .black
        subtitleLabel.isHidden = true
        
        indicator = MDCActivityIndicator(frame: CGRect(x: 0, y: 0, width: LoanCalculatorTableCellSize.TextField.height, height: LoanCalculatorTableCellSize.TextField.height/2))
        indicator.cycleColors = [.lightGray]
        indicator.tintColor = .lightGray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.radius = (LoanCalculatorTableCellSize.TextField.height/2) * 0.45
    }
    
    func embedSubviews() {
        mainView.addSubview(titleLabel)
        mainView.addSubview(textField)
        mainView.addSubview(subtitleLabel)
    }
    
    func addSubviewsConstraints() {
        titleLabel.snp.remakeConstraints { (maker) in
            let leftRight = ApplicationSize.paddingLeftRight + LoanCalculatorTableCellSize.TextField.height/2
            maker.top.equalTo(ApplicationSize.paddingLeftRight)
            maker.left.equalTo(leftRight)
            maker.right.equalTo(-leftRight)
        }
        
        textField.snp.remakeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(Adaptive.val(5))
            maker.left.equalTo(ApplicationSize.paddingLeftRight)
            maker.right.equalTo(-ApplicationSize.paddingLeftRight)
            maker.height.equalTo(LoanCalculatorTableCellSize.TextField.height)
        }
        
        subtitleLabel.snp.remakeConstraints { (maker) in
            maker.left.right.equalTo(titleLabel)
            maker.top.equalTo(textField.snp.bottom).offset(Adaptive.val(3))
        }
    }
}

struct LoanCalculatorTableCellSize {
    struct TextField {
        static let height = Adaptive.val(54)
    }
}
