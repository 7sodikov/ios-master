//
//  LoanCalculatorTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/12/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

class LoanCalculatorTableCell: UITableViewCell, LoanCalculatorTableCellInstaller {
    var mainView: UIView { contentView }
    var titleLabel: UILabel!
    var textField: LoginTextField!
    var subtitleLabel: UILabel!
    var indicator: MDCActivityIndicator!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setPreloader() {
        textField.rightView = indicator
        textField.rightViewMode = .unlessEditing
    }
    
    var animateIndicator: Bool {
        get {
            indicator.isAnimating
        }
        set {
            if newValue {
                indicator.startAnimating()
            } else {
                indicator.stopAnimating()
            }
        }
    }
}
