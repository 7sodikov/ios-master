//
//  BottomSheet.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

class BottomSheet {
    let tag: Int
    let viewHeight: CGFloat
    let sections: [BottomSheetSection]
    weak var delegate: BottomSheetDelegate? = nil
    
    init(viewHeight: CGFloat, dataSource sections: [BottomSheetSection], with tag: Int, delegate: BottomSheetDelegate? = nil) {
        self.viewHeight = viewHeight
        self.sections = sections
        self.delegate = delegate
        self.tag = tag
    }
}

struct BottomSheetSection {
    
    var header: String
    var rows: [TableViewModel]
    
}
