//
//  BottomSheetViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/23/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import PanModal

protocol BottomSheetDelegate: AnyObject {
    func didSelect(item viewModel: TableViewModel, at indexPath: IndexPath, with tag: Int)
    func bottomSheet(didSelectCardAt indexPath: IndexPath, card: Any, with tag: Int)
    func panModalDidDismiss()
    func panModalWillDismiss()
}

class BottomSheetViewController: UIViewController, PanModalPresentable {
    
    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.tableFooterView = .init()
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    let bottomSheet: BottomSheet
    
    private var sections: [BottomSheetSection] { bottomSheet.sections }
    private var delegate: BottomSheetDelegate? { bottomSheet.delegate }
    

    var panScrollable: UIScrollView? { self.tableView }
    
    init(bottomSheet: BottomSheet) {
        self.bottomSheet = bottomSheet
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view.addSubview(tableView)
        tableView.constraint { (make) in
            make.top(16).bottom.horizontal.equalToSuperView()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        sections.map(\.rows).reduce(.init(), +).forEach { (vm) in
            tableView.register(vm.cellType)
        }
    }
    
    var longFormHeight: PanModalHeight {
        .contentHeight(bottomSheet.viewHeight)
    }
    
    func panModalDidDismiss() {
        delegate?.panModalDidDismiss()
    }
    
    func panModalWillDismiss() {
        delegate?.panModalWillDismiss()
    }
}

extension BottomSheetViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sections[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = sections[indexPath.section].rows[indexPath.row]
        let cell = tableView.reusable(viewModel.cellType, for: indexPath)
        (cell as? CardsRouletteCell)?.cardsDelegate = self
        cell.set(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        delegate?.didSelect(item: sections[indexPath.section].rows[indexPath.row], at: indexPath, with: bottomSheet.tag)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !sections[section].header.isEmpty {
            let header = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64))
            header.backgroundColor = .white
            
            let label = UILabel()
            label.textColor = .buttonBlack
            label.font = .gothamNarrow(size: 18, .bold)
            label.text = sections[section].header
            
            header.addSubview(label)
            label.constraint { $0.horizontal(16).top(32).bottom.equalToSuperView() }
            
            return header
        }
        return .init()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        sections[section].header.isEmpty ? .zero : 64.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        0
    }
}

extension BottomSheetViewController: CardsRouletteDelegate {
    
    func cardsRoulette(didSelectCardAt indexPath: IndexPath, card: Any) {
        delegate?.bottomSheet(didSelectCardAt: indexPath, card: card, with: bottomSheet.tag)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension BottomSheetDelegate {
    
    func didSelect(item viewModel: TableViewModel, at indexPath: IndexPath, with tag: Int) {}
    func bottomSheet(didSelectCardAt indexPath: IndexPath, card: Any, with tag: Int) {}
    func panModalDidDismiss() {}
    func panModalWillDismiss() {}
}
