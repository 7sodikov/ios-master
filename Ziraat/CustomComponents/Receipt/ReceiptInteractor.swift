//
//  ReceiptInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ReceiptInteractorProtocol: class {
    
}

class ReceiptInteractor: ReceiptInteractorProtocol {
    weak var presenter: ReceiptInteractorToPresenterProtocol!
    
}
