//
//  ReceiptRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ReceiptRouterProtocol: class {
    static func createModule() -> UIViewController
}

class ReceiptRouter: ReceiptRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ReceiptVC()
        let presenter = ReceiptPresenter()
        let interactor = ReceiptInteractor()
        let router = ReceiptRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
