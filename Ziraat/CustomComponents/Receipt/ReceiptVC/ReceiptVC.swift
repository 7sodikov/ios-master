//
//  ReceiptVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import PDFKit

protocol ReceiptVCProtocol: class {
    
}

class ReceiptVC: BaseViewController, ReceiptViewInstaller {
    var receiptView: PDFView!
    var saveFileButton: NextButton!
    var backButton: NextButton!
    var mainView: UIView { view }
    
    var presenter: ReceiptPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
    }
    
    @objc private func saveFileButtonClicked() {
        
    }
    
    @objc private func backButtonClicked() {
        navigationController?.popViewControllerRetro()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
}

extension ReceiptVC: ReceiptVCProtocol {
    
}
