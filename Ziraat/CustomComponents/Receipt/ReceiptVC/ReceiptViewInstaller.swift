//
//  ReceiptViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import PDFKit

protocol ReceiptViewInstaller: ViewInstaller {
    var receiptView: PDFView! { get set }
    var saveFileButton: NextButton! { get set }
    var backButton: NextButton! { get set }
}

extension ReceiptViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .white
        
        receiptView = PDFView()
        
        saveFileButton = NextButton.systemButton(with: RS.btn_save_file.localized(), cornerRadius: Size.buttonHeight/2)
        saveFileButton.setBackgroundColor(.dashRed, for: .normal)
        
        backButton = NextButton.systemButton(with: RS.btn_back.localized(), cornerRadius: Size.buttonHeight/2)
        backButton.setBackgroundColor(.white, for: .normal)
        backButton.layer.borderWidth = Adaptive.val(1)
        backButton.layer.borderColor = ColorConstants.highlightedGray.cgColor
    }
    
    func embedSubviews() {
        mainView.addSubview(receiptView)
        mainView.addSubview(saveFileButton)
        mainView.addSubview(backButton)
    }
    
    func addSubviewsConstraints() {
        receiptView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(Adaptive.val(90))
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().inset(Size.paddingLeftRight)
            maker.height.equalTo(Adaptive.val(371))
        }
        
        saveFileButton.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().inset(Size.paddingLeftRight)
            maker.height.equalTo(Adaptive.val(50))
            maker.bottom.equalTo(backButton.snp.top).offset(Adaptive.val(15))
        }
        
        backButton.snp.remakeConstraints { (maker) in
            maker.leading.equalToSuperview().offset(Size.paddingLeftRight)
            maker.trailing.equalToSuperview().inset(Size.paddingLeftRight)
            maker.height.equalTo(Adaptive.val(50))
            maker.bottom.equalTo(Adaptive.val(53))
        }
    }
}

fileprivate struct Size {
    static var buttonHeight = Adaptive.val(50)
    static var paddingLeftRight = Adaptive.val(20)
}
