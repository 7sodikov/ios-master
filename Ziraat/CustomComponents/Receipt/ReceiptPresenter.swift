//
//  ReceiptPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 3/18/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ReceiptPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ReceiptVM { get }
}

protocol ReceiptInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class ReceiptPresenter: ReceiptPresenterProtocol {
    weak var view: ReceiptVCProtocol!
    var interactor: ReceiptInteractorProtocol!
    var router: ReceiptRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ReceiptVM()
    
    // Private property and methods
    
}

extension ReceiptPresenter: ReceiptInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
