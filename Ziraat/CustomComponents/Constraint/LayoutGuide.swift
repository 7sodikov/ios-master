//
//  LayoutGuide.swift
//  MilliyCore
//
//  Created by Macbook on 4/4/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public final class LayoutGuide: UILayoutGuide {
    
    private let constraintLayout: ConstraintLayout
    
    public init(_ view: UIView) {
        self.constraintLayout = ConstraintLayout(view)
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        activateConstraints()
    }
    
    @discardableResult public func equalToSuperView(_ file: String = #file) -> ConstraintLayout {
        guard let superView = constraintLayout.superView else {
            fatalError("did not find superView in \(file)")
        }
        return equalTo(superView)
    }
    
    @discardableResult public func equalTo(_ superView: UIView) -> ConstraintLayout {

        constraintLayout.constraints.filter { !$1.isValid }.forEach { $1.superView = superView }
        
        constraintLayout.anchorTypes
            .filter { !constraintLayout.constraints.keys.contains($0) }
            .forEach {
                constraintLayout.constraints[$0] = ConstraintData(type: $0,
                                                                         view: constraintLayout.view,
                                                                         superView: superView)
            }
        return constraintLayout
    }
    
    @discardableResult public func equalTo(_ horizontalAnchor: NSLayoutXAxisAnchor) -> ConstraintLayout {
        guard let type = constraintLayout.lastSelectedType else {
            fatalError("was not show NSLayoutYAxisAnchor. it is error in LayoutGuide")
        }
        constraintLayout.constraints[type] = ConstraintData(type: type, view: constraintLayout.view, horizontalAnchor: horizontalAnchor)
        return constraintLayout
    }
    
    @discardableResult public func equalTo(_ verticalAnchor: NSLayoutYAxisAnchor) -> ConstraintLayout {
        guard let type = constraintLayout.lastSelectedType else {
            fatalError("was not show NSLayoutYAxisAnchor. it is error in LayoutGuide")
        }
        constraintLayout.constraints[type] = ConstraintData(type: type, view: constraintLayout.view, verticalAnchor: verticalAnchor)
        return constraintLayout
    }
    
    public func equalToConstant(_ constant: CGFloat) {
        constraintLayout.anchorTypes.forEach { (type) in
            if let anchor = dimension(at: type) {
                anchor.constraint(equalToConstant: constant).isActive = true
                constraintLayout.anchorTypes.remove(type)
            }
        }
    }
    
    @discardableResult public func lessThanOrEqualToConstant(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.forEach { (type) in
            if let anchor = dimension(at: type) {
                anchor.constraint(lessThanOrEqualToConstant: constant).isActive = true
                constraintLayout.anchorTypes.remove(type)
            }
        }
        return self
    }
    
    @discardableResult public func greaterThanOrEqualToConstant(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.forEach { (type) in
            if let anchor = dimension(at: type) {
                anchor.constraint(greaterThanOrEqualToConstant: constant).isActive = true
                constraintLayout.anchorTypes.remove(type)
            }
            
        }
        return self
    }
    
    private func dimension(at type: AnchorType) -> NSLayoutDimension? {
        switch type {
            
        case .width:
            constraintLayout.dimensions.append(type)
            return constraintLayout.layoutGuide.widthAnchor
        case .height:
            constraintLayout.dimensions.append(type)
            return constraintLayout.layoutGuide.heightAnchor
            
        default: return nil
        }
    }
    
    private func activateConstraints() {
        let constraints: [NSLayoutConstraint] = constraintLayout.constraints.filter { !constraintLayout.dimensions.contains($0.key) }.compactMap { (item) in
            let (type, constraintData) = item
            let guide = constraintData.view
            switch type {
                
            case .top:     return guide.topAnchor.constraint(equalTo: constraintData.topAnchor, constant: constraintData.constant)
            case .left:    return guide.leftAnchor.constraint(equalTo: constraintData.leftAnchor, constant: constraintData.constant)
            case .right:   return guide.rightAnchor.constraint(equalTo: constraintData.rightAnchor, constant: constraintData.constant)
            case .bottom:  return guide.bottomAnchor.constraint(equalTo: constraintData.bottomAnchor, constant: constraintData.constant)
            case .leading: return guide.leadingAnchor.constraint(equalTo: constraintData.leadingAnchor, constant: constraintData.constant)
            case .trailing: return guide.trailingAnchor.constraint(equalTo: constraintData.trailingAnchor, constant: constraintData.constant)
            case .centerX: return guide.centerXAnchor.constraint(equalTo: constraintData.centerXAnchor)
            case .centerY: return guide.centerYAnchor.constraint(equalTo: constraintData.centerYAnchor)
            case .width:   return guide.widthAnchor.constraint(equalTo: constraintData.widthAnchor)
            case .height:  return guide.heightAnchor.constraint(equalTo: constraintData.heightAnchor)
                
            default: return nil
            }
        }
        NSLayoutConstraint.activate(constraints)
    }
}

// MARK:- properties

public extension LayoutGuide {
    
    var left: LayoutGuide {
        constraintLayout.lastSelectedType = .left
        constraintLayout.anchorTypes.insert(.left)
        return self
    }
    
    var right: LayoutGuide {
        constraintLayout.lastSelectedType = .right
        constraintLayout.anchorTypes.insert(.right)
        return self
    }
    
    var top: LayoutGuide {
        constraintLayout.lastSelectedType = .top
        constraintLayout.anchorTypes.insert(.top)
        return self
    }
    
    var bottom: LayoutGuide {
        constraintLayout.lastSelectedType = .bottom
        constraintLayout.anchorTypes.insert(.bottom)
        return self
    }
    
    var leading: LayoutGuide {
        constraintLayout.lastSelectedType = .leading
        constraintLayout.anchorTypes.insert(.leading)
        return self
    }
    
    var trailing: LayoutGuide {
        constraintLayout.lastSelectedType = .trailing
        constraintLayout.anchorTypes.insert(.trailing)
        return self
    }
    
    var horizontal: LayoutGuide {
        constraintLayout.lastSelectedType = .leading
        constraintLayout.anchorTypes.insert(.leading)
        constraintLayout.anchorTypes.insert(.trailing)
        return self
    }
    
    var vertical: LayoutGuide {
        constraintLayout.lastSelectedType = .top
        constraintLayout.anchorTypes.insert(.top)
        constraintLayout.anchorTypes.insert(.bottom)
        return self
    }
    
    var centerX: LayoutGuide {
        constraintLayout.lastSelectedType = .centerX
        constraintLayout.anchorTypes.insert(.centerX)
        return self
    }
    
    var centerY: LayoutGuide {
        constraintLayout.lastSelectedType = .centerY
        constraintLayout.anchorTypes.insert(.centerY)
        return self
    }
    
    var center: LayoutGuide {
        constraintLayout.anchorTypes.insert(.centerX)
        constraintLayout.anchorTypes.insert(.centerY)
        return self
    }
    
    var edges: LayoutGuide {
        constraintLayout.anchorTypes.insert(.left)
        constraintLayout.anchorTypes.insert(.right)
        constraintLayout.anchorTypes.insert(.top)
        constraintLayout.anchorTypes.insert(.bottom)
        return self
    }
    
    var width: LayoutGuide {
        constraintLayout.lastSelectedType = .width
        constraintLayout.anchorTypes.insert(.width)
        return self
    }
    
    var height: LayoutGuide {
        constraintLayout.lastSelectedType = .height
        constraintLayout.anchorTypes.insert(.height)
        return self
    }
    
    var square: LayoutGuide {
        constraintLayout.anchorTypes.insert(.width)
        constraintLayout.anchorTypes.insert(.height)
        return self
    }
    
}


//MARK: - prop functions with margin

public extension LayoutGuide {
    
    @discardableResult func left(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.left)
        constraintLayout.constraints[.left] = .init(type: .left, view: constraintLayout.view, constant: constant)
        return self
    }
    
    @discardableResult func right(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.right)
        constraintLayout.constraints[.right] = .init(type: .right, view: constraintLayout.view, constant: constant)
        return self
    }
    
    @discardableResult func top(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.top)
        constraintLayout.constraints[.top] = .init(type: .top, view: constraintLayout.view, constant: constant)
        return self
    }
    
    @discardableResult func bottom(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.bottom)
        constraintLayout.constraints[.bottom] = .init(type: .bottom, view: constraintLayout.view, constant: constant)
        return self
    }
    
    @discardableResult func leading(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.leading)
        constraintLayout.constraints[.leading] = .init(type: .leading, view: constraintLayout.view, constant: constant)
        return self
    }
    
    @discardableResult func trailing(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.trailing)
        constraintLayout.constraints[.trailing] = .init(type: .trailing, view: constraintLayout.view, constant: constant)
        return self
    }
    
    @discardableResult func horizontal(_ constant: CGFloat) -> LayoutGuide {
        leading(constant)
        trailing(-constant)
        return self
    }
    
    @discardableResult func vertical(_ constant: CGFloat) -> LayoutGuide {
        top(constant)
        bottom(-constant)
        return self
    }
    
    @discardableResult func width(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.width)
        equalToConstant(constant)
        return self
    }
    
    @discardableResult func height(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.height)
        equalToConstant(constant)
        return self
    }
    
    @discardableResult func square(_ constant: CGFloat) -> LayoutGuide {
        constraintLayout.anchorTypes.insert(.width)
        constraintLayout.anchorTypes.insert(.height)
        equalToConstant(constant)
        return self
    }
    
    @discardableResult func edges(_ constant: UIEdgeInsets) -> LayoutGuide {
        vertical(constant.top)
        horizontal(constant.left)
        return self
    }
    
}


//MARK: - prop functions with LayoutConstraint

public extension LayoutGuide {
    
    @discardableResult func left(_ leftAnchor: NSLayoutXAxisAnchor, offset constant: CGFloat = 0) -> LayoutGuide {
        constraintLayout.constraints[.left] = .init(type: .left, view: constraintLayout.view, horizontalAnchor: leftAnchor, constant: constant)
        return self
    }
    
    @discardableResult func right(_ rightAnchor: NSLayoutXAxisAnchor, offset constant: CGFloat = 0) -> LayoutGuide {
        constraintLayout.constraints[.right] = .init(type: .right, view: constraintLayout.view, horizontalAnchor: rightAnchor, constant: constant)
        return self
    }
    
    @discardableResult func top(_ topAnchor: NSLayoutYAxisAnchor, offset constant: CGFloat = 0) -> LayoutGuide {
        constraintLayout.constraints[.top] = .init(type: .top, view: constraintLayout.view, verticalAnchor: topAnchor, constant: constant)
        return self
    }
    
    @discardableResult func bottom(_ bottomAnchor: NSLayoutYAxisAnchor, offset constant: CGFloat = .zero) -> LayoutGuide {
        constraintLayout.constraints[.bottom] = .init(type: .bottom, view: constraintLayout.view, verticalAnchor: bottomAnchor, constant: constant)
        return self
    }
    
    @discardableResult func leading(_ leadingAnchor: NSLayoutXAxisAnchor, offset constant: CGFloat = .zero) -> LayoutGuide {
        constraintLayout.constraints[.leading] = .init(type: .leading, view: constraintLayout.view, horizontalAnchor: leadingAnchor, constant: constant)
        return self
    }
    
    @discardableResult func trailing(_ trailingAnchor: NSLayoutXAxisAnchor, offset constant: CGFloat = .zero) -> LayoutGuide {
        constraintLayout.constraints[.trailing] = .init(type: .trailing, view: constraintLayout.view, horizontalAnchor: trailingAnchor, constant: constant)
        return self
    }
    
    @discardableResult func centerX(_ centerXAnchor: NSLayoutXAxisAnchor) -> LayoutGuide {
        constraintLayout.constraints[.centerX] = .init(type: .centerX, view: constraintLayout.view, horizontalAnchor: centerXAnchor)
        return self
    }
    
    @discardableResult func centerY(_ centerYAnchor: NSLayoutYAxisAnchor) -> LayoutGuide {
        constraintLayout.constraints[.centerY] = .init(type: .centerY, view: constraintLayout.view, verticalAnchor: centerYAnchor)
        return self
    }
    
}


//MARK: - prop functions with view

public extension LayoutGuide {
    
    @discardableResult func left(_ view: UIView, offset constant: CGFloat = 0) -> LayoutGuide {
        left(view.safeAreaLayoutGuide.leftAnchor, offset: constant)
    }
    
    @discardableResult func right(_ view: UIView, offset constant: CGFloat = 0) -> LayoutGuide {
        right(view.safeAreaLayoutGuide.rightAnchor, offset: constant)
    }
    
    @discardableResult func top(_ view: UIView, offset constant: CGFloat = 0) -> LayoutGuide {
        top(view.safeAreaLayoutGuide.topAnchor, offset: constant)
    }
    
    @discardableResult func bottom(_ view: UIView, offset constant: CGFloat = .zero) -> LayoutGuide {
        bottom(view.safeAreaLayoutGuide.bottomAnchor, offset: constant)
    }
    
    @discardableResult func leading(_ view: UIView, offset constant: CGFloat = .zero) -> LayoutGuide {
        leading(view.safeAreaLayoutGuide.leadingAnchor, offset: constant)
    }
    
    @discardableResult func trailing(_ view: UIView, offset constant: CGFloat = .zero) -> LayoutGuide {
        trailing(view.safeAreaLayoutGuide.trailingAnchor, offset: constant)
    }
    
    @discardableResult func centerX(_ view: UIView) -> LayoutGuide {
        centerX(view.safeAreaLayoutGuide.centerXAnchor)
    }
    
    @discardableResult func centerY(_ view: UIView) -> LayoutGuide {
        centerY(view.safeAreaLayoutGuide.centerYAnchor)
    }
    
    @discardableResult func center(_ view: UIView) -> LayoutGuide {
        centerX(view)
        return centerY(view)
    }
    
    @discardableResult func edges(_ view: UIView, offset edgeInsert: UIEdgeInsets = .zero) -> LayoutGuide {
        leading(view, offset: edgeInsert.left)
        top(view, offset: edgeInsert.top)
        trailing(view, offset: -edgeInsert.right)
        return bottom(view, offset: -edgeInsert.bottom)
    }
    
}
