//
//  ConstraintLayout.swift
//  MilliyCore
//
//  Created by Macbook on 4/4/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

public class ConstraintLayout {
    
    internal var dimensions: [AnchorType] = []
    internal var lastSelectedType: AnchorType? = nil
    internal var anchorTypes: Set<AnchorType> = Set()
    internal var constraints: Dictionary<AnchorType, ConstraintData> = [:]
    
    internal let view: UIView
    internal var superView: UIView? {
        view.superview
    }
    
    internal var layoutGuide: UILayoutGuide {
        view.safeAreaLayoutGuide
    }
    
    init(_ view: UIView) {
        self.view = view
        
    }
    
    public func offset(_ margin: CGFloat) {
        guard let type = lastSelectedType ?? anchorTypes.first, margin != 0 else { return }
        constraints[type]?.constant = margin
    }
    
    public func insert(_ edgeInsets: UIEdgeInsets) {
        anchorTypes.forEach { (type) in
            switch type {
                
            case .top:
                constraints[.top]?.constant = edgeInsets.top
            case .left, .leading:
                constraints[type]?.constant = edgeInsets.left
            case .bottom:
                constraints[.bottom]?.constant = -edgeInsets.bottom
            case .right, .trailing:
                constraints[.right]?.constant = -edgeInsets.right
                     
            default: break
            }
        }
    }
}
