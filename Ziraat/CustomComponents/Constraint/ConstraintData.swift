//
//  ConstraintData.swift
//  MilliyCore
//
//  Created by Macbook on 4/4/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

class ConstraintData {
    let type: AnchorType
    var horizontalAnchor: NSLayoutXAxisAnchor? = nil
    var verticalAnchor: NSLayoutYAxisAnchor? = nil
    var superView: UIView? = nil
    var view: UIView
    var constant: CGFloat = 0.0
    
    init(type: AnchorType, view: UIView, horizontalAnchor: NSLayoutXAxisAnchor? = nil, verticalAnchor: NSLayoutYAxisAnchor? = nil, superView: UIView? = nil, constant: CGFloat = 0.0) {
        
        self.type = type
        self.horizontalAnchor = horizontalAnchor
        self.verticalAnchor = verticalAnchor
        self.view = view
        self.superView = superView
        self.constant = constant
    }
    
    var isValid: Bool {
        superView != nil || horizontalAnchor != nil || verticalAnchor != nil
    }
    
    /// is called superView topAnchor
    public var topAnchor: NSLayoutYAxisAnchor {
        (verticalAnchor ?? superView?.safeAreaLayoutGuide.topAnchor)!
    }
    
    /// is called superView bottomAnchor
    public var bottomAnchor: NSLayoutYAxisAnchor {
        (verticalAnchor ?? superView?.safeAreaLayoutGuide.bottomAnchor)!
    }
    
    /// is called superView leadingAnchor
    public var leadingAnchor: NSLayoutXAxisAnchor {
        (horizontalAnchor ?? superView?.safeAreaLayoutGuide.leadingAnchor)!
    }
    
    /// is called superView trailingAnchor
    public var trailingAnchor: NSLayoutXAxisAnchor {
        (horizontalAnchor ?? superView?.safeAreaLayoutGuide.trailingAnchor)!
    }
    
    /// is called superView leftAnchor
    public var leftAnchor: NSLayoutXAxisAnchor {
        (horizontalAnchor ?? superView?.safeAreaLayoutGuide.leftAnchor)!
    }
    
    /// is called superView rightAnchor
    public var rightAnchor: NSLayoutXAxisAnchor {
        (horizontalAnchor ?? superView?.safeAreaLayoutGuide.rightAnchor)!
    }
    
    /// is called superView centerXAnchor
    public var centerXAnchor: NSLayoutXAxisAnchor {
        (horizontalAnchor ?? superView?.centerXAnchor)!
    }
    
    /// is called superView centerYAnchor
    public var centerYAnchor: NSLayoutYAxisAnchor {
        (verticalAnchor ?? superView?.centerYAnchor)!
    }
    
    /// is called superView widthAnchor
    public var widthAnchor: NSLayoutDimension {
        superView!.safeAreaLayoutGuide.widthAnchor
    }
    
    /// is called superView heightAnchor
    public var heightAnchor: NSLayoutDimension {
        superView!.safeAreaLayoutGuide.heightAnchor
    }
}
