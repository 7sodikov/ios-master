//
//  AnchorType.swift
//  MilliyCore
//
//  Created by Macbook on 4/4/20.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

internal enum AnchorType: Int {
    case left, right, top, bottom, leading, trailing
    case centerX, centerY, horizontal, vertical, center
    case edges
    case width, height
}
