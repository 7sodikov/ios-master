//
//  GraphicKeyCellInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol GraphicKeyCellInstaller: ViewInstaller {
    var numberImageView: UIButton! { get set }
}

extension GraphicKeyCellInstaller {
    func initSubviews() {
        mainView.backgroundColor = .clear
        
        numberImageView = UIButton()
        numberImageView.setImage(UIImage(named: "btn_graphic_key"), for: .normal)
        numberImageView.setBackgroundColor(.red, for: .selected)
    }
    
    func embedSubviews() {
        mainView.addSubview(numberImageView)
    }
    
    func addSubviewsConstraints() {
        numberImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(0)
            maker.height.width.equalTo(Size.height)
        }
    }
}

fileprivate struct Size {
    static let height = Adaptive.val(90)
}

