//
//  GraphicKeyCellKeyboard.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class GraphicKeyCellKeyboard: UIView, GraphicKeyCellKeyboardInstaller {
    var buttonStackView: UIStackView!
    var firstRowStackView: UIStackView!
    var oneButton: GraphicKeyCell!
    var twoButton: GraphicKeyCell!
    var threeButton: GraphicKeyCell!
    var secondRowStackView: UIStackView!
    var fourButton: GraphicKeyCell!
    var fiveButton: GraphicKeyCell!
    var sixButton: GraphicKeyCell!
    var thirdRowStackView: UIStackView!
    var sevenButton: GraphicKeyCell!
    var eightButton: GraphicKeyCell!
    var nineButton: GraphicKeyCell!
    var mainView: UIView { self }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        
        
    }
}
