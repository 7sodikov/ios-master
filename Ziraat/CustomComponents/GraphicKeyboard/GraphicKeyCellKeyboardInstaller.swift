//
//  GraphicKeyCellKeyboardInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 10/25/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol GraphicKeyCellKeyboardInstaller: ViewInstaller {
    var buttonStackView: UIStackView! { get set }
    var firstRowStackView: UIStackView! { get set }
    var oneButton: GraphicKeyCell! { get set }
    var twoButton: GraphicKeyCell! { get set }
    var threeButton: GraphicKeyCell! { get set }
    var secondRowStackView: UIStackView! { get set }
    var fourButton: GraphicKeyCell! { get set }
    var fiveButton: GraphicKeyCell! { get set }
    var sixButton: GraphicKeyCell! { get set }
    var thirdRowStackView: UIStackView! { get set }
    var sevenButton: GraphicKeyCell! { get set }
    var eightButton: GraphicKeyCell! { get set }
    var nineButton: GraphicKeyCell! { get set }
    
}

extension GraphicKeyCellKeyboardInstaller {
    func initSubviews() {
        mainView.backgroundColor = .clear
        
        buttonStackView = UIStackView()
        buttonStackView.axis = .vertical
        buttonStackView.distribution = .equalSpacing
        buttonStackView.spacing = Adaptive.val(30)
        
        firstRowStackView = UIStackView()
        firstRowStackView.axis = .horizontal
        firstRowStackView.distribution = .equalSpacing
        firstRowStackView.spacing = Adaptive.val(27)

        oneButton = GraphicKeyCell()
        
        twoButton = GraphicKeyCell()

        threeButton = GraphicKeyCell()

        secondRowStackView = UIStackView()
        secondRowStackView.axis = .horizontal
        secondRowStackView.distribution = .equalSpacing
        secondRowStackView.spacing = Adaptive.val(27)
        
        fourButton = GraphicKeyCell()

        fiveButton = GraphicKeyCell()

        sixButton = GraphicKeyCell()

        thirdRowStackView = UIStackView()
        thirdRowStackView.axis = .horizontal
        thirdRowStackView.distribution = .equalSpacing
        thirdRowStackView.spacing = Adaptive.val(27)
        
        sevenButton = GraphicKeyCell()
        
        eightButton = GraphicKeyCell()
        
        nineButton = GraphicKeyCell()
    }
    
    func embedSubviews() {
        mainView.addSubview(buttonStackView)
        
        buttonStackView.addArrangedSubview(firstRowStackView)
        firstRowStackView.addArrangedSubview(oneButton)
        firstRowStackView.addArrangedSubview(twoButton)
        firstRowStackView.addArrangedSubview(threeButton)
        
        buttonStackView.addArrangedSubview(secondRowStackView)
        secondRowStackView.addArrangedSubview(fourButton)
        secondRowStackView.addArrangedSubview(fiveButton)
        secondRowStackView.addArrangedSubview(sixButton)
        
        buttonStackView.addArrangedSubview(thirdRowStackView)
        thirdRowStackView.addArrangedSubview(sevenButton)
        thirdRowStackView.addArrangedSubview(eightButton)
        thirdRowStackView.addArrangedSubview(nineButton)
    }
    
    func addSubviewsConstraints() {
        buttonStackView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.centerX.equalToSuperview()
        }
        
    }
}

struct GraphicKeyKeyboardViewSize {
    struct Indicator {
        static let size = Adaptive.val(40)
    }
}
