//
//  BottomPanel.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BottomPanelDelegate: class {
    func searchButtonClicked()
    func rightButtonClicked()
}

class BottomPanel: UIView, BottomPanelViewInstaller {
    var mainView: UIView { self }
    var designSeparatorView: UIView!
    var microphoneImageView: UIImageView!
    var microphoneLabel: ClipboardControlledTextField!
    var unavailableButton: UIButton!
    var menuImageView: UIImageView!
    var menuLabel: UILabel!
    var homeImageView: UIImageView!
    var mainActionButton: UIButton!
    
    weak var delegate: BottomPanelDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        mainActionButton.addTarget(self, action: #selector(menuButtonClicked(_:)), for: .touchUpInside)
        unavailableButton.addTarget(self, action: #selector(unavailableButtonClicked(_:)), for: .touchUpInside)
    }

    @objc private func menuButtonClicked(_ sender: NextButton) {
        delegate?.rightButtonClicked()
    }
    
    @objc private func unavailableButtonClicked(_ sender: UIButton) {
        delegate?.searchButtonClicked()
    }
    
//    func localizeText() {
//        menuLabel.text = RS.lbl_menu.localized()
//        microphoneLabel.placeholder = RS.lbl_help.localized()
//    }
}
