//
//  BottomPanelViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 9/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol BottomPanelViewInstaller: ViewInstaller {
    var designSeparatorView: UIView! { get set }
    var microphoneImageView: UIImageView! { get set }
    var microphoneLabel: ClipboardControlledTextField! { get set }
    var menuImageView: UIImageView! { get set }
    var menuLabel: UILabel! { get set }
    var homeImageView: UIImageView! { get set }
    var mainActionButton: UIButton! { get set }
    var unavailableButton: UIButton! { get set }
}

extension BottomPanelViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .dashDarkBlue//ColorConstants.backgroundBlack
        
        designSeparatorView = UIView()
        designSeparatorView.backgroundColor = ColorConstants.borderLine
        
        microphoneImageView = UIImageView()
        microphoneImageView.image = UIImage(named: "img_icon_search")
        
        microphoneLabel = ClipboardControlledTextField()
        microphoneLabel.text = RS.lbl_help.localized()
        microphoneLabel.textColor = .dashLightGray //ColorConstants.borderLine
        microphoneLabel.font = .gothamNarrow(size: 16, .book) //EZFontType.medium.sfuiDisplay(size: 16)
        microphoneLabel.isUserInteractionEnabled = false
        
        unavailableButton = UIButton()
        
        menuImageView = UIImageView()
        menuImageView.image =  UIImage(named: "img_dash_menu") //.setImage(UIImage(named: "img_dash_menu"), for: .normal)
        menuImageView.contentMode = .scaleAspectFit
        
        menuLabel = UILabel()
        menuLabel.text = RS.lbl_menu.localized()
        menuLabel.textColor = .white
        menuLabel.font = .gothamNarrow(size: 14, .book) //EZFontType.medium.sfuiDisplay(size: 14)
        
        homeImageView = UIImageView()
        homeImageView.image = UIImage(named: "btn_close")
        homeImageView.isHidden = true
        
        mainActionButton = UIButton()
    }
    
    func embedSubviews() {
        mainView.addSubview(designSeparatorView)
        mainView.addSubview(microphoneImageView)
        mainView.addSubview(microphoneLabel)
        mainView.addSubview(unavailableButton)
        mainView.addSubview(menuImageView)
        mainView.addSubview(menuLabel)
        mainView.addSubview(homeImageView)
        mainView.addSubview(mainActionButton)
    }
    
    func addSubviewsConstraints() {
        designSeparatorView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(1))
            maker.bottom.equalToSuperview()
            maker.top.equalToSuperview()
            maker.trailing.equalToSuperview().offset(-Adaptive.val(68))
        }
        
        microphoneImageView.snp.remakeConstraints { (maker) in
            maker.top.equalToSuperview().offset(Adaptive.val(20.5))
            maker.leading.equalToSuperview().offset(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(32))
            maker.width.equalTo(Adaptive.val(28))
        }
        
        microphoneLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(microphoneImageView.snp.trailing).offset(16)
            maker.trailing.equalTo(designSeparatorView).offset(-Adaptive.val(17))
            maker.centerY.equalTo(microphoneImageView)
        }
        
        unavailableButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(microphoneLabel)
        }
        
        menuImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(28)
            maker.height.equalTo(20)
            maker.trailing.equalToSuperview().offset(-Adaptive.val(20))
            maker.leading.equalTo(designSeparatorView.snp.trailing).offset(Adaptive.val(20))
            maker.top.equalToSuperview().offset(Adaptive.val(19))
        }
        
        menuLabel.snp.remakeConstraints { (maker) in
            maker.centerX.equalTo(menuImageView)
            maker.top.equalTo(menuImageView.snp.bottom).offset(Adaptive.val(10))
        }
        
        homeImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(28))
            maker.height.equalTo(Adaptive.val(30))
            maker.centerY.equalToSuperview()
            maker.trailing.equalToSuperview().offset(-Adaptive.val(20))
        }
        
        mainActionButton.snp.remakeConstraints { (maker) in
            maker.top.trailing.bottom.equalToSuperview()
            maker.leading.equalTo(designSeparatorView)
        }
    }
}

fileprivate struct Size {
    
}
