//
//  ForgotPasswordDropDownInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordDropDownInteractorProtocol: class {
    
}

class ForgotPasswordDropDownInteractor: ForgotPasswordDropDownInteractorProtocol {
    weak var presenter: ForgotPasswordDropDownInteractorToPresenterProtocol!
    
}
