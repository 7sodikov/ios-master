//
//  ForgotPasswordDropDownRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordDropDownRouterProtocol: class {
    static func createModule() -> UIViewController
}

class ForgotPasswordDropDownRouter: ForgotPasswordDropDownRouterProtocol {
    static func createModule() -> UIViewController {
        let vc = ForgotPasswordDropDownVC()
        let presenter = ForgotPasswordDropDownPresenter()
        let interactor = ForgotPasswordDropDownInteractor()
        let router = ForgotPasswordDropDownRouter()
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
