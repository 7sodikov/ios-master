//
//  ForgotPasswordDropDownPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/7/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ForgotPasswordDropDownPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ForgotPasswordDropDownVM { get }
}

protocol ForgotPasswordDropDownInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class ForgotPasswordDropDownPresenter: ForgotPasswordDropDownPresenterProtocol {
    weak var view: ForgotPasswordDropDownVCProtocol!
    var interactor: ForgotPasswordDropDownInteractorProtocol!
    var router: ForgotPasswordDropDownRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ForgotPasswordDropDownVM()
}

extension ForgotPasswordDropDownPresenter: ForgotPasswordDropDownInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
