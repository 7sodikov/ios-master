//
//  ForgotPasswordDropDownVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ForgotPasswordDropDownVCProtocol: BaseViewControllerProtocol {
    var navigationCtrl: Any { get }

}

class ForgotPasswordDropDownVC: BaseViewController, ForgotPasswordDropDownVCInstaller {
    var mainView: UIView { view }
    var phoneCodeTableView: UITableView!

    private let cellid = "\(ForgotPasswordDropDown.self)"
    
    var presenter: ForgotPasswordDropDownPresenterProtocol!
    
    override func viewDidLoad() {
            super.viewDidLoad()
            
            setupSubviews()
        
            
        }
    
}

extension ForgotPasswordDropDownVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! ForgotPasswordDropDown
        return cell
    }
    
    
}

extension ForgotPasswordDropDownVC: ForgotPasswordDropDownVCProtocol {
    var navigationCtrl: Any {
        return self.navigationController as Any
    }
    
}

