//
//  ForgotPasswordDropDownVCInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol ForgotPasswordDropDownVCInstaller: ViewInstaller {
    var phoneCodeTableView: UITableView! { get set }
}

extension ForgotPasswordDropDownVCInstaller {
    func initSubviews() {
        phoneCodeTableView = UITableView()
    }
    
    func embedSubviews() {
        mainView.addSubview(phoneCodeTableView)
    }
    
    func addSubviewsConstraints() {
        phoneCodeTableView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(100))
            maker.height.equalTo(Adaptive.val(200))
        }
    }
}

