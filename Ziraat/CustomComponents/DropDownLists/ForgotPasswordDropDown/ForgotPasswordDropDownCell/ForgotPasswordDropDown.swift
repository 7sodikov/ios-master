//
//  ForgotPasswordDropDown.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/3/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//


import UIKit


class ForgotPasswordDropDown: UITableViewCell, ForgotPasswordDropDownInstaller {
    var mainView: UIView { contentView }
    
    var phoneCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupSubviews()
    }
    
}
    

