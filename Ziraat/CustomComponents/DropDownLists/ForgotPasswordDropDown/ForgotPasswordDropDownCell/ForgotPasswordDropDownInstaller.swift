//
//  ForgotPasswordDropDownInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/3/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//


import UIKit
import SnapKit


protocol ForgotPasswordDropDownInstaller: ViewInstaller {
    var phoneCodeLabel: UILabel! { get set }
//    var layout: UICollectionViewFlowLayout! { get set }
}

extension ForgotPasswordDropDownInstaller {
    func initSubviews() {
        phoneCodeLabel = UILabel()
        
//        layout = UICollectionViewFlowLayout()
//        layout.sectionInset = .zero
//        layout.scrollDirection = .vertical
    }
    
    func embedSubviews() {
        mainView.addSubview(phoneCodeLabel)
    }
    
    func addSubviewsConstraints() {
        
    }
}

fileprivate struct Size {
    struct TextField {
        static let height = Adaptive.val(50)
    }
    
    static let interItemSpace = Adaptive.val(20)
}
