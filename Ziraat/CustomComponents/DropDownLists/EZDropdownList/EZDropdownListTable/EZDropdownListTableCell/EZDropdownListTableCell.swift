//
//  EZDropdownListTableCell.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

class EZDropdownListTableCell: UITableViewCell, EZDropdownListTableCellInstaller {
    var mainView: UIView { contentView }
    var label: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @discardableResult
    func setup(with title: String?, leftRightPadding: CGFloat) -> EZDropdownListTableCell {
        label.text = title
        label.snp.remakeConstraints { (maker) in
            maker.top.bottom.equalToSuperview()
            maker.left.equalTo(leftRightPadding)
            maker.right.equalTo(-leftRightPadding)
        }
        return self
    }
}
