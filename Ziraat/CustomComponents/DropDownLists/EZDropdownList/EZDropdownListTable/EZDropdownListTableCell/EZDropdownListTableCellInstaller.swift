//
//  EZDropdownListTableCellInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/11/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZDropdownListTableCellInstaller: ViewInstaller {
    var label: UILabel! { get set }
}

extension EZDropdownListTableCellInstaller {
    func initSubviews() {
        label = UILabel()
        label.textColor = .black
    }
    
    func embedSubviews() {
        mainView.addSubview(label)
    }
    
    func addSubviewsConstraints() {
        
    }
}

fileprivate struct Size {
    static let paddingLeftRight = Adaptive.val(10)
    
}
