//
//  EZDropdownListTable.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol EZDropdownListTableDelegate: class {
    func ezDropdownList(_ dropdownList: EZDropdownListTable, didSelectAt index: Int)
    func ezDropdownListNonSelectionClicked(_ dropdownList: EZDropdownListTable)
}

class EZDropdownListTable: UIView, EZDropdownListTableViewInstaller {
    var mainView: UIView { self }
    var tableCoverView: UIView!
    var tableView: UITableView!
    var arrowImageView: UIImageView!
    
    private var cellHeight: CGFloat = Adaptive.val(30)
    var selectedIndex: Int = -1
    var titleLeftPadding: CGFloat = Adaptive.val(10)
    var arrowRightPadding: CGFloat = Adaptive.val(5)
    var itemFont: UIFont?
    var nonSelectionItem: String?
    var items: [String] = []
    var itemColor = UIColor.black
    var allowNonSelection: Bool = true
    weak var delegate: EZDropdownListTableDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.removeFromSuperview()
    }
    
    func open(for anchorView: UIView) {
        cellHeight = anchorView.frame.height
        let view = UIApplication.shared.keyWindow!
        self.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(self)
        
        let anchorFrame = anchorView.superview!.convert(anchorView.frame, to: self)
        let itemsCount = items.count + (allowNonSelection ? 1 : 0)
        let tableActualHeight = CGFloat(itemsCount) * cellHeight
        
        let bottomSpace = view.frame.height - anchorFrame.minY - EZDropdownListTableSize.padding
        let fitsToTheBottom = tableActualHeight <= bottomSpace
        
        var openToBottom = true
        if fitsToTheBottom {
            openToBottom = true
        } else {
            if bottomSpace >= 3 * cellHeight {
                openToBottom = true
            } else {
                openToBottom = false
            }
        }
        
        let fontHeight = itemFont?.pointSize ?? 0
        if openToBottom {
            let availableHeight = view.frame.height - anchorFrame.origin.y - EZDropdownListTableSize.padding
            let neededHeight = (tableActualHeight < availableHeight) ? tableActualHeight : availableHeight
            tableCoverView.frame = CGRect(x: anchorFrame.origin.x,
                                          y: anchorFrame.origin.y,
                                          width: anchorFrame.width,
                                          height: neededHeight)
            arrowImageView.snp.remakeConstraints { (maker) in
                maker.height.equalTo(fontHeight)
                maker.top.equalTo((cellHeight - fontHeight)/2)
                maker.right.equalTo(-arrowRightPadding)
            }
        } else {
            let availableHeight = anchorFrame.maxY - EZDropdownListTableSize.padding
            let neededHeight = (tableActualHeight < availableHeight) ? tableActualHeight : availableHeight
            let y = availableHeight - neededHeight + EZDropdownListTableSize.padding
            tableCoverView.frame = CGRect(x: anchorFrame.origin.x,
                                          y: y,
                                          width: anchorFrame.width,
                                          height: neededHeight)
            arrowImageView.snp.remakeConstraints { (maker) in
                maker.height.equalTo(fontHeight)
                maker.bottom.equalTo(-(cellHeight - fontHeight)/2)
                maker.right.equalTo(-arrowRightPadding)
            }
        }
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: anchorView.layer.cornerRadius, left: 0, bottom: anchorView.layer.cornerRadius, right: 0)
        tableCoverView.layer.cornerRadius = anchorView.layer.cornerRadius
    }
    
    @objc private func headerViewClicked(_ gesture: UITapGestureRecognizer) {
        delegate?.ezDropdownListNonSelectionClicked(self)
        self.removeFromSuperview()
    }
}

extension EZDropdownListTable: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = String(describing: EZDropdownListTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! EZDropdownListTableCell
        cell.backgroundColor = .clear
        let isSelected = (selectedIndex == indexPath.row)
        cell.label.font = itemFont
        cell.label.textColor = isSelected ? ColorConstants.mainRed : itemColor
        return cell.setup(with: items[indexPath.row], leftRightPadding: titleLeftPadding)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = EZDropdownListTableCell(style: .default, reuseIdentifier: nil)
        headerView.backgroundColor = .white
        headerView.label.font = itemFont
        headerView.label.textColor = itemColor.withAlphaComponent(0.5)
        headerView.setup(with: nonSelectionItem, leftRightPadding: titleLeftPadding)
        let tap = UITapGestureRecognizer(target: self, action: #selector(headerViewClicked(_:)))
        headerView.addGestureRecognizer(tap)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return allowNonSelection ? cellHeight : 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.ezDropdownList(self, didSelectAt: indexPath.row)
        self.removeFromSuperview()
    }
}
