//
//  EZDropdownListTableViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/10/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZDropdownListTableViewInstaller: ViewInstaller {
    var tableCoverView: UIView! { get set }
    var tableView: UITableView! { get set }
    var arrowImageView: UIImageView! { get set }
    
}

extension EZDropdownListTableViewInstaller {
    func initSubviews() {
        tableCoverView = UIView()
        tableCoverView.backgroundColor = .white
        tableCoverView.clipsToBounds = true
        
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.register(EZDropdownListTableCell.self, forCellReuseIdentifier: String(describing: EZDropdownListTableCell.self))
        tableView.backgroundColor = .clear
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: CGFloat.leastNormalMagnitude, height: CGFloat.leastNormalMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: CGFloat.leastNormalMagnitude, height: CGFloat.leastNormalMagnitude))
        
        arrowImageView = UIImageView()
        arrowImageView.contentMode = .scaleAspectFit
        arrowImageView.image = UIImage(named: "btn_up")?.changeColor(.black)
    }
    
    func embedSubviews() {
        mainView.addSubview(tableCoverView)
        tableCoverView.addSubview(tableView)
        tableCoverView.addSubview(arrowImageView)
    }
    
    func addSubviewsConstraints() {
        tableView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct EZDropdownListTableSize {
    static let padding = Adaptive.val(20)
}
