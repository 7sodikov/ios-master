//
//  EZDropdownListViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import SnapKit

protocol EZDropdownListViewInstaller: ViewInstaller {
    var placeholderLabel: UILabel! { get set }
    var titleLabel: UILabel! { get set }
    var arrowImageView: UIImageView! { get set }
}

extension EZDropdownListViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .clear
        
        placeholderLabel = UILabel()
        
        titleLabel = UILabel()
        
        arrowImageView = UIImageView()
        arrowImageView.contentMode = .scaleAspectFit
        arrowImageView.image = UIImage(named: "btn_down")
    }
    
    func embedSubviews() {
        mainView.addSubview(placeholderLabel)
        mainView.addSubview(titleLabel)
        mainView.addSubview(arrowImageView)
    }
    
    func addSubviewsConstraints() {
        arrowImageView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

fileprivate struct Size {
    
}
