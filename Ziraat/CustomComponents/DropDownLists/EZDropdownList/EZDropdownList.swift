//
//  EZDropdownList.swift
//  Ziraat
//
//  Created by Shamsiddin on 11/14/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol EZDropdownListDelegate: class {
    func ezDropdownListItems(_ dropdownList: EZDropdownList) -> [String]
    func ezDropdownList(_ dropdownList: EZDropdownList, didSelectItemAt index: Int)
    func ezDropdownListNonSelectionClicked(_ dropdownList: EZDropdownList)
}

class EZDropdownList: UIView, EZDropdownListViewInstaller {
    var mainView: UIView { self }
    var placeholderLabel: UILabel!
    var titleLabel: UILabel!
    var arrowImageView: UIImageView!
    
    weak var delegate: EZDropdownListDelegate?
    
    var edgeInsets = Adaptive.edges(top: 0, left: 10, bottom: 0, right: 10)
    var selectedIndex: Int = -1 {
        didSet {
            setTitle()
        }
    }
    
    var allowNonSelection: Bool = true
    var itemFont = UIFont.boldSystemFont(ofSize: (Adaptive.val(16.0)))
    var arrowSize = Adaptive.size(25, 11)
    var titleColor = UIColor.black
    var placeholder: String? {
        didSet {
            setTitle()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
        backgroundColor = .white
        let tap = UITapGestureRecognizer(target: self, action: #selector(openDropdownListTable(_:)))
        self.addGestureRecognizer(tap)
    }
    
    func setupProperties() {
        titleLabel.font = itemFont
        placeholderLabel.font = itemFont
        titleLabel.textColor = titleColor
        placeholderLabel.textColor = titleColor.withAlphaComponent(0.5)
        setTitle()
        
        placeholderLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(edgeInsets.top)
            maker.left.equalTo(edgeInsets.left)
            maker.bottom.equalTo(edgeInsets.bottom)
            maker.right.equalTo(arrowImageView.snp.left).offset(Adaptive.val(10))
        }
        
        titleLabel.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(placeholderLabel)
        }
        
        arrowImageView.snp.remakeConstraints { (maker) in
            maker.right.equalTo(-edgeInsets.right)
            maker.centerY.equalToSuperview()
            maker.size.equalTo(arrowSize)
        }
    }
    
    private func setTitle() {
        let items = delegate?.ezDropdownListItems(self) ?? []
        
        if selectedIndex > -1 && selectedIndex < items.count {
            placeholderLabel.isHidden = true
            titleLabel.isHidden = false
            titleLabel.text = items[selectedIndex]
        } else {
            placeholderLabel.text = placeholder
            placeholderLabel.isHidden = false
            titleLabel.isHidden = true
        }
    }
    
    @objc private func openDropdownListTable(_ gesture: UITapGestureRecognizer) {
        let dropdownList = EZDropdownListTable(frame: .zero)
        dropdownList.delegate = self
        dropdownList.selectedIndex = selectedIndex
        dropdownList.itemFont = itemFont
        dropdownList.items = delegate?.ezDropdownListItems(self) ?? []
        dropdownList.titleLeftPadding = edgeInsets.left
        dropdownList.arrowRightPadding = edgeInsets.right
        dropdownList.allowNonSelection = allowNonSelection
        dropdownList.nonSelectionItem = placeholder
        dropdownList.open(for: self)
    }
}

extension EZDropdownList: EZDropdownListTableDelegate {
    func ezDropdownList(_ dropdownList: EZDropdownListTable, didSelectAt index: Int) {
        selectedIndex = index
        setTitle()
        delegate?.ezDropdownList(self, didSelectItemAt: index)
    }
    
    func ezDropdownListNonSelectionClicked(_ dropdownList: EZDropdownListTable) {
        selectedIndex = -1
        setTitle()
        delegate?.ezDropdownListNonSelectionClicked(self)
    }
}
