//
//  TableViewModel.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

open class TableViewModel: NSObject {
    
    public private(set) var isSelected: Bool
    public let uid: String?
    
    public var estimatedRowHeight: CGFloat {
        UITableView.automaticDimension
    }
    
    public init(uid: String? = nil, isSelected: Bool = false) {
        self.uid = uid
        self.isSelected = isSelected
        super.init()
    }
        
    public var cellType: TableViewCell.Type {
        TableViewCell.self
    }
    
    public func setSelected(_ isSelected: Bool) {
        self.isSelected = isSelected
    }
}

