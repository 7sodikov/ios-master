//
//  TableViewCell.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit.UITableViewCell
import SwipeCellKit

open class TableViewCell: SwipeTableViewCell {
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public var cellHeight: CGFloat {
        CGFloat.leastNonzeroMagnitude
    }
    
    private var dynamicViewModel: Dynamic<TableViewModel>! {
        didSet {
            dynamicViewModel.bindAndFire { (viewModel) in
                self.didUpdate(viewModel: viewModel)
            }
        }
    }
    
    internal func didUpdate(viewModel: TableViewModel) { }
    
    public func set(viewModel: TableViewModel) {
        if dynamicViewModel == nil {
            dynamicViewModel = Dynamic(value: viewModel)
            dynamicViewModel.bindAndFire { (viewModel) in
                self.didUpdate(viewModel: viewModel)
            }
        } else {
            dynamicViewModel.value = viewModel
        }
    }

    public func reload() {
        
    }
    
    
}

