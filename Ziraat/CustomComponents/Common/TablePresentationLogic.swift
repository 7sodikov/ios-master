//
//  TablePresentationLogic.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/9/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit


protocol TablePresentationLogic {
    func numberOfSections(in table: UITableView) -> Int
    
    func tablePresenter(_ table: UITableView, numberOfRowsInSection section: Int) -> Int
    func tablePresenter(_ table: UITableView, viewModelAtIndexPath indexPath: IndexPath) -> TableViewModel
    func tablePresenter(_ table: UITableView, didSelectRowAt indexPath: IndexPath)
    func tablePresenter(_ table: UITableView, dataForHeaderViewInSection section: Int) -> Any?
    func tablePresenter(_ table: UITableView, dataForFooterViewInSection section: Int) -> Any?
    func tablePresenter(_ table: UITableView, titleForHeaderInSection section: Int) -> String?
}

extension TablePresentationLogic {
    
    func numberOfSections(in table: UITableView) -> Int {
        return 1
    }
    
    func tablePresenter(_ table: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func tablePresenter(_ table: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tablePresenter(_ table: UITableView, dataForHeaderViewInSection section: Int) -> Any? {
        return nil
    }
    
    func tablePresenter(_ table: UITableView, dataForFooterViewInSection section: Int) -> Any? {
        return nil
    }
    
}
