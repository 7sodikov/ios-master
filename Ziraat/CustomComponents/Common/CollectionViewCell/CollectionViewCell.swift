//
//  CollectionViewCell.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 25/01/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit

open class CollectionViewCell: UICollectionViewCell {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public var cellHeight: CGFloat {
        CGFloat.leastNonzeroMagnitude
    }
    
    private var dynamicViewModel: Dynamic<CollectionViewModel>! {
        didSet {
            dynamicViewModel.bindAndFire { (viewModel) in
                self.didUpdate(viewModel: viewModel)
            }
        }
    }
    
    internal func didUpdate(viewModel: CollectionViewModel) { }
    
    public func configure(viewModel: CollectionViewModel) {
        if dynamicViewModel == nil {
            dynamicViewModel = Dynamic(value: viewModel)
            dynamicViewModel.bindAndFire { (viewModel) in
                self.didUpdate(viewModel: viewModel)
            }
        } else {
            dynamicViewModel.value = viewModel
        }
    }
    
}
