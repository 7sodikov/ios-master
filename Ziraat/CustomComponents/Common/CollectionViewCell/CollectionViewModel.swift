//
//  CollectionViewModel.swift
//  MilliyCore
//
//  Created by Bilal Bakhrom on 25/01/2020.
//  Copyright © 2020 Bilal Bakhrom. All rights reserved.
//

import UIKit.UICollectionView

open class CollectionViewModel {
    
    public private(set) var isSelected: Bool
    public let uid: String?
    
    public init(uid: String? = nil, isSelected: Bool = false) {
        self.uid = uid
        self.isSelected = isSelected
    }
    
    public var cellType: CollectionViewCell.Type {
        CollectionViewCell.self
    }
    
    public var itemSize: CGSize {
        .zero
    }
    
    public func setSelected(_ isSelected: Bool) {
        self.isSelected = isSelected
    }
}
