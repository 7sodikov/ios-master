//
//  PresentationProtocol.swift
//  Ziraat
//
//  Created by Jasur Amirov on 3/16/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol PresentationProtocol: class {
    func viewDidLayoutSubviews()
    func viewDidLoad()
    func viewWillAppear(_ animated: Bool)
    func viewDidAppear(_ animated: Bool)
    func viewWillDisappear(_ animated: Bool)
    func viewDidDisappear(_ animated: Bool)
}


extension PresentationProtocol {
    
    func viewDidLayoutSubviews() {
        fatalError("Implementation pending...")
    }
    
    func viewDidLoad() {
        fatalError("Implementation pending...")
    }
    
    func viewWillAppear(_ animated: Bool) {
        fatalError("Implementation pending...")
    }
    
    func viewWillDisappear(_ animated: Bool) {
        fatalError("Implementation pending...")
    }
    
    func viewDidAppear(_ animated: Bool) {
        fatalError("Implementation pending...")
    }
    
    func viewDidDisappear(_ animated: Bool) {
        fatalError("Implementation pending...")
    }
}

