//
//  PdfViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import PDFKit

protocol PdfViewInstaller: ViewInstaller {
    var pdfView: PDFView! {get set}
    var sendButton: Button! {get set}
    var backButton: Button! {get set}
    
}

extension PdfViewInstaller {
        
    func initSubviews() {
        
        mainView.backgroundColor = .white
        
        pdfView = PDFView()
        pdfView.backgroundColor = .clear
        pdfView.displayMode = .singlePageContinuous
        pdfView.autoScales = true
        pdfView.contentMode = .center
        pdfView.displayDirection = .horizontal
        pdfView.pageShadowsEnabled = false
        
        sendButton = Button(method: .network, style: .red)
        sendButton.setup(title: RS.btn_send_by_email.localized().uppercased())
        sendButton.setBackgroundColor(ColorConstants.highlightedMainRed, for: .highlighted)
        
        backButton = Button(method: .network, style: .back)
        backButton.setup()
    }
    
    func embedSubviews() {
        mainView.addSubview(pdfView)
        mainView.addSubview(sendButton)
        mainView.addSubview(backButton)
    }
    
    func addSubviewsConstraints() {
        backButton.constraint { (make) in
            make.bottom(-8).horizontal(16).equalToSuperView()
        }
        sendButton.constraint { (make) in
            make.bottom(self.backButton.topAnchor, offset: -8).horizontal(16).equalToSuperView()
        }
        
        pdfView.constraint { (make) in
            make.bottom(self.sendButton.topAnchor, offset: -32).horizontal
                .top.equalToSuperView()
        }
        
    }
}
