//
//  PdfViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import PDFKit
import UIKit
import MessageUI

class PdfViewController: BaseViewController, PdfViewInstaller {
    
    var pdfView: PDFView!
    var sendButton: Button!
    var backButton: Button!
    var mainView: UIView { self.view }
    
    var operation: String
    
    var user: UserResponse?
    
    init(operation: String, title: String) {
        self.operation = operation
        super.init(nibName: nil, bundle: nil)
        self.title = title
        
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func loadView() {
        super.loadView()
        
        setupSubviews()
        navigationController?.transparentBackgorund()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTargets()
        
        guard let data = operation.dataFromBase64 else {
            return
        }
        pdfView.document = PDFDocument(data: data)
    }
    
    override func setupTargets() {
        
        backButton.didClick {
            self.navigationController?.popViewController(animated: true)
        }
        
        sendButton.didClick {
            self.prepareSendingPdfChequeToMail()
        }
        
        let dashboardImage = UIImage(named: "img_share_account")!
        let dashboardButton   = UIBarButtonItem(image: dashboardImage,  style: UIBarButtonItem.Style.plain, target: self, action: #selector(shareCheque))
        self.navigationItem.rightBarButtonItem = dashboardButton
    }
    
    private func prepareSendingPdfChequeToMail() {
        sendButton.on(lock: {
            self.getMe(completion: self.sendButton.didGetResponseFromServer(state:))
        }) {
            self.confirmateMail()
        }
    }
    
    private func confirmateMail() {
        guard let data = operation.dataFromBase64, let mail = user?.email else {
            return
        }
        let vc = InputAlertViewController(inpurt: .sendMessage(mail: mail, data: data))
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @objc private func shareCheque() {
        guard let data = operation.dataFromBase64,
              let dataPdf = PDFDocument(data: data)?.dataRepresentation() else {
            return
        }
        
        let activityController = UIActivityViewController(activityItems: [dataPdf], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true)
    }
    
    private func getMe(completion: @escaping(Bool) -> Void) {
        NetworkService.User.getUserMeInfo() { (result) in
            switch result {
                
            case .success(let response):
                self.user = response.data
                completion(true)
            case .failure(let error):
                self.showError(message: error.localizedDescription)
                completion(false)
            }
        }
    }
    
    
    
}

extension PdfViewController: InputAlertDelegate, MFMailComposeViewControllerDelegate {
    func send() {
        guard let data = operation.dataFromBase64, let mail = user?.email else {
            return
        }
        self.sendEmail(mail: mail, data: data)
    }
    
    func sendEmail(mail: String, data: Data?){
        if( MFMailComposeViewController.canSendMail() ) {
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self

            mailComposer.setToRecipients([mail])
            mailComposer.setSubject("Ziraat Bank")
            mailComposer.setMessageBody("", isHTML: true)

            if let fileData = data {
                mailComposer.addAttachmentData(fileData, mimeType: "application/pdf", fileName: "cheque.pdf")
           }
            
           self.present(mailComposer, animated: true, completion: nil)
                return
        }
        print("Email is not configured")

    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
