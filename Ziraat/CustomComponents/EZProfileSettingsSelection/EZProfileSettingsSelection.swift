//
//  EZProfileSettingsSelection.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class EZProfileSettingsSelection: UIView, EZProfileSettingsSelectionInstaller {
    var mainView: UIView { self }
    var backView: UIView!
    var actionTitleLabel: UILabel!
    var arrowImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
    
    func setTitle(title: String) -> EZProfileSettingsSelection {
        let view = EZProfileSettingsSelection(frame: .zero)
        view.actionTitleLabel.text = title
        return view
    }
}
