//
//  EZProfileSettingsSelectionInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit

protocol EZProfileSettingsSelectionInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var actionTitleLabel: UILabel! { get set }
    var arrowImageView: UIImageView! { get set }
}

extension EZProfileSettingsSelectionInstaller {
    func initSubviews() {
        backView = UIView()
        backView.layer.cornerRadius = Adaptive.val(6)
        backView.layer.borderWidth = Adaptive.val(1)
        backView.layer.borderColor = UIColor.black.cgColor
        backView.backgroundColor = .white
        
        actionTitleLabel = UILabel()
        actionTitleLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(15))
        actionTitleLabel.textColor = .black
        
        arrowImageView = UIImageView()
        arrowImageView.image = UIImage(named: "img_icon_arr")
    }
    
    func embedSubviews() {
        mainView.addSubview(backView)
        mainView.addSubview(actionTitleLabel)
        mainView.addSubview(arrowImageView)
    }
    
    func addSubviewsConstraints() {
        backView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(42))
            maker.leading.equalToSuperview()//.offset(Adaptive.val(22))
            maker.trailing.equalToSuperview()//.offset(-Adaptive.val(22))
        }
        
        actionTitleLabel.snp.remakeConstraints { (maker) in
            maker.centerY.equalTo(backView)
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(15))
        }
        
        arrowImageView.snp.remakeConstraints { (maker) in
            maker.width.equalTo(Adaptive.val(12))
            maker.height.equalTo(Adaptive.val(21))
            maker.centerY.equalTo(backView)
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(20))
        }
    }
}

fileprivate struct Size {
    
}

