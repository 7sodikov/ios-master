//
//  Button.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialActivityIndicator

final public class Button: BaseButton {
    
    internal typealias Attributes = [NSAttributedString.Key: Any]
    
    internal var style: Style
    internal let fontPalette = Fonts()
    internal let colorPalette = Colors()
    
    public override var intrinsicContentSize: CGSize {
        return style.intrinsicContentSize
    }
    
    internal var defaultSpacing: CGFloat {
        10
    }
    
    // MARK: - Initializer
    
    public init(method: ButtonConnection, style: Style) {
        self.style = style
        super.init(frame: .zero, method: method)
        load()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = style.cornerRadius
    }
    
    public override var isEnabled: Bool {
        didSet {
            super.isEnabled = self.isEnabled
            backgroundColor = (self.isEnabled) ? style.backgroundColor : style.backgroundColor.withAlphaComponent(0.3)
        }
    }
    
    internal var styleImage: UIImage? {
        UIImage(named: style.iconName)?.resize(targetSize: style.iconSize)
    }
    
    internal override func load() {
        super.load()
        
        backgroundColor = (self.isEnabled) ? style.backgroundColor : style.backgroundColor.withAlphaComponent(0.3)
        
        if indicator == nil {
            indicator = style.indicator(self)
            indicator?.isHidden = true
        }
        
        if self.style.hasBorder {
            layer.borderWidth = 2
            layer.borderColor = UIColor.buttonBlack.cgColor
        } else {
            layer.borderWidth = 0
        }
        
    }
    
    public func updateStyle(_ style: Style) {
        self.style = style
        load()
        setup()
    }

    
    public func setup(title: String? = nil) {
        let title = (title ?? style.title) ?? ""
        
        switch style {
        case .general, .custom, .tranparentBackground, .cancel, .back, .nextGray:
            setup(title: title, color: colorPalette.black, font: fontPalette.size14)
        
        case .confirm,
             .next,
             .close,
             .red:
            setup(title: title, color: colorPalette.white, font: fontPalette.size14)
        }
    }
    
    fileprivate func configureShortWhiteStyle() {
        imageView?.tintColor = .white
        
        set(image: styleImage?.withRenderingMode(.alwaysTemplate),
            attributedTitle: title(fontPalette.size14, colorPalette.black),
            at: .right,
            width: defaultSpacing, state: .normal)
    }
    
    fileprivate func configureShortClearStyle() {
        set(image: styleImage,
            attributedTitle: title(fontPalette.size14, colorPalette.white),
            at: .right,
            width: defaultSpacing, state: .normal)
    }
    
    fileprivate func configureExtendedCenteredStyle() {
        set(image: styleImage,
            attributedTitle: title(fontPalette.size14, colorPalette.black),
            at: .right,
            width: defaultSpacing,
            state: .normal)
    }
    
    fileprivate func configureExtendedStyle() {
        contentHorizontalAlignment = .left
        set(image: styleImage, attributedTitle: title(fontPalette.size14, colorPalette.black), at: .right, width: 0, state: .normal)
       
        updateEdgeInsets()
    }
    
    fileprivate func configureShortStyle() {
        contentHorizontalAlignment = .left
        titleLabel?.numberOfLines = 2
        set(image: styleImage, attributedTitle: title(fontPalette.size14, colorPalette.black75), at: .right, width: 0, state: .normal)
        updateEdgeInsets()
    }
    
    // MARK: - Supporting methods
    
    fileprivate func title(_ font: UIFont, _ color: UIColor) -> NSAttributedString {
        NSAttributedString(string: style.title ?? "", attributes: attributes(font, color))
    }
    
    fileprivate func attributes(_ font: UIFont, _ color: UIColor) -> Attributes {
        [.font: font, .foregroundColor: color]
    }
    
    fileprivate func updateEdgeInsets() {
        contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
    }
}


extension Button {
    
    struct Fonts {
        let size14 = UIFont.gothamNarrow(size: 14, .medium)
    }
    
    struct Colors {
        let black = UIColor.buttonBlack
        let black75 = UIColor.buttonBlack.withAlphaComponent(0.75)
        let white = UIColor.white
    }
    
}


//public extension UIButton {
//
//    enum ButtonPosition: Int {
//        case top, bottom, left, right
//    }
//    /// This method sets an image and title for a UIButton and
//    ///   repositions the titlePosition with respect to the button image.
//    ///
//    /// - Parameters:
//    ///   - image: Button image
//    ///   - title: Button title
//    ///   - titlePosition: UIViewContentModeTop, UIViewContentModeBottom, UIViewContentModeLeft or UIViewContentModeRight
//    ///   - additionalSpacing: Spacing between image and title
//    ///   - state: State to apply this behaviour
//    func set(image: UIImage?, title: String, titlePosition: ButtonPosition, additionalSpacing: CGFloat, state: UIControl.State) {
//        imageView?.contentMode = .center
//        setImage(image, for: state)
//        setTitle(title, for: state)
//        titleLabel?.contentMode = .center
//        adjust(title: title as NSString, at: titlePosition, with: additionalSpacing)
//    }
//    /// This method sets an image and an attributed title for a UIButton and
//    ///   repositions the titlePosition with respect to the button image.
//    ///
//    /// - Parameters:
//    ///   - image: Button image
//    ///   - title: Button attributed title
//    ///   - titlePosition: UIViewContentModeTop, UIViewContentModeBottom, UIViewContentModeLeft or UIViewContentModeRight
//    ///   - additionalSpacing: Spacing between image and title
//    ///   - state: State to apply this behaviour
//    func set(image: UIImage?,
//             attributedTitle title: NSAttributedString,
//             at position: ButtonPosition,
//             width spacing: CGFloat = 0,
//             state: UIControl.State = .normal) {
//        imageView?.contentMode = .scaleAspectFit
//        setImage(image, for: state)
//        adjust(attributedTitle: title, at: position, with: spacing)
//        titleLabel?.contentMode = .center
//        setAttributedTitle(title, for: state)
//    }
//
//    // MARK: Private Methods
//    private func adjust(title: NSString, at position: ButtonPosition, with spacing: CGFloat) {
//        let imageRect: CGRect = self.imageRect(forContentRect: frame)
//
//        // Use predefined font, otherwise use the default
//        let titleFont: UIFont = titleLabel?.font ?? UIFont()
//        let titleSize: CGSize = title.size(withAttributes: [NSAttributedString.Key.font: titleFont])
//
//        arrange(titleSize: titleSize, imageRect: imageRect, atPosition: position, withSpacing: spacing)
//    }
//
//    private func adjust(attributedTitle: NSAttributedString, at position: ButtonPosition, with spacing: CGFloat) {
//        let imageRect: CGRect = self.imageRect(forContentRect: frame)
//        let titleSize = attributedTitle.size()
//
//        arrange(titleSize: titleSize, imageRect: imageRect, atPosition: position, withSpacing: spacing)
//    }
//
//    private func arrange(titleSize: CGSize, imageRect:CGRect, atPosition position: ButtonPosition, withSpacing spacing: CGFloat) {
//        switch (position) {
//        case .top:
//            titleEdgeInsets = UIEdgeInsets(top: -(imageRect.height + titleSize.height + spacing), left: -(imageRect.width), bottom: 0, right: 0)
//            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
//            contentEdgeInsets = UIEdgeInsets(top: spacing / 2 + titleSize.height, left: -imageRect.width/2, bottom: 0, right: -imageRect.width/2)
//        case .bottom:
//            titleEdgeInsets = UIEdgeInsets(top: (imageRect.height + titleSize.height + spacing), left: -(imageRect.width), bottom: 0, right: 0)
//            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
//            contentEdgeInsets = UIEdgeInsets(top: 0, left: -imageRect.width/2, bottom: spacing / 2 + titleSize.height, right: -imageRect.width/2)
//        case .left:
//            titleEdgeInsets = UIEdgeInsets(top: 0, left: -(imageRect.width * 2), bottom: 0, right: 0)
//            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -(titleSize.width * 2 + spacing))
//            contentEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing)
//
//        case .right:
//            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -spacing)
//            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing / 2)
//        }
//    }
//}
