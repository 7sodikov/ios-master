//
//  ButtonStyle.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import Rswift
import MaterialComponents.MaterialActivityIndicator

extension Button {
    
    public enum Style {
        case general, red, tranparentBackground, next, cancel, confirm, back, nextGray, close
        
        case custom
        
        var iconName: String {
            switch self {
            
            default:
                return  .init()
            }
        }
        
        var title: String? {
            switch self {
            case .next:
                return RS.btn_further.localized().uppercased()
                
            case .cancel:
                return RS.lbl_cancel.localized().uppercased()
                
            case .confirm:
                return RS.btn_confirm.localized().uppercased()
            
            case .back:
                return RS.lbl_back.localized().uppercased()
            case .close:
                return RS.btn_close.localized().uppercased()
                
            default:
                return nil
            }
        }
        
        var backgroundColor: UIColor {
            switch self {
            case  .general:
                return .white
                
            case .red, .next, .confirm, .close:
                return .buttonRed
                
            case .tranparentBackground, .cancel, .back:
                return .clear
                
            case .custom:
                return .clear
                
            case .nextGray:
                return .buttonGray
            }
        }
        
        var intrinsicContentSize: CGSize {
            switch self {
            
            default:
                let width = UIScreen.main.bounds.width - 32
                return CGSize(width: width, height: 50)
            }
        }
        
        var cornerRadius: CGFloat {
            switch self {
            default:
                return intrinsicContentSize.height/2
            }
        }
        
        var iconSize: CGSize {
            switch self {
            
            default:
                return .zero
            }
        }
        
        
        func indicator(_ button: UIButton) -> MDCActivityIndicator? {
            let indicator = MDCActivityIndicator()
            indicator.tintColor = indicatorTintColor
            indicator.cycleColors = [indicatorTintColor]
            indicator.radius = UI.Indicator.radius
            indicator.isHidden = true
            indicator.translatesAutoresizingMaskIntoConstraints = false
            
            switch self {
            case .general,
                 .confirm,
                 .next,
                 .custom,
                 .close,
                 .tranparentBackground:
                button.addSubview(indicator)
                NSLayoutConstraint.activate([
                    indicator.centerYAnchor.constraint(equalTo: button.centerYAnchor),
                    indicator.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: -UI.Indicator.trailing)
                ])
                
            default:
                return nil
            }
            return indicator
        }
        
        private var indicatorTintColor: UIColor {
            switch self {
            case .general, .confirm, .next, .close:
                return UIColor.white
                
            case .custom, .tranparentBackground:
                return UIColor.buttonBlack
                
            default:
                return UIColor.clear
            }
        }
        
        var hasBorder: Bool {
            [Style.back, .general, .cancel, .tranparentBackground, .nextGray].contains(self)
        }
        
    }
    
}

fileprivate struct UI {
    
    struct Indicator {
        static let trailing: CGFloat = 20
        static let radius: CGFloat = 20
    }
    
}
