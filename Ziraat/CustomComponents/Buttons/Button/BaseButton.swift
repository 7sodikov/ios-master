//
//  BaseButton.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/12/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import MaterialComponents.MDCActivityIndicator

public class BaseButton: UIButton {
    
    // MARK: - Enums
    
    public enum ButtonConnection {
        case local
        case network
    }
    
    public enum ButtonInteraction {
        case locked
        case unlocked
        
        func act(completion: @escaping () -> Void) {
            guard self == .unlocked else { return }
            
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    // MARK: - Properties
    
    private let kSuccessful = true
    
    internal let method: ButtonConnection
    internal var interaction: ButtonInteraction
    internal var indicator: MDCActivityIndicator?
    internal var lockCompletion: (() -> Void)?
    internal var unlockCompletion: (() -> Void)?
    internal var errorCompletion: (() -> Void)?
    internal var didClickCompletion: (() -> Void)?
    
    // MARK: - Initializer
    
    public init(frame: CGRect, method: ButtonConnection) {
        self.method = method
        interaction = .unlocked
        super.init(frame: frame)
        load()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Public methods
    
    public func onError(completion: @escaping () -> Void) {
        errorCompletion = completion
    }
    
    public func didClick(_ completion: @escaping () -> Void) {
        didClickCompletion = completion
    }
    
    public func on(lock lockCompletion: @escaping () -> Void, unlockCompletion: @escaping () -> Void) {
//        guard AppPreferences.shared.isNetworkReachable() else {
//        guard ConnectionManager.shared.isReachable else {
//            lockCompletion()
//            return
//        }
        self.lockCompletion = lockCompletion
        self.unlockCompletion = unlockCompletion
        lock()
    }
    
    @objc public func didGetResponseFromServer(state: Bool) {
        unlock(with: state)
    }
    
    internal func load() {
        addTarget(self, action: #selector(clicked), for: .touchUpInside)
    }
}


internal extension BaseButton {
    
    func lock() {
        interaction = .locked
        lockCompletion?()
        
        if method == .network {
            indicator?.isHidden = false
            indicator?.startAnimating()
        }
    }
    
    func unlock(with state: Bool) {
        interaction = .unlocked
        isUserInteractionEnabled = true
        indicator?.stopAnimating()
        indicator?.isHidden = true
        
        DispatchQueue.main.async { [weak self] in
            if state == self?.kSuccessful {
                guard let unlocked = self?.unlockCompletion else { return }
                self?.unlockCompletion = nil
                unlocked()
            } else {
                self?.errorCompletion?()
            }
        }
    }
    
    func setup(title: String, color: UIColor = .buttonBlack, font: UIFont) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        setAttributedTitle(NSAttributedString(string: title,
                                              attributes: [.font: font,
                                                           .foregroundColor: color,
                                                           .paragraphStyle: paragraphStyle]), for: .normal)
        
        setAttributedTitle(NSAttributedString(string: title,
                                              attributes: [.font: font,
                                                           .foregroundColor: color.withAlphaComponent(0.3),
                                                           .paragraphStyle: paragraphStyle]), for: .disabled)
    }
}


private extension BaseButton {
    
    @objc func clicked() {
        guard isUserInteractionEnabled else { return }
        isUserInteractionEnabled = false
        
        interaction.act {
            self.didClickCompletion?()
            self.isUserInteractionEnabled = true
        }
    }
    
}
