//
//  NextButton.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 8/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

class NextButton: UIButton {
    var animateIndicator: Bool {
        get {
            indicator.isAnimating
        }
        set {
            if newValue {
                indicator.startAnimating()
                isEnabled = false
            } else {
                indicator.stopAnimating()
                isEnabled = true
            }
        }
    }
    
    private var indicator: MDCActivityIndicator = {
        let indicator = MDCActivityIndicator()
        indicator.cycleColors = [.white]
        indicator.tintColor = .white
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setPreloader(for: 20)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setPreloader(for btnCornerRadius: CGFloat) {
        addSubview(indicator)
        indicator.radius = btnCornerRadius * 0.45
        
        indicator.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(indicator.radius * 2)
            maker.centerY.equalToSuperview()
            maker.right.equalTo(-btnCornerRadius * 0.45)
        }
    }
    
    static func systemButton(with title: String, cornerRadius: CGFloat) -> NextButton {
        let btn = NextButton(frame: .zero)
        btn.layer.masksToBounds = true
        btn.setTitleColor(.white, for: .normal)
        btn.setBackgroundColor(ColorConstants.gray, for: .normal)
        btn.setBackgroundColor(ColorConstants.highlightedGray, for: .highlighted)
        btn.layer.cornerRadius = cornerRadius
        btn.setTitle(title, for: .normal)
        btn.titleLabel?.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(20))
        btn.setPreloader(for: cornerRadius)
        return btn
    }
}
