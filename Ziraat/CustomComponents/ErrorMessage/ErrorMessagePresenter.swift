//
//  ErrorMessagePresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ErrorMessagePresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: ErrorMessageVM { get }
}

protocol ErrorMessageInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class ErrorMessagePresenter: ErrorMessagePresenterProtocol {
    weak var view: ErrorMessageVCProtocol!
    var interactor: ErrorMessageInteractorProtocol!
    var router: ErrorMessageRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = ErrorMessageVM()
    
    // Private property and methods
    
}

extension ErrorMessagePresenter: ErrorMessageInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
