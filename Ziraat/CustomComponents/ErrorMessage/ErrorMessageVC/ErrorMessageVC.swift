//
//  ErrorMessageVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ErrorMessageVCProtocol: class {
    
}

class ErrorMessageVC: UIViewController, ErrorMessageViewInstaller {
    var mainView: UIView { view }
    var backView: UIView!
    var exclamationMarkImageView: UIImageView!
    var mainLabel: UILabel!
    var buttonStackView: UIStackView!
    var yesButton: NextButton!
    
    var textMessage: String?
    
    var presenter: ErrorMessagePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        
        mainLabel.text = textMessage
        
        yesButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
    }
    
    @objc func closeButtonClicked() {
        self.dismiss(animated: false, completion: nil)
    }
}

extension ErrorMessageVC: ErrorMessageVCProtocol {
    
}
