//
//  ErrorMessageInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol ErrorMessageInteractorProtocol: class {
    
}

class ErrorMessageInteractor: ErrorMessageInteractorProtocol {
    weak var presenter: ErrorMessageInteractorToPresenterProtocol!
    
}
