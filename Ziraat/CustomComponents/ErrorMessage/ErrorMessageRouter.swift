//
//  ErrorMessageRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 12/23/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol ErrorMessageRouterProtocol: class {
    static func createModule(message: String?) -> UIViewController
}

class ErrorMessageRouter: ErrorMessageRouterProtocol {
    static func createModule(message: String?) -> UIViewController {
        let vc = ErrorMessageVC()
        let presenter = ErrorMessagePresenter()
        let interactor = ErrorMessageInteractor()
        let router = ErrorMessageRouter()
        
        vc.presenter = presenter
        vc.textMessage = message
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
    
}
