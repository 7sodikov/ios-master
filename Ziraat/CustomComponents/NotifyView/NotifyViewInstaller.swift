//
//  NotifyViewInstaller.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

protocol NotifyViewInstaller: ViewInstaller {
    var container: UIView! {get set}
    var imageView: UIImageView! {get set}
    var textLabel: UILabel! {get set}
    var topButton: UIButton! {get set}
    var bottomButton: UIButton! {get set}
    var stackButtonView: UIStackView! {get set}
    var containerHeightConstaraint: NSLayoutConstraint! {get set}
}

extension NotifyViewInstaller {
    
    func initSubviews() {
        container = UIView()
        container.backgroundColor = .white
        container.layer.cornerRadius = 12
        container.addShadow()
        
        imageView = UIImageView()
        imageView.layer.cornerRadius = 40
        imageView.addShadow()
        
        textLabel = UILabel()
        textLabel.font = .gothamNarrow(size: 14, .book)
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byWordWrapping
        textLabel.textAlignment = .center
        
        topButton = UIButton()
        topButton.layer.cornerRadius = Adaptive.val(25)
        topButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        topButton.backgroundColor = UIColor(225,5,20)
        topButton.setTitleColor(.white, for: .normal)
        
        bottomButton = UIButton()
        bottomButton.layer.cornerRadius = Adaptive.val(25)
        bottomButton.titleLabel?.font = .gothamNarrow(size: 14, .bold)
        bottomButton.backgroundColor = .clear
        bottomButton.setTitleColor(.black, for: .normal)
        bottomButton.layer.borderWidth = 2
        bottomButton.layer.borderColor = UIColor(68,80,86).cgColor
        
        stackButtonView = UIStackView()
        stackButtonView.axis = .vertical
        stackButtonView.spacing = 4
        
        mainView.backgroundColor = .clear
        
        if let notify = parameter as? NotifyViewController.Notify {
            topButton.setTitle(notify.buttonTitle.top, for: .normal)
            topButton.isHidden = notify.buttonTitle.top.isEmpty
            
            bottomButton.setTitle(notify.buttonTitle.bottom, for: .normal)
            bottomButton.isHidden = notify.buttonTitle.bottom.isEmpty
            
            textLabel.text = notify.notifyText
            
            imageView.image = UIImage(named: notify.image)
        }
    }
    
    func embedSubviews() {
        mainView.addSubview(container)
        container.addSubview(imageView)
        container.addSubview(textLabel)
        container.addSubview(stackButtonView)
        
        stackButtonView.addArrangedSubview(topButton)
        stackButtonView.addArrangedSubview(bottomButton)
    }
    
    func addSubviewsConstraints() {
        var height: CGFloat = 268
        if let notify = parameter as? NotifyViewController.Notify {
            height += notify.notifyText.height(withConstrainedWidth: CGFloat(290))
            height -= notify.anyButtonIsHidden ? 58 : 0
        }
        container.constraint { (make) in
            make.width(312).height(height).center.equalToSuperView()
        }
        
        imageView.constraint { (make) in
            make.square(80).top(24).centerX.equalToSuperView()
        }
        textLabel.constraint { (make) in
            make.top(self.imageView.bottomAnchor, offset: 16)
                .horizontal(16).equalToSuperView()
        }
        stackButtonView.constraint { (make) in
            make.top(self.textLabel.bottomAnchor, offset: 24)
            .bottom(-16).horizontal(16).equalToSuperView()
        }
        
        topButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bottomButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    
}

fileprivate extension UIButton {
    
    func update(_ notify: NotifyViewController.Notify) {
        
    }
    
}
