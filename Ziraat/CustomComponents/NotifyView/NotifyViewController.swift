//
//  NotifyViewController.swift
//  Ziraat
//
//  Created by Jasur Amirov on 4/7/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

class NotifyViewController: UIViewController, NotifyViewInstaller {
    var container: UIView!
    var imageView: UIImageView!
    var textLabel: UILabel!
    var topButton: UIButton!
    var bottomButton: UIButton!
    var stackButtonView: UIStackView!
    var containerHeightConstaraint: NSLayoutConstraint!
    
    var topButtonAction: (()->Void)? = nil
    
    var parameter: Any? { self.notify }
    var mainView: UIView { self.view }
    
    var animateDissmissing: Bool = false
    let notify: Notify
    
    init(_ notify: Notify, completionHandler: @escaping() -> Void) {
        self.notify = notify
        self.topButtonAction = completionHandler
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        setupSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topButton.addTarget(self, action: #selector(topButtonClicked), for: .touchUpInside)
        bottomButton.addTarget(self, action: #selector(bottomButtonClicked), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        container.isHidden = false
    }
    
    @objc func topButtonClicked() {
        self.dismiss(animated: animateDissmissing) {
            self.topButtonAction?()
        }
    }
    
    @objc func bottomButtonClicked() {
        self.dismiss(animated: animateDissmissing, completion: nil)
    }

}

extension NotifyViewController {
    
    enum Notify {
        case confirmationConversion
        case noConnection
        case changeStatus
        // a red button
        case custom(message: String)
//        case sendMessage(mail: String)
        
        var buttonTitle: (top: String, bottom: String) {
            switch self {
            
            case .confirmationConversion, .changeStatus:
                return (RS.btn_confirm.localized(), RS.lbl_cancel.localized())
            case .noConnection, .custom:
                return (RS.btn_close.localized() ,"")

//            case .sendMessage:
//                return (RS.btn_send_by_email.localized().uppercased(), RS.lbl_cancel.localized().uppercased())
            }
        }
        
        var notifyText: String {
            switch self {
                
            case .confirmationConversion:
                return RS.lbl_sure_cancel_transaction.localized()
            case .noConnection:
                return RS.al_msg_no_connection.localized()
            case .custom(let message):
                return message
            case .changeStatus:
                return RS.al_msg_change_status.localized()
            }
        }
        
        var image: String {
            switch self {
            case .confirmationConversion, .noConnection, .custom, .changeStatus:
                return "img_excl_mark"
            }
        }
        
        var anyButtonIsHidden: Bool {
            buttonTitle.top.isEmpty || buttonTitle.bottom.isEmpty
        }
        
    }
    
}
