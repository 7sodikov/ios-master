//
//  BottomPanelSettingsPresenter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/5/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BottomPanelSettingsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var viewModel: BottomPanelSettingsVM { get }
    func languageSegmentClicked(for index: Int)
    func actionClicked(_ index : Int)
}

protocol BottomPanelSettingsInteractorToPresenterProtocol: class {
    // INTERACTOR -> PRESENTER
    
}

class BottomPanelSettingsPresenter: BottomPanelSettingsPresenterProtocol {
    var delegate:BottomPanelSetttingsDelegate?
    weak var view: BottomPanelSettingsVCProtocol!
    var interactor: BottomPanelSettingsInteractorProtocol!
    var router: BottomPanelSettingsRouterProtocol!
    
    // VIEW -> PRESENTER
    private(set) var viewModel = BottomPanelSettingsVM()
    var selectedLanguage: Language = .english
    
    // Private property and methods
    
    func languageSegmentClicked(for index: Int) {
//        switch index {
//        case 0: selectedLanguage = .turkish
//        case 1: selectedLanguage = .english
//        case 2: selectedLanguage = .russian
//        case 3: selectedLanguage = .uzbek
//        default:
//            selectedLanguage = .english
//        }
    }
    
    func actionClicked(_ index: Int) {
        view.dismiss()
        self.delegate?.actionClicked(index)
    }
}

extension BottomPanelSettingsPresenter: BottomPanelSettingsInteractorToPresenterProtocol {
    // INTERACTOR -> PRESENTER
    
}
