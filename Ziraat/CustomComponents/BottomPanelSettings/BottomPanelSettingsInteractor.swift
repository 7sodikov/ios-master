//
//  BottomPanelSettingsInteractor.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/5/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

protocol BottomPanelSettingsInteractorProtocol: class {
    
}

class BottomPanelSettingsInteractor: BottomPanelSettingsInteractorProtocol {
    weak var presenter: BottomPanelSettingsInteractorToPresenterProtocol!
    
}
