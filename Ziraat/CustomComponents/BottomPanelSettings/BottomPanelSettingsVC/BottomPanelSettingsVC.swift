//
//  BottomPanelSettingsVC.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/5/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BottomPanelSettingsVCProtocol: class {
    func dismiss()
}

class BottomPanelSettingsVC: BaseViewController, BottomPanelSettingsViewInstaller, EZLanguageSelectionControlDelegate {
    var backView: UIView!
    var aboutApplicationStackView: UIStackView!
    var aboutApplicationImage: UIImageView!
    var aboutApplicationLabel: UILabel!
    var aboutApplicationButton: UIButton!
    var recommendStackView: UIStackView!
    var recommendImage: UIImageView!
    var recommendLabel: UILabel!
    var recommendButton: UIButton!
    var languageSelectionStackView: UIStackView!
    var languageSelectionImage: UIImageView!
    var languageSelectionLabel: UILabel!
    var languageButtonsView: EZLanguageSelectionControl!
    var closeStackView: UIStackView!
    var closeImageView: UIImageView!
    var closeLabel: UILabel!
    var closeButton: UIButton!
    var mainStackView: UIStackView!
    var mainView: UIView { view }
    var presenter: BottomPanelSettingsPresenterProtocol!
    var bgBtn: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setupSubviews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        localizeText()

        languageButtonsView.delegate = self
        bgBtn.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
        aboutApplicationButton.addTarget(self, action: #selector(self.aboutClicked), for: .touchUpInside)
        recommendButton.addTarget(self, action: #selector(shareClicked), for: .touchUpInside)
        
    }
    
    func dismiss(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func aboutClicked(){
        self.presenter.actionClicked(0)
    }
    
    @objc func shareClicked(){
        self.presenter.actionClicked(1)
    }
    
    @objc func closeButtonClicked() {
        NotificationCenter.default.post(name: Notification.Name("BarItem"), object: nil)
        self.presenter.actionClicked(3)
    }
    
    @objc func updateView(_ notification:Notification) {
        setNeedsStatusBarAppearanceUpdate()
        NotificationCenter.default.post(name: Notification.Name("BarItem"), object: nil)
        dismiss(animated: false, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if LocalizationManager.language == Language.turkish {
            languageButtonsView.selectedIndex = 0
        } else if LocalizationManager.language == Language.english {
            languageButtonsView.selectedIndex = 1
        } else if LocalizationManager.language == Language.russian {
            languageButtonsView.selectedIndex = 2
        } else {
            languageButtonsView.selectedIndex = 3
        }
    }
    
    @objc func aboutButtonClicked() {        
        dismiss(animated: false, completion: nil)
    }
    
    func languageSelected(at language: Language) {
        LocalizationManager.language = language
        localizeText()
        appDelegate.window?.rootViewController = LoginInitRouter.createModule()
        appDelegate.window?.makeKeyAndVisible()
    }
}

extension BottomPanelSettingsVC: BottomPanelSettingsVCProtocol {
    
}
