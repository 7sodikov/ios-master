//
//  BottomPanelSettingsViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/5/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol BottomPanelSettingsViewInstaller: ViewInstaller {
    var backView: UIView! { get set }
    var aboutApplicationStackView: UIStackView! { get set }
    var aboutApplicationImage: UIImageView! { get set }
    var aboutApplicationLabel: UILabel! { get set }
    var aboutApplicationButton: UIButton! { get set }
    var recommendStackView: UIStackView! { get set }
    var recommendImage: UIImageView! { get set }
    var recommendLabel: UILabel! { get set }
    var recommendButton: UIButton! { get set }
    var languageSelectionStackView: UIStackView! { get set }
    var languageSelectionImage: UIImageView! { get set }
    var languageSelectionLabel: UILabel! { get set }
    var languageButtonsView: EZLanguageSelectionControl! { get set }
    var closeStackView: UIStackView! { get set }
    var closeImageView: UIImageView! { get set }
    var closeLabel: UILabel! { get set }
    var closeButton: UIButton! { get set }
    var mainStackView: UIStackView! { get set }
    var bgBtn: UIButton! { get set }
}

extension BottomPanelSettingsViewInstaller {
    func initSubviews() {
        bgBtn = UIButton()
        bgBtn.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        mainView.backgroundColor = .clear
        
        backView = UIView()
        backView.backgroundColor = .white
        
        aboutApplicationStackView = UIStackView()
        aboutApplicationStackView.axis = .horizontal
        aboutApplicationStackView.spacing = Size.betweenRows
        
        aboutApplicationImage = UIImageView()
        aboutApplicationImage.image = UIImage(named: "img_login_info")
        
        aboutApplicationLabel = UILabel()
        aboutApplicationLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        aboutApplicationLabel.textColor = .black
        
        aboutApplicationButton = UIButton()
        aboutApplicationButton.setTitle("", for: .normal)
        
        recommendStackView = UIStackView()
        recommendStackView.axis = .horizontal
        recommendStackView.spacing = Size.betweenRows
        
        recommendImage = UIImageView()
        recommendImage.image = UIImage(named: "btn_icon_iban")
        
        recommendLabel = UILabel()
        recommendLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        recommendLabel.textColor = .black
        
        recommendButton = UIButton()
        recommendButton.setTitle("", for: .normal)
        
        languageSelectionStackView = UIStackView()
        languageSelectionStackView.axis = .horizontal
        languageSelectionStackView.spacing = Size.betweenRows
        
        languageSelectionImage = UIImageView()
        languageSelectionImage.image = UIImage(named: "img_login_language")
        
        languageSelectionLabel = UILabel()
        languageSelectionLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        languageSelectionLabel.textColor = .black
        languageSelectionLabel.isUserInteractionEnabled = true
        
        languageButtonsView = EZLanguageSelectionControl()
        languageButtonsView.addSegment(with: "TR", language: .turkish)
        languageButtonsView.addSegment(with: "EN", language: .english)
        languageButtonsView.addSegment(with: "RU", language: .russian)
        languageButtonsView.addSegment(with: "UZ", language: .uzbek)
        languageButtonsView.selectedIndex = 0
        
        closeStackView = UIStackView()
        closeStackView.axis = .horizontal
        closeStackView.spacing = Size.betweenRows
        
        closeImageView = UIImageView()
        closeImageView.image = UIImage(named: "img_login_close")
        
        closeLabel = UILabel()
        closeLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(17))
        closeLabel.textColor = .black
        
        closeButton = UIButton()
        closeButton.setTitle("", for: .normal)
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.spacing = Size.betweenLines
        
    }
    
    func localizeText() {
        aboutApplicationLabel.text = RS.lbl_about_app.localized()
        recommendLabel.text = RS.lbl_recommend.localized()
        languageSelectionLabel.text = RS.lbl_select_lang.localized()
        closeLabel.text = RS.btn_close.localized()
    }
    
    func embedSubviews() {
        mainView.addSubview(bgBtn)
        mainView.addSubview(backView)
        mainView.addSubview(mainStackView)
        
        mainStackView.addArrangedSubview(aboutApplicationStackView)
        aboutApplicationStackView.addArrangedSubview(aboutApplicationImage)
        aboutApplicationStackView.addArrangedSubview(aboutApplicationLabel)
        mainView.addSubview(aboutApplicationButton)
        
        mainStackView.addArrangedSubview(recommendStackView)
        recommendStackView.addArrangedSubview(recommendImage)
        recommendStackView.addArrangedSubview(recommendLabel)
        mainView.addSubview(recommendButton)
        
        mainStackView.addArrangedSubview(languageSelectionStackView)
        languageSelectionStackView.addArrangedSubview(languageSelectionImage)
        languageSelectionStackView.addArrangedSubview(languageSelectionLabel)
        
        mainView.addSubview(languageButtonsView)
        
        mainStackView.addArrangedSubview(closeStackView)
        closeStackView.addArrangedSubview(closeImageView)
        closeStackView.addArrangedSubview(closeLabel)
        mainView.addSubview(closeButton)
    }
    
    func addSubviewsConstraints() {
        bgBtn.snp.remakeConstraints { (make) in
            make.left.right.bottom.top.equalToSuperview()
        }
        
        backView.snp.remakeConstraints { (maker) in
            maker.leading.bottom.trailing.equalTo(0)
            maker.height.equalTo(Adaptive.val(190))
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(27))
            maker.leading.equalTo(backView.snp.leading).offset(Adaptive.val(21))
        }
        
        languageButtonsView.snp.remakeConstraints { (maker) in
            maker.top.equalTo(backView.snp.top).offset(Adaptive.val(101))
            maker.trailing.equalTo(backView.snp.trailing).offset(-Adaptive.val(24))
            maker.width.equalTo(Adaptive.val(131))
        }
        
        aboutApplicationImage.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(19))
        }
        
        aboutApplicationButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(aboutApplicationStackView)
        }
        
        recommendImage.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(19))
        }
        
        recommendButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(recommendStackView)
        }
        
        languageSelectionImage.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(19))
        }
        
        closeImageView.snp.remakeConstraints { (maker) in
            maker.width.height.equalTo(Adaptive.val(19))
        }
        
        closeButton.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(closeStackView)
        }
    }
}

fileprivate struct Size {
    static let betweenRows = Adaptive.val(18)
    static let betweenLines = Adaptive.val(19)
}
