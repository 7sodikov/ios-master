//
//  BottomPanelSettingsRouter.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 11/5/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
protocol BottomPanelSetttingsDelegate: class {
    func actionClicked(_ index : Int)
}

protocol BottomPanelSettingsRouterProtocol: class {
    static func createModule(_ delegate : BottomPanelSetttingsDelegate) -> UIViewController
}

class BottomPanelSettingsRouter: BottomPanelSettingsRouterProtocol {
    static func createModule(_ delegate : BottomPanelSetttingsDelegate) -> UIViewController {
        let vc = BottomPanelSettingsVC()
        let presenter = BottomPanelSettingsPresenter()
        let interactor = BottomPanelSettingsInteractor()
        let router = BottomPanelSettingsRouter()
        presenter.delegate = delegate
        
        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        
        return vc
    }
}
