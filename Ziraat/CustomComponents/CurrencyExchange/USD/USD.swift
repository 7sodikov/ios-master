//
//  USD.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import Foundation
import UIKit

class USDView: UIView, USDViewInstaller {
    var flagImageView: UIImageView!
    var currencyLabel: UILabel!
    var mainView: UIView { self }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
    
    @discardableResult
    func setup(title: String, flag: UIImage) -> USDView {
        flagImageView.image = flag
        currencyLabel.text = title
        return self
    }
}
