//
//  USDViewInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 1/25/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

protocol USDViewInstaller: ViewInstaller {
    var flagImageView: UIImageView! { get set }
    var currencyLabel: UILabel! { get set }
}

extension USDViewInstaller {
    func initSubviews() {
        flagImageView = UIImageView()
        flagImageView.contentMode = .scaleAspectFit
        currencyLabel = UILabel()
        currencyLabel.font = EZFontType.medium.sfuiDisplay(size: Adaptive.val(16))
        currencyLabel.textColor = .black
    }
    
    func embedSubviews() {
        mainView.addSubview(flagImageView)
        mainView.addSubview(currencyLabel)
    }
    
    func addSubviewsConstraints() {
        flagImageView.snp.remakeConstraints() { (maker) in
            maker.width.equalTo(Adaptive.val(21))
            maker.height.equalTo(Adaptive.val(14))
            maker.top.leading.bottom.equalToSuperview()
        }
        
        currencyLabel.snp.remakeConstraints() { (maker) in
            maker.centerY.equalTo(flagImageView)
            maker.leading.equalTo(flagImageView.snp.trailing).offset(4)
        }
    }
}

fileprivate struct Size {
    static let viewHeight = Adaptive.val(54)
}
