//
//  FillFieldUpView.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 6/4/21.
//  Copyright © 2021 TUNE Consulting. All rights reserved.
//

import UIKit

enum ArrowType {
    case up
    case down
}

class FillFieldUpView: UIView {
        
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    static func fillView(with title: String, arrow: ArrowType) -> FillFieldUpView {
        let view = FillFieldUpView(frame: .zero)
        let backImage = UIImageView()
        let titleLabel = UILabel()

        titleLabel.font = .gothamNarrow(size: 12, .medium)
        titleLabel.textColor = .white
        titleLabel.text = title

        view.addSubview(backImage)
        view.addSubview(titleLabel)
        
        backImage.snp.remakeConstraints { (maker) in
            maker.edges.equalTo(view)
        }
        
        if arrow == .up {
            backImage.image = UIImage(named: "img_arr_up")!

            titleLabel.snp.remakeConstraints { (maker) in
                maker.leading.equalToSuperview().offset(Adaptive.val(22))
                maker.bottom.equalToSuperview().offset(-Adaptive.val(10))
            }
        } else {
            backImage.image = UIImage(named: "img_arr_down")!

            titleLabel.snp.remakeConstraints { (maker) in
                maker.leading.equalToSuperview().offset(Adaptive.val(22))
                maker.top.equalToSuperview().offset(Adaptive.val(10))
            }
        }
        
        return view
    }
}
