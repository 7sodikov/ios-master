//
//  DashboardComponentStackInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//


import UIKit
import SnapKit

protocol DashboardComponentStackInstaller: ViewInstaller {
    var uzsCell: DashboardComponent! { get set }
    var usdCell: DashboardComponent! { get set }
    var eurCell: DashboardComponent! { get set }
    var hiddenCell: DashboardComponent! { get set }
    var currencyStackView: UIStackView! { get set }
    var mainStackView: UIStackView! { get set }
}

extension DashboardComponentStackInstaller {
    func initSubviews() {
        uzsCell = DashboardComponent.systemComponent(with: CurrencyType.uzs.data.label, titleColor: .black, backColor: .white)
        
        usdCell = DashboardComponent.systemComponent(with: CurrencyType.usd.data.label, titleColor: .white, backColor: UIColor.black.withAlphaComponent(0.4))
        
        eurCell = DashboardComponent.systemComponent(with: CurrencyType.eur.data.label, titleColor: .white, backColor: UIColor.black.withAlphaComponent(0.4))
        
        hiddenCell = DashboardComponent.systemComponent(with: "", titleColor: .white, backColor: UIColor.black.withAlphaComponent(0.4))
        hiddenCell.componentImage.isHidden = false
        
        currencyStackView = UIStackView()
        currencyStackView.axis = .vertical
        currencyStackView.distribution = .fillEqually
        currencyStackView.alignment = .fill
        
        mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.spacing = DashboardComponentStackInstallerSize.interStackViewSpace
        
    }
    
    func embedSubviews() {
        currencyStackView.addArrangedSubview(uzsCell)
        currencyStackView.addArrangedSubview(usdCell)
        currencyStackView.addArrangedSubview(eurCell)
        mainStackView.addSubview(currencyStackView)
        
        mainStackView.addArrangedSubview(currencyStackView)
        mainStackView.addArrangedSubview(hiddenCell)
        mainView.addSubview(mainStackView)
    }
    
    func addSubviewsConstraints() {
        uzsCell.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(currencyStackView.snp.leading)
            maker.top.equalTo(currencyStackView.snp.top)
            maker.trailing.equalTo(currencyStackView.snp.trailing)
        }
        
        usdCell.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(currencyStackView.snp.leading)
            maker.trailing.equalTo(currencyStackView.snp.trailing)
        }
        
        eurCell.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(currencyStackView.snp.leading)
            maker.bottom.equalTo(currencyStackView.snp.bottom)
            maker.trailing.equalTo(currencyStackView.snp.trailing)
        }
        
        currencyStackView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(mainStackView.snp.leading)
            maker.top.equalTo(mainStackView.snp.top)
            maker.trailing.equalTo(mainStackView.snp.trailing)
        }
        
        hiddenCell.snp.remakeConstraints { (maker) in
            maker.leading.trailing.bottom.equalTo(mainStackView)
        }
        
        mainStackView.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct DashboardComponentStackInstallerSize {
    static let interCellSpace = Adaptive.val(16)
    static let interStackViewSpace = Adaptive.val(51)
    struct Cell {
        static let size = Adaptive.size(45, 45)
    }
}
