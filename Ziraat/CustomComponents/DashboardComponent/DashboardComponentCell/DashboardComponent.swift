//
//  DashboardComponent.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class DashboardComponent: UIControl, DashboardComponentInstaller {
    
    var mainView: UIView { self }
    
    var backgroundView: UIView!
    
    var componentLabel: UILabel!
    
    var componentImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSubviews()
    }
    
    static func systemComponent(with title: String, titleColor: UIColor, backColor: UIColor) -> DashboardComponent {
        let comp = DashboardComponent(frame: .zero)
        comp.backgroundView.backgroundColor = backColor
        comp.componentLabel.text = title
        comp.componentLabel.textColor = titleColor
        comp.componentImage.image = UIImage(named: "img_eye")
        comp.componentImage.isHidden = true
        return comp
    }
    
}
