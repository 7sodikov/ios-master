//
//  DashboardComponentInstaller.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/13/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

protocol DashboardComponentInstaller: ViewInstaller {
    var backgroundView: UIView! { get set }
    var componentLabel: UILabel! { get set }
    var componentImage: UIImageView! { get set }
}

extension DashboardComponentInstaller {
    func initSubviews() {
        backgroundView = UIView()
        backgroundView.layer.cornerRadius = Adaptive.val(16)
        
        componentLabel = UILabel()
        componentLabel.font = EZFontType.bold.sfuiDisplay(size: Adaptive.val(14))
        componentLabel.textAlignment = .center
        
        componentImage = UIImageView()
    }
    
    func embedSubviews() {
        mainView.addSubview(backgroundView)
        mainView.addSubview(componentLabel)
        mainView.addSubview(componentImage)
    }
    
    func addSubviewsConstraints() {
        backgroundView.snp.remakeConstraints { (maker) in
            maker.height.equalTo(Adaptive.val(34))
            maker.width.equalTo(Adaptive.val(52))
        }
        
        componentLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(backgroundView.snp.leading)
            maker.trailing.equalTo(backgroundView.snp.trailing)
            maker.top.equalTo(backgroundView.snp.top)
            maker.bottom.equalTo(backgroundView.snp.bottom)
        }
        
        componentImage.snp.remakeConstraints { (maker) in
            maker.center.equalTo(backgroundView)
            maker.width.equalTo(Adaptive.val(22))
            maker.height.equalTo(Adaptive.val(18))
            
        }
    }
}

