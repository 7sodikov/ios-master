//
//  DashboardComponentStack.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class DashboardComponentStack: UIControl, DashboardComponentStackInstaller {
    var uzsCell: DashboardComponent!
    
    var usdCell: DashboardComponent!
    
    var eurCell: DashboardComponent!
    
    var hiddenCell: DashboardComponent!
    
    var currencyStackView: UIStackView!
    
    var mainStackView: UIStackView!
    
    var layout: UICollectionViewFlowLayout!
    var collectionView: UICollectionView!
    var mainView: UIView { self }
    
    var model = [DashboardComponentModel]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
}

