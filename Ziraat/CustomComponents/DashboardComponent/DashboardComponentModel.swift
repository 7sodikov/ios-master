//
//  DashboardComponentModel.swift
//  Ziraat
//
//  Created by Guzalyor Ikromova on 9/24/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import Foundation

class DashboardComponentModel {
    var nameLabel: String!
    
    init(name: String) {
        self.nameLabel = name
    }
    
}
