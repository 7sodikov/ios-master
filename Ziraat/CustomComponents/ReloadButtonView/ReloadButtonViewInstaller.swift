//
//  ReloadButtonViewInstaller.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

protocol ReloadButtonViewInstaller: ViewInstaller {
    var indicator: MDCActivityIndicator! { get set }
    var reloadButton: UIButton! { get set }
}

extension ReloadButtonViewInstaller {
    func initSubviews() {
        mainView.backgroundColor = .clear
        
        indicator = MDCActivityIndicator()
        indicator.cycleColors = [.white]
        indicator.tintColor = .white
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.radius = ReloadButtonViewSize.Indicator.size/2
        
        reloadButton = UIButton()
        reloadButton.setImage(UIImage(named: "btn_reload"), for: .normal)
    }
    
    func embedSubviews() {
        mainView.addSubview(indicator)
        mainView.addSubview(reloadButton)
    }
    
    func addSubviewsConstraints() {
        indicator.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        
        reloadButton.snp.remakeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
    }
}

struct ReloadButtonViewSize {
    struct Indicator {
        static let size = Adaptive.val(40)
    }
}
