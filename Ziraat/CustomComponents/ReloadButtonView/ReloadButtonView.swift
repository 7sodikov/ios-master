//
//  ReloadButtonView.swift
//  Ziraat
//
//  Created by Shamsiddin on 10/6/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

class ReloadButtonView: UIView, ReloadButtonViewInstaller {
    var mainView: UIView { self }
    var indicator: MDCActivityIndicator!
    var reloadButton: UIButton!
    
    var animateIndicator: Bool {
        get {
            indicator.isAnimating
        }
        set {
            if newValue {
                indicator.startAnimating()
                reloadButton.isHidden = true
            } else {
                indicator.stopAnimating()
                reloadButton.isHidden = false
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    func initialize() {
        setupSubviews()
    }
}
