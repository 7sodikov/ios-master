//
//  LoginTextField.swift
//  Ziraat
//
//  Created by Shamsiddin on 8/17/20.
//  Copyright © 2020 TUNE Consulting. All rights reserved.
//

import UIKit

class LoginTextField: ClipboardControlledTextField {
    var amountFormat = false
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    required init(with placeholder: String,
                     keyboard: UIKeyboardType = .default,
                     cornerRadius: CGFloat,
                     isSecureTextEntry: Bool) {
        super.init(frame: .zero)
        initialize()
        
        let font = UIFont.boldSystemFont(ofSize: (Adaptive.val(16.0)))
        self.isSecureTextEntry = isSecureTextEntry
        keyboardType = keyboard
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
            .foregroundColor: UIColor.lightGray,
            .font: font
        ])
        self.font = font
    }
    
    func initialize() {
        textColor = .black
        autocorrectionType = .no
        backgroundColor = .white
        layer.masksToBounds = true
        setLeftPaddingPoints(Adaptive.val(15))
    }
    
    var safeText: String? {
        get {
            return nil
        }
    }
    
    func set(placeholder: String, color: UIColor = UIColor.lightGray) {
        self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
            .foregroundColor: color,
            .font: self.font as Any
        ])
    }
    
    func textFieldLimit(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 9
    }

}
