//
//  ErrorFactory.swift
//  iTaxi
//
//  Created by Ilkhom on 1/11/18.
//  Copyright © 2018 SmartFuture. All rights reserved.
//

import Foundation

//enum AppError: Error {
//    case serverError(code: Int, desc: String?, userInfo: [String: Any?]?)
//    case serverJSONError(JSON: Params)
//    case networkError
//    case wrongData
//    case notJSON
//    case noData
//    case badInput
//    case dictObjectNotFound(key: String)
//    case unexpectedResponseStructure
//    case unexpectedStructure(key: String)
//    case dataBaseSavingError
//    
//    func errorDesc() -> String {
//        switch self {
//        case let .serverError(_, desc, _):
//            return desc ?? ""
//        case let .serverJSONError(JSON):
//            var errorMsg = ""
//            if let message = JSON["message"] as? String {
//                errorMsg = message
//            }
//            return errorMsg
//        case let .dictObjectNotFound(key):
//            return "Object for \"\(key)\" not found"
//        case .unexpectedResponseStructure:
//            return "Response structure is unexpected (invalid)."
//        case let .unexpectedStructure(key):
//            return "Structure of object for \"\(key)\" is unexpected (invalid)."
//        case .dataBaseSavingError:
//            return "Error occured while saving to local data base."
//        default:
//            return localizedDescription
//        }
//    }
//    
//    func errorData() -> Params? {
//        switch self {
//        case let .serverJSONError(JSON):
//            if let data = JSON["data"] as? Params {
//                return data
//            }
//            return nil
//        default:
//            return nil
//        }
//    }
//    
//    func errorCode() -> Int64 {
//        switch self {
//        case let .serverJSONError(JSON):
//            var errorCode: Int64 = 0
//            if let code = JSON["code"] as? Int64 {
//                errorCode = code
//            }
//            return errorCode
//        default:
//            return 0
//        }
//    }
//}
//
//struct ErrorData {
//    let title: String
//    var message: String
//    var errorCode: Int = 0
//    
//    init(error: AppError) {
//        switch error {
//        case .networkError:
//            title = "Ошибка соединения"
//            message = "Проверьте соединение"
//        case .wrongData, .notJSON:
//            title = "Ошибка данных"
//            message = "Неверные данные"
//        case .noData:
//            title = "Ошибка"
//            message = "Нет данных"
//        case .serverError(code: let code, desc: _, userInfo: let dict):
//            title = "Внимание"
//            message = "Что то пошло не так :("
//            errorCode = code
//            if code == 0 {
//                if let d = dict, let c = d["_kCFStreamErrorCodeKey"] as? Int, let msg = d["NSLocalizedDescription"] as? String {
//                    message = msg
//                    errorCode = c
//                }
//            } else {
//                if let d = dict {
//                    switch code {
//                    case 1:
//                        break
//                    default:
//                        message = d["message"] as? String ?? "Произошла ошибка с кодом: '\(errorCode)'"
//                    }
//                }
//            }
//        case .badInput:
//            title = "Внимание"
//            message = "Введены неверные данные"
//        default:
//            title = ""
//            message = ""
//        }
//    }
//}
