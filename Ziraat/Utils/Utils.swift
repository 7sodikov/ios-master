//
//  Utils.swift
//  Prepo
//
//  Created by Shamsiddin on 3/1/20.
//  Copyright © 2020 Jafton. All rights reserved.
//

import UIKit
import AVFoundation

public class Utils {
    
    var vibrationsCount: Int8 = 0
    
    init() {}
    
    static func delay(seconds: Double, completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
    }
    
    static func checkSize(textWidth: CGFloat, width: CGFloat, size: CGFloat, value: String) -> CGFloat {
        var valueFont = UIFont.regular(ofSize: size)
        var txtSize = size
        if textWidth > width {
            txtSize -= 1
            valueFont = UIFont.regular(ofSize: size)
            let valueSize = value.size(withConstrainedWidth: .infinity, font: valueFont)
            return checkSize(textWidth: valueSize.width, width: width, size: txtSize, value: value)
        } else {
            return txtSize
        }
    }
    
    static func vibrate() {
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    static func getObjectName(_ object: Any) -> String {
        return String(describing: type(of: object)) 
    }
    
    func lightVibrate(count: Int8 = 1) {
        if #available(iOS 10.0, *) {
            if vibrationsCount < count {
                vibrationsCount += 1
                Utils.vibrate(style: .medium)
                Utils.delay(seconds: 0.15) {
                    self.lightVibrate(count: count)
                }
            } else {
                vibrationsCount = 0
            }
        }
    }
    
    static func divideAmountString(text: String, char: String) -> String {
        if text == "" || text == "0" || String(text[0]) == "." {
            if String(text[0]) == "." {
                return "0\(text)"
            }
            return text
        }
        
        let arr = text.split(separator: ".")
        let result = String(arr[0])
        var count = 0
        var str = ""
        for i in (0..<result.count).reversed() {
            if count == 3 {
                str.append(char)
                count = 0
            }
            str.append(result[i])
            count += 1
        }
        
        var resultStr = String(str.reversed())
        if arr.count > 1 {
            if arr[1] != "00" {
                resultStr += ",\(arr[1])"
            }
        }
        
        return resultStr
    }
    
    // Doesn't work prior to iOS 10 and iPhone 7 models!
    @available(iOS 10.0, *)
    static func vibrate(style: UIImpactFeedbackGenerator.FeedbackStyle) {
        let generator = UIImpactFeedbackGenerator(style: style)
        generator.impactOccurred()
    }
    
    static func rotateTransform(_ view: UIView, angle: CGFloat, byX: Bool = false, byY: Bool = false, byZ: Bool = false) {
        var identity = CATransform3DIdentity
        identity.m34 = -1.0/500
        view.layer.transform = CATransform3DRotate(identity, angle, byX ? 1:0, byY ? 1:0, byZ ? 1:0)
        view.alpha = 1
    }
    
//    static func version() -> String {
//        if let dictionary = Bundle.main.infoDictionary {
//            if let version = dictionary["CFBundleShortVersionString"] as? String {
//                return "\(version)"
//            }
//        }
//        return UD.app_version
//    }
    
    static func buildNumber() -> String {
        if let dictionary = Bundle.main.infoDictionary {
            if let buildNumber = dictionary["CFBundleVersion"] as? String {
                return "\(buildNumber)"
            }
        }
        return ""
    }
    
    static func compileDate() -> Date {
        let bName = bundleName() ?? "Info.plist"
        if let infoPath = Bundle.main.path(forResource: bName, ofType: nil), let infoAttr = try? FileManager.default.attributesOfItem(atPath: infoPath),
            let infoDate = infoAttr[FileAttributeKey.creationDate] as? Date {
            return infoDate
        }
        return Date()
    }
    
    static func bundleName() -> String? {
        return Bundle.main.infoDictionary?["CFBundleName"] as? String
    }
    
    static func bundleDisplayName() -> String? {
        return Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String
    }
    
//    static func compileDateString() -> String {
//        return DateFormatter().getStrFromDate(compileDate(), defaultDateFormat)
//    }
    
    static func callNumberPhone(phoneNumber: String) {
        if let link = URL(string: "tel:\(phoneNumber)") {
            let canOpen = UIApplication.shared.canOpenURL(link)
            if canOpen {
                UIApplication.shared.openURL(link)
            }
        }
    }
    
    static func toggleFlash() {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if device.hasTorch {
                do {
                    try device.lockForConfiguration()
                    if device.torchMode == AVCaptureDevice.TorchMode.on {
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                        do {
                            try device.setTorchModeOn(level: 1.0)
                        } catch {
                            // do nothing
                        }
                    }
                    device.unlockForConfiguration()
                } catch {
                    // do nothing
                }
            }
        }
    }
    
    static func toggleFlashLightIsOn() -> Bool {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if device.hasTorch {
                return device.torchMode == AVCaptureDevice.TorchMode.on
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    static func statusBarHeight() -> CGFloat {
        return UIApplication.shared.statusBarFrame.size.height
    }
    
}
