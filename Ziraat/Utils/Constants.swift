//
//  Constants.swift
//  Prepo
//
//  Created by Shamsiddin on 3/1/20.
//  Copyright © 2020 Jafton. All rights reserved.
//

import UIKit

let GoogleMapsApiKey = "asdfasdfasdf"
let GooglePlacesApiKey = "aadsfasdfasdf"

let ANGLE_M_PI_2 = CGFloat(.pi * 0.5)
let REPORTS_COLLECTION_VIEW_DURATION = TimeInterval(0.25)

let defaultAnimationDuration = TimeInterval(0.3)
let defaultCashBackAnimationDuration = TimeInterval(1)
var isOfflineMode = false

typealias Params = [String: Any?]
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let timerValue = 60

struct URLConstant {
//    // "https://testmobil.ziraatbank.uz:9092"
//    static let domain = [51, 58, 39, 35, 28, 72, 91, 107, 17, 22, 16, 6, 4, 31, 22, 6, 30, 2, 90, 60, 59, 55, 8, 17, 21, 77, 78, 37, 125, 58, 24, 80, 92, 83, 77, 30].reveal
//
//    // "testmobil.ziraatbank.uz"
//    static let hostName = [47, 43, 32, 39, 2, 29, 22, 45, 9, 93, 25, 27, 27, 17, 21, 27, 16, 77, 78, 62, 103, 35, 19].reveal
    
    // "https://mobil.ziraatbank.uz:9092"
    static let domain = [51, 58, 39, 35, 28, 72, 91, 107, 8, 28, 1, 27, 5, 94, 14, 6, 0, 77, 65, 33, 43, 55, 7, 14, 89, 89, 90, 116, 106, 127, 91, 88].reveal
    
    // "mobil.ziraatbank.uz"
    static let hostName = [54, 33, 49, 58, 3, 92, 14, 45, 23, 18, 2, 6, 11, 17, 26, 4, 92, 89, 90].reveal
    
    // "https://apps.apple.com/us/app/id1540767956"
    static let appstoreURL = [51, 58, 39, 35, 28, 72, 91, 107, 4, 3, 19, 1, 71, 17, 4, 31, 30, 73, 14, 54, 38, 59, 70, 16, 4, 3, 65, 62, 35, 96, 11, 14, 84, 86, 64, 28, 23, 99, 126, 125, 84, 66].reveal
    
    // "https://bireysel.ziraatbank.uz/upload/docs/offer/offer_tr.docx"
    static let publicOfferTurkish = [51, 58, 39, 35, 28, 72, 91, 107, 7, 26, 17, 23, 16, 3, 17, 3, 92, 86, 73, 39, 40, 55, 29, 7, 22, 66, 75, 96, 38, 53, 77, 31, 21, 15, 27, 77, 68, 122, 45, 43, 2, 7, 74, 63, 15, 5, 14, 23, 93, 67, 70, 40, 54, 60, 42, 25, 16, 75, 22, 41, 12, 10].reveal
    
    // "https://bireysel.ziraatbank.uz/upload/docs/offer/offer_en.docx"
    static let publicOfferEnglish = [51, 58, 39, 35, 28, 72, 91, 107, 7, 26, 17, 23, 16, 3, 17, 3, 92, 86, 73, 39, 40, 55, 29, 7, 22, 66, 75, 96, 38, 53, 77, 31, 21, 15, 27, 77, 68, 122, 45, 43, 2, 7, 74, 63, 15, 5, 14, 23, 93, 67, 70, 40, 54, 60, 42, 8, 12, 75, 22, 41, 12, 10].reveal
    
    // "https://bireysel.ziraatbank.uz/upload/docs/offer/offer_ru.docx"
    static let publicOfferRussian = [51, 58, 39, 35, 28, 72, 91, 107, 7, 26, 17, 23, 16, 3, 17, 3, 92, 86, 73, 39, 40, 55, 29, 7, 22, 66, 75, 96, 38, 53, 77, 31, 21, 15, 27, 77, 68, 122, 45, 43, 2, 7, 74, 63, 15, 5, 14, 23, 93, 67, 70, 40, 54, 60, 42, 31, 23, 75, 22, 41, 12, 10].reveal
    
    // "https://bireysel.ziraatbank.uz/upload/docs/offer/offer_uz.docx"
    static let publicOfferUzbek = [51, 58, 39, 35, 28, 72, 91, 107, 7, 26, 17, 23, 16, 3, 17, 3, 92, 86, 73, 39, 40, 55, 29, 7, 22, 66, 75, 96, 38, 53, 77, 31, 21, 15, 27, 77, 68, 122, 45, 43, 2, 7, 74, 63, 15, 5, 14, 23, 93, 67, 70, 40, 54, 60, 42, 24, 24, 75, 22, 41, 12, 10].reveal
    
    // "https://ziraatbank.uz/en/branches-atms?BranchType=0&CityID=0"
    static let nearestBranchEnglish = [51, 58, 39, 35, 28, 72, 91, 107, 31, 26, 17, 19, 8, 4, 22, 14, 28, 71, 14, 32, 51, 121, 12, 11, 88, 78, 82, 47, 61, 44, 10, 15, 22, 78, 21, 88, 77, 38, 118, 6, 19, 21, 11, 51, 1, 55, 18, 21, 23, 17, 16, 104, 16, 39, 1, 20, 43, 33, 79, 118].reveal
    
    // "https://ziraatbank.uz/ru/branches-atms?BranchType=0&CityID=0"
    static let nearestBranchRussian = [51, 58, 39, 35, 28, 72, 91, 107, 31, 26, 17, 19, 8, 4, 22, 14, 28, 71, 14, 32, 51, 121, 27, 16, 88, 78, 82, 47, 61, 44, 10, 15, 22, 78, 21, 88, 77, 38, 118, 6, 19, 21, 11, 51, 1, 55, 18, 21, 23, 17, 16, 104, 16, 39, 1, 20, 43, 33, 79, 118].reveal
    
    // "https://ziraatbank.uz/uz/branches-atms?BranchType=0&CityID=0"
    static let nearestBranchUzbek = [51, 58, 39, 35, 28, 72, 91, 107, 31, 26, 17, 19, 8, 4, 22, 14, 28, 71, 14, 32, 51, 121, 28, 31, 88, 78, 82, 47, 61, 44, 10, 15, 22, 78, 21, 88, 77, 38, 118, 6, 19, 21, 11, 51, 1, 55, 18, 21, 23, 17, 16, 104, 16, 39, 1, 20, 43, 33, 79, 118].reveal
    
    // "https://ziraatbank.uz/tr/branches-atms?BranchType=0&CityID=0"
    static let nearestBranchTurkish = [51, 58, 39, 35, 28, 72, 91, 107, 31, 26, 17, 19, 8, 4, 22, 14, 28, 71, 14, 32, 51, 121, 29, 23, 88, 78, 82, 47, 61, 44, 10, 15, 22, 78, 21, 88, 77, 38, 118, 6, 19, 21, 11, 51, 1, 55, 18, 21, 23, 17, 16, 104, 16, 39, 1, 20, 43, 33, 79, 118].reveal
    
//    var url: URL? {
//        return try? self.rawValue.asURL()
//    }
}
